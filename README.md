# Demo of Atomic Claim Engine (ACE)

Experimental prototype to illustrate and discover the capabilities of modern administrative registers.
Built between november 2023 and march 2024 by VNG Realisatie.

The Dutch innovation research project 'Uit Betrouwbare Bron' (UBB) continued the research that started with this prototype.
See https://www.uitbetrouwbarebron.nl (Dutch) for more information about 'why', capabilities etc.

### Implemented concepts

#### Atomic Claim
Each piece of information can be changed independantly. We use the word 'claim' and not 'fact' to stress that we should allways consider applicability in context, actuality and possible annotations of doubt or other information about the 'truth value' of a claim.
The concept it self is not new. It originated in the family of Fact Oriented Methods / Fact Based Methods such as NIAM, Object Role Modeling (ORM) and FCO-IM.

#### Annotation
Introduced to model and store generic information about (any) claim, such as:
- Context: A context annotation links a claim to information about the context in which the claim originated. This can be a command or event that provides information such as type of command/event, source documents, case numbers, descriptions such as deviations from regulations.
- Time: When was the claim registered, when did it expire. When did it became valid, when did this validity end?
- Doubt: Has doubt been expressed about a claim?

Annotations are a special type of claim: a claim about any other claim. As a result annotations can be annotated. So an annotation of doubt can have its own timeline.

### License
The code is released under the [EUPL license](https://commission.europa.eu/content/european-union-public-licence_en).<br>
Files may contain code snippets from public sites such as stackoverflow. We have tried to mark all those snippets with a reference to the original discussion on the site.

### Examples
Several examples have been prepared. A full list with short descriptions can be found [here](bin/samples/EXAMPLES.md).

### Usage
- The bin folder contains binaries for:
  - Linux (x86 based processors)
  - Windows (x86 based processors)
- Select the correct zip file for your system
- Extract the zip file in a convenient location on your system
- Run the binary
- Press Ctrl+O to load a demo: [List of examples/demos](EXAMPLES.md).
- Execute the demo.

Unfortunately a native Mac version is not (yet?) available.

### Release notes

#### 14-11-2024 - Version 0.13

- All Delphi code has been ported to Lazarus/Free Pascal (open source).
- Multi platform: Windows and Linux.
- Switched from private GitHub repo to GitLab repo of the UBB project.
