program ACE;

{$MODE Delphi}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Interfaces,
  Forms,
  SysUtils,
  ace.Debug, ace.Settings, printer4lazarus, lazcontrols, pascalscript,
  common.ErrorHandling, common.Utils, common.RadioStation, common.SimpleLists,
  common.FileUtils in 'common\common.FileUtils.pas',
  diagram.Core in 'diagram\diagram.Core.pas',
  diagram.Settings in 'diagram\diagram.Settings.pas',
  diagram.Selection in 'diagram\diagram.Selection.pas',
  ace.Commands.Tokenizer in 'commands\ace.Commands.Tokenizer.pas',
  ace.Core.Engine in 'core\ace.Core.Engine.pas',
  ace.Core.Query in 'core\ace.Core.Query.pas',
  diagram.Operations in 'diagram\diagram.Operations.pas',
  diagram.InfoPanel in 'diagram\diagram.InfoPanel.pas',
  diagram.SourceClaims in 'diagram\diagram.SourceClaims.pas',
  diagram.Temporal in 'diagram\diagram.Temporal.pas',
  diagram.Lines in 'diagram\diagram.Lines.pas',
  diagram.Transaction in 'diagram\diagram.Transaction.pas',
  diagram.ClaimSets in 'diagram\diagram.ClaimSets.pas',
  diagram.Model in 'diagram\diagram.Model.pas',
  ace.Core.Tokenizer in 'core\ace.Core.Tokenizer.pas',
  ace.Core.Graph in 'core\ace.Core.Graph.pas',
  ace.Core.Notification in 'core\ace.Core.Notification.pas',
  ace.Import.xmi in 'portability\ace.Import.xmi.pas',
  ace.Forms.Main in 'interface\ace.Forms.Main.pas' {fMain},
  ace.Forms.Console in 'interface\ace.Forms.Console.pas' {fConsole},
  ace.Forms.HistorySettings in 'interface\ace.Forms.HistorySettings.pas' {fHistorySettings},
  ace.Forms.Transactions in 'interface\ace.Forms.Transactions.pas' {fTransactions},
  ace.Forms.Tutorial in 'interface\ace.Forms.Tutorial.pas' {fTutorial},
  ace.Forms.Model in 'interface\ace.Forms.Model.pas' {fModel},
  ace.Forms.GraphEditor in 'interface\ace.Forms.GraphEditor.pas' {fGraphEditor},
  ace.Forms.ScriptEditor in 'interface\ace.Forms.ScriptEditor.pas' {fScriptEditor},
  ace.Forms.Inspector in 'interface\ace.Forms.Inspector.pas' {fInspector},
  ace.Forms.Debug in 'interface\ace.Forms.Debug.pas',
  ace.Forms.SampleUI in 'interface\ace.Forms.SampleUI.pas',
  ace.Commands.Claims in 'commands\ace.Commands.Claims.pas',
  ace.Commands.Dispatcher in 'commands\ace.Commands.Dispatcher.pas',
  ace.Commands.List in 'commands\ace.Commands.List.pas',
  ace.Commands.Misc in 'commands\ace.Commands.Misc.pas',
  ace.Commands.Show in 'commands\ace.Commands.Show.pas',
  ace.Commands.Structure in 'commands\ace.Commands.Structure.pas',
  ace.Commands.Transactions in 'commands\ace.Commands.Transactions.pas',
  ace.Commands.Algorithms in 'commands\ace.Commands.Algorithms.pas',
  ace.Commands.Portability in 'commands\ace.Commands.Portability.pas',
  ace.API.Server in 'api\ace.API.Server.pas' {fAPIServer},
  ace.API.SimpleQuery in 'api\ace.API.SimpleQuery.pas',
  ace.API.Operations in 'api\ace.API.Operations.pas',
  ace.Script in 'scripting\ace.Script.pas',
  ace.Script.Debug in 'scripting\ace.Script.Debug.pas',
  ace.Script.Delphi in 'scripting\ace.Script.Delphi.pas',
  ace.Script.Json in 'scripting\ace.Script.Json.pas',
  ace.Script.API in 'scripting\ace.Script.API.pas',
  common.XML;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Scaled := True;
  Application.Initialize;
  Application.CreateForm(TfMain, fMain);
  Application.Run;
end.

