// -----------------------------------------------------------------------------
//
// ace.Debug
//
// -----------------------------------------------------------------------------
//
// Utility routines for debugging.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Debug;

{$MODE Delphi}

interface

procedure Debug(aTopic: string; aMessage: string); overload;
procedure Debug(aTopic: string; aMessage: string; const aArgs: array of const); overload;

procedure DebugIncIndent(aTopic: string);
procedure DebugDecIndent(aTopic: string);
procedure DebugResetIndent(aTopic: string);
procedure DebugClear(aTopic: string);

implementation

uses
  SysUtils, Forms,
  ace.Forms.Debug, ace.Settings;

var
  gIndent: integer;

procedure CheckDebugForm;
  begin
  if not Assigned(fDebug)
     then fDebug := TfDebug.Create(Application);
  if not fDebug.Visible
      then fDebug.Show;
  end;

function IndentStr: string;
  var
    i: integer;
  begin
  result := '';
  for i := 0 to gIndent-1 do result := result + '  ';
  end;

procedure Debug(aTopic: string; aMessage: string);
  begin
  if Application.Terminated then exit;
  Debug(aTopic, aMessage, []);
  end;

procedure Debug(aTopic: string; aMessage: string; const aArgs: array of const);
  begin
  if Application.Terminated then exit;
  if gSettings.DebugTopicEnabled(aTopic)
    then begin
         CheckDebugForm;
         fDebug.mDebug.Lines.Add(IndentStr + Format(aMessage, aArgs));
         end;
  end;

procedure DebugIncIndent(aTopic: string);
  begin
  if Application.Terminated then exit;
  if gSettings.DebugTopicEnabled(aTopic)
    then Inc(gIndent);
  end;

procedure DebugDecIndent(aTopic: string);
  begin
  if Application.Terminated then exit;
  if gSettings.DebugTopicEnabled(aTopic)
    then begin
         Dec(gIndent);
         if gIndent<0 then gIndent := 0;
         end;
  end;

procedure DebugResetIndent(aTopic: string);
  begin
  if Application.Terminated then exit;
  if gSettings.DebugTopicEnabled(aTopic)
    then gIndent := 0;
  end;

procedure DebugClear(aTopic: string);
  begin
  if Application.Terminated then exit;
  if gSettings.DebugTopicEnabled(aTopic)
    then begin
         CheckDebugForm;
         fDebug.mDebug.Clear;
         end;
  end;

initialization
  gIndent := 0;
end.
