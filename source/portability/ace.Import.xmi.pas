// -----------------------------------------------------------------------------
//
// ace.Import.xmi
//
// -----------------------------------------------------------------------------
//
// Import VNG/MIM XMI from EA.
// Produces a script that defines LabelTypes and ClaimTypes.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Import.xmi;

{$MODE Delphi}
{$WARN 4105 off : Implicit string type conversion with potential data loss from "$1" to "$2"}
{$WARN 4104 off : Implicit string type conversion from "$1" to "$2"}
interface

uses
  Classes, SysUtils, Dom, common.XML,
  common.SimpleLists,
  ace.Core.Engine;

type
  TcUMLClass = class;

  TcUMLAttribute =
    class
      private
        fClass: TcUMLClass;
        fName: string;
        fTypeClass: TcUMLClass;
        fTypeName: string;
        fStereoType: string;
        fDescription: string;
      protected
        function GetIdentifier: string;
        function GetDescriptionAsComment: string;
      public
        constructor Create(aClass: TcUMLClass);
        property Name: string read fName write fName;
        property AsIdentifier: string read GetIdentifier;
        property TypeClass: TcUMLClass read fTypeClass write fTypeClass;
        property TypeName: string read fTypeName write fTypeName;
        property StereoType: string read fStereoType write fStereoType;
        property Description: string read fDescription write fDescription;
        property DescriptionAsComment: string read GetDescriptionAsComment;
      end;

  TcUMLAttributes =
    class
      private
        fItems: TcStringObjectList<TcUMLAttribute>;
      protected
        function GetCount: integer;
        function GetItem(const aIndex: integer): TcUMLAttribute;
      public
        constructor Create;
        destructor Destroy; override;

        procedure Add(aAttribute: TcUMLAttribute);
        function Find(aName: string): TcUMLAttribute;

        property Count: integer read GetCount;
        property Items[const aIndex: integer]: TcUMLAttribute read GetItem; default;
      end;

  TcUMLClass =
    class
      private
        fID: string;
        fName: string;
        fStereoType: string;
        fDescription: string;
        fAttributes: TcUMLAttributes;
        fSupertype: TcUMLClass;
        fValues: TStringlist;
        fLength: string;
        fDefined: boolean;
      protected
        function GetIdentifier: string;
        function GetScalarName: string;
        function GetDescriptionAsComment: string;
      public
        constructor Create;
        destructor Destroy; override;

        property ID: string read fID write fID;
        property Name: string read fName write fName;
        property AsIdentifier: string read GetIdentifier;
        property AsScalar: string read GetScalarName;
        property StereoType: string read fStereoType write fStereoType;
        property Description: string read fDescription write fDescription;
        property DescriptionAsComment: string read GetDescriptionAsComment;
        property Attributes: TcUMLAttributes read fAttributes;
        property Supertype: TcUMLClass read fSupertype write fSupertype;
        property Values: TStringlist read fValues;
        property Length: string read fLength write fLength;
        property Defined: boolean read fDefined write fDefined;
      end;

  TcUMLClasses =
    class
      private
        fItems: TcStringObjectList<TcUMLClass>;
      protected
        function GetCount: integer;
        function GetItem(const aIndex: integer): TcUMLClass;
      public
        constructor Create;
        destructor Destroy; override;

        procedure Add(aClass: TcUMLClass);
        function Find(aID: string): TcUMLClass;
        function FindByName(aName: string): TcUMLClass;

        property Count: integer read GetCount;
        property Items[const aIndex: integer]: TcUMLClass read GetItem; default;
      end;

  TcUMLAssociation =
    class
      private
        fID: string;
        fName: string;
        fStereoType: string;
        fDescription: string;
        fSource: TcUMLClass;
        fTarget: TcUMLClass;
        fSourceMultiplicity: string;
        fTargetMultiplicity: string;
      protected
        function GetIdentifier: string;
        function GetDescriptionAsComment: string;
      public
        property ID: string read fID write fID;
        property Name: string read fName write fName;
        property AsIdentifier: string read GetIdentifier;
        property StereoType: string read fStereoType write fStereoType;
        property Description: string read fDescription write fDescription;
        property DescriptionAsComment: string read GetDescriptionAsComment;
        property Source: TcUMLClass read fSource write fSource;
        property Target: TcUMLClass read fTarget write fTarget;
        property SourceMultiplicity: string read fSourceMultiplicity write fSourceMultiplicity;
        property TargetMultiplicity: string read fTargetMultiplicity write fTargetMultiplicity;
    end;

  TcUMLAssociations =
    class
      private
        fItems: TcStringObjectList<TcUMLAssociation>;
      protected
        function GetCount: integer;
        function GetItem(const aIndex: integer): TcUMLAssociation;
      public
        constructor Create;
        destructor Destroy; override;

        procedure Add(aAssociation: TcUMLAssociation);

        property Count: integer read GetCount;
        property Items[const aIndex: integer]: TcUMLAssociation read GetItem; default;
      end;

  TcImportXMI =
    class
      private
        fSession: TcSession;
        fEngine: TcEngine;
        fScript: TStringlist;
        fXML: TXMLDocument;
        fUMLClasses: TcUMLClasses;
        fUMLAssociations: TcUMLAssociations;
        fTaggedValueTypes: TStringlist;

      protected

        function XMI_FindID(aID: string): IXMLNode;
        function iXMI_FindID(aID: string; aNode: IXMLNode): IXMLNode;

        procedure Execute_Pass1;
        procedure Handle_Node_Pass1(aPath: string; aNode: IXMLNode);
        procedure Handle_UMLClass_Pass1(aPath: string; aNode: TDOMElement);
        procedure Handle_UMLAttributes_Pass1(aClass: TcUMLClass; aNode: IXMLNode);
        procedure Handle_UMLAttribute_Pass1(aAttribute: TcUMLAttribute; aNode: IXMLNode);
        procedure Handle_EAStub_Pass1(aNode: TDOMElement);

        procedure Execute_Pass2;
        procedure Handle_Node_Pass2(aPath: string; aNode: IXMLNode);
        procedure Handle_UMLClass_Pass2(aPath: string; aNode: TDOMElement);
        procedure Handle_UMLAttributes_Pass2(aClass: TcUMLClass; aNode: IXMLNode);
        procedure Handle_UMLAttribute_Pass2(aAttribute: TcUMLAttribute; aNode: IXMLNode);
        procedure Handle_UMLGeneralization_Pass2(aNode: TDOMElement);
        procedure Handle_UMLAssociation_Pass2(aPath: string; aNode: TDOMElement);
        procedure Handle_UMLTaggedValue_Pass2(aPath: string; aNode: TDOMElement);

        function Fetch_AttributeTypeID(aNode: IXMLNode): string;
        function Fetch_StereoType(aNode: TDOMElement): string;
        function Fetch_TaggedValue(aTagName: string; aNode: IXMLNode): string;

        function ReferencedByAttribute(aClass: TcUMLClass): boolean;
        procedure CreateClaimTypeScript_PrimitiveDataTypes;
        procedure CreateClaimTypeScript_Enumerations;
        procedure CreateClaimTypeScript_Classes(aStereoType, aStereoTypeName: string);
        procedure CreateClaimTypeScript_Associations;
        procedure CreateClaimTypeScript;

        procedure WriteToScript(aString: string); overload;
        procedure WriteToScript(aString: string; const aArgs: array of const); overload;

        property Session: TcSession read fSession;
        property Engine: TcEngine read fEngine;
        property XML: TXMLDocument read fXML;
        property UMLClasses: TcUMLClasses read fUMLClasses;
        property UMLAssociations: TcUMLAssociations read fUMLAssociations;
        property TaggedValueTypes: TStringlist read fTaggedValueTypes;

      public
        constructor Create(aSession: TcSession; aXML: TXMLDocument);
        destructor Destroy; override;

        procedure Execute(aScriptFilename: string);
      end;

implementation

uses
  common.ErrorHandling, common.Utils,
  ace.Debug, ace.Settings;

function NodeTypeToStr(aNodeType: TNodeType): string; overload;
  begin
  case aNodeType of
    //ntReserved: result := 'Reserved';
    ntElement: result := 'Element';
    ntAttribute: result := 'Attribute';
    ntText: result := 'Text';
    ntCData: result := 'CData';
    ntEntityRef: result := 'EntityRef';
    ntEntity: result := 'Entity';
    ntProcessingInstr: result := 'ProcessingInstr';
    ntComment: result := 'Comment';
    ntDocument: result := 'Document';
    ntDocType: result := 'DocType';
    ntDocFragment: result := 'DocFragment';
    ntNotation: result := 'Notation';
    else result := 'Unknown';
    end;
  end;

procedure UnexpectedNode(aNode: IXMLNode);
  begin
  EInt('TcImportXML.*', 'Unexpected node. Name: %s - Type: %s', [aNode.NodeName, NodeTypeToStr(aNode.NodeType)]);
  end;

procedure Ignore(aNode: IXMLNode);
  begin
  if assigned(aNode) then ; // ignore compiler warning
  end;

{ TcUMLAttribute }

constructor TcUMLAttribute.Create(aClass: TcUMLClass);
  begin
  inherited Create;
  fClass := aClass;
  end;

function TcUMLAttribute.GetIdentifier: string;
  begin
  result := ToIdentifier(Name, ['-'], '_');
  end;

function TcUMLAttribute.GetDescriptionAsComment: string;
  begin
  result := StringReplace(Description, #13, '', [rfReplaceAll]);
  result := StringReplace(result, #10, '', [rfReplaceAll]);
  end;


{ TcUMLAttributes }

constructor TcUMLAttributes.Create;
  begin
  inherited Create;
  fItems := TcStringObjectList<TcUMLAttribute>.Create(true {Owned}, false {Sorted});
  end;

destructor TcUMLAttributes.Destroy;
  begin
  fItems.Free;
  inherited;
  end;

function TcUMLAttributes.GetCount: integer;
  begin
  result := fItems.Count;
  end;

function TcUMLAttributes.GetItem(const aIndex: integer): TcUMLAttribute;
  begin
  result := fItems.Objects[aIndex];
  end;

procedure TcUMLAttributes.Add(aAttribute: TcUMLAttribute);
  begin
  fItems.Add(aAttribute.Name, aAttribute);
  end;

function TcUMLAttributes.Find(aName: string): TcUMLAttribute;
  var
    lIndex: integer;
  begin
  if fItems.Find(aName, lIndex)
     then result := fItems.Objects[lIndex]
     else result := nil;
  end;


{ TcUMLClass }

constructor TcUMLClass.Create;
  begin
  inherited Create;
  fAttributes := TcUMLAttributes.Create;
  fValues := TStringlist.Create;
  fDefined := false;
  end;

destructor TcUMLClass.Destroy;
  begin
  fAttributes.Free;
  fValues.Free;
  inherited;
  end;

function TcUMLClass.GetIdentifier: string;
  begin
  result := ToIdentifier(Name, ['-'], '_');
  end;

function TcUMLClass.GetScalarName: string;
  begin
  result := '';
  if Name='CharacterString'
     then result := 'string'
  else if Name='DateTime'
     then result := 'timestamp'
  else if Name='Integer'
     then result := 'integer'
  else if Name='Boolean'
     then result := 'boolean'
     else EInt('TcUMLClass.GetScalarName', 'Unexpected class name: %s', [Name]);
  end;

function TcUMLClass.GetDescriptionAsComment: string;
  begin
  result := StringReplace(Description, #13, '', [rfReplaceAll]);
  result := StringReplace(result, #10, '', [rfReplaceAll]);
  end;



{ TcUMLClasses }

constructor TcUMLClasses.Create;
  begin
  inherited Create;
  fItems := TcStringObjectList<TcUMLClass>.Create(true {Owned}, true {Sorted});
  end;

destructor TcUMLClasses.Destroy;
  begin
  fItems.Free;
  inherited;
  end;

function TcUMLClasses.GetCount: integer;
  begin
  result := fItems.Count;
  end;

function TcUMLClasses.GetItem(const aIndex: integer): TcUMLClass;
  begin
  result := fItems.Objects[aIndex];
  end;

procedure TcUMLClasses.Add(aClass: TcUMLClass);
  begin
  fItems.Add(aClass.ID, aClass);
  end;

function TcUMLClasses.Find(aID: string): TcUMLClass;
  var
    lIndex: integer;
  begin
  if fItems.Find(aID, lIndex)
     then result := fItems.Objects[lIndex]
     else result := nil;
  end;

function TcUMLClasses.FindByName(aName: string): TcUMLClass;
  var
    c: integer;
  begin
  result := nil;
  for c := 0 to fItems.Count-1 do
    begin
    if fItems.Objects[c].Name = aName
       then begin
            result := fItems.Objects[c];
            exit;
            end;
    end;
  end;


{ TcUMLAssociation }

function TcUMLAssociation.GetIdentifier: string;
  begin
  result := Format('%s_%s_%s', [
     Source.AsIdentifier,
     ToIdentifier(Name, ['-'], '_'),
     Target.AsIdentifier
  ]);
  end;

function TcUMLAssociation.GetDescriptionAsComment: string;
  begin
  result := StringReplace(Description, #13, '', [rfReplaceAll]);
  result := StringReplace(result, #10, '', [rfReplaceAll]);
  end;


{ TcUMLAssociations }

constructor TcUMLAssociations.Create;
  begin
  inherited Create;
  fItems := TcStringObjectList<TcUMLAssociation>.Create(true {Owned}, true {Sorted});
  end;

destructor TcUMLAssociations.Destroy;
  begin
  fItems.Free;
  inherited;
  end;

function TcUMLAssociations.GetCount: integer;
  begin
  result := fItems.Count;
  end;

function TcUMLAssociations.GetItem(const aIndex: integer): TcUMLAssociation;
  begin
  result := fItems.Objects[aIndex];
  end;

procedure TcUMLAssociations.Add(aAssociation: TcUMLAssociation);
  begin
  fItems.Add(aAssociation.ID, aAssociation);
  end;


{ TcImportXMI }

constructor TcImportXMI.Create(aSession: TcSession; aXML: TXMLDocument);
  begin
  inherited Create;
  fSession := aSession;
  fEngine := aSession.Engine;
  fScript := TStringlist.Create;
  fXML := aXML;
  fUMLClasses := TcUMLClasses.Create;
  fUMLAssociations := TcUMLAssociations.Create;
  fTaggedValueTypes := TStringlist.Create;
  fTaggedValueTypes.Sorted := true;
  fTaggedValueTypes.Duplicates := dupIgnore;
  end;

destructor TcImportXMI.Destroy;
  begin
  fScript.Free;
  fUMLClasses.Free;
  fUMLAssociations.Free;
  fTaggedValueTypes.Free;
  inherited;
  end;

procedure TcImportXMI.Execute(aScriptFilename: string);
  var
    t: integer;
  begin
  Debug('xmi', 'First pass:');
  DebugIncIndent('xmi');
  Execute_Pass1;
  DebugDecIndent('xmi');

  Debug('xmi', '');
  Debug('xmi', 'Second pass:');
  DebugIncIndent('xmi');
  Execute_Pass2;
  DebugDecIndent('xmi');

  Debug('xmi', '');
  Debug('xmi', 'Tagged value types:');
  for t := 0 to TaggedValueTypes.Count-1 do
    Debug('xmi', '  %s', [TaggedValueTypes[t]]);

  Debug('xmi', '');
  Debug('xmi', 'Creating claimtype script');
  CreateClaimTypeScript;

  fScript.SaveToFile(aScriptFilename);
  Debug('xmi', 'Script %s saved.', [aScriptFilename]);
  end;

procedure TcImportXMI.Execute_Pass1;
  var
    i: integer;
    lNode: IXMLNode;
  begin
  for i := 0 to XML.ChildNodes.Count-1 do
    begin
    lNode := XML.ChildNodes[i];

    if lNode.NodeType = ntProcessingInstr
       then continue
    else if (lNode.NodeType = ntElement) and (CompareText(lNode.NodeName, 'XMI')=0)
       then Handle_Node_Pass1('', lNode as TDOMElement)
    else UnexpectedNode(lNode);
    end;
  end;

procedure TcImportXMI.Execute_Pass2;
  var
    i: integer;
    lNode: IXMLNode;
  begin
  for i := 0 to XML.ChildNodes.Count-1 do
    begin
    lNode := XML.ChildNodes[i];

    if lNode.NodeType = ntProcessingInstr
       then continue
    else if (lNode.NodeType = ntElement) and (CompareText(lNode.NodeName, 'XMI')=0)
       then Handle_Node_Pass2('', lNode)
    else UnexpectedNode(lNode);
    end;
  end;

procedure TcImportXMI.Handle_Node_Pass1(aPath: string; aNode: IXMLNode);
  var
    c: integer;
    lNode: IXMLNode;
  begin
  // Path
  if (aNode.NodeName <> 'XMI') and
     (aNode.NodeName <> 'XMI.content') and
     (aNode.NodeName <> 'UML:Model') and
     (aNode.NodeName <> 'UML:Namespace.ownedElement')
     then begin
          if aPath<>'' then aPath := aPath + '/';
          if (aNode is TDOMElement) and TDOMElement(aNode).HasAttribute('name')
             then aPath := aPath + Format('%s', [TDOMElement(aNode).AttribStrings['name']])
             else aPath := aPath + Format('%s', [aNode.NodeName]);
          end;

  // Process child nodes
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];

    // Classes
    if (lNode.NodeName='UML:Class')
       then Handle_UMLClass_Pass1(aPath, lNode as TDOMElement)

    // Stubs
    else if (lNode.NodeName='EAStub')
       then Handle_EAStub_Pass1(lNode as TDOMElement)

    // Other
    else Handle_Node_Pass1(aPath, lNode);
    end;
  end;

procedure TcImportXMI.Handle_Node_Pass2(aPath: string; aNode: IXMLNode);
  var
    c: integer;
    lNode: IXMLNode;
  begin
  // Path
  if (aNode.NodeName <> 'XMI') and
     (aNode.NodeName <> 'XMI.content') and
     (aNode.NodeName <> 'UML:Model') and
     (aNode.NodeName <> 'UML:Namespace.ownedElement')
     then begin
          if aPath<>'' then aPath := aPath + '/';
          if (aNode is TDOMElement) and TDOMElement(aNode).HasAttribute('name')
             then aPath := aPath + Format('%s', [TDOMElement(aNode).AttribStrings['name']])
             else aPath := aPath + Format('%s', [aNode.NodeName]);
          end;

  // Process child nodes
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];

    // Classes
    if (lNode.NodeName='UML:Class')
       then Handle_UMLClass_Pass2(aPath, lNode as TDOMElement)
    else if (lNode.NodeName='UML:Generalization')
       then Handle_UMLGeneralization_Pass2(lNode as TDOMElement)
    else if (lNode.NodeName='UML:Association')
       then Handle_UMLAssociation_Pass2(aPath, lNode as TDOMElement)
    else if (aNode.NodeName='XMI.content') and (lNode.NodeName='UML:TaggedValue')
       then Handle_UMLTaggedValue_Pass2(aPath, lNode as TDOMElement)

    // Other
    else Handle_Node_Pass2(aPath, lNode);
    end;
  end;

procedure TcImportXMI.Handle_UMLClass_Pass1(aPath: string; aNode: TDOMElement);
  var
    c: integer;
    lClass: TcUMLClass;
    lNode: IXMLNode;
  begin
  if not aNode.HasAttribute('name') then EInt('TcImportXMI.Handle_UMLClass_Pass1', 'name expected');
  if not aNode.HasAttribute('xmi.id') then EInt('TcImportXMI.Handle_UMLClass_Pass1', 'xml.id expected');

  if aNode.AttribStrings['name']='EARootClass' then exit;

  Debug('xmi', aPath);
  DebugIncIndent('xmi');

  // Fetch attributes
  lClass := TcUMLClass.Create;
  lClass.ID := aNode.AttribStrings['xmi.id'];
  lClass.Name := aNode.AttribStrings['name'];

  Debug('xmi', 'Class: %s', [lClass.Name]);
  DebugIncIndent('xmi');
  Debug('xmi', 'id: %s', [lClass.id]);

  // Process child nodes
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];

    // Stereotype
    if (lNode.NodeName='UML:ModelElement.stereotype')
       then begin
            lClass.StereoType := Fetch_StereoType(lNode as TDOMElement);
            Debug('xmi', 'stereoType: %s', [lClass.StereoType]);
            end

    // Description
    else if (lNode.NodeName='UML:ModelElement.taggedValue')
       then begin
            lClass.Description := Fetch_TaggedValue('documentation', lNode);
            if Length(lClass.Description)>30
               then Debug('xmi', 'description: %s...', [Copy(lClass.Description, 1, 30)])
               else Debug('xmi', 'description: %s', [lClass.Description]);
            end

    // Attributes
    else if (lNode.NodeName='UML:Classifier.feature')
       then begin
            Debug('xmi', 'attributes:');
            Handle_UMLAttributes_Pass1(lClass, lNode);
            end

    else if (lNode.NodeName='UML:ModelElement.constraint')
       then Ignore(lNode)
       else UnexpectedNode(lNode);
    end;
  DebugDecIndent('xmi');
  DebugDecIndent('xmi');

  // Remember Class
  UMLClasses.Add(lClass);
  end;

procedure TcImportXMI.Handle_UMLClass_Pass2(aPath: string; aNode: TDOMElement);
  var
    c: integer;
    lClass: TcUMLClass;
    lNode: IXMLNode;
  begin
  if not aNode.HasAttribute('name') then EInt('TcImportXMI.Handle_UMLClass_Pass2', 'name expected');
  if not aNode.HasAttribute('xmi.id') then EInt('TcImportXMI.Handle_UMLClass_Pass2', 'xml.id expected');
  if aNode.AttribStrings['name']='EARootClass' then exit;

  Debug('xmi', aPath);
  DebugIncIndent('xmi');

  // Fetch attributes
  lClass := UMLClasses.Find(aNode.AttribStrings['xmi.id']);
  if not Assigned(lClass) then EInt('TcImportXMI.Handle_UMLClass_Pass2', 'Class not found');

  Debug('xmi', 'Class: %s', [lClass.Name]);
  DebugIncIndent('xmi');
  Debug('xmi', 'id: %s', [lClass.id]);

  // Process child nodes
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];

    // Attributes
    if (lNode.NodeName='UML:Classifier.feature')
       then begin
            Debug('xmi', 'attributes:');
            Handle_UMLAttributes_Pass2(lClass, lNode);
            end

    else if
       (lNode.NodeName='UML:ModelElement.stereotype') or
       (lNode.NodeName='UML:ModelElement.taggedValue') or
       (lNode.NodeName='UML:ModelElement.constraint')
       then Ignore(lNode)
       else UnexpectedNode(lNode);
    end;
  DebugDecIndent('xmi');
  DebugDecIndent('xmi');
  end;

procedure TcImportXMI.Handle_UMLAttributes_Pass1(aClass: TcUMLClass; aNode: IXMLNode);
  var
    c: integer;
    lNode: IXMLNode;
    lAttribute: TcUMLAttribute;
  begin
  // Process child nodes
  DebugIncIndent('xmi');
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];

    // Stereotype
    if (lNode.NodeName='UML:Attribute')
       then begin
            if aClass.StereoType='Enumeratie'
               then aClass.Values.Add(TDOMElement(lNode).AttribStrings['name'])
               else begin
                    lAttribute := TcUMLAttribute.Create(aClass);
                    lAttribute.Name := TDOMElement(lNode).AttribStrings['name'];
                    Handle_UMLAttribute_Pass1(lAttribute, lNode);
                    aClass.Attributes.Add(lAttribute);
                    end;
            end
       else UnexpectedNode(lNode);
    end;
  DebugDecIndent('xmi');
  end;

procedure TcImportXMI.Handle_UMLAttributes_Pass2(aClass: TcUMLClass; aNode: IXMLNode);
  var
    c: integer;
    lNode: IXMLNode;
    lAttribute: TcUMLAttribute;
  begin
  // Process child nodes
  DebugIncIndent('xmi');
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];

    // Stereotype
    if (lNode.NodeName='UML:Attribute')
       then begin
            if aClass.StereoType<>'Enumeratie'
               then begin
                    lAttribute := aClass.Attributes.Find(TDOMElement(lNode).AttribStrings['name']);
                    if not Assigned(lAttribute) then EInt('TcImportXMI.Handle_UMLAttributes_Pass2', 'Attribute not found');
                    Handle_UMLAttribute_Pass2(lAttribute, lNode);
                    end;
            end
       else UnexpectedNode(lNode);
    end;
  DebugDecIndent('xmi');
  end;

procedure TcImportXMI.Handle_UMLAttribute_Pass1(aAttribute: TcUMLAttribute; aNode: IXMLNode);
  var
    c: integer;
    lNode: IXMLNode;
  begin
  // Process child nodes
  Debug('xmi', 'Attribute: '+aAttribute.Name);
  DebugIncIndent('xmi');
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];

    // Stereotype
    if (lNode.NodeName='UML:ModelElement.stereotype')
       then begin
            aAttribute.StereoType := Fetch_StereoType(lNode as TDOMElement);
            Debug('xmi', 'stereoType: %s', [aAttribute.StereoType]);
            end
    else if (lNode.NodeName='UML:ModelElement.taggedValue')
       then begin
            aAttribute.Description := Fetch_TaggedValue('description', lNode);
            if Length(aAttribute.Description)>30
               then Debug('xmi', 'description: %s...', [Copy(aAttribute.Description, 1, 30)])
               else Debug('xmi', 'description: %s', [aAttribute.Description]);

            aAttribute.TypeName := Fetch_TaggedValue('type', lNode);
            Debug('xmi', 'Type Name: %s', [aAttribute.TypeName]);
            end
    else if
       (lNode.NodeName='UML:Attribute.initialValue') or
       (lNode.NodeName='UML:ModelElement.constraint') or
       (lNode.NodeName='UML:StructuralFeature.type')
       then Ignore(lNode)
       else UnexpectedNode(lNode);
    end;
  DebugDecIndent('xmi');
  end;

procedure TcImportXMI.Handle_UMLAttribute_Pass2(aAttribute: TcUMLAttribute; aNode: IXMLNode);
  var
    c: integer;
    lNode: IXMLNode;
    lTypeID: string;
    lClass: TcUMLClass;
  begin
  // Process child nodes
  Debug('xmi', 'Attr: '+aAttribute.Name);
  DebugIncIndent('xmi');
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];

    // Stereotype
    if (lNode.NodeName='UML:StructuralFeature.type')
       then begin
            lTypeID := Fetch_AttributeTypeID(lNode);
            Debug('xmi', 'Type ID: %s', [lTypeID]);

            lClass := UMLClasses.Find(lTypeID);
            if Assigned(lClass)
               then aAttribute.TypeClass := lClass
               else Debug('xmi', '!! Could not resolve type reference: %s. Type: %s', [lTypeID, aAttribute.TypeName]);
            end
    else if
       (lNode.NodeName='UML:Attribute.initialValue') or
       (lNode.NodeName='UML:ModelElement.stereotype') or
       (lNode.NodeName='UML:ModelElement.taggedValue') or
       (lNode.NodeName='UML:ModelElement.constraint')
       then Ignore(lNode)
       else UnexpectedNode(lNode);
    end;
  DebugDecIndent('xmi');
  end;

procedure TcImportXMI.Handle_EAStub_Pass1(aNode: TDOMElement);
  var
    lID: string;
    lName: string;
    lStereotype: string;
    lClass: TcUMLClass;
  begin
  lID := aNode.AttribStrings['xmi.id'];
  lName := aNode.AttribStrings['name'];
  lStereotype := aNode.AttribStrings['UMLType'];
  lClass := UMLClasses.Find(lID);
  if not Assigned(lClass)
     then begin
          lClass := TcUMLClass.Create;
          lClass.ID := lID;
          lClass.Name := lName;
          lClass.StereoType := lStereotype;
          UMLClasses.Add(lClass);
          end;
  end;

procedure TcImportXMI.Handle_UMLGeneralization_Pass2(aNode: TDOMElement);
  var
    lSubtypeID, lSuperTypeID: string;
    lSubtype, lSupertype: TcUMLClass;
  begin
  if not aNode.HasAttribute('subtype') then EInt('TcImportXMI.Handle_UMLGeneralization', 'Attribute subtype expected');
  if not aNode.HasAttribute('supertype') then EInt('TcImportXMI.Handle_UMLGeneralization', 'Attribute supertype expected');

  lSubtypeID := aNode.AttribStrings['subtype'];
  lSupertypeID := aNode.AttribStrings['supertype'];

  lSubtype := UMLClasses.Find(lSubtypeID);
  if not Assigned(lSubtype) then EInt('TcImportXMI.Handle_UMLGeneralization_Pass2', 'Subtype not found');
  lSupertype := UMLClasses.Find(lSupertypeID);
  if not Assigned(lSupertype) then EInt('TcImportXMI.Handle_UMLGeneralization_Pass2', 'Supertype not found');
  lSubtype.Supertype := lSupertype;
  end;

procedure TcImportXMI.Handle_UMLAssociation_Pass2(aPath: string; aNode: TDOMElement);
  var
    c, s: integer;
    lStereotype: string;
    lAssociation: TcUMLAssociation;
    lNode, lSubNode, lSubSubNode: IXMLNode;
    lName, lMultiplicity, lEnd: string;
  begin
  if not aNode.HasAttribute('xmi.id') then EInt('TcImportXMI.Handle_UMLAssociation_Pass2', 'xml.id expected');

  // Check stereotype
  lNode := aNode.FindNode('UML:ModelElement.stereotype');
  if not Assigned(lNode) then EInt('TcImportXMI.Handle_UMLAssociation_Pass2', 'UML:ModelElement.stereotype expected');
  lStereotype := Fetch_StereoType(lNode as TDOMElement);
  if lStereotype = 'trace' then exit;

  if not aNode.HasAttribute('name') then EInt('TcImportXMI.Handle_UMLAssociation_Pass2', 'name expected');

  Debug('xmi', aPath);
  DebugIncIndent('xmi');

  // Fetch attributes
  lAssociation := TcUMLAssociation.Create;
  lAssociation.ID := aNode.AttribStrings['xmi.id'];
  lAssociation.Name := aNode.AttribStrings['name'];
  lAssociation.StereoType := lStereotype;

  Debug('xmi', 'Association: %s', [lAssociation.Name]);
  DebugIncIndent('xmi');

  // Process child nodes
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];

    if (lNode.NodeName='UML:ModelElement.taggedValue')
       then begin
            // Description
            lAssociation.Description := Fetch_TaggedValue('documentation', lNode);
            if Length(lAssociation.Description)>30
               then Debug('xmi', 'description: %s...', [Copy(lAssociation.Description, 1, 30)])
               else Debug('xmi', 'description: %s', [lAssociation.Description]);

            // Source
            lName := Fetch_TaggedValue('ea_sourceName', lNode);
            lAssociation.Source := UMLClasses.FindByName(lName);
            if not Assigned(lAssociation.Source) then EInt('TcImportXMI.Handle_UMLAssociation_Pass2', 'Source not found');
            Debug('xmi', 'source: %s', [lName]);

            // Target
            lName := Fetch_TaggedValue('ea_targetName', lNode);
            lAssociation.Target := UMLClasses.FindByName(lName);
            if not Assigned(lAssociation.Target) then EInt('TcImportXMI.Handle_UMLAssociation_Pass2', 'Target not found');
            Debug('xmi', 'target: %s', [lName]);

												// <UML:TaggedValue tag="lb" value="0..*"/>
												// <UML:TaggedValue tag="rb" value="1"/>
            end
    else if (lNode.NodeName='UML:Association.connection')
       then begin
            for s := 0 to lNode.ChildNodes.Count-1 do
              begin
              lSubNode := lNode.ChildNodes[s];
              if (lSubNode.NodeName='UML:AssociationEnd')
                 then begin
                      if not (lSubNode as TDOMElement).HasAttribute('multiplicity') then EInt('TcImportXMI.Handle_UMLAssociation_Pass2', 'Multiplicity expected');
                      lMultiplicity := (lSubNode as TDOMElement).AttribStrings['multiplicity'];

                      if lSubNode.ChildNodes.Count<>1 then EInt('TcImportXMI.Handle_UMLAssociation_Pass2', 'Only one subnode expected');
                      lSubSubNode := lSubNode.ChildNodes[0];
                      if lSubSubNode.NodeName<>'UML:ModelElement.taggedValue' then EInt('TcImportXMI.Handle_UMLAssociation_Pass2', 'Tagged value expected');
                      lEnd := Fetch_TaggedValue('ea_end', lSubSubNode);

                      if lEnd='source'
                         then begin
                              lAssociation.SourceMultiplicity := lMultiplicity;
                              Debug('xmi', 'Source multiplicity: %s', [lMultiplicity]);
                              end
                      else if lEnd='target'
                         then begin
                              lAssociation.TargetMultiplicity := lMultiplicity;
                              Debug('xmi', 'Target multiplicity: %s', [lMultiplicity]);
                              end
                         else EInt('TcImportXMI.Handle_UMLAssociation_Pass2', 'Unexpected ea_end');
                      end
                 else UnexpectedNode(lNode);
              end;
            end

    else if (lNode.NodeName='UML:ModelElement.stereotype')
       then Ignore(lNode)
       else UnexpectedNode(lNode);
    end;
  DebugDecIndent('xmi');
  DebugDecIndent('xmi');

  // Remember Association
  UMLAssociations.Add(lAssociation);
  end;

procedure TcImportXMI.Handle_UMLTaggedValue_Pass2(aPath: string; aNode: TDOMElement);
  var
    lTag: string;
    lValue: string;
    lElementID: string;
    lNode: IXMLNode;
    lClass: TcUMLClass;
    lContext: string;
  begin
  (* Tags found in EA XMI export:
  class:Alternatieve naam
  class:Begrip
  class:Datum opname
  class:Definitie
  class:Eigenaar
  class:Formeel patroon
  class:Herkomst
  class:Herkomst definitie
  class:Is afgeleid
  class:Kwaliteit
  class:Kwaliteitsbegrip
  class:Lengte
  class:Locatie
  class:Maximumwaarde
  class:Minimumwaarde
  class:Patroon
  class:Populatie
  class:Positie
  class:Regels
  class:Toelichting
  model:Afkorting
  model:Alternatieve naam
  model:Beheerder
  model:Definitie
  model:Herkomst
  model:Imvertor
  model:Informatiedomein
  model:Informatiemodel type
  model:Is afgeleid
  model:MIM extensie
  model:MIM taal
  model:MIM versie
  model:Niveau
  model:Relatiemodelleringstype
  model:relatiemodelleringtype
  model:release
  model:Supplier-name
  model:Supplier-project
  model:Supplier-release
  model:Toelichting
  model:tpos
  model:Versie ID
  model:Versienummer ImZTC
  model:Versienummer RGBZ
  model:Web locatie
  package:Afkorting
  package:Alternatieve naam
  package:Beheerder
  package:Definitie
  package:Intern project
  package:Interne naam
  package:Interne release
  package:Is afgeleid
  package:Positie
  package:Ref-release
  package:Ref-version
  package:release
  package:Supplier-package-name
  package:Toelichting
  package:Versie ID
  package:Web locatie
  *)

  lTag := aNode.AttribStrings['tag'];

  Debug('xmi', aPath);
  DebugIncIndent('xmi');
  Debug('xmi', 'Tag: %s', [lTag]);
  DebugIncIndent('xmi');
  try
    if not aNode.HasAttribute('modelElement')
       then begin
            Debug('xmi', 'Tagged value has no model element');
            Exit;
            end;

    lElementID := aNode.AttribStrings['modelElement'];
    lClass := UMLClasses.Find(lElementID);
    if Assigned(lClass)
       then lContext := 'class'
       else begin
            lNode := XMI_FindID(lElementID);
            if (lNode.NodeName='UML:Model')
               then lContext := 'model'
            else if (lNode.NodeName='UML:ClassifierRole')
               then lContext := 'package'
               else EInt('TcImportXMI.Handle_UMLTaggedValue_Pass2', 'Tagged value for unexpected node: %s', [lNode.NodeName]);
            end;

    // Fetch value
    if aNode.HasAttribute('value')
       then lValue := aNode.AttribStrings['value'];

    // Store value
    if (lContext='class') and (lTag='Lengte')
       then begin
            lClass.Length := lValue;
            end;

  finally
    TaggedValueTypes.Add(lContext+':'+lTag);
    DebugDecIndent('xmi');
    DebugDecIndent('xmi');
    end;
  end;

function TcImportXMI.Fetch_StereoType(aNode: TDOMElement): string;
  var
    c: integer;
    lNode: IXMLNode;
  begin
  result := '';

  // Process child nodes
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];
    if not (lNode as TDOMElement).HasAttribute('name') then EInt('TcImportXMI.Fetch_StereoType', 'Attribute name expected.');

    // Stereotype
    if (lNode.NodeName='UML:Stereotype')
       then result := (lNode as TDOMElement).AttribStrings['name']
       else UnexpectedNode(lNode);
    end;
  end;

function TcImportXMI.Fetch_AttributeTypeID(aNode: IXMLNode): string;
  var
    c: integer;
    lNode: IXMLNode;
  begin
  result := '';

  // Process child nodes
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];
    if not (lNode as TDOMElement).HasAttribute('xmi.idref') then EInt('TcImportXMI.Fetch_AttrTypeID', 'Attribute xmi.idref expected.');

    // Stereotype
    if (lNode.NodeName='UML:Classifier')
       then begin
            result := (lNode as TDOMElement).AttribStrings['xmi.idref'];
            exit;
            end
       else UnexpectedNode(lNode);
    end;
  end;

function TcImportXMI.Fetch_TaggedValue(aTagName: string; aNode: IXMLNode): string;
  var
    c: integer;
    lNode: IXMLNode;
  begin
  result := '';

  // Process child nodes
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];
    if not (lNode as TDOMElement).HasAttribute('tag') then EInt('TcImportXMI.Fetch_TaggedValue', 'Attribute tag expected.');

    if (lNode.NodeName='UML:TaggedValue')
       then begin
            if (lNode as TDOMElement).AttribStrings['tag'] = aTagName
               then begin
                    result := (lNode as TDOMElement).AttribStrings['value'];
                    exit;
                    end;
            end
       else UnexpectedNode(lNode);
    end;
  end;

function TcImportXMI.XMI_FindID(aID: string): IXMLNode;
  var
    i: integer;
    lNode: IXMLNode;
  begin
  result := nil;
  for i := 0 to XML.ChildNodes.Count-1 do
    begin
    lNode := XML.ChildNodes[i];

    if lNode.NodeType = ntProcessingInstr
       then continue
    else if (lNode.NodeType = ntElement) and (CompareText(lNode.NodeName, 'XMI')=0)
       then result := iXMI_FindID(aID, lNode)
    else UnexpectedNode(lNode);
    end;
  end;

function TcImportXMI.iXMI_FindID(aID: string; aNode: IXMLNode): IXMLNode;
  var
    c: integer;
    lNode: IXMLNode;
  begin
  result := nil;
  for c := 0 to aNode.ChildNodes.Count-1 do
    begin
    lNode := aNode.ChildNodes[c];
    if (lNode is TDOMElement) and (lNode as TDOMElement).HasAttribute('xmi.id')
       then begin
            if (lNode as TDOMElement).AttribStrings['xmi.id']=aID
               then begin
                    result := lNode;
                    exit;
                    end
               else result := iXMI_FindID(aID, lNode);
            end
       else result := iXMI_FindID(aID, lNode);

    if assigned(result)
      then exit;
    end;
  end;

function TcImportXMI.ReferencedByAttribute(aClass: TcUMLClass): boolean;
  var
    c, a: integer;
    lClass: TcUMLClass;
    lAttribute: TcUMLAttribute;
  begin
  result := false;
  for c := 0 to UMLClasses.Count-1 do
    begin
    lClass := UMLClasses[c];
    for a := 0 to lClass.Attributes.Count-1 do
      begin
      lAttribute := lClass.Attributes[a];
      if lAttribute.TypeClass = aClass
         then begin
              result := true;
              exit;
              end;
      end;
    end;
  end;

procedure TcImportXMI.CreateClaimTypeScript_PrimitiveDataTypes;
  var
    c: integer;
    lClass: TcUMLClass;
    lStr1, lStr2: string;
  begin
  // Primitief datatype -> Labeltype
  WriteToScript('');
  WriteToScript('');
  WriteToScript('// Primitive Datatype -> LabelTypes');
  WriteToScript('');
  for c := 0 to UMLClasses.Count-1 do
    begin
    lClass := UMLClasses[c];
    if (lClass.StereoType <> 'Primitief datatype')
       then continue;
    if not ReferencedByAttribute(lClass)
       then continue;

    lClass.Defined := true;
    if lClass.Description<>''
       then WriteToScript('// %s', [lClass.DescriptionAsComment]);
    WriteToScript('define labelType: %s', [lClass.AsIdentifier]);
    WriteToScript('  scalar: %s', [lClass.Supertype.AsScalar]);
    if Trim(lClass.Length)<>''
       then begin
            SplitString(lClass.Length, '#NOTES#', lStr1, lStr2);
            if lStr1<>''
               then WriteToScript('// Length = %s', [lStr1]);
            end;

    WriteToScript('');
    end;
  end;

procedure TcImportXMI.CreateClaimTypeScript_Enumerations;
  var
    c: integer;
    lClass: TcUMLClass;
  begin
  // Enumeration -> Labeltype
  WriteToScript('');
  WriteToScript('');
  WriteToScript('// Enumerations -> LabelTypes');
  WriteToScript('');
  for c := 0 to UMLClasses.Count-1 do
    begin
    lClass := UMLClasses[c];

    // Correct stereotype?
    if (lClass.StereoType <> 'Enumeratie')
       then continue;

    // Are there references to this labeltype?
    if not ReferencedByAttribute(lClass)
       then continue;

    lClass.Defined := true;
    if lClass.Description<>''
       then WriteToScript('// %s', [lClass.DescriptionAsComment]);
    WriteToScript('define labelType: %s', [lClass.AsIdentifier]);
    WriteToScript('  scalar: string');
    WriteToScript('// Allowed values: %s', [StringlistToString(lClass.Values, ', ')]);
    WriteToScript('');
    end;
  end;

procedure TcImportXMI.CreateClaimTypeScript_Classes(aStereoType, aStereoTypeName: string);
  var
    c, a, ca: integer;
    lProgress: integer;
    lClass, lSupertype, lTypeClass: TcUMLClass;
    lAttribute, lCompositeAttribute: TcUMLAttribute;
    lFirstAttribute: integer;
    lClassesToProcess: TStringlist;
  begin
  // StereoType -> ClaimTypes
  WriteToScript('');
  WriteToScript('');
  WriteToScript('// %s -> ClaimTypes', [aStereoTypeName]);
  WriteToScript('');

  lClassesToProcess := TStringlist.Create;
  try
    repeat
      lProgress := 0;
      for c := 0 to UMLClasses.Count-1 do
        begin
        lClass := UMLClasses[c];

        if lClass.Defined then continue; // Already defined?
        if (lClass.StereoType <> aStereoType) then continue; // Correct stereotype?
        if lClass.Attributes.Count=0 // Has attributes?
           then begin
                WriteToScript('Class %s ignored. No attributes defined.', [lClass.Name]);
                Continue;
                end;
        if Assigned(lClass.Supertype) and not lClass.Supertype.Defined then Continue; // Supertype defined?

        Inc(lProgress);
        lClass.Defined := true;
        lClassesToProcess.AddObject(lClass.Name, lClass);

        // Main ClaimType
        if lClass.Description<>''
           then WriteToScript('// %s', [lClass.DescriptionAsComment]);
        if not Assigned(lClass.Supertype)
           then begin
                // 'Normal' class
                lAttribute := lClass.Attributes[0];
                if lAttribute.Description<>''
                   then WriteToScript('// %s', [lAttribute.DescriptionAsComment]);
                WriteToScript('define claimType: %s', [lClass.AsIdentifier]);
                WriteToScript('  expressionTemplate: Er bestaat een %s <%s:%s>', [lClass.AsIdentifier, lAttribute.AsIdentifier, lAttribute.TypeClass.AsIdentifier]);
                WriteToScript('  nestedExpressionTemplate: %s <%s:%s>', [lClass.AsIdentifier, lAttribute.AsIdentifier, lAttribute.TypeClass.AsIdentifier]);
                WriteToScript('  identity: %s', [lAttribute.AsIdentifier]);
                end
           else begin
                // Supertype
                lSupertype := lClass.Supertype;
                WriteToScript('define claimType: %s', [lClass.AsIdentifier]);
                WriteToScript('  expressionTemplate: <%s> is een %s', [lSupertype.AsIdentifier, lClass.AsIdentifier]);
                WriteToScript('  nestedExpressionTemplate: %s <%s>', [lClass.AsIdentifier, lSupertype.AsIdentifier]);
                WriteToScript('  identity: %s', [lSupertype.AsIdentifier]);
                end;

        // History limitations for reference lists
        if (lClass.StereoType = 'Referentielijst')
           then WriteToScript('  allowchange: false');
        WriteToScript('');
        end;

      until lProgress=0;

    // Groupable ClaimTypes
    WriteToScript('');
    WriteToScript('');
    WriteToScript('// Attributes of %s -> ClaimTypes', [aStereoTypeName]);
    WriteToScript('');
    for c := 0 to lClassesToProcess.Count-1 do
      begin
      lClass := TcUMLClass(lClassesToProcess.Objects[c]);
      if Assigned(lClass.Supertype)
         then lFirstAttribute := 0
         else lFirstAttribute := 1;

      for a := lFirstAttribute to lClass.Attributes.Count-1 do
        begin
        lAttribute := lClass.Attributes[a];
        lTypeClass := lAttribute.TypeClass;
        if lTypeClass.Attributes.Count=0
           then begin
                // 'Normal' attribute
                if lAttribute.Description<>''
                   then WriteToScript('// %s', [lAttribute.DescriptionAsComment]);
                WriteToScript('define claimType: %s/%s', [lClass.AsIdentifier, lAttribute.AsIdentifier]);
                WriteToScript('  expressionTemplate: <%s> heeft <%s:%s>', [lClass.AsIdentifier, lAttribute.AsIdentifier, lAttribute.TypeClass.AsIdentifier]);
                WriteToScript('  identity: %s', [lClass.AsIdentifier]);
                WriteToScript('');
                end
           else begin
                // Attribute references a class ('Gegevensgroep')
                for ca := 0 to lTypeClass.Attributes.Count-1 do
                  begin
                  lCompositeAttribute := lTypeClass.Attributes[ca];
                  if lCompositeAttribute.Description<>''
                     then WriteToScript('// %s', [lCompositeAttribute.DescriptionAsComment]);
                  WriteToScript('define claimType: %s/%s_%s', [lClass.AsIdentifier, lAttribute.AsIdentifier, lCompositeAttribute.AsIdentifier]);
                  WriteToScript('  expressionTemplate: <%s> heeft <%s_%s:%s>', [lClass.AsIdentifier, lAttribute.AsIdentifier, lCompositeAttribute.AsIdentifier, lCompositeAttribute.TypeClass.AsIdentifier]);
                  WriteToScript('  identity: %s', [lClass.AsIdentifier]);
                  WriteToScript('');
                  end;
                end;
        end;
      end;
  finally
    lClassesToProcess.Free;
    end;
  end;

procedure TcImportXMI.CreateClaimTypeScript_Associations;
  var
    r: integer;
    lAssociation: TcUMLAssociation;
  begin
  WriteToScript('');
  WriteToScript('');
  WriteToScript('// Associations -> ClaimTypes');
  WriteToScript('');
  for r := 0 to UMLAssociations.Count-1 do
    begin
    lAssociation := UMLAssociations[r];

    if lAssociation.Description<>''
       then WriteToScript('// %s', [lAssociation.DescriptionAsComment]);
    WriteToScript('define claimType: %s', [lAssociation.AsIdentifier]);
    WriteToScript('  expressionTemplate: <%s> %s <%s>', [
       lAssociation.Source.AsIdentifier, lAssociation.Name, lAssociation.Target.AsIdentifier]);
    if (lAssociation.TargetMultiplicity='0..1') or (lAssociation.TargetMultiplicity='1')
       then WriteToScript('  identity: %s', [lAssociation.Source.AsIdentifier])
    else if (lAssociation.SourceMultiplicity='0..1') or (lAssociation.SourceMultiplicity='1')
       then WriteToScript('  identity: %s', [lAssociation.Target.AsIdentifier])
    else WriteToScript('  identity: %s, %s', [lAssociation.Source.AsIdentifier, lAssociation.Target.AsIdentifier]);
    WriteToScript('');
    end;
  end;

procedure TcImportXMI.CreateClaimTypeScript;
  begin
  WriteToScript('// Quick fixes');
  WriteToScript('');
  WriteToScript('define labelType: CharacterString');
  WriteToScript('  scalar: string');
  WriteToScript('');
  WriteToScript('define labelType: Date');
  WriteToScript('  scalar: date');
  WriteToScript('');

  CreateClaimTypeScript_PrimitiveDataTypes;
  CreateClaimTypeScript_Enumerations;
  CreateClaimTypeScript_Classes('Referentielijst', 'Reference lists');
  CreateClaimTypeScript_Classes('Objecttype', 'Objecttypes');
  CreateClaimTypeScript_Associations;
  end;

procedure TcImportXMI.WriteToScript(aString: string);
  begin
  fScript.Add(aString);
  end;

procedure TcImportXMI.WriteToScript(aString: string; const aArgs: array of const);
  begin
  WriteToScript(Format(aString, aArgs));
  end;

end.
