// -----------------------------------------------------------------------------
//
// ace.Forms.SampleUI
//
// -----------------------------------------------------------------------------
//
// Form to demo a sample user interface.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Forms.SampleUI;

interface

uses
  LCLType, SysUtils, Classes, Types, Menus, Buttons, Graphics, Controls,
  Forms, StdCtrls, ExtCtrls, Grids,
  common.SimpleLists, Generics.Collections,
  ace.Core.Engine, ace.Core.Query;

type

  { TfSampleUI }

  TfSampleUI = class(TForm)
    ImageList: TImageList;
    pnlClass: TPanel;
    pnlHeader: TPanel;
    lClassName: TLabel;
    pnlOperations: TPanel;
    sgOperations: TStringGrid;
    lClassDivider: TShape;
    lClassCaption: TLabel;
    lOperationsCaption: TLabel;
    lOperationsDivider: TShape;
    sbTimeNormal: TSpeedButton;
    sbTimeTravel: TSpeedButton;
    pnlOperation: TPanel;
    lOperationDivider: TShape;
    lOperationCaption: TLabel;
    pnlValidity: TPanel;
    lValidityCaption: TLabel;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure pnlClassPaint(Sender: TObject);
    procedure pnlHeaderPaint(Sender: TObject);
    procedure pnlOperationPaint(Sender: TObject);
    procedure pnlOperationsPaint(Sender: TObject);
    procedure pnlValidityPaint(Sender: TObject);
    procedure sgOperationsSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
    procedure sgOperationsDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sbTimeNormalClick(Sender: TObject);
  private
    { Private declarations }
    fSessionOwned: boolean;
    fSession: TcSession;
    fEngine: TcEngine;

    fClass: TcClass;
    fMainClaimIdentity: String;
    fMainClaimIDStr: String;
    fOperation: TcOperation;
    fValidTimes: TcIntegerlist;
    fFilterValidFrom: integer;
    fFilterValidUntil: integer;

    fInitialized: boolean;
    fEdits: TList<TEdit>;
    fIndicators: TList<TImage>;
    fValidTimeTabs: TList<TPanel>;
    fEditDocument: TEdit;
    fLabelDocumentType: TLabel;
    fEditDossier: TEdit;
    fEditRechtsgrond: TEdit;
    fEditToelichting: TMemo;

    procedure Draw;
    procedure RedrawClass;
    procedure RedrawOperations;
    procedure RedrawOperation;
    procedure InitComponents;
    function CreateDataLabel(aIndex: integer; aData: String; aParent: TWinControl): TLabel;
    function CreateEdit(aIndex: integer; aParent: TWinControl): TEdit;
    function CreateLabel(aIndex: integer; aName: String; aParent: TWinControl): TLabel;
    function CreateMemo(aIndex, aHeightInRows: integer; aParent: TWinControl): TMemo;
    function CreateIndicator(aIndex: integer; aHint: String; aParent: TWinControl): TImage;
    procedure DeriveValidTimeline;
    procedure SetValidAtCaption(aValidTimeIndex: integer);
    procedure CreateValidTimeTabs;
    procedure ValidityPanelClick(Sender: TObject);

  public
    { Public declarations }
    procedure Init(aSession: TcSession; aClass: TcClass; aMainClaimIdentity: String);

    property Session: TcSession read fSession;
    property Engine: TcEngine read fEngine;
    property Operation: TcOperation read fOperation;
  end;

var
  fSampleUI: TfSampleUI;

implementation

{$R *.lfm}

uses
  common.ErrorHandling,
  ace.Settings, diagram.Settings;

const
  cRoundCornerSpacing = 4;

type
  { TCustomEditHelper }
  // See: https://forum.lazarus.freepascal.org/index.php/topic,6188.msg472489.html#msg472489
  TCustomEditHelper = class helper for TCustomEdit
    procedure HInit;
    procedure HPaint;
  end;

 TCustomPanelHelper = class helper for TCustomPanel
   procedure HInit;
   procedure HPaint;
 end;

{ TfClaimSet }

procedure TfSampleUI.FormCreate(Sender: TObject);
  begin
  fInitialized := false;

  fSession := nil;
  fSessionOwned := false;
  fEngine := nil;
  fOperation := nil;
  fValidTimes := TcIntegerList.Create;
  fValidTimes.IgnoreDuplicates := true;
  fValidTimes.Sorted := true;
  fFilterValidFrom := cNoTime;
  fFilterValidUntil := cEndOfTime;

  pnlHeader.Width := pnlClass.Width;

  gSettings.LoadFormPosition(Self);

  ClientWidth := pnlHeader.Width + 16;
  ClientHeight := pnlClass.Top + pnlClass.Height + 8;
  pnlOperations.Hide;
  //pnlOperation.Hide; - Font gets garbled when hide and show is called
  pnlValidity.Hide;
  sbTimeTravel.Hide;

  fEdits := TList<TEdit>.Create;
  fIndicators := TList<TImage>.Create;
  fValidTimeTabs := TList<TPanel>.Create;

  Application.HintColor := clWebLemonChiffon;

  sgOperations.FocusColor := sgOperations.Color;

  // Init rounded corners
  pnlHeader.hInit;
  pnlClass.hInit;
  pnlOperations.hInit;
  pnlOperation.hInit;
  pnlValidity.hInit;
  end;

procedure TfSampleUI.pnlClassPaint(Sender: TObject);
  var
    lEdit: TEdit;
  begin
  if Sender = pnlClass
    then pnlClass.HPaint;
  for lEdit in fEdits do
     lEdit.HPaint;
  end;

procedure TfSampleUI.pnlHeaderPaint(Sender: TObject);
  begin
  pnlHeader.HPaint;
  end;

procedure TfSampleUI.pnlOperationPaint(Sender: TObject);
  begin
  if not fInitialized
    then Exit;

  if Sender = pnlOperation
    then pnlOperation.HPaint;
  fEditDocument.HPaint;
  fEditDossier.HPaint;
  fEditRechtsgrond.HPaint;
  fEditToelichting.HPaint;
  end;

procedure TfSampleUI.pnlOperationsPaint(Sender: TObject);
  begin
  pnlOperations.HPaint;
  end;

procedure TfSampleUI.pnlValidityPaint(Sender: TObject);
  begin
  pnlValidity.hPaint;
  end;

procedure TfSampleUI.FormDestroy(Sender: TObject);
  begin
  fEdits.Free;
  fIndicators.Free;
  fValidTimeTabs.Free;

  fValidTimes.Free;

  gSettings.SaveFormPosition(Self);

  if fSessionOwned
     then begin
          if Assigned(fSession.Transaction)
             then fSession.Rollback;
          FreeAndNil(fSession);
          end;
  end;

procedure TfSampleUI.FormClose(Sender: TObject; var CloseAction: TCloseAction);
  begin
  CloseAction := caFree;
  end;


// -- StringGrid drawing handlers

procedure TfSampleUI.sbTimeNormalClick(Sender: TObject);
  begin
  ClientWidth := pnlOperation.Left + pnlOperation.Width + 8;
  ClientHeight := pnlValidity.Top + pnlValidity.Height + 8;
  pnlHeader.Width := pnlOperation.Left + pnlOperation.Width - pnlClass.Left;
  sbTimeNormal.Hide;
  SbTimeTravel.Show;
  pnlOperations.Show;
  //pnlOperation.Show;
  pnlValidity.Show;
  end;


procedure TfSampleUI.sgOperationsDrawCell(Sender: TObject; ACol, ARow: Integer;  Rect: TRect; State: TGridDrawState);
  var
    sg: TStringGrid;
    lOperation: TcOperation;
    lStr: string;
  begin
  sg := TStringGrid(Sender);
  lOperation := nil;

  if ( (aRow >= 0) and (aRow <= sg.RowCount-1) )
     and
     ( (aCol >= 0) and (aCol <= sg.ColCount-1) )
     then begin
          lStr := sg.Cells[aCol, aRow];
          if Assigned(sg.Objects[aCol, aRow])
             then lOperation := TcOperation(sg.Objects[aCol, aRow]);

          if Assigned(lOperation) and Assigned(Operation) and
             (lOperation.RegisteredAt < Operation.RegisteredAt)
             then sg.Canvas.Brush.Color := clWhite
          else if Assigned(lOperation) and Assigned(Operation) and
             (lOperation.RegisteredAt = Operation.RegisteredAt)
             then sg.Canvas.Brush.Color := clWebLemonChiffon
             else sg.Canvas.Brush.Color := clBtnFace;

          sg.Canvas.FillRect(Rect);
          sg.Canvas.Font.Color := clBlack;
          sg.Canvas.TextOut(Rect.Left+4, Rect.Top+6, lStr);
          end;
  end;


// -- Components / Drawing

procedure TfSampleUI.Init(aSession: TcSession; aClass: TcClass; aMainClaimIdentity: String);
  var
    lClaimSet: TcClaimSet;
  begin
  if Assigned(fSession) then EInt('TfSampleUI.Init', 'Already initialized');

  // -- Assign or create session
  if not Assigned(aSession)
     then begin
          fSession := TcSession.Create(gSettings.Engine);
          fSessionOwned := true;
          end
     else fSession := aSession;
  fEngine := fSession.Engine;

  // -- Prepare data to be displayed
  fClass := aClass;
  fMainClaimIdentity := aMainClaimIdentity;
  Assert(Assigned(fClass.MainClaimType));
  Assert(Assigned(fClass.MainClaimType.ClaimSetList));
  lClaimSet := fClass.MainClaimType.ClaimSetList.Find(aMainClaimIdentity);
  Assert(Assigned(lClaimSet));
  Assert(lClaimSet.SourceClaims.Count=1);
  fMainClaimIDStr := lClaimSet.SourceClaims[0].IDStr;

  // -- Create components
  InitComponents;

  // -- Draw actual contents
  Draw;

  fInitialized := true;
  end;

const
  cTop = 44;
  cRowHeight = 34; //32;
  cHintMargin = 10;

function TfSampleUI.CreateLabel(aIndex: integer; aName: String; aParent: TWinControl): TLabel;
  begin
  result := TLabel.Create(Self);
  with result do
    begin
    Left := 3;
    Width := 118;
    Height := 16;
    Alignment := taRightJustify;
    AutoSize := False;
    Font.Charset := DEFAULT_CHARSET;
    Font.Color := clBlack;
    Font.Height := -13;
    Font.Name := 'Inter';
    Font.Style := [];
    ParentFont := False;
    Top := cTop + 5 {3} + (aIndex * cRowHeight);
    Caption := aName;
    end;
  result.Parent := aParent;
  end;

function TfSampleUI.CreateEdit(aIndex: integer; aParent: TWinControl): TEdit;
  begin
  result := TEdit.Create(Self);
  with result do
    begin
    Left := 129;
    Width := 234 - cHintMargin;
    Height := 26; //23;
    AutoSize := False;
    Caption := '';
    Font.Charset := ANSI_CHARSET;
    Font.Color := clWindowText;
    Font.Height := -13;
    Font.Name := 'Inter';
    Font.Style := [];
    TabOrder := 0;
    TabStop := False;
    ParentFont := False;
    Top := cTop + (aIndex * cRowHeight);
    ReadOnly := True;
    Text := ''
    end;
  result.Parent := aParent;
  result.HInit;
  end;

function TfSampleUI.CreateMemo(aIndex, aHeightInRows: integer; aParent: TWinControl): TMemo;
  begin
  result := TMemo.Create(Self);
  with result do
    begin
    Left := 129;
    Width := 234 - cHintMargin;
    Height := 23 * aHeightInRows;
    Caption := '';
    Font.Charset := ANSI_CHARSET;
    Font.Color := clWindowText;
    Font.Height := -13;
    Font.Name := 'Inter';
    Font.Style := [];
    TabOrder := 0;
    TabStop := False;
    ParentFont := False;
    Top := cTop + (aIndex * cRowHeight);
    ReadOnly := True;
    WordWrap := True;
    Text := '';
    end;
  result.Parent := aParent;
  result.HInit;
  end;

function TfSampleUI.CreateDataLabel(aIndex: integer; aData: String; aParent: TWinControl): TLabel;
  begin
  result := TLabel.Create(Self);
  with result do
    begin
    Left := 129;
    Width := 230 - cHintMargin;
    Height := 16;
    Alignment := taRightJustify;
    AutoSize := False;
    Font.Charset := DEFAULT_CHARSET;
    Font.Color := clBlack;
    Font.Height := -11;
    Font.Name := 'Inter';
    Font.Style := [];
    ParentFont := False;
    Top := cTop -3 + (aIndex * cRowHeight);
    Caption := aData;
    end;
  result.Parent := aParent;
  end;

function TfSampleUI.CreateIndicator(aIndex: integer; aHint: String; aParent: TWinControl): TImage;
  begin
  result := TImage.Create(Self);
  with result do
    begin
    Left := 355;
    Width := 16;
    Height := 16;
    Images := ImageList;
    ImageIndex := 0;

    Top := cTop -4 + (aIndex * cRowHeight);
    Hint := aHint;
    ParentShowHint := False;
    ShowHint := Hint<>'';
    end;
  result.Parent := aParent;
  result.BringToFront;
  end;

procedure TfSampleUI.InitComponents;
  var
    r: integer;
    lRole: TcRole;
    lEdit: TEdit;
  begin
  // Class name
  lClassName.Caption := Format('%s', [fClass.Name]);

  // Create fields for class
  for r := 0 to fClass.Roles.Count-1 do
    begin
    lRole := fClass.Roles[r];
    CreateLabel(r, lRole.Name, pnlClass);
    lEdit := CreateEdit(r, pnlClass);
    fEdits.Add(lEdit);
    end;

  // Create fields for Operation
  r := 0;
  CreateLabel(r, 'Document', pnlOperation);
  fEditDocument := CreateEdit(r, pnlOperation);

  r := 1;
  fLabelDocumentType := CreateDataLabel(r, '...', pnlOperation);

  r := 2;
  CreateLabel(r, 'Dossier', pnlOperation);
  fEditDossier := CreateEdit(r, pnlOperation);

  r := 3;
  CreateLabel(r, 'Rechtsgrond', pnlOperation);
  fEditRechtsgrond := CreateEdit(r, pnlOperation);

  r := 4;
  CreateLabel(r, 'Toelichting', pnlOperation);
  fEditToelichting := CreateMemo(r, 4, pnlOperation);
  end;

procedure TfSampleUI.Draw;
  begin
  RedrawOperations;
  RedrawOperation;
  DeriveValidTimeline;
  RedrawClass;
  end;

procedure TfSampleUI.RedrawOperations;
  var
    op, i: integer;
    lOperation: TcOperation;
  begin
  sgOperations.RowCount := Engine.Operations.Count;
  i := 0;
  for op := Engine.Operations.Count-1 downto 0 do
    begin
    lOperation := Engine.Operations[op];

    sgOperations.Cells[0, i] :=
      ' T'+IntToStr(lOperation.RegisteredAt)+
      ' - ' + lOperation.Name;

    sgOperations.Objects[0, i] := lOperation;

    inc(i);
    end;
  end;

procedure TfSampleUI.RedrawOperation;
  var
    ct: integer;
    lClaimType: TcClaimType;
    lElement: TcElement;
    lClaimSet: TcClaimSet;
    lClaim: TcClaim;
    lClaimDossier, lClaimDocument, lClaimDocumentOmschr: TcClaim;
  begin
  sgOperations.Refresh;

  fEditDocument.Text := '';
  fEditDocument.Color := clWhite;
  fEditDossier.Text := '';
  fEditDossier.Color := clWhite;
  fEditRechtsgrond.Text := '';
  fEditRechtsgrond.Color := clWhite;
  fEditToelichting.Lines.Clear;
  fEditToelichting.Color := clWhite;

  lOperationCaption.Caption := '?';
  lOperationCaption.Font.Color := clGrayText;

  if not Assigned(Operation)
     then exit;

  lOperationCaption.Caption := Operation.Name;
  lOperationCaption.Font.Color := clBlack;

  for ct := 0 to Engine.OperationClaimTypes.Count-1 do
    begin
    lClaimType := Engine.OperationClaimTypes[ct];
    lClaimSet := lClaimType.ClaimSetList.Find(cRefChar+Operation.IDStr);
    if not Assigned(lClaimSet) then continue;
    if lClaimSet.SourceClaims.Count=0 then continue;
    lClaim := lClaimSet.SourceClaims[0];

    if CompareText(lClaimType.Name, 'Operation\Document')=0
       then begin
            // Verwijzing naar Source claim van type Document
            lElement := Engine.FindElementByRefIDStr(lClaim.GetValueStrWithoutIdentity);
            Assert(Assigned(lElement));
            Assert(lElement is TcClaim);
            lClaimDocument := lElement as TcClaim;
            if Assigned(lClaimDocument)
               then begin
                    fEditDocument.Text := lClaimDocument.ClaimSet.IdentityStr;
                    fEditDocument.Color := clWebLemonChiffon;
                    end;

            // Bepaal document type
            lClaimType := Engine.ConceptualTypes.FindClaimType('Document\Omschrijving');
            Assert(Assigned(lClaimType));
            lClaimSet := lClaimType.ClaimSetList.Find(lClaim.GetValueStrWithoutIdentity);
            if not Assigned(lClaimSet) then continue;
            if lClaimSet.SourceClaims.Count=0 then continue;
            lClaimDocumentOmschr := lClaimSet.SourceClaims[0];
            if not Assigned(lClaimDocumentOmschr) then continue;
            fLabelDocumentType.Caption := lClaimDocumentOmschr.GetValueStrWithoutIdentity;
            end
    else if CompareText(lClaimType.Name, 'Operation\Dossier')=0
       then begin
            // Verwijzing naar Source claim van type Dossier
            lElement := Engine.FindElementByRefIDStr(lClaim.GetValueStrWithoutIdentity);
            Assert(Assigned(lElement));
            Assert(lElement is TcClaim);
            lClaimDossier := lElement as TcClaim;
            if Assigned(lClaimDossier)
               then begin
                    fEditDossier.Text := lClaimDossier.ClaimSet.IdentityStr;
                    fEditDossier.Color := clWebLemonChiffon;
                    end;
            end
    else if CompareText(lClaimType.Name, 'Operation\Rechtsgrond')=0
       then begin
            fEditRechtsgrond.Text := lClaim.GetValueStrWithoutIdentity;
            fEditRechtsgrond.Color := clWebLemonChiffon;
            end
    else if CompareText(lClaimType.Name, 'Operation\Toelichting')=0
       then begin
            fEditToelichting.Lines.Add(lClaim.GetValueStrWithoutIdentity);
            fEditToelichting.Color := clWebLemonChiffon;
            end;
    end;

  // Force a repaint of the edits due to the possible chaning colors
  pnlOperationPaint(nil);
  pnlClassPaint(nil);
  end;

procedure TfSampleUI.RedrawClass;
  var
    lEdit: TEdit;
    i, ct, c, a: integer;
    lClaimType: TcClaimType;
    lClaimSet: TcClaimSet;
    lQuery: TcTemporalQuery;
    lClaim: TcClaim;
    lSourceClaim: TcClaim;
    lAnnotation: TcAnnotation;
  begin
  // -- Destroy old indicators
  for i := 0 to fIndicators.Count-1 do
    fIndicators[i].Free;
  fIndicators.Clear;

  //DebugClear('x');
  //Debug('x', 'Filter ValidFrom: T%d', [fFilterValidFrom]);
  //Debug('x', 'Filter ValidUntil: T%d', [fFilterValidUntil]);

  // -- Identity in first Edit
  Assert(fEdits.Count>0);
  Assert(Assigned(fEdits[0]));
  lEdit := fEdits[0];
  lEdit.Text := fMainClaimIdentity;

  for ct := 0 to fClass.GroupedClaimTypes.Count-1 do
    begin
    Assert(fEdits.Count>ct);
    Assert(Assigned(fEdits[ct+1]));
    lEdit := fEdits[ct+1];

    // Assume no value
    lEdit.Text := '';
    lEdit.Color := clWhite;

    // Find ClaimSet
    lClaimType := fClass.GroupedClaimTypes[ct];
    lClaimSet := lClaimType.ClaimSetList.Find(cRefChar+fMainClaimIDStr);
    if not Assigned(lClaimSet)
       then Continue;

    //Debug('x', 'ClaimType: %s', [lClaimType.Name]);

    lQuery := TcTemporalQuery.Create(Session);
    try
      lQuery.AssignClaimSet(lClaimSet);

      if Assigned(Operation)
         then begin
              lQuery.FromTT := Operation.RegisteredAt;
              lQuery.UntilTT := Operation.RegisteredAt;
              lQuery.FromVT := cNoTime;
              lQuery.UntilVT := cNoTime;
              end
         else lQuery.SetDefaultFilter;
      lQuery.Execute;

      for c := 0 to lClaimSet.DerivedClaims.Count-1 do
        begin
        lClaim := lClaimSet.DerivedClaims[c];
        if lQuery.InResult(lClaim)
           then begin
                //Debug('x', '  Claim: %s - %s', [lClaim.IDStr, lClaim.GetValueStrWithoutIdentity]);
                //Debug('x', '    Valid: T%d - T%d', [lClaim.ValidFrom, lClaim.ValidUntil]);

                // Value found, display?
                if (fFilterValidFrom = cNoTime) or // No filter
                   (
                      (lClaim.ValidFrom <= fFilterValidFrom) and
                      (lClaim.ValidUntil >= fFilterValidUntil)
                   )
                   then begin
                        //Debug('x', '    >> Display');
                        lEdit.Text := lClaim.GetValueStrWithoutIdentity;
                        if Assigned(Operation) and (lClaim.Operation = Operation)
                           then lEdit.Color := clWebLemonChiffon;

                        lSourceClaim := Engine.FindClaim(lClaim.SourceClaim);
                        if Assigned(lSourceClaim) and (lSourceClaim.Annotations.Count>0)
                           then begin
                                // Claim has annotations
                                for a := 0 to lSourceClaim.Annotations.Count-1 do
                                  begin
                                  lAnnotation := lSourceClaim.Annotations[a];
                                  if lAnnotation.Kind = akDoubt
                                     then begin
                                          if not Assigned(Operation) or
                                             ( Assigned(Operation) and (lAnnotation.RegisteredAt <= Operation.RegisteredAt) )
                                             then begin
                                                  fIndicators.Add(CreateIndicator(
                                                    ct+1,
                                                    format('Twijfel geregistreerd op T%d.', [lAnnotation.RegisteredAt]),
                                                    pnlClass));
                                                  end;
                                          end;
                                  end;
                                end;
                        end;
                end;
        end;
    finally
      lQuery.Free;
      end;
    end;

  pnlClassPaint(nil);
  end;

procedure TfSampleUI.DeriveValidTimeline;
  var
    ct, c: integer;
    lClaimType: TcClaimType;
    lClaimSet: TcClaimSet;
    lQuery: TcTemporalQuery;
    lClaim: TcClaim;
  begin
  //DebugClear('x');

  fValidTimes.Clear;
  for ct := 0 to fClass.GroupedClaimTypes.Count-1 do
    begin
    // Find ClaimSet
    lClaimType := fClass.GroupedClaimTypes[ct];
    lClaimSet := lClaimType.ClaimSetList.Find(cRefChar+fMainClaimIDStr);
    if not Assigned(lClaimSet)
       then Continue;

    lQuery := TcTemporalQuery.Create(Session);
    try
      lQuery.AssignClaimSet(lClaimSet);
      if Assigned(Operation)
         then begin
              lQuery.FromTT := Operation.RegisteredAt;
              lQuery.UntilTT := Operation.RegisteredAt;
              end
         else begin
              lQuery.FromTT := Engine.SystemTime;
              lQuery.UntilTT := Engine.SystemTime;
              end;
      lQuery.FromVT := cNoTime;
      lQuery.UntilVT := cNoTime;
      lQuery.Execute;

      for c := 0 to lClaimSet.DerivedClaims.Count-1 do
        begin
        lClaim := lClaimSet.DerivedClaims[c];
        //Debug('x', 'Claim: Type = %s, Value = %s', [lClaimType.Name, lClaim.GetValueStrWithoutIdentity]);
        //Debug('x', '  Valid From: T%d', [lClaim.ValidFrom]);
        if lQuery.InResult(lClaim)
           then begin
                fValidTimes.Add(lClaim.ValidFrom);
                //Debug('x', '  In result');
                end;
           //else Debug('x', '  NOT in result');
        end;

    finally
      lQuery.Free;
      end;
    end;

  CreateValidTimeTabs;
  end;

procedure TfSampleUI.SetValidAtCaption(aValidTimeIndex: integer);
  var
    lPanel: TPanel;
  begin
  lValidityCaption.Caption := 'Geldig op:';
  for lPanel in fValidTimeTabs do
    begin
    if lPanel.Tag = aValidTimeIndex
      then begin
           lPanel.Color := clWebLemonChiffon;
           lPanel.BevelOuter := bvLowered;
           lPanel.Font.Style := [fsBold];
           end
      else begin
           lPanel.Color := clWhite;
           lPanel.BevelOuter := bvRaised;
           lPanel.Font.Style := [];
           end;
    end;
  end;

procedure TfSampleUI.CreateValidTimeTabs;
  const
    cPanelWidth = 32;
    cPanelHeight = 24;

    cPanelSpacer = 4;

  var
    lPanel: TPanel;
    I: Integer;
  begin
  for lPanel in fValidTimeTabs do
    lPanel.Free;
  fValidTimeTabs.Clear;

  for i := 0 to fValidTimes.Count - 1 do
    begin
    lPanel := TPanel.Create(Self);
    lPanel.Caption := Format('T%d', [fValidTimes[i]]);
    lPanel.SetBounds(lValidityCaption.Left + I * (cPanelWidth + cPanelSpacer), 32, cPanelWidth, cPanelHeight);
    lPanel.Tag := I;
    lPanel.Color := clWhite;
    lPanel.OnClick := ValidityPanelClick;
    lPanel.Parent := pnlValidity;
    fValidTimeTabs.Add(lPanel);
    end;

  if fValidTimeTabs.Count > 0
    then SetValidAtCaption(fValidTimeTabs.Count-1);
  end;

// -- Interface handlers

procedure TfSampleUI.ValidityPanelClick(Sender: TObject);
  var
    lTag: Integer;
  begin
  lTag := (Sender as TComponent).Tag;
  fFilterValidFrom := fValidTimes[lTag];
  if lTag = fValidTimes.Count - 1
     then fFilterValidUntil := cEndOfTime
     else fFilterValidUntil := fValidTimes[lTag+1];

  SetValidAtCaption(lTag);
  RedrawClass;
  end;

procedure TfSampleUI.sgOperationsSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
  begin
  if Assigned(sgOperations.Objects[aCol, aRow])
     then fOperation := TcOperation(sgOperations.Objects[aCol, aRow])
     else fOperation := nil;
  DeriveValidTimeLine;
  RedrawClass;
  RedrawOperation;
  end;

{ TCustomEditHelper}
procedure TCustomEditHelper.HInit;
  begin
  AutoSize := false;
  BorderStyle := bsNone;
  SetBounds(Left + cRoundCornerSpacing, Top + cRoundCornerSpacing, Width - cRoundCornerSpacing- 3, Height - 8);
  end;

procedure TCustomEditHelper.HPaint;
  var
    WC: TWinControl;
    CV: TCanvas;
    R: TRect;
  begin
  R := BoundsRect;
  WC := Self;
  while not (WC is TCustomControl) do
    WC := WC.parent;
  CV := (WC as TCustomControl).Canvas;

  CV.Brush := Brush;
  CV.Pen.Color := clWindowFrame;

  CV.RoundRect(Left - cRoundCornerSpacing, Top - cRoundCornerSpacing, R.Right + cRoundCornerSpacing + 1, R.Bottom + cRoundCornerSpacing + 1, 10, 10);
  end;

{ TCustomPanelHelper }
procedure TCustomPanelHelper.HInit;
  begin
  AutoSize := false;
  BorderStyle := bsNone;
  BevelOuter := bvNone;

  Color := clWhite;
  end;

procedure TCustomPanelHelper.HPaint;
  begin
  Canvas.Pen.Color := clWindowFrame;
  Canvas.Brush.Color := clBtnFace;
  Canvas.RoundRect(0, 0 , Width, Height, 14, 14);
  end;

end.

