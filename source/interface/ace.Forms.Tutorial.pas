// -----------------------------------------------------------------------------
//
// ace.forms.Tutorial
//
// -----------------------------------------------------------------------------
//
// Form to display tutorial pages.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Forms.Tutorial;

{$MODE Delphi}

interface

uses
  SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, ExtCtrls;

type

  { TfTutorial }

  TfTutorial = class(TForm)
    Image: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private

  public
    procedure ShowStep(aSequenceNr: integer);
  end;

var
  fTutorial: TfTutorial;

implementation

{$R *.lfm}

uses
  common.FileUtils, ace.Settings;

procedure TfTutorial.FormClose(Sender: TObject; var Action: TCloseAction);
  begin
  Action := caFree;
  end;

procedure TfTutorial.FormCreate(Sender: TObject);
  begin
  gSettings.LoadFormPosition(Self);
  end;

procedure TfTutorial.FormDestroy(Sender: TObject);
  begin
  gSettings.SaveFormPosition(Self);

  fTutorial := nil;
  end;

procedure TfTutorial.ShowStep(aSequenceNr: integer);
  var
    lPath, lFilename: string;
  begin
  lPath := TPath.Combine(gSettings.ProjectFolder, 'tutorial');
  lFilename := TPath.Combine(lPath, FormaT('Slide%d.png', [aSequenceNr]));
  Image.Picture.LoadFromFile(lFilename);
  Image.Transparent := false;
  ClientWidth := Image.Picture.Width;
  ClientHeight := Image.Picture.Height;
  end;

end.
