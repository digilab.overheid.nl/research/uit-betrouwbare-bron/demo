// -----------------------------------------------------------------------------
//
// ace.Forms.ClaimSet
//
// -----------------------------------------------------------------------------
//
// Form to inspect Operations, Claims and Annotations.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Forms.Inspector;

{$MODE Delphi}

interface

uses
  SysUtils, Variants, Classes, Types,
  Graphics, Controls, Forms, Dialogs, ComCtrls, StdCtrls, ExtCtrls, ActnList,
  common.RadioStation, common.SimpleLists, common.Utils,
  ace.Core.Engine, ace.Core.Query, ace.Forms.HistorySettings,
  diagram.Core, diagram.Selection, diagram.Operations, diagram.InfoPanel, diagram.SourceClaims, diagram.Temporal,
  Menus, Buttons;

const
  cMinimizedFormWidth = 640;
  cMinimizedFormHeight = 480;
  cNormalFormWidth = 1000;
  cNormalFormHeight = 1000;

  cLaneMargin = 0;

  cInspectorCanvasColor: TColor = clBtnFace;

  cQueryHint =
    'Click on the time axis to select a moment.'#13#10 +
    'Shift + click to select a period.'#13#10 +
    'Ctrl + click to clear the query.';

type

  { TfInspector }

  TfInspector = class(TForm)
    pnlContent: TPanel;
    pnlContentBorderBottom: TPanel;
    pnlContentBorderLeft: TPanel;
    pnlContentBorderRight: TPanel;
    pnlContentFrame: TPanel;
    pnlContentInner: TPanel;
    pnlContentScrollBox: TPanel;
    pnlOperations: TPanel;
    pnlOperationsScrollBox: TPanel;
    ActionList: TActionList;
    acToggleInterface: TAction;
    pnlHeader: TPanel;
    pnlSession: TPanel;
    lSession: TLabel;
    pm: TPopupMenu;
    acQuery: TAction;
    acDerive: TAction;
    acExperiment: TAction;
    acShowTemporalAnnotations: TAction;
    acRedrawDuringTransactions: TAction;
    Query1: TMenuItem;
    Derive1: TMenuItem;
    Experiment1: TMenuItem;
    N1: TMenuItem;
    Showimplicitannotations1: TMenuItem;
    Redrawduringtransactions1: TMenuItem;
    btnPM: TSpeedButton;
    cbClaim: TComboBox;
    pnlSessionBorder: TPanel;
    TabControl: TTabControl;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure pnlOperationsScrollBoxResize(Sender: TObject);
    procedure acToggleInterfaceExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acQueryExecute(Sender: TObject);
    procedure acDeriveExecute(Sender: TObject);
    procedure acExperimentExecute(Sender: TObject);
    procedure acShowTemporalAnnotationsExecute(Sender: TObject);
    procedure acRedrawDuringTransactionsExecute(Sender: TObject);
    procedure btnPMClick(Sender: TObject);
    procedure cbClaimChange(Sender: TObject);
    procedure pnlContentScrollBoxResize(Sender: TObject);
    procedure TabControlChange(Sender: TObject);
    procedure tabsetChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);

  private
    { Private declarations }
    fClaimType: TcClaimType;
    fClaimSet: TcClaimSet;
    fSelection: TcSelection;

    fOperationScrollBox: TcdScrollBox;
    fOperationDiagram: TcdDiagram;
    fOperationStack: TcdOperationStack;

    fContentScrollBox: TcdScrollBox;
    fContentDiagram: TcdDiagram;
    fSourceClaimStack: TcdSourceClaimStack;
    fTemporalChart: TcdTemporalChart;
    fInfoPanel: TcdInfoPanel;

    fSessionOwned: boolean;
    fSession: TcSession;
    fEngine: TcEngine;
    fQuery: TcTemporalQuery;
    fMinimized: boolean;
    fHistorySettings: TfHistorySettings;

    fInitialized: boolean;
    fInRedraw: boolean;
    fRadioOnline: boolean;

    function GetClaimsForOperation(aOperation: TcOperation): TcStringObjectList<TcClaim>; { Not Owned, Not Sorted }
    procedure UpdateClaimCombo(aOperationID: integer);
    function GetSelectedClaim: TcClaim;

    procedure LazyRedraw;
    procedure Redraw;
    procedure Form_Minimize;
    procedure Form_Normal;

  public
    { Public declarations }
    procedure Init(aSession: TcSession; aClaimType: TcClaimType; aClaimSet: TcClaimSet; aMinimized, aRedrawDuringTransactions: boolean);

    procedure ListenToRadio (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);
    procedure ReleaseHistorySettingsForm;
    procedure ClearSelection;

    property ClaimType: TcClaimType read fClaimType;
    property ClaimSet: TcClaimSet read fClaimSet;
    property Selection: TcSelection read fSelection;

    property Session: TcSession read fSession;
    property Engine: TcEngine read fEngine;
    property Query: TcTemporalQuery read fQuery;
    property Minimized: boolean read fMinimized;

    property OperationScrollBox: TcdScrollBox read fOperationScrollBox;
    property OperationDiagram: TcdDiagram read fOperationDiagram;
    property OperationStack: TcdOperationStack read fOperationStack;

    property ContentScrollBox: TcdScrollBox read fContentScrollBox;
    property ContentDiagram: TcdDiagram read fContentDiagram;
    property SourceClaimStack: TcdSourceClaimStack read fSourceClaimStack;
    property TemporalChart: TcdTemporalChart read fTemporalChart;
    property InfoPanel: TcdInfoPanel read fInfoPanel;

  end;

var
  fInspector: TfInspector;

implementation

{$R *.lfm}

uses
  common.ErrorHandling,
  diagram.Lines,
  ace.Settings;


{ TfClaimSet }

procedure TfInspector.FormCreate(Sender: TObject);
  begin
  fInitialized := false;

  // -- Create custom components at runtime

  // Operations
  pnlOperationsScrollBox.Color := cInspectorCanvasColor;
  fOperationScrollBox := TcdScrollBox.Create(Self);
  with fOperationScrollBox do
    begin
    Left := 0; Top := 30;
    BorderStyle := bsNone;
    Align := alClient;
    Color := cInspectorCanvasColor;
    HorzScrollBar.Visible := false;
    VertScrollBar.Visible := false;
    end;
  fOperationScrollBox.Parent := pnlOperationsScrollBox;
  fOperationScrollBox.Width := pnlOperationsScrollBox.Width-4; // Patch. Align := alClient doesn't work.
  fOperationScrollBox.Height := pnlOperationsScrollBox.Height; // Patch
  fOperationDiagram := TcdDiagram.Create(OperationScrollBox, 'Default');

  // Content
  pnlContent.Color := cInspectorCanvasColor;
  pnlContentBorderLeft.Color := cInspectorCanvasColor;
  pnlContentBorderRight.Color := cInspectorCanvasColor;
  pnlContentBorderBottom.Color := cInspectorCanvasColor;
  pnlContentScrollBox.Color := cInspectorCanvasColor;
  fContentScrollBox := TcdScrollBox.Create(Self);
  with fContentScrollBox do
    begin
    Left := 0; Top := 30;
    BorderStyle := bsNone;
    Align := alClient;
    Color := cInspectorCanvasColor;
    HorzScrollBar.Visible := false;
    VertScrollBar.Visible := false;
    end;
  fContentScrollBox.Parent := pnlContentScrollBox;
  fContentScrollBox.Width := pnlContentScrollBox.Width-4; // Patch. Align := alClient doesn't work.
  fContentScrollBox.Height := pnlContentScrollBox.Height; // Patch
  fContentDiagram := TcdDiagram.Create(ContentScrollBox, 'Default');

  fSessionOwned := false;
  fQuery := nil;
  fSelection := nil;

  gRadioStation.AddRadio(Self, ListenToRadio);
  fRadioOnline := true;

  fMinimized := false;
  end;

procedure TfInspector.FormDestroy(Sender: TObject);
  begin
  OperationDiagram.Clear;
  ContentDiagram.Clear;

  if fRadioOnline
    then gRadioStation.RemoveRadio(Self);

  if Assigned(fQuery)
     then FreeAndNil(fQuery);

  FreeAndNil(fSelection);

  if fSessionOwned
     then begin
          if Assigned(fSession.Transaction)
             then fSession.Rollback;
          FreeAndNil(fSession);
          end;
  end;

procedure TfInspector.FormClose(Sender: TObject; var Action: TCloseAction);
  begin
  Action := caFree;
  end;

procedure TfInspector.Init(aSession: TcSession; aClaimType: TcClaimType; aClaimSet: TcClaimSet; aMinimized, aRedrawDuringTransactions: boolean);
  begin
  if Assigned(fSession) then EInt('TfInspector.Init', 'Already initialized');

  if not Assigned(aSession)
     then begin
          fSession := TcSession.Create(gSettings.Engine);
          fSessionOwned := true;
          end
     else fSession := aSession;

  pnlSession.ParentColor := false;
  pnlSession.Color := fSession.GetColor;

  fEngine := fSession.Engine;
  fClaimType := aClaimType;
  fClaimSet := aClaimSet;

  fQuery := TcTemporalQuery.Create(Session);
  fQuery.AssignClaimSet(aClaimSet);
  fSelection := TcSelection.Create(fSession.Engine);
  fInRedraw := false;
  fInitialized := true;

  acRedrawDuringTransactions.Checked := aRedrawDuringTransactions;
  if aMinimized
     then Form_Minimize
     else Form_Normal;
  end;

procedure TfInspector.Form_Minimize;
  begin
  Width := cMinimizedFormWidth;
  Height := cMinimizedFormHeight;
  pnlHeader.Visible := false;
  fMinimized := true;
  Redraw;
  end;

procedure TfInspector.Form_Normal;
  begin
  Width := cNormalFormWidth;
  Height := cNormalFormHeight;
  pnlHeader.Visible := true;
  fMinimized := false;
  Redraw;
  end;

procedure TfInspector.ListenToRadio (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);
  var
    lObject: TObject;
  begin
  if aRadioEvent = cRadioEvent_Offline
    then begin
         fRadioOnline := false;
         exit;
         end;

  lObject := TObject(aData);
  if (aRadioEvent = cRadioEvent_Session_ClaimSet_Changed)
     then begin
          if not Assigned(lObject) then exit;
          if lObject <> Session then exit; // Another session
          LazyRedraw;
          end
  else if (aRadioEvent = cRadioEvent_OperationStack_Operation_Selected)
     then begin
          if lObject <> fOperationStack then exit; // Another diagram
          UpdateClaimCombo(cNoID); // Invokes a redraw
          end
  else if (aRadioEvent = cRadioEvent_SourceClaimStack_SourceClaim_Selected)
     then begin
          if lObject <> fSourceClaimStack then exit; // Another diagram
          LazyRedraw;
          end
  else if (aRadioEvent = cRadioEvent_TemporalChart_QueryCriteria_Changed)
     then begin
          if lObject <> fTemporalChart then exit; // Another diagram
          LazyRedraw;
          end
  else if (aRadioEvent = cRadioEvent_TemporalChart_Claim_Selected)
     then begin
          if lObject <> fTemporalChart then exit; // Another diagram
          LazyRedraw;
          end
  else if (aRadioEvent = cRadioEvent_Transaction_Start)
     then begin
          if not Assigned(lObject) then exit;
          if lObject <> Session then exit; // Another session
          // Starting a transaction (for reading) might change the snapshot to Stable
          // This might change the claims that are visible
          LazyRedraw
          end
  else if (aRadioEvent = cRadioEvent_Transaction_AfterEnd)
     then begin
          // Update for all transactions
          Redraw;
          end
  else if (aRadioEvent = cRadioEvent_SystemTime_Changed)
     then LazyRedraw;
  end;

procedure TfInspector.UpdateClaimCombo(aOperationID: integer);
  var
    lOperationID: integer;
    lElement: TcElement;
    lOperation: TcOperation;
    lSelectedClaimSet: TcClaimSet;
    lSelectedType: TcClaimType;
    lClaimType: TcClaimType;
    lClaimSet: TcClaimSet;
    lClaim: TcClaim;
    lStr, lStr1, lStr2: string;
    lList: TcStringObjectList<TcClaim>;
    i, lItemIndex: integer;
  begin
  // Remember Current Value
  lSelectedClaimSet := nil;
  lSelectedType := nil;
  if cbClaim.ItemIndex>=0
     then begin
          lClaim := TcClaim(cbClaim.Items.Objects[cbClaim.ItemIndex]);
          lSelectedClaimSet := lClaim.ClaimSet;
          lSelectedType := lClaim.ClaimType;
          end;

  if not Assigned(lSelectedClaimSet) then lSelectedClaimSet := ClaimSet;
  if not Assigned(lSelectedType) then lSelectedType := ClaimType;

  // Clear Combo
  cbClaim.Items.Clear;
  tabControl.Tabs.Clear;

  // Translate Selected ID to operation
  if aOperationID <> cNoID
     then lOperationID := aOperationID
     else lOperationID := Selection.ID;

  if lOperationID = cNoID
     then begin
          // There is no operation
          // Select the first ClaimSet of the specified ClaimType?
          if not Assigned(ClaimSet)
             then begin
                  if not Assigned(ClaimType) then exit; // Nothing to display but operations
                  if not (ClaimType is TcClaimType) then exit; // Not (yet?) supported
                  lClaimType := (ClaimType as TcClaimType);
                  if lClaimType.ClaimSetList.Count=0 then exit;
                  lClaimSet := lClaimType.ClaimSetList[0];
                  if lClaimSet.SourceClaims.Count=0 then exit;
                  lClaim := lClaimSet.SourceClaims[0];
                  lStr := Format('%s (%s: %d)', [lClaimType.Name, 'Claim', lClaim.ID]);
                  cbClaim.AddItem(lStr, lClaim);
                  tabControl.Tabs.AddObject(lClaimType.Name, lClaim);
                  end;
          end
     else begin
          lElement := Engine.Elements.Find(lOperationID);
          if not Assigned(lElement) then EInt('TfInspector.UpdateClaimCombo', 'Expected selected item is an operation');
          if not (lElement is TcOperation) then EInt('TfInspector.UpdateClaimCombo', 'Expected selected item is an operation');
          lOperation := lElement as TcOperation;

          // Determine claims added by operation
          lList := GetClaimsForOperation(lOperation);
          try
            // Rebuilt combo
            for i := 0 to lList.Count-1 do
              begin
              cbClaim.AddItem(lList[i], lList.Objects[i]);
              SplitString(lList[i], '(', lStr1, lStr2);
              tabControl.Tabs.AddObject(lStr1, lList.Objects[i]);
              end;
          finally
            lList.Free;
            end;
          end;

  // Selected previously selected item
  lItemIndex := -1;
  if Assigned(lSelectedClaimSet)
     then begin
          for i := 0 to cbClaim.Items.Count-1 do
            begin
            lClaim := TcClaim(cbClaim.Items.Objects[i]);
            if lClaim.ClaimSet = lSelectedClaimSet
               then begin
                    lItemIndex := i;
                    break;
                    end;
            end;
          end;

  if lItemIndex < 0 // Found?
     then begin // Try to select by type
          for i := 0 to cbClaim.Items.Count-1 do
            begin
            lClaim := TcClaim(cbClaim.Items.Objects[i]);
            if lClaim.ClaimType = lSelectedType
               then begin
                    lItemIndex := i;
                    break;
                    end;
            end;
          end;

  if lItemIndex < 0 // Found?
     then begin
          if cbClaim.Items.Count>0
             then lItemIndex := 0; // Default = First item
          end;

  if tabControl.Tabs.Count=0
     then tabControl.Hide
     else begin
          tabControl.Show;
          tabControl.TabIndex := lItemIndex;
          end;

  if cbClaim.ItemIndex <> lItemIndex
     then begin
          cbClaim.ItemIndex := lItemIndex;
          end;
  Redraw;
  end;

function TfInspector.GetSelectedClaim: TcClaim;
  begin
  if (cbClaim.Items.Count=0) and (Selection.ID = cNoID)
     then begin
          if Assigned(fOperationStack)
             then UpdateClaimCombo(fOperationStack.GetOldestOperationID)
          else if Engine.Operations.Count>0
             then UpdateClaimCombo(Engine.Operations[0].ID)
             else UpdateClaimCombo(cNoID);
          end;

  if (cbClaim.ItemIndex>=0) and (cbClaim.ItemIndex < cbClaim.Items.Count)
     then result := TcClaim(cbClaim.Items.Objects[cbClaim.ItemIndex])
  else if cbClaim.Items.Count>0
     then result := TcClaim(cbClaim.Items.Objects[0])
     else result := nil;
  end;

function TfInspector.GetClaimsForOperation(aOperation: TcOperation): TcStringObjectList<TcClaim>;
  var
    i: integer;
    lClaim: TcClaim;
    lClaimType: TcClaimType;
    lTypeStr, lStr: string;
  begin
  result := TcStringObjectList<TcClaim>.Create(false {not owned}, false {not sorted});

  for i := 0 to aOperation.Claims.Count-1 do
    begin
    lClaim := aOperation.Claims.Objects[i];
    if not Assigned(lClaim) then EInt('TcdInfoPanel.Init', 'Operation has destroyed Claim');

    lClaimType := lClaim.ClaimType;
    if not Assigned(lClaimType) then EInt('TcdInfoPanel.Init', 'Claim without ClaimType');

    if lClaim is TcClaim
       then lTypeStr := 'Claim'
       else lTypeStr := 'Annotation';

    lStr := Format('%s (%s: %d)', [lClaimType.Name, lTypeStr, lClaim.ID]);
    result.Add(lStr, lClaim);
    end;
  end;

procedure TfInspector.LazyRedraw;
  begin
  if Assigned(fSession.Transaction) and (fSession.Transaction.EndTime = cEndOfTime) and not acRedrawDuringTransactions.Checked
     then exit;
  Redraw;
  end;

procedure TfInspector.Redraw;
  var
    lLeft, lHeight, lMaxHeight: integer;
    lClaim: TcClaim;
    lOperationLines: TcdOperationLines;
    lContentLines: TcdContentLines;
  begin
  if not fInitialized then exit;
  if fInRedraw then exit;

  fInRedraw := true;
//  OperationDiagram.BeginChange;
  try
    // Header: Session info
    lSession.Caption := Session.GetInfoStr;

    // Clear Diagrams
    OperationDiagram.Clear;
    ContentDiagram.Clear;
    fInfoPanel := nil;
    fOperationStack := nil;
    fSourceClaimStack := nil;
    fTemporalChart := nil;

    // Lane 1: Operations
    if not Minimized and (Engine.Operations.Count>0)
       then begin
            // Operation Stack
            fOperationStack := TcdOperationStack.Create(OperationDiagram, Session, Selection);
            OperationDiagram.Add(fOperationStack, 16, 0);
            OperationStack.ResetBounds;

            // Operation Info
            if Assigned(Selection.Operation) and (Selection.Operation.Claims.Count>0)
               then begin
                    fInfoPanel := TcdInfoPanel.Create(OperationDiagram, Session);
                    InfoPanel.Init(Selection.Operation);
                    OperationDiagram.Add(fInfoPanel, fOperationStack.Width + 42, 20);
                    InfoPanel.ResetBounds;
                    end;
            end;

    // Adjust height of lane 1
    lMaxHeight := 0;
    if Assigned(fOperationStack) then lMaxHeight := fOperationStack.Top + fOperationStack.Height + 4;
    if Assigned(fInfoPanel)
       then begin
            lHeight := fInfoPanel.Top + fInfoPanel.Height + 20;
            if lHeight > lMaxHeight then lMaxHeight := lHeight;
            end;
    if lMaxHeight = 0
       then pnlOperations.Visible := false
       else begin
            pnlOperations.Height := lMaxHeight + 20;
            pnlOperations.Visible := true;
            end;

    lOperationLines := TcdOperationLines.Create(OperationDiagram);
    lOperationLines.Init(fOperationStack, fInfoPanel);
    OperationDiagram.Add(lOperationLines, 0,0);
    lOperationLines.ResetBounds;

    // Lane 2: Claims

    // Determine what to display
    lClaim := GetSelectedClaim;
    if not Assigned(lClaim) then exit;

    if lClaim is TcClaim
       then fClaimSet := (lClaim as TcClaim).ClaimSet
       else fClaimSet := nil;
    if fQuery.ClaimSet <> ClaimSet
       then fQuery.AssignClaimSet(ClaimSet);
    fQuery.Execute;

    if fMinimized
       then Caption := cbClaim.Text
       else Caption := 'Inspector';

    lLeft := 0;
    if not Minimized and Assigned(ClaimSet)
       then begin
            // Display Source Claim Stack
            fSourceClaimStack := TcdSourceClaimStack.Create(ContentDiagram, Session, ClaimSet, Selection);
            fSourceClaimStack.ShowTemporalAnnotations := acShowTemporalAnnotations.Checked;
            ContentDiagram.Add(fSourceClaimStack, 0, 0);
            SourceClaimStack.ResetBounds;
            lLeft := fSourceClaimStack.Width;
            end;

    // Display Temporal Chart
    fTemporalChart := TcdTemporalChart.Create(ContentDiagram, Session, Selection);
    if Assigned(ClaimSet)
       then fTemporalChart.Init(ClaimSet, Query)
    else if (lClaim is TcAnnotation)
       then fTemporalChart.Init(lClaim as TcAnnotation)
       else EInt('TfInspector.Redraw', 'Can only handle TcClaim and TcAnnotation');
    fTemporalChart.ShowTemporalAnnotations := acShowTemporalAnnotations.Checked;
    ContentDiagram.Add(fTemporalChart, lLeft, 0);
    TemporalChart.ResetBounds;

    // Determine top of Lane 3
    lMaxHeight := fTemporalChart.Top + fTemporalChart.Height + cLaneMargin;

    // Lane 3: Claim, Annotation or Query info
    if Assigned(Selection.SourceClaim)
       then begin
            fInfoPanel := TcdInfoPanel.Create(ContentDiagram, Session);
            fInfoPanel.ShowTemporalAnnotations := acShowTemporalAnnotations.Checked;
            InfoPanel.Init(Selection.SourceClaim, fTemporalChart.Width - 100); // Use with of temporal diagram
            ContentDiagram.Add(fInfoPanel, fSourceClaimStack.Left + 20, lMaxHeight);
            InfoPanel.ResetBounds;
            end;

    if Query.Active
       then begin
            fInfoPanel := TcdInfoPanel.Create(ContentDiagram, Session);
            InfoPanel.Init(Query, fTemporalChart.Width - 100);
            ContentDiagram.Add(fInfoPanel, fTemporalChart.Left + 80, lMaxHeight);
            InfoPanel.ResetBounds;
            end;

    if Assigned(Selection.DerivedClaim)
       then begin
            fInfoPanel := TcdInfoPanel.Create(ContentDiagram, Session);
            fInfoPanel.ShowTemporalAnnotations := acShowTemporalAnnotations.Checked;
            InfoPanel.Init(Selection.DerivedClaim, fTemporalChart.Width - 100);
            ContentDiagram.Add(fInfoPanel, fTemporalChart.Left + 80, lMaxHeight);
            InfoPanel.ResetBounds;
            end;

    lContentLines := TcdContentLines.Create(ContentDiagram);
    lContentLines.Init(fSourceClaimStack, fTemporalChart, fInfoPanel);
    ContentDiagram.Add(lContentLines, 0,0);
    lContentLines.ResetBounds;

    // Update history settings
    if Assigned(fHistorySettings) and Assigned(ClaimSet)
       then fHistorySettings.UpdateSettings;

  finally
//    OperationDiagram.EndChange;
    fInRedraw := false;
    end;
  end;

procedure TfInspector.ReleaseHistorySettingsForm;
  begin
  fHistorySettings := nil;
  end;

procedure TfInspector.ClearSelection;
  begin
  Selection.CLear;
  end;


// -- Interface handlers

procedure TfInspector.acDeriveExecute(Sender: TObject);
  begin
  ClearSelection;
  if not Assigned(ClaimSet) then exit;
  
  fSession.StartTransaction(true {implicit}, false {not readonly}, false {not stable});
  try
    ClaimSet.ClaimType.ClaimSetList.DeriveClaims(fSession);
  finally
    fSession.Commit;
    end;
  end;

procedure TfInspector.acExperimentExecute(Sender: TObject);
  begin
  if Assigned(fHistorySettings)
     then fHistorySettings.Show
     else begin
          fHistorySettings := TfHistorySettings.Create(Application);
          fHistorySettings.Init(Self, Session);
          fHistorySettings.UpdateSettings;
          end;
  end;

procedure TfInspector.acQueryExecute(Sender: TObject);
  begin
  ShowMessage(cQueryHint);
  end;

procedure TfInspector.acRedrawDuringTransactionsExecute(Sender: TObject);
  begin
  acRedrawDuringTransactions.Checked := not acRedrawDuringTransactions.Checked;
  end;

procedure TfInspector.acShowTemporalAnnotationsExecute(Sender: TObject);
  begin
  acShowTemporalAnnotations.Checked := not acShowTemporalAnnotations.Checked;
  Redraw;
  end;

procedure TfInspector.acToggleInterfaceExecute(Sender: TObject);
  begin
  if fMinimized
     then Form_Normal
     else Form_Minimize;
  end;

procedure TfInspector.btnPMClick(Sender: TObject);
  var
    lPoint: TPoint;
  begin
  lPoint := btnPM.ClientToScreen(point(0, btnPM.Height));
  pm.Popup(lPoint.X, lPoint.Y);
  end;

procedure TfInspector.cbClaimChange(Sender: TObject);
  begin
  Redraw;
  end;

procedure TfInspector.TabControlChange(Sender: TObject);
  begin
  cbClaim.ItemIndex := TabControl.TabIndex;
  cbClaimChange(Self);
  end;

procedure TfInspector.tabsetChange(Sender: TObject; NewTab: Integer; var AllowChange: Boolean);
  begin
  end;

procedure TfInspector.pnlContentScrollBoxResize(Sender: TObject);
  begin
  // Patch. Align := alCLient doesn't work.
  fContentScrollBox.Width := pnlContentScrollBox.Width-4;
  fContentScrollBox.Height := pnlContentScrollBox.Height;
  if fInitialized and Assigned(fTemporalChart) then fTemporalChart.ResetBounds;
  if fInitialized and Assigned(fInfoPanel) then fInfoPanel.ResetBounds;
  end;

procedure TfInspector.pnlOperationsScrollBoxResize(Sender: TObject);
  begin
  // Patch. Align := alCLient doesn't work.
  fOperationScrollBox.Width := pnlOperationsScrollBox.Width-4;
  fOperationScrollBox.Height := pnlOperationsScrollBox.Height;
  if fInitialized and Assigned(fInfoPanel) then fInfoPanel.ResetBounds;
  end;

end.
