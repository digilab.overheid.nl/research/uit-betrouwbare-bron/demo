// -----------------------------------------------------------------------------
//
// ace.Forms.HistorySettings
//
// -----------------------------------------------------------------------------
//
// Form to change Temporal Settings of a ClaimType
// (to experiment with derivation of temporal claims from source claims).
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Forms.HistorySettings;

{$MODE Delphi}

interface

uses
  SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
  ace.Core.Engine;

type
  TfHistorySettings = class(TForm)
    pnlHistorySettings: TPanel;
    pnlHistorySettingsControls: TPanel;
    cbAllowChange: TCheckBox;
    cbTimelineInterpretation: TCheckBox;
    cbRecordTransactionTime: TCheckBox;
    cbKeepExpired: TCheckBox;
    cbRecordValidTime: TCheckBox;
    cbKeepEnded: TCheckBox;
    btnChangeSettingsAndDeriveClaimSets: TButton;
    mExplanation: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnChangeSettingsAndDeriveClaimSetsClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    fForm: TForm;
    fSession: TcSession;
    fClaimType: TcClaimType;

    property ClaimType: TcClaimType read fClaimType;

  public
    { Public declarations }
    procedure Init(aInspector: TForm; aSession: TcSession);
    procedure UpdateSettings;
  end;

implementation

{$R *.lfm}

uses
  ace.Forms.Inspector;

procedure TfHistorySettings.FormCreate(Sender: TObject);
  begin
  mExplanation.Clear;
  mExplanation.Lines.Add(
    'The history derivation settings can be changed to experiment. '+
    'They will be changed for the ClaimType. '+
    'After changing all derived Claims for all ClaimSets will be deleted '+
    'and derived again from the source Claims. '+
    'Please note that not all possible combinations make sense and '+
    'rederiving all ClaimSets may take some time.'
  );
  end;

procedure TfHistorySettings.FormDestroy(Sender: TObject);
  begin
  (fForm as TfInspector).ReleaseHistorySettingsForm;
  end;

procedure TfHistorySettings.Init(aInspector: TForm; aSession: TcSession);
  var
    lClaimSet: TcClaimset;
  begin
  fForm := aInspector;
  fSession := aSession;
  lClaimSet := (aInspector as TfInspector).ClaimSet;
  if Assigned(lClaimSet)
     then fClaimType := lClaimSet.ClaimType
     else fClaimType := nil;
  end;

procedure TfHistorySettings.FormClose(Sender: TObject; var Action: TCloseAction);
  begin
  Action := caFree;
  end;

procedure TfHistorySettings.UpdateSettings;
  var
    lAllowGrayed: boolean;
  begin
  lAllowGrayed := not Assigned(ClaimType);

  cbAllowChange.AllowGrayed := lAllowGrayed;
  cbTimelineInterpretation.AllowGrayed := lAllowGrayed;
  cbRecordTransactionTime.AllowGrayed := lAllowGrayed;
  cbKeepExpired.AllowGrayed := lAllowGrayed;
  cbRecordValidTime.AllowGrayed := lAllowGrayed;
  cbKeepEnded.AllowGrayed := lAllowGrayed;

  if Assigned(ClaimType)
     then begin
          cbAllowChange.Checked := ClaimType.AllowChange;
          cbTimelineInterpretation.Checked := ClaimType.TimelineInterpretation;
          cbRecordTransactionTime.Checked := ClaimType.RecordTransactionTime;
          cbKeepExpired.Checked := ClaimType.KeepExpired;
          cbRecordValidTime.Checked := ClaimType.RecordValidTime;
          cbKeepEnded.Checked := ClaimType.KeepEnded;

          btnChangeSettingsAndDeriveClaimSets.Enabled := true;
          end
     else begin
          cbAllowChange.State := cbGrayed;
          cbTimelineInterpretation.State := cbGrayed;
          cbRecordTransactionTime.State := cbGrayed;
          cbKeepExpired.State := cbGrayed;
          cbRecordValidTime.State := cbGrayed;
          cbKeepEnded.State := cbGrayed;

          btnChangeSettingsAndDeriveClaimSets.Enabled := false;
          end;
  end;

procedure TfHistorySettings.btnChangeSettingsAndDeriveClaimSetsClick(Sender: TObject);
  begin
  ClaimType.AllowChange := cbAllowChange.Checked;
  ClaimType.TimelineInterpretation := cbTimelineInterpretation.Checked;
  ClaimType.RecordTransactionTime := cbRecordTransactionTime.Checked;
  ClaimType.KeepExpired := cbKeepExpired.Checked;
  ClaimType.RecordValidTime := cbRecordValidTime.Checked;
  ClaimType.KeepEnded := cbKeepEnded.Checked;

  (fForm as TfInspector).ClearSelection;

  fSession.StartTransaction(true {implicit}, false {not readonly}, false {not stable});
  try
    ClaimType.ClaimSetList.DeriveClaims(fSession);
  finally
    fSession.Commit;
    end;
  end;

end.
