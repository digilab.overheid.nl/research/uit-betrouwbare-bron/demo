// -----------------------------------------------------------------------------
//
// ace.Forms.GraphEditor
//
// -----------------------------------------------------------------------------
//
// Form to experiment with the syntax of operations/predefined queries derived
// by walking the ClaimType graph from a given start ClaimType.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Forms.GraphEditor;

interface

uses
  SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
  ace.Core.Engine, ComCtrls;//, JvExComCtrls, JvComCtrls, JvCheckTreeView;

//const
//  cDefaultSelected = false; // Determines if new nodes are selected by default or not

type

  { TfGraphEditor }

  TfGraphEditor = class(TForm)
    pnlMain: TPanel;
    pnlMainControls: TPanel;
    lClaimType: TLabel;
    cbClaimTypes: TComboBox;
    btnAdd: TButton;
    TreeView: TTreeView;
    pnlNodeControls: TPanel;
    btnExpand: TButton;
    btnCollapse: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnAddClick(Sender: TObject);
    procedure btnExpandClick(Sender: TObject);
    procedure btnCollapseClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    { Private declarations }
    fEngine: TcEngine;

    function AddClaimType(aNode: TTreeNode; aClaimType: TcClaimType): TTreeNode;
    function DeriveListName(aBaseClaimType: TcClaimType; aRefRole: TcRole; aRefClaimType: TcClaimType): string;

  public
    { Public declarations }
    property Engine: TcEngine read fEngine;
  end;

var
  fGraphEditor: TfGraphEditor;

implementation

{$R *.lfm}

uses
  common.ErrorHandling,
  common.Utils, ace.Settings;

{ TfGraphEditor }

procedure TfGraphEditor.FormCreate(Sender: TObject);
  var
    t: integer;
    lType: TcType;
  begin
  fEngine := gSettings.Engine;

  for t := 0 to Engine.ConceptualTypes.Count-1 do
    begin
    lType := Engine.ConceptualTypes[t];
    if lType is TcClaimType
       then cbClaimTypes.Items.Add(lType.Name);
    end;
  end;

procedure TfGraphEditor.FormDestroy(Sender: TObject);
  begin
  fGraphEditor := nil;
  end;

procedure TfGraphEditor.FormClose(Sender: TObject; var Action: TCloseAction);
  begin
  Action := caFree;
  end;

function TfGraphEditor.DeriveListName(aBaseClaimType: TcClaimType; aRefRole: TcRole; aRefClaimType: TcClaimType): string;
  begin
  try
    result := aRefRole.Name;

    // Often referencing Roles are named after the ClaimType they're referencing
    if CompareText(aBaseClaimType.Name, aRefRole.Name)<>0
       then exit; // Nope, Referencing Role has a different name. Use it.

    // Same name
    result := aRefClaimType.Name;

    // Name could be part of the name of the referencing ClaimType
    result := RemoveNameFromIdentifier(result, aRefRole.Name);

  finally
    if result = ''
       then result := aRefClaimType.Name; // Fail safe
    end;
  end;

procedure TfGraphEditor.btnAddClick(Sender: TObject);
  var
    lClaimType: TcClaimType;
    lNode: TTreeNode;
  begin
  if cbClaimTypes.ItemIndex < 0
     then exit;

  lClaimType := Engine.ConceptualTypes.FindClaimType(cbClaimTypes.Items[cbClaimTypes.ItemIndex]);
  if not Assigned(lClaimType)
     then EInt('TfGraphEditor.btnAddClick', 'Cannot find selected ClaimType');

  lNode := AddClaimType(nil, lClaimType);
  lNode.Expand(true);
  end;

procedure TfGraphEditor.btnExpandClick(Sender: TObject);
  var
    lNode: TTreeNode;
    lType: TcType;
    lClaimType: TcClaimType;
  begin
  if TreeView.SelectionCount = 0
     then begin
          ShowMessage('Select a node to expand.');
          exit;
          end;

  if TreeView.SelectionCount > 1
     then begin
          ShowMessage('Cannot expand multiple nodes at once.');
          exit;
          end;

  lNode := Treeview.Selections[0];
  lType := TcType(lNode.Data);
  if lType is TcLabelType
     then begin
          ShowMessage('Cannot expand a LabelType.');
          exit;
          end;

  lClaimType := lType as TcClaimType;
  AddClaimType(lNode, lClaimType);
  lNode.Expand(true);
  end;

procedure TfGraphEditor.btnCollapseClick(Sender: TObject);
  var
    lNode: TTreeNode;
    lType: TcType;
  begin
  if TreeView.SelectionCount = 0
     then begin
          ShowMessage('Select a node to collapse.');
          exit;
          end;

  if TreeView.SelectionCount > 1
     then begin
          ShowMessage('Cannot collapse multiple nodes at once.');
          exit;
          end;

  lNode := Treeview.Selections[0];
  lType := TcType(lNode.Data);
  if lType is TcLabelType
     then begin
          ShowMessage('Cannot collapse a LabelType.');
          exit;
          end;

  lNode.DeleteChildren;
  end;

function TfGraphEditor.AddClaimType(aNode: TTreeNode; aClaimType: TcClaimType): TTreeNode;
  var
    r, ct: integer;
    lRole: TcRole;
    lType: TcType;
    lLabelType: TcLabelType;
    lReferencedClaimType: TcClaimType;
    lReferencingClaimType: TcClaimType;
    lReferencingRole: TcRole;
    lStr, lName: string;
    lGroupable: boolean;
    lNode{, lSubNode}: TTreeNode;
  begin
  if not Assigned(aNode)
     then begin
          aNode := TreeView.Items.AddObject(nil, aClaimType.Name, aClaimType); // Add ClaimType
          TreeView.Select(aNode);
          //TreeView.Checked[aNode] := cDefaultSelected;
          end;
  result := aNode;

  // Add the roles of the ClaimType
  for r := 0 to aClaimType.Roles.Count-1 do
    begin
    lRole := aClaimType.Roles[r];

    if lRole.Type_ is TcLabelType
       then begin
            lLabelType := lRole.Type_ as TcLabelType;
            lStr := Format('%s: %s (%s)', [lRole.Name, lLabelType.Name, lLabelType.DataType.Name]);
            {lNode := }TreeView.Items.AddChildObject(aNode, lStr, lLabelType);
            //TreeView.Checked[lNode] := cDefaultSelected;
            end
       else begin
            lStr := Format('%s: %s (object)', [lRole.Name, lRole.Type_.Name]);
            lReferencedClaimType := lRole.Type_ as TcClaimType;
            {lNode := }TreeView.Items.AddChildObject(aNode, lStr, lReferencedClaimType);
            //TreeView.Checked[lNode] := cDefaultSelected;
            end;
    end;

  // Search for ClaimTypes with Roles referencing the main ClaimType
  for ct := 0 to Engine.ConceptualTypes.Count-1 do
    begin
    lType := Engine.ConceptualTypes[ct];
    if not (lType is TcClaimType) then continue; // Only consider ClaimTypes

    // Does this ClaimType reference the main ClaimType?
    lReferencingClaimType := lType as TcClaimType;
    lReferencingRole := nil;
    for r := 0 to lReferencingClaimType.Roles.Count-1 do
      begin
      lRole := lReferencingClaimType.Roles[r];
      if lRole.Type_ = aClaimType
         then begin
              lReferencingRole := lRole;
              break;
              end;
      end;

    if Assigned(lReferencingRole)
       then begin
            // ClaimType references the main ClaimType

            // If the referencing Role is the only Role in the identity of the
            // Referencing ClaimType, the Role is groupable. This means there
            // can only be one instance of the Referencing ClaimType per
            // instance of the main ClaimType.
            // If not there could be multiple.
            lGroupable := false;
            if (lReferencingClaimType.Identity.Count=1) and
               (lReferencingClaimType.Identity[0] = lReferencingRole)
               then lGroupable := true;

            if lGroupable
               then begin
                    // Group Referencing ClaimType
                    for r := 0 to lReferencingClaimType.Roles.Count-1 do
                      begin
                      lRole := lReferencingClaimType.Roles[r];
                      if lRole = lReferencingRole then continue;

                      if lRole.Type_ is TcLabelType
                         then begin
                              lLabelType := lRole.Type_ as TcLabelType;
                              lStr := Format('%s: %s (%s)', [lRole.Name, lLabelType.Name, lLabelType.DataType.Name]);
                              {lNode := }TreeView.Items.AddChildObject(aNode, lStr, lLabelType);
                              //TreeView.Checked[lNode] := cDefaultSelected;
                              end
                         else begin
                              lStr := Format('%s: %s (object)', [lRole.Name, lRole.Type_.Name]);
                              lReferencedClaimType := lRole.Type_ as TcClaimType;
                              {lNode := }TreeView.Items.AddChildObject(aNode, lStr, lReferencedClaimType);
                              //TreeView.Checked[lNode] := cDefaultSelected;
                              end;
                      end;
                    end
               else begin
                    // There could be multiple ClaimTypes per main ClaimType
                    lName := DeriveListName(aClaimType, lReferencingRole, lReferencingClaimType);
                    lStr := Format('%s_List : array of %s', [lName, lReferencingClaimType.Name]);
                    lNode := TreeView.Items.AddChildObject(aNode, lStr, lReferencingClaimType);
                    //TreeView.Checked[lNode] := cDefaultSelected;

                    // Add Roles
                    for r := 0 to lReferencingClaimType.Roles.Count-1 do
                      begin
                      lRole := lReferencingClaimType.Roles[r];
                      if lRole = lReferencingRole then continue;

                      if lRole.Type_ is TcLabelType
                         then begin
                              lLabelType := lRole.Type_ as TcLabelType;
                              lStr := Format('%s: %s (%s)', [lRole.Name, lLabelType.Name, lLabelType.DataType.Name]);
                              {lSubNode := }TreeView.Items.AddChildObject(lNode, lStr, lLabelType);
                              //TreeView.Checked[lSubNode] := cDefaultSelected;
                              end
                         else begin
                              lStr := Format('%s: %s (object)', [lRole.Name, lRole.Type_.Name]);
                              lReferencedClaimType := lRole.Type_ as TcClaimType;
                              {lSubNode := }TreeView.Items.AddChildObject(lNode, lStr, lReferencedClaimType);
                              //TreeView.Checked[lSubNode] := cDefaultSelected;
                              end;
                      end;
                    end;
            end;
    end;

  end;

end.
