// -----------------------------------------------------------------------------
//
// ace.forms.Console
//
// -----------------------------------------------------------------------------
//
// Console used to send commands to the engine.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Forms.Console;

{$MODE Delphi}

interface

uses
  SysUtils, Variants, Classes, ActnList, Generics.Collections,
  Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls,
  common.RadioStation,
  ace.Commands.Dispatcher, ace.Core.Engine,
  diagram.Core, SynEdit, SynEditHighlighter,
  SynHighlighterAny, SynCompletion;

type

  { TfConsole }

  TfConsole = class(TForm)
    pnlMain: TPanel;
    pnlInput: TPanel;
    pnlOutput: TPanel;
    pnlInputHeader: TPanel;
    lInput: TLabel;
    pnlInputMemo: TPanel;
    pnlOutputHeader: TPanel;
    lOutput: TLabel;
    pnlOutputMemo: TPanel;
    mOutput: TMemo;
    pnlInputControls: TPanel;
    btnExecute: TButton;
    btnLoad: TButton;
    pnlOutputControls: TPanel;
    btnClearOutput: TButton;
    ActionList: TActionList;
    actClearInput: TAction;
    actClearOutput: TAction;
    pnlInputControlsRight: TPanel;
    btnClearInput: TButton;
    actLoad: TAction;
    actExecute: TAction;
    pnlOutputControlsRight: TPanel;
    actToggleInterface: TAction;
    actNext: TAction;
    actPrevious: TAction;
    pnlSession: TPanel;
    lSession: TLabel;
    btnPrevious: TButton;
    btnNext: TButton;
    mInput: TSynEdit;
    CompletionProposal: TSynCompletion;
    pnlSessionBorder: TPanel;
    splitterInputOutput: TSplitter;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure actClearInputExecute(Sender: TObject);
    procedure actClearOutputExecute(Sender: TObject);
    procedure actLoadExecute(Sender: TObject);
    procedure actExecuteExecute(Sender: TObject);
    procedure actToggleInterfaceExecute(Sender: TObject);
    procedure actNextExecute(Sender: TObject);
    procedure actPreviousExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
    fEngine: TcEngine;
    fSession: TcSession;
    fRadioOnline: boolean;
    fInputHistory: TList<TStringlist>; // Owned
    fInputHistoryIndex: integer;

    // Interface
    procedure ClearInput(aLineNr: integer = 0); // 0 = All lines
    procedure WriteOutput(aLine: string; const aArgs: array of const); overload;
    procedure WriteOutput(aLine: string); overload;
    procedure ClearOutput;
    procedure HideInterface;
    procedure ShowInterface;
    procedure ToggleInterface;
    procedure UpdateSessionInfo;

    // History
    procedure RememberInput(aCurrentLineNr: integer);
    procedure NextInput;
    procedure PreviousInput;

    // Script execution
    procedure HandleException(aMessage: string; aLineNr: integer);
    procedure ExecuteCommand(aCommand: TStringlist; aCurrentLineNr: Integer; var aContinue: boolean);
    procedure ExecuteScript;

    // Event handling
    procedure ListenToRadio(aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);

    property InputHistory: TList<TStringlist> read fInputHistory;
    property InputHistoryIndex: integer read fInputHistoryIndex write fInputHistoryIndex;

  public
    { Public declarations }
    procedure Init(aEngine: TcEngine; aIsFirst: boolean);

    // Script handling
    procedure LoadScript(aStringlist: TStringlist); overload;
    procedure LoadScript(aFilename: string); overload;

    // Visual Focus
    procedure ShowVisualFocus;
    procedure HideVisualFocus;

    property Engine: TcEngine read fEngine;
    property Session: TcSession read fSession;
  end;

var
  // See initialization of the unit
  gIntroText: TStringlist;


implementation

{$R *.lfm}

uses
  common.ErrorHandling, common.Utils,
  ace.Settings, ace.Forms.Main, diagram.Settings;

// -- Initialisation and finalisation
procedure TfConsole.FormCreate(Sender: TObject);
  begin
  // Configure main form based on settings
  if not gSettings.ShowControls
    then HideInterface;

  // Editor
  with mInput do
    begin
    with Gutter do
      begin
      Color := clWebWhiteSmoke;
      Font.Color := clGray;
      end;
    Highlighter := fMain.EditorHighLighter;
    Gutter.Color := clBtnText;
    Font.Name := gSettings.FontName_Console;
    Font.Height := gSettings.FontHeight_Console;
    end;
  mOutput.Font.Name := gSettings.FontName_Console;
  mOutput.Font.Height := gSettings.FontHeight_Console;

  gCommandDispatcher.AddItemsToCompletionProposal(CompletionProposal);

  // Init input history
  fInputHistory := TList<TStringlist>.Create;
  fInputHistoryIndex := -1;

  // RadioStation
  gRadioStation.AddRadio(Self, ListenToRadio);
  fRadioOnline := true;
  end;

procedure TfConsole.Init(aEngine: TcEngine; aIsFirst: boolean);
  var
    i: integer;
  begin
  fEngine := aEngine;

  fSession := TcSession.Create(aEngine);
  pnlSession.ParentColor := false;
  pnlSession.Color := Session.GetColor;
  UpdateSessionInfo;

  if not aIsFirst
     then exit;

  // Intro text is stored for reuse in 'about' command
  for i := 0 to gIntroText.Count-1 do
    WriteOutput(gIntroText[i]);

  // Initial help text is only shown during startup
  WriteOutput('Press Ctrl+L to load an example script.');
  WriteOutput('Type ''help'' to see a list of commands.');
  WriteOutput('Type ''shortcuts'' and press Ctrl+Enter to see a list of shortcuts.');
  WriteOutput('');
  WriteOutput('--');
  WriteOutput('');
  end;

procedure TfConsole.FormDestroy(Sender: TObject);
  var
    i: integer;
  begin
  if fRadioOnline
     then gRadioStation.RemoveRadio(Self);

  fSession.Free;

  for i := 0 to InputHistory.Count-1 do
    InputHistory[i].Free;
  FreeAndNil(fInputHistory);

  fMain.ConsoleWillClose(Self);
  end;

procedure TfConsole.ShowVisualFocus;
  begin
  pnlInput.BorderWidth := 2;
  pnlInput.ParentBackground := false;
  pnlInput.Color := clMaroon;
  end;

procedure TfConsole.HideVisualFocus;
  begin
  pnlInput.BorderWidth := 0;
  pnlInput.ParentBackground := true;
  end;

procedure TfConsole.FormClose(Sender: TObject; var Action: TCloseAction);
  begin
  Action := caFree
  end;


// -- Interface

procedure TfConsole.ClearInput(aLineNr: integer = 0);
  var
    i: integer;
  begin
  if aLineNr = 0
    then mInput.Clear
    else begin
         for i := 0 to aLineNr-1 do
           mInput.Lines.Delete(0);
         end;
  mInput.SetFocus;
  end;

procedure TfConsole.WriteOutput(aLine: string; const aArgs: array of const);
  begin
  mOutput.Lines.Add(Format(aLine, aArgs));
  end;

procedure TfConsole.WriteOutput(aLine: string);
  begin
  WriteOutput(aLine, []);
  end;

procedure TfConsole.ClearOutput;
  begin
  mOutput.Clear;
  mInput.SetFocus;
  end;

procedure TfConsole.HideInterface;
  begin
  pnlInputHeader.Hide;
  pnlInputControls.Hide;
  pnlOutputHeader.Hide;
  pnlOutputControls.Hide;
  end;

procedure TfConsole.ShowInterface;
  begin
  pnlInputHeader.Show;
  pnlInputControls.Show;
  pnlOutputHeader.Show;
  pnlOutputControls.Show;
  end;

procedure TfConsole.ToggleInterface;
  begin
  if pnlInputControls.Visible
    then HideInterface
    else ShowInterface;
  end;

procedure TfConsole.UpdateSessionInfo;
  begin
  lSession.Caption := Session.GetInfoStr;
  end;


// -- History

procedure TfConsole.RememberInput(aCurrentLineNr: integer);
  var
    i: integer;
    lLine: string;
    lExecutedStatements,
    lUpcommingStatements,
    lOldUpcommingStatements: TStringlist;
  begin

  lExecutedStatements := TStringlist.Create;
  for i := 0 to aCurrentLineNr-1 do
    begin
    lLine := mInput.Lines[i];
    if not ((lExecutedStatements.Count=0) and (Trim(lLine)='')) // Skip empty lines at start
       then lExecutedStatements.Add(lLine);
    end;
  for i := 0 to aCurrentLineNr-1 do
    mInput.Lines.Delete(0);

  lUpcommingStatements := TStringlist.Create;
  for i := 0 to mInput.Lines.Count-1 do
    begin
    lLine := mInput.Lines[i];
    if not ((lUpcommingStatements.Count=0) and (Trim(lLine)='')) // Skip empty lines at start
       then lUpcommingStatements.Add(lLine);
    end;

  if InputHistory.Count=0
     then begin
          // First time, store both executed and upcomming
          InputHistory.Add(lExecutedStatements);
          InputHistory.Add(lUpcommingStatements);
          InputHistoryIndex := 1; // Previous will decrease and show the executed
          end
     else begin
          lOldUpcommingStatements := InputHistory[InputHistory.Count-1];
          InputHistory.Delete(InputHistory.Count-1);
          lOldUpcommingStatements.Free;
          InputHistory.Add(lExecutedStatements);
          InputHistory.Add(lUpcommingStatements);
          InputHistoryIndex := InputHistory.Count; // Previous will decrease and show the executed
          end;
  end;

procedure TfConsole.PreviousInput;
  var
    lStrs: TStringlist;
  begin
  if InputHistory.Count = 0 // Is there history?
    then exit;

  if InputHistoryIndex<=0 // Reached the start?
    then exit;

  InputHistoryIndex := InputHistoryIndex - 1;

  if InputHistoryIndex>=InputHistory.Count // Out of bounds?
    then InputHistoryIndex := InputHistory.Count-1; // Return last item

  lStrs := InputHistory[InputHistoryIndex];
  if Assigned(lStrs)
    then begin
         ClearInput;
         mInput.Lines.Assign(lStrs);
         end;
  end;

procedure TfConsole.NextInput;
  var
    lStrs: TStringlist;
  begin
  if InputHistory.Count = 0 // Is there history?
    then exit;

  if InputHistoryIndex>=InputHistory.Count-1 // Reached the end?
    then exit;

  InputHistoryIndex := InputHistoryIndex + 1;

  if InputHistoryIndex<0
    then InputHistoryIndex := 0; // Return the first item

  lStrs := InputHistory[InputHistoryIndex];
  if Assigned(lStrs)
    then begin
         ClearInput;
         mInput.Lines.Assign(lStrs);
         end;
  end;


// -- Script handling

procedure TfConsole.LoadScript(aStringlist: TStringlist);
  begin
  mInput.Lines.Assign(aStringlist);
  end;

procedure TfConsole.LoadScript(aFilename: string);
  var
    lRootFolder: string;
    lFilename: string;
  begin
  lFilename := aFilename;

  if (lFilename='')
     then begin
          if gSettings.ProjectName<>''
             then lRootFolder := gSettings.ProjectFolder
             else lRootFolder := gSettings.ProjectsFolder;

          if not PromptForOpenFileName('Load console script', 'txt', lRootFolder, '*.txt', lFilename)
             then exit;
          end;

  if ExtractFileExt(lFilename)=''
     then lFilename := lFilename + '.txt';

  if FileExists(lFilename)
     then begin
          mInput.Lines.LoadFromFile(lFilename);
          gSettings.ProjectLoaded(ExtractFileDir(lFilename));
          end
     else mOutput.Lines.Add(Format('Cannot find console script file: %s', [lFilename]));
  end;


// -- Script execution

procedure TfConsole.HandleException(aMessage: string; aLineNr: integer);
  var
    lMessage: string;
  begin
  lMessage := Format('Error: "%s" line %d.', [aMessage, aLineNr]);
  WriteOutput(lMessage);
  ShowMessage(lMessage);
  end;

procedure TfConsole.ExecuteCommand(aCommand: TStringlist; aCurrentLineNr: Integer; var aContinue: boolean);
  var
    i: integer;
    lResponse: TStringlist;
  begin
  aContinue := true;
  lResponse := TStringlist.Create;
  try
    // Handle wait command
    if (aCommand.Count=1) and (CompareText(aCommand[0], 'wait')=0)
      then begin
           aContinue := false;
           RememberInput(aCurrentLineNr);
           exit;
           end;

    // Handle all other commands
    gCommandDispatcher.Execute(Session, aCommand, lResponse);

    // Write response
    for i := 0 to lResponse.Count-1 do
      WriteOutput(lResponse[i]);

    // Force empty line after response
    if (lResponse.Count>0) and (Trim(lResponse[lResponse.Count-1])<>'')
      then WriteOutput('');

    Application.ProcessMessages; // Give cycles to display response before executing a new command

  finally
    lResponse.Free;
    end;
  end;

procedure TfConsole.ExecuteScript;
  var
    i: integer;
    inStatement: boolean;
    lLine: string;
    lStatement: TStringlist;
    lContinue: boolean;
  begin
  i := 0;
  inStatement := false;
  lStatement := TStringlist.Create;
  lContinue := true;
  try try
    while i < mInput.Lines.Count do
      begin
      lLine := mInput.Lines[i];

      // Remove indents for commands part of an operation
//      if (Session.InOperation) and (Length(lLine)>1) and (Copy(lLine, 1, 2)='  ')
//         then lLine := Copy(lLine, 3, Length(lLine));

      if Copy(lLine, 1, 2) = '//' // ignore comments
        then begin
             inc(i);
             continue;
             end;

      if inStatement
        then begin
             if (Trim(lLine)='') // empty line = end of statement
               then begin
                    ExecuteCommand(lStatement, i, lContinue);
                    if not lContinue then exit;
                    lStatement.Clear;
                    inStatement := false;
                    end
             else if (lLine[1]<>' ') // new command (starts at pos 1) = end of statement
               then begin
                    ExecuteCommand(lStatement, i, lContinue);
                    if not lContinue then exit;
                    lStatement.Clear;

                    // First line of new statement
                    lStatement.Add(lLine);
                    end

             else lStatement.Add(lLine); // Add to current statement
             end

        else begin
             if (Trim(lLine)='') // empty line
               then begin
                    // Ignore
                    end
             else if (Copy(lLine, 1, 1) = ' ') and (Trim(lLine)<>'') // indented line
//             else if (Copy(lLine, 1, 2) = '  ') and (Trim(lLine)<>'') // indented line
               then begin
                    // Indent only allowed when in statement
                    EUsr('Commands should start at the start of the line.');
                    end
             else begin
                  // Start of new statement
                  lStatement.Add(lLine);
                  inStatement := true;
                  end;
             end;
      inc(i);
      end;

    if lStatement.Count>0
      then ExecuteCommand(lStatement, i, lContinue); // Execute final statement

    RememberInput(i);

  except
    on E: Exception do
      HandleException(E.Message, i);
    end;

  finally
    lStatement.Free;
    end;
  end;


// -- Event handling

procedure TfConsole.ListenToRadio (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);
  begin
  if aRadioEvent = cRadioEvent_Offline
    then begin
         fRadioOnline := false;
         exit;
         end;

  if (aRadioEvent = cRadioEvent_SystemTime_Changed) or
     (aRadioEvent = cRadioEvent_Transaction_Start) or
     (aRadioEvent = cRadioEvent_Transaction_AfterEnd)
     then UpdateSessionInfo;
  end;

// -- Interface handlers

procedure TfConsole.actClearInputExecute(Sender: TObject);
  begin
  ClearInput;
  end;

procedure TfConsole.actClearOutputExecute(Sender: TObject);
  begin
  ClearOutput;
  end;

procedure TfConsole.actExecuteExecute(Sender: TObject);
  begin
  ExecuteScript;
  end;

procedure TfConsole.actLoadExecute(Sender: TObject);
  begin
  LoadScript('');
  end;

procedure TfConsole.actNextExecute(Sender: TObject);
  begin
  NextInput;
  end;

procedure TfConsole.actPreviousExecute(Sender: TObject);
  begin
  PreviousInput;
  end;

procedure TfConsole.actToggleInterfaceExecute(Sender: TObject);
  begin
  ToggleInterface;
  end;

initialization
  gIntroText := TStringlist.Create;
  gIntroText.Add('ACE - Atomic Claim Engine - ' + GetApplicationVersionStr);
  gIntroText.Add('');
  gIntroText.Add('Experimental prototype to discover, illustrate and experiment ' +
    'with capabilities of modern administrative registers.');
  gIntroText.Add('');

finalization
  FreeAndNil(gIntroText);
end.

