// -----------------------------------------------------------------------------
//
// ace.forms.ScriptEditor
//
// -----------------------------------------------------------------------------
//
// Simple editor to edit pascal scripts that can be executed at runtime.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Forms.ScriptEditor;

interface

uses
  SysUtils, Variants, Classes,
  Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, ActnList,
  common.RadioStation,
  ace.Commands.Dispatcher,
  diagram.Core, SynEdit, SynEditHighlighter,
  SynHighlighterAny, SynCompletion, SynHighlighterPas;

type
  TfScriptEditor = class(TForm)
    pnlMain: TPanel;
    pnlInput: TPanel;
    pnlInputHeader: TPanel;
    lInput: TLabel;
    pnlInputMemo: TPanel;
    pnlInputControls: TPanel;
    btnCompile: TButton;
    ActionList: TActionList;
    acCompileAndSave: TAction;
    pnlSession: TPanel;
    mInput: TSynEdit;
    EditorHighLighter: TSynPasSyn;
    acSave: TAction;
    btnSave: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCompileAndSaveExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    
  public
    procedure Init;

    // Script handling
    procedure LoadScript(aStringlist: TStringlist); overload;
    procedure LoadScript(aFilename: string); overload;

  end;

var
  fScriptEditor: TfScriptEditor;

implementation

{$R *.lfm}

uses
  common.ErrorHandling, common.Utils, common.FileUtils,
  ace.Settings, ace.Script,
  IdCustomHTTPServer,
  ace.API.Server, Diagram.Settings;


// -- Initialisation and finalisation

procedure TfScriptEditor.FormCreate(Sender: TObject);
  begin
  gSettings.LoadFormPosition(Self);

  with mInput do
    begin
    with Gutter do
      begin
      Visible := true;
      Color := clWebWhiteSmoke;
      Font.Color := clGray;
      end;
    Highlighter := EditorHighLighter;
    Font.Name := gSettings.FontName_Console;
    Font.Height := gSettings.FontHeight_Console;
    end;

  // Configure Editor HighLighter
  with EditorHighLighter do
    begin
    CommentAttri.Foreground := clWebDarkgreen;
    KeyAttri.Foreground := clWebMidnightBlue; //clWebDarkSlategray; //clWebDarkBlue;
    KeyAttri.Style := [];
    end;
  end;

procedure TfScriptEditor.FormDestroy(Sender: TObject);
  begin
  gSettings.SaveFormPosition(Self);

  fScriptEditor := nil;
  end;

procedure TfScriptEditor.Init;
  var
    lFileName: string;
  begin
  lFileName := TPath.Combine(gSettings.ProjectsFolder, gSettings.ProjectName + '.pas');
  mInput.Lines.LoadFromFile(lFilename);
  end;

procedure TfScriptEditor.FormClose(Sender: TObject; var Action: TCloseAction);
  begin
  Action := caFree
  end;


// -- Script handling

procedure TfScriptEditor.LoadScript(aStringlist: TStringlist);
  begin
  mInput.Lines.Assign(aStringlist);
  end;

procedure TfScriptEditor.LoadScript(aFilename: string);
  var
    lRootFolder: string;
    lFilename: string;
  begin
  lFilename := aFilename;

  if (lFilename='')
     then begin
          if gSettings.ProjectName<>''
             then lRootFolder := gSettings.ProjectFolder
             else lRootFolder := gSettings.ProjectsFolder;

          if not PromptForOpenFileName('Load Pascal Script', 'pas', lRootFolder, '*.pas', lFilename)
             then exit;
          end;

  if ExtractFileExt(lFilename)=''
     then lFilename := lFilename + '.pas';

  if FileExists(lFilename)
     then begin
          mInput.Lines.LoadFromFile(lFilename);
          gSettings.ProjectLoaded(lFilename);
          end
     else EUsr('Cannot find pascal script file: %s', [lFilename]);
  end;


// -- Interface


// -- Interface handlers

procedure TfScriptEditor.acCompileAndSaveExecute(Sender: TObject);
  var
    lScript: TcScript;
    lFilename: string;
  begin
  // Project required
  if not gSettings.AskProjectFolderWhenEmpty then exit;

  // Compile
  lScript := TcScript.Create;
  try
    lScript.LoadFromString(mInput.Lines.Text);
    lScript.Compile;
  finally
    lScript.Free;
    end;

  // Save script (after succesful compile)
  lFilename := TPath.Combine(gSettings.ProjectFolder, 'script.pas');
  mInput.Lines.SaveToFile(lFilename);
  ShowMessage(Format('Compile succesful.'#13#10'Saved to: %s', [lFilename]));

  // Broadcast change
  if Assigned(gRadioStation)
     then gRadioStation.BroadCast(cRadioEvent_Script_Changed, nil);
  end;

procedure TfScriptEditor.acSaveExecute(Sender: TObject);
  var
    lFilename: string;
  begin
  // Save script
  lFilename := TPath.Combine(gSettings.ProjectFolder, 'script.pas');
  mInput.Lines.SaveToFile(lFilename);
  ShowMessage(Format('Script saved to: %s', [lFilename]));
  end;

end.

