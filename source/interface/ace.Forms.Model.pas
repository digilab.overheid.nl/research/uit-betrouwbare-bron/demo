// -----------------------------------------------------------------------------
//
// ace.Forms.Model
//
// -----------------------------------------------------------------------------
//
// Form to draw models with ClaimTypes or Classses.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV
//
// -----------------------------------------------------------------------------

unit ace.Forms.Model;

{$MODE Delphi}
{$WARN 5024 OFF : Parameter "$1" not used}

interface

uses
  LCLType, LCLIntf, Classes, Generics.Collections,
  Types,
  ImgList, Controls, Dialogs, Menus, ActnList, StdCtrls,
  Buttons, ExtCtrls, Forms, PrintersDlgs,
  common.ErrorHandling, common.Utils, common.RadioStation,
  ace.Core.Engine,
  diagram.Core, diagram.Model;

const
  cDefaultScalePercentage = 140;
  cFilenamePrefix = 'diagram';

type
  TcDiagramInfo =
    class
      private
        fName: string;
        fKind: TcdDiagramKind;
      public
        constructor Create(aName: string; aKind: TcdDiagramKind);
        property Name: string read fName;
        property Kind: TcdDiagramKind read fKind;
      end;

type
  TcArtifactKind = (akLabelType, akClaimType, akAnnotationType, akClass);
  TcArtifactInfo =
    class
      private
        fID: Integer;
        fName: string;
        fKind: TcArtifactKind;
      public
        constructor Create(aID: Integer; aName: string; aKind: TcArtifactKind);
        property ID: integer read fID;
        property Name: string read fName;
        property Kind: TcArtifactKind read fKind;
      end;

  { TfModel }

  TfModel =
    class (TForm)
      acRefresh: TAction;
      btnTasks: TBitBtn;
      pnlLeft: TPanel;
      pnlDiagram: TPanel;
      Separator1: TMenuItem;
      SplitterH1: TSplitter;
      SplitterV1: TSplitter;
      pm: TPopupMenu;
      ActionList: TActionList;
      acPrint: TAction;
      pnlDiagramControls: TPanel;
      cbZoom: TComboBox;
      lZoomPercentage: TLabel;
      acDiagramRename: TAction;
      acDiagramDelete: TAction;
      acDiagramNew: TAction;
      pmDiagrams: TPopupMenu;
      miDiagramNew: TMenuItem;
      miDiagramRename: TMenuItem;
      miDiagramDelete: TMenuItem;
      pmTasks: TPopupMenu;
      Refresh1: TMenuItem;
      Print1: TMenuItem;
      PrintDialog: TPrintDialog;
      ImageList: TImageList;
      miExporttoSVG: TMenuItem;
      acExportAllToPNG: TAction;
      ExportalltoPNG1: TMenuItem;
      acExportPNG: TAction;
      lPageZoom: TLabel;
      cbPageScale: TComboBox;
      acSelectPrinter: TAction;
      Selectprinter1: TMenuItem;
      acPrintAll: TAction;
      PrintAll1: TMenuItem;
      miPrinting: TMenuItem;
      miExporting: TMenuItem;
      N1: TMenuItem;
      miSaveToFile: TMenuItem;
      miLoadFromFile: TMenuItem;
      acAllowPaperChange: TAction;
      Allowpaperchange1: TMenuItem;
      acDiagramActive: TAction;
      lStyle: TLabel;
      cbStyles: TComboBox;
      cbShowMetaConstructs: TCheckBox;
      SplitterH3: TSplitter;
      pnlArtifacts: TPanel;
      pnlArtifactsCaption: TPanel;
      lbArtifacts: TListBox;
      pnlDiagrams: TPanel;
      pnlDiagramsCaption: TPanel;
      lbDiagrams: TListBox;

      procedure acRefreshExecute(Sender: TObject);
      procedure FormClose(Sender: TObject; var Action: TCloseAction);

      // Diagram
      procedure lbDiagramsDrawItem(aControl: TWinControl; aIndex: Integer; aRect: Types.TRect; aState: StdCtrls.TOwnerDrawState);
      procedure lbDiagramsClick(Sender: TObject);
      procedure acDiagramNewUpdate(Sender: TObject);
      procedure acDiagramActiveUpdate(Sender: TObject);
      procedure acDiagramRenameExecute(Sender: TObject);
      procedure acDiagramDeleteExecute(Sender: TObject);
      procedure acDiagramNewExecute(Sender: TObject);

      // Artifacts
      procedure lbArtifactsDrawItem(aControl: TWinControl; aIndex: Integer; aRect: TRect; aState: TOwnerDrawState);

      // Control bar above diagram
      procedure cbZoomChange(Sender: TObject);
      procedure cbZoomExit(Sender: TObject);
      procedure cbPageScaleChange(Sender: TObject);
      procedure cbPageScaleExit(Sender: TObject);
      procedure cbStylesChange(Sender: TObject);
      procedure pmTasksPopup(Sender: TObject);

      // Scrollbox
      procedure HandleScrollBoxDragDrop(Sender, Source: TObject; X, Y: Integer);
      procedure HandleScrollBoxDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
      procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);

      // Tasks
      procedure acPrintExecute(Sender: TObject);
      procedure acPrintAllExecute(Sender: TObject);
      procedure acSelectPrinterExecute(Sender: TObject);
      procedure acAllowPaperChangeExecute(Sender: TObject);
      procedure acExportPNGExecute(Sender: TObject);
      procedure acExportAllToPNGExecute(Sender: TObject);
      procedure FormDestroy(Sender: TObject);

    private
      fEngine: TcEngine;
      fRadioOnline: boolean;

      fActiveDiagramIndex: Integer;
      fDiagram: TcdClassDiagram;
      fScrollBox: TcdScrollBox;
      fArtifacts: TstringList;
      fDiagrams: TObjectList<TcDiagramInfo>;

      // 'readonly' or generated diagrams should not save. This saves time :-)
      fSaveActiveDiagrams: Boolean;

      procedure TaskButtonClick(Sender: TObject);

    protected
      // Diagram
      procedure InitDiagrams;
      procedure SetZoomFactor(aZoomFactor: integer);
      procedure InitStyles;
      procedure SetStyleByName(aStyleName: string);
      function NewDiagramAllowed: Boolean;
      function DiagramNameInUse(aName: string): boolean;
      function DeriveFilename(aDiagramInfo: TcDiagramInfo; aExt: string = 'json'): string; overload;
      function DeriveFilename(aName: string; aKind: TcdDiagramKind; aExt: string = 'json'): string; overload;

      procedure NewDiagram(aName: string; aKind: TcdDiagramKind);
      procedure OpenDiagram(aDiagramInfo: TcDiagramInfo; aForcedStyle: TcdClassStyle);
      procedure SaveActiveDiagram;
      procedure CloseActiveDiagram;
      procedure RenameDiagram(aDiagramInfo: TcDiagramInfo; const aOldName, aNewName: string);
      procedure DeleteActiveDiagram;
      function GetActiveDiagramInfo: TcDiagramInfo;
      procedure SetActiveDiagramInfo(aDiagramInfo: TcDiagramInfo);
      procedure SelectDiagramByName(aName: string);

      procedure LoadFromJson(aFilename: string; aForcedStyle: TcdClassStyle);
      procedure SaveToJson(aFilename: string);

      // Artifacts
      procedure InitArtifacts;
      procedure AddArtifact(aID: integer; aX, aY: Integer);
      procedure Diagram_OnNewClass(Sender: TObject);
      procedure lbArtifacts_Clear;
      procedure lbArtifacts_Add(aID: Integer; aName: string; aKind: TcArtifactKind);
      function GetArtifactInfoFromSelectedListboxItem(aSource: TObject): TcArtifactInfo;

      // Control bar above diagram
      function AskScalePercentage(var aPercentage: Integer): Boolean;
      procedure ChangeStyle(aStyle: TcdClassStyle);

      // Scrollbox
      function DragDropSourceAccepted(aSource: TObject): Boolean;

      // Tasks
      procedure PrintDiagram(const aJobTitle: string);

      // Properties
      property SaveActiveDiagrams: Boolean read fSaveActiveDiagrams write fSaveActiveDiagrams;

    public
      constructor Create(aOwner: TComponent; aEngine: TcEngine; aKind: TcdDiagramKind); reintroduce;
      procedure Init;
      destructor Destroy; override;

      procedure ListenToRadio (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);

      property Engine: TcEngine read fEngine;
      property ScrollBox: TcdScrollBox read fScrollBox;
      property Diagram: TcdClassDiagram read fDiagram write fDiagram;
      property Diagrams: TObjectList<TcDiagramInfo> read fDiagrams;
      property ActiveDiagramInfo: TcDiagramInfo read GetActiveDiagramInfo write SetActiveDiagramInfo;
    end;

var
  fModel: TfModel;

implementation

{$R *.lfm}

uses
   SysUtils, Common.Json, fpjson, Graphics,
   common.InputQueryList, common.FileUtils,
   ace.Settings;

const
   // 10 percent is the minimum zoom level
   cMinScalePercentage = 10;

{ Conversion functions }

function DiagramKindToFileExt(aKind: TcdDiagramKind): string;
  begin
  result := ''; // Suppress warning
  case aKind of
    dkAtomicTypes: result := 'at_diagram';
    dkClasses: result := 'cl_diagram';
    else EInt('DiagramKindToFileExt', 'Unknown diagram kind');
    end;
  end;

function DiagramKindToName(aKind: TcdDiagramKind): string;
  begin
  result := ''; // Suppress warning
  case aKind of
    dkAtomicTypes: result := 'AtomicTypes';
    dkClasses: result := 'Classes';
    else EInt('DiagramKindToName', 'Unknown diagram kind');
    end;
  end;

function StrToDiagramKind(aStr: string): TcdDiagramKind;
  begin
  result := dkAtomicTypes; // Suppress warning
  if CompareText(aStr, 'AtomicTypes')=0
     then result := dkAtomicTypes
  else if CompareText(aStr, 'Classes')=0
     then result := dkClasses
  else EInt('StrToDiagramKind', 'Unkwown diagram kind');
  end;

function DiagramKindAllowsStyle(aKind: TcdDiagramKind; aStyleName: string): boolean;
  var
    lAllowedStyles: TStringlist;
    lIndex: Integer;
  begin
  lAllowedStyles := TStringlist.Create;
  lAllowedStyles.CaseSensitive := false;
  lAllowedStyles.Sorted := true;
  lAllowedStyles.Duplicates := dupError;
  try
    case aKind of
      dkAtomicTypes:
        begin
        lAllowedStyles.Add('ORM');
        lAllowedStyles.Add('ORM2');
        end;
      dkClasses:
        begin
        lAllowedStyles.Add('IM');
        lAllowedStyles.Add('UML');
        lAllowedStyles.Add('UML-IM');
        lAllowedStyles.Add('ERD');
        lAllowedStyles.Add('Record');
        lAllowedStyles.Add('ORM2 Compact');
        end;
      else EInt('DiagramKindAllowsStyle', 'Unknown diagramKind');
      end;

    result := lAllowedStyles.Find(aStyleName, lIndex);

  finally
    lAllowedStyles.Free;
    end;
  end;


{ TcDiagramInfo }

constructor TcDiagramInfo.Create(aName: string; aKind: TcdDiagramKind);
  begin
  inherited Create;
  fName := aName;
  fKind := aKind;
  end;


{ TcArtifactInfo }

constructor TcArtifactInfo.Create(aID: Integer; aName: string; aKind: TcArtifactKind);
   BEGin
   inherited Create;
   fID := aID;
   fName := aName;
   fKind := aKind;
   end;


{ TfBaseDiagram }

// -- Initialisation / Finalisation

constructor TfModel.Create(aOwner: TComponent; aEngine: TcEngine; aKind: TcdDiagramKind);
  procedure SetComboWidth(aCombo: TCombobox);
    var
      lItem: string;
      lMaxLen: Integer;
    begin
    lMaxLen := 0;
    for lItem in aCombo.Items do
       if aCombo.Canvas.TextWidth(lItem) > lMaxLen
         then lMaxLen := aCombo.Canvas.TextWidth(lItem);

    // Unable to find an easy way to get the dropdownwidth button
    aCombo.ClientWidth := lMaxLen +  GetSystemMetrics(SM_CXVSCROLL) + GetSystemMetrics(SM_SWSCROLLBARSPACING) + GetSystemMetrics(SM_CXBORDER)*2 + 10;
    end;

  var
    lInitialStyle: string;
  begin
  inherited Create(aOwner);

  gSettings.LoadFormPosition(Self);

  fEngine := aEngine;
  fActiveDiagramIndex := -1;
  fSaveActiveDiagrams := True;

  // Diagram
  fScrollBox := TcdScrollBox.Create(pnlDiagram);
  fScrollBox.BorderStyle := bsNone;
  fScrollBox.OnDragOver := HandleScrollBoxDragOver;
  fScrollBox.OnDragDrop := HandleScrollBoxDragDrop;
  fScrollBox.Parent := pnlDiagram;
  fScrollBox.Align := alClient;
  fScrollBox.PopupMenu := pm;

  case aKind of
   dkClasses:
     lInitialStyle := 'UML-IM';
   else
     lInitialStyle := 'ORM';
  end;

  fDiagram := TcdClassDiagram.Create(ScrollBox, lInitialStyle);
  Diagram.Init(gSettings.Engine, aKind);
  Diagram.OnNewClass := Diagram_OnNewClass;

  // Diagrams
  fDiagrams := TObjectList<TcDiagramInfo>.Create;
  InitDiagrams;
  SetZoomFactor(cDefaultScalePercentage);

  // Artifacts
  fArtifacts := TStringList.Create;

  // Controle bar above diagram
  Diagram.Style := TcdClassStyle(DiagramStyles.GetStyle(lInitialStyle));
  if not Assigned(Diagram.Style)
     then EInt('TfBaseDiagram.Create', 'Invalid diagram style ''%s''', [lInitialStyle]);

  InitStyles;

  SetComboWidth(cbZoom);
  SetComboWidth(cbPageScale);
  SetComboWidth(cbStyles);

  btnTasks.Layout := blGlyphRight;
  btnTasks.Spacing := -1;
  btnTasks.PopupMenu := pmTasks;
  btnTasks.OnClick := TaskButtonClick;

  // Make sure a (default) diagram exists
  if Diagrams.Count = 0
     then NewDiagram('', aKind);

  gRadioStation.AddRadio(Self, ListenToRadio);
  fRadioOnline := true;
  end;

procedure TfModel.Init;
  begin
  if fActiveDiagramIndex = -1
     then ActiveDiagramInfo := fDiagrams[0];
  end;

procedure TfModel.FormClose(Sender: TObject; var Action: TCloseAction);
  begin
  CloseActiveDiagram;
  Action := caFree;
  end;

procedure TfModel.acRefreshExecute(Sender: TObject);
begin
  Diagram.ForceRedraw;
end;

procedure TfModel.FormDestroy(Sender: TObject);
  begin
  if fRadioOnline
    then gRadioStation.RemoveRadio(Self);

  CloseActiveDiagram;

  fArtifacts.Free;
  fDiagrams.Free;

  lbArtifacts_Clear;

  fModel := nil;
  end;

destructor TfModel.Destroy;
  begin
  gSettings.SaveFormPosition(Self);

  inherited Destroy;
  end;

procedure TfModel.ListenToRadio (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);
  begin
  if aRadioEvent = cRadioEvent_Offline
    then begin
         fRadioOnline := false;
         exit;
         end;

  if (aRadioEvent = cRadioEvent_LabelType_Created) or
     (aRadioEvent = cRadioEvent_ClaimType_Created) or
     (aRadioEvent = cRadioEvent_AnnotationType_Created) or
     (aRadioEvent = cRadioEvent_Class_Created)
     then InitArtifacts;
  end;

// -- Core -- Diagram

procedure TfModel.InitDiagrams;
  var
    i: integer;
    lFiles: TStringlist;
    lFileMask: string;
    lFilename: string;
    lName: string;
  begin
  CloseActiveDiagram;

  fDiagrams.Clear;
  lbDiagrams.Clear;

  // Retrieve ClaimType diagrams
  lFiles := TStringlist.Create;
  lFileMask := cFilenamePrefix+'_atomictypes*.json';
  try
    ScanFolder(gSettings.ProjectFolder, lFileMask, lFiles, false);
    for i := 0 to lFiles.Count-1 do
      begin
      lFilename := lFiles[i];
      lName := Copy(lFilename, Length(cFilenamePrefix)+2, MaxInt);
      lName := TPath.GetFilenameWithoutExtension(lName);
      if Length(lName)>Length('atomictypes')
         then lName := Copy(lName, Length('atomictypes')+2, MaxInt);
      Diagrams.Add(TcDiagramInfo.Create(lName, dkAtomicTypes));
      end;
  finally
    lFiles.Free;
    end;

  // Retrieve Class diagrams
  lFiles := TStringlist.Create;
  lFileMask := cFilenamePrefix+'_classes*.json';
  try
    ScanFolder(gSettings.ProjectFolder, lFileMask, lFiles, false);
    for i := 0 to lFiles.Count-1 do
      begin
      lFilename := lFiles[i];
      lName := Copy(lFilename, Length(cFilenamePrefix)+2, MaxInt);
      lName := TPath.GetFilenameWithoutExtension(lName);
      if Length(lName)>Length('classes')
         then lName := Copy(lName, Length('classes')+2, MaxInt);
      Diagrams.Add(TcDiagramInfo.Create(lName, dkClasses));
      end;
  finally
    lFiles.Free;
    end;

  // Add to listbox
  for i := 0 to fDiagrams.Count-1 do
    lbDiagrams.Items.AddObject(fDiagrams[i].Name, fDiagrams[i]);
  end;

procedure TfModel.InitStyles;
  var
    i: Integer;
    lStyle: TcdStyle;
  begin
  cbStyles.Items.Clear;
  // cbStyles.Sorted := True;

  for i := 0 to DiagramStyles.Count-1 do
    begin
    lStyle := TcdStyle(DiagramStyles.Objects[i]);
    if not (lStyle is TcdClassStyle) then continue;

    cbStyles.Items.AddObject(lStyle.Name, lStyle);
    end;

  if cbStyles.Items.Count>20
     then cbStyles.DropDownCount := 20
     else cbStyles.DropDownCount := cbStyles.Items.Count;

  if not Assigned(ActiveDiagramInfo)
     then exit;

  for i := 0 to cbStyles.Items.Count-1 do
    begin
    if cbStyles.Items.Objects[i] = Diagram.Style
       then begin
            cbStyles.ItemIndex := i;
            exit;
            end;
    end;
  end;

procedure TfModel.SetZoomFactor(aZoomFactor: integer);
  var
    i: integer;
    lZoomFactorStr: string;
  begin
  lZoomFactorStr := IntToStr(aZoomFactor)+'%';
  if (cbZoom.Text = lZoomFactorStr) and (Diagram.ScalePercentage = aZoomFactor) then exit;

  for i := 0 to cbZoom.Items.Count-1 do
    if cbZoom.Items[i] = lZoomFactorStr
       then begin
            cbZoom.ItemIndex := i;
            break;
            end;
  cbZoomChange(Self);
  end;

procedure TfModel.SetStyleByName(aStyleName: string);
  var
    i: integer;
  begin
  cbStyles.ItemIndex := -1;
  for i := 0 to cbStyles.Items.Count-1 do
    begin
    if cbStyles.Items[i] = aStyleName
       then begin
            cbStyles.ItemIndex := i;
            break;
            end;
    end;
  if cbStyles.ItemIndex>=0
     then cbStylesChange(Self);
  end;

function TfModel.NewDiagramAllowed: Boolean;
  begin
  Result := True;
  end;

function TfModel.DiagramNameInUse(aName: string): boolean;
  var
    i: integer;
    lDiagramInfo: TcDiagramInfo;
  begin
  result := false;
  for i := 0 to fDiagrams.Count-1 do
    begin
    lDiagramInfo := fDiagrams[i];
    if CompareText(lDiagramInfo.Name, aName)=0
       then begin
            result := true;
            exit;
            end;
    end;
  end;

function TfModel.DeriveFilename(aDiagramInfo: TcDiagramInfo; aExt: string = 'json'): string;
  begin
  result := DeriveFilename(aDiagramInfo.Name, aDiagramInfo.Kind, aExt);
  end;

function TfModel.DeriveFilename(aName: string; aKind: TcdDiagramKind; aExt: string = 'json'): string;
  begin
  if (CompareText(aName, 'AtomicTypes')=0) or (CompareText(aName, 'Classes')=0)
     then begin
          // Default filename
          result := Format('%s_%s.%s', [cFilenamePrefix, lowercase(aName), aExt]);
          end
     else begin
          // Custom filename
          result := Format('%s_%s_%s.%s', [cFilenamePrefix, lowercase(DiagramKindToName(aKind)), lowercase(aName), aExt]);
          end;
  result := TPath.Combine(gSettings.ProjectFolder, result);
  end;

procedure TfModel.NewDiagram(aName: string; aKind: TcdDiagramKind);
  var
    lFilename: string;
    lOption: string;
    lDiagramKind: TcdDiagramKind;
  begin
  CloseActiveDiagram;

  if aKind = dkUnknown
     then begin
          // Ask diagramtype
          lOption := '';
          if not InputQueryList('Diagram type', 'Select the diagram type', ['AtomicTypes', 'Classes'], true, lOption)
             then exit;
          lDiagramKind := StrToDiagramKind(lOption);
          end
     else lDiagramKind := aKind;

  if aName = ''
     then aName := DiagramKindToName(lDiagramKind);

  if DiagramNameInUse(aName)
     then begin
          repeat
            if not InputQuery('New diagram', 'Name', aName)
               then exit;
            aName := ConvertToFilename(aName);
            until not DiagramNameInUse(aName);
          end;

  // Derive filename
  lFilename := DeriveFilename(aName, lDiagramKind);

  // Create diagram
  Diagram.NewDiagram;

  case lDiagramKind of
    dkAtomicTypes: Diagram.Style := TcdClassStyle(DiagramStyles.GetStyle('ORM'));
    dkClasses: Diagram.Style := TcdClassStyle(DiagramStyles.GetStyle('UML-IM'));
    end;
  Diagram.ScalePercentage := cDefaultScalePercentage;

  // Save to disk
  SaveToJson(lFilename);

  // Reinit list of diagram and select the new diagram
  InitDiagrams;
  SelectDiagramByName(aName);
  InitArtifacts;
  InitStyles;

  SetStyleByName(Diagram.Style.Name);
  SetZoomFactor(Diagram.ScalePercentage);
  end;

procedure TfModel.OpenDiagram(aDiagramInfo: TcDiagramInfo; aForcedStyle: TcdClassStyle);
  var
    lFilename: string;
  begin
  if not Assigned(aDiagramInfo)
     then Exit;
  //BeforeOpenDiagram;

  Screen.Cursor := crHourglass;
  Diagram.BeginChange;

  // Make sure we're at 0.0 because everything is depending on the
  // scrollbox position during add
  fScrollBox.HorzScrollbarPosition := 0;
  fScrollBox.VertScrollbarPosition := 0;
  try
    lFilename := DeriveFilename(aDiagramInfo);
    LoadFromJson(lFilename, aForcedStyle);

    cbZoom.Text := IntToStr(Diagram.ScalePercentage) + '%';
    cbZoom.SelLength := 0;

    cbPageScale.Text := IntToStr(Diagram.ScalePercentagePrint) + '%';
    cbPageScale.SelLength := 0;

    fActiveDiagramIndex := fDiagrams.IndexOf(aDiagramInfo);
    lbDiagrams.ItemIndex := fActiveDiagramIndex;

    InitArtifacts;
    InitStyles;

    //AfterOpenDiagram(lIsNew);
  finally
    Diagram.EndChange;
    Screen.Cursor := crDefault;
    end
  end;

procedure TfModel.SaveActiveDiagram;
  var
    //lWasModified: Boolean;
    lFilename: string;
  begin
  if not SaveActiveDiagrams
     then Exit;

  if not Assigned(ActiveDiagramInfo)
     then Exit;

  Screen.Cursor := crHourGlass;
  try
    //lWasModified :=  Diagram.Modified;

    lFilename := DeriveFilename(ActiveDiagramInfo);
    SaveToJson(lFilename);

    //AfterSaveDiagram(lWasModified);

  finally
    Screen.Cursor := crDefault;
    end;
  end;

procedure TfModel.CloseActiveDiagram;
  begin
  if Assigned(ActiveDiagramInfo)
     then begin
          SaveActiveDiagram;
          // Reset the index, not the object. This causes a loop
          fActiveDiagramIndex := -1;
          InitArtifacts;
          InitStyles;
          end;

  Diagram.Clear;

  //AfterCloseDiagram;
  end;

procedure TfModel.RenameDiagram(aDiagramInfo: TcDiagramInfo; const aOldName, aNewName: string);
  var
    lOldFilename, lNewFilename: string;
  begin
  lOldFilename := DeriveFilename(aOldName, aDiagramInfo.Kind);
  lNewFilename := DeriveFilename(aNewName, aDiagramInfo.Kind);
  RenameFile(lOldFilename, lNewFileName);
  InitDiagrams;
  SelectDiagramByName(aNewName);
  InitArtifacts;
  InitStyles;
  end;

procedure TfModel.DeleteActiveDiagram;
  var
    lDiagramInfo: TcDiagramInfo;
    lFilename: string;
  begin
  lDiagramInfo := ActiveDiagramInfo;

  lFilename := DeriveFilename(lDiagramInfo);
  SysUtils.DeleteFile(lFilename);

  ActiveDiagramInfo := nil;

  InitDiagrams;
  InitArtifacts;
  InitStyles;
  end;

function TfModel.GetActiveDiagramInfo: TcDiagramInfo;
  begin
  if (fActiveDiagramIndex<0) or not Assigned(fDiagrams)
     then Result := nil
     else Result := fDiagrams[fActiveDiagramIndex];
  end;

procedure TfModel.SetActiveDiagramInfo(aDiagramInfo: TcDiagramInfo);
  begin
  if ActiveDiagramInfo <> aDiagramInfo
     then begin
          CloseActiveDiagram;
          OpenDiagram(aDiagramInfo, nil);
          end;
  end;

procedure TfModel.SelectDiagramByName(aName: string);
  var
    lDiagramInfo: TcDiagramInfo;
  begin
  for lDiagramInfo in Diagrams do
     if lDiagramInfo.Name = aName
        then begin
             ActiveDiagramInfo := lDiagramInfo;
             Exit;
             end;
  end;

procedure TfModel.LoadFromJson(aFilename: string; aForcedStyle: TcdClassStyle);
  var
    lChangeHandler: TNotifyEvent;
    lJsonStrings: TStringlist;
    lJsonString: string;
    lJson: TJsonObject;
    i: integer;
  begin

  lJsonStrings := TStringlist.Create;
  try
    lJsonStrings.LoadFromFile(aFilename);
    lJsonString := StringListToString(lJsonStrings, #13#10);
    lJson := TJsonObject.ParseJSONValue(lJsonString) as TJsonObject;
  finally
    lJsonStrings.Free;
    end;

  Diagram.LoadFromJson(lJson, aForcedStyle);

  // Make sure the combobox displays the correct style
  lChangeHandler := cbStyles.OnChange;
  cbStyles.OnChange := nil;
  try
    for i := 0 to cbStyles.Items.Count-1 do
      begin
      if CompareText(cbStyles.Items[i], Diagram.Style.Name)=0
         then begin
              cbStyles.ItemIndex := i;
              break;
              end;
      end;
  finally
    cbStyles.OnChange := lChangeHandler;
    end;
  end;

procedure TfModel.SaveToJson(aFilename: string);
  var
    lJson: TJsonObject;
    lJsonStrings: TStringlist;
  begin
  lJson := TJsonObject.Create;
  Diagram.SaveToJson(lJson);

  lJsonStrings := TStringlist.Create;
  try
    lJsonStrings.Add(lJson.Format(2));
    lJsonStrings.SaveToFile(aFilename);
  finally
    lJsonStrings.Free;
    end;
  end;


// -- Core -- Artifacts

procedure TfModel.InitArtifacts;
  var
    i: integer;
    lArtifactSyncID: Integer;
    lDiagramInfo: TcDiagramInfo;
    lType: TcType;
    lClass: TcClass;
    lKind: TcArtifactKind;
  begin
  if lbArtifacts.ItemIndex>=0
     then lArtifactSyncID := TcArtifactInfo(lbArtifacts.Items.Objects[lbArtifacts.ItemIndex]).ID
     else lArtifactSyncID := -1;

  lbArtifacts.Items.BeginUpdate;
  try
    lbArtifacts_Clear;

    lDiagramInfo := ActiveDiagramInfo;
    if not Assigned(lDiagramInfo) then exit;

    if lDiagramInfo.Kind = dkAtomicTypes
       then begin
            // Add existing types
            for i := 0 to Engine.ConceptualTypes.Count-1 do
              begin
              lType := Engine.ConceptualTypes[i];
              if (not cbShowMetaConstructs.Checked) and (lType.Name[1] = cMetaChar) then continue;

              lKind := akLabelType; // Suppress warning
              if lType is TcLabelType
                 then lKind := akLabelType
              else if lType is TcClaimType
                 then lKind := akClaimType
              else if lType is TcAnnotationType
                 then lKind := akAnnotationType
              else EINt('TfModel.InitArtifacts', 'Unknown TcType');

              lbArtifacts_Add(lType.ID, lType.Name, lKind);

              if lArtifactSyncID = lType.ID
                 then lbArtifacts.ItemIndex := lbArtifacts.Items.Count-1;
              end;
            end
       else begin
            // Add existing classes
            for i := 0 to Engine.Classes.Count-1 do
              begin
              lClass := Engine.Classes[i];

              lbArtifacts_Add(lClass.ID, lClass.Name, akClass);

              if lArtifactSyncID = lClass.ID
                 then lbArtifacts.ItemIndex := lbArtifacts.Items.Count-1;
              end;
            end;

  finally
    lbArtifacts.Items.EndUpdate;
    end;
  end;

procedure TfModel.AddArtifact(aID: integer; aX, aY: Integer);
  var
    lClass: TcdBaseClass;
    lElement: TcElement;
  begin
  lClass := Diagram.Classes.Find(aID);
  if Assigned(lClass)
      THEN EUsr('''%s'' already added to diagram', [lClass.Name]);

  Diagram.BeginChange;
  try
    lElement := Engine.Elements.Find(aID);
    if not Assigned(lElement)
       then EInt('TfBaseDiagram.AddObject', 'Element not found');
    if lElement is TcLabelType
       then lClass := TcdSimpleClass.Create(Diagram)
    else if (lElement is TcClaimType)
       then lClass := TcdComplexClass.Create(Diagram, ctClaimType)
    else if (lElement is TcAnnotationType)
       then lClass := TcdComplexClass.Create(Diagram, ctAnnotationType)
    else if (lElement is TcClass)
       then lClass := TcdComplexClass.Create(Diagram, ctClass)
       else EInt('TfBaseDiagram.AddObject', 'Cannot add element of type ''%s''', [lElement.ClassName]);

    lClass.LoadFromData(aID);
    Diagram.Add(lClass, aX, aY);
    lClass.Link;
  finally
    Diagram.EndChange;
  end;
  end;

procedure TfModel.Diagram_OnNewClass(Sender: TObject);
  begin
  // Make sure the new Artifact is added to the lists
  InitArtifacts;
  end;

procedure TfModel.lbArtifacts_Clear;
  var
    i: Integer;
  begin
  for i := 0 TO lbArtifacts.Items.Count-1 do
     lbArtifacts.Items.Objects[i].Free;
  lbArtifacts.Clear;
  end;

procedure TfModel.lbArtifacts_Add(aID: Integer; aName: string; aKind: TcArtifactKind);
  var
    lArtifactInfo: TcArtifactInfo;
  begin
  lArtifactInfo := TcArtifactInfo.Create(aID, aName, aKind);
  lbArtifacts.Items.AddObject(aName, lArtifactInfo);
  end;

function TfModel.GetArtifactInfoFromSelectedListboxItem(aSource: TObject): TcArtifactInfo;
  var
     lb: TListbox;
  begin
  result := nil;
  if aSource <> lbArtifacts
     then exit;

  lb := TListbox(aSource);
  if lb.ItemIndex >= 0
     then Result := TcArtifactInfo(lb.Items.Objects[lb.ItemIndex]);
  end;


// -- Core -- Control bar above diagram

function TfModel.AskScalePercentage(var aPercentage: Integer): Boolean;
  var
     lScalePercStr: string;
     lScalePerc: Integer;
  begin
  lScalePercStr := aPercentage.Tostring();
  Result := InputQuery('Scale percentage', 'Factor (in %)', lScalePercStr);
  if not Result then Exit;

  lScalePerc := StrToIntDef(lScalePercStr, -1);
  if (lScalePerc<1) OR (lScalePerc>500)
     then begin
          ShowMessage('Factor should be between 1 and 500.');
          Result := False;
          Exit;
          end;

  aPercentage := lScalePerc;
  Result := True;
  end;

procedure TfModel.ChangeStyle(aStyle: TcdClassStyle);
  var
     lActiveDiagramInfo: TcDiagramInfo;
  begin
  if Diagram.IsChanging then Exit;
  if Diagram.Style = aStyle then Exit;

  lActiveDiagramInfo := ActiveDiagramInfo;
  CloseActiveDiagram;
  OpenDiagram(lActiveDiagramInfo, aStyle);

  gModeler_SimpleClass_Name := aStyle.SimpleClass_Name;
  gModeler_SimpleClass_PluralName := aStyle.SimpleClass_PluralName;
  gModeler_ComplexClass_Name := aStyle.ComplexClass_Name;
  gModeler_ComplexClass_PluralName := aStyle.ComplexClass_PluralName;
  gModeler_Attribute_Name := aStyle.Attribute_Name;
  gModeler_Attribute_PluralName := aStyle.Attribute_PluralName;
  end;


// -- Core -- Scrollbox

function TfModel.DragDropSourceAccepted(aSource: TObject): Boolean;
  begin
  Result := (aSource = lbArtifacts);
  end;


// -- Core -- Tasks

procedure TfModel.PrintDiagram(const aJobTitle: string);
  begin
  fDiagram.Print(aJobTitle);
  end;


// -- Interface binding

// -- Interface binding -- Diagram

procedure TfModel.lbDiagramsDrawItem(aControl: TWinControl; aIndex: Integer; aRect: Types.TRect; aState: StdCtrls.TOwnerDrawState);
  var
    lFlags: Longint;
    lStr: string;
    lDiagramInfo: TcDiagramInfo;
    lImageIndex: integer;
  begin
  lbDiagrams.Canvas.FillRect(aRect);
  if aIndex < lbDiagrams.Items.Count
     then begin
          lDiagramInfo := TcDiagramInfo(lbDiagrams.Items.Objects[aIndex]);
          if not Assigned(lDiagramInfo)
             then EInt('TfModel.lbDiagramsDrawItem', 'Diagraminfo missing');

          lImageIndex := 0; // Suppress warning
          case lDiagramInfo.Kind of
           dkAtomicTypes: lImageIndex := 1;
           dkClasses: lImageIndex := 3;
           else EInt('TfModel.lbDiagramsDrawItem', 'Unknown kind');
          end;
          ImageList.Draw(lbDiagrams.Canvas, aRect.Left+2, aRect.Top+2, lImageIndex, dsTransparent, itImage);
          Inc(aRect.Left, ImageList.Width + 2);

          lFlags := DT_SinGLELinE or DT_VCENTER or DT_NOPREFIX;
          if not UseRightToLeftAlignment
             then Inc(aRect.Left, 2)
             else Dec(aRect.Right, 2);
          lStr := lbDiagrams.Items[aIndex];
          DrawText(lbDiagrams.Canvas.Handle, PChar(lStr), Length(lStr), aRect, lFlags);
          end;
  end;

procedure TfModel.lbDiagramsClick(Sender: TObject);
  begin
  if lbDiagrams.ItemIndex<0
     then CloseActiveDiagram;

  if ActiveDiagramInfo = fDiagrams[lbDiagrams.ItemIndex]
     then begin
          if lbDiagrams.SelCount <= 1
             then begin
                  lbDiagrams.ClearSelection;
                  CloseActiveDiagram
                  end
          end
     else ActiveDiagramInfo := fDiagrams[lbDiagrams.ItemIndex];
  end;

procedure TfModel.acDiagramNewUpdate(Sender: TObject);
  begin
  TAction(Sender).Enabled := NewDiagramAllowed;
  end;

procedure TfModel.acDiagramActiveUpdate(Sender: TObject);
  begin
  (Sender as TAction).Enabled := Assigned(ActiveDiagramInfo);
  end;

procedure TfModel.acDiagramRenameExecute(Sender: TObject);
  var
    lOldName, lNewName: string;
  begin
  if not Assigned(ActiveDiagramInfo)
     then Exit;

  lOldName := ActiveDiagramInfo.Name;
  lNewName := lOldName;
  if not InputQuery('Rename diagram', 'New name', lNewName) then Exit;

  RenameDiagram(ActiveDiagramInfo, lOldName, lNewName);
  end;

procedure TfModel.acDiagramDeleteExecute(Sender: TObject);
  begin
  if not Assigned(ActiveDiagramInfo)
     then Exit;

  if MessageDlg(Format('Are you sure you want to remove the diagram ''%s''?', [ActiveDiagramInfo.Name]), mtWarning, [mbYes, mbNo], 0)=mrYes
     then begin
          DeleteActiveDiagram;
          end;
  end;

procedure TfModel.acDiagramNewExecute(Sender: TObject);
  begin
  NewDiagram('', dkUnknown);
  end;

// -- Interface binding -- Artifacts
procedure TfModel.lbArtifactsDrawItem(aControl: TWinControl; aIndex: Integer; aRect: Types.TRect; aState: StdCtrls.TOwnerDrawState);
  var
    lFlags: Longint;
    lStr: string;
    lArtifactInfo: TcArtifactInfo;
    lImageIndex: integer;
  begin
  lbArtifacts.Canvas.FillRect(aRect);
  if aIndex < lbArtifacts.Items.Count
     then begin
          lArtifactInfo := TcArtifactInfo(lbArtifacts.Items.Objects[aIndex]);
          lImageIndex := 0; // Suppress warning
          case lArtifactInfo.Kind of
           akLabelType: lImageIndex := 0;
           akClaimType: lImageIndex := 1;
           akAnnotationType: lImageIndex := 2;
           akClass: lImageIndex := 3;
           else EInt('TfModel.lbArtifactsDrawItem', 'Unknown kind');
          end;
          ImageList.Draw(lbArtifacts.Canvas, aRect.Left+2, aRect.Top+2, lImageIndex, dsTransparent, itImage);
          Inc(aRect.Left, ImageList.Width + 2);

          lFlags := DT_SINGLELINE or DT_VCENTER or DT_NOPREFIX;
          if not UseRightToLeftAlignment
             then Inc(aRect.Left, 2)
             else Dec(aRect.Right, 2);
          lStr := lbArtifacts.Items[aIndex];
          DrawText(lbArtifacts.Canvas.Handle, PChar(lStr), Length(lStr), aRect, lFlags);
          end;
  end;

// -- Interface binding -- Control bar above diagram

procedure TfModel.cbZoomChange(Sender: TObject);
  var
    lPercentage: Integer;
    lScaleFactor: Double;
  begin
  try
    if Copy(cbZoom.Text, Length(cbZoom.Text), 1) = '%'
       then lPercentage := StrToInt(Copy(cbZoom.Text, 1, Length(cbZoom.Text)-1))
       else lPercentage := StrToInt(cbZoom.Text);

    if (Diagram.ScalePercentage <> lPercentage) AND (lPercentage > cMinScalePercentage)
       then begin
            lScaleFactor := lPercentage / Diagram.ScalePercentage;
            Diagram.ScalePercentage := lPercentage;

            cbZoom.Text := IntToStr(lPercentage) + '%';
            // Make sure the cursor is set after the last digit
            cbZoom.SelStart := Length(cbZoom.Text) - 1;

            Scrollbox.HorzScrollBarPosition := Round(Scrollbox.HorzScrollBarPosition * lScaleFactor);
            Scrollbox.VertScrollBarPosition := Round(Scrollbox.VertScrollBarPosition * lScaleFactor);
            end;
  except
    end;
  end;

procedure TfModel.cbZoomExit(Sender: TObject);
  begin
  // Make sure the selection is not visible (ugly)
  cbZoom.SelLength := 0;
  end;

procedure TfModel.cbPageScaleChange(Sender: TObject);
  var
    lPercentage: Integer;
  begin
  try
     if Copy(cbPageScale.Text, Length(cbPageScale.Text), 1) = '%'
        then lPercentage := StrToInt(Copy(cbPageScale.Text, 1, Length(cbPageScale.Text)-1))
        else lPercentage := StrToInt(cbPageScale.Text);

     if (Diagram.ScalePercentagePrint <> lPercentage) AND (lPercentage > cMinScalePercentage)
        then begin
             Diagram.ScalePercentagePrint := lPercentage;
             cbPageScale.Text := IntToStr(lPercentage) + '%';
             // Make sure the cursor is set after the last digit
             cbPageScale.SelStart := Length(cbPageScale.Text) - 1;
             end;
  except
    end;
  end;

procedure TfModel.cbPageScaleExit(Sender: TObject);
  begin
  // Make sure the selection is not visible (ugly)
  cbPageScale.SelLength := 0;
  end;

procedure TfModel.cbStylesChange(Sender: TObject);
  var
    lStyle: TcdClassStyle;
  begin
  Assert(cbStyles.ItemIndex >= 0);
  Assert(cbStyles.ItemIndex < cbStyles.Items.Count);
  lStyle := TcdClassStyle(cbStyles.Items.Objects[cbStyles.ItemIndex]);
  lStyle.DeriveProperties(Diagram);
  ChangeStyle(lStyle);
  end;

procedure TfModel.pmTasksPopup(Sender: TObject);
  begin
  acAllowPaperChange.Checked := Diagram.AllowPaperChange
  end;

procedure TfModel.TaskButtonClick(Sender: TObject);
  var
    lPoint: TPoint;
  begin
  lPoint := btnTasks.ClientToScreen(Classes.Point(0, btnTasks.Height));

  pmTasks.Popup(lPoint.x, lPoint.y);
  end;


// -- Interface binding -- Scrollbox

procedure TfModel.HandleScrollBoxDragDrop(Sender, Source: TObject; X, Y: Integer);
  var
    lArtifactInfo: TcArtifactInfo;
  begin
  if DragDropSourceAccepted(Source)
      then begin
           lArtifactInfo := GetArtifactInfoFromSelectedListboxItem(Source);
           if not Assigned(lArtifactInfo) then exit;
           AddArtifact(lArtifactInfo.ID, X, Y);
           end;
  end;

procedure TfModel.HandleScrollBoxDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
  var
    lActiveDiagramInfo: TcDiagramInfo;
    lArtifactInfo: TcArtifactInfo;
  begin
  Accept := False;

  // Source must be valid
  if not DragDropSourceAccepted(Source) then exit;

  // Diagram should be active
  lActiveDiagramInfo := ActiveDiagramInfo;
  if not Assigned(lActiveDiagramInfo) then exit;

  // Artifact must be valid
  lArtifactInfo := GetArtifactInfoFromSelectedListboxItem(Source);
  if not Assigned(lArtifactInfo) then exit;

  Accept := True;
  end;

procedure TfModel.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  begin
  inherited;
  if KEY = VK_ESCAPE
     then begin
          if Diagram.Scrollbox.DragMode <> dmNone
             then Diagram.Scrollbox.CancelDragMode
          else if Diagram.Selection.Count > 0
             then Diagram.Selection.Clear;
          end;
  end;

// -- Interface binding -- Tasks

procedure TfModel.acPrintExecute(Sender: TObject);
  begin
  if PrintDialog.Execute
     then PrintDiagram(ActiveDiagramInfo.Name);
  end;

procedure TfModel.acPrintAllExecute(Sender: TObject);
  var
    i: Integer;
    lDiagramInfo: TcDiagramInfo;
  begin
  if PrintDialog.Execute
     then begin
          // Iterate over diagrams
          for i := 0 TO lbDiagrams.Count-1 do
             begin
             // If none or 1 selected (you shouldn't use all if you want to print 1)
             // or it's selected
             if (lbDiagrams.SelCount <= 1) OR
                lbDiagrams.Selected[i]
                then begin
                     lDiagramInfo := fDiagrams[i];

                     CloseActiveDiagram;
                     OpenDiagram(lDiagramInfo, nil);

                     PrintDiagram(lDiagramInfo.Name);
                     end;

             end;

          ShowMessage('Printen gereed.');
          end;
  end;

procedure TfModel.acSelectPrinterExecute(Sender: TObject);
  begin
  if PrintDialog.Execute
     then begin
          Diagram.PrinterChanged;
          end;
  end;

procedure TfModel.acAllowPaperChangeExecute(Sender: TObject);
  begin
  Diagram.AllowPaperChange := not Diagram.AllowPaperChange;
  acAllowPaperChange.Checked := Diagram.AllowPaperChange;
  end;

procedure TfModel.acExportPNGExecute(Sender: TObject);
  var
    lFileName: string;
    lScalePerc: Integer;
  begin
  lFileName := DeriveFilename(ActiveDiagramInfo, 'png');

  lScalePerc := 100;
  if PromptForSaveFileName('Export diagram as PNG', '.png', '', 'PNG files|*.png|All files|*.*', lFileName) AND
     AskScalePercentage(lScalePerc)
     then begin
          Diagram.ExportToPng(lFileName, lScalePerc);
          ShowMessage('Export done.');
          end;
  end;

procedure TfModel.acExportAllToPNGExecute(Sender: TObject);
  var
    lFolderName: string;
    lFileName: string;
    lDiagramInfo: TcDiagramInfo;
    lScalePerc: Integer;
  begin
  lFolderName := '';
  lScalePerc := 100;
  if PromptForFolder('Select destination', lFolderName) AND
     AskScalePercentage(lScalePerc)
     then begin

          // Iterate over diagrams
          for lDiagramInfo in fDiagrams do
             begin
             CloseActiveDiagram;
             OpenDiagram(lDiagramInfo, nil);

             lFileName := lFolderName+'\'+DeriveFilename(lDiagramInfo, 'png');
             Diagram.ExportToPng(lFileName, lScalePerc);
             end;

          // Open first diagram
          ActiveDiagramInfo := fDiagrams[0];

          ShowMessage('Export done.');
          end;
  end;

end.


