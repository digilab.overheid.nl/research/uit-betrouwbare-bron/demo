// -----------------------------------------------------------------------------
//
// ace.forms.Debug
//
// -----------------------------------------------------------------------------
//
// Console to display debug messages.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Forms.Debug;

interface

uses
  SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls, ExtCtrls;

type

  { TfDebug }

  TfDebug = class(TForm)
    pnlDebug: TPanel;
    pnlDebugHeader: TPanel;
    lDebug: TLabel;
    pnlDebugMemo: TPanel;
    mDebug: TMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fDebug: TfDebug = nil;

implementation

{$R *.lfm}

uses
  ace.Settings;

procedure TfDebug.FormClose(Sender: TObject; var Action: TCloseAction);
  begin
  Action := caFree;
  end;

procedure TfDebug.FormCreate(Sender: TObject);
  begin
  gSettings.LoadFormPosition(Self);
  end;

procedure TfDebug.FormDestroy(Sender: TObject);
  begin
  gSettings.SaveFormPosition(Self);

  fDebug := nil;
  end;

end.
