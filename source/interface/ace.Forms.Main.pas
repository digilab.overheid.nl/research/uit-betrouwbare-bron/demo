// -----------------------------------------------------------------------------
//
// ace.forms.Main
//
// -----------------------------------------------------------------------------
//
// Main form.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Forms.Main;

{$MODE Delphi}

interface

uses
  Classes, Generics.Collections, ActnList,
  Forms, Controls, ExtCtrls,
  Graphics, Menus,
  diagram.Model,
  ace.Core.Engine, ace.Forms.Console, ace.Forms.Transactions,
  ace.Forms.Model, ace.Forms.GraphEditor, ace.API.Server,
  SynEditHighlighter, SynHighlighterAny, Dialogs;

type

  { TfMain }

  TfMain = class(TForm)
    ApplicationProperties1: TApplicationProperties;
    Image1: TImage;
    MainMenu: TMainMenu;
    ActionList: TActionList;
    acExit: TAction;
    acOpenConsole: TAction;
    acShowTransactions: TAction;
    File1: TMenuItem;
    miShowTransactions: TMenuItem;
    miOpenConsole: TMenuItem;
    N1: TMenuItem;
    miFileExit: TMenuItem;
    acOpenProject: TAction;
    miFileOpen: TMenuItem;
    EditorHighLighter: TSynAnySyn;
    acShowModeller: TAction;
    miShowModeller: TMenuItem;
    acNewProject: TAction;
    miFileNew: TMenuItem;
    miConcurrency: TMenuItem;
    acShowGraphEditor: TAction;
    miShowGraphEditor: TMenuItem;
    acShowAPIServer: TAction;
    miShowAPIServer: TMenuItem;
    miShowScriptEditor: TMenuItem;
    miCore: TMenuItem;
    miAPI: TMenuItem;
    acShowScriptEditor: TAction;
    acShowInspector: TAction;
    miShowInspector: TMenuItem;
    acShowSampleUI: TAction;
    ShowSampleUI1: TMenuItem;
    procedure ApplicationProperties1Activate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acOpenConsoleExecute(Sender: TObject);
    procedure acShowTransactionsExecute(Sender: TObject);
    procedure acOpenProjectExecute(Sender: TObject);
    procedure acShowModellerExecute(Sender: TObject);
    procedure acNewProjectExecute(Sender: TObject);
    procedure acShowGraphEditorExecute(Sender: TObject);
    procedure acShowAPIServerExecute(Sender: TObject);
    procedure acShowScriptEditorExecute(Sender: TObject);
    procedure acShowInspectorExecute(Sender: TObject);
    procedure acShowSampleUIExecute(Sender: TObject);

  private
    { Private declarations }
    fActivated: boolean;
    fConsoles: TList<TfConsole>;
    fFocussedConsole: TfConsole;

    procedure CloseAll;
    function GetActiveFormByType(aType: TClass): TCustomForm;

    function SelectProject: string;
    procedure OpenProject(aFolder: string);
    function NewConsole: TfConsole;
    procedure LoadConsole(aFilename: string);
    procedure LoadScript(aFilename: string);

  public
    { Public declarations }

    function CreateProject: boolean; // Accessed by gSettings

    procedure ConsoleWillClose(aConsole: TfConsole);
    function HasMultipleConsoles: boolean;
    procedure ShowConsole(aSeqNr: integer);

    procedure ShowTutorialStep(aSeqNr: integer);

    procedure ShowTransactions;
    procedure ShowModel(aKind: TcdDiagramKind);
    procedure ShowScriptEditor;
    procedure ShowGraphEditor;
    procedure ShowSampleUI;
    procedure ShowAPIServer;
    procedure ShowInspector;
  end;

var
  fMain: TfMain;

implementation

{$R *.lfm}

uses
  SysUtils,
  common.Utils, common.FileUtils, common.ErrorHandling, common.InputQueryList,
  ace.Commands.Dispatcher,
  ace.Forms.ScriptEditor, ace.Forms.SampleUI,
  ace.Settings, ace.Forms.Inspector,
  ace.Forms.Tutorial,
  diagram.Settings;


// -- Create/Activate/Destroy

procedure TfMain.FormCreate(Sender: TObject);
  begin
  fActivated := false;
  fConsoles := TList<TfConsole>.Create;

  Caption := 'Atomic Claim Engine - v'+GetApplicationVersionStr;

  // Configure Editor HighLighter
  with EditorHighLighter do
    begin
    CommentAttri.Foreground := clWebDarkgreen;
    Comments := [csCStyle];
    KeyAttri.Foreground := clWebMidnightBlue; //clWebDarkSlategray; //clWebDarkBlue;
    KeyAttri.Style := [];
    end;

  gCommandDispatcher.AddKeywordsToHighLighter(EditorHighLighter);

  gSettings.LoadFormPosition(Self);
  end;

procedure TfMain.ApplicationProperties1Activate(Sender: TObject);
  var
    lFolder: string;
  begin
  if fActivated then exit;
  fActivated := true;

  if ParamCount>0
     then begin
          lFolder := TPath.Combine(gSettings.ProjectsFolder, ParamStr(1));
          // On linux an exception during starting gives a SEGFAULT
          {$ifdef windows}
          OpenProject(lFolder);
          {$else}
          try
            OpenProject(lFolder);
          except
            ShowmessageFmt('Cannot find folder: %s', [lFolder]);
            end;
          {$endif}
          end
     else NewConsole; // Open with empty console
  end;


procedure TfMain.FormDestroy(Sender: TObject);
  begin
  gSettings.SaveFormPosition(Self);

  fConsoles.Free;
  end;


// -- Handlers

procedure TfMain.CloseAll;
  var
    i: integer;
    lEngine: TcEngine;
  begin
  // Close all other forms
  for i := Screen.FormCount-1 downto 1 do
    if Screen.Forms[I].visible
       then Screen.Forms[I].Free;
  fConsoles.Clear;
  fFocussedConsole := nil;
  gSessionSeqNr := 0;

  lEngine := gSettings.Engine;
  lEngine.Free;
  lEngine := TcEngine.Create;
  gSettings.ProjectClosed;
  gSettings.Engine := lEngine;
  end;

function TfMain.GetActiveFormByType(aType: TClass): TCustomForm;
  var
    i: integer;
  begin
  result := nil;
  for i := 0 to Screen.FormCount-1 do
    if Screen.Forms[i] is aType
       then begin
            result := Screen.Forms[i];
            exit;
            end;
  end;

function TfMain.SelectProject: string;
  begin
  Result := '';
  if not Dialogs.SelectDirectory('Open Project', gSettings.ProjectsFolder,
           Result, False, 0)
     then exit;
  end;

procedure TfMain.OpenProject(aFolder: string);
  var
    lFilename: string;
  begin
  CloseAll;

  if not DirectoryExists(aFolder)
     then EUsr('Cannot find folder: %s', [aFolder]);

  gSettings.ProjectLoaded(aFolder);

  // Search for Console script
  lFilename := TPath.Combine(gSettings.ProjectFolder, 'console.txt');
  if FileExists(lFilename)
     then LoadConsole(lFilename);

  // Search for Pascal script
  lFilename := TPath.Combine(gSettings.ProjectFolder, 'script.pas');
  if FileExists(lFilename)
     then LoadScript(lFilename);
  end;

function TfMain.NewConsole: TfConsole;
  begin
  result := TfConsole.Create(Application);
  if fConsoles.Count = 0
    then gSettings.LoadFormPosition(result);

  result.Init(gSettings.Engine, fConsoles.Count=0 {First?});
  result.Show;
  fConsoles.Add(result);
  end;

procedure TfMain.LoadConsole(aFilename: string);
  const
    cConsoleStatement = '## Console ';
  var
    i: integer;
    lCompleteScript, lConsoleScript: TStringlist;
    lMultiConsoleScript: boolean;
    lNextConsole: boolean;
    lLine: string;
    lfConsole: TfConsole;
  begin
  lCompleteScript := TStringlist.Create;
  lConsoleScript := TStringlist.Create;
  try
    lCompleteScript.LoadFromFile(aFilename);

    if lCompleteScript.Count>0
       then begin
            lLine := lCompleteScript[0];
            lMultiConsoleScript := CompareText(Copy(lLine, 1, Length(cConsoleStatement)), cConsoleStatement)=0
            end
       else lMultiConsoleScript := false;

    if lMultiConsoleScript
       then begin
            for i := 1 to lCompleteScript.Count-1 do
              begin
              lLine := lCompleteScript[i];
              lNextConsole := CompareText(Copy(lLine, 1, Length(cConsoleStatement)), cConsoleStatement)=0;

              if lNextConsole
                 then begin
                      lfConsole := NewConsole;
                      lfConsole.LoadScript(lConsoleScript);
                      lConsoleScript.Clear;
                      end
                 else lConsoleScript.Add(lLine);
              end;

            if lConsoleScript.Count>0
               then begin
                    lfConsole := NewConsole;
                    lfConsole.LoadScript(lConsoleScript);
                    end;
            end
       else begin
            lfConsole := NewConsole;
            lfConsole.LoadScript(lCompleteScript);
            end;

  finally
    lCompleteScript.Free;
    lConsoleScript.Free;
    end;

  if HasMultipleConsoles
     then ShowConsole(1);
  end;

procedure TfMain.LoadScript(aFilename: string);
  var
    lScript: TStringlist;
    lfScriptEditor: TfScriptEditor;
  begin
  lScript := TStringlist.Create;
  try
    lScript.LoadFromFile(aFilename);

    lfScriptEditor := TfScriptEditor(GetActiveFormByType(TfScriptEditor));
    if not Assigned(lfScriptEditor)
       then lfScriptEditor := TfScriptEditor.Create(Application);
    lfScriptEditor.Show;
    lfScriptEditor.LoadScript(lScript);

  finally
    lScript.Free;
    end;
  end;

function TfMain.CreateProject: boolean;
  var
    lFolder: string;
  begin
  result := false;
  if Dialogs.SelectDirectory('Create or Select Project', gSettings.ProjectsFolder, lFolder)
     then begin
          gSettings.ProjectLoaded(lFolder);
          result := true;
          end;
  end;

procedure TfMain.ConsoleWillClose(aConsole: TfConsole);
  begin
  fConsoles.Remove(aConsole);

  if fConsoles.Count = 0
    then gSettings.SaveFormPosition(aConsole);
  end;

function TfMain.HasMultipleConsoles: boolean;
  begin
  result := fConsoles.Count>1;
  end;

procedure TfMain.ShowConsole(aSeqNr: integer);
  var
    i: integer;
    fConsole: TfConsole;
  begin
  i := aSeqNr - 1;
  if i<fConsoles.Count
     then begin
          fConsole := fConsoles[i];

          if Assigned(fFocussedConsole)
             then fFocussedConsole.HideVisualFocus;

          fFocussedConsole := fConsole;
          fConsole.ShowVisualFocus;
          end
     else EUsr('Invalid console sequence nr: %d', [aSeqNr]);
  end;

procedure TfMain.ShowTutorialStep(aSeqNr: integer);
  begin
  if not Assigned(fTutorial)
     then fTutorial := TfTutorial.Create(Application);
  fTutorial.Show;
  fTutorial.ShowStep(aSeqNr);
  end;

procedure TfMain.ShowTransactions;
  begin
  if not Assigned(fTransactions)
     then begin
          fTransactions := TfTransactions.Create(Application);
          fTransactions.Init(gSettings.Engine);
          end;
  fTransactions.Show;
  end;

procedure TfMain.ShowModel(aKind: TcdDiagramKind);
  begin
  if not Assigned(fModel)
     then begin
          // Diagrams can only be used in a 'known' project
          if not gSettings.AskProjectFolderWhenEmpty
             then exit;

          fModel := TfModel.Create(Application, gSettings.Engine, aKind);
          fModel.Init;
          end;
  fModel.Show;
  end;

procedure TfMain.ShowScriptEditor;
  begin
  if not Assigned(fScriptEditor)
     then fScriptEditor := TfScriptEditor.Create(Application);
  fScriptEditor.Show;
  end;

procedure TfMain.ShowGraphEditor;
  begin
  if not Assigned(fGraphEditor)
     then fGraphEditor := TfGraphEditor.Create(Application);
  fGraphEditor.Show;
  end;

procedure TfMain.ShowSampleUI;

  function AskClassName: TcClass;
    var
      i: integer;
      lClass: TcClass;
      lClasses: TStringlist;
      lClassName: String;
    begin
    result := nil;

    // Check if there are classes
    if gSettings.Engine.Classes.Count=0
       then EUsr('For this feature classes are required. Please group the claimmodel.');

    // Select class
    lClasses := TStringlist.Create;
    try
      for i := 0 to gSettings.Engine.Classes.Count-1 do
        begin
        lClass := gSettings.Engine.Classes[i];
        lClasses.Add(lClass.Name);
        end;

      lClassName := '';
      if not InputQueryList('Class selection', 'Select class to display', lClasses, true, lClassName)
         then exit;

      result := gSettings.Engine.Classes.Find(lClassName);
      Assert(Assigned(result));

    finally
      lClasses.Free;
      end;
    end;

  function AskInstance(aClass: TcClass): string;
    var
      cs: integer;
      lClaimSetIdentities: TStringlist;
    begin
    result := '';

    // Create list of instances (of the original claims)
    lClaimSetIdentities := TStringlist.Create;
    try
      for cs := 0 to aClass.MainClaimType.ClaimSetList.Count-1 do
        lClaimSetIdentities.Add(aClass.MainClaimType.ClaimSetList[cs].IdentityStr);

      // Check if there are instances
      if lClaimSetIdentities.Count=0
         then EUsr('For this feature instances are required.');

      if not InputQueryList('Instance selection', 'Select instance to display', lClaimSetIdentities, true, result)
        then Exit;

    finally
      lClaimSetIdentities.Free;
      end;
    end;

  var
    lClass: TcClass;
    lInstance: String;
  begin

  // Ask class and instance
  lClass := AskClassName;
  if not Assigned(lClass)
    then exit;

  lInstance := AskInstance(lClass);
  if lInstance = ''
    then exit;

  // Show Form
  fSampleUI := TfSampleUI.Create(Application);
  fSampleUI.Init(nil, lClass, lInstance);
  fSampleUI.Show;
  end;

procedure TfMain.ShowAPIServer;
  begin
  if not Assigned(fAPIServer)
     then fAPIServer := TfAPIServer.Create(Application);
  fAPIServer.Show;
  end;

procedure TfMain.ShowInspector;
  var
    lfInspector: TfInspector;
  begin
  lfInspector := TfInspector(GetActiveFormByType(TfInspector));
  if not Assigned(lfInspector)
     then begin
          lfInspector := TfInspector.Create(Application);
          lfInspector.Init(nil, nil, nil, false, false);
          end;
  lfInspector.Show;
  end;


// -- Interface binding

procedure TfMain.acNewProjectExecute(Sender: TObject);
  begin
  CloseAll;
  NewConsole;
  end;

procedure TfMain.acOpenProjectExecute(Sender: TObject);
  var
    lFolder: string;
  begin
  lFolder := SelectProject;
  if lFolder <> ''
    then OpenProject(lFolder);
  end;

procedure TfMain.acExitExecute(Sender: TObject);
  begin
  Close;
  end;

procedure TfMain.acShowModellerExecute(Sender: TObject);
  begin
  ShowModel(dkUnknown);
  end;

procedure TfMain.acShowSampleUIExecute(Sender: TObject);
  begin
  ShowSampleUI;
  end;

procedure TfMain.acShowScriptEditorExecute(Sender: TObject);
  begin
  ShowScriptEditor;
  end;

procedure TfMain.acShowTransactionsExecute(Sender: TObject);
  begin
  ShowTransactions;
  end;

procedure TfMain.acShowGraphEditorExecute(Sender: TObject);
  begin
  ShowGraphEditor;
  end;

procedure TfMain.acShowInspectorExecute(Sender: TObject);
  begin
  ShowInspector;
  end;

procedure TfMain.acShowAPIServerExecute(Sender: TObject);
  begin
  ShowAPIServer;
  end;

procedure TfMain.acOpenConsoleExecute(Sender: TObject);
  begin
  NewConsole;
  end;

end.

