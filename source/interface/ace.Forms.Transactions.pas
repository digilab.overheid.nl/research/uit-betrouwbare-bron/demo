// -----------------------------------------------------------------------------
//
// ace.Forms.Transactions
//
// -----------------------------------------------------------------------------
//
// Form to inspect transactions and all (beware!) claimsets in the engine.
// Used to debug concurrency.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Forms.Transactions;

{$MODE Delphi}

interface

uses
  SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, ExtCtrls, ComCtrls, ActnList,
  common.RadioStation, ace.Core.Engine,
  diagram.Core, diagram.Transaction, diagram.ClaimSets;

type
  TfTransactions = class(TForm)
    pnlMain: TPanel;
    pnlDiagram: TPanel;
    pnlScrollBoxTransactions: TPanel;
    pnlClaimSets: TPanel;
    pnlScrollboxClaimSets: TPanel;
    splitter: TSplitter;
    pnlLegend: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure pnlScrollBoxTransactionsResize(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
    ftrScrollBox: TcdScrollBox;
    ftrDiagram: TcdDiagram;
    fTransactionDiagram: TcdTransactionDiagram;

    fcsScrollBox: TcdScrollBox;
    fcsDiagram: TcdDiagram;
    fClaimSetsDiagram: TcdClaimSetsDiagram;

    fEngine: TcEngine;

    fRadioOnline: boolean;
    procedure Display;
    
  public
    { Public declarations }
    procedure Init(aEngine: TcEngine);

    procedure ListenToRadio (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);

    property Engine: TcEngine read fEngine;

    property trScrollBox: TcdScrollBox read ftrScrollBox;
    property trDiagram: TcdDiagram read ftrDiagram;
    property TransactionDiagram: TcdTransactionDiagram read fTransactionDiagram;

    property csScrollBox: TcdScrollBox read fcsScrollBox;
    property csDiagram: TcdDiagram read fcsDiagram;
    property ClaimSetsDiagram: TcdClaimSetsDiagram read fClaimSetsDiagram;
  end;

var
  fTransactions: TfTransactions;

implementation

{$R *.lfm}

uses
  common.ErrorHandling, ace.Settings;

procedure TfTransactions.FormCreate(Sender: TObject);
  begin
  gSettings.LoadFormPosition(Self);

  // Create custom components at runtime

  // Transactions
  ftrScrollBox := TcdScrollBox.Create(Self);
  with ftrScrollBox do
    begin
    Left := 0; Top := 30;
    BorderStyle := bsNone;
    Align := alClient;
    Color := clWindow;
    HorzScrollBar.Visible := false;
    VertScrollBar.Visible := false;
    end;
  ftrScrollBox.Parent := pnlScrollBoxTransactions;
  // Patch. Align := alCLient doesn't work.
  ftrScrollBox.Width := pnlScrollBoxTransactions.Width-4;
  ftrScrollBox.Height := pnlScrollBoxTransactions.Height;
  ftrDiagram := TcdDiagram.Create(trScrollBox, 'Default');

  // ClaimSets
  fcsScrollBox := TcdScrollBox.Create(Self);
  with fcsScrollBox do
    begin
    Left := 0; Top := 30;
    BorderStyle := bsNone;
    Align := alClient;
    Color := clWindow;
    HorzScrollBar.Visible := true;
    VertScrollBar.Visible := true;
    end;
  fcsScrollBox.Parent := pnlScrollBoxClaimSets;
  // Patch. Align := alCLient doesn't work.
  fcsScrollBox.Width := pnlScrollBoxClaimSets.Width-4;
  fcsScrollBox.Height := pnlScrollBoxClaimSets.Height;
  fcsDiagram := TcdDiagram.Create(csScrollBox, 'Default');
//  fcsDiagram.AllowPaperChange := False;

  gRadioStation.AddRadio(Self, ListenToRadio);
  fRadioOnline := true;
  end;

procedure TfTransactions.FormDestroy(Sender: TObject);
  begin
  gSettings.SaveFormPosition(Self);

  if fRadioOnline
    then gRadioStation.RemoveRadio(Self);
  fTransactions := nil;
  end;

procedure TfTransactions.FormClose(Sender: TObject; var Action: TCloseAction);
  begin
  Action := caFree;
  end;

procedure TfTransactions.Init(aEngine: TcEngine);
  begin
  if Assigned(fEngine) then EInt('TfTransactions.Init', 'Already initialized');

  fEngine := aEngine;
  Display;
  end;

procedure TfTransactions.ListenToRadio (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);
  begin
  if aRadioEvent = cRadioEvent_Offline
    then begin
         fRadioOnline := false;
         exit;
         end;

  if (aRadioEvent = cRadioEvent_Transaction_Start) or
     (aRadioEvent = cRadioEvent_Transaction_AfterEnd) or // Listen to before end to see last transaction (ghost)
     (aRadioEvent = cRadioEvent_Transaction_Selected)
     then Display;
  end;

procedure TfTransactions.Display;
  begin
  // Display Transactions
  fTransactionDiagram := nil;
  trDiagram.Clear;
  fTransactionDiagram := TcdTransactionDiagram.Create(Engine, trDiagram);
  trDiagram.Add(fTransactionDiagram, 0, 0);
  fTransactionDiagram.ResetBounds;

  // Display ClaimSets
  fClaimSetsDiagram := nil;
  csDiagram.Clear;
  fClaimSetsDiagram := TcdClaimSetsDiagram.Create(Engine, csDiagram);
  csDiagram.Add(fClaimSetsDiagram, 0, 0);
  fClaimSetsDiagram.ResetBounds;

  // Patch. Align := alCLient doesn't work.
  fcsScrollBox.Width := pnlScrollBoxClaimSets.Width-4;
  fcsScrollBox.Height := pnlScrollBoxClaimSets.Height;
  end;


// -- Interface handlers

procedure TfTransactions.pnlScrollBoxTransactionsResize(Sender: TObject);
  begin
  // Patch. Align := alCLient doesn't work.
  ftrScrollBox.Width := pnlScrollBoxTransactions.Width-4;
  ftrScrollBox.Height := pnlScrollBoxTransactions.Height;
  if Assigned(TransactionDiagram)
    then TransactionDiagram.ResetBounds;

  // Patch. Align := alCLient doesn't work.
  fcsScrollBox.Width := pnlScrollBoxClaimSets.Width-4;
  fcsScrollBox.Height := pnlScrollBoxClaimSets.Height;
  if Assigned(ClaimSetsDiagram)
    then ClaimSetsDiagram.ResetBounds;
  end;

end.
