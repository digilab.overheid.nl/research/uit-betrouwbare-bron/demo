// -----------------------------------------------------------------------------
//
// ace.Command.List
//
// -----------------------------------------------------------------------------
//
// Commands to list (display in console) ClaimTypes and their Claims.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Commands.List;

interface

uses
  Classes, SysUtils,
  ace.Core.Engine,
  ace.Commands.Dispatcher;

type
  TchList =
    class (TcCommandHandler)
      private
        procedure List_DataTypes;
        procedure List_LabelTypes;
        procedure List_LabelType(aIndent, aName: string);
        procedure List_ClaimTypes(aDataOnly: boolean);
        procedure List_ClaimType(aIndent, aName: String; aDataOnly: Boolean);

      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
        function Match(aCommand: String): boolean; override;
      end;

implementation

uses
  common.Utils,
  ace.Core.Query;

{ TchList }

constructor TchList.Create;
  begin
  inherited Create(ceVisualisation);
  Name := 'list';
  MatchOperator := moCustom;

  SetHelpText(
    'list scalars' + cLineBreak +
    '------------' + cLineBreak +
    'Lists all scalars defined in the register.' + cLineBreak +
    'Note: DataTypes cannot be defined manually.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'list scalars' + cLineBreak +
    '' + cLineBreak +

    'list labelTypes' + cLineBreak +
    '---------------' + cLineBreak +
    'Lists all label types defined in the register.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'list labelTypes' + cLineBreak +
    '' + cLineBreak +

    'list claimTypes' + cLineBreak +
    '---------------' + cLineBreak +
    'Lists all claim types defined in the register.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'list claimTypes' + cLineBreak +
    '' + cLineBreak +

    'list claimType' + cLineBreak +
    '--------------' + cLineBreak +
    'Lists the details of a claim type.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'list <name>' + cLineBreak +
    '' + cLineBreak +
    'Optional parameters:' + cLineBreak +
    '  data  // Only displays data, no type information'
  );

  Keywords.Add('list');
  Keywords.Add('scalars');
  Keywords.Add('labeltypes');
  Keywords.Add('claimtypes');
  Keywords.Add('data');
  AutoComplete.Add('list DataTypes');
  AutoComplete.Add('list LabelTypes');
  AutoComplete.Add('list ClaimTypes');
  AutoComplete.Add('list ');
  end;

function TchList.Match(aCommand: String): boolean;
  var
    lName: string;
  begin
  result := false;

  // Starts with 'list ' ?
  lName := Name + ' ';
  if (CompareText(Copy(aCommand, 1, Length(lName)), lName)=0)
    then begin
         result := true;
         exit;
         end;
  end;

procedure TchList.iExecute;
  var
    lStr, lName: string;
    lDataOnly: boolean;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command']);

  if (CompareText(Tokens[0], 'list DataTypes')=0) or (CompareText(Tokens[0], 'lsc')=0)
    then List_DataTypes
  else if (CompareText(Tokens[0], 'list LabelTypes')=0) or (CompareText(Tokens[0], 'llt')=0)
    then List_LabelTypes
  else if (CompareText(Tokens[0], 'list ClaimTypes')=0) or (CompareText(Tokens[0], 'lct')=0)
    then begin
         lDataOnly := Params.IndexOfName('data')>=0;
         List_claimTypes(lDataOnly);
         end
    else begin
         // List <claimType>
         SplitString(Tokens[0], ' ', lStr, lName);
         lDataOnly := Params.IndexOfName('data')>=0;
         if lName=''
           then ParamMissing('name')
           else List_ClaimType('', lName, lDataOnly);
         end;
  end;
 
procedure TchList.List_DataTypes;
  var
    i: integer;
    lDataType: TcDataType;
    lIndent: string;
  begin
  lIndent := cIndent;
  Response.Add('DataTypes:');
  for i := 0 to Engine.DataTypes.Count-1 do
    begin
    lDataType := Engine.DataTypes[i];
    Response.Add(lIndent + Format('%s%d %s', [cRefChar, lDataType.ID, lDataType.Name]));
    end;
  Response.Add('');
  end;

procedure TchList.List_LabelTypes;
  var
    i: integer;
    lType: TcType;
  begin
  Response.Add('LabelTypes:');
  for i := 0 to Engine.ConceptualTypes.Count-1 do
    begin
    lType := Engine.ConceptualTypes[i];
    if lType is TcLabeltype
      then List_LabelType(cIndent, lType.Name);
    end;
  Response.Add('');
  end;

procedure TchList.List_ClaimTypes(aDataOnly: boolean);
  var
    i: integer;
    lType: TcType;
  begin
  Response.Add('ClaimTypes:');
  for i := 0 to Engine.ConceptualTypes.Count-1 do
    begin
    lType := Engine.ConceptualTypes[i];
    if lType is TcClaimtype
      then List_ClaimType(cIndent, lType.Name, aDataOnly);
    end;
  Response.Add('');
  end;
 
procedure TchList.List_LabelType(aIndent, aName: String);
  var
    lLabelType: TcLabelType;
  begin
  lLabelType := Engine.ConceptualTypes.FindLabelType(aName);
  Response.Add(aIndent + Format('%s%d %s (%s)', [cRefChar, lLabelType.ID, lLabelType.Name, lLabelType.DataType.Name]));
  end;

procedure TchList.List_ClaimType(aIndent, aName: String; aDataOnly: boolean);

  function Indent(aLevel: integer): string;
    var
      i: integer;
    begin
    result := aIndent;
    for i := 0 to aLevel do
      result := result + cIndent;
    end;

  var
    s, c, a: integer;
    lClaimType: TcClaimType;
    lStrs: TStringlist;
    lRole: TcRole;
    lClaimSet: TcClaimSet;
    lClaim: TcClaim;
    lAnnotation: TcAnnotation;
    lQuery: TcTemporalQuery;
  begin
  lStrs := TStringlist.Create;
  try
    lClaimType := Engine.ConceptualTypes.FindClaimType(aName);
    if not Assigned(lClaimType)
      then TypeUnknown('ClaimType', aName);

    Response.Add(Indent(0) + Format('%s%d: %s', [cRefChar, lClaimType.ID, lClaimType.Name]));

    if not aDataOnly
       then begin
            // Templates
            Response.Add(Indent(1) + 'ExpressionTemplates:');
            if Assigned(lClaimType.ExpressionTemplate)
               then Response.Add(Indent(2) + Format('Claim: %s', [lClaimType.ExpressionTemplate.Template]));
            if Assigned(lClaimType) and Assigned(lClaimType.NestedExpressionTemplate)
               then Response.Add(Indent(2) + Format('Object: %s', [lClaimType.NestedExpressionTemplate.Template]));

            // Roles
            Response.Add(Indent(1) + 'Roles:');
            for s := 0 to lClaimType.Roles.Count-1 do
              begin
              lRole := lClaimType.Roles[s];
              Response.Add(Indent(2) + Format('%s%d: %s (%s - %s)', [
                cRefChar, lRole.ID, lRole.Name, lRole.Type_.Name,
                Copy(lRole.Type_.ClassName, 3, Length(lRole.Type_.ClassName))]));
              end;

            // Identity
            Response.Add(Indent(1) + 'Identity:');
            lStrs.Clear;
            for s := 0 to lClaimType.Identity.Count-1 do
              lStrs.Add(lClaimType.Identity[s].Name);
            Response.Add(Indent(2) + Format('%s%d: %s', [cRefChar, lClaimType.Identity.ID, StringlistToString(lStrs, ', ')]));

            // Time aspects
            Response.Add(Indent(1) + 'Time aspects:');
            Response.Add(Indent(2) + Format('Allow change: %s', [BooleanToYesNo(lClaimType.AllowChange)]));
            Response.Add(Indent(2) + Format('Timeline interpretation: %s', [BooleanToYesNo(lClaimType.TimelineInterpretation)]));
            Response.Add(Indent(2) + Format('Record transaction time: %s', [BooleanToYesNo(lClaimType.RecordTransactionTime)]));
            Response.Add(Indent(2) + Format('Keep expired: %s', [BooleanToYesNo(lClaimType.KeepExpired)]));
            Response.Add(Indent(2) + Format('Record valid time: %s', [BooleanToYesNo(lClaimType.RecordValidTime)]));
            Response.Add(Indent(2) + Format('Keep ended: %s', [BooleanToYesNo(lClaimType.KeepEnded)]));
            end;

    // ClaimSets
    Response.Add(Indent(1) + 'ClaimSets:');
    if lClaimType.Name = cMetaClaimType_Claim
       then begin
            Response.Add(Indent(2) + 'Contains every Claim from every ClaimType.');
            end
    else if Assigned(lClaimType)
       then begin
            for s := 0 to lClaimType.ClaimSetList.Count-1 do
              begin
              lClaimSet := lClaimType.ClaimSetList[s];
              Response.Add(Indent(2) + Format('Set: %s%d - Identity: %s', [cRefChar, lClaimSet.ID, lClaimSet.IdentityStr]));

              lQuery := TcTemporalQuery.Create(Session);
              lQuery.AssignClaimSet(lClaimSet);
              lQuery.Execute;

              Response.Add(Indent(2) + 'Source Claims:');
              for c := 0 to lClaimSet.SourceClaims.Count-1 do
                begin
                lClaim := lClaimSet.SourceClaims[c];
                if not lQuery.InTransaction(lClaim)
                   then continue;

                Response.Add(Indent(3) + Format('%s%d: %s', [cRefChar, lClaim.ID, lClaim.GetExpression]));
                Response.Add(Indent(4) + Format('%s', [lClaim.GetTimeAspects]));

                if lClaim.Annotations.Count>0
                   then begin
                        Response.Add(Indent(5) + 'Annotations');
                        for a := 0 to lClaim.Annotations.Count-1 do
                          begin
                          lAnnotation := lClaim.Annotations[a];
                          Response.Add(Indent(6) + Format('%s%d: %s', [cRefChar, lAnnotation.ID, lAnnotation.GetExpression]));
                          Response.Add(Indent(7) + Format('%s', [lAnnotation.GetTimeAspects]));
                          end;
                        end;
                end;

              Response.Add(Indent(2) + 'Derived Claims:');
              for c := 0 to lClaimSet.DerivedClaims.Count-1 do
                begin
                lClaim := lClaimSet.DerivedClaims[c];
                if not lQuery.InTransaction(lClaim)
                   then continue;

                Response.Add(Indent(3) + Format('%s%d: %s', [cRefChar, lClaim.ID, lClaim.GetExpression]));
                Response.Add(Indent(4) + Format('%s', [lClaim.GetTimeAspects]));

                if lClaim.Annotations.Count>0
                   then begin
                        Response.Add(Indent(5) + 'Annotations');
                        for a := 0 to lClaim.Annotations.Count-1 do
                          begin
                          lAnnotation := lClaim.Annotations[a];
                          Response.Add(Indent(6) + Format('%s%d: %s', [cRefChar, lAnnotation.ID, lAnnotation.GetExpression]));
                          Response.Add(Indent(7) + Format('%s', [lAnnotation.GetTimeAspects]));
                          end;
                        end;
                end;
              end;
            end;

  finally
    lStrs.Free;
    end;
  end;


initialization
  // Register handlers
  gCommandDispatcher.Add(TchList.Create);
end.

