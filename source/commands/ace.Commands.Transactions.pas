// -----------------------------------------------------------------------------
//
// ace.Command.Operations
//
// -----------------------------------------------------------------------------
//
// Commands to work with transactions and operations.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Commands.Transactions;

interface

uses
  Classes, SysUtils,
  ace.Core.Engine,
  ace.Commands.Dispatcher;

type
  TchStartTransaction =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchStartOperation =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchCommit =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchRollBack =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;


implementation

uses
  common.Utils;


{ TchStartTransaction }

constructor TchStartTransaction.Create;
  begin
  inherited Create(ceTransaction);
  Name := 'start Transaction';
  MatchOperator := moFull;

  SetHelpText(
    'start Transaction' + cLineBreak +
    '-----------------' + cLineBreak +
    'Starts a Transaction.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'start Transaction' + cLineBreak +
    '' + cLineBreak +
    'Optional parameters:' + cLineBreak +
    '  readonly ' + cLineBreak +
    '  stable // See explanation below ' + cLineBreak +
    '' + cLineBreak +
    '// Stable: Will use a stable snapshot. As long as the transactional ' + cLineBreak +
    '// timeline is not violated, stable snapshot will never changed. ' + cLineBreak +
    '// The drawback: Can only be readonly. And the data will be retrieved ' + cLineBreak +
    '// from a moment before the oldest active write transaction.'
  );

  Keywords.Add('start');
  Keywords.Add('transaction');
  Keywords.Add('readonly');
  Keywords.Add('stable');
  AutoComplete.Add('start Transaction'#13#10'  readonly'#13#10'  stable');
  end;

procedure TchStartTransaction.iExecute;
  var
    lReadOnly, lStable: boolean;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command', 'readonly', 'stable']);

  lReadOnly := Params.IndexOfName('readonly')>=0;
  lStable := Params.IndexOfName('stable')>=0;

  if lStable and not lReadOnly
     then GeneralError('Stable transactions must be readonly.', []);

  if Session.InOperation
     then Response.Add('Transaction active.')
     else begin
          Session.StartTransaction(false {not implicit}, lReadOnly, lStable);
          Response.Add(Format('Transaction %s%d started.', [cRefChar, Session.Transaction.ID]));
          if lStable
             then begin
                  Response.Add(Format('  Stable snapshot ID: %d.', [Session.Transaction.StableReadSnapshotID]));
                  Response.Add(Format('  Stable snapshot time: T%d.', [Session.Transaction.StableReadSnapshotTime]));
                  Response.Add('  All currenty active transactions (if any) started after this moment.');
                  end;
          end;
  end;


{ TchStartOperation }

constructor TchStartOperation.Create;
  begin
  inherited Create(ceTransaction);
  Name := 'start Operation';
  MatchOperator := moFull;

  SetHelpText(
    'start Operation' + cLineBreak +
    '---------------' + cLineBreak +
    'Starts an Operation.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'start Operation: <name>' + cLineBreak +
    '' + cLineBreak +
    'Optional parameters:' + cLineBreak +
    '  from: <time>' + cLineBreak +
    '  until: <time>'
  );

  Keywords.Add('start');
  Keywords.Add('operation');
  Keywords.Add('from');
  Keywords.Add('until');
  AutoComplete.Add('start Operation');
  end;

procedure TchStartOperation.iExecute;
  var
    lName: string;
    lValidFrom, lValidUntil: TcTime;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command : name') then SyntaxError;
  CheckParams(['command', 'name', 'from', 'until']);

  lName := Params.Values['name'];

  if Session.InOperation
     then begin
          Response.Add('Operation already active.');
          exit;
          end;

  Session.StartOperation(lName);

  if Params.IndexOfName('from')>=0
    then begin
         lValidFrom := StrToIntDef(Params.Values['from'], -1);
         if lValidFrom<0
            then GeneralError('From parameter specifies an invalid time: %s', [Params.Values['from']]);
         end
    else lValidFrom := cNoTime;
  Session.Operation.ValidFrom := lValidFrom;

  if Params.IndexOfName('until')>=0
    then begin
         lValidUntil := StrToIntDef(Params.Values['until'], -1);
         if lValidUntil<0
            then GeneralError('Until parameter specifies an invalid time: %s', [Params.Values['until']]);
         end
    else lValidUntil := cNoTime;
  Session.Operation.ValidUntil := lValidUntil;

  Response.Add(Format('Operation %s%d started.', [cRefChar, Session.Operation.ID]))
  end;


{ TchCommit }

constructor TchCommit.Create;
  begin
  inherited Create(ceTransaction);
  Name := 'commit';
  MatchOperator := moFull;

  SetHelpText(
    'commit' + cLineBreak +
    '------' + cLineBreak +
    'Ends a Transaction or Operation. Any changes that were made will be saved.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'commit'
  );

  Keywords.Add('commit');
  AutoComplete.Add('commit');
  end;

procedure TchCommit.iExecute;
  var
    lTransactionID: integer;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command']);

  if Session.InTransaction
     then begin
          lTransactionID := Session.Transaction.ID;
          Session.Commit;
          Response.Add(Format('Transaction %s%d committed.', [cRefChar, lTransactionID]))
          end
     else begin
          Response.Add('No transaction active.')
          end;
  end;


{ TchRollback }

constructor TchRollback.Create;
  begin
  inherited Create(ceTransaction);
  Name := 'rollback';
  MatchOperator := moFull;

  SetHelpText(
    'rollback' + cLineBreak +
    '--------' + cLineBreak +
    'Ends a Transaction or Operation. Any changes that were made will be undone.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'rollback'
  );

  Keywords.Add('rollback');
  AutoComplete.Add('rollback');
  end;

procedure TchRollback.iExecute;
  var
    lTransactionID: integer;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command']);

  if Session.InTransaction
     then begin
          lTransactionID := Session.Transaction.ID;
          Session.Rollback;
          Response.Add(Format('Transaction %s%d rolled back.', [cRefChar, lTransactionID]))
          end
     else begin
          Response.Add('No transaction active.')
          end;
  end;


initialization
  // Register handlers
  gCommandDispatcher.Add(TchStartTransaction.Create);
  gCommandDispatcher.Add(TchStartOperation.Create);
  gCommandDispatcher.Add(TchCommit.Create);
  gCommandDispatcher.Add(TchRollback.Create);
end.
