// -----------------------------------------------------------------------------
//
// ace.Command.Misc
//
// -----------------------------------------------------------------------------
//
// Miscellaneous commands.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Commands.Misc;

interface

uses
  Classes, SysUtils,
  ace.Commands.Dispatcher, ace.Core.Engine;

type
  TchHelp =
    class (TcCommandHandler)
      private
        procedure DisplayHelp;
        procedure DisplayHelpCommand(aName: string);

      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
        function Match(aCommand: String): boolean; override;
      end;

  TchShortCuts =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchAbout =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchWait =
    class (TcCommandHandler)
      public
        constructor Create; reintroduce;
      end;

  TchTutorialStep =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchConsole =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchStartAPI =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

implementation

uses
  Forms, common.Utils,
  diagram.Model,
  ace.Forms.Main, ace.Forms.Console, ace.API.Server;

{ TchHelp }

constructor TchHelp.Create;
  begin
  inherited Create(ceApplication);
  Name := 'help';
  MatchOperator := moCustom;
  ShowInHelp := false;

  AutoComplete.Add('help ');
  end;

function TchHelp.Match(aCommand: String): boolean;
  begin
  // Starts with 'help' ?
  // Note: Inherited checks for 'help '. That doesn't match the simple 'help' command.
  result := (CompareText(Copy(aCommand, 1, Length(Name)), Name)=0);
  end;

procedure TchHelp.iExecute;
  var
    lStr, lName: string;
  begin
  inherited;

  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command']);

  if CompareText(Tokens[0], Name)=0
    then DisplayHelp
    else begin
         // List <command>
         SplitString(Tokens[0], ' ', lStr, lName);
         if lName=''
           then ParamMissing('name')
           else DisplayHelpCommand(lName);
         end;
  end;

procedure TchHelp.DisplayHelp;
  var
    i: integer;
    lHandler: TcCommandHandler;
  begin
  Response.Add('Commands:');
  for i := 0 to gCommandDispatcher.Count-1 do
    begin
    lHandler := gCommandDispatcher[i];
    if lHandler.ShowInHelp
      then Response.Add(cIndent + lHandler.Name);
    end;
  Response.Add('');
  Response.Add('Type ''help <command>'' for information about a command.');
  end;

procedure TchHelp.DisplayHelpCommand(aName: string);
  var
    i: integer;
    lHandler: TcCommandHandler;
  begin
  lHandler := gCommandDispatcher.Find(aName);
  if not Assigned(lHandler) then CommandUnknown(aName);

  if lHandler.ShowInHelp
    then begin
         for i := 0 to lHandler.HelpText.Count-1 do
           Response.Add(lHandler.HelpText[i]);
         end
    else Response.Add(Format('No help available for command %s', [aName]));
  end;


{ TchShortCuts }

constructor TchShortCuts.Create;
  begin
  inherited Create(ceApplication);
  Name := 'shortcuts';
  MatchOperator := moFull;
  ShowInHelp := false;

  AutoComplete.Add('shortcuts');
  end;

procedure TchShortCuts.iExecute;
  begin
  inherited;

  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command']);

  Response.Add('Main window:');
  Response.Add('See menu options for shortcuts.');
  Response.Add('');
  Response.Add('Console:');
  Response.Add('Ctrl + L            Load Script');
  Response.Add('Ctrl + Enter        Execute Script');
  Response.Add('Ctrl + Alt + I      Clear Input');
  Response.Add('Ctrl + Alt + O      Clear Output');
  Response.Add('Ctrl + P            Previous from Script history');
  Response.Add('Ctrl + N            Next from Script history');
  Response.Add('Ctrl + H            Hide/Show Controls');
  Response.Add('');
  Response.Add('Script Editor:');
  Response.Add('Ctrl + Enter        Compile and Save');
  Response.Add('Ctrl + S            Save');
  Response.Add('');
  Response.Add('Inspector:');
  Response.Add('Ctrl + H            Hide/Show Controls');
  end;


{ TchAbout }

constructor TchAbout.Create;
  begin
  inherited Create(ceApplication);
  Name := 'about';
  MatchOperator := moFull;
  ShowInHelp := false;

  AutoComplete.Add('about');
  end;

procedure TchAbout.iExecute;
  var
    i: integer;
  begin
  inherited;

  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command']);

  // Reuse introtext that is displayed in the main form
  for i := 0 to gIntroText.Count-1 do
    Response.Add(gIntroText[i]);
  end;


{ TchWait }

constructor TchWait.Create;
  begin
  inherited Create(ceApplication);

  // Wait will never be executed since the command is captured and handled by the main form.
  // This command was only made to show help text about the command.
  Name := 'wait';
  MatchOperator := moCustom; // Not implemented, so there will never be a match

  SetHelpText(
    'wait' + cLineBreak +
    '--------------' + cLineBreak +
    'The wait command can be used to interrupt the execution of a script.' + cLineBreak +
    'This allows inspection of the results before continuing.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'wait'
  );

  Keywords.Add('wait');
  AutoComplete.Add('wait');
  end;


{ TchTutorialStep }

constructor TchTutorialStep.Create;
  begin
  inherited Create(ceApplication);
  Name := 'tutorial step';
  MatchOperator := moFull;

  SetHelpText(
    'tutorial step' + cLineBreak +
    '-------------' + cLineBreak +
    'Displays the specified step of the tutorial.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'tutorial step: <sequence nr>' + cLineBreak +
    '' + cLineBreak +
    'Tutorial are stored in the ''tutorial'' subfolder in a project. ' + cLineBreak +
    'The folder should contain images with filenames ''Slide<sequence nr>.png'' ' + cLineBreak +
    'These images should be scalable down to 600 x 800 (width x height). '
  );

  Keywords.Add('tutorial');
  Keywords.Add('step');
  AutoComplete.Add('tutorial step');
  end;

procedure TchTutorialStep.iExecute;
  var
    lSeqNr: integer;
  begin
  inherited;

  if not MatchPattern('command: seqnr') then SyntaxError;
  CheckParams(['command','seqnr']);
  CheckRequiredParams(['seqnr']);

  lSeqNr := StrToIntDef(Params.Values['seqnr'], -1);
  if lSeqNr = -1
     then Response.Add(Format('Invalid tutorial sequence nr: %s.', [Params.Values['seqnr']]))
     else fMain.ShowTutorialStep(lSeqNr);
  end;


{ TchConsole }

constructor TchConsole.Create;
  begin
  inherited Create(ceApplication);

  Name := 'console';
  MatchOperator := moFull;

  SetHelpText(
    'console' + cLineBreak +
    '-------' + cLineBreak +
    'Activates the specified console.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'console: <sequence nr>'
  );

  Keywords.Add('console');
  AutoComplete.Add('Console ');
  end;

procedure TchConsole.iExecute;
  var
    lSeqNr: integer;
  begin
  inherited;

  if not MatchPattern('command: seqnr') then SyntaxError;
  CheckParams(['command','seqnr']);
  CheckRequiredParams(['seqnr']);

  lSeqNr := StrToIntDef(Params.Values['seqnr'], -1);
  if lSeqNr = -1
     then Response.Add(Format('Invalid console sequence nr: %s.', [Params.Values['seqnr']]))
     else fMain.ShowConsole(lSeqNr);
  end;


{ TchStartAPI }

constructor TchStartAPI.Create;
  begin
  inherited Create(ceApplication);

  Name := 'start API';
  MatchOperator := moFull;

  SetHelpText(
    'start API' + cLineBreak +
    '---------' + cLineBreak +
    'Starts the specified API.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'start API: <identifier>'
  );

  Keywords.Add('start');
  Keywords.Add('API');
  AutoComplete.Add('start API: |');
  end;

procedure TchStartAPI.iExecute;
  var
    lAPIIdentifier: string;
  begin
  inherited;

  if not MatchPattern('command: identifier') then SyntaxError;
  CheckParams(['command','identifier']);
  CheckRequiredParams(['identifier']);

  lAPIIdentifier := Params.Values['identifier'];
  fMain.ShowAPIServer;
  Application.ProcessMessages;
  fAPIServer.StartAPI(lAPIIdentifier);
  end;

initialization
  // Register handlers
  gCommandDispatcher.Add(TchHelp.Create);
  gCommandDispatcher.Add(TchShortCuts.Create);
  gCommandDispatcher.Add(TchAbout.Create);
  gCommandDispatcher.Add(TchWait.Create);
  gCommandDispatcher.Add(TchTutorialStep.Create);
  gCommandDispatcher.Add(TchConsole.Create);
  gCommandDispatcher.Add(TchStartAPI.Create);
end.
