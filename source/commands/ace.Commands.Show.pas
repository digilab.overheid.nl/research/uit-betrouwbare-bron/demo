// -----------------------------------------------------------------------------
//
// ace.Command.Show
//
// -----------------------------------------------------------------------------
//
// Commands to visualise ClaimSets (temporal diagram).
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Commands.Show;

interface

uses
  Classes, SysUtils,
  ace.Core.Engine,
  ace.Commands.Dispatcher;

type
  TchShow =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

implementation

uses
  common.Utils,
  ace.Forms.Main, diagram.Model, ace.Forms.Inspector, Forms;

{ TchShow }

constructor TchShow.Create;
  begin
  inherited Create(ceVisualisation);
  Name := 'show';
  MatchOperator := moStart;

  SetHelpText(
    'show' + cLineBreak +
    '----' + cLineBreak +
    'The command can take multiple forms:' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'show <window>' + cLineBreak +
    '' + cLineBreak +
    'where <window> can be:' + cLineBreak +
    '- inspector        -- Shows inspector that visualises Operations, Claims and Annotations.' +
    '- transactions     -- Shows a window to monitor Transactions and view the contents of the Engine.' +
    '- model            -- Shows a window to create diagrams of the types in the Engine.' +
    '                      Specify the type as a param on the second line (type: <type>)' +
    '                      where <type> can be ''AtomicTypes'' or ''Classes''.' +
    '- scriptEditor     -- Shows the Pascal Script Editor.' +
    '- apiServer        -- Shows the API Server.' +
    '- graphEditor      -- Shows the Graph Editor.' +
    '- sampleUI         -- Shows the sample UI.' +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'show <AtomicType>  -- Shows inspector that visualises the specified Atomic Type.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'show <ClaimSetID>  -- Shows inspector that visualises the specified ClaimSet.' + cLineBreak +
    '' + cLineBreak +
    'Optional parameters:' + cLineBreak +
    '  redrawDuringTransactions  -- Only applicable to inspector' + cLineBreak +
    '  minimized                 -- Only applicable to inspector' + cLineBreak +
    '  type: <type>              -- Only applicable to model.'
  );

  Keywords.Add('show');
  Keywords.Add('minimized');
  Keywords.Add('redrawDuringTransactions');
  AutoComplete.Add('show ');
  end;

procedure TchShow.iExecute;
  var
    lStr, lIdentifier, lType: string;
    lID: integer;
    lClaimType: TcClaimType;
    lClaimSet: TcClaimSet;
    lfInspector: TfInspector;
    lMinimized, lRedrawDuringTransactions: boolean;
    lDiagramKind: TcdDiagramKind;
  begin
  inherited;

  if not MatchPattern('command')
     then SyntaxError;
  CheckParams(['command', 'minimized', 'redrawduringtransactions', 'type']);

  SplitString(Tokens[0], ' ', lStr, lIdentifier);
  if Params.IndexOfName('type')>=0
     then lType := Params.Values['Type']
     else lType := '';

  if CompareText(lIdentifier, 'Inspector')=0
     then begin
          // Inspector
          lClaimType := nil;
          lClaimSet := nil;
          lMinimized := Params.IndexOfName('Minimized')>=0;
          lRedrawDuringTransactions := Params.IndexOfName('RedrawDuringTransactions')>=0;
          lfInspector := TfInspector.Create(Application);
          lfInspector.Init(Session, lClaimType, lClaimSet, lMinimized, lRedrawDuringTransactions);
          lfInspector.Show;
          end
  else if CompareText(lIdentifier, 'Transactions')=0
     then fMain.ShowTransactions
  else if CompareText(lIdentifier, 'Model')=0
     then begin
          lDiagramKind := dkAtomicTypes; // Prevent warning
          if CompareText(lType, 'AtomicTypes')=0
             then lDiagramKind := dkAtomicTypes
          else if CompareText(lType, 'Classes')=0
             then lDiagramKind := dkClasses
          else GeneralError('Unknown diagram kind: %s', [lType]);
          fMain.ShowModel(lDiagramKind);
          end
  else if CompareText(lIdentifier, 'ScriptEditor')=0
     then fMain.ShowScriptEditor
  else if CompareText(lIdentifier, 'GraphEditor')=0
     then fMain.ShowGraphEditor
  else if CompareText(lIdentifier, 'SampleUI')=0
     then fMain.ShowSampleUI
  else if CompareText(lIdentifier, 'APIServer')=0
     then fMain.ShowAPIServer
  else begin
       // No standard window identifier
       // Must be either Type name or ClaimSet number
       if lIdentifier[1] = cRefChar
          then lIdentifier := Copy(lIdentifier, 2, Length(lIdentifier));

       lID := StrToIntDef(lIdentifier, -1);
       if lID<0
         then begin
              // Assume a Type name was specified
              lClaimType := Engine.ConceptualTypes.FindClaimType(lIdentifier);
              if not Assigned(lClaimType)
                then TypeUnknown('Type', lIdentifier);

              lClaimSet := nil;
              end
         else begin
              // Assume a ClaimSet id was specified
              lClaimSet := Engine.FindClaimSet(lID);
              if not Assigned(lClaimSet)
                then GeneralError('Could not find ClaimSet %d.', [lID]);

              lClaimType := lClaimSet.ClaimType;
              end;

       lMinimized := Params.IndexOfName('Minimized')>=0;
       lRedrawDuringTransactions := Params.IndexOfName('RedrawDuringTransactions')>=0;
       lfInspector := TfInspector.Create(Application);
       lfInspector.Init(Session, lClaimType, lClaimSet, lMinimized, lRedrawDuringTransactions);
       lfInspector.Show;
       end;
  end;

initialization
  // Register handlers
  gCommandDispatcher.Add(TchShow.Create);
end.

