// -----------------------------------------------------------------------------
//
// ace.Command.Structure
//
// -----------------------------------------------------------------------------
//
// Commands to define LabelTypes and ClaimTypes.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Commands.Structure;

interface

uses
  Classes, SysUtils,
  ace.Core.Engine,
  ace.Commands.Dispatcher;

type
  TchDefineLabelType =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchDefineClaimType =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchDefineAnnotationType =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchAddPluralName =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchAddUnicity =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchAddTotality =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

  TchAddNestedExpressionTemplate =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

implementation

uses
  common.Utils, common.RadioStation;

{ TchDefineLabelType }

constructor TchDefineLabelType.Create;
  begin
  inherited Create(ceStructure);
  Name := 'define LabelType';
  MatchOperator := moFull;

  SetHelpText(
    'define LabelType' + cLineBreak +
    '----------------' + cLineBreak +
    'Defines a new LabelType.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'define LabelType: <name>' + cLineBreak +
    '  Scalar: <name>'
  );

  Keywords.Add('define');
  Keywords.Add('labeltype');
  Keywords.Add('scalar');
  AutoComplete.Add('define Labeltype: '#13#10'  scalar: ');
  end;

procedure TchDefineLabelType.iExecute;
  var
    lLabelType: TcLabelType;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command : name') then SyntaxError;
  // RenameParam('sc', 'scalar');
  CheckParams(['command', 'name', 'scalar']);
  CheckRequiredParams(['scalar']);

  // Create and add Labeltype
  lLabelType := TcLabelType.Create(Engine, Params.Values['name'], Params.Values['scalar']);
  Engine.ConceptualTypes.Add(lLabelType.Name, lLabelType);

  Response.Add(Format('LabelType %s created.', [Params.Values['name']]));

  if Assigned(gRadioStation)
     then gRadioStation.BroadCast(cRadioEvent_LabelType_Created, lLabelType);
  end;


{ TchDefineClaimType }

constructor TchDefineClaimType.Create;
  begin
  inherited Create(ceStructure);
  Name := 'define ClaimType';
  MatchOperator := moFull;

  SetHelpText(
    'define ClaimType' + cLineBreak +
    '----------------' + cLineBreak +
    'Defines a new ClaimType.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'define ClaimType: <name>' + cLineBreak +
    '  ExpressionTemplate: <expression template>' + cLineBreak +
    '' + cLineBreak +
    'Optional parameters:' + cLineBreak +
    '  PluralName: <name>' + cLineBreak +
    '  NestedExpressionTemplate: <expression template>' + cLineBreak +
    '  Identity: <role>[,<role>]*' + cLineBreak +
    '  Unicity: <role>[,<role>]*     // Use the ''Add unicity'' command to add multiple unicity constraints.' + cLineBreak +
    '  Totality: <role>[,<role>]*    // A totality constraints is created for each role. Multi role totality constraints are not supported.' + cLineBreak +
    '  AllowChange: false            // default = true' + cLineBreak +
    '  TimelineInterpretation: false // default = true' + cLineBreak +
    '  RecordTransactionTime: false  // default = true' + cLineBreak +
    '  KeepExpired: false            // default = true' + cLineBreak +
    '  RecordValidTime: false        // default = true' + cLineBreak +
    '  KeepEnded: false              // default = true'
  );

  Keywords.Add('define');
  Keywords.Add('claimtype');
  Keywords.Add('expressiontemplate');
  Keywords.Add('pluralname');
  Keywords.Add('nestedexpressiontemplate');
  Keywords.Add('identity');
  Keywords.Add('unicity');
  Keywords.Add('totality');
  Keywords.Add('allowchange');
  Keywords.Add('timelineinterpretation');
  Keywords.Add('recordtransactiontime');
  Keywords.Add('keepexpired');
  Keywords.Add('recordvalidtime');
  Keywords.Add('keepended');
  AutoComplete.Add('define ClaimType: '#13#10'  ExpressionTemplate: ');
  AutoComplete.Add('  PluralName: ');
  AutoComplete.Add('  NestedExpressionTemplate: ');
  AutoComplete.Add('  Identity: ');
  AutoComplete.Add('  Unicity: ');
  AutoComplete.Add('  Totality: ');
  AutoComplete.Add('  AllowChange: ');
  AutoComplete.Add('  Timelineinterpretation: ');
  AutoComplete.Add('  RecordTransactionTime: ');
  AutoComplete.Add('  KeepExpired: ');
  AutoComplete.Add('  RecordValidTime: ');
  AutoComplete.Add('  KeepEnded: ');
  end;

procedure TchDefineClaimType.iExecute;
  var
    lClaimType: TcClaimType;
    lIdentityMessage: String = '';
    lUnicity: TcUnicity;
    //lIsObjectType: boolean;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command : name') then SyntaxError;
  CheckParams(['command', 'name', 'ExpressionTemplate',
    'PluralName', 'NestedExpressionTemplate',
    'Identity', 'Unicity', 'Totality',
    'AllowChange', 'TimelineInterpretation',
    'RecordTransactionTime', 'KeepExpired',
    'RecordValidTime', 'KeepEnded']);
  CheckRequiredParams(['ExpressionTemplate']);

  // Create ClaimType
  lClaimType := TcClaimType.Create(Engine, Params.Values['name']);
  lClaimType.SetExpressionTemplate(Params.Values['ExpressionTemplate']);

  // Handle parameters
  //lIsObjectType := Params.IndexOfName('NestedExpressionTemplate')>=0;

  if Params.IndexOfName('PluralName')>=0
    then lClaimType.PluralName := Params.Values['PluralName'];

  if Params.IndexOfName('Identity')>=0
    then lClaimType.SetIdentity(Params.Values['Identity'])
    else lClaimType.DeriveIdentity(lIdentityMessage);

  if Params.IndexOfName('Unicity')>=0
    then begin
         lUnicity := TcUnicity.Create(lClaimType);
         lUnicity.AddRoles(Params.Values['Unicity']);
         lClaimType.UnicityList.Add(lUnicity.ID, lUnicity);
         end;

  if Params.IndexOfName('Totality')>=0
    then lClaimType.AddTotalityConstraints(Params.Values['Totality']);

  if Params.IndexOfName('NestedExpressionTemplate')>=0
    then lClaimType.SetNestedExpressionTemplate(Params.Values['NestedExpressionTemplate']);

  if Params.IndexOfName('AllowChange')>=0
    then lClaimType.AllowChange := TrueFalseToBoolean(Params.Values['AllowChange']);

  if Params.IndexOfName('TimelineInterpretation')>=0
    then lClaimType.TimelineInterpretation := TrueFalseToBoolean(Params.Values['TimelineInterpretation']);

  if Params.IndexOfName('RecordTransactionTime')>=0
    then lClaimType.RecordTransactionTime := TrueFalseToBoolean(Params.Values['RecordTransactionTime']);

  if Params.IndexOfName('KeepExpired')>=0
    then lClaimType.KeepExpired := TrueFalseToBoolean(Params.Values['KeepExpired']);

  if Params.IndexOfName('RecordValidTime')>=0
    then lClaimType.RecordValidTime := TrueFalseToBoolean(Params.Values['RecordValidTime']);

  if Params.IndexOfName('KeepEnded')>=0
    then lClaimType.KeepEnded := TrueFalseToBoolean(Params.Values['KeepEnded']);

  // Add ClaimType
  lClaimType.AfterInit;
  Engine.RelationTypes.Add(lClaimType.ID, lClaimType);
  Engine.ConceptualTypes.Add(lClaimType.Name, lClaimType);

  Response.Add(Format('ClaimType %s created.', [Params.Values['name']]));
  if lIdentityMessage<>''
    then Response.Add('  ' + lIdentityMessage);

  if Assigned(gRadioStation)
     then gRadioStation.BroadCast(cRadioEvent_ClaimType_Created, lClaimType);
  end;


{ TchDefineAnnotationType }

constructor TchDefineAnnotationType.Create;
  begin
  inherited Create(ceStructure);
  Name := 'define AnnotationType';
  MatchOperator := moFull;

  SetHelpText(
    'define AnnotationType' + cLineBreak +
    '---------------------' + cLineBreak +
    'Defines a new AnnotationType.' + cLineBreak +
    '' + cLineBreak +
    'Syntax: ' + cLineBreak +
    'define AnnotationType: <name>' + cLineBreak +
    '  ExpressionTemplate: <expression template>' + cLineBreak +
    '  Kind: info | doubt ' + cLineBreak +
    '' + cLineBreak +
    'Optional parameters:' + cLineBreak +
    '  AllowChange: false            // default = true' + cLineBreak +
    '  TimelineInterpretation: false // default = true' + cLineBreak +
    '  RecordTransactionTime: false  // default = true' + cLineBreak +
    '  KeepExpired: false            // default = true' + cLineBreak +
    '  RecordValidTime: false        // default = true' + cLineBreak +
    '  KeepEnded: false              // default = true'
  );

  Keywords.Add('define');
  Keywords.Add('annotationtype');
  Keywords.Add('expressiontemplate');
  Keywords.Add('kind');
  Keywords.Add('allowchange');
  Keywords.Add('timelineinterpretation');
  Keywords.Add('recordtransactiontime');
  Keywords.Add('keepexpired');
  Keywords.Add('recordvalidtime');
  Keywords.Add('keepended');
  AutoComplete.Add('define AnnotationType: '#13#10'  ExpressionTemplate: '#13#10'  Kind: ');
  end;

procedure TchDefineAnnotationType.iExecute;
  var
    lAnnotationType: TcAnnotationType;
    lAnnotationKind: TcAnnotationKind;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command : name') then SyntaxError;
  CheckParams(['command', 'name', 'Kind', 'ExpressionTemplate',
    'AllowChange', 'TimelineInterpretation',
    'RecordTransactionTime', 'KeepExpired',
    'RecordValidTime', 'KeepEnded']);
  CheckRequiredParams(['ExpressionTemplate', 'kind']);

  // Create AnnotationType
  lAnnotationKind := AnnotationKindStrToKind(Params.Values['kind']);
  lAnnotationType := TcAnnotationType.Create(Engine, Params.Values['name'], lAnnotationKind);
  lAnnotationType.SetExpressionTemplate(Params.Values['ExpressionTemplate']);
  lAnnotationType.SetIdentity(cMetaRoleName_Claim);

  // Handle parameters
  if Params.IndexOfName('AllowChange')>=0
    then lAnnotationType.AllowChange := TrueFalseToBoolean(Params.Values['AllowChange']);

  if Params.IndexOfName('TimelineInterpretation')>=0
    then lAnnotationType.TimelineInterpretation := TrueFalseToBoolean(Params.Values['TimelineInterpretation']);

  if Params.IndexOfName('RecordTransactionTime')>=0
    then lAnnotationType.RecordTransactionTime := TrueFalseToBoolean(Params.Values['RecordTransactionTime']);

  if Params.IndexOfName('KeepExpired')>=0
    then lAnnotationType.KeepExpired := TrueFalseToBoolean(Params.Values['KeepExpired']);

  if Params.IndexOfName('RecordValidTime')>=0
    then lAnnotationType.RecordValidTime := TrueFalseToBoolean(Params.Values['RecordValidTime']);

  if Params.IndexOfName('KeepEnded')>=0
    then lAnnotationType.KeepEnded := TrueFalseToBoolean(Params.Values['KeepEnded']);

  // Add ClaimType
  lAnnotationType.AfterInit;
  Engine.RelationTypes.Add(lAnnotationType.ID, lAnnotationType);
  Engine.ConceptualTypes.Add(lAnnotationType.Name, lAnnotationType);

  Response.Add(Format('AnnotationType %s created.', [Params.Values['name']]));

  if Assigned(gRadioStation)
     then gRadioStation.BroadCast(cRadioEvent_AnnotationType_Created, lAnnotationType);
  end;


constructor TchAddPluralName.Create;
  begin
  inherited Create(ceStructure);
  Name := 'add PluralName';
  MatchOperator := moFull;

  SetHelpText(
    'add PluralName' + cLineBreak +
    '--------------' + cLineBreak +
    'Adds or changes the PluralName of a ClaimType.' + cLineBreak +
    '' + cLineBreak +
    'Syntax: ' + cLineBreak +
    'add PluralName: <ClaimTypeName> = <name>'
  );

  Keywords.Add('add');
  Keywords.Add('pluralname');
  AutoComplete.Add('add PluralName: '#13#10' | ');
  end;

procedure TchAddPluralName.iExecute;
  var
    lClaimType: TcClaimType;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command : claimtype = name') then SyntaxError;
  CheckParams(['command', 'claimtype', 'name']);
  CheckRequiredParams(['claimtype', 'name']);

  // Fetch ClaimType
  lClaimType := Engine.ConceptualTypes.FindClaimType(Params.Values['claimtype']);
  if not Assigned(lClaimType)
     then GeneralError('ClaimType %s not found.', [Params.Values['claimtype']]);

  lClaimType.PluralName := Params.Values['name'];
  Response.Add(Format('PluralName of ClaimType %s set to %s.', [lClaimType.Name, Params.Values['name']]));
  end;


constructor TchAddUnicity.Create;
  begin
  inherited Create(ceStructure);
  Name := 'add Unicity';
  MatchOperator := moFull;

  SetHelpText(
    'add Unicity' + cLineBreak +
    '--------------' + cLineBreak +
    'Adds a unicity constraint to a ClaimType.' + cLineBreak +
    '' + cLineBreak +
    'Syntax: ' + cLineBreak +
    'add Unicity: <ClaimTypeName> = <role>[,<role>]*'
  );

  Keywords.Add('add');
  Keywords.Add('unicity');
  AutoComplete.Add('add Unicity: '#13#10' | ');
  end;

procedure TchAddUnicity.iExecute;
  var
    lClaimType: TcClaimType;
    lUnicity: TcUnicity;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command : claimtype = roles') then SyntaxError;
  CheckParams(['command', 'claimtype', 'roles']);
  CheckRequiredParams(['claimtype', 'roles']);

  // Fetch ClaimType
  lClaimType := Engine.ConceptualTypes.FindClaimType(Params.Values['claimtype']);
  if not Assigned(lClaimType)
     then GeneralError('ClaimType %s not found.', [Params.Values['claimtype']]);

  lUnicity := TcUnicity.Create(lClaimType);
  lUnicity.AddRoles(Params.Values['roles']);
  lClaimType.UnicityList.Add(lUnicity.ID, lUnicity);

  Response.Add(Format('Added unicity constraint on roles %s to ClaimType %s.', [Params.Values['roles'], lClaimType.Name]));
  end;


constructor TchAddTotality.Create;
  begin
  inherited Create(ceStructure);
  Name := 'add Totality';
  MatchOperator := moFull;

  SetHelpText(
    'add Totality' + cLineBreak +
    '--------------' + cLineBreak +
    'Adds a totality constraint to a ClaimType.' + cLineBreak +
    '' + cLineBreak +
    'Syntax: ' + cLineBreak +
    'add Totality: <ClaimTypeName> = <role>'
  );

  Keywords.Add('add');
  Keywords.Add('totality');
  AutoComplete.Add('add Totality: '#13#10' | ');
  end;

procedure TchAddTotality.iExecute;
  var
    lClaimType: TcClaimType;
    lRole: TcRole;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command : claimtype = role') then SyntaxError;
  CheckParams(['command', 'claimtype', 'role']);
  CheckRequiredParams(['claimtype', 'role']);

  // Fetch ClaimType
  lClaimType := Engine.ConceptualTypes.FindClaimType(Params.Values['claimtype']);
  if not Assigned(lClaimType)
     then GeneralError('ClaimType %s not found.', [Params.Values['claimtype']]);

  lRole := lClaimType.Roles.Find(Params.Values['role']);
  if not Assigned(lRole)
     then GeneralError('Role %s not found.', [Params.Values['role']]);
  lClaimType.AddTotalityConstraint(lRole);

  Response.Add(Format('Added unicity constraint on roles %s to ClaimType %s.', [Params.Values['roles'], lClaimType.Name]));
  end;


constructor TchAddNestedExpressionTemplate.Create;
  begin
  inherited Create(ceStructure);
  Name := 'add NestedExpressionTemplate';
  MatchOperator := moFull;

  SetHelpText(
    'add NestedExpressionTemplate' + cLineBreak +
    '--------------' + cLineBreak +
    'Adds a NestedExpressionTemplate to a ClaimType.' + cLineBreak +
    '' + cLineBreak +
    'Syntax: ' + cLineBreak +
    'add NestedExpressionTemplate: <ClaimTypeName> = <template>'
  );

  Keywords.Add('add');
  Keywords.Add('NestedExpressionTemplate');
  AutoComplete.Add('add NestedExpressionTemplate: '#13#10' | ');
  end;

procedure TchAddNestedExpressionTemplate.iExecute;
  var
    lClaimType: TcClaimType;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command : claimtype = template') then SyntaxError;
  CheckParams(['command', 'claimtype', 'template']);
  CheckRequiredParams(['claimtype', 'template']);

  // Fetch ClaimType
  lClaimType := Engine.ConceptualTypes.FindClaimType(Params.Values['claimtype']);
  if not Assigned(lClaimType)
     then GeneralError('ClaimType %s not found.', [Params.Values['claimtype']]);

  lClaimType.SetNestedExpressionTemplate(Params.Values['template']);

  Response.Add(Format('Added NestedExpressionTemplate %s to ClaimType %s.', [Params.Values['template'], lClaimType.Name]));
  end;


initialization
  // Register handlers
  gCommandDispatcher.Add(TchDefineLabelType.Create);
  gCommandDispatcher.Add(TchDefineClaimType.Create);
  gCommandDispatcher.Add(TchDefineAnnotationType.Create);
  gCommandDispatcher.Add(TchAddPluralName.Create);
  gCommandDispatcher.Add(TchAddUnicity.Create);
  gCommandDispatcher.Add(TchAddTotality.Create);
  gCommandDispatcher.Add(TchAddNestedExpressionTemplate.Create);
end.
