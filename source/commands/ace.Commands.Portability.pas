// -----------------------------------------------------------------------------
//
// ace.Command.Portability
//
// -----------------------------------------------------------------------------
//
// Commands to import VNG/MIM EA XMI file.
// (Implementation in portability\ace.Import.xmi).
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Commands.Portability;

{$MODE Delphi}

interface

uses
  Classes, SysUtils,
  ace.Commands.Dispatcher, ace.Core.Engine;

type
  TchImportXMI =
    class (TcCommandHandler)
      private

      protected
        procedure iExecute; override;

      public
        constructor Create; reintroduce;
      end;

implementation

uses
  XMLRead, DOM,
  common.Utils,
  common.FileUtils,
  common.ErrorHandling,
  ace.Forms.Console,
  ace.Settings,
  ace.Import.xmi, xmltextreader, xmlutils;


{ TchImportXMI }

constructor TchImportXMI.Create;
  begin
  inherited Create(cePortability);
  Name := 'import xmi';
  MatchOperator := moFull;

  SetHelpText(
    'import xmi' + cLineBreak +
    '----------' + cLineBreak +
    'Loads an EA XMI file and converts in into a script to define atomic claims.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'import xmi' + cLineBreak +
    '  filename: <filename>'
  );

  Keywords.Add('import xmi');
  Keywords.Add('filename');
  AutoComplete.Add('import xmi');
  end;

procedure TchImportXMI.iExecute;
  var
    lXMIFilename, lScriptFilename: string;
    lXML: TXMLDocument;
    lImportXMI: TcImportXMI;
  begin

  inherited;

  // Interpret command
  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command', 'filename']);

  if Params.IndexOfName('filename')>=0
     then begin
          lXMIFilename := TPath.Combine(gSettings.ProjectFolder, Params.Values['filename']);
          if not FileExists(lXMIFilename)
             then GeneralError('File not found: %s' , [Params.Values['filename']]);
          end
     else lXMIFilename := '';

  if (lXMIFilename='') and not PromptForOpenFileName('Import xml', 'xmi', gSettings.ProjectFolder, '*.xmi', lXMIFilename)
     then exit;

  if not gSettings.AskProjectFolderWhenEmpty
     then EUsr('Cannot import without an active project.');

  lScriptFilename := Format('ClaimScript-for-%s.txt', [TPath.GetFileNameWithoutExtension(lXMIFilename)]);
  if not PromptForSaveFileName('ClaimType script', 'txt', gSettings.ProjectFolder, '*.txt', lScriptFilename)
     then exit;

  ReadXMLFile(lXML, lXMIFilename);
  try
    lImportXMI := TcImportXMI.Create(Session, lXML);
    try
      lImportXMI.Execute(lScriptFilename);
    finally
      lImportXMI.Free;
      end;
  finally
    lXML.Free;
    end;

  Response.Add('XMI imported.');
  Response.Add('ClaimType script saved to:');
  Response.Add('  '+lScriptFilename);
  end;

function DecoderWindows1252(const AEncoding: string; out Decoder: TDecoder): Boolean; stdcall;
  begin
  result := false;
  if CompareText(AEncoding, 'windows-1252') = 0
     then begin
          Decoder := Default(TDecoder);
          Decoder.Decode := @Decode_8859_1;
          result := True;
          end;
  end;

initialization
  // Register handlers
  gCommandDispatcher.Add(TchImportXMI.Create);

  xmltextreader.RegisterDecoder(DecoderWindows1252);

end.
