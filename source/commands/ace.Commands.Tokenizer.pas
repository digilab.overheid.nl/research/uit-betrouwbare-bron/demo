// -----------------------------------------------------------------------------
//
// ace.Commands.Tokenizer
//
// -----------------------------------------------------------------------------
//
// Tokenizer used to tokenize commands.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Commands.Tokenizer;

interface

uses
  Classes, SysUtils;

const
  // Characters that separate tokens
  cStatementSeparators: TSysCharSet = [':', '='];

// Convert a command to tokens
procedure CommandToTokens (aCommand: String; aCommandTokens: TStringlist);

implementation

uses
  common.ErrorHandling;

const
  cNoChar = #0;
  cSingleQuote = '''';
  cDoubleQuote = '"';
  cSpace = ' ';

procedure CommandToTokens (aCommand: String; aCommandTokens: TStringlist);
  var
    i: integer;
    character: Char;
    nextCharacter: Char;
    inToken: boolean;
    quote: Char;
    token: string;
  begin
  i := 1;
  inToken := false;
  quote := cNoChar;
  token := '';
  while i <= Length(aCommand) do
    begin
    character := aCommand[i];
    if i+1 <= Length(aCommand)
      then nextCharacter := aCommand[i+1]
      else nextCharacter := cNoChar;

    if not inToken // Not yet in token
      then begin
           if CharInSet(character, cStatementSeparators) // Token could be a separator
             then aCommandTokens.Add(character) // Add separator as token

           else if character <> cSpace // Ignore spaces
             then begin // Text token
                  inToken := true;

                  if (character = cSingleQuote) and (nextCharacter = cSingleQuote) // Quote = character
                    then begin
                         token := cSingleQuote;
                         inc(i);
                         end

                  else if character = cSingleQuote
                    then quote := cSingleQuote

                  else if (character = cDoubleQuote) and (nextCharacter = cDoubleQuote) // Quote = character
                    then begin
                         token := cDoubleQuote;
                         inc(i);
                         end

                  else if character = cDoubleQuote
                    then quote := cDoubleQuote

                  else token := character;
                  end;
           end

    else if quote = cNoChar // In token without quotes
      then begin
           if (character = cSingleQuote) or (character = cDoubleQuote) // No quotes allowed in simple token
             then EUsr('Unexpected quote %s at position %d.', [character, i+1]);

           if CharInSet(character, cStatementSeparators) // Separator ends the token
             then begin
                  inToken := false;
                  quote := cNoChar;
                  aCommandTokens.Add(Trim(token)); // Add token
                  token := '';
                  aCommandTokens.Add(character); // Add separator as token
                  end
           else token := token + character; // Add character to token
           end

    else begin // In token with quotes
         if (character = quote) and (nextCharacter = quote) // Double quote -> add as single quote
           then begin
                token := token + quote;
                inc(i);
                end

         else if character = quote // Single quote -> ends token
           then begin
                inToken := false;
                quote := cNoChar;
                aCommandTokens.Add(Trim(token));
                token := '';
                end

         else token := token + character; // Add character to token
         end;

    inc(i);
    end;

  if inToken
    then begin
         if quote <> cNoChar // Quoted tokens should be closed properly
           then EUsr('Unexpected end of statement. Quote (%s) expected.', [quote]);

         aCommandTokens.Add(token);
         end;
  end;

end.
