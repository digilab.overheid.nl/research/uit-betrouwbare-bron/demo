// -----------------------------------------------------------------------------
//
// ace.Command.Dispatcher
//
// -----------------------------------------------------------------------------
//
// Command interpreter and dispatcher.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Commands.Dispatcher;

{$MODE Delphi}

interface

uses
  Classes, SysUtils,
  SynEditHighlighter, SynHighlighterAny, SynCompletion,
  common.SimpleLists,
  ace.Core.Engine;

const
  // Indent used in output
  cIndent = '  ';

type
  ESyntaxError = class (Exception);
  ECommandUnknown = class (Exception);
  EParamMissing = class (Exception);
  EParamUnknown = class (Exception);
  ETypeUnknown = class (Exception);
  EElementUnkown = class (Exception);
  EGeneral = class (Exception);

  // Forward
  TcCommandHandler = class;
  TcMatchOperator = (moFull, moStart, moCustom);
  TcCommandEffect = (ceStructure, ceTime, ceContent, ceTransaction, ceVisualisation, ceApplication, cePortability);

  TcCommandDispatcher =
    class
      private
        fHandlers: TcStringObjectList<TcCommandHandler>; // Name, Handler - Owned, Sorted

        function GetItems(const aIndex: integer): TcCommandHandler;
        function GetCount: integer;

      public
        constructor Create;
        destructor Destroy; override;

        function Execute(aSession: TcSession; aCommand, aResponse: TStringlist): TStringlist;

        procedure AddKeywordsToHighLighter(aHighLighter: TSynAnySyn);
        procedure AddItemsToCompletionProposal(aCompletionProposal: TSynCompletion);

        procedure Add(aHandler: TcCommandHandler);
        function Find(aName: string): TcCommandHandler;

        property Items[const aIndex: integer]: TcCommandHandler read GetItems; default;
        property Count: integer read GetCount;
      end;

  TcCommandHandler =
    class
      private
        fName: string;
        fMatchOperator: TcMatchOperator;
        fEffect: TcCommandEffect;

        fShowInHelp: boolean;
        fHelpText: TStringlist;

        fEngine: TcEngine;
        fSession: TcSession;

        fTokens: TStringlist;
        fParams: TStringlist;
        fResponse: TStringlist;

        fKeywords: TStringlist;
        fAutoComplete: TStringlist;

      protected
        // Error handling
        procedure SyntaxError;
        procedure CommandUnknown(aName: string);
        procedure ParamMissing(aParamName: string);
        procedure ParamUnknown(aParamName: string);
        procedure TypeUnknown(aTypeName, aName: string);
        procedure ElementUnknown(aID: string);
        procedure GeneralError(aMessage: string; const aArgs: array of const);

        // Warnings
        procedure TimelineIntegrityWarning;

        procedure CheckParams(const aNames: array of string);
        procedure CheckRequiredParams(const aNames: array of string);

        // Tries to match the tokens of the command to the specified pattern.
        // The names in the pattern will be added to the Params (which is a key/value list)
        // The key will be the name in the pattern, the value is the corresponding value in the command.
        function MatchPattern (aPattern: String): boolean;

        procedure SetHelpText(aText: String);

        property Engine: TcEngine read fEngine;
        property Session: TcSession read fSession;

        property Tokens: TStringlist read fTokens;
        property Params: TStringlist read fParams;
        property Response: TStringlist read fResponse;

        property Keywords: TStringlist read fKeywords;
        property AutoComplete: TStringlist read fAutoComplete;

        procedure iExecute; virtual;

      public
        constructor Create(aEffect: TcCommandEffect); virtual;
        destructor Destroy; override;

        function Match(aCommand: String): boolean; virtual;

        // Called by dispatcher. Manages transactions and calls iExecute.
        // Decendants should override iExecute not Execute.
        procedure Execute(aSession: TcSession; aTokens, aParams, aResponse: TStringlist); virtual; final;

        property Name: string read fName write fName;
        property MatchOperator: TcMatchOperator read fMatchOperator write fMatchOperator;
        property Effect: TcCommandEffect read fEffect;
        property ShowInHelp: boolean read fShowInHelp write fShowInHelp;
        property HelpText: TStringlist read fHelpText;
      end;

var
  gCommandDispatcher: TcCommandDispatcher;

implementation

uses
  Dialogs,
  common.ErrorHandling, common.Utils,
  ace.Commands.Tokenizer;


{ TcCommandDispatcher }

constructor TcCommandDispatcher.Create;
  begin
  inherited Create;
  fHandlers := TcStringObjectList<TcCommandHandler>.Create(true {Owned}, true {Sorted});
  end;

destructor TcCommandDispatcher.Destroy;
  begin
  fHandlers.Free;
  inherited destroy;
  end;

function TcCommandDispatcher.GetItems(const aIndex: integer): TcCommandHandler;
  begin
  result := fHandlers.Objects[aIndex];
  end;

function TcCommandDispatcher.GetCount: integer;
  begin
  result := fHandlers.Count;
  end;

procedure TcCommandDispatcher.Add(aHandler: TcCommandHandler);
  begin
  fHandlers.Add(aHandler.Name, aHandler);
  end;

function TcCommandDispatcher.Find(aName: string): TcCommandHandler;
  var
    i: integer;
  begin
  if fHandlers.Find(aName, i)
    then result := fHandlers.Objects[i]
    else result := nil;
  end;

function TcCommandDispatcher.Execute(aSession: TcSession; aCommand, aResponse: TStringlist): TStringlist;
  var
    i: Integer;
    lFirstLine, lLine, lName, lValue: string;
    lCommand: string;
    lTokens, lParams: TStringlist;
    lHandler: TcCommandHandler;
    lHandled: boolean;
  begin
  result := nil;
  lTokens := TStringlist.Create;
  lParams := TStringlist.Create;
  aResponse.Clear;

  try
    // Is there at least 1 line?
    if aCommand.Count<1 then exit;

    // First line determines command type
    lFirstLine := aCommand[0];
    if lFirstLine = '' then exit;

    // Tokenize command
    CommandToTokens(lFirstLine, lTokens);

    // Is there at least 1 token?
    if lTokens.Count = 0
      then EUsr('Invalid command: %s', [lFirstLine]);

    // Collect additional properties
    for i := 1 to aCommand.Count-1 do
      begin
      lLine := aCommand[i];
      SplitString(lLine, ':', lName, lValue);
      lName := Trim(lName);
      lValue := Trim(lValue);
      if (lName='') then EUsr('Invalid name/value pair: %s', [lLine]);
      lParams.AddPair(lName, lValue);
      end;

    // Dispatch command
    lCommand := lTokens[0];
    lHandled := false;
    for i := 0 to fHandlers.Count-1 do
      begin
      lHandler := fHandlers.Objects[i];
      if lHandler.Match(lCommand)
        then begin
             if (lHandler.Effect = ceStructure) and (aSession.Engine.InTransaction)
                then EUsr('Cannot change the structure during an active Transaction.');

             lHandler.Execute(aSession, lTokens, lParams, aResponse);

             lHandled := true;
             break;
             end;
      end;
    if not lHandled
      then EUsr('Unknown command: %s', [lFirstLine]);

  finally
    lTokens.Free;
    lParams.Free;
    end;
  end;

procedure TcCommandDispatcher.AddKeywordsToHighLighter(aHighLighter: TSynAnySyn);
  var
    i, k: integer;
    lKeywords: TStringlist;
    lHandler: TcCommandHandler;
  begin
  lKeywords := TStringlist.Create;
  lKeywords.Sorted := true;
  lKeywords.CaseSensitive := true;
  lKeywords.Duplicates := dupIgnore;
  try
    for i := 0 to fHandlers.Count-1 do
      begin
      lHandler := fHandlers.Objects[i];
      for k := 0 to lHandler.Keywords.Count-1 do
        lKeywords.Add(lHandler.Keywords[k]);
      end;

    aHighLighter.KeyWords.Assign(lKeywords);
  finally
    lKeywords.Free;
    end;
  end;

procedure TcCommandDispatcher.AddItemsToCompletionProposal(aCompletionProposal: TSynCompletion);
  var
    i, a: integer;
    lHandler: TcCommandHandler;
  begin
  for i := 0 to fHandlers.Count-1 do
    begin
    lHandler := fHandlers.Objects[i];
    for a := 0 to lHandler.AutoComplete.Count-1 do
      aCompletionProposal.ItemList.Add(lHandler.AutoComplete[a]);
    end;
  end;


{ TcCommandHandler }

constructor TcCommandHandler.Create(aEffect: TcCommandEffect);
  begin
  inherited Create;
  fEffect := aEffect;
  fHelpText := TStringlist.Create;
  fShowInHelp := true;
  fKeywords := TStringlist.Create;
  fAutoComplete := TStringlist.Create;
  end;

destructor TcCommandHandler.Destroy;
  begin
  fHelpText.Free;
  fKeywords.Free;
  fAutoComplete.Free;
  inherited;
  end;

procedure TcCommandHandler.SyntaxError;
  begin
  raise ESyntaxError.Create('Syntax Error');
  end;

procedure TcCommandHandler.CommandUnknown(aName: string);
  begin
  raise ECommandUnknown.CreateFmt('Command %s unknown', [aName]);
  end;

procedure TcCommandHandler.ParamMissing(aParamName: string);
  begin
  raise EParamMissing.CreateFmt('Parameter %s is required', [aParamName]);
  end;

procedure TcCommandHandler.ParamUnknown(aParamName: string);
  begin
  raise EParamUnknown.CreateFmt('Parameter %s is unknown', [aParamName]);
  end;

procedure TcCommandHandler.TypeUnknown(aTypeName, aName: string);
  begin
  raise ETypeUnknown.CreateFmt('%s %s unknown', [aTypeName, aName]);
  end;

procedure TcCommandHandler.ElementUnknown(aID: string);
  begin
  raise ETypeUnknown.CreateFmt('Element %s unknown', [aID]);
  end;

procedure TcCommandHandler.GeneralError(aMessage: string; const aArgs: array of const);
  begin
  raise EGeneral.CreateFmt(aMessage, aArgs);
  end;

procedure TcCommandHandler.TimelineIntegrityWarning;
  begin
  Response.Add('!! Warning: This violates the integrity of the transactional timeline.');
  end;

procedure TcCommandHandler.CheckParams(const aNames: array of string);
  var
    i, lIndex: integer;
    lAllowedParams: TStringlist;
  begin
  lAllowedParams := TStringlist.Create;
  lAllowedParams.Sorted := true;
  lAllowedParams.CaseSensitive := false;
  lAllowedParams.Duplicates := dupError;
  try
    // Create a sorted list of allowed params
    for i := Low(aNames) to High(aNames) do
      lAllowedParams.Add(aNames[i]);

    // Check params
    for i := 0 to Params.Count-1 do
      begin
      if not lAllowedParams.Find(Params.Names[i], lIndex)
        then ParamUnknown(Params[i]);
      end;

  finally
    lAllowedParams.Free;
    end;
  end;

procedure TcCommandHandler.CheckRequiredParams(const aNames: array of string);
  var
    i: integer;
  begin
  for i := Low(aNames) to High(aNames) do
    if Params.IndexOfName(aNames[i])<0
      then ParamMissing(aNames[i]);
  end;

function TcCommandHandler.MatchPattern (aPattern: String): boolean;
  var
    i: Integer;
    lPatternTokens: TStringlist;
    lPatternToken: string;
    lToken: string;
  begin
  result := false;

  // Convert pattern to tokens
  lPatternTokens := TStringlist.Create;
  CommandToTokens(aPattern, lPatternTokens);

  try
    // Number of tokens should match
    if lPatternTokens.Count <> Tokens.Count
      then exit;

    for i := 0 to lPatternTokens.Count-1 do
      begin
      lPatternToken := lPatternTokens[i];
      lToken := Tokens[i];

      if CharInSet(lPatternToken[1], cStatementSeparators)
        then begin
             // Pattern expects a separator
             if lToken <> lPatternToken then exit;
             end
        else begin
             // Token should not be a separator
             if CharInSet(lToken[1], cStatementSeparators) then exit;

             Params.AddPair(lPatternToken, lToken);
             end;
      end;

    result := true;

  finally
    lPatternTokens.Free;
    end;
  end;

procedure TcCommandHandler.SetHelpText(aText: String);
  var
    lStrs: TStringlist;
  begin
  lStrs := StringToStringlist(aText, cLineBreak);
  try
    HelpText.AddStrings(lStrs);
  finally
    lStrs.Free;
    end;
  end;

function TcCommandHandler.Match(aCommand: String): boolean;
  var
    lName: string;
  begin
  // Custom -- Custom code in derived handler class
  if MatchOperator = moCustom
    then result := false

  // Full
  else if MatchOperator = moFull
    then result := (CompareText(aCommand, Name)=0)

  // Start
  else begin
       lName := Name + ' ';
       result := (CompareText(Copy(aCommand, 1, Length(lName)), lName)=0);
       end;
  end;

procedure TcCommandHandler.Execute(aSession: TcSession; aTokens, aParams, aResponse: TStringlist);
  var
    lNeedsTransaction: boolean;
    lCommit: boolean;
  begin
  // Init context
  fEngine := aSession.Engine;
  fSession := aSession;
  fTokens := aTokens;
  fParams := aParams;
  fResponse := aResponse;

  // Check transaction
  lCommit := true;
  if (not aSession.InTransaction) and (Effect = ceContent)
     then lNeedsTransaction := true
     else lNeedsTransaction := false;

  if lNeedsTransaction
     then aSession.StartTransaction(true {Implicit}, false {not readonly}, false {no StableRead});

  try try

    if (Effect = ceContent) and aSession.Transaction.Readonly_
       then EUsr('Cannot change Claims in a readonly transaction.');

    iExecute;

  except
    on E:Exception do
      begin
      if lNeedsTransaction
         then lCommit := false;
      raise;
      end;
    end;

  finally
    if lNeedsTransaction
       then begin
            if lCommit
               then aSession.Commit
               else aSession.Rollback;
            end;
    end;

  end;

procedure TcCommandHandler.iExecute;
  begin
  end;

initialization
  gCommandDispatcher := TcCommandDispatcher.Create;
finalization
  FreeAndNil(gCommandDispatcher);
end.
