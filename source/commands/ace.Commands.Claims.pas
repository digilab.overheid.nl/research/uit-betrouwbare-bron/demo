// -----------------------------------------------------------------------------
//
// ace.Command.Claims
//
// -----------------------------------------------------------------------------
//
// Commands to register and manipulate Claims.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Commands.Claims;

interface

uses
  Classes, SysUtils,
  ace.Core.Engine,
  ace.Commands.Dispatcher;

type
  TchSetSystemTime =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;

  TchSetAutoIncrementSystemTime =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;

  TchSetUseMVCC =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;

  TchSetCreateAnnotations =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;

  TchRegisterClaim =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;

  TchExpire =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;

  TchDelete =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;

  TchStartValidity =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;

  TchEndValidity =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;


implementation

uses
  common.Utils, common.ErrorHandling;


{ TchSetSystemTime }

constructor TchSetSystemTime.Create;
  begin
  inherited Create(ceTime);
  Name := 'set SystemTime';
  MatchOperator := moFull;

  SetHelpText(
    'set SystemTime' + cLineBreak +
    '-------------------' + cLineBreak +
    'In a normal register the system time would progress with the system clock.' + cLineBreak +
    'Such an implementation prevents easy experimentation. Therefore the' + cLineBreak +
    'time can be set manually in this prototype and is implemented as a positive integer.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'set SystemTime: <time>'
  );

  Keywords.Add('set');
  Keywords.Add('systemtime');
  AutoComplete.Add('set SystemTime: ');
  end;

procedure TchSetSystemTime.iExecute;
  var
    lTime: integer;
  begin
  // Interpret command
  if not MatchPattern('command : time') then SyntaxError;
  CheckParams(['command', 'time']);
  CheckRequiredParams(['time']);

  lTime := StrToIntDef(Params.Values['time'], -1);
  if lTime<0
    then EUsr('Time parameter specifies an invalid time: %s', [Params.Values['Time']]);

  // Set System time
  if Engine.SystemTime>lTime
     then TimelineIntegrityWarning;

  Engine.SystemTime := lTime;

  Response.Add(Format('SystemTime set to: %d', [Engine.SystemTime]));
  end;


{ TchSetAutoIncrementSystemTime }

constructor TchSetAutoIncrementSystemTime.Create;
  begin
  inherited Create(ceTime);
  Name := 'set AutoIncrementSystemTime';
  MatchOperator := moFull;

  SetHelpText(
    'set AutoIncrementSystemTime' + cLineBreak +
    '-------------------' + cLineBreak +
    'The system time will be increased before each content change.' + cLineBreak +
    'When content changes are grouped into an operation, the time will only ' + cLineBreak +
    'change once at the start of the operation.'  + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'set AutoIncrementSystemTime: <y|yes|true|n|no|false> // Default = Yes'
  );

  Keywords.Add('set');
  Keywords.Add('autoincrementsystemtime');
  AutoComplete.Add('set AutoIncrementSystemTime: no');
  end;

procedure TchSetAutoIncrementSystemTime.iExecute;
  var
    lEnabled: boolean;
  begin
  // Interpret command
  if not MatchPattern('command : boolean') then SyntaxError;
  CheckParams(['command', 'boolean']);
  CheckRequiredParams(['boolean']);

  lEnabled := TrueFalseToBoolean(Params.Values['boolean']);

  Engine.SetAutoIncrementSystemTime(lEnabled);

  Response.Add(Format('AutoIncrementSystemTime: %s', [BooleanToYesNo(Engine.AutoIncrementSystemTime)]));
  end;


{ TchSetUseMVCC }

constructor TchSetUseMVCC.Create;
  begin
  inherited Create(ceTime);
  Name := 'set UseMVCC';
  MatchOperator := moFull;

  SetHelpText(
    'set UseMVCC' + cLineBreak +
    '-----------' + cLineBreak +
    'Be default Multi Version Concurrency Control (MVCC) is enabled.' + cLineBreak +
    'For experimentation purposes MVCC can be disbled.' + cLineBreak +
    'This should be done before Claims are manipulated and should not be ' + cLineBreak +
    'changed after manipulation.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'set UseMVCC: <n|no|false> // Default = Yes'
  );

  Keywords.Add('set');
  Keywords.Add('usemvcc');
  AutoComplete.Add('set UseMVCC: no');
  end;

procedure TchSetUseMVCC.iExecute;
  var
    lEnabled: boolean;
  begin
  // Interpret command
  if not MatchPattern('command : boolean') then SyntaxError;
  CheckParams(['command', 'boolean']);
  CheckRequiredParams(['boolean']);

  lEnabled := TrueFalseToBoolean(Params.Values['boolean']);

  Engine.UseMVCC := lEnabled;

  Response.Add(Format('Use Multi Version Concurrency Control: %s', [BooleanToYesNo(Engine.UseMVCC)]));
  end;


{ TchSetCreateAnnotations }

constructor TchSetCreateAnnotations.Create;
  begin
  inherited Create(ceTime);
  Name := 'set CreateAnnotations';
  MatchOperator := moFull;

  SetHelpText(
    'set CreateAnnotations' + cLineBreak +
    '-----------' + cLineBreak +
    'Be default CreateAnnotations is enabled.' + cLineBreak +
    'Annotations will be created when Claims are Started, Ended or Expired as a '+
    'result of adding another Claim to a ClaimSet.' + cLineBreak +
    'For experimentation purposes CreateAnnotations can be disbled.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'set CreateAnnotations: <n|no|false> // Default = Yes'
  );

  Keywords.Add('set');
  Keywords.Add('CreateAnnotations');
  AutoComplete.Add('set CreateAnnotations: no');
  end;

procedure TchSetCreateAnnotations.iExecute;
  var
    lEnabled: boolean;
  begin
  // Interpret command
  if not MatchPattern('command : boolean') then SyntaxError;
  CheckParams(['command', 'boolean']);
  CheckRequiredParams(['boolean']);

  lEnabled := TrueFalseToBoolean(Params.Values['boolean']);

  Engine.UseMVCC := lEnabled;

  Response.Add(Format('Use Multi Version Concurrency Control: %s', [BooleanToYesNo(Engine.UseMVCC)]));
  end;


{ TchRegisterClaim }

constructor TchRegisterClaim.Create;
  begin
  inherited Create(ceContent);
  Name := 'register';
  MatchOperator := moStart;

  SetHelpText(
    'register Claim' + cLineBreak +
    '--------------' + cLineBreak +
    'Claims are the content of the system. The are specified by a list' + cLineBreak +
    'of values. The order of those values should match the Roles in the' + cLineBreak +
    'first ExpressionTemplate of the ClaimType.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'register <claimType>: <value>[,<value>]*' + cLineBreak +
    '' + cLineBreak +
    'Optional parameters:' + cLineBreak +
    '  from: <time>' + cLineBreak +
    '  until: <time>'
  );

  Keywords.Add('register');
  Keywords.Add('claim');
  Keywords.Add('from');
  Keywords.Add('until');
  AutoComplete.Add('register ');
  end;

procedure TchRegisterClaim.iExecute;
  var
    lStr, lClaimTypeName: string;
    lTypeStr: string = '';
    lClaimType: TcClaimType;
    lClaim: TcClaim;
    lValidFrom, lValidUntil: Integer;
  begin
  // Interpret command
  if not MatchPattern('command : claim') then SyntaxError;
  CheckParams(['command', 'claim', 'from', 'until']);

  if Params.IndexOfName('From')>=0
    then lValidFrom := StrToIntDef(Params.Values['From'], -1)
    else lValidFrom := cStartOfTime;
  if lValidFrom<0
    then EUsr('From parameter specifies an invalid time: %s', [Params.Values['From']]);

  if Params.IndexOfName('Until')>=0
    then lValidUntil := StrToIntDef(Params.Values['Until'], -1)
    else lValidUntil := cEndOfTime;
  if lValidUntil<0
    then EUsr('Until parameter specifies an invalid time: %s', [Params.Values['Until']]);

  // Check if claimtype is valid
  SplitString(Tokens[0], ' ', lStr, lClaimTypeName);
  lClaimType := Engine.ConceptualTypes.FindClaimType(lClaimTypeName);
  if not Assigned(lClaimType) then TypeUnknown('Type', lClaimTypeName);

  // Register claim
  lClaim := nil;
  if lClaimType is TcAnnotationType
     then begin
          lClaim := (lClaimType as TcAnnotationType).Add(Session, Params.Values['claim'], lValidFrom, lValidUntil);
          lTypeStr := 'Annotation';
          end
  else if lClaimType is TcClaimType
     then begin
          lClaim := (lClaimType as TcClaimType).Add(Session, Params.Values['claim'], lValidFrom, lValidUntil);
          lTypeStr := 'Claim';
          end
  else EInt('TchRegisterClaim.Execute', 'Unexpected ClaimType');

  Response.Add(Format('%s "%s" created.', [lTypeStr, lClaim.GetExpression]));
  end;


{ TchExpire }

constructor TchExpire.Create;
  begin
  inherited Create(ceContent);
  Name := 'expire';
  MatchOperator := moStart;

  SetHelpText(
    'expire Claim/Claimset' + cLineBreak +
    '---------------------' + cLineBreak +
    'Sets ExpireAt to the current transaction time for the specified Claim(s).' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'expire $ask          -- Will ask for the ID of the Claim to expire.' + cLineBreak +
    'expire <claim id>    -- Will expire the specified Claim.' + cLineBreak +
    'expire <claimSet id> -- Will expire all Claims in the specified ClaimSet.'
  );

  Keywords.Add('expire');
  AutoComplete.Add('expire #');
  end;

procedure TchExpire.iExecute;
  var
    lStr, lElementIDStr: string;
    lElementID: integer;
    lElement: TcElement;
    lClaim: TcClaim;
    lClaimSet: TcClaimSet;
  begin
  // Interpret command
  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command']);

  // Check if reference is valid
  SplitString(Tokens[0], ' ', lStr, lElementIDStr);
  lElementID := CheckOrAskClaimReference(lElementIDStr);
  lElement := Engine.Elements.Find(lElementID);
  if lElement is TcClaim
     then begin
          lClaim := lElement as TcClaim;
          lClaimSet := lClaim.ClaimSet;
          if not Assigned(lClaimSet) then EInt('TchExpire.Execute', 'ClaimSet not assigned');
          lClaimSet.Expire(Session, lClaim);
          Response.Add(Format('Claim %s%d expired.', [cRefChar, lElementID]));
          end
  else if lElement is TcClaimSet
     then begin
          lClaimSet := lElement as TcClaimSet;
          lClaimSet.ExpireAll(Session);
          Response.Add(Format('ClaimSet %s%d expired.', [cRefChar, lElementID]));
          end
  else GeneralError('ID should reference a Claim or ClaimSet.', []);
  end;


{ TchEndValidity }

constructor TchEndValidity.Create;
  begin
  inherited Create(ceContent);
  Name := 'end validity';
  MatchOperator := moStart;

  SetHelpText(
    'end validity <Claim>' + cLineBreak +
    '--------------------' + cLineBreak +
    'Sets the ValidUntil of the specified Claim.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'end validity <claim id>: <valid until>'
  );

  Keywords.Add('end');
  Keywords.Add('validity');
  AutoComplete.Add('end validity #');
  end;

procedure TchEndValidity.iExecute;
  var
    lTime: TcTime;
    lStr, lElementIDStr, lTmpStr: string;
    lElementID: integer;
    lElement: TcElement;
    lClaim: TcClaim;
    lClaimSet: TcClaimSet;
  begin
  // Interpret command
  if not MatchPattern('command : time') then SyntaxError;
  CheckParams(['command', 'time']);
  CheckRequiredParams(['time']);

  lTime := StrToIntDef(Params.Values['time'], -1);
  if lTime<0
    then EUsr('Time parameter specifies an invalid time: %s', [Params.Values['Time']]);

  // Check if claimtype is valid
  SplitString(Tokens[0], ' ', lStr, lTmpStr); // Chops off 'end'
  SplitString(lTmpStr, ' ', lStr, lElementIDStr); // Chops off 'validity'
  lElementID := CheckOrAskClaimReference(lElementIDStr);
  lElement := Engine.Elements.Find(lElementID);
  if lElement is TcClaim
     then begin
          lClaim := lElement as TcClaim;
          lClaimSet := lClaim.ClaimSet;
          if not Assigned(lClaimSet) then EInt('TchEndValidity.Execute', 'ClaimSet not assigned');
          lClaimSet.End_(Session, lClaim, lTime);
          Response.Add(Format('Claim %s%d ended at %d.', [cRefChar, lElementID, lTime]));
          end
  else GeneralError('ID should reference a Claim.', []);
  end;


{ TchStartValidity }

constructor TchStartValidity.Create;
  begin
  inherited Create(ceContent);
  Name := 'start validity';
  MatchOperator := moStart;

  SetHelpText(
    'start validity <Claim>' + cLineBreak +
    '----------------------' + cLineBreak +
    'Sets the ValidFrom of the specified Claim.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'start validity <claim id>: <valid from>'
  );

  Keywords.Add('start');
  Keywords.Add('validity');
  AutoComplete.Add('start validity #');
  end;

procedure TchStartValidity.iExecute;
  var
    lTime: TcTime;
    lStr, lElementIDStr: string;
    lElementID: integer;
    lElement: TcElement;
    lClaim: TcClaim;
    lClaimSet: TcClaimSet;
  begin
  // Interpret command
  if not MatchPattern('command : time') then SyntaxError;
  CheckParams(['command', 'time']);
  CheckRequiredParams(['time']);

  lTime := StrToIntDef(Params.Values['time'], -1);
  if lTime<0
    then EUsr('Time parameter specifies an invalid time: %s', [Params.Values['Time']]);

  // Check if claimtype is valid
  SplitString(Tokens[0], ' ', lStr, lElementIDStr);
  lElementID := CheckOrAskClaimReference(lElementIDStr);
  lElement := Engine.Elements.Find(lElementID);
  if lElement is TcClaim
     then begin
          lClaim := lElement as TcClaim;
          lClaimSet := lClaim.ClaimSet;
          if not Assigned(lClaimSet) then EInt('TchStartValidity.Execute', 'ClaimSet not assigned');
          lClaimSet.Start(Session, lClaim, lTime);
          Response.Add(Format('Claim %s%d started at %d.', [cRefChar, lElementID, lTime]));
          end
  else GeneralError('ID should reference a Claim.', []);
  end;


{ TchDelete }

constructor TchDelete.Create;
  begin
  inherited Create(ceContent);
  Name := 'delete';
  MatchOperator := moStart;

  SetHelpText(
    'delete Claim' + cLineBreak +
    '------------' + cLineBreak +
    'Deletes the specified Claim.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'delete <claim id>'
  );

  Keywords.Add('delete');
  AutoComplete.Add('delete #');
  end;

procedure TchDelete.iExecute;
  var
    lStr, lElementIDStr: string;
    lElementID: integer;
    lElement: TcElement;
    lClaim: TcClaim;
    lClaimSet: TcClaimSet;
  begin
  // Interpret command
  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command']);

  // Check if claimtype is valid
  SplitString(Tokens[0], ' ', lStr, lElementIDStr);
  lElementID := CheckOrAskClaimReference(lElementIDStr);
  lElement := Engine.Elements.Find(lElementID);
  if lElement is TcClaim
     then begin
          lClaim := lElement as TcClaim;
          lClaimSet := lClaim.ClaimSet;
          if not Assigned(lClaimSet) then EInt('TchDelete.Execute', 'ClaimSet not assigned');
          lClaimSet.Delete(Session, lClaim);
          TimelineIntegrityWarning;
          Response.Add(Format('Claim %s%d deleted.', [cRefChar, lElementID]));
          end
  else GeneralError('ID should reference a Claim.', []);
  end;


initialization
  // Register handlers
  gCommandDispatcher.Add(TchSetSystemTime.Create);
  gCommandDispatcher.Add(TchSetAutoIncrementSystemTime.Create);
  gCommandDispatcher.Add(TchSetUseMVCC.Create);
  gCommandDispatcher.Add(TchRegisterClaim.Create);
  gCommandDispatcher.Add(TchExpire.Create);
  gCommandDispatcher.Add(TchStartValidity.Create);
  gCommandDispatcher.Add(TchEndValidity.Create);
  gCommandDispatcher.Add(TchDelete.Create);
end.

