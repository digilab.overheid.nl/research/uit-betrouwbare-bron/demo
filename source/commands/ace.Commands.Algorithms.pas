// -----------------------------------------------------------------------------
//
// ace.Command.Algorithms
//
// -----------------------------------------------------------------------------
//
// Commands to group ClaimTypes into a grouped model and to generate a yaml
// specification.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Commands.Algorithms;

interface

uses
  Classes, Generics.Collections,
  ace.Core.Engine,
  ace.Commands.Dispatcher;

type
  TchGroup =
    class (TcCommandHandler)
      private
        fStepNr: integer;
        // function ClaimTypeIsReferenced(aClaimType: TcClaimType): boolean;
      protected
        procedure ClearClasses;
        procedure GroupClaimTypes;

        procedure iEndObjects(aObjectSet: TcObjectSet; aRegisteredAt, aValidUntil: TcTime);
        procedure iExpireObjects(aObjectSet: TcObjectSet; aExpiredAt: TcTime);
        procedure GroupTuples;

        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;

  TchYaml =
    class (TcCommandHandler)
      protected
        procedure GenerateYaml(aDoc: TStringlist);

        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;

  { TchSetGroupTuples }

  TchSetGroupTuples =
    class (TcCommandHandler)
      protected
        procedure iExecute; override;
      public
        constructor Create; reintroduce;
      end;

implementation

uses
  common.Utils, common.FileUtils, common.RadioStation,
  common.SimpleLists, common.ErrorHandling,
  ace.Debug, ace.Settings,
  ace.API.Server, ace.API.SimpleQuery,
  SysUtils;

{ TchGroup }

constructor TchGroup.Create;
  begin
  inherited Create(ceStructure);
  Name := 'group';
  MatchOperator := moFull;

  SetHelpText(
    'group' + cLineBreak +
    '-----' + cLineBreak +
    'Groups ClaimTypes into Classes.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'group'
  );

  Keywords.Add('group');
  AutoComplete.Add('group');
  end;

procedure TchGroup.iExecute;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command']);

  fStepNr := 1;
  ClearClasses;
  GroupClaimTypes;

  if Engine.GroupTuples
     then GroupTuples;
  end;

procedure TchGroup.ClearClasses;
  begin
  if Engine.Classes.Count>0
     then begin
          // Step: Free existing classes
          Response.Add(Format('%d. Clear previously created Classes.', [fStepNr]));
          Inc(fStepNr);
          Engine.Classes.FreeAndClear;
          end;
  end;

(*
function TchGroup.ClaimTypeIsReferenced(aClaimType: TcClaimType): boolean;
  var
    ct, r: integer;
    lClaimType: TcClaimType;
    lRole: TcRole;
  begin
  result := false;

  for ct := 0 to Engine.ConceptualTypes.Count-1 do
    begin
    if not (Engine.ConceptualTypes[ct] is TcClaimType)
      then continue;

    lClaimType := Engine.ConceptualTypes[ct] as TcClaimType;

    if lClaimType = aClaimType then continue;

    for r := 0 to lClaimType.Roles.Count-1 do
      begin
      lRole := lClaimType.Roles[r];
      if lRole.Type_ = aClaimType
         then begin
              result := true;
              exit;
              end;
      end;
    end;
  end;
*)

procedure TchGroup.GroupClaimTypes;
  var
    i, ct, cl, r: integer;
    lClaimTypes: TList<TcClaimType>;
    lType: TcType;
    lClaimType: TcClaimType;
    lClass, lReferencedClass: TcClass;
    lRole: TcRole;
  begin
  lClaimTypes := TList<TcClaimType>.Create;
  try
    // Create a list with all ClaimTypes
    for i := 0 to Engine.ConceptualTypes.Count-1 do
      begin
      if Engine.ConceptualTypes[i] is TcClaimType
         then begin
              lClaimType := Engine.ConceptualTypes[i] as TcClaimType;
              if lClaimType.IsMetaType then continue;
              lClaimTypes.Add(lClaimType);
              end;
      end;

    // Step: Create Classes for all ClaimTypes that are ObjectTypes
    Response.Add(Format('%d. Create Classes for ClaimType that are referenced by other ClaimTypes (ObjectTypes).', [fStepNr]));
    Inc(fStepNr);
    ct := 0;
    while ct < lClaimTypes.Count do
      begin
      lClaimType := lClaimTypes[ct];

      if not lClaimType.IsObjectType
         then begin
              inc(ct);
              continue;
              end;

      lClass := TcClass.Create(Engine, lClaimType);
      lClass.AfterInit;
      Engine.RelationTypes.Add(lClass.ID, lClass);
      Engine.Classes.Add(lClass.Name, lClass);
      lClaimTypes.Delete(ct);

      if lClass.PluralName=''
         then Response.Add(Format('  WARNING: Class %s has no pluralname.', [lClass.Name]));

      Response.Add(Format('  Created %s', [lClass.Name]));
      end;

    // Step: Group remaining ClaimTypes (when possible)
    Response.Add(Format('%d. Group remaining ClaimTypes - when possible.', [fStepNr]));
    Inc(fStepNr);
    i := 0;
    while i < lClaimTypes.Count do
      begin
      lClaimType := lClaimTypes[i];

      inc(i); // Increment counter assuming ClaimType cannot be grouped

      // Condition 1: ClaimType must be binary
      if lClaimType.Roles.Count<>2 then continue;

      // Condition 2: Identity must span a single role
      if lClaimType.Identity.Count<>1 then continue;

      // Condition 3: Identifying role must be referencing a Class
      lRole := lClaimType.Identity[0];
      lClass := Engine.Classes.Find(lRole.Type_.Name);
      if not Assigned(lClass) then continue;

      dec(i); // Decrement counter, this ClaimType will be grouped and removed from the list

      // Group ClaimType into Class
      lClass.AddClaimType(lClaimType, lRole);
      lClaimTypes.Delete(i);

      Response.Add(Format('  Grouped %s into %s', [lClaimType.Name, lClass.Name]));
      end;

    // Step: Create Classes for all remaining ClaimTypes
    Response.Add(Format('%d. Create Classes for remaining ClaimTypes.', [fStepNr]));
    Inc(fStepNr);
    for i := 0 to lClaimTypes.Count-1 do
      begin
      lClaimType := lClaimTypes[i];

      // For now, skipp classes with meta roles
      if lClaimType.HasMetaRole
         then begin
              Response.Add(Format('  WARNING: Claimtype %s ignored. It has a role referencing a meta construct.', [lClaimType.Name]));
              Continue;
              end;

      lClass := TcClass.Create(Engine, lClaimType);
      lClass.AfterInit;
      Engine.RelationTypes.Add(lClass.ID, lClass);
      Engine.Classes.Add(lClass.Name, lClass);
      if lClass.PluralName=''
         then Response.Add(Format('  WARNING: Class %s has no pluralname.', [lClass.Name]));
      Response.Add(Format('  Created %s', [lClass.Name]));
      end;

    // Step: Change references to ClaimTypes into references to their corresponding Classes.
    Response.Add(Format('%d. Change references to ClaimTypes into references to their corresponding Classes.', [fStepNr]));
    Inc(fStepNr);
    for cl := 0 to Engine.Classes.Count-1 do
      begin
      lClass := Engine.Classes[cl];

      // A. Make sure Roles either reference a LabelType or a Class
      for r := 0 to lClass.Roles.Count-1 do
        begin
        lRole := lClass.Roles[r];
        lType := lRole.Type_;
        if lType is TcClaimType
           then begin
                lReferencedClass := Engine.Classes.Find(lType.Name);
                lRole.ChangeType(lReferencedClass);
                end;
        end;

      // B. Make sure SuperTypes reference their corresponding Class
      if Assigned(lClass.SuperType)
         then begin
              lReferencedClass := Engine.Classes.Find(lClass.SuperType.Name);
              lClass.ChangeSuperType(lReferencedClass);
              end;
      end;


  finally
    lClaimTypes.Clear;
    lClaimTypes.Free;

    if Assigned(gRadioStation)
       then gRadioStation.BroadCast(cRadioEvent_Class_Created, nil);
    end;
  end;

type
  TcSortedClaims =
    class (TcSortedList)
      private
        fClass: TcClass;

      protected
        function Compare (const aObjectA, aObjectB: TObject): integer; overload; override;
        function Compare (const aObject: TObject; const aKeyValue: string): integer; override;

      public
        constructor Create(aClass: TcClass);
      end;

{ TcSortedClaims }

constructor TcSortedClaims.Create(aClass: TcClass);
  begin
  inherited Create;
  fClass := aClass;
  IgnoreDuplicates := false;
  end;

function TcSortedClaims.Compare(const aObjectA, aObjectB: TObject): integer;
  var
    lClaimA, lClaimB: TcClaim;
  begin
  lClaimA := TcClaim(aObjectA);
  lClaimB := TcClaim(aObjectB);

  // First compare on RegisteredAt
  if lClaimA.RegisteredAt > lClaimB.RegisteredAt
     then result := 1
  else if lClaimA.RegisteredAt < lClaimB.RegisteredAt
     then result := -1
  else begin
       // No difference on RegisteredAt

       // Compare on ValidFrom
       if lClaimA.ValidFrom > lClaimB.ValidFrom
          then result := 1
       else if lClaimA.ValidFrom < lClaimB.ValidFrom
          then result := -1
       else begin
            // No difference on ValidFrom

            // Compare on sequence nr of ClaimType in class
            if fClass.SequenceNrOfClaimType(lClaimA.ClaimType) > fClass.SequenceNrOfClaimType(lClaimB.ClaimType)
               then result := 1
            else if fClass.SequenceNrOfClaimType(lClaimA.ClaimType) < fClass.SequenceNrOfClaimType(lClaimB.ClaimType)
               then result := -1
               else result := 0;
            end;
       end;
  end;

function TcSortedClaims.Compare(const aObject: TObject; const aKeyValue: string): integer;
  begin
  result := 0;
  EDev('Working with KeyValues not supported on TcSortedClaims.');
  end;

procedure TchGroup.iEndObjects(aObjectSet: TcObjectSet; aRegisteredAt, aValidUntil: TcTime);
  var
    o: integer;
    lObject: TcObject;
  begin
  for o := 0 to aObjectSet.Objects.Count-1 do
    begin
    lObject := aObjectSet.Objects[o];
    if (lObject.RegisteredAt = aRegisteredAt) and (lObject.ValidUntil = cEndOfTime)
       then lObject.ValidUntil := aValidUntil;
    end;
  end;

procedure TchGroup.iExpireObjects(aObjectSet: TcObjectSet; aExpiredAt: TcTime);
  var
    o: integer;
    lObject: TcObject;
  begin
  for o := 0 to aObjectSet.Objects.Count-1 do
    begin
    lObject := aObjectSet.Objects[o];
    if (lObject.ExpiredAt = cEndOfTime)
       then lObject.ExpiredAt := aExpiredAt;
    end;
  end;

procedure TchGroup.GroupTuples;
  var
    cl, cs, gct, gc, c, r: integer;
    lClass: TcClass;
    lClaimType: TcClaimType;
    lMainClaimSet, lClaimSet: TcClaimSet;
    lMainClaim, lClaim: TcClaim;
    lSortedClaims: TcSortedClaims;
    lObjectSet: TcObjectSet;
    lObject, lPreviousObject: TcObject;
    lRole: TcRole;
    lIndex, lPrevIndex: integer;
  begin
  Session.StartTransaction(true {Implicit}, false {Not readonly}, false {Not stable});
  try
    // Step: Derive objects
    Response.Add(Format('%d. Derive Objects for each Class.', [fStepNr]));
    Inc(fStepNr);

    // Iterate over all classes
    for cl := 0 to Engine.Classes.Count-1 do
      begin
      lClass := Engine.Classes[cl];
      if lClass.Name[1]=cMetaChar then Continue; // Ignore meta classes

      Response.Add(Format('  Processing Class: %s', [lClass.Name]));
      Debug('GroupTuples', 'Class: %s', [lClass.Name]);
      DebugIncIndent('GroupTuples');
      Debug('GroupTuples', 'MainClaimType: %s', [lClass.MainClaimType.Name]);

      // Iterate over every ClaimSet in the MainClaimType
      for cs := 0 to lClass.MainClaimType.ClaimSetList.Count-1 do
        begin
        lMainClaimSet := lClass.MainClaimType.ClaimSetList[cs];
        Debug('GroupTuples', 'MainClaimSet.IdentityStr: %s', [lMainClaimSet.IdentityStr]);

        // Every MainClaimSet contains only one Claim (since every MainClaim
        // has a Identity that spans all roles, there can only be one instance).
        Assert(lMainClaimSet.SourceClaims.Count<=1);
        if lMainClaimSet.SourceClaims.Count=0 then continue;
        lMainClaim := lMainClaimSet.SourceClaims[0];
        Debug('GroupTuples', 'MainClaim.ID: %s', [lMainClaim.RefIDStr]);

        // Create a Sorted Collection of all claims
        // Sorting:
        // - First on RegisteredAt (low to high)
        // - Second on ValidFrom (low to high)
        // - Third on Order of ClaimType in Class
        // (Sorting performed by TcSortedClaims, see Compare method)
        lSortedClaims := TcSortedClaims.Create(lClass);

        // Add all claims from the corresponding ClaimSets in the GroupedClaimTypes
        for gct := 0 to lClass.GroupedClaimTypes.Count-1 do
          begin
          lClaimType := lClass.GroupedClaimTypes.Objects[gct];
          Debug('GroupTuples', 'GroupedClaimType: %s', [lClaimType.Name]);
          DebugIncIndent('GroupTuples');

          lClaimSet := lClaimType.ClaimSetList.Find(lMainClaim.RefIDStr);
          if not Assigned(lClaimSet) then continue;
          for gc := 0 to lClaimSet.SourceClaims.Count-1 do
            begin
            lClaim := lClaimSet.SourceClaims[gc];
            lSortedClaims.Add(lClaim);
            Debug('GroupTuples', 'Claim: RA:%d, VF:%d, ValueStr:%s', [lClaim.RegisteredAt, lClaim.ValidFrom, lClaim.ValueStr]);
            end;
          DebugDecIndent('GroupTuples');
          end;

        Debug('GroupTuples', 'Sorted ClaimList');
        DebugIncIndent('GroupTuples');
        for c := 0 to lSortedClaims.Count-1 do
          begin
          lClaim := TcClaim(lSortedClaims[c]);
          Debug('GroupTuples', 'Claim: RA:%d, VF:%d, Type: %s, ValueStr:%s', [
            lClaim.RegisteredAt, lClaim.ValidFrom, lClaim.ClaimType.Name, lClaim.ValueStr
          ]);
          end;
        DebugDecIndent('GroupTuples');

        // Create an ObjectSet in the Class
        lObjectSet := TcObjectSet.Create(lClass, lMainClaim.IdentityStr);
        lClass.ObjectSetList.Add(lObjectSet.IdentityStr, lObjectSet);
        Debug('GroupTuples', 'ObjectSet.IdentityStr: %s', [lObjectSet.IdentityStr]);
        DebugIncIndent('GroupTuples');

        // Create Objects and add them to the ObjectSet
        lPreviousObject := nil;
        while lSortedClaims.Count>0 do
          begin
          lClaim := TcClaim(lSortedClaims[0]);

          // Build an object
          lObject := TcObject.Create(Session, lClass);
          lObject.IdentityStr := lObjectSet.IdentityStr;
          lObject.RegisteredAt := lClaim.RegisteredAt;
          lObject.ValidFrom := lClaim.ValidFrom;
          lObjectSet.Objects.Add(lObjectSet.IdentityStr, lObject);
          Debug('GroupTuples', 'Object: RA:%d, VF:%d', [lObject.RegisteredAt, lObject.ValidFrom]);
          DebugIncIndent('GroupTuples');

          // Add identification roles
          for r := 0 to lMainClaim.RoleValues.Count-1 do
            begin
            lObject.RoleValues.Add(lMainClaim.RoleValues.NameByIndex(r), lMainClaim.RoleValues.ValueByIndex(r), lMainClaim.RoleValues.Objects[r]);
            lObject.RoleValueClaims.Add(lMainClaim.RoleValues.NameByIndex(r), lMainClaim.RoleValues.ValueByIndex(r), lMainClaim);
            end;

          // Group all claims with the same RegisteredAt and ValidFrom into
          // the object
          repeat
            // Add value from Claim to the Object
            for r := 0 to lClaim.RoleValues.Count-1 do
              begin
              lRole := lClaim.RoleValues.Objects[r];
              if lRole.Type_ = lClass.MainClaimType
                 then continue;
              lObject.RoleValues.Add(lClaim.RoleValues.NameByIndex(r), lClaim.RoleValues.ValueByIndex(r), lClaim.RoleValues.Objects[r]);
              lObject.RoleValueClaims.Add(lClaim.RoleValues.NameByIndex(r), lClaim.RoleValues.ValueByIndex(r), lClaim);
              end;

            // Remove this claim, it's processed
            lSortedClaims.RemoveAt(0);

            // Are there claims left?
            if lSortedClaims.Count>0
               then lClaim := TcClaim(lSortedClaims[0])
               else lClaim := nil;

            // Continue as long as there is a next Claim and RegisteredAt and ValidFrom are the same
            until not Assigned(lClaim) or (lClaim.RegisteredAt <> lObject.RegisteredAt) or (lClaim.ValidFrom <> lObject.ValidFrom);

          // Interpret changes in RegisteredAt and ValidFrom
          if Assigned(lClaim)
             then begin
                  if (lClaim.RegisteredAt = lObject.RegisteredAt) and (lClaim.ValidFrom <> lObject.ValidFrom)
                     then begin
                          // Only the ValidFrom changed
                          // All unended objects with the same RegisteredAt should be ended at lClaim.ValidFrom
                          iEndObjects(lObjectSet, lObject.RegisteredAt, lClaim.ValidFrom);
                          end
                  else if (lClaim.RegisteredAt <> lObject.RegisteredAt)
                     then begin
                          // The RegisteredAt changes
                          // Expire all 'unexpired' objects at the new RegisteredAt
                          iExpireObjects(lObjectSet, lClaim.RegisteredAt);
                          end;
                  end;

          // Fill in the blanks
          if Assigned(lPreviousObject)
             then begin
                  for r := 0 to lClass.Roles.Count-1 do
                    begin
                    lRole := lClass.Roles[r];
                    lIndex := lObject.RoleValues.IndexOfName(lRole.Name);
                    lPrevIndex := lPreviousObject.RoleValues.IndexOfName(lRole.Name);
                    if (lIndex<0) and (lPrevIndex>=0)
                       then begin
                            lObject.RoleValues.Add(lRole.Name, lPreviousObject.RoleValues.Values[lRole.Name], lRole);
                            lObject.RoleValueClaims.Add(lRole.Name, lPreviousObject.RoleValues.Values[lRole.Name], lPreviousObject.RoleValueClaims.Objects[lPrevIndex]);
                            end;
                    end;
                  end;

          // Derive the ValueStr of the Object
          lObject.ValueStr := lObject.TupleStrInRoleOrder(lObject.Class_, lObject.RoleValues);
          DebugIncIndent('GroupTuples');
          Debug('GroupTuples', 'ValueStr: %s', [lObject.ValueStr]);
          DebugIncIndent('GroupTuples');
          for r := 0 to lClass.Roles.Count-1 do
            begin
            lRole := lClass.Roles[r];
            lIndex := lObject.RoleValueClaims.IndexOfName(lRole.Name);
            if lIndex > -1 then
              Debug('GroupTuples', '%s: %s', [lObject.RoleValueClaims.NameByIndex(lIndex), lObject.RoleValueClaims.Objects[lIndex].GetExpressionFmt]);
            end;
          DebugDecIndent('GroupTuples');
          DebugDecIndent('GroupTuples');

          // Remember this object
          lPreviousObject := lObject;

          DebugDecIndent('GroupTuples');
          end;
        DebugDecIndent('GroupTuples');

        Assert(lSortedClaims.Count=0);
        FreeAndNil(lSortedClaims);
        end;

      DebugDecIndent('GroupTuples');
      end;

    Session.Commit;

  except
    Session.Rollback;
    raise;
    end;
  end;


{ TchYaml }

constructor TchYaml.Create;
  begin
  inherited Create(cePortability);
  Name := 'yaml';
  MatchOperator := moFull;

  SetHelpText(
    'yaml' + cLineBreak +
    '----' + cLineBreak +
    'Create a basic CRUD-API yaml for the classes.' + cLineBreak +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'yaml'
  );

  Keywords.Add('yaml');
  AutoComplete.Add('yaml');
  end;

procedure TchYaml.iExecute;
  var
    lDoc: TStringlist;
    lFilename: string;
  begin
  inherited;

  // Interpret command
  if not MatchPattern('command') then SyntaxError;
  CheckParams(['command']);

  if not gSettings.AskProjectFolderWhenEmpty
     then EUsr('Cannot generate Yaml file without an active project.');

  lDoc := TStringlist.Create;
  GenerateYaml(lDoc);
  lFilename := TPath.Combine(gSettings.ProjectFolder, 'simpel_query_api.yaml');
  lDoc.SaveToFile(lFilename);
  end;

procedure TchYaml.GenerateYaml(aDoc: TStringlist);

  procedure Add(aStrLst: TStringlist; aStr: string); overload;
    begin
    aStrLst.Add(aStr);
    end;

  procedure Add(aStrLst: TStringlist; aStr: string; aArgs: array of const); overload;
    begin
    Add(aStrLst, Format(aStr, aArgs));
    end;

  var
    c, r, l: integer;
    lClass: TcClass;
    lRole: TcRole;
    lLabelType: TcLabelType;
    lLabelTypes: TStringlist;
    lIntro, lPaths, lComps, lStruc: TStringlist;
  begin
  lLabelTypes := TStringlist.Create;
  lLabelTypes.Sorted := true;
  lLabelTypes.Duplicates := dupIgnore;

  lIntro := TStringlist.Create;
  lPaths := TStringlist.Create;
  lComps := TStringlist.Create;
  lStruc := TStringlist.Create;
  try
    // -- Intro
    Add(lIntro, 'openapi: "3.1.0"');
    Add(lIntro, 'servers:');
    Add(lIntro, '- description: "Test server"');
    Add(lIntro, Format('  url: "http://%s:%d"', [cIPAddress, cSimpleQuery_Port]));
    Add(lIntro, 'info:');
    Add(lIntro, '  title: "%s"', [gSettings.ProjectName]);
    Add(lIntro, '  description: "Test API to access the data of %s in the Atomic Claim Engine."', [gSettings.ProjectName]);
    Add(lIntro, '  version: 0.0.1');
    Add(lIntro, '  contact:');
    Add(lIntro, '    url: "https://atomicclaimengine.org"');
    Add(lIntro, '    email: "api@atomicclaimengine.org"');
    Add(lIntro, '  license:');
    Add(lIntro, '    name: "European Union Public License, version 1.2 (EUPL-1.2)"');
    Add(lIntro, '    url: "https://eupl.eu/1.2/nl/"');

    // -- Paths
    Add(lPaths, 'paths:');
    for c := 0 to Engine.Classes.Count-1 do
      begin
      lClass := Engine.Classes[c];
      if lClass.Name[1]=cMetaChar then continue;

      // GET - Identity + TimeTravel
      Add(lPaths, '  /%s:', [toIdentifier(lClass.GetOrDerivePluralName)]);
      Add(lPaths, '    get:');
      Add(lPaths, '      operationId: "get%s"', [toIdentifier(lClass.Name)]);
      Add(lPaths, '      description: "Operation to retrieve information about a(n) %s."', [toIdentifier(lClass.Name)]);
      Add(lPaths, '      parameters:');

      // Identity
      for r := 0 to lClass.Identity.Count-1 do
        begin
        lRole := lClass.Identity[r];
        Add(lPaths, '      - name: "%s"', [lRole.Name]);
        Add(lPaths, '        in: "query"');
        Add(lPaths, '        required: true');
        Add(lPaths, '        schema:');
        if lRole.Type_ is TcLabelType
           then Add(lPaths, '          $ref: "#/components/schemas/%s"', [lRole.Type_.Name])
           else Add(lPaths, '          $ref: "#/components/schemas/TechnicalID"');
        end;

      // TimeTravel
      Add(lPaths, '      - name: "ValidOn"');
      Add(lPaths, '        in: "query"');
      Add(lPaths, '        required: false');
      Add(lPaths, '        schema:');
      Add(lPaths, '          $ref: "#/components/schemas/SimpleTime"');
      Add(lPaths, '      - name: "KnowledgeOf"');
      Add(lPaths, '        in: "query"');
      Add(lPaths, '        required: false');
      Add(lPaths, '        schema:');
      Add(lPaths, '          $ref: "#/components/schemas/SimpleTime"');

      Add(lPaths, '      responses:');
      Add(lPaths, '        200:');
      Add(lPaths, '          description: "Ok"');
      Add(lPaths, '          headers:');
      Add(lPaths, '            api-version:');
      Add(lPaths, '              $ref: "#/components/headers/api_version"');
      Add(lPaths, '            warning:');
      Add(lPaths, '              $ref: "#/components/headers/warning"');
      Add(lPaths, '          content:');
      Add(lPaths, '            application/json:');
      Add(lPaths, '              schema:');
      Add(lPaths, '                $ref: "#/components/schemas/%s"', [toIdentifier(lClass.Name)]);
      Add(lPaths, '        400:');
      Add(lPaths, '          $ref: "#/components/responses/400"');
      Add(lPaths, '      tags:');
      Add(lPaths, '      - "%s"', [toIdentifier(lClass.GetOrDerivePluralName)]);

      // GET - Technical ID
      Add(lPaths, '  /%s/{id}:', [toIdentifier(lClass.GetOrDerivePluralName)]);
      Add(lPaths, '    get:');
      Add(lPaths, '      operationId: "get%s_tid"', [toIdentifier(lClass.Name)]);
      Add(lPaths, '      description: "Operation to retrieve information about a(n) %s."', [toIdentifier(lClass.Name)]);
      Add(lPaths, '      parameters:');
      Add(lPaths, '      - in: "path"');
      Add(lPaths, '        name: "id"');
      Add(lPaths, '        description: "Unique technical key for a(n) %s."', [toIdentifier(lClass.Name)]);
      Add(lPaths, '        required: true');
      Add(lPaths, '        schema:');
      Add(lPaths, '          $ref: "#/components/schemas/TechnicalID"');
      Add(lPaths, '      responses:');
      Add(lPaths, '        200:');
      Add(lPaths, '          description: "Ok"');
      Add(lPaths, '          headers:');
      Add(lPaths, '            api-version:');
      Add(lPaths, '              $ref: "#/components/headers/api_version"');
      Add(lPaths, '            warning:');
      Add(lPaths, '              $ref: "#/components/headers/warning"');
      Add(lPaths, '          content:');
      Add(lPaths, '            application/json:');
      Add(lPaths, '              schema:');
      Add(lPaths, '                $ref: "#/components/schemas/%s"', [toIdentifier(lClass.Name)]);
      Add(lPaths, '        400:');
      Add(lPaths, '          $ref: "#/components/responses/400"');
      Add(lPaths, '      tags:');
      Add(lPaths, '      - "%s"', [toIdentifier(lClass.GetOrDerivePluralName)]);

      // Schema's
      Add(lStruc, '    %s:', [toIdentifier(lClass.Name)]);
      Add(lStruc, '      type: "object"');
      Add(lStruc, '      description: "%s"', [toIdentifier(lClass.Name)]);
      Add(lStruc, '      properties:');
      for r := 0 to lClass.Roles.Count-1 do
        begin
        lRole := lClass.Roles[r];
        if lRole.Type_ is TcLabelType
           then begin
                lLabelTypes.Add(lRole.Type_.Name);
                Add(lStruc, '        %s:', [toIdentifier(lRole.Name)]);
                Add(lStruc, '          $ref: "#/components/schemas/%s"', [toIdentifier(lRole.Type_.Name)]);
                end
           else begin
                Add(lStruc, '        %sId:', [toIdentifier(lRole.Name)]);
                Add(lStruc, '          $ref: "#/components/schemas/TechnicalID"');
                end;
        end;
      end;

    // LabelTypes
    for l := 0 to lLabelTypes.Count-1 do
      begin
      lLabelType := Engine.ConceptualTypes.FindLabelType(lLabelTypes[l]);
      if not Assigned(lLabelType)
         then EInt('TchYaml.GenerateYaml', 'Could not find LabelType');

      Add(lStruc, '    %s:', [toIdentifier(lLabelType.Name)]);
      Add(lStruc, '      type: "%s"', [toIdentifier(lLabelType.DataType
      .Name)]);
      end;

    // Components
    Add(lComps, 'components:');
    Add(lComps, '  headers:');
    Add(lComps, '    api_version:');
    Add(lComps, '      schema:');
    Add(lComps, '        type: string');
    Add(lComps, '        description: Geeft een specifieke API-versie aan in de context van een specifieke aanroep.');
    Add(lComps, '    warning:');
    Add(lComps, '      schema:');
    Add(lComps, '        type: string');
    Add(lComps, '        description: Zie RFC 7234.');
    Add(lComps, '  responses:');
    Add(lComps, '    ''400'':');
    Add(lComps, '      description: Bad Request');
    Add(lComps, '      headers:');
    Add(lComps, '        api-version:');
    Add(lComps, '          $ref: "#/components/headers/api_version"');
    Add(lComps, '      content:');
    Add(lComps, '        application/problem+json:');
    Add(lComps, '          schema:');
    Add(lComps, '            $ref: "#/components/schemas/BadRequestFoutbericht"');
    Add(lComps, '  schemas:');
    Add(lComps, '    Foutbericht:');
    Add(lComps, '      type: object');
    Add(lComps, '      description: Terugmelding bij een fout. JSON representatie in lijn met [RFC7807](https://tools.ietf.org/html/rfc7807).');
    Add(lComps, '      properties:');
    Add(lComps, '        type:');
    Add(lComps, '          type: string');
    Add(lComps, '          format: uri');
    Add(lComps, '          description: Link naar meer informatie over deze fout');
    Add(lComps, '        title:');
    Add(lComps, '          type: string');
    Add(lComps, '          description: Beschrijving van de fout');
    Add(lComps, '        status:');
    Add(lComps, '          type: integer');
    Add(lComps, '          description: Http status code');
    Add(lComps, '        detail:');
    Add(lComps, '          type: string');
    Add(lComps, '          description: Details over de fout');
    Add(lComps, '        instance:');
    Add(lComps, '          type: string');
    Add(lComps, '          format: uri');
    Add(lComps, '          description: Uri van de aanroep die de fout heeft veroorzaakt');
    Add(lComps, '        code:');
    Add(lComps, '          type: string');
    Add(lComps, '          description: Systeemcode die het type fout aangeeft');
    Add(lComps, '          minLength: 1');
    Add(lComps, '    InvalidParams:');
    Add(lComps, '      type: object');
    Add(lComps, '      description: Details over fouten in opgegeven parameters');
    Add(lComps, '      properties:');
    Add(lComps, '        type:');
    Add(lComps, '          type: string');
    Add(lComps, '          format: uri');
    Add(lComps, '        name:');
    Add(lComps, '          type: string');
    Add(lComps, '          description: Naam van de parameter');
    Add(lComps, '        code:');
    Add(lComps, '          type: string');
    Add(lComps, '          description: Systeemcode die het type fout aangeeft');
    Add(lComps, '          minLength: 1');
    Add(lComps, '        reason:');
    Add(lComps, '          type: string');
    Add(lComps, '          description: Beschrijving van de fout op de parameterwaarde');
    Add(lComps, '    BadRequestFoutbericht:');
    Add(lComps, '      allOf:');
    Add(lComps, '      - $ref: "#/components/schemas/Foutbericht"');
    Add(lComps, '      - type: "object"');
    Add(lComps, '        properties:');
    Add(lComps, '          invalidParams:');
    Add(lComps, '            description: Foutmelding per fout in een parameter. Alle gevonden fouten worden ��n keer teruggemeld.');
    Add(lComps, '            type: array');
    Add(lComps, '            items:');
    Add(lComps, '              $ref: "#/components/schemas/InvalidParams"');

    Add(lComps, '    TechnicalID:');
    Add(lComps, '      type: "string"');
    Add(lComps, '      description: "Unieke technische identificerende code, primair bedoeld voor gebruik bij interacties tussen IT-systemen."');
    Add(lComps, '      readOnly: true');
    Add(lComps, '      maxLength: 40');
    Add(lComps, '      minLength: 1');

    Add(lStruc, '    SimpleTime:');
    Add(lStruc, '      type: "integer"');

  finally
    aDoc.AddStrings(lIntro);
    lIntro.Free;

    aDoc.AddStrings(lPaths);
    lPaths.Free;

    aDoc.AddStrings(lComps);
    lComps.Free;

    aDoc.AddStrings(lStruc);
    lStruc.Free;
    end;
  end;

{ TchSetGroupTuples }
constructor TchSetGroupTuples.Create;
  begin
  inherited Create(ceStructure);

  Name := 'set GroupTuples';
  MatchOperator := moFull;

  SetHelpText(
    'set GroupTuples' + cLineBreak +
    '-------------------' + cLineBreak +
    'By default the grouping algorithm will only group claimtypes.' + cLineBreak +
    'If this option is set to ‘yes’ it will also group the claims. Effectively creating instances for each class that has been created during grouping.' +
    '' + cLineBreak +
    'Syntax:' + cLineBreak +
    'set GroupTuples: <y|yes|true|n|no|false> // Default = no'
  );

  Keywords.Add('set');
  Keywords.Add('grouptuples');
  AutoComplete.Add('set GroupTuples: yes');
  end;

procedure TchSetGroupTuples.iExecute;
  var
    lEnabled: boolean;
  begin
  // Interpret command
  if not MatchPattern('command : boolean') then SyntaxError;
  CheckParams(['command', 'boolean']);
  CheckRequiredParams(['boolean']);

  lEnabled := TrueFalseToBoolean(Params.Values['boolean']);

  Engine.GroupTuples := lEnabled;

  Response.Add(Format('GroupTuples: %s', [BooleanToYesNo(Engine.GroupTuples)]));
  end;

initialization
  // Register handlers
  gCommandDispatcher.Add(TchGroup.Create);
  gCommandDispatcher.Add(TchYaml.Create);
  gCommandDispatcher.Add(TchSetGroupTuples.Create);
end.
