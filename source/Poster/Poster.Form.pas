// -----------------------------------------------------------------------------
//
// Poster.Form
//
// -----------------------------------------------------------------------------
//
// Simple form to post json files to the Operations API at http://127.0.0.1:4200/
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------
unit Poster.Form;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, EditBtn, ExtCtrls, StdCtrls, SynEdit;

type

  { TForm1 }

  TForm1 = class(TForm)
    btnExecute: TButton;
    eURL: TEdit;
    eFileNameEdit: TFileNameEdit;
    ListBox: TListBox;
    Panel1: TPanel;
    Panel2: TPanel;
    SynEdit: TSynEdit;
    procedure btnExecuteClick(Sender: TObject);
    procedure eFileNameEditChange(Sender: TObject);
    procedure ListBoxClick(Sender: TObject);
  private

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

uses
  IniFiles, fphttpclient;

  { TForm1 }

procedure TForm1.eFileNameEditChange(Sender: TObject);
var
  lIni: TIniFile;
  lItems: TStringList;
begin
  lIni := TIniFile.Create(eFileNameEdit.Text);

  lItems := TStringList.Create;
  lIni.ReadSections(lItems);
  ListBox.Items.Assign(lItems);
  lItems.Free;
  lIni.Free;
end;

procedure TForm1.btnExecuteClick(Sender: TObject);
var
  lHTTPClient: TFPHTTPClient;
begin
  lHTTPClient := TFPHTTPClient.Create(nil);
  try
    lHTTPClient.RequestBody := TStringStream.Create(SynEdit.Lines.Text);
    lHTTPClient.AddHeader('content-type', 'application/json');
    lHTTPClient.Post(eURL.Text);
    lHTTPClient.RequestBody.Free;

  finally
    lHTTPClient.Free;
  end;
end;


procedure TForm1.ListBoxClick(Sender: TObject);
var
  lIni: TIniFile;
  lSection: String;
begin
  lIni := TIniFile.Create(eFileNameEdit.Text);

  lSection := Listbox.Items[Listbox.ItemIndex];

  eURL.Text := 'http://127.0.0.1:4200/' + lIni.ReadString(lSection, 'path', '');
  SynEdit.Lines.LoadFromFile(ConcatPaths([ExtractFilePath(eFileNameEdit.Text), lIni.ReadString(lSection, 'filename', '')]));
  lIni.Free;
end;


end.
