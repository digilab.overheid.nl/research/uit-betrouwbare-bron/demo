// -----------------------------------------------------------------------------
//
// ace.Script.Json
//
// -----------------------------------------------------------------------------
//
// Adds Json functionality to the script.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Script.Json;

interface

uses
  Classes, SysUtils,
  ace.Script,
  uPSCompiler, uPSRuntime;

procedure Script_OnCompileImport_Json(aCompiler: TPSPascalCompiler);
procedure Script_OnExecuteImport_Json(aExec: TPSExec; aImporter: TPSRuntimeClassImporter);

implementation

uses
  fpjson, common.Json;

// -- Stubs

procedure TJsonArray_Add(Self: TJsonArray; aVal: TJsonObject);
  begin
  Self.Add(aVal);
  end;

function TJsonObject_Format(Self: TJsonObject; Indentation: Integer): string;
  begin Result := Self.Format(Indentation); end;
procedure TJsonObject_AddPairS(Self: TJsonObject; const aStr: string; const aVal: string);
  begin Self.AddPair(aStr, aVal); end;
procedure TJsonObject_AddPairI(Self: TJsonObject; aStr: string; aVal: integer);
  begin Self.AddPair(aStr, TJSONIntegerNumber.Create(aVal)); end;
procedure TJsonObject_AddPairB(Self: TJsonObject; aStr: string; aVal: boolean);
  begin Self.AddPair(aStr, TJSONBoolean.Create(aVal)); end;
procedure TJsonObject_AddPairO(Self: TJsonObject; aStr: string; aVal: TJsonObject);
  begin Self.AddPair(aStr, aVal); end;
procedure TJsonObject_AddPairA(Self: TJsonObject; aStr: string; aVal: TJsonArray);
  begin Self.AddPair(aStr, aVal); end;

function TJsonObject_TryGetValueS(Self: TJsonObject; Path: string; var Val: string): boolean;
  begin result := Self.TryGetValue(Path, Val); end;
function TJsonObject_TryGetValueI(Self: TJsonObject; Path: string; var Val: integer): boolean;
  begin result := Self.TryGetValue(Path, Val); end;
function TJsonObject_TryGetValueB(Self: TJsonObject; Path: string; var Val: boolean): boolean;
  begin result := Self.TryGetValue(Path, Val); end;
function TJsonObject_TryGetValueO(Self: TJsonObject; Path: string; var Val: TJsonObject): boolean;
  begin result := Self.TryGetValue(Path, Val); end;
function TJsonObject_TryGetValueA(Self: TJsonObject; Path: string; var Val: TJsonArray): boolean;
  begin result := Self.TryGetValue(Path, Val); end;

function ParseJsonStr(aJsonStr: string): TJsonObject;
  begin result := TJsonObject.ParseJSONValue(aJsonStr) as TJsonObject; end;
procedure TJsonArray_Count_R(Self: TJsonArray; var Result: Integer);
  begin Result := Self.Count; end;
procedure TJsonArray_Items_R(Self: TJsonArray; var Result: TJsonValue; aIndex: Integer);
  begin Result := Self.Items[aIndex]; end;


// -- Registration

procedure Script_OnCompileImport_Json(aCompiler: TPSPascalCompiler);
  begin
  with aCompiler.AddClassN(aCompiler.FindClass('TObject'), 'TJsonValue') do
    begin
    if not RegisterMethod('constructor Create') then EScriptRegistration('TJsonValue.Create');
    end;

  with aCompiler.AddClassN(aCompiler.FindClass('TObject'), 'TJsonObject') do
    begin
    if not RegisterMethod('constructor Create') then EScriptRegistration('TJsonObject.Create');
    if not RegisterMethod('function Format(Indentation: Integer): string') then EScriptRegistration('TJsonObject.Format');
    if not RegisterMethod('function AddPairS(const Str: string; const Val: string): TJsonObject') then EScriptRegistration('TJsonObject.AddPairS');
    if not RegisterMethod('function AddPairI(const Str: string; const Val: integer): TJsonObject') then EScriptRegistration('TJsonObject.AddPairI');
    if not RegisterMethod('function AddPairB(const Str: string; const Val: boolean): TJsonObject') then EScriptRegistration('TJsonObject.AddPairB');
    if not RegisterMethod('function TryGetValueS(Path: string; var Val: string): boolean') then EScriptRegistration('TJsonObject.TryGetValueS');
    if not RegisterMethod('function TryGetValueI(Path: string; var Val: integer): boolean') then EScriptRegistration('TJsonObject.TryGetValueI');
    if not RegisterMethod('function TryGetValueB(Path: string; var Val: boolean): boolean') then EScriptRegistration('TJsonObject.TryGetValueB');
    end;

  with aCompiler.AddClassN(aCompiler.FindClass('TObject'), 'TJsonArray') do
    begin
    if not RegisterMethod('constructor Create') then EScriptRegistration('TJsonArray.Create');
    if not RegisterMethod('procedure Add(const Obj: TJsonObject)') then EScriptRegistration('TJsonArray.Add');
    RegisterProperty('Count', 'Integer', iptr);
    RegisterProperty('Items', 'Integer TJsonObject', iptr);
    SetDefaultPropery('Items');
    end;

  if not Assigned(aCompiler.AddDelphiFunction('function ParseJsonStr(aJsonStr: string): TJsonObject')) then EScriptRegistration('ParseJsonStr');

  // -- Additional properties that would have caused a circular reference if registered directly
  with aCompiler.FindClass('TJsonObject') do
    begin
    if not RegisterMethod('function AddPairO(const Str: string; const Val: TJsonObject): TJSONObject') then EScriptRegistration('TJsonObject.AddPairO');
    if not RegisterMethod('function AddPairA(const Str: string; const Val: TJsonArray): TJSONObject') then EScriptRegistration('TJsonObject.AddPairA');
    if not RegisterMethod('function TryGetValueO(Path: string; var Val: TJsonObject): boolean') then EScriptRegistration('TJsonObject.TryGetValueO');
    if not RegisterMethod('function TryGetValueA(Path: string; var Val: TJsonArray): boolean') then EScriptRegistration('TJsonObject.TryGetValueA');
    end;
  end;

procedure Script_OnExecuteImport_Json(aExec: TPSExec; aImporter: TPSRuntimeClassImporter);
  begin
  aExec.RegisterDelphiFunction(@ParseJsonStr, 'ParseJsonStr', cdRegister);

  with aImporter.Add(TJsonValue) do
    begin
    RegisterConstructor(@TJsonValue.Create, 'Create');
    end;

  with aImporter.Add(TJsonObject) do
    begin
    RegisterConstructor(@TJsonObject.Create, 'Create');
    RegisterMethod(@TJsonObject_Format, 'Format');
    RegisterMethod(@TJsonObject_AddPairS, 'AddPairS');
    RegisterMethod(@TJsonObject_AddPairI, 'AddPairI');
    RegisterMethod(@TJsonObject_AddPairB, 'AddPairB');
    RegisterMethod(@TJsonObject_AddPairO, 'AddPairO');
    RegisterMethod(@TJsonObject_AddPairA, 'AddPairA');
    RegisterMethod(@TJsonObject_TryGetValueS, 'TryGetValueS');
    RegisterMethod(@TJsonObject_TryGetValueI, 'TryGetValueI');
    RegisterMethod(@TJsonObject_TryGetValueB, 'TryGetValueB');
    RegisterMethod(@TJsonObject_TryGetValueO, 'TryGetValueO');
    RegisterMethod(@TJsonObject_TryGetValueA, 'TryGetValueA');
    RegisterMethod(@TJsonObject_TryGetValueS, 'GetValueS');
    RegisterMethod(@TJsonObject_TryGetValueI, 'GetValueI');
    RegisterMethod(@TJsonObject_TryGetValueB, 'GetValueB');
    RegisterMethod(@TJsonObject_TryGetValueO, 'GetValueO');
    RegisterMethod(@TJsonObject_TryGetValueA, 'GetValueA');
    end;

  with aImporter.Add(TJsonArray) do
    begin
    RegisterConstructor(@TJsonArray.Create, 'Create');
    RegisterMethod(@TJsonArray_Add, 'Add');
    RegisterPropertyHelper(@TJsonArray_Count_R, nil, 'Count');
    RegisterPropertyHelper(@TJsonArray_Items_R, nil, 'Items');
    end;
  end;

end.
