// -----------------------------------------------------------------------------
//
// ace.Script.Delphi
//
// -----------------------------------------------------------------------------
//
// Adds Delphi functionality to the script.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Script.Delphi;

interface

uses
  Classes, SysUtils, ace.Script,
  uPSCompiler, uPSRuntime;

procedure Script_OnCompileImport_Delphi(aCompiler: TPSPascalCompiler);
procedure Script_OnExecuteImport_Delphi(aExec: TPSExec);

implementation

uses
  Dialogs;

procedure Script_OnCompileImport_Delphi(aCompiler: TPSPascalCompiler);
  begin
  if not Assigned(aCompiler.AddDelphiFunction('procedure ShowMessage(const aMsg: String)')) then EScriptRegistration('ShowMessage');
  if not Assigned(aCompiler.AddDelphiFunction('function Format(Format: string; Args: array of const): string')) then EScriptRegistration('Format');
  end;

procedure Script_OnExecuteImport_Delphi(aExec: TPSExec);
  begin
  aExec.RegisterDelphiFunction(@ShowMessage, 'ShowMessage', cdRegister);
  aExec.RegisterDelphiFunction(@Format, 'Format', cdRegister);
  end;

end.
