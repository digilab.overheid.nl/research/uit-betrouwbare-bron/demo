// -----------------------------------------------------------------------------
//
// ace.Script.Debug
//
// -----------------------------------------------------------------------------
//
// Adds Debug functionality to the script.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Script.Debug;

interface

uses
  Classes, SysUtils, ace.Script,
  uPSCompiler, uPSRuntime;

procedure Script_OnCompileImport_Debug(aCompiler: TPSPascalCompiler);
procedure Script_OnExecuteImport_Debug(aExec: TPSExec);

implementation

uses
  ace.Debug;

procedure Stub_Debug(aTopic: string; aMessage: string);
  begin Debug(aTopic, aMessage); end;
procedure Stub_DebugF(aTopic: string; aMessage: string; const aArgs: array of const);
  begin Debug(aTopic, aMessage, aArgs); end;

procedure Script_OnCompileImport_Debug(aCompiler: TPSPascalCompiler);
  begin
  if not Assigned(aCompiler.AddDelphiFunction('procedure Debug(aTopic: string; aMessage: string)')) then EScriptRegistration('Debug');
  if not Assigned(aCompiler.AddDelphiFunction('procedure DebugF(aTopic: string; aMessage: string; aArgs: array of const)')) then EScriptRegistration('DebugF');
  if not Assigned(aCompiler.AddDelphiFunction('procedure DebugIncIndent(aTopic: string)')) then EScriptRegistration('DebugIncIndent');
  if not Assigned(aCompiler.AddDelphiFunction('procedure DebugDecIndent(aTopic: string)')) then EScriptRegistration('DebugDecIndent');
  if not Assigned(aCompiler.AddDelphiFunction('procedure DebugResetIndent(aTopic: string)')) then EScriptRegistration('DebugResetIndent');
  if not Assigned(aCompiler.AddDelphiFunction('procedure DebugClear(aTopic: string)')) then EScriptRegistration('DebugClear');
  end;

procedure Script_OnExecuteImport_Debug(aExec: TPSExec);
  begin
  aExec.RegisterDelphiFunction(@Stub_Debug, 'Debug', cdRegister);
  aExec.RegisterDelphiFunction(@Stub_DebugF, 'DebugF', cdRegister);
  aExec.RegisterDelphiFunction(@DebugIncIndent, 'DebugIncIndent', cdRegister);
  aExec.RegisterDelphiFunction(@DebugDecIndent, 'DebugDecIndent', cdRegister);
  aExec.RegisterDelphiFunction(@DebugResetIndent, 'DebugResetIndent', cdRegister);
  aExec.RegisterDelphiFunction(@DebugClear, 'DebugClear', cdRegister);
  end;

end.
