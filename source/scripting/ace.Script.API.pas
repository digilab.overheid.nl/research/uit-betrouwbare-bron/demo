// -----------------------------------------------------------------------------
//
// ace.Script.API
//
// -----------------------------------------------------------------------------
//
// Adds API related functionality to the script.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Script.API;

interface

uses
  Classes, SysUtils, ace.Script, ace.Core.Engine,
  uPSCompiler, uPSRuntime;

procedure Script_OnCompileImport_API(aCompiler: TPSPascalCompiler);
procedure Script_OnExecuteImport_API(aImporter: TPSRuntimeClassImporter);

implementation

uses
  ace.API.Server, fpjson;

// Request
procedure TcRequest_CommandType_R(Self: TcRequest; var Result: string);
  begin Result := Self.CommandTypeStr; end;
procedure TcRequest_Resource_R(Self: TcRequest; var Result: string);
  begin Result := Self.Resource; end;
procedure TcRequest_Params_R(Self: TcRequest; var Result: TStringlist);
  begin Result := Self.Params; end;
procedure TcRequest_ParamStr_R(Self: TcRequest; var Result: string);
  begin Result := Self.ParamStr; end;
procedure TcRequest_Content_R(Self: TcRequest; var Result: string);
  begin Result := Self.Content; end;
procedure TcRequest_ContentType_R(Self: TcRequest; var Result: string);
  begin Result := Self.ContentType; end;
procedure TcRequest_Json_R(Self: TcRequest; var Result: TJsonObject);
  begin Result := Self.Json; end;
procedure TcRequest_ContentParseError_R(Self: TcRequest; var Result: string);
  begin Result := Self.ContentParseError; end;

// Response
procedure TcResponse_Code_R(Self: TcResponse; var Result: integer);
  begin Result := Self.Code; end;
procedure TcResponse_Code_W(Self: TcResponse; Value: integer);
  begin Self.Code := Value; end;
procedure TcResponse_Content_R(Self: TcResponse; var Result: string);
  begin Result := Self.Content; end;
procedure TcResponse_Content_W(Self: TcResponse; Value: string);
  begin Self.Content := Value; end;

// Claim
procedure TcClaim_RefIDStr_R(Self: TcClaim; var Result: string);
  begin Result := Self.RefIDStr; end;
procedure TcClaim_IdentityStr_R(Self: TcClaim; var Result: string);
  begin Result := Self.IdentityStr; end;

// Annotation
procedure TcAnnotation_RefIDStr_R(Self: TcAnnotation; var Result: string);
  begin Result := Self.RefIDStr; end;
procedure TcAnnotation_IdentityStr_R(Self: TcClaim; var Result: string);
  begin Result := Self.IdentityStr; end;

// Operation
procedure TcOperation_ValidFrom_R(Self: TcOperation; var Result: integer);
  begin Result := Self.ValidFrom; end;
procedure TcOperation_ValidUntil_R(Self: TcOperation; var Result: integer);
  begin Result := Self.ValidUntil; end;


procedure Script_OnCompileImport_API(aCompiler: TPSPascalCompiler);
  begin
  with aCompiler.AddClassN(aCompiler.FindClass('TObject'), 'TcRequest') do
    begin
    RegisterProperty('CommandType', 'string', iptr);
    RegisterProperty('Resource', 'string', iptr);
    RegisterProperty('Params', 'TStringList', iptr);
    RegisterProperty('ParamStr', 'string', iptr);
    RegisterProperty('Content', 'string', iptr);
    RegisterProperty('ContentType', 'string', iptr);
    RegisterProperty('Json', 'TJsonObject', iptr);
    RegisterProperty('ContentParseError', 'string', iptr);
    end;
  AddImportedClassVariable(aCompiler, 'Request', 'TcRequest');

  with aCompiler.AddClassN(aCompiler.FindClass('TObject'), 'TcResponse') do
    begin
    RegisterProperty('Code', 'string', iptrw);
    RegisterProperty('Response', 'string', iptrw);
    end;
  AddImportedClassVariable(aCompiler, 'Response', 'TcResponse');

  with aCompiler.AddClassN(aCompiler.FindClass('TObject'), 'TcClaim') do
    begin
    RegisterProperty('RefIDStr', 'string', iptr);
    RegisterProperty('IdentityStr', 'string', iptr);
    end;

  with aCompiler.AddClassN(aCompiler.FindClass('TObject'), 'TcAnnotation') do
    begin
    RegisterProperty('RefIDStr', 'string', iptr);
    RegisterProperty('IdentityStr', 'string', iptr);
    end;

  with aCompiler.AddClassN(aCompiler.FindClass('TObject'), 'TcOperation') do
    begin
    if not RegisterMethod('function FindClaim(aClaimTypeName, aIdentityStr: string): TcClaim') then EScriptRegistration('FindClaim');
    RegisterProperty('ValidFrom', 'integer', iptr);
    RegisterProperty('ValidUntil', 'integer', iptr);
    end;
  AddImportedClassVariable(aCompiler, 'Operation', 'TcOperation');

  with aCompiler.AddClassN(aCompiler.FindClass('TObject'), 'TcAPIScriptInterface') do
    begin
    if not RegisterMethod('procedure ProcessingFailed(aPath, aMessage: string)') then EScriptRegistration('ProcessingFailed');

    if not RegisterMethod('procedure CheckTime(aPath: String; aTime: integer)') then EScriptRegistration('CheckTime');

    if not RegisterMethod('procedure StartRule(aName: string)') then EScriptRegistration('StartRule');
    if not RegisterMethod('procedure RuleViolation(aMessage: string)') then EScriptRegistration('RuleViolation');
    if not RegisterMethod('procedure EndRule') then EScriptRegistration('EndRule');

    if not RegisterMethod('function JsonTryGetStr(aJson: TJsonObject; aKey: string; var aValue: string): boolean') then EScriptRegistration('JsonTryGetStr');
    if not RegisterMethod('function JsonGetStr(aJson: TJsonObject; aKey: string): string') then EScriptRegistration('JsonGetStr');
    if not RegisterMethod('function JsonTryGetInt(aJson: TJsonObject; aKey: string; var aValue: integer): boolean') then EScriptRegistration('JsonTryGetInt');
    if not RegisterMethod('function JsonGetInt(aJson: TJsonObject; aKe: string): integer') then EScriptRegistration('JsonGetInt');
    if not RegisterMethod('function JsonTryGetBool(aJson: TJsonObject; aKey: string; var aValue: boolean): boolean') then EScriptRegistration('JsonTryGetBool');
    if not RegisterMethod('function JsonGetBool(aJson: TJsonObject; aKey: string): boolean') then EScriptRegistration('JsonGetBool');
    if not RegisterMethod('function JsonTryGetObj(aJson: TJsonObject; aKey: string; var aValue: TJsonObject): boolean') then EScriptRegistration('JsonTryGetObj');
    if not RegisterMethod('function JsonGetObj(aJson: TJsonObject; aKey: string): TJsonObject') then EScriptRegistration('JsonGetObj');
    if not RegisterMethod('function JsonTryGetArray(aJson: TJsonObject; aKey: string; var aValue: TJsonArray): boolean') then EScriptRegistration('JsonTryGetArray');
    if not RegisterMethod('function JsonGetArray(aJson: TJsonObject; aKey: string): TJsonArray') then EScriptRegistration('JsonGetArray');

    if not RegisterMethod('procedure SetRegisteredAt(aTime: integer)') then EScriptRegistration('SetRegisteredAt');
    if not RegisterMethod('procedure SetExpiredAt(aTime: integer)') then EScriptRegistration('SetExpiredAt');
    if not RegisterMethod('procedure SetValidFrom(aTime: integer)') then EScriptRegistration('SetValidFrom');
    if not RegisterMethod('procedure SetValidUntil(aTime: integer)') then EScriptRegistration('SetValidUntil');
    if not RegisterMethod('procedure ResetTemporal') then EScriptRegistration('ResetTemporal');

    if not RegisterMethod('function FindClaim(aClaimTypeName: string; aIdentityStr: string; var aClaim: TcClaim): boolean') then EScriptRegistration('FindClaim');
    if not RegisterMethod('function AddClaim(aClaimTypeName: string; aTuple: array of const): TcClaim)') then EScriptRegistration('AddClaim');

    if not RegisterMethod('function Annotate(aClaim: TcClaim; aAnnotationTypeName: string; aTuple: array of const): TcAnnotation') then EScriptRegistration('Annotate');

    if not RegisterMethod('function FindObject(aObjectTypeName: string; aIdentityStr: string; var aObject: TcClaim): boolean') then EScriptRegistration('FindObject');
    if not RegisterMethod('function GetProperty(aObject: TcClaim; aPropertyName: string): TcClaim') then EScriptRegistration('GetProperty');
    if not RegisterMethod('function AddObject(aObjectTypeName: string; aTuple: array of const): TcClaim)') then EScriptRegistration('AddObject');
    if not RegisterMethod('function AddPropertyStr(aObject: TcClaim; aPropertyName: string; aValue: string): TcClaim') then EScriptRegistration('AddPropertyStr');
    if not RegisterMethod('function AddPropertyInt(aObject: TcClaim; aPropertyName: string; aValue: integer): TcClaim') then EScriptRegistration('AddPropertyInt');
    if not RegisterMethod('function AddPropertyBool(aObject: TcClaim; aPropertyName: string; aValue: boolean): TcClaim') then EScriptRegistration('AddPropertyBool');
    if not RegisterMethod('function GetPropertyStr(aObject: TcClaim; aPropertyName: string): string') then EScriptRegistration('GetPropertyStr');
    if not RegisterMethod('function GetPropertyInt(aObject: TcClaim; aPropertyName: string): integer') then EScriptRegistration('GetPropertyInt');
    if not RegisterMethod('function GetPropertyBool(aObject: TcClaim; aPropertyName: string): boolean') then EScriptRegistration('GetPropertyBool');

    if not RegisterMethod('procedure BroadCastCloudEvent(aTypeStr: string; aObjectName, aObjectKey: string)') then EScriptRegistration('BroadCastCloudEvent');
    end;
  AddImportedClassVariable(aCompiler, 'ace', 'TcAPIScriptInterface');
  end;

procedure Script_OnExecuteImport_API(aImporter: TPSRuntimeClassImporter);
  begin
  with aImporter.Add(TcRequest) do
    begin
    RegisterPropertyHelper(@TcRequest_CommandType_R, nil, 'CommandType');
    RegisterPropertyHelper(@TcRequest_Resource_R, nil, 'Resource');
    RegisterPropertyHelper(@TcRequest_Params_R, nil, 'Params');
    RegisterPropertyHelper(@TcRequest_ParamStr_R, nil, 'ParamStr');
    RegisterPropertyHelper(@TcRequest_Content_R, nil, 'Content');
    RegisterPropertyHelper(@TcRequest_ContentType_R, nil, 'ContentType');
    RegisterPropertyHelper(@TcRequest_Json_R, nil, 'Json');
    RegisterPropertyHelper(@TcRequest_ContentParseError_R, nil, 'ContentParseError');
    end;

  with aImporter.Add(TcResponse) do
    begin
    RegisterPropertyHelper(@TcResponse_Code_R, @TcResponse_Code_W, 'Code');
    RegisterPropertyHelper(@TcResponse_Content_R, @TcResponse_Content_W, 'Content');
    end;

  with aImporter.Add(TcClaim) do
    begin
    RegisterPropertyHelper(@TcClaim_RefIDStr_R, nil, 'RefIDStr');
    RegisterPropertyHelper(@TcClaim_IdentityStr_R, nil, 'IdentityStr');
    end;

  with aImporter.Add(TcAnnotation) do
    begin
    RegisterPropertyHelper(@TcAnnotation_RefIDStr_R, nil, 'RefIDStr');
    RegisterPropertyHelper(@TcAnnotation_IdentityStr_R, nil, 'IdentityStr');
    end;

  with aImporter.Add(TcOperation) do
    begin
    RegisterMethod(@TcOperation.FindClaim, 'FindClaim');
    RegisterPropertyHelper(@TcOperation_ValidFrom_R, nil, 'ValidFrom');
    RegisterPropertyHelper(@TcOperation_ValidUntil_R, nil, 'ValidUntil');
    end;

  with aImporter.Add(TcAPIScriptInterface) do
    begin
    RegisterMethod(@TcAPIScriptInterface.ProcessingFailed, 'ProcessingFailed');

    RegisterMethod(@TcAPIScriptInterface.CheckTime, 'CheckTime');

    RegisterMethod(@TcAPIScriptInterface.StartRule, 'StartRule');
    RegisterMethod(@TcAPIScriptInterface.RuleViolation, 'RuleViolation');
    RegisterMethod(@TcAPIScriptInterface.EndRule, 'EndRule');

    RegisterMethod(@TcAPIScriptInterface.JsonTryGetStr, 'JsonTryGetStr');
    RegisterMethod(@TcAPIScriptInterface.JsonGetStr, 'JsonGetStr');
    RegisterMethod(@TcAPIScriptInterface.JsonTryGetInt, 'JsonTryGetInt');
    RegisterMethod(@TcAPIScriptInterface.JsonGetInt, 'JsonGetInt');
    RegisterMethod(@TcAPIScriptInterface.JsonTryGetBool, 'JsonTryGetBool');
    RegisterMethod(@TcAPIScriptInterface.JsonGetBool, 'JsonGetBool');
    RegisterMethod(@TcAPIScriptInterface.JsonTryGetObj, 'JsonTryGetObj');
    RegisterMethod(@TcAPIScriptInterface.JsonGetObj, 'JsonGetObj');
    RegisterMethod(@TcAPIScriptInterface.JsonTryGetArray, 'JsonTryGetArray');
    RegisterMethod(@TcAPIScriptInterface.JsonGetArray, 'JsonGetArray');

    RegisterMethod(@TcAPIScriptInterface.SetRegisteredAt, 'SetRegisteredAt');
    RegisterMethod(@TcAPIScriptInterface.SetExpiredAt, 'SetExpiredAt');
    RegisterMethod(@TcAPIScriptInterface.SetValidFrom, 'SetValidFrom');
    RegisterMethod(@TcAPIScriptInterface.SetValidUntil, 'SetValidUntil');
    RegisterMethod(@TcAPIScriptInterface.ResetTemporal, 'ResetTemporal');

    RegisterMethod(@TcAPIScriptInterface.FindClaim, 'FindClaim');
    RegisterMethod(@TcAPIScriptInterface.AddClaim, 'AddClaim');

    RegisterMethod(@TcAPIScriptInterface.Annotate, 'Annotate');

    RegisterMethod(@TcAPIScriptInterface.FindObject, 'FindObject');
    RegisterMethod(@TcAPIScriptInterface.GetProperty, 'GetProperty');
    RegisterMethod(@TcAPIScriptInterface.AddObject, 'AddObject');
    RegisterMethod(@TcAPIScriptInterface.AddPropertyStr, 'AddPropertyStr');
    RegisterMethod(@TcAPIScriptInterface.AddPropertyInt, 'AddPropertyInt');
    RegisterMethod(@TcAPIScriptInterface.AddPropertyBool, 'AddPropertyBool');
    RegisterMethod(@TcAPIScriptInterface.GetPropertyStr, 'GetPropertyStr');
    RegisterMethod(@TcAPIScriptInterface.GetPropertyInt, 'GetPropertyInt');
    RegisterMethod(@TcAPIScriptInterface.GetPropertyBool, 'GetPropertyBool');

    RegisterMethod(@TcAPIScriptInterface.BroadCastCloudEvent, 'BroadCastCloudEvent');
    end;
  end;

end.
