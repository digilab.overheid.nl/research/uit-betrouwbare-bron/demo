// -----------------------------------------------------------------------------
//
// ace.Script
//
// -----------------------------------------------------------------------------
//
// Integrates PascalScript (from RemObjects) in the prototype. Enables writing,
// compiling and running pascal scripts at runtime.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Script;

interface

uses
  Classes,
  uPSComponent, uPSCompiler, uPSRuntime,
  ace.Core.Engine, ace.API.Server;

// Events
const
  cRadioEvent_Script_Changed = 'Script.Changed'; // Object = nil

type
  TcScript =
    class
      private
        fScript: string;
        fPSScript: TPSScript;
        fMethods: TStringlist;
        fMessages: TStringlist;
        fErrorPos: Cardinal;

      protected
        procedure OnCompileImport(Sender: TObject; aCompiler: TPSPascalCompiler);
        procedure OnExecuteImport(Sender: TObject; aExec: TPSExec; aImporter: TPSRuntimeClassImporter);
        procedure OnBindContext;

      public
        // -- Context
        ASI: TcAPIScriptInterface; // Has to be directly and publicly accessible for mappping methods

        constructor Create;
        procedure Init(aAPIScriptInterface: TcAPIScriptInterface);
        destructor Destroy; override;

        // 1. Load a script (from File or from a string)
        procedure LoadFromFile;
        procedure LoadFromString(aScript: string);

        // 2. Compile and store a list of functions
        procedure Compile;

        // 3. function HasMethod(aMethodName: String): boolean;
        function HasMethod(aMethodName: string): boolean;

        // 4. procedure CallMethod(aMethodName: String; const aParams: array of Variant);
        function CallMethod(aMethodName: String; const aParams: array of Variant): Variant;

        // Properties
        property Messages: TStringlist read fMessages;
        property ErrorPos: Cardinal read fErrorPos;
      end;

procedure EScriptRegistration(aElement: string);

implementation

uses
  SysUtils,
  Dialogs,
  Common.ErrorHandling, common.Utils, common.FileUtils,
  ace.Settings,
  uPSC_std, uPSC_classes, uPSR_std, uPSR_classes,
  ace.Script.Delphi, ace.Script.Debug, ace.Script.Json, ace.Script.API,
  ace.API.Operations;

procedure EScriptRegistration(aElement: string);
  begin
  EDev('Registration error: %s', [aElement]);
  end;


{ TcScripter }

constructor TcScript.Create;
  begin
  inherited;
  fPSScript := TPSScript.Create(nil);
  fPSScript.CompilerOptions := [icAllowNoBegin, icAllowNoEnd, icBooleanShortCircuit];
  fPSScript.OnCompImport := OnCompileImport;
  fPSScript.OnExecImport := OnExecuteImport;

  fMethods := TStringlist.Create;
  fMethods.Sorted := true;
  fMethods.Duplicates := dupError;
  fMethods.CaseSensitive := false;

  fMessages := TStringlist.Create;
  end;

procedure TcScript.Init(aAPIScriptInterface: TcAPIScriptInterface);
  begin
  ASI := aAPIScriptInterface;
  end;

destructor TcScript.Destroy;
  begin
  FreeAndNil(fPSScript);
  FreeAndNil(fMethods);
  FreeAndNil(fMessages);
  inherited;
  end;

procedure TcScript.LoadFromFile;
  var
    lFilename: string;
    lScript: TStringlist;
  begin
  lFilename := TPath.Combine(gSettings.ProjectFolder, 'script.pas');
  if FileExists(lFilename)
     then begin
          lScript := TStringlist.Create;
          try
            lScript.LoadFromFile(lFilename);
            fScript := StringlistToString(lScript, #13#10);
          finally
            lScript.Free;
            end;
          end;
  end;

procedure TcScript.LoadFromString(aScript: string);
  begin
  fScript := aScript;
  end;

procedure TcScript.Compile;

  function StoreMessages: String;
    var
      i: Integer;
    begin
    Result := '';
    for i := 0 to fPSScript.CompilerMessageCount - 1 do
      if (fPSScript.CompilerMessages[i] is TIFPSPascalCompilerError)
         then Messages.Add(Format('''%s''', [
              String(fPSScript.CompilerMessages[i].MessageToString){,
              fPSScript.CompilerMessages[i].Row,
              fPSScript.CompilerMessages[i].Col}
           ]));
    end;

  var
    i: integer;
  begin
  // Compile
  fPSScript.Script.Text := fScript;

  fMessages.Clear;
  if not fPSScript.Compile
    then if fPSScript.Exec.ExceptionCode = erNoError
           then begin
                StoreMessages;
                if fMessages.Count>0
                   then EUsr(fMessages[0])
                end
           else EUsr('Compile error: %s', [fPSScript.ExecErrorToString]);

  // Publish context to script
  OnBindContext;

  // Store list of methods
  fMethods.Clear;
  for i := 0 to fPSScript.Exec.GetProcCount - 1 do
    if fPSScript.Exec.GetProcNo(i) is TPSInternalProcRec then
      fMethods.Add(fPSScript.Exec.ProcNames[i]);
  end;

function TcScript.HasMethod(aMethodName: string): boolean;
  var
    lIndex: integer;
  begin
  result := fMethods.Find(aMethodName, lIndex);
  end;

function TcScript.CallMethod(aMethodName: String; const aParams: array of Variant): Variant;
  begin
  result := fPSScript.ExecuteFunction(aParams, String(aMethodName));
  end;

procedure TcScript.OnCompileImport(Sender: TObject; aCompiler: TPSPascalCompiler);
  begin
  SIRegister_Std(aCompiler);
  SIRegister_Classes(aCompiler, False);
  Script_OnCompileImport_Delphi(aCompiler);
  Script_OnCompileImport_Debug(aCompiler);
  Script_OnCompileImport_Json(aCompiler);
  Script_OnCompileImport_API(aCompiler);
  end;

procedure TcScript.OnExecuteImport(Sender: TObject; aExec: TPSExec; aImporter: TPSRuntimeClassImporter);
  begin
  RIRegister_Std(aImporter);
  RIRegister_Classes(aImporter, False);
  Script_OnExecuteImport_Debug(aExec);
  Script_OnExecuteImport_Delphi(aExec);
  Script_OnExecuteImport_Json(aExec, aImporter);
  Script_OnExecuteImport_API(aImporter);
  end;

procedure TcScript.OnBindContext;
  begin
  if Assigned(asi)
     then begin
          fPSScript.SetVarToInstance('ace', asi);
          fPSScript.SetVarToInstance('request', asi.Request);
          fPSScript.SetVarToInstance('response', asi.Response);
          if Assigned(asi.Session) and Assigned(asi.Session.Operation)
             then fPSScript.SetVarToInstance('operation', asi.Session.Operation)
             else fPSScript.SetVarToInstance('operation', nil)
          end
     else begin
          fPSScript.SetVarToInstance('ace', nil);
          fPSScript.SetVarToInstance('request', nil);
          fPSScript.SetVarToInstance('response', nil);
          fPSScript.SetVarToInstance('operation', nil);
          end;
  end;

end.
