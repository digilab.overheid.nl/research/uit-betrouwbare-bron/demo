// -----------------------------------------------------------------------------
//
// ace.Settings
//
// -----------------------------------------------------------------------------
//
// Settings read from ini file.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Settings;

{$MODE Delphi}

interface

uses
  LCLType, Classes, IniFiles, ace.Core.Engine, Forms;

type
  TcSettings =
    class
      private
        // Interface
        fFontSize_ScaleFactor: integer;
        fShowControls: boolean;
        fFontName_ClaimSetInspector: string;
        fFontName_Console: string;
        fFontSize_Console: integer;
        fFontSize_TransactionsClaimSets: integer;

        // Project
        fProjectName: string;
        fProjectFolder: string;

        // Engine (global instance)
        fEngine: TcEngine;

        // Debug
        fDebugTopics: TStringlist;

        function GetIniFilename: String;
        procedure ReadIniFile;

        function GetFontHeight(aFontSize: integer): integer;
        function GetFontName_Console: string;
        function GetFontHeight_Console: integer;
        function GetFontHeight_TransactionsClaimSets: integer;

        function GetProjectName: string;

      public
        constructor Create;
        destructor Destroy; override;

        procedure SaveFormPosition(aForm: TForm);
        procedure LoadFormPosition(aForm: TForm);

        property IniFilename: String read GetIniFilename;

        // Interface
        property FontSize_ScaleFactor: integer read fFontSize_ScaleFactor;
        property ShowControls: boolean read fShowControls;
        property FontName_ClaimSetInspector: string read fFontName_ClaimSetInspector;
        property FontName_Console: string read GetFontName_Console;
        property FontHeight_Console: integer read GetFontHeight_Console;
        property FontHeight_TransactionsClaimSets: integer read GetFontHeight_TransactionsClaimSets;

        // Project
        procedure ProjectLoaded(aFolder: string);
        procedure ProjectClosed;
        function AskProjectFolderWhenEmpty: boolean;

        function ProjectsFolder: string;
        property ProjectFolder: string read fProjectFolder;
        property ProjectName: string read GetProjectName;

        // Engine
        property Engine: TcEngine read fEngine write fEngine;

        // Debug
        function DebugTopicEnabled(aTopic: string): boolean;
      end;

var
  // Created and Destroyed by this unit
  gSettings: TcSettings;


implementation

uses
  SysUtils,
  diagram.Settings, ace.Forms.Main, common.FileUtils;

{ TcSettings }

constructor TcSettings.Create;
  begin
  inherited Create;
  fDebugTopics := TStringlist.Create;
  ReadIniFile;
  gDiagramSettings := TcDiagramSettings.Create;
  fEngine := TcEngine.Create;
  end;

destructor TcSettings.Destroy;
  begin
  FreeAndNil(fDebugTopics);
  FreeAndNil(gDiagramSettings);
  FreeAndNil(fEngine);
  inherited;
  end;

function TcSettings.GetIniFilename: String;
  begin
  result := TPath.ChangeFileExt(Application.ExeName, '.ini');
  end;

function TcSettings.GetFontHeight(aFontSize: integer): integer;
  begin
  result := -MulDiv(aFontSize, Screen.PixelsPerInch, 96);
  end;

function TcSettings.GetFontHeight_Console: integer;
  begin
  result := GetFontHeight(fFontSize_Console);
  end;

function TcSettings.GetFontName_Console: string;
  begin
  result := fFontName_Console;
  end;

function TcSettings.GetFontHeight_TransactionsClaimSets: integer;
  begin
  result := GetFontHeight(fFontSize_TransactionsClaimSets);
  end;

const
  cDefault_Interface_FontSize_ScaleFactor = 100;
  cDefault_Interface_ShowControls = true;
  cDefault_Interface_FontName_ClaimSetInspector = 'Courier New';
  cDefault_Interface_FontName_Console = 'Courier New';
  cDefault_Interface_FontSize_Console = 14;
  cDefault_Interface_FontSize_TransactionsClaimSets = 12;

procedure TcSettings.ReadIniFile;

  var
    lIniFile: TMemIniFile;
    lIniSection: String;
  begin
  lIniFile := TMemIniFile.Create(IniFilename);
  try
    lIniSection := 'Interface';
    fFontSize_ScaleFactor := lIniFile.ReadInteger(lIniSection, 'FontSize_ScaleFactor', cDefault_Interface_FontSize_ScaleFactor);
    fShowControls := lIniFile.ReadBool(lIniSection, 'ShowControls', cDefault_Interface_ShowControls);
    fFontName_ClaimSetInspector := lIniFile.ReadString(lIniSection, 'FontName_ClaimSetInspector', cDefault_Interface_FontName_ClaimSetInspector);
    fFontName_Console := lIniFile.ReadString(lIniSection, 'FontName_Console', cDefault_Interface_FontName_Console);
    fFontSize_Console := lIniFile.ReadInteger(lIniSection, 'FontSize_Console', cDefault_Interface_FontSize_Console);
    fFontSize_TransactionsClaimSets := lIniFile.ReadInteger(lIniSection, 'FontSize_TransactionsClaimSets', cDefault_Interface_FontSize_TransactionsClaimSets);

    lIniSection := 'Debug';
    if lIniFile.SectionExists(lIniSection)
      then lIniFile.ReadSectionValues(lIniSection, fDebugTopics);

  finally
    lIniFile.Free;
    end;
  end;

procedure TcSettings.SaveFormPosition(aForm: TForm);
  var
    lIniFile: TMemIniFile;
    lIniSection: String;
  begin
  lIniFile := TMemIniFile.Create(IniFilename);
  try
    lIniSection := Format('Position %s', [aForm.ClassName]);;
    lIniFile.WriteInteger(lIniSection, 'Left', aForm.Left);
    lIniFile.WriteInteger(lIniSection, 'Top', aForm.Top);
    lIniFile.WriteInteger(lIniSection, 'Width', aForm.Width);
    lIniFile.WriteInteger(lIniSection, 'Height', aForm.Height);
  finally
    lIniFile.Free;
    end;
  end;

procedure TcSettings.LoadFormPosition(aForm: TForm);
  var
    lIniFile: TMemIniFile;
    lIniSection: String;
    lRect: TRect;
    lPositionLoaded: Boolean;
  begin
  lRect := aForm.BoundsRect;;

  lIniFile := TMemIniFile.Create(IniFilename);
  try
    lIniSection := Format('Position %s', [aForm.ClassName]);;
    lPositionLoaded := lIniFile.ReadInteger(lIniSection, 'Left', -1) <> -1;

    if lPositionLoaded then
      begin
      lRect.Left := lIniFile.ReadInteger(lIniSection, 'Left', aForm.Left);
      lRect.Top := lIniFile.ReadInteger(lIniSection, 'Top', aForm.Top);
      lRect.Width := lIniFile.ReadInteger(lIniSection, 'Width', aForm.Width);
      lRect.Height := lIniFile.ReadInteger(lIniSection, 'Height', aForm.Height);
      end;
  finally
    lIniFile.Free;
  end;

  if lPositionLoaded then
    begin
    if lRect.Left > Round(aForm.Monitor.WorkareaRect.Right * 0.9)
       then lRect := Bounds(0, lRect.Top, lRect.Width, lRect.Height);
    if lRect.Top > Round(aForm.Monitor.WorkareaRect.Bottom * 0.9)
       then lRect := Bounds(lRect.Left, 0, lRect.Width, lRect.Height);

    aForm.Position := poDesigned;
    aForm.SetBounds(lRect.Left, lRect.Top, lRect.Width, lRect.Height);
    end
  else begin
       // Switching Position to poDefaultPosOnly causes random violations on linux during starting
       // of the Main form. If Position = poDesigned, set position to 0.0.
       if aForm.Position = poDesigned
          then aForm.SetBounds(0, 0, aForm.Width, aForm.Height)
       end

  else aForm.Position := poDefaultPosOnly;
end;

function TcSettings.DebugTopicEnabled(aTopic: string): boolean;
  var
    lStr: string;
  begin
  if not Assigned(fDebugTopics)
     then result := false
     else begin
          lStr := fDebugTopics.Values[aTopic];
          result := lStr = '1';
          end;
  end;

function TcSettings.ProjectsFolder: string;
  begin
  result := TPath.Combine(ExtractFilePath(Application.ExeName), 'samples');
  end;

procedure TcSettings.ProjectLoaded(aFolder: string);
  begin
  fProjectName := TPath.GetFilenameWithoutExtension(aFolder);
  fProjectFolder := aFolder;
  end;

function TcSettings.AskProjectFolderWhenEmpty: boolean;
  begin
  result := true;
  if fProjectName<>'' then exit;
  result := fMain.CreateProject;
  end;

procedure TcSettings.ProjectClosed;
  begin
  fProjectName := '';
  fProjectFolder := '';
  end;

function TcSettings.GetProjectName: string;
  begin
  if fProjectName=''
     then result := 'default'
     else result := fProjectName;
  end;

initialization
  gSettings := TcSettings.Create;

finalization
  FreeAndNil(gSettings);

end.
