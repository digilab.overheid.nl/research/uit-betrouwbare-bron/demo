// -----------------------------------------------------------------------------
//
// common.ErrorHandling
//
// -----------------------------------------------------------------------------
//
// Collection of general purpose error routines.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV
//
// -----------------------------------------------------------------------------

unit common.ErrorHandling;

{$MODE Delphi}

interface

uses
  SysUtils;

type
   // Use EUser if the error is a result of a user action or should be shown
   // to and understand by the user. The errors should be in the users
   // language. Use the EUsr routines to raise an error.
   EUser = class (Exception);

   // Use EDeveloper when the error is a result of a developers action or
   // should be shown to and understand by a developer. The error messages
   // should be written in English. Use the EDev routines to raise an error.
   EDeveloper = class (Exception);

   // Use EInternal when the error should not occur. Usaully EInternal is
   // preferred above code using an assertion. The error messages should be
   // written in English. Use the EInt routines to raise an error.
   EInternal = class (Exception);


// Routines to raise the default exceptions.
// The overloaded version with the ARRAY OF const can be used with a format
// string in the aMessage parameter.
procedure EUsr(const aMessage: string); overload;
procedure EUsr(const aMessage: string; const aArgs: array of const); overload;

procedure EDev(const aMessage: string); overload;
procedure EDev(const aMessage: string; const aArgs: array of const); overload;

procedure EInt(const aMethod, aMessage: string); overload;
procedure EInt(const aMethod, aMessage: string; const aArgs: array of const); overload;

implementation

uses
  common.Utils;

function FormatMessage(aStr: string): string;
  begin
  while Copy(aStr, Length(aStr)-1, 2)=#13#10 do Delete(aStr, Length(aStr)-1, 2);
  result := CheckTrailingDot(aStr);
  end;

procedure EUsr(const aMessage: string);
  begin
  raise EUser.Create(FormatMessage(aMessage));
  end;

procedure EUsr(const aMessage: string; const aArgs: array of const);
  begin
  raise EUser.CreateFmt(FormatMessage(aMessage), aArgs);
  end;

procedure EDev(const aMessage: string);
  begin
  raise EDeveloper.Create(FormatMessage(aMessage));
  end;

procedure EDev(const aMessage: string; const aArgs: array of const);
  begin
  raise EDeveloper.CreateFmt(FormatMessage(aMessage), aArgs);
  end;

procedure EInt(const aMethod, aMessage: string);
  begin
  raise EInternal.Create('Internal error. Location: '+aMethod+'. Message: '+FormatMessage(aMessage));
  end;

procedure EInt(const aMethod, aMessage: string; const aArgs: array of const);
  begin
  raise EInternal.CreateFmt(aMethod+' - '+FormatMessage(aMessage), aArgs);
  end;

end.
