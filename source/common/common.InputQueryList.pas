// -----------------------------------------------------------------------------
//
// common.InputQueryList
//
// -----------------------------------------------------------------------------
//
// Dialog to ask a value from a predefined list.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV
//
// -----------------------------------------------------------------------------

unit common.InputQueryList;

{$MODE Delphi}

interface

uses
  Forms, Controls, StdCtrls, ExtCtrls, Classes;

type
  TfInputQueryList =
    class (TForm)
        pnlButtons: TPanel;
        btnOK: TButton;
        btnCancel: TButton;
        pnlMain: TPanel;
        lQuestion: TLabel;
        cbOptions: TComboBox;
        procedure cbOptionsChange(Sender: TObject);
        procedure btnOKClick(Sender: TObject);
        procedure btnCancelClick(Sender: TObject);
        procedure FormActivate(Sender: TObject);

      private
        fActivated: boolean;
        fOptionRequired: boolean;

      protected

      public
        procedure Init(aCaption, aQuestion: string; aOptions: TStringlist; aOptionRequired: boolean);
      end;

function InputQueryList(const aCaption, aQuestion: string; aOptions: array of string; aOptionRequired: boolean; out aOption: string): Boolean; overload;
function InputQueryList(const aCaption, aQuestion: string; aOptions: TStringlist; aOptionRequired: boolean; out aOption: string): Boolean; overload;

implementation

uses common.ErrorHandling;

{$R *.lfm}

procedure TfInputQueryList.Init(aCaption, aQuestion: string; aOptions: TStringlist; aOptionRequired: boolean);
  begin
  Caption := aCaption;
  lQuestion.Caption := aQuestion;
  fOptionRequired := aOptionRequired;
  fActivated := false;

  cbOptions.Items.AddStrings(aOptions);
  if cbOptions.Items.Count=0
     then EInt('TfInputQueryList.Init', 'No options');

  cbOptionsChange(Self);
  end;

procedure TfInputQueryList.btnCancelClick(Sender: TObject);
  begin
  ModalResult := mrCancel;
  end;

procedure TfInputQueryList.btnOKClick(Sender: TObject);
  begin
  ModalResult := mrOk;
  end;

procedure TfInputQueryList.cbOptionsChange(Sender: TObject);
  begin
  if (cbOptions.Text <> '') and (cbOptions.Items.IndexOf(cbOptions.Text) = -1)
     then begin
          cbOptions.ItemIndex := -1;
          btnOk.Enabled := false
          end
     else begin
          if fOptionRequired and ((cbOptions.ItemIndex = -1) or (cbOptions.ItemIndex >= cbOptions.Items.Count))
             then btnOK.Enabled := false
             else btnOK.Enabled := true;
          end;
  end;

procedure TfInputQueryList.FormActivate(Sender: TObject);
  begin
  if fActivated then exit;
    cbOptions.SetFocus;
  fActivated := true;
  end;

function InputQueryList(const aCaption, aQuestion: string; aOptions: TStringlist; aOptionRequired: Boolean; out aOption: string): Boolean;
  var
    lDialog: TfInputQueryList;
    lModalResult: integer;
  begin
  lDialog := TfInputQueryList.Create(Application);
  lDialog.Init(aCaption, aQuestion, aOptions, aOptionRequired);
  lModalResult := lDialog.ShowModal;
  result := lModalResult = mrOk;
  if lDialog.cbOptions.ItemIndex>=0
     then aOption := lDialog.cbOptions.Items[lDialog.cbOptions.ItemIndex]
     else aOption := '';
  lDialog.Free;
  end;

function InputQueryList(const aCaption, aQuestion: string; aOptions: array of string; aOptionRequired: Boolean; out aOption: string): Boolean;
  var
    i: integer;
    lOptionList: TStringlist;
  begin
  lOptionList := TStringlist.Create;
  try
    for i := Low(aOptions) to High(aOptions) do
      lOptionList.Add(aOptions[i]);

    result := InputQueryList(aCaption, aQuestion, lOptionList, aOptionRequired, aOption);

  finally
    lOptionList.Free;
    end;
  end;

end.
