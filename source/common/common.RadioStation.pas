// -----------------------------------------------------------------------------
//
// common.RadioStation
//
// -----------------------------------------------------------------------------
//
// Classes to broadcast and listen to events.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV
//
// -----------------------------------------------------------------------------
//
//   If an object want to listen to a radio it needs to implement a method with
//   the following specifications: The first parameter (the sender) is the
//   radiostation which sends the message. Because of this parameter the notify
//   procedure can be used to listen at multiple radio's. The broadcasted message
//   type is specified by the aRadioEvent parameter. The aData parameter refers
//   to a blok of data associated with the message type.
//   A radio station can catch a message by setting the aRadioEvent parameter
//   to reClear, this will stop the broadcasting of that message.
//   Just before a radiostation is distroyed it send a reLinkLost event. After
//   this event the radio will no longer receive message from the station and
//   is automaticly unregistered from the station.
//
// -----------------------------------------------------------------------------

unit common.RadioStation;

{$MODE Delphi}

interface

uses
  common.SimpleLists;

// Debug
const
  dtEventBroadcast = 'EventBroadcast';
  dtEventHandling = 'EventHandling';

const
  // Return value to indicate a message is handled
  cRadioEvent_Clear = 'common.RadioStation.Clear';

  // Indicates the link between the listner and the station will be lost
  cRadioEvent_Offline = 'common.RadioStation.Offline'; // Object = nil

type
  // Used to broadcast messages
  TcRadioStation_Event = string;

  // Should be implemented to listen to one or more radiostations
  TcRadioStation_NotifyEvent  =
    procedure (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData : Pointer) of object;


  // __________________________________________________________________________
  //
  // # TcRadio
  // __________________________________________________________________________
  //
  // Description:
  //
  //   Wrapper for an object that can receive broadcasted messages
  // __________________________________________________________________________
  //
  TcRadio =
    class
      protected
        fRadio: TObject;
        fNotifyProcedure: TcRadioStation_NotifyEvent;
        fWantNotification: Boolean;
      public
        constructor Create(aRadio: TObject; aNotifyProcedure: TcRadioStation_NotifyEvent;
                           aWantNotification: Boolean);
      end;

  // __________________________________________________________________________
  //
  // # TcRadioStation
  // __________________________________________________________________________
  //
  // Description:
  //
  //   Sends messages to registered listners.
  // __________________________________________________________________________
  //
  TcRadioStation =
    class
      private
        fRadios: TcList;

        // Used for the synchronized broadcast. Since we cannot use anonymous
        // methods, we need something to store the data.
        fCurrentSynchronizationNotification:
          record
            InBroadCast: Boolean;
            Radio: TcRadio;
            RadioEvent: TcRadioStation_Event;
            Data: Pointer;
          end;


        procedure SynchronizeNotification;
      public
        constructor Create;
        destructor  Destroy; override;

        // Use this method, to add a radio (=listner) to the station.
        // The radio must implement a method to listen. (aNotifyProcedure)
        procedure AddRadio (aRadio: TObject; aNotifyProcedure: TcRadioStation_NotifyEvent);

        // Use this method to remove a radio from the station.
        procedure RemoveRadio (aRadio: TObject);

        // Use this method to switch a registered radio on or off.
        procedure ChangeNotification (aRadio: TObject; aWantNotification: Boolean);

        // Change the NotifyProcedure of a registered radio.
        procedure ChangeNotifyProc (aRadio: TObject; aNotifyProcedure: TcRadioStation_NotifyEvent);

        // Send a message to all radio's. Note that an object does not have
        // to be a listner to be able to send a message.
        procedure BroadCast (aRadioEvent: TcRadioStation_Event; aData: Pointer);
      end;

// # Instances of the Radio Station
var
  gRadioStation: TcRadioStation;

implementation

uses
  Classes, SysUtils, common.ErrorHandling, ace.Debug;

// _____________________________________________________________________________
//
// # Radiostations
// _____________________________________________________________________________
//

{ TcmRadio }

constructor TcRadio.Create(aRadio: TObject; aNotifyProcedure: TcRadioStation_NotifyEvent; aWantNotification: Boolean);
  begin
  inherited Create;
  fRadio := aRadio;
  fNotifyProcedure := aNotifyProcedure;
  fWantNotification := aWantNotification;
  end;


{ TcRadioStation }

constructor TcRadioStation.Create;
  begin
  inherited;
  fRadios := TcList.Create;
  end;

destructor TcRadioStation.Destroy;
  begin
  Broadcast(cRadioEvent_Offline, nil);
  fRadios.Free;

  inherited Destroy;
  end;

procedure TcRadioStation.AddRadio(aRadio: TObject; aNotifyProcedure: TcRadioStation_NotifyEvent);
  var
    lRadio: TcRadio;
  begin
  lRadio := TcRadio.Create(aRadio, aNotifyProcedure, True);
  fRadios.Add(lRadio);
  end;

procedure TcRadioStation.RemoveRadio(aRadio: TObject);
  var
    T: integer;
  begin
  for T := 0 to fRadios.Count-1 do
    if TcRadio(fRadios.Objects[T]).fRadio = aRadio
       then begin
            fRadios.KillAt(T);
            Exit;
            end;
  end;

procedure TcRadioStation.ChangeNotification(aRadio: TObject; aWantNotification: Boolean);
  var
    T: integer;
  begin
  for T := 0 to fRadios.Count-1 do
    if TcRadio(fRadios.Objects[T]).fRadio = aRadio
       then begin
            TcRadio(fRadios.Objects[T]).fWantNotification := aWantNotification;
            Exit;
            end;
  end;

procedure TcRadioStation.ChangeNotifyProc(aRadio: TObject; aNotifyProcedure: TcRadioStation_NotifyEvent);
  var
    T: integer;
  begin
  for T := 0 to fRadios.Count-1 do
    if TcRadio(fRadios.Objects[T]).fRadio = aRadio
       then begin
            TcRadio(fRadios.Objects[T]).fNotifyProcedure := aNotifyProcedure;
            Exit;
            end;
  end;

procedure TcRadioStation.SynchronizeNotification;
  begin
  with fCurrentSynchronizationNotification do
    begin
    Radio.fNotifyProcedure(Self, RadioEvent, Data);
    InBroadCast := False;
    end;
  end;

procedure TcRadioStation.BroadCast(aRadioEvent: TcRadioStation_Event; aData: Pointer);
  var
    T: integer;
    lRadioEvent: TcRadioStation_Event;

  begin
  if aRadioEvent<>cRadioEvent_Offline
     then Debug(dtEventBroadcast, aRadioEvent);

  // Notify all Radios off the occured event
  lRadioEvent := aRadioEvent;
  for T := 0 to fRadios.Count-1 do
    begin
    if (TcRadio(fRadios.Objects[T]).fRadio <> nil) and
       (TcRadio(fRadios.Objects[T]).fWantNotification)
       then begin
            // Since current FPC doesn't support anonymous methods, store the
            // data in a temp structur and call the Synchonize. For failsafe
            // make sure there is only 1 broadcast scheduled.
            if fCurrentSynchronizationNotification.InBroadCast then
              EInt('TcRadioStation.BroadCast', 'InBroadCast for Synchronize');
            with fCurrentSynchronizationNotification do
              begin
              Radio := TcRadio(fRadios.Objects[T]);;
              RadioEvent:= aRadioEvent;
              Data := aData;
              end;
            TThread.Synchronize(TThread.CurrentThread, SynchronizeNotification);
            end;
    if lRadioEvent = cRadioEvent_Clear then Exit;
    end;
  end;

initialization
  gRadioStation := TcRadioStation.Create;

finalization
  FreeAndNil(gRadioStation);
end.

