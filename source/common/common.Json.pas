// -----------------------------------------------------------------------------
//
// common.Json
//
// -----------------------------------------------------------------------------
//
// Miscellaneous Json utility routines for Delphi compatibility.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV
//
// -----------------------------------------------------------------------------
unit common.Json;

{$MODE Delphi}

interface

uses
  Classes, SysUtils, fpjson;

type
  // Alias for Delphi compatibility
  TJsonValue = TJSonData;

  { TJSonObjectHelper }
  // Helper for FPC to support Delphi methods
  TJSonObjectHelper = class helper for TJSonObject
  private
  public
    procedure AddPair(const aStr: string; const aVal: string); overload;
    procedure AddPair(const aStr: string; const aVal: TJSONData); overload;

    function TryGetValue(Key: string; out Val: string): boolean; overload;
    function TryGetValue(Key: string; out Val: integer): boolean; overload;
    function TryGetValue(Key: string; out Val: boolean): boolean; overload;
    function TryGetValue(Key: string; out Val: double): boolean; overload;
    function TryGetValue(Key: string; out Val: TJsonObject): boolean; overload;
    function TryGetValue(Key: string; out Val: TJsonArray): boolean; overload;

    function Format(Indentation: integer): string;

    class function ParseJSONValue(aJsonStr: String): TJSonObject;

  end;

implementation

{ TJSonObjectHelper }
procedure TJSonObjectHelper.AddPair(const aStr: string; const aVal: string);
begin
  Add(aStr, aVal);
end;

procedure TJSonObjectHelper.AddPair(const aStr: string; const aVal: TJSONData);
begin
  Add(aStr, aVal);
end;

function TJSonObjectHelper.TryGetValue(Key: string; out Val: string): boolean;
  var
    lData: TJSonData;
  begin
  lData := Find(Key);
  Result := Assigned(lData);
  if Result
    then Val := lData.AsString;
  end;

function TJSonObjectHelper.TryGetValue(Key: string; out Val: integer): boolean;
  var
    lData: TJSonData;
  begin
  lData := Find(Key);
  Result := Assigned(lData);
  if Result
    then Val := lData.AsInteger;
  end;

function TJSonObjectHelper.TryGetValue(Key: string; out Val: boolean): boolean;
  var
    lData: TJSonData;
  begin
  lData := Find(Key);
  Result := Assigned(lData);
  if Result
    then Val := lData.AsBoolean;
  end;

function TJSonObjectHelper.TryGetValue(Key: string; out Val: double): boolean;
  var
    lData: TJSonData;
  begin
  lData := Find(Key);
  Result := Assigned(lData);
  if Result
    then Val := lData.AsFloat;
  end;


function TJSonObjectHelper.TryGetValue(Key: string; out Val: TJsonObject): boolean;
  begin
  Result := Find(Key, Val);
  end;

function TJSonObjectHelper.TryGetValue(Key: string; out Val: TJsonArray): boolean;
  begin
  Result := Find(Key, Val);
  end;

function TJSonObjectHelper.Format(Indentation: integer): string;
  begin
  Result := FormatJSON(DefaultFormat, Indentation);
  end;

class function TJSonObjectHelper.ParseJSONValue(aJsonStr: String): TJSonObject;
  var
    lData: TJSonData;
  begin
  lData := GetJSON(aJsonStr);
  Result := lData as TJSonObject;
  end;

end.

