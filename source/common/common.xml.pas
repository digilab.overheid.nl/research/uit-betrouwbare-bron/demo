// -----------------------------------------------------------------------------
//
// common.XML
//
// -----------------------------------------------------------------------------
//
// Miscellaneous XML utility routines for Delphi compatibility.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV
//
// -----------------------------------------------------------------------------
unit common.XML;

{$MODE Delphi}

interface

uses
  Classes, SysUtils, DOM;

const
  ntElement = ELEMENT_NODE;
  ntAttribute = ATTRIBUTE_NODE;
  ntText = TEXT_NODE;
  ntCData = CDATA_SECTION_NODE;
  ntEntityRef = ENTITY_REFERENCE_NODE;
  ntEntity = ENTITY_NODE;
  ntProcessingInstr = PROCESSING_INSTRUCTION_NODE;
  ntComment = COMMENT_NODE;
  ntDocument = DOCUMENT_NODE;
  ntDocType = DOCUMENT_TYPE_NODE;
  ntDocFragment = DOCUMENT_FRAGMENT_NODE;
  ntNotation = NOTATION_NODE;

type
  IXMLNode = TDOMNode;
  TNodeType = Integer;

implementation

end.

