// -----------------------------------------------------------------------------
//
// common.FileUtils
//
// -----------------------------------------------------------------------------
//
// Miscellaneous file utility routines for Delphi compatibility.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV
//
// -----------------------------------------------------------------------------

unit common.FileUtils;

{$MODE Delphi}

interface

uses
  Classes, SysUtils;

type
  { TPath }
  TPath = record
    public
      class function GetFilenameWithoutExtension(const aFileName: String): String; static;
      class function Combine(const aPath1, aPath2: String): String; static;
      class function GetDirectoryName(const aFilename: String): String; static;
      class function ChangeFileExt(const FileName, Extension: String): String; static;

    end;

implementation

{ TPath }
class function TPath.GetFilenameWithoutExtension(const aFileName: String): String;
  begin
  Result :=  ChangeFileExt(ExtractFileName(aFileName), '');
  end;

class function TPath.Combine(const aPath1, aPath2: String): String;
begin
  Result := ConcatPaths([aPath1, aPath2]);
end;

class function TPath.GetDirectoryName(const aFilename: String): String;
begin
  Result := ExtractFileDir(aFilename);
end;

class function TPath.ChangeFileExt(const FileName, Extension: string): string;
begin
  Result := SysUtils.ChangeFileExt(FileName, Extension);
end;

end.

