// -----------------------------------------------------------------------------
//
// common.SimpleLists
//
// -----------------------------------------------------------------------------
//
// Simple list classes expressed as wrappers around Delphi classes.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV
//
// -----------------------------------------------------------------------------
//
//   These classes are introduced to expose the properties and methods that
//   are actually used in the code. Reason for exposure is reimplementation
//   on other platforms. It prevents reimplementation of all properties and
//   methods of the underlying Delphi classes. Only the exposed properties
//   need to be reimplemented.
//
// -----------------------------------------------------------------------------

unit common.SimpleLists;

{$MODE Delphi}

interface

uses
  Classes, Generics.Collections, Generics.Defaults;

type
  // String-Object List
  //
  //   Tuple = string, object
  //   Param Owned = Are the objects destroyed by the list?
  //   Param Sorted = Are the tuples sorted on the string value?
  // Note: the list used to be a TStringList for simplicity. however, this
  // caused unexplained memory overwrites in TchGroup.GroupClaimTypes.
  TcStringObjectList<T> =
    class
      private type
        TPairType = TPair<String, T>;
        TListType = TSortedList<TPairType>;

        TKeyComparer = class(TInterfacedObject, IComparer<TPairType>)
          public
            function Compare(constref ALeft, ARight: TPairType): Integer;
          end;
      private
        fOwned: boolean;
        fList: TListType;

        function GetString(const aIndex: integer): string;
        function GetObject(const aIndex: integer): T;
        function GetCount: integer;

      protected
        property List: TListType read fList;

      public
        constructor Create(aOwned, aSorted: boolean);
        destructor Destroy; override;

        procedure Add(aString: string; aObject: T);
        procedure Delete(aIndex: integer);
        procedure Clear;
        procedure FreeAndClear;

        function AsString: string; // Comma separated string with values

        property Owned: boolean read fOwned;

        // Sorted list: Binary search
        // Unsorted list: Linear search (case insensitive)
        function Find(aString: string; out aIndex: integer): boolean; overload;
        function Find(aString: string): T; overload;

        function IndexOf(const S: string): integer;
        function IndexOfObject(aObject: T): integer;

        property Strings[const aIndex: integer]: string read GetString; default;
        property Objects[const aIndex: integer]: T read GetObject;

        property Count: integer read GetCount;
      end;

  // Key-Value-Object list
  //
  //   Descendant of TcStringObjectList
  //   Strings are expected to be of the format 'Key=Value'.
  //
  TcKeyValueObjectList<T: class> =
    class
      private type
        TPairType = TPair<String, String,T>;
        TListType = TSortedList<TPairType>;

        TKeyComparer = class(TInterfacedObject, IComparer<TPairType>)
          public
            function Compare(constref ALeft, ARight: TPairType): Integer;
          end;
      private
        fOwned: boolean;

      protected
        fList: TListType;

        function GetValue(const Name: string): string;
        procedure SetValue(const Name, Value: string);
        function GetCount: integer;
        function GetObject(const aIndex: integer): T;

      public
        constructor Create(aOwned, aSorted: boolean);
        destructor Destroy; override;
        property Owned: boolean read fOwned;
        procedure Clear;
        procedure FreeAndClear;
        procedure Add(aKey, aValue: string; aObject: T);

        function IndexOfName(const Name: string): integer;
        function NameByIndex(aIndex: integer): string;
        function ValueByIndex(aIndex: integer): string;
        property Values[const Name: string]: string read GetValue write SetValue;
        property Count: integer read GetCount;
        property Objects[const aIndex: integer]: T read GetObject;
      end;

  // List
  //
  //   Basic unsorted list, owning the object (they are destroyed with the list)
  //
  TcList =
    class
      private
        fList: TList;
        function GetObject(const aIndex: integer): TObject;
        function GetCount: integer;

      public
        constructor Create;
        destructor Destroy; override;

        function Add(aObject: TObject): integer; virtual;
        procedure Insert(aIndex: integer; aObject: TObject);
        procedure Assign(aList: TcList);
        function IndexOf(aObject: TObject): integer;
        procedure RemoveAt(aIndex: integer);
        procedure Remove(aObject: TObject);
        procedure RemoveAll;
        procedure KillAt(aIndex: integer);
        procedure KillAll;
        procedure Clear;

        property Objects[const aIndex: integer]: TObject read GetObject; default;
        property Count: integer read GetCount;
      end;

  // Sorted List
  //
  //   Descendant of TcList that allows sorting on the contents of the objects.
  //   This list can only be used by creating a subtype. In this subtype the
  //   compare methods should be implemented.
  //
  TcSortedList =
    class (TcList)
      protected
        fIgnoreDuplicates: boolean;

        // Override this method to compare two objects in the list
        //   if A > B then result := >0
        //   if A = B then result := 0
        //   if A < B then result := <0
        function Compare (const aObjectA, aObjectB: TObject): integer; overload; virtual; abstract;

        // Override this method to compare the objects in the list to a single
        // keyvalue represented as string.
        //   if Obj > Key then result := >0
        //   if Obj = Key then result := 0
        //   if Obj < Key then result := <0
        function Compare (const aObject: TObject; const aKeyValue: string): integer; overload; virtual; abstract;

      public
        constructor Create;

        function Add(aObject: TObject): integer; override;

        // Search using a single value key (represented as string).
        // Returns -1 if no object was found.
        function PosOf (aKeyValue: string): integer; overload;

        // Search using the content of a sample/search object.
        // Returns -1 if no object was found.
        function PosOf (aObject: TObject): integer; overload;

        // Search using a single value key (represented as string).
        function Search (aKeyValue: string; out aPos: integer): boolean; overload;

        // Search using the content of a sample/search object.
        function Search (aObject: TObject; out aPos: integer): boolean; overload;

        // Searches using a single value key and returns the object.
        function Find (const aKeyValue: string): TObject;

        property IgnoreDuplicates: boolean read fIgnoreDuplicates write fIgnoreDuplicates;
      end;

  // Integer list
  //
  //   List with integers.
  //   Can be sorted, can be set to ignore duplicates.
  //

  { TcIntegerList }

  TcIntegerList =
    class
      private
        function GetIgnoreDuplicates: boolean;
        function GetSorted: boolean;
        procedure SetIgnoreDuplicates(AValue: boolean);
        procedure SetSorted(const aValue: boolean);

      protected
        fList: TSortedList<Integer>;

        function GetInteger(aIndex: integer): integer;
        procedure SetInteger(aIndex: integer; AValue: integer);
        function GetCount: integer;

      public
        constructor Create;
        destructor Destroy; override;

        procedure Assign(aList: TcIntegerList);
        procedure Clear;
        procedure Insert(const aIndex, aInteger: integer);
        procedure Add(aInteger: integer);
        procedure RemoveAt(const aIndex: integer);
        procedure Remove(const aInteger: integer);
        function IndexOf(const aInteger: integer): integer;
        function Find(const aInt: integer; out aIndex: integer): boolean;

        property Count: integer read GetCount;
        property Values[aIndex: integer]: integer read GetInteger write SetInteger; default;
        property Sorted: boolean read GetSorted write SetSorted;
        property IgnoreDuplicates: boolean read GetIgnoreDuplicates write SetIgnoreDuplicates;
      end;

implementation

uses
  SysUtils, common.Utils;


{ TcStringObjectList<T> }

constructor TcStringObjectList<T>.Create(aOwned, aSorted: boolean);
  begin
  inherited Create;
  fOwned := aOwned;

  fList := TListType.Create(TKeyComparer.Create);
  fList.Sorted := aSorted;
  if aSorted
    then List.Duplicates := dupError;
  end;

destructor TcStringObjectList<T>.Destroy;
  begin
  FreeAndClear;
  FreeAndNil(fList);
  inherited;
  end;

function TcStringObjectList<T>.GetString(const aIndex: integer): string;
  begin
  result := List[aIndex].Key;
  end;

function TcStringObjectList<T>.GetObject(const aIndex: integer): T;
  begin
  result := List[aIndex].Value;
  end;

function TcStringObjectList<T>.GetCount: integer;
  begin
  result := List.Count;
  end;

procedure TcStringObjectList<T>.Add(aString: string; aObject: T);
  var
    lPair: TPairType;
  begin
  lPair.Key := aString;
  lPair.Value := aObject;
  List.Add(lPair);
  end;

procedure TcStringObjectList<T>.Delete(aIndex: integer);
  begin
  List.Delete(aIndex);
  end;

procedure TcStringObjectList<T>.Clear;
  begin
  List.Clear;
  end;

procedure TcStringObjectList<T>.FreeAndClear;
  var
    i: integer;
    lObject: TObject;
  begin
  if Owned
    then begin
         for i := 0 to List.Count-1 do
           begin
           lObject := List[i].Value;
           FreeAndNil(lObject);
           end;
         end;
  Clear;
  end;

function TcStringObjectList<T>.AsString: string;
  var
    lStrLst: TStringList;
    lPair: TPairType;
  begin
  lStrLst := TStringList.Create;
  for lPair in fList do
    lStrLst.Add(lPair.Key);
  result := StringListToString(lStrLst, ', ');
  lStrLst.Free;
  end;

function TcStringObjectList<T>.IndexOf(const S: string): integer;
  begin
  if not Find(S, result)
    then result := -1;
  end;

function TcStringObjectList<T>.IndexOfObject(aObject: T): integer;
  var
    i: integer;
  begin
  for i := 0 to List.Count - 1 do
    if List[i].Value = aObject
      then begin
           Exit(I);
           end;
  result := -1;
  end;

function TcStringObjectList<T>.Find(aString: string; out aIndex: integer): boolean;
  var
    lPair: TPairType;
    lIndex: SizeInt;
  begin
  lPair.Key := aString;

  if List.Sorted
     then begin
          // Binary search crashed with an empty list
          lIndex := 0;
          result := (List.Count > 0) and List.BinarySearch(lPair, lIndex);
          aIndex := lIndex;
          end
     else begin
          aIndex := List.IndexOf(lPair);
          result := aIndex > -1;
          end;
  end;

function TcStringObjectList<T>.Find(aString: string): T;
  var
    i: integer;
  begin
  if Find(aString, i)
    then result := Objects[i]
    else result := nil;
  end;


{ TcStringObjectList.TKeyComparer }

function TcStringObjectList<T>.TKeyComparer.Compare(constref ALeft,
  ARight: TPairType): Integer;
begin
  Result := CompareText(aLeft.Key, aRight.Key);
end;

{ TcKeyValueObjectList }

constructor TcKeyValueObjectList<T>.Create(aOwned, aSorted: boolean);
  begin
  inherited Create;
  fOwned := aOwned;

  fList := TListType.Create(TKeyComparer.Create);
  fList.Sorted := aSorted;
  if aSorted
    then fList.Duplicates := dupError;
  end;

destructor TcKeyValueObjectList<T>.Destroy;
  begin
  FreeAndClear;
  FreeAndNil(fList);
  inherited;
  end;

procedure TcKeyValueObjectList<T>.Clear;
  begin
  fList.Clear;
  end;


procedure TcKeyValueObjectList<T>.FreeAndClear;
  var
    i: integer;
    lObject: TObject;
  begin
  if Owned
    then begin
         for i := 0 to fList.Count-1 do
           begin
           lObject := fList[i].Info;
           FreeAndNil(lObject);
           end;
         end;
  Clear;
  end;


procedure TcKeyValueObjectList<T>.Add(aKey, aValue: string; aObject: T);
  var
     lPair: TPairType;
  begin
  lPair.Key := aKey;
  lPair.Value := aValue;
  lPair.Info := aObject;
  fList.Add(lPair);
  end;

function TcKeyValueObjectList<T>.IndexOfName(const Name: string): integer;
  var
    lPair: TPairType;
    lIndex: SizeInt;
  begin
  lPair.Key := Name;

  if fList.Sorted
     then begin
          if fList.BinarySearch(lPair, lIndex)
            then result := lIndex
            else result := -1;
          end
     else begin
          result := fList.IndexOf(lPair);
          end;
  end;


function TcKeyValueObjectList<T>.NameByIndex(aIndex: integer): string;
  begin
  Result := fList[aIndex].Key;
  end;

function TcKeyValueObjectList<T>.ValueByIndex(aIndex: integer): string;
  begin
  Result := fList[aIndex].Value;
  end;

function TcKeyValueObjectList<T>.GetValue(const Name: string): string;
  var
    lIndex: SizeInt;
  begin
  lIndex := IndexOfName(Name);
  if lIndex > -1
    then result := fList[lIndex].Value
    else result := '';
  end;

procedure TcKeyValueObjectList<T>.SetValue(const Name, Value: string);
  var
    lIndex: int64;
    lPair: TPairType;
  begin
  lIndex := IndexOfName(Name);
  if lIndex > -1
    then begin
         lPair := fList[lIndex];
         lPair.Value := Value;
         fList[lIndex] := lPair;
         end
    else Add(Name, Value, nil);
  end;

function TcKeyValueObjectList<T>.GetCount: integer;
  begin
  result := fList.Count;
  end;

function TcKeyValueObjectList<T>.GetObject(const aIndex: integer): T;
  begin
  result := fList[aIndex].Info;
  end;

{ TcStringObjectList.TKeyComparer }

function TcKeyValueObjectList<T>.TKeyComparer.Compare(constref ALeft,
  ARight: TPairType): Integer;
begin
  Result := CompareText(aLeft.Key, aRight.Key);
end;


{ TcList }

constructor TcList.Create;
  begin
  fList := TList.Create;
  inherited;
  end;

destructor TcList.Destroy;
  begin
  KillAll;
  fList.Free;
  inherited;
  end;

function TcList.GetObject(const aIndex: integer): TObject;
  begin
  result := fList[aIndex];
  end;

function TcList.GetCount: integer;
  begin
  result := fList.Count;
  end;

function TcList.Add(aObject: TObject): integer;
  begin
  result := fList.Count;
  fList.Add(aObject);
  end;

procedure TcList.Insert(aIndex: integer; aObject: TObject);
  begin
  fList.Insert(aIndex, aObject);
  end;

procedure TcList.Assign(aList: TcList);
  begin
  fList.Assign(aList.fList);
  end;

function TcList.IndexOf(aObject: TObject): integer;
  begin
  result := fList.IndexOf(aObject);
  end;

procedure TcList.Clear;
  begin
  fList.Clear;
  end;

procedure TcList.RemoveAt(aIndex: integer);
  begin
  fList.Delete(aIndex);
  end;

procedure TcList.Remove(aObject: TObject);
  begin
  fList.Remove(aObject);
  end;

procedure TcList.RemoveAll;
  begin
  fList.Clear;
  end;

procedure TcList.KillAt(aIndex: integer);
  var
    lObject: TObject;
  begin
  lObject := fList[aIndex];
  fList.Delete(aIndex);
  if Assigned(lObject)
     then lObject.Free;
  end;

procedure TcList.KillAll;
  var
    i: integer;
    lObject: TObject;
  begin
  for i := 0 to fList.Count-1 do
    begin
    if Assigned(fList[i])
       then begin
            lObject := fList[i];
            lObject.Free;
            end;
    end;
  Clear;
  end;


{ TcSortedList }

constructor TcSortedList.Create;
  begin
  inherited Create;
  fIgnoreDuplicates := False;
  end;

function TcSortedList.Add(aObject: TObject): integer;
  var
    t: integer;
  begin
  t := -1;

  // Search for the element in the list, only add the element if it does not
  // exists or if duplicates are allowed.
  if not Search(aObject, t) or IgnoreDuplicates // Do not change the order, the t is set in the Search
     then Insert(t, aObject)
     else raise Exception.Create('Attempt to insert duplicate object');

  result := t;
  end;

function TcSortedList.PosOf(aKeyValue: string): integer;
  var
    t: integer;
  begin
  result := -1;
  if Search(aKeyValue, t)
     then result := t; // Duplicates are ignored, just return the first hit.
  end;

function TcSortedList.PosOf(aObject: TObject): integer;
  var
    t: integer;
  begin
  result := -1;
  if Search(aObject, t)
     then begin
          // If duplicates are allowed, search for the pointer value and ignore
          // the key value.
          if IgnoreDuplicates
             then begin
                  while (t < Count) and (aObject <> fList[t]) do Inc(t);
                  end;

          if (t < Count) then result := t;
          end;
  end;

function TcSortedList.Search(aKeyValue: string; out aPos: integer): Boolean;
  var
    low, high, index, compared: integer;
  begin
  // Default binary search algoritm
  // Adjusted for duplicate key values
  // In case of a duplicate value, the first hit will be returned
  result := false;
  low := 0;
  high := Count - 1;
  while low <= high do
    begin
    index := (low + high) shr 1;
    compared := Compare(fList[index], aKeyValue);
    if compared < 0
       then low := index + 1
       else begin
            high := index - 1;
            if compared = 0
               then begin
                    result := true;
                    if not IgnoreDuplicates then low := index;
                    end;
            end;
    end;
  aPos := low;
  end;

function TcSortedList.Search(aObject: TObject; out aPos: integer): Boolean;
  var
    low, high, index, compared: integer;
  begin
  // Default binary search algoritm
  // Adjusted for duplicate key values
  // In case of a duplicate value, the first hit will be returned
  result := false;
  low := 0;
  high := Count - 1;
  while low <= high do
     begin
     index := (low + high) shr 1;
     compared := Compare(fList[index], aObject);
     if compared < 0
        then low := index + 1
        else begin
             high := index - 1;
             if compared = 0
                then begin
                     result := true;
                     if not IgnoreDuplicates then low := index;
                     end;
             end;
     end;
  aPos := low;
  end;

function TcSortedList.Find(const aKeyValue: string): TObject;
  var
    t: integer;
  begin
  result := nil;
  if Search(aKeyValue, t)
     then result := Objects[t];
  end;


{ TcIntegerList }

constructor TcIntegerList.Create;
  begin
  inherited Create;
  fList := TSortedList<Integer>.Create;
  end;

destructor TcIntegerList.Destroy;
  begin
  fList.Free;
  inherited Destroy;
  end;

procedure TcIntegerList.Assign (aList: TcIntegerList);
  var
    i: integer;
  begin
  fList.Clear;
  fList.Capacity := aList.fList.Count;
  for i := 0 to aList.fList.Count - 1 do
     fList.Add(aList.fList[i]);
  end;

procedure TcIntegerList.Clear;
  begin
  fList.Clear;
  end;

procedure TcIntegerList.Add(aInteger: integer);
//  var
//    lIndex: integer;
  begin
(*  if Sorted
     then begin
          if Find(aInteger, lIndex) and IgnoreDuplicates
             then Exit;
          end
     else lIndex := fList.Count;
*)
  fList.Add(aInteger);
  end;

procedure TcIntegerList.Insert (const aIndex, aInteger: integer);
  begin
  fList.Insert(aIndex, aInteger);
  end;

procedure TcIntegerList.RemoveAt (const aIndex: integer);
  begin
  fList.Delete(aIndex);
  end;

procedure TcIntegerList.Remove(const aInteger: integer);
  var
     lIndex: integer;
  begin
  lIndex := IndexOf(aInteger);
  if lIndex = -1
     then raise Exception.Create('Object not in list.')
     else RemoveAt(lIndex);
  end;

function TcIntegerList.IndexOf(const aInteger: integer): integer;
  begin
  result := fList.IndexOf(aInteger);
  end;

function TcIntegerList.Find(const aInt: integer; out aIndex: integer): boolean;
  begin
  aIndex := fList.IndexOf(aInt);
  result := aIndex > -1;
  end;

function TcIntegerList.GetIgnoreDuplicates: boolean;
  begin
  result := fList.Duplicates = dupIgnore;
  end;

function TcIntegerList.GetSorted: boolean;
  begin
  result := fList.Sorted;
  end;

procedure TcIntegerList.SetIgnoreDuplicates(AValue: boolean);
  begin
  if aValue
    then fList.Duplicates := dupIgnore
    else fList.Duplicates := dupError;
  end;

procedure TcIntegerList.SetSorted(const aValue: boolean);
  begin
  fList.Sorted := aValue;
  end;

function TcIntegerList.GetInteger(aIndex: integer): integer;
  begin
  result := fList[aIndex];
  end;

procedure TcIntegerList.SetInteger(aIndex: integer; AValue: integer);
  begin
  fList[aIndex] := aValue;
  end;

function TcIntegerList.GetCount: integer;
  begin
  result := fList.Count;
  end;

end.
