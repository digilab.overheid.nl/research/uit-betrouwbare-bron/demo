// -----------------------------------------------------------------------------
//
// common.Utils
//
// -----------------------------------------------------------------------------
//
// Miscellaneous utility routines.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV
//
// -----------------------------------------------------------------------------

unit common.Utils;

{$MODE Delphi}

interface

uses
  Classes, SysUtils;


// -- Constants
const
  cLineBreak = #13#10;

// -- Strings

// Splits string at first occurence of the separator.
// if no separator is found Part1 will contain the whole string.
// Parts will be trimmed.
procedure SplitString(aStr: string; aSeparator: string; out aPart1, aPart2: string);

// Splits a string into a stringlist using the separator to split the values. Values are NOT trimmed.
function StringToStringList(aString: string; const aSeparator: string): TStringlist;

// Splits a string into a stringlist using the separator to split the values. Values are trimmed.
function StringToStringListTrimmed(aString: string; const aSeparator: string): TStringlist;

// Splits a string into a stringlist using the separator to split the values. Values are trimmed.
// Quoted values (single and double quotes) will be detected and handled.
// Double quotes within a quoted value are converted to a single quote in the value.
function StringToStringListWithQuotes(aString: string; const aSeparator: string): TStringlist;

// Concatenates the values of a stringlist to a string using the specified separator between the elements.
function StringListToString(const aStrLst: TStrings; const aSeparator: string): string; overload;

// Concatenates a stringlist to a string using the specified separator between the elements.
// The LastSeparator can be used to create sequences like 'A, B, C, and D'.
function StringListToString(const aStrLst: TStrings; const aSeparator, aLastSeparator: string): string; overload;

// Makes sure message ends with a dot
function CheckTrailingDot(const aMessage: string): string;

// Does the string start with the specified character?
function StartsWithChar(aStr: string; aChar: char): boolean;

// Identifier should start with a letter. Subsequente characters should either be letters or digits.
// All other characters are ignored.
function ToIdentifier(const aIdentifier: string): string; overload;

// Identifier should start with a letter. Subsequente characters should either be letters or digits or part of the AdditionalChars set.
// All other characters are replace with aReplaceChar. aReplaceChar can be ''.
function ToIdentifier(const aIdentifier: string; const aAdditionalChars: TSysCharSet; const aReplaceChar: string): string; overload;

// Removes the specified name from the identifier.
// Created to remove names from identifiers such as 'person/name' or 'person-name'.
function RemoveNameFromIdentifier(const aIdentifier: string; const aName: string): string;


// -- Dialogs

// Dialogbox to open a file
// DefaultExt without .
function PromptForOpenFileName(aTitle, aDefaultExt, aInitialDir, aFilter: string; var aFileName: string): boolean;

// Dialogbox to save a file
// DefaultExt without .
function PromptForSaveFileName(aTitle, aDefaultExt, aInitialDir, aFilter: string; var aFileName: string): boolean;

// Dialogbox to select a folder
function PromptForFolder(const aCaption: string; var aFolder: string): Boolean;

// Dialogbox to ask for confirmation
function PromptForConfirmation(const aQuestion: string): boolean; overload;
function PromptForConfirmation(const aQuestion: string; const aArgs: array of const): boolean; overload;


// -- Filenames

// Create a filename by removing invalid characters from string
function ConvertToFilename(aStr: string): string;

// Removes the backslash at the end of a str (if there is one)
function RemoveTrailingBackslash(aStr: string): string;


// -- Files

// Scan a folder for every file (not directory) and return the contents in a stringlist. Uses  the TcFileIterator classes
procedure ScanFolder(const aFolder: string; aFiles: TStringList; aRecursive: Boolean = False); overload;
procedure ScanFolder(const aFolder, aFileMask: string; aFiles: TStringList; aRecursive: Boolean = False); overload;
procedure ScanFolder(const aFolder, aFileMask: string; aFiles, aFolders: TStringList; aRecursive: Boolean = False); overload;


// -- Value conversion

// Convert boolean value to yes/no
function BooleanToYesNo(const aBoolean: boolean): string;

// Convert a string to boolean value
// Accepted: 1/0, t/f, true/false, y/n, yes/no -- case insensitive
function TrueFalseToBoolean(const aStr: string): boolean;

// Convert boolean value to true/false
function BooleanToTrueFalse(const aBoolean: boolean): string;


// -- Misc

function ControlPressed: boolean;
function AltPressed: boolean;
function ShiftPressed: boolean;

// Retrieve version of application
function GetApplicationVersionStr: string;


// -- Streams

// Write the string to an existing stream including the string size
procedure WriteStringToStream(aStream: TStream; const aStr: string; aEncoding: TEncoding = nil);

// Read a string from a stream including the string size. If a size is given,
// the string may not exceed the MaxLength. If it does, an exception is raised
function ReadStringFromStream(aStream: TStream; aMaxLenght: integer = 0): string;


implementation

uses
  Dialogs, Controls, FileInfo,
  common.ErrorHandling, System.UITypes;

// -- Strings

procedure SplitString(aStr: string; aSeparator: string; out aPart1, aPart2: string);
  var
    lPos: integer;
  begin
  lPos := Pos(aSeparator, aStr);
  if lPos>0
     then begin
          aPart1 := Trim(Copy(aStr, 1, lPos-1));
          aPart2 := Trim(Copy(aStr, lPos+Length(aSeparator), Length(aStr)));
          end
     else begin
          aPart1 := Trim(aStr);
          aPart2 := '';
          end;
  end;

function StringToStringList(aString: string; const aSeparator: string): TStringlist;
  var
    i: integer;
  begin
  result := TStringlist.Create;
  i := Pos(aSeparator, aString);
  while i>0 do
    begin
    result.Add(Copy(aString, 1, i-1));
    aString := Copy(aString, i+Length(aSeparator), Length(aString));
    i := Pos(aSeparator, aString);
    end;

  // Does the result string contain any characters? If so, add to the list.
  // If not, only add the empty string when the result contains any entries:
  // 'a,b,' should return 3 elements, '' should return none.
  if aString <> ''
     then result.Add(aString)
     else if result.Count > 0
             then result.Add(aString)
  end;

function StringToStringListTrimmed(aString: string; const aSeparator: string): TStringlist;
  var
    i: integer;
  begin
  result := StringtoStringList(aString, aSeparator);
  for i := 0 to result.Count - 1 do
    result[i] := Trim(result[i]);
  end;

function StringToStringListWithQuotes(aString: string; const aSeparator: string): TStringlist;
  var
    lPart: string;
    lQuoteChar, lCurrentChar: Char;
  begin
  result := TStringlist.Create;

  aString := Trim(aString);
  lPart := '';
  lQuoteChar := #0;
  while Length(aString)>0 do
    begin
    lCurrentChar := aString[1];

    // Quote handling
    if (lCurrentChar=lQuoteChar) and CharInSet(lQuoteChar, ['''', '"'])
       then begin
            // Look ahead, might be a double quote
            if (Length(aString)>1) and (aString[2]=lQuoteChar)
               then begin
                    lPart := lPart + lQuoteChar;
                    Delete(aString, 1, 2);
                    end
               else begin
                    // Must be end quote
                    result.Add(lPart);
                    lPart := '';
                    lQuoteChar := #0;
                    Delete(aString, 1, 1);
                    aString := Trim(aString);

                    // Separator should follow - or - end of string
                    if Length(aString)>1
                       then begin
                            if aString[1]<>aSeparator
                               then raise Exception.Create('Invalid format in string');
                            Delete(aString, 1, 1);
                            aString := Trim(aString);
                            end;
                    end;
            end

    // Separator?
    else if (lCurrentChar=aSeparator[1]) and (lQuoteChar=#0)
       then begin
            // End of part
            result.Add(lPart);
            lPart := '';
            Delete(aString, 1, 1);
            aString := Trim(aString);
            end

    // Start of quotes?
    else if (lPart='') and (lQuoteChar=#0) and CharInSet(lCurrentChar, ['''', '"'])
       then begin
            lQuoteChar := lCurrentChar;
            Delete(aString, 1, 1);
            end

    // Normal character
    else begin
         lPart := lPart + lCurrentChar;
         Delete(aString, 1, 1);
         end;
    end;

   // Add final part
   if lPart<>''
      then result.Add(lPart);
   end;

function StringListToString(const aStrLst: TStrings; const aSeparator: string): string; overload;
   begin
   result := StringListToString(aStrLst, aSeparator, aSeparator);
   end;

function StringListToString(const aStrLst: TStrings; const aSeparator, aLastSeparator: string): string; overload;
  var
    i: integer;
    lStr: string;
    lPChar: PChar;
    lLength: integer;
    lLengthSep,
    lLengthLastSep: integer;
    lBeforeLastSepIndex: integer;
   begin
   // Get length of all strings
   lLength := 0;
   for i := 0 to aStrLst.Count - 1 do
      Inc(lLength, Length(aStrLst[i]));

   // Store separator length
   lLengthSep := Length(aSeparator);
   lLengthLastSep := Length(aLastSeparator);

   // Add length of last sep
   if aStrLst.Count > 1
      then Inc(lLength, lLengthLastSep);
   // Add length of other seps
   if aStrLst.Count > 2
      then Inc(lLength, (aStrLst.Count - 2) * lLengthSep);

   SetString(result, nil, lLength);
   lPChar := PChar(result);

   // Store index of last sep
   lBeforeLastSepIndex := (aStrLst.Count - 2);
   for i := 0 to aStrLst.Count - 1 do
      begin
      lStr := aStrLst[I];
      lLength := Length(lStr);

      // Add the string
      Move(Pointer(lStr)^, lPChar^, lLength * SizeOf(Char));
      Inc(lPChar, lLength);

      // Check if separator has to be added
      if I < lBeforeLastSepIndex
         then begin
              Move(Pointer(aSeparator)^, lPChar^, lLengthSep * SizeOf(Char));
              Inc(lPChar, lLengthSep);
              end
      // else add the last separator
      else if I < (aStrLst.Count - 1)
         then begin
              Move(Pointer(aLastSeparator)^, lPChar^, lLengthLastSep * SizeOf(Char));
              Inc(lPChar, lLengthLastSep);
              end;
      end;
   end;

function CheckTrailingDot(const aMessage: string): string;
  begin
  result := aMessage;
  if (Length(aMessage)>0) and (aMessage[Length(aMessage)]<>'.')
    then Result := Result + '.';
  end;

function StartsWithChar(aStr: string; aChar: char): boolean;
  begin
  result := false;
  if Length(aStr)=0 then exit;
  result := aStr[1] = aChar;
  end;

function ToIdentifier(const aIdentifier: string): string; overload;
  begin
  result := ToIdentifier(aIdentifier, [], '');
  end;

function ToIdentifier(const aIdentifier: string; const aAdditionalChars: TSysCharSet; const aReplaceChar: string): string; overload;
  const
    Alpha = ['A'..'Z', 'a'..'z', '_'];
    AlphaNumeric = Alpha + ['0'..'9'];
  var
    i: integer;
  begin
  result := '';
  if Length(aIdentifier) > 0
     then begin
          if not CharInSet(aIdentifier[1], Alpha)
             then result := aReplaceChar
             else result := result + aIdentifier[1];

          for i := 2 to Length(aIdentifier) do
            if not CharInSet(aIdentifier[i], AlphaNumeric) and not CharInSet(aIdentifier[i], aAdditionalChars)
               then result := result + aReplaceChar
               else result := result + aIdentifier[i];
          end;
  end;

function RemoveNameFromIdentifier(const aIdentifier: string; const aName: string): string;
  var
    lIndex: integer;
  const
    cSeparators = ['-','\','/','_','|','~',':','.'];
  begin
  result := aIdentifier;
  lIndex := Pos(Uppercase(aName), Uppercase(result));
  if lIndex > 0
     then begin
          Delete(result, lIndex, Length(aName));
          result := trim(result);
          if result='' then exit;

          if lIndex = 1
             then begin
                  // Name may now start with a separator
                  if CharInSet(result[1], cSeparators)
                     then result := Copy(result, 2, Length(result));
                  end
             else begin
                  if CharinSet(result[Length(result)], cSeparators)
                     then result := Copy(result, 1, Length(result)-1);
                  end;
          end;
  end;



// -- Dialogs

function PromptForOpenFileName(aTitle, aDefaultExt, aInitialDir, aFilter: string; var aFileName: string): boolean;
  var
    lDialog: TOpenDialog;
  begin
  lDialog := TOpenDialog.Create(nil);
  lDialog.Title := aTitle;
  lDialog.DefaultExt := aDefaultExt;
  lDialog.InitialDir := aInitialDir;
  lDialog.FileName := aFileName;
  lDialog.Filter := aFilter;
  lDialog.Options := lDialog.Options + [ofPathMustExist, ofFileMustExist];

  result := lDialog.Execute;
  if result
     then aFileName := lDialog.FileName;

  lDialog.Free;
  end;

function PromptForSaveFileName(aTitle, aDefaultExt, aInitialDir, aFilter: string; var aFileName: string): boolean;
  var
    lDialog: TSaveDialog;
  begin
  lDialog := TSaveDialog.Create(nil);
  lDialog.Title := aTitle;
  lDialog.DefaultExt := aDefaultExt;
  lDialog.InitialDir := aInitialDir;
  lDialog.FileName := aFileName;
  lDialog.Filter := aFilter;
  lDialog.Options := lDialog.Options + [ofOverwritePrompt];

  result := lDialog.Execute;
  if result
     then aFileName := lDialog.FileName;

  lDialog.Free;
  end;

function PromptForFolder(const aCaption: string; var aFolder: string): Boolean;
  var
    lDialog: TSelectDirectoryDialog;
  begin
  if aFolder <> ''
     then aFolder := RemoveTrailingBackslash(aFolder);

  lDialog := TSelectDirectoryDialog.Create(nil);
  lDialog.Title := aCaption;
//  lDialog.Options := [fdoPickFolders];
  lDialog.FileName := aFolder;
  result := lDialog.Execute;

  if result
     then aFolder := lDialog.FileName;

  lDialog.Free;
  end;

function PromptForConfirmation(const aQuestion: string): boolean; overload;
  var
    lResponse: integer;
  begin
  lResponse := MessageDlg(aQuestion, mtCustom, [mbYes,mbNo], 0);
  result := lResponse = mrYes;
  end;

function PromptForConfirmation(const aQuestion: string; const aArgs: array of const): boolean; overload;
  begin
  result := PromptForConfirmation(Format(aQuestion, aArgs));
  end;


// -- Filenames

function ConvertToFilename(aStr: string): string;
  var
    i: Integer;
  begin
  result := '';
  for i := 1 to Length(aStr) do
    begin
    if CharInSet(aStr[I], ['A'..'Z', 'a'..'z', '0'..'9', ' ', '-', '_', '.'])
       then result := result + aStr[I];
    end;
  end;

function RemoveTrailingBackslash(aStr: string): string;
  var
    l: integer;
  begin
  l := Length(aStr);
  if (l>0) and (aStr[l]='\')
     then result := Copy(aStr, 1, l-1)
     else result := aStr;
  end;


// -- Files

{$WARNINGS OFF}

// FileIterator
type
  TcFileIterator_HandleFileEvent = procedure (const aFolder, aFilename: string; aTime: TDateTime; aSize, aAttr: Integer; aObject: TObject) of object;
  TcFileIterator_HandleFolderEvent = procedure (const aPath, aFolder: string; aObject: TObject; var aNewObject: TObject; var aScanFolder: Boolean) of object;
  TcFileIterator_FolderHandling = (fhDontcare, fhFirst, fhLast);

  // Class which allows files to be searched in folders. On every found file/folder
  // a callback event can be sent. The ScanFolder routines use this class
  TcFileIterator =
    class
      protected
       fFileMask: string;
       fAttr: Integer;
       fRecursive, fStop, fSorted: Boolean;
       fFolderHandling: TcFileIterator_FolderHandling;
       fHandleFile: TcFileIterator_HandleFileEvent;
       fHandleFolder: TcFileIterator_HandleFolderEvent;
       fBaseFolder: string;

       procedure ScanFolder(aFolder: string; aObject: TObject);
     public
       constructor Create;
       procedure Execute(const aFolder: string; aObject: TObject);

       property Stop: Boolean read fStop write fStop;

       property Attr: Integer read fAttr write fAttr default faAnyFile;
       property FileMask: String read fFileMask write fFileMask;
       property Recursive: Boolean read fRecursive write fRecursive default True;
       property Sorted: Boolean read fSorted write fSorted default False;
       property FolderHandling: TcFileIterator_FolderHandling read fFolderhandling write fFolderhandling default fhDontcare;

       property HandleFile: TcFileIterator_HandleFileEvent read fHandleFile write fHandleFile;
       property HandleFolder: TcFileIterator_HandleFolderEvent read fHandleFolder write fHandleFolder;
    end;

{ TcFileIterator }

constructor TcFileIterator.Create;
   begin
   inherited Create;
   fAttr := faAnyFile;
   fFileMask := '*';
   fRecursive := True;
   fStop := False;
   fHandleFile := nil;
   fHandleFolder := nil;
   end;

procedure TcFileIterator.Execute(const aFolder: string; aObject: TObject);
   begin
   Stop := False;
   fBaseFolder := IncludeTrailingPathDelimiter(aFolder);
   ScanFolder(fBaseFolder, aObject);
   end;

type
  TcFileIterator_EntryInfo =
    class
      Kind, Size, Attr, Time: Integer;
      constructor Create(aKind, aSize, aAttr, aTime: Integer);
      end;

constructor TcFileIterator_EntryInfo.Create(aKind, aSize, aAttr, aTime: Integer);
   begin
   Kind := aKind; Size := aSize; Attr := aAttr; Time := aTime;
   end;

procedure TcFileIterator.ScanFolder(aFolder: string; aObject: TObject);
  var
    Entries: TStringlist;
    NewObject: TObject;
  const
    ekFolder   = 1;
    ekFile     = 0;
    ekDontcare = -1;

  procedure HandleEntries(aEntryKind: Integer);
    var
      T: Integer;
      lEntryInfo: TcFileIterator_EntryInfo;
      lDoScanFolder: Boolean;
    begin
    for T := 0 to Entries.Count-1 do
      begin
      lEntryInfo := TcFileIterator_EntryInfo(Entries.Objects[T]);
      if (aEntryKind=ekDontcare) or (aEntryKind=lEntryInfo.Kind)
         then begin
              if lEntryInfo.Kind=ekFolder
                 then begin
                      // Folder
                      lDoScanFolder := fRecursive;
                      if Assigned(HandleFolder)
                         then HandleFolder(aFolder+Entries[T], Entries[T],
                                 aObject, NewObject, lDoScanFolder);
                      if lDoScanFolder
                         then ScanFolder(aFolder+Entries[T], NewObject);
                      end
                 else begin
                      // File
                      if Assigned(HandleFile)
                         then HandleFile(aFolder, Entries[T],
                                 FileDateToDateTime(lEntryInfo.Time),
                                 lEntryInfo.Size, lEntryInfo.Attr, aObject);
                      end;
              end;
      end;
    end;

  var
    lError, T: Integer;
    lSearchRec: TSearchRec;
    lFolderMask: string;
  begin
  // First pass: just fetch the files and folders
  Entries := TStringlist.Create;

  aFolder := IncludeTrailingPathDelimiter(aFolder);

  // If recursive, scan folders first.. this is because of the filemask
  // Scan for *.txt searches *.txt folders only..this is not wanted. So do a first
  // scan for folders
  if Recursive
     then lFolderMask := '*.*'
     else lFolderMask := FileMask;

  lError := SysUtils.FindFirst(aFolder+lFolderMask, (Attr and not faAnyFile) or faDirectory, lSearchRec);
  while (lError=0) and not Stop do
    begin
    // Skip Volume ID
    if (lSearchRec.Attr and faVolumeID <> 0) then Continue;

    if (lSearchRec.Attr and faDirectory <> 0)
       then begin
            // Folder
            if lSearchRec.Name[1]<>'.'
               then Entries.AddObject(lSearchRec.Name,
                       TcFileIterator_EntryInfo.Create(ekFolder, 0, lSearchRec.Attr, lSearchRec.Time));
            end;
    lError := SysUtils.FindNext(lSearchRec);
    end;
  SysUtils.FindClose(lSearchRec);

  lError := SysUtils.FindFirst(aFolder+FileMask, Attr and not faDirectory, lSearchRec);
  while (lError=0) and not Stop do
    begin
    // Skip Volume ID
    if (lSearchRec.Attr and faVolumeID <> 0) then Continue;

    // File
    Entries.AddObject(lSearchRec.Name,
       TcFileIterator_EntryInfo.Create(ekFile, lSearchRec.Size, lSearchRec.Attr, lSearchRec.Time));

    lError := SysUtils.FindNext(lSearchRec);
    end;
  SysUtils.FindClose(lSearchRec);

  // Sorting?
  if Sorted then Entries.Sorted := True;

  // Handle files and folders
  case FolderHandling of
    fhDontcare: begin HandleEntries(ekDontcare); end;
    fhFirst:    begin HandleEntries(ekFolder); HandleEntries(ekFile); end;
    fhLast:     begin HandleEntries(ekFile); HandleEntries(ekFolder); end;
    end;

  // Free entries list
  for T := 0 to Entries.Count-1 do TcFileIterator_EntryInfo(Entries.Objects[T]).Free;
  Entries.Clear;
  Entries.Free;
  end;

// TcFileIterator_DirectoryInfo
type
  // Encapsulsation of the FileIterator for use with the exported procedures
  // without the need of the creation of the iterator component and the object
  // methods
  TcFileIterator_DirectoryInfo =
    class
      protected
        fFiles,
        fFolders: TStringList;
        fRecursive: Boolean;

        procedure HandleFileEvent(const aFolder, aFilename: string; aTime: TDateTime; aSize, aAttr: Integer; aObject: TObject);
        procedure HandleFolderEvent(const aPath, aFolder: string; aObject: TObject; var aNewObject: TObject; var aScanFolder: Boolean);

      public
        procedure ScanFolder(const aFolder, aFilemask: string; aFiles, aFolders: TStringList; aRecursive: Boolean);
      end;

{ TcFileIterator_DirectoryInfo }

procedure TcFileIterator_DirectoryInfo.ScanFolder(const aFolder, aFilemask: string; aFiles, aFolders: TStringList; aRecursive: Boolean);
   var
      lFI: TcFileIterator;
   begin
   fRecursive := aRecursive;

   lFI := TcFileIterator.Create;
   lFI.Recursive := fRecursive;
   if aFilemask<>''
      then lFI.FileMask := aFileMask;
   try
      fFiles := aFiles;
      fFolders := aFolders;

      if Assigned(fFiles)
         then begin
              lFI.HandleFile := HandleFileEvent;
              fFiles.Clear;
              end;
      if Assigned(fFolders)
         then begin
              lFI.HandleFolder := HandleFolderEvent;
              fFolders.Clear;
              end;

      lFI.Execute(aFolder, nil);
   finally
      lFI.Free;
      fFiles := nil;
      fFolders := nil;
      end;
   end;

procedure TcFileIterator_DirectoryInfo.HandleFileEvent(const aFolder, aFilename: string; aTime: TDateTime; aSize, aAttr: Integer; aObject: TObject);
   begin
   if fRecursive
      then fFiles.Add(aFolder + aFilename)
      else fFiles.Add(aFilename);
   end;

procedure TcFileIterator_DirectoryInfo.HandleFolderEvent(const aPath, aFolder: string; aObject: TObject; var aNewObject: TObject; var aScanFolder: Boolean);
   begin
   if fRecursive
      then fFolders.Add(aPath)
      else fFolders.Add(aFolder);
   end;

procedure ScanFolder(const aFolder: string; aFiles: TStringList; aRecursive: Boolean = False);
   var
      lDI: TcFileIterator_DirectoryInfo;
   begin
   lDI := TcFileIterator_DirectoryInfo.Create;
   try
      lDI.ScanFolder(aFolder, '', aFiles, nil, aRecursive);
   finally
      lDI.Free;
      end;
   end;

procedure ScanFolder(const aFolder, aFileMask: string; aFiles: TStringList; aRecursive: Boolean = False);
   var
      lDI: TcFileIterator_DirectoryInfo;
   begin
   lDI := TcFileIterator_DirectoryInfo.Create;
   try
      lDI.ScanFolder(aFolder, aFilemask, aFiles, nil, aRecursive);
   finally
      lDI.Free;
      end;
   end;

procedure ScanFolder(const aFolder, aFileMask: string; aFiles, aFolders: TStringList; aRecursive: Boolean = False);
   var
      lDI: TcFileIterator_DirectoryInfo;
   begin
   lDI := TcFileIterator_DirectoryInfo.Create;
   try
      lDI.ScanFolder(aFolder, aFilemask, aFiles, aFolders, aRecursive);
   finally
      lDI.Free;
      end;
   end;

{$WARNINGS ON}


// -- Value conversion

function BooleanToYesNo(const aBoolean: Boolean): string;
   begin
   if aBoolean then Result := 'Yes' else Result := 'No';
   end;

// Convert a string to boolean value
function TrueFalseToBoolean(const aStr: string): boolean;
  var
    lStr: string;
  begin
  lStr := Uppercase(aStr);

  if (lStr='1') or (lStr='T') or (lStr='TRUE') or (lStr='Y') or (lStr='YES')
    then result := true

  else if (lStr='0') or (lStr='F') or (lStr='FALSE') or (lStr='N') or (lStr='NO')
    then result := false

  else raise Exception.CreateFmt('Cannot convert %s to boolean', [aStr]);
  end;

function BooleanToTrueFalse(const aBoolean: boolean): string;
  begin
  if aBoolean then Result := 'true' else Result := 'false';
  end;


// -- Misc

function ControlPressed: boolean;
  begin
  result := ssCtrl in GetKeyShiftState;
  end;

function AltPressed: boolean;
  begin
  result := ssAlt in GetKeyShiftState;
  end;

function ShiftPressed: boolean;
  begin
  result := ssShift in GetKeyShiftState;
  end;

function GetApplicationVersionStr: string;
 var
   lFileVerInfo: TFileVersionInfo;
 begin
 lFileVerInfo := TFileVersionInfo.Create(nil);
 try
   lFileVerInfo.ReadFileInfo;
   Result := lFileVerInfo.VersionStrings.Values['FileVersion'];
 finally
   lFileVerInfo.Free;
   end;
 end;


// -- Streams

procedure WriteStringToStream(aStream: TStream; const aStr: string; aEncoding: TEncoding);
  var
    lLen: Integer;
    lBuffer, lPreamble: TBytes;
  begin
  lLen := Length(aStr);
  if lLen = 0
     then aStream.Write(lLen, SizeOf(lLen))
     else begin
          if aEncoding = nil
             then aEncoding := TEncoding.ANSI;

          lBuffer := aEncoding.GetBytes(UnicodeString(aStr));
          lPreamble := aEncoding.GetPreamble;

          lLen := Length(lPreamble) + Length(lBuffer);
          aStream.Write(lLen, SizeOf(lLen));

          if Length(lPreamble) > 0
             then aStream.WriteBuffer(lPreamble, Length(lPreamble));

          aStream.WriteBuffer(lBuffer, Length(lBuffer));
          end;
  end;

function ReadStringFromStream(aStream: TStream; aMaxLenght: integer = 0): string;
  var
    lLen: integer = 0;
    lStart: integer;
    lBuffer: TBytes = [];
    lEncoding: TEncoding;
  begin
  if aStream.Read(lLen, SizeOf(lLen)) <> SizeOf(lLen)
     then EUsr('Error reading stream');

  if (aMaxLenght> 0) and (lLen > aMaxLenght)
     then EDev('string size overflow');

  if lLen = 0
     then result := ''
     else begin
          SetLength(lBuffer, lLen);
          if aStream.Read(lBuffer, lLen) <> lLen
             then EUsr('Error reading stream');
          lEncoding := nil;
          lStart := TEncoding.GetBufferEncoding(lBuffer, lEncoding);
          result := string(lEncoding.GetString(lBuffer, lStart, lLen - lStart));
          end;
  end;

end.
