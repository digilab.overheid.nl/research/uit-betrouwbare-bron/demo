program NotificationHub;

{$MODE Delphi}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Interfaces,Forms, indylaz,
  NotificationHub.Form in 'NotificationHub.Form.pas' {fNotifications},
  common.Utils in '..\common\common.Utils.pas',
  common.ErrorHandling in '..\common\common.ErrorHandling.pas';

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Scaled := True;
  Application.Initialize;
  Application.CreateForm(TfNotifications, fNotifications);
  Application.Run;
end.
