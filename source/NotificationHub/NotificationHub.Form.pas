// -----------------------------------------------------------------------------
//
// NotificationHub.Form
//
// -----------------------------------------------------------------------------
//
// Simple form to display incomming Cloud Events
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------
unit NotificationHub.Form;

interface

uses
  SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls,
  IdCustomHTTPServer, IdHTTPServer, IdContext, IdHTTP,
  ExtCtrls, ActnList, SynEditHighlighter, SynEdit;


const
  cNotificationHubUrl = '127.0.0.1';
  cNotificationHubPort = 4300;

type

  { TfNotifications }

  TfNotifications = class(TForm)
    HttpServer: TIdHTTPServer;
    pnlMain: TPanel;
    pnlOutput: TPanel;
    pnlOutputHeader: TPanel;
    lOutput: TLabel;
    pnlSession: TPanel;
    pnlOutputMemo: TPanel;
    pnlOutputControls: TPanel;
    pnlInputControlsRight: TPanel;
    btnClearInput: TButton;
    ActionList: TActionList;
    actClearOutput: TAction;
    mOutput: TSynEdit;
    procedure FormCreate(Sender: TObject);
    procedure actClearOutputExecute(Sender: TObject);
  private
    procedure HandleHttpServerCommand(aContext: TIdContext; aRequestInfo: TIdHTTPRequestInfo; aResponseInfo: TIdHTTPResponseInfo);
    procedure HandleHttpServerOtherCommand(aContext: TIdContext; aRequestInfo: TIdHTTPRequestInfo; aResponseInfo: TIdHTTPResponseInfo);
    procedure AddCorsHeadersToResponse(aResponseInfo: TIdHTTPResponseInfo);
    procedure ClearOutput;

  public

  end;

var
  fNotifications: TfNotifications;

implementation

{$R *.lfm}

uses
  fpjson, common.Json, common.Utils;

procedure TfNotifications.FormCreate(Sender: TObject);
  begin
  with mOutput do
    begin
    with Gutter do
      begin
      Visible := true;
      Color := $F5F5F5; //clWebWhiteSmoke
      Font.Color := clGray;
      end;
    end;

  HttpServer.Active := false;
  HttpServer.Bindings.Clear;
  with HttpServer.Bindings.Add do
    begin
    ip := cNotificationHubUrl;
    port := cNotificationHubPort;
    end;

  HttpServer.OnCommandGet := HandleHttpServerCommand;
  HttpServer.OnCommandOther := HandleHttpServerOtherCommand;
  HttpServer.Active := True;
  end;


procedure TfNotifications.HandleHttpServerCommand(aContext: TIdContext; aRequestInfo: TIdHTTPRequestInfo; aResponseInfo: TIdHTTPResponseInfo);
  var
    lEndpoint: string;
    lStringStream: TStringStream;
    lContent: string;
    lJsonValue: TJsonValue;
    lJson: TJsonObject;
    lLines: TStringlist;
    i: integer;
  begin
  try
    // Ignore standaard html requests
    if aRequestInfo.Document = '/favicon.ico' then exit;

    // Assume bad request
    aResponseInfo.ResponseNo := 400;

    // Endpoint
    lEndpoint := Copy(aRequestInfo.Document, 2, Length(aRequestInfo.Document));
    if CompareText(lEndpoint, 'notification')<>0
       then begin
            aResponseInfo.ContentText := 'Invalid endpoint.';
            exit;
            end;

    // Command should be POST
    if aRequestInfo.CommandType <> hcPOST
       then begin
            aResponseInfo.ContentText := 'POST expected.';
            exit;
            end;

    // There should be no parameters
    if aRequestInfo.Params.Count > 0
       then begin
            aResponseInfo.ContentText := 'No parameters allowed.';
            exit;
            end;

    // Content should be Json
    if CompareText(aRequestInfo.ContentType, 'application/json')<>0
       then begin
            aResponseInfo.ContentText := 'Content must be json.';
            exit;
            end;

    // Parse Json
    try
      lStringStream := TStringStream.Create;
      lStringStream.LoadFromStream(aRequestInfo.PostStream);
      lContent := lStringStream.DataString;
      lStringStream.Free;

      lJsonValue := TJsonObject.ParseJSONValue(lContent);
      if Assigned(lJsonValue) and (lJsonValue is TJsonObject)
         then lJson := lJsonValue as TJsonObject;
    except
      on E:Exception do
        begin
        aResponseInfo.ContentText := 'Invalid json.';
        exit;
        end;
      end;

    // Display Json
    mOutput.Lines.Add(Format('# CloudEvent received at "%s"', [DateTimeToStr(Now)]));
    lLines := StringToStringlist(lJson.Format(2), #13#10);
    try
      for i := 0 to lLines.Count-1 do
        mOutput.Lines.Add(lLines[i]);
    finally
      lLines.Free;
      end;
    mOutput.Lines.Add('');

    // All ok
    aResponseInfo.ResponseNo := 200;

  finally
    // Pass response
    AddCORSHeadersToResponse(aResponseInfo);
    end;
  end;

procedure TfNotifications.HandleHttpServerOtherCommand(aContext: TIdContext; aRequestInfo: TIdHTTPRequestInfo; aResponseInfo: TIdHTTPResponseInfo);
  begin
  AddCORSHeadersToResponse(aResponseInfo);
  end;

procedure TfNotifications.ClearOutput;
  begin
  mOutput.Clear;
  end;

procedure TfNotifications.actClearOutputExecute(Sender: TObject);
  begin
  ClearOutput;
  end;

procedure TfNotifications.AddCorsHeadersToResponse(aResponseInfo: TIdHTTPResponseInfo);
  begin
  aResponseInfo.CustomHeaders.AddValue('Access-Control-Allow-Origin', '*');
  aResponseInfo.CustomHeaders.AddValue('Access-Control-Allow-Headers', '*');
  end;

end.
