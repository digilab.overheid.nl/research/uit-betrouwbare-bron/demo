// -----------------------------------------------------------------------------
//
// diagram.Model
//
// -----------------------------------------------------------------------------
//
// Visualisation of models with RelationTypes. Used in ace.Forms.Model.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV
//
// -----------------------------------------------------------------------------
//
// Code for editing and modules has been disabled.
// The following comment conventions have been used to mark such code:
//?! Editing -> Code related to editing the model (from the diagrammer)
//?! Inter-Class Rules -> Support for graphical constraints between objects
//
// -----------------------------------------------------------------------------

unit diagram.Model;

{$MODE Delphi}

interface

uses
  LCLType, Classes, SysUtils,
  Graphics, Controls, Menus,
  common.SimpleLists, common.ErrorHandling,
  ace.Core.Engine, diagram.Core, Types,
  common.Json, fpjson;

const
  cCurrentClassDiagramVersion = 1; // 2024: Major refactor, reset to 1

  cColumnSeparatorStr = 'XX';

  cMaxModuleColors = 14;
  cModuleColors: array[0..cMaxModuleColors-1] of TColor =
    ($AAAAFF, $AAFFAA, $FFAAAA, $AAAAAA, $AAFFFF, $FFFFAA, $FFAAFF, $7777FF,
     $77FF77, $FF7777, $777777, $77FFFF, $FFFF77, $FF77FF);

var
  gModeler_Model: string = 'Model';
  gModeler_SimpleClass_Name: string = 'label type';
  gModeler_SimpleClass_PluralName: string = 'labeltypes';
  gModeler_ComplexClass_Name: string = 'claim type';
  gModeler_ComplexClass_PluralName: string = 'claim types';
  gModeler_Attribute_Name: string = 'role';
  gModeler_Attribute_PluralName: string = 'roles';

type
  TcdClassStyle = class;
  TcdClassList = class;
  TcdBaseClass = class;
  TcdSimpleClass = class;
  TcdComplexClass = class;
  TcdSimpleClassTitle = class;
  TcdComplexClassTitle = class;
  TcdComplexClassConnectorFrame = class;
  TcdAttribute = class;
  TcdAttributeList = class;
  TcdRule = class;
  TcdRuleList = class;
  TcdReferenceLine = class;
  TcdSubtypeLine = class;

  TcdAttributeOrientation = (aoVertical, aoHorizontal);
  TcdTitleAlignment = (ctaLeft, ctaLeftCenter, ctaCenter, ctaRight, ctaRightCenter);
  TcdReferenceStyle = (rsClassToClass, rsAttributeToClass);
  TcdReferenceNamePosition = (rnpCenter, rnpAttribute);
  TcdVerticalPosition = (vpTop, vpBottom);
  TcdHorizontalPosition = (hpLeft, hpRight);
  TcdConnectorFrameStyle = (cfsRoundRect, cfsEllipse);
  TcdRuleType = (ctIdentification, ctUnicity, ctSortIndex, ctTotality, ctDisplay, ctValue, ctCustom);
  TcdSimpleClassShape = (scsRect, scsRoundRect, scsCircle);

  TcdDiagramKind = (dkUnknown, dkAtomicTypes, dkClasses);
  TcdClassType = (ctNone, ctLabelType, ctClaimType, ctAnnotationType, ctClass);


  // __________________________________________________________________________
  //
  // TcdClassDiagram
  // __________________________________________________________________________
  //
  TcdClassDiagram =
    class (TcdDiagram)
      private
        fClassDiagramVersion: Integer;

        fEngine: TcEngine;
        fKind: TcdDiagramKind;
        fModel: Integer;

        // "Root" objects
        fClasses: TcdClassList;
        fReferenceLines: TcList;
        fSubtypeLines: TcList;
        fInterClassRules: TcList;
        fModules: TcIntegerlist;

        fOnNewClass: TNotifyEvent;

        procedure PopulateSimpleClassPopupMenu;
        procedure PopulateComplexClassPopupMenu;
        procedure PopulateAttributePopupMenu;

        function  GetSimpleClassFromPopupMenuItem (aSender: TObject): TcdSimpleClass;
        function  GetComplexClassFromPopupMenuItem (aSender: TObject): TcdComplexClass;

        procedure Handle_SimpleClass_Remove(aSender: TObject);
        //procedure Handle_SimpleClass_Edit(aSender: TObject);
        //procedure Handle_SimpleClass_Delete(aSender: TObject);

        //procedure Handle_ComplexClass_New(Sender: TObject);

        procedure Handle_ComplexClass_Remove(aSender: TObject);
        //procedure Handle_ComplexClass_Edit(aSender: TObject);
        //procedure Handle_ComplexClass_Delete(aSender: TObject);
        //procedure Handle_ComplexClass_DefaultForm(aSender: TObject);

        //procedure Handle_Attribute_AddIdentificationRule(aSender: TObject);
        //procedure Handle_Attribute_DeleteIdentificationRule(aSender: TObject);
        //procedure Handle_Attribute_AddUniqueRule(aSender: TObject);
        //procedure Handle_Attribute_DeleteUniqueRule(aSender: TObject);
        //procedure Handle_Attribute_AddSortRule(aSender: TObject);
        //procedure Handle_Attribute_DeleteSortRule(aSender: TObject);
        //procedure Handle_Attribute_AddDisplayRule(aSender: TObject);
        //procedure Handle_Attribute_DeleteDisplayRule(aSender: TObject);

        //procedure AddRuleFromSelectedAttributes(aType: string);
        //procedure DeleteRuleFromSelectedAttributes(aType: string);

        procedure CacheModules;

      protected
        procedure DoSimpleClassPopupMenu(aX, aY: Integer; aSimpleClass: TcdSimpleClass);
        procedure DoComplexClassPopupMenu(aX, aY: Integer; aComplexClass: TcdComplexClass);
        procedure DoAttributePopupMenu(aX, aY: Integer; aAttribute: TcdAttribute);

        procedure PopulateScrollBoxPopupMenu; override;

      public
        constructor Create(aOwner: TComponent; aStylename: string); override;
        destructor Destroy; override;

        procedure Init(aEngine: TcEngine; aKind: TcdDiagramKind); reintroduce;

        procedure LoadFromJson(aJsonRoot: TJsonObject; aForcedStyle: TcdStyle = nil); override;
        procedure SaveToJson(aJsonRoot: TJsonObject); override;

        procedure RefreshFromData; override;

        procedure EditSimpleClass(aClassSyncID: Integer);
        procedure DeleteSimpleClass(aClassSyncID: Integer);
        function NewSimpleClass: Integer;

        procedure EditComplexClass(aClassSyncID: Integer);
        procedure DeleteComplexClass(aClassSyncID: Integer);
        function NewComplexClass: Integer;

        procedure ComplexClass_DefaultForm(aClassSyncID: Integer);

        // Global routines for polymorphic rules
        function Rule_CreateAndLoadFromObject(aComplexClass: TcdComplexClass;
          aRelationType: TcRelationType; aRuleType: TcdRuleType; aRoles: TcElementList<TcRole>; aRole: TcRole = nil): TcdRule;

        function GetModuleColor(aModuleID: Integer): TColor;

        property ClassDiagramVersion: Integer read fClassDiagramVersion;

        property Engine: TcEngine read fEngine;
        property Kind: TcdDiagramKind read fKind;
        property Model: Integer read fModel write fModel;

        property Classes: TcdClassList read fClasses;
        property ReferenceLines: TcList read fReferenceLines;
        property SubtypeLines: TcList read fSubtypeLines;
        property InterClassRules: TcList read fInterClassRules;

        property OnNewClass: TNotifyEvent read fOnNewClass write fOnNewClass;
      end;


  // __________________________________________________________________________
  //
  // TcdClassStyle
  // __________________________________________________________________________
  //
  TcdClassStyle =
    class (TcdStyle)
      private
        // -- Main style
        fSimpleClass_Name, fSimpleClass_PluralName,
        fComplexClass_Name, fComplexClass_PluralName,
        fAttribute_Name, fAttribute_PluralName: string;

        fDisplayReferencingAttributes: Boolean;
        fAttributeOrientation: TcdAttributeOrientation;
        fReferenceStyle: TcdReferenceStyle;
        fReferenceNamePosition: TcdReferenceNamePosition;

        fReferenceLineType: TcdLineType;
        fReferenceLineStyle: TcdLineStyle;
        fReferenceLineFromObjectConType: TcdObjectConType;
        fReferenceLineToObjectConType: TcdObjectConType;

        fSubtypeLineType: TcdLineType;
        fSubtypeLineStyle: TcdLineStyle;
        fSubtypeLineFromObjectConType: TcdObjectConType;
        fSubtypeLineToObjectConType: TcdObjectConType;

        fAutoSizeSimpleClass: Boolean;
        fSimpleClassShape: TcdSimpleClassShape;
        fiSimpleClassWidth, feSimpleClassWidth: Integer;
        fiSimpleClassHeight, feSimpleClassHeight: Integer;

        fConnectorFrame: Boolean;
        fConnectorFrameStyle: TcdConnectorFrameStyle;

        fDisplayAttributeTypeNames: Boolean;
        fAttributeTypeName_StartSymbol: string;
        fAttributeTypeName_EndSymbol: string;

        fDisplayORWIndicator: Boolean;

        fORWIndicators: TStringlist;
        fORWIndicatorDisplayStrs: TStringlist;
        fORWIndicatorVerticalPosition: TcdVerticalPosition;

        fDerivableIndicatorPosition: TcdHorizontalPosition;
        fDerivableIndicator, fInitialDerivableIndicator: string;

        fDisplayIndexes, fDisplayTotality: Boolean;
        fIndex_IdentificationStr: string;
        fIndex_UniqueStr: string;
        fIndex_SortStr: string;
        fTotalityStr: string;
        fDisplayRuleStr: string;

        fDisplayUnicityArrow: Boolean;

        fDoubleIdentityLine: Boolean;


        // -- Customizable visual appreance within a style
        fClassFontName: string;
        fiClassFontHeight, feClassFontHeight: Integer;
        fClassFontStyle: TFontStyles;
        fClassFontColor: TColor;

        fSimpleClassPenColor: TColor;
        fSimpleClassFillColor: TColor;
        fSimpleClassPenColor_Selected: TColor;
        fSimpleClassFillColor_Selected: TColor;

        fSimpleClassTitleFontName: string;
        fiSimpleClassTitleFontHeight, feSimpleClassTitleFontHeight: Integer;
        fSimpleClassTitleFontStyle: TFontStyles;
        fSimpleClassTitleFontColor: TColor;

        fiSimpleClassTitleHorizontalMargin, feSimpleClassTitleHorizontalMargin: Integer;
        fiSimpleClassTitleVerticalMargin, feSimpleClassTitleVerticalMargin: Integer;

        fComplexClassPenColor: TColor;
        fComplexClassFillColor: TColor;
        fComplexClassPenColor_Selected: TColor;
        fComplexClassFillColor_Selected: TColor;

        fComplexClassTitleFontName: string;
        fiComplexClassTitleFontHeight, feComplexClassTitleFontHeight: Integer;
        fComplexClassTitleFontStyle: TFontStyles;
        fComplexClassTitleFontColor: TColor;

        fComplexClassTitleAlignment: TcdTitleAlignment;
        fiComplexClassTitleVerticalMargin, feComplexClassTitleVerticalMargin: Integer;

        fiComplexClassTitleFrameHorizontalMargin, feComplexClassTitleFrameHorizontalMargin: Integer;
        fiComplexClassTitleFrameVerticalMargin, feComplexClassTitleFrameVerticalMargin: Integer;
        fComplexClassTitleFrameFillColor: TColor;
        fComplexClassTitleFrameFillColor_Selected: TColor;

        fiConnectorFrameMargin, feConnectorFrameMargin: Integer;

        fAttributeFontColor_Selected: TColor;
        fAttributeFillColor_Selected: TColor;
        fiAttributeFrameHorizontalMargin, feAttributeFrameHorizontalMargin: Integer;
        fiAttributeFrameVerticalMargin, feAttributeFrameVerticalMargin: Integer;

        fReferencingAttributeFontStyle: TFontStyles;

        fRuleFontName: string;
        fiRuleFontHeight, feRuleFontHeight: Integer;
        fRuleFontStyle: TFontStyles;
        fRuleFontColor: TColor;

        fiUnicityRuleMargin, feUnicityRuleMargin: Integer;
        fiUnicityArrowLength, feUnicityArrowLength: Integer;
        fiUnicityArrowHeightDIV2, feUnicityArrowHeightDIV2: Integer;

        fiDoubleIdentityLineMargin, feDoubleIdentityLineMargin: Integer;

        // Derived properties
        fiAttributeFrameWidth: Integer;
        fiAttributeFrameHeight: Integer;
        fiAttributeFrameVerticalMargin_Name: Integer;
        fiAttributeFrameVerticalMargin_ORW: Integer;
        fiAttributeFrameVerticalMargin_Type: Integer;
        fiORWIndicatorRightMargin: Integer;

      public
        constructor Create(aName: string); override;
        destructor Destroy; override;

        procedure DeriveProperties (aDiagram: TcdDiagram); override;

        // -- Utility

        procedure DeleteDisplayStrsForORWIndicators;
        procedure AddDisplayStrForORWIndicator(aORWIndicator, aDisplayStr: string);
        function GetDisplayStrForORWIndicator(aORWIndicator: string): string;


        // -- Main Style

        property SimpleClass_Name: string read fSimpleClass_Name write fSimpleClass_Name;
        property SimpleClass_PluralName: string read fSimpleClass_PluralName write fSimpleClass_PluralName;
        property ComplexClass_Name: string read fComplexClass_Name write fComplexClass_Name;
        property ComplexClass_PluralName: string read fComplexClass_PluralName write fComplexClass_PluralName;
        property Attribute_Name: string read fAttribute_Name write fAttribute_Name;
        property Attribute_PluralName: string read fAttribute_PluralName write fAttribute_PluralName;

        // Should referencing attributes be displayed (ERD, UML-IM, ORM) or not (UML).
        property DisplayReferencingAttributes: Boolean read fDisplayReferencingAttributes;

        // Should attributes be displayed in a vertical list (UML, ERD) or horizontal (ORM, Record)
        property AttributeOrientation: TcdAttributeOrientation read fAttributeOrientation;

        // How are references between classes drawn?
        // From class to class (UML), from attribute to class (ORM, ERD) or from attribute to attribute (Record)
        property ReferenceStyle: TcdReferenceStyle read fReferenceStyle;

        // Relative position of a reference name (relative to line)
        property ReferenceNamePosition: TcdReferenceNamePosition read fReferenceNamePosition;

        // Definition of referencing (attribute) lines
        property ReferenceLineType: TcdLineType read fReferenceLineType;
        property ReferenceLineStyle: TcdLineStyle read fReferenceLineStyle;
        property ReferenceLineFromObjectConType: TcdObjectConType read fReferenceLineFromObjectConType;
        property ReferenceLineToObjectConType: TcdObjectConType read fReferenceLineToObjectConType;

        // Definition of subtype lines
        property SubtypeLineType: TcdLineType read fSubtypeLineType;
        property SubtypeLineStyle: TcdLineStyle read fSubtypeLineStyle;
        property SubtypeLineFromObjectConType: TcdObjectConType read fSubtypeLineFromObjectConType;
        property SubtypeLineToObjectConType: TcdObjectConType read fSubtypeLineToObjectConType;

        property AutoSizeSimpleClass: Boolean read fAutoSizeSimpleClass;
        property SimpleClassShape: TcdSimpleClassShape read fSimpleClassShape;
        property SimpleClassWidth: Integer read fiSimpleClassWidth;
        property SimpleClassHeight: Integer read fiSimpleClassHeight;

        property ConnectorFrame: Boolean read fConnectorFrame;
        property ConnectorFrameStyle: TcdConnectorFrameStyle read fConnectorFrameStyle;

        property DisplayAttributeTypeNames: Boolean read fDisplayAttributeTypeNames;
        property AttributeTypeName_StartSymbol: string read fAttributeTypeName_StartSymbol;
        property AttributeTypeName_EndSymbol: string read fAttributeTypeName_EndSymbol;

        property DisplayORWIndicator: Boolean read fDisplayORWIndicator;
        property ORWIndicatorVerticalPosition: TcdVerticalPosition read fORWIndicatorVerticalPosition;

        property DerivableIndicatorPosition: TcdHorizontalPosition read fDerivableIndicatorPosition;
        property DerivableIndicator: string read fDerivableIndicator;
        property InitialDerivableIndicator: string read fInitialDerivableIndicator;

        property DisplayIndexes: Boolean read fDisplayIndexes;
        property DisplayTotality: Boolean read fDisplayTotality;
        property Index_IdentificationStr: string read fIndex_IdentificationStr;
        property Index_UniqueStr: string read fIndex_UniqueStr;
        property Index_SortStr: string read fIndex_SortStr;
        property TotalityStr: string read fTotalityStr;
        property DisplayRuleStr: string read fDisplayRuleStr;

        property DisplayUnicityArrow: Boolean read fDisplayUnicityArrow;

        property DoubleIdentityLine: Boolean read fDoubleIdentityLine;


        // -- Customizable visual appearance within a style

        property ClassFontName: string read fClassFontName;
        property ClassFontHeight: Integer read fiClassFontHeight;
        property ClassFontStyle: TFontStyles read fClassFontStyle;
        property ClassFontColor: TColor read fClassFontColor;

        property SimpleClassPenColor: TColor read fSimpleClassPenColor;
        property SimpleClassFillColor: TColor read fSimpleClassFillColor;
        property SimpleClassPenColor_Selected: TColor read fSimpleClassPenColor_Selected;
        property SimpleClassFillColor_Selected: TColor read fSimpleClassFillColor_Selected;

        property SimpleClassTitleFontName: string read fSimpleClassTitleFontName;
        property SimpleClassTitleFontHeight: Integer read fiSimpleClassTitleFontHeight;
        property SimpleClassTitleFontStyle: TFontStyles read fSimpleClassTitleFontStyle;
        property SimpleClassTitleFontColor: TColor read fSimpleClassTitleFontColor;

        property SimpleClassTitleHorizontalMargin: Integer read fiSimpleClassTitleHorizontalMargin;
        property SimpleClassTitleVerticalMargin: Integer read fiSimpleClassTitleVerticalMargin;

        property ComplexClassPenColor: TColor read fComplexClassPenColor;
        property ComplexClassFillColor: TColor read fComplexClassFillColor;
        property ComplexClassPenColor_Selected: TColor read fComplexClassPenColor_Selected;
        property ComplexClassFillColor_Selected: TColor read fComplexClassFillColor_Selected;

        property ComplexClassTitleFontName: string read fComplexClassTitleFontName;
        property ComplexClassTitleFontHeight: Integer read fiComplexClassTitleFontHeight;
        property ComplexClassTitleFontStyle: TFontStyles read fComplexClassTitleFontStyle;
        property ComplexClassTitleFontColor: TColor read fComplexClassTitleFontColor;

        property ComplexClassTitleAlignment: TcdTitleAlignment read fComplexClassTitleAlignment;
        property ComplexClassTitleVerticalMargin: Integer read fiComplexClassTitleVerticalMargin;

        property ComplexClassTitleFrameHorizontalMargin: Integer read fiComplexClassTitleFrameHorizontalMargin;
        property ComplexClassTitleFrameVerticalMargin: Integer read fiComplexClassTitleFrameVerticalMargin;
        property ComplexClassTitleFrameFillColor: TColor read fComplexClassTitleFrameFillColor;
        property ComplexClassTitleFrameFillColor_Selected: TColor read fComplexClassTitleFrameFillColor_Selected;

        property ConnectorFrameMargin: Integer read fiConnectorFrameMargin;

        property AttributeFontColor_Selected: TColor read fAttributeFontColor_Selected;
        property AttributeFillColor_Selected: TColor read fAttributeFillColor_Selected;

        property AttributeFrameHorizontalMargin: Integer read fiAttributeFrameHorizontalMargin;
        property AttributeFrameVerticalMargin: Integer read fiAttributeFrameVerticalMargin;

        property ReferencingAttributeFontStyle: TFontStyles read fReferencingAttributeFontStyle;

        property RuleFontName: string read fRuleFontName;
        property RuleFontHeight: Integer read fiRuleFontHeight;
        property RuleFontStyle: TFontStyles read fRuleFontStyle;
        property RuleFontColor: TColor read fRuleFontColor;

        property UnicityRuleMargin: Integer read fiUnicityRuleMargin;
        property UnicityArrowLength: Integer read fiUnicityArrowLength;
        property UnicityArrowHeightDIV2: Integer read fiUnicityArrowHeightDIV2;

        property DoubleIdentityLineMargin: Integer read fiDoubleIdentityLineMargin;

        // Automatically derived
        property AttributeFrameWidth: Integer read fiAttributeFrameWidth;
        property AttributeFrameHeight: Integer read fiAttributeFrameHeight;
        property AttributeFrameVerticalMargin_Name: Integer read fiAttributeFrameVerticalMargin_Name;
        property AttributeFrameVerticalMargin_ORW: Integer read fiAttributeFrameVerticalMargin_ORW;
        property AttributeFrameVerticalMargin_Type: Integer read fiAttributeFrameVerticalMargin_Type;
        property ORWIndicatorRightMargin: Integer read fiORWIndicatorRightMargin;
      end;


  // __________________________________________________________________________
  //
  // TcdClassStyle_UML
  // __________________________________________________________________________
  //
  TcdClassStyle_UML =
    class (TcdClassStyle)
      public
        constructor Create(aName: string); override;
      end;


  // __________________________________________________________________________
  //
  // TcdClassStyle_UMLIM
  // __________________________________________________________________________
  //
  TcdClassStyle_UMLIM =
    class (TcdClassStyle)
      public
        constructor Create(aName: string); override;
      end;


  // __________________________________________________________________________
  //
  // TcdClassStyle_IM
  // __________________________________________________________________________
  //
  TcdClassStyle_IM =
    class (TcdClassStyle)
      public
        constructor Create(aName: string); override;
      end;

  // __________________________________________________________________________
  //
  // TcdClassStyle_ERD
  // __________________________________________________________________________
  //
  TcdClassStyle_ERD =
    class (TcdClassStyle)
      public
        constructor Create(aName: string); override;
      end;

  // __________________________________________________________________________
  //
  // TcdClassStyle_Record
  // __________________________________________________________________________
  //
  TcdClassStyle_Record =
    class (TcdClassStyle)
      public
        constructor Create(aName: string); override;
      end;


  // __________________________________________________________________________
  //
  // TcdClassStyle_ORM
  // __________________________________________________________________________
  //
  TcdClassStyle_ORM =
    class (TcdClassStyle)
      public
        constructor Create(aName: string); override;
      end;


  // __________________________________________________________________________
  //
  // TcdClassStyle_ORM2
  // __________________________________________________________________________
  //
  TcdClassStyle_ORM2 =
    class (TcdClassStyle)
      public
        constructor Create(aName: string); override;
      end;


  // __________________________________________________________________________
  //
  // TcdClassStyle_ORM2_Compact
  // __________________________________________________________________________
  //
  TcdClassStyle_ORM2_Compact =
    class (TcdClassStyle)
      public
        constructor Create(aName: string); override;
      end;


  // __________________________________________________________________________
  //
  // TcdClassList
  // __________________________________________________________________________
  //
  TcdClassList =
    class (TcSortedList)
        function Compare (const aObjectA, aObjectB: TObject): Integer; override;
        function Compare (const aObject: TObject; const aKeyValue: string): Integer; override;
        function Find(aSyncID: Integer): TcdBaseClass;
        function FindByName(aName: string): TcdBaseClass;
      end;


  // __________________________________________________________________________
  //
  // TcdBaseClass
  // __________________________________________________________________________
  //
  TcdBaseClass =
    class (TcdDragObject)
      private
      protected
        // Meta data
        fSyncID: Integer;
        fType: TcdClassType;
        fName: string;
        fModule: Integer;

        procedure SetSyncID(aValue: Integer);

      public
        procedure LoadFromData(aSyncID: Integer); reintroduce; virtual; abstract;
        procedure Link; virtual; abstract;

        // Data
        property SyncID: Integer read fSyncID write SetSyncID;
        property Type_: TcdClassType read fType write fType;
        property Name: string read fName write fName;
        property Module: Integer read fModule write fModule;
      end;


  // __________________________________________________________________________
  //
  // TcdSimpleClass
  // __________________________________________________________________________
  //
  TcdSimpleClass =
    class (TcdBaseClass)
      private
        // Drawing structures
        fdTitle: TcdSimpleClassTitle;

        // Points for round rect
        fPoints: array [0..24] of TPoint;

      protected
        procedure MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer); override;
        procedure DblClick; override;

        procedure ResetBounds_Internal; override;
        procedure ResetBounds_Children; override;

        procedure CorrectZOrder; override;

        function GetConnectionPoint(aLine: TcdLine; aPoint: TPoint): TPoint; override;

        function GetDisplayName: string;

      public
        constructor Create(aDiagram: TcdDiagram); reintroduce;
        destructor Destroy; override;

        procedure InitChildren; override;

        procedure LoadFromJson(aJsonObject: TJsonObject); override;
        procedure SaveToJson(aJsonObject: TJsonObject); override;

        procedure LoadFromData(aSyncID: Integer); override;
        procedure LoadFromObject(aLabelType: TcLabelType);

        procedure Link; override;

        procedure PaintOnCanvas(aCanvas: TCanvas); override;

        // Data
        property SyncID: Integer read fSyncID write SetSyncID;
        property Name: string read fName write fName;

        property DisplayName: string read GetDisplayName;

        // Child controls
        property dTitle: TcdSimpleClassTitle read fdTitle;
      end;


  // __________________________________________________________________________
  //
  // TcdComplexClass
  // __________________________________________________________________________
  //
  TcdComplexClass =
    class (TcdBaseClass)
      private
        // Meta data
        fDerivable: string;

        fAttributes: TcdAttributeList;

        // Drawing structures
        fdTitle: TcdComplexClassTitle;
        fdConnectorFrame: TcdComplexClassConnectorFrame;

        // Derived information
        fNrAttributes,
        fTitleFrameHeight,
        fAttributeFrameHeight,
        fXPos_AttributeTypeName,
        fXPos_RuleStr: Integer;

        fIntraRules: TcdRuleList;

        fConnectionPoints_Top,
        fConnectionPoints_Bottom,
        fConnectionPoints_TitleFrameSide: TcList;

        fLowestRuleTop: Integer;

      protected
        procedure MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer); override;
        procedure DblClick; override;

        procedure ResetBounds_Internal; override;
        procedure ResetBounds_Children; override;

        procedure CorrectZOrder; override;

        function GetConnectionPoint(aLine: TcdLine; aPoint: TPoint): TPoint; override;

        function GetDisplayName: string;

        function IsStartForSubtypeLine: Boolean;

      public
        constructor Create(aDiagram: TcdDiagram; aType: TcdClassType); reintroduce;
        destructor Destroy; override;

        procedure DeleteConnectionPoints; override;

        procedure InitChildren; override;

        procedure Changed; override;

        procedure LoadFromJson(aJsonObject: TJsonObject); override;
        procedure SaveToJson(aJsonObject: TJsonObject); override;

        procedure LoadFromData(aSyncID: Integer); override;
        procedure LoadFromObject(aRelationType: TcRelationType);

        procedure Link; override;

        function ConnectToMaster(aMaster: TcdBaseClass; aDetailAttribute: TcdAttribute): TcdReferenceLine;
        function ConnectToDetail(aDetail: TcdComplexClass; aDetailAttribute: TcdAttribute): TcdReferenceLine;
        function ConnectToSupertype(aSupertype: TcdComplexClass): TcdSubtypeLine;
        function ConnectToSubtype(aSubtype: TcdComplexClass): TcdSubtypeLine;

        function ReconnectToMaster(aOldLine: TcdLine; aMaster: TcdBaseClass; aDetailAttribute: TcdAttribute): TcdReferenceLine;
        function ReconnectToDetail(aOldLine: TcdLine; aDetail: TcdComplexClass; aDetailAttribute: TcdAttribute): TcdReferenceLine;
        function ReconnectToSupertype(aOldLine: TcdLine; aSupertype: TcdComplexClass): TcdSubtypeLine;
        function ReconnectToSubtype(aOldLine: TcdLine; aSubtype: TcdComplexClass): TcdSubtypeLine;

        function ConnectedToMaster(aMaster: TcdBaseClass; aDetailAttributeSyncID: Integer): TcdReferenceLine;
        function ConnectedToDetail(aDetail: TcdComplexClass; aDetailAttributeSyncID: Integer): TcdReferenceLine;
        function ConnectedToSupertype(aSupertype: TcdComplexClass): TcdSubtypeLine;
        function ConnectedToSubtype(aSubtype: TcdComplexClass): TcdSubtypeLine;

        function GetObjectSide(aLine: TcdLine; aPoint: TPoint): TcdObjectSide; override;

        procedure PaintOnCanvas(aCanvas: TCanvas); override;

        // Data
        property Derivable: string read fDerivable write fDerivable;
        property Attributes: TcdAttributeList read fAttributes;
        property IntraRules: TcdRuleList read fIntraRules;

        property DisplayName: string read GetDisplayName;

        // Derived data
        property LowestRuleTop: Integer read fLowestRuleTop write fLowestRuleTop;

        // Child controls
        property dTitle: TcdComplexClassTitle read fdTitle;
        property dConnectorFrame: TcdComplexClassConnectorFrame read fdConnectorFrame;

        // Connectionpoint lists
        property ConnectionPoints_Top: TcList read fConnectionPoints_Top;
        property ConnectionPoints_Bottom: TcList read fConnectionPoints_Bottom;
        property ConnectionPoints_TitleFrameSide: TcList read fConnectionPoints_TitleFrameSide;
      end;


  // __________________________________________________________________________
  //
  // TcdSimpleClassTitle
  // __________________________________________________________________________
  //
  // Description:
  // Displays a title text near but separate from the simple class
  // __________________________________________________________________________
  //
  TcdSimpleClassTitle =
    class (TcdObject)
      private
        fSimpleClass: TcdSimpleClass;

      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aSimpleClass: TcdSimpleClass); reintroduce;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;

        property SimpleClass: TcdSimpleClass read fSimpleClass;
      end;


  // __________________________________________________________________________
  //
  // TcdComplexClassTitle
  // __________________________________________________________________________
  //
  // Description:
  // Displays a title text near but separate from the complex class
  // __________________________________________________________________________
  //
  TcdComplexClassTitle =
    class (TcdObject)
      private
        fComplexClass: TcdComplexClass;

      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aComplexClass: TcdComplexClass); reintroduce;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;

        property ComplexClass: TcdComplexClass read fComplexClass;
      end;


  // __________________________________________________________________________
  //
  // TcdComplexClassConnectorFrame
  // __________________________________________________________________________
  //
  // Description:
  // Displays a frame around the complexclass. This frame can be used to
  // connect to.
  // __________________________________________________________________________
  //
  TcdComplexClassConnectorFrame =
    class (TcdConObject)
      private
        fComplexClass: TcdComplexClass;
        fPoints: array [0..24] of TPoint;

      protected
        procedure ResetBounds_Internal; override;
        procedure SetSelected (aValue: Boolean); override;

        function GetConnectionPoint(aLine: TcdLine; aPoint: TPoint): TPoint; override;

      public
        constructor Create(aDiagram: TcdDiagram; aComplexClass: TcdComplexClass); reintroduce;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;

        property ComplexClass: TcdComplexClass read fComplexClass;
      end;


  // __________________________________________________________________________
  //
  // TcdAttribute
  // __________________________________________________________________________
  //
  TcdAttribute =
    class (TcdConObject)
      private
        // Meta data
        fComplexClass: TcdComplexClass;

        fSyncID: Integer;
        fName: string;
        fORWIndicator: string;
        fRefersTo: Integer;
        fTypeName: string;
        fType: string;
        fDerivable: string;
        fModule: Integer;

        // Derived information
        fRelativeNr: Integer;
        fRuleStr: string;
        fIntraRules: TcdRuleList;

      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aComplexClass: TcdComplexClass); reintroduce;
        destructor Destroy; override;

        procedure MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer); override;
        procedure MouseUp(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer); override;
        procedure MouseMove(aShiftState: TShiftState; aX, aY: Integer); override;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;

        function DeriveWidth: Integer;
        function DeriveRuleStr: string;
        function GetDisplayName: string;

        function GetORWIndicatorAsCardinality: Integer;

        function HasSingleAttributeTotalityRule: Boolean;

        property ComplexClass: TcdComplexClass read fComplexClass;

        property SyncID: Integer read fSyncID write fSyncID;
        property Name: string read fName write fName;
        property ORWIndicator: string read fORWIndicator write fORWIndicator;
        property RefersTo: Integer read fRefersTo write fRefersTo;
        property TypeName: string read fTypeName write fTypeName;
        property Type_: string read fType write fType;
        property Derivable: string read fDerivable write fDerivable;
        property Module: Integer read fModule write fModule;

        property DisplayName: string read GetDisplayName;
        property RelativeNr: Integer read fRelativeNr write fRelativenr;
        property IntraRules: TcdRuleList read fIntraRules;
      end;


  // __________________________________________________________________________
  //
  // TcdAttributeList
  // __________________________________________________________________________
  //
  TcdAttributeList =
    class (TcList)
      private
        fDiagram: TcdClassDiagram;

      public
        constructor Create(aDiagram: TcdClassDiagram); reintroduce;

        function First: TcdAttribute;
        function Last: TcdAttribute;

        function GetAttributeByRelativenr (aRelativenr: Integer): TcdAttribute;
        function GetAttributeBySyncID (aSyncID: Integer): TcdAttribute;
        function GetAttributeByName (aName: string): TcdAttribute;

        function TotalWidth: Integer;

        property Diagram: TcdClassDiagram read fDiagram;
      end;


  // __________________________________________________________________________
  //
  // TcdRule
  // __________________________________________________________________________
  //
  TcdRule =
    class (TcdObject)
      private
        fSyncID: Integer;
        fClass: TcdBaseClass;
        fName: string;
        fType: TcdRuleType;
        fRelativeID: Integer;
        fAttributes: TcdAttributeList;
        fAttributeIDs: TcIntegerList;

      protected
        procedure SetSyncID(aValue: Integer);
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aClass: TcdBaseClass; aType: TcdRuleType); reintroduce;

        destructor Destroy; override;

        procedure LoadFromObject(aRelationType: TcRelationType; aType: TcdRuleType;
          aRoles: TcElementList<TcRole>; aRole: TcRole = nil); reintroduce; overload;

        procedure PaintOnCanvas(aCanvas: TCanvas); override;

        property SyncID: Integer read fSyncID write SetSyncID;
        property Class_: TcdBaseClass read fClass write fClass;
        property Name: string read fName write fName;
        property Type_: TcdRuleType read fType write fType;
        property RelativeID: Integer read fRelativeID write fRelativeID;
        property Attributes: TcdAttributeList read fAttributes;
        property AttributeIDs: TcIntegerList read fAttributeIDs;
      end;


  // __________________________________________________________________________
  //
  // TcdRuleList
  // __________________________________________________________________________
  //
  TcdRuleList =
    class (TcSortedList)
      private
        fIdentityCount,
        fUnicityCount: Integer;

      public
        function Compare (const aObjectA, aObjectB: TObject): Integer; override;
        function Compare (const aObject: TObject; const aKeyValue: string): Integer; override;
        function Find(aSyncID: Integer): TcdRule;

        // Derive counts and relative rule numbers
        procedure Derive;

        property IdentityCount: Integer read fIdentityCount;
        property UnicityCount: Integer read fUnicityCount;
      end;


  // __________________________________________________________________________
  //
  // TcdIndex
  // __________________________________________________________________________
  //
  TcdIndex =
    class (TcdRule)
      end;


  // __________________________________________________________________________
  //
  // TcdTotality
  // __________________________________________________________________________
  //
  TcdTotality =
    class (TcdRule)
      end;


  // __________________________________________________________________________
  //
  // TcdDisplayRule
  // __________________________________________________________________________
  //
  TcdDisplayRule =
    class (TcdRule)
      end;


  // __________________________________________________________________________
  //
  // TcdReferenceLine
  // __________________________________________________________________________
  //
  TcdReferenceLine =
    class (TcdLine)
      public
        constructor Create (aDiagram: TcdDiagram; aFromObject, aToObject: TcdConObject; aConData: TcdLineConData); reintroduce;
        destructor Destroy; override;

        procedure LoadFromJson(aJsonObject: TJsonObject); override;

        procedure SetAttributeName;
      end;


  // __________________________________________________________________________
  //
  // TcdReferenceLineData
  // __________________________________________________________________________
  //
  TcdReferenceLineData =
    class (TcdLineConData)
      private
        fReferenceLine: TcdReferenceLine;
        fAttributeSyncID: Integer;
      public
        procedure Init(aLine: TcdReferenceLine);

        procedure LoadFromData(aDetailAttribute: TcdAttribute);
        procedure LoadFromJson(aJsonObject: TJsonObject); override;
        procedure SaveToJson(aJsonObject: TJsonObject); override;

        property ReferenceLine: TcdReferenceLine read fReferenceLine;
        property AttributeSyncID: Integer read fAttributeSyncID;
      end;


  // __________________________________________________________________________
  //
  // TcdSubtypeLine
  // __________________________________________________________________________
  //
  TcdSubtypeLine =
    class (TcdLine)
      public
        constructor Create (aDiagram: TcdDiagram; aFromObject, aToObject: TcdConObject; aConData: TcdLineConData); reintroduce;
      end;

function Diagrammer_RoundTo(aNumber, aRoundTo: Integer): Integer;

implementation

uses
  Math, Dialogs,
  common.Utils;

function Diagrammer_RoundTo(aNumber, aRoundTo: Integer): Integer;
  begin
  if aRoundTo=0
     then result := aNumber
     else begin
          result := (aNumber div aRoundTo) * aRoundTo;
          if aNumber mod aRoundTo > 0
             then result := result + aRoundTo;
          end;
  end;

function RuleTypeToStr (aType: TcdRuleType): string;
  begin
  Result := '';
  case aType of
    ctIdentification: Result := 'I';
    ctUnicity:        Result := 'U';
    ctSortIndex:      Result := 'S';
    ctTotality:       Result := 'T';
    ctDisplay:        Result := 'D';
    ctValue:          Result := 'V';
    ctCustom:         Result := 'C';
    else EInt('ConstaintTypeToStr', 'Unknown rule type');
    end;
  end;

function StrToRuleType (aString: string): TcdRuleType;
  begin
  Result := ctIdentification; // Anti compiler warning
  case aString[1] of
    'I': Result := ctIdentification;
    'U': Result := ctUnicity;
    'S': Result := ctSortIndex;
    'T': Result := ctTotality;
    'D': Result := ctDisplay;
    'V': Result := ctValue;
    'C': Result := ctCustom;
    else EInt('StrToConstaintType', 'Unknown rule type');
    end;
  end;

function ClassTypeToStr(aClassType: TcdClassType): string;
  begin
  case aClassType of
    ctLabelType: result := 'LabelType';
    ctClaimType: result := 'ClaimType';
    ctAnnotationType: result := 'AnnotationType';
    ctClass: result := 'Class';
    else result := 'none';
    end;
  end;

function StrToClassType(aStr: string): TcdClassType;
  begin
  if CompareText(aStr, 'LabelType')=0
     then result := ctLabelType
  else if CompareText(aStr, 'ClaimType')=0
     then result := ctClaimType
  else if CompareText(aStr, 'AnnotationType')=0
     then result := ctAnnotationType
  else if CompareText(aStr, 'Class')=0
     then result := ctClass
     else result := ctNone;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdClassDiagram
//
procedure TcdClassDiagram.PopulateScrollBoxPopupMenu;
  begin
  inherited;
  if (Selection.Count = 0)
     then begin
          //?! Editing
          // CreateMenuItem(ObjectPopupMenu, 'New &Class', 0,  Handle_ComplexClass_New);
          end;
  end;

procedure TcdClassDiagram.PopulateSimpleClassPopupMenu;
  begin
  ObjectPopupMenu.Items.Clear;

  //?! Editing
  //CreateMenuItem(ObjectPopupMenu, '&Edit', 0, Handle_SimpleClass_Edit);
  CreateMenuItem(ObjectPopupMenu, '&Remove from diagram', 0, Handle_SimpleClass_Remove);
  //CreateMenuItem(ObjectPopupMenu, '&Delete from model', 0, Handle_SimpleClass_Delete);
  end;

procedure TcdClassDiagram.PopulateComplexClassPopupMenu;
  begin
  ObjectPopupMenu.Items.Clear;

  //?! Editing
  //CreateMenuItem(ObjectPopupMenu, '&Edit', 0, Handle_ComplexClass_Edit);
  CreateMenuItem(ObjectPopupMenu, '&Remove from diagram', 0, Handle_ComplexClass_Remove);
  //CreateMenuItem(ObjectPopupMenu, '&Delete from model', 0, Handle_ComplexClass_Delete);
  //CreateMenuItem(ObjectPopupMenu, '-', 0, NIL);
  //CreateMenuItem(ObjectPopupMenu, 'Show (default) form', 0, Handle_ComplexClass_DefaultForm);
  end;

procedure TcdClassDiagram.PopulateAttributePopupMenu;
  var
    lCanAddRule,
    lCanDeleteRule: Boolean;
    lcdComplexClass: TcdComplexClass;
    I: Integer;
    //lHasIdentification,
    //lHasUnique,
    //lHasDisplay,
    //lHasSort: Boolean;
    //lcdAttribute: TcdAttribute;
  begin
  ObjectPopupMenu.Items.Clear;

  // Add complex class menu if a single class was selected
  if (Selection.Count=1) and (Selection[0] is TcdComplexClass)
     then begin
          //?! Editing
          //CreateMenuItem(ObjectPopupMenu, '&Edit', 0, Handle_ComplexClass_Edit);
          CreateMenuItem(ObjectPopupMenu, '&Remove from diagram', 0, Handle_ComplexClass_Remove);
          //CreateMenuItem(ObjectPopupMenu, '&Delete from model', 0, Handle_ComplexClass_Delete);
          //CreateMenuItem(ObjectPopupMenu, '-', 0, NIL);
          //if not ModelerSketchmode
          //   then CreateMenuItem(ObjectPopupMenu, 'Show default form', 0, Handle_ComplexClass_DefaultForm);
          end;

  // Is there a selection, and every selected item is an attribute and
  // all attributes belong to the same class
  lcdComplexClass := nil;
  lCanAddRule := False;
  if (Selection.Count > 0)
     then begin
          if Selection[0] is TcdAttribute
             then begin
                  lCanAddRule := True;
                  lcdComplexClass := TcdAttribute(Selection[0]).ComplexClass;
                  if lCanAddRule
                     then for I := 1 to Selection.Count - 1 do
                            lCanAddRule := lCanAddRule and (Selection[I] is TcdAttribute) and (TcdAttribute(Selection[I]).ComplexClass = lcdComplexClass);
                  end;
          end;

  lCanDeleteRule := (Selection.Count = 1) and (Selection[0] is TcdAttribute);

  if lCanAddRule or lCanDeleteRule
     then begin
          Assert(Assigned(lcdComplexClass)); // Is assigned when lCanAddRule because true, and lCanAddRule is tested

          //?! Editing
          // Check existing rules
          // lcdAttribute := TcdAttribute(Selection[0]);
          // lHasIdentification := False;
          // lHasUnique := False;
          // lHasDisplay := False;
          // lHasSort := False;
          //
          // WITH CT.CreateQry(
          //    'SELECT R.Type_, RA.Attribute_ ' +
          //    'FROM ' +
          //    '   Mat$Rule R ' +
          //    '   LEFT OUTER JOIN Mat$RuleAttribute RA ON RA.Rule_ = R.ID ' +
          //    'WHERE ' +
          //    '   R.Class_ = :Class ') DO
          //    BEGIN
          //    ParamByName('Class').AsInteger := lComplexClass.SyncID;
          //    Execute;
          //
          //    WHILE not EOF DO
          //       BEGIN
          //       IF (FieldByName('Type_').AsString = 'I') and (FieldByName('Attribute_').AsInteger = lAttribute.SyncID)
          //          then lHasIdentification := True
          //       ELSE IF (FieldByName('Type_').AsString = 'U') and (FieldByName('Attribute_').AsInteger = lAttribute.SyncID)
          //          then lHasUnique := True
          //       ELSE IF (FieldByName('Type_').AsString = 'D') and (FieldByName('Attribute_').AsInteger = lAttribute.SyncID)
          //          then lHasDisplay := True
          //       ELSE IF (FieldByName('Type_').AsString = 'S') and (FieldByName('Attribute_').AsInteger = lAttribute.SyncID)
          //          then lHasSort := True;
          //
          //       Next;
          //       END;
          //    Free;
          //    END;
          //
          // if lHasIdentification and lCanDeleteRule
          //    then CreateMenuItem(ObjectPopupMenu, 'Delete Identification rule', 0, Handle_Attribute_DeleteIdentificationRule);
          // if lCanAddRule and not lHasIdentification
          //    then CreateMenuItem(ObjectPopupMenu, 'New Identification rule', 0, Handle_Attribute_AddIdentificationRule);
          //
          // if lHasUnique and lCanDeleteRule
          //    then CreateMenuItem(ObjectPopupMenu, 'Delete Unique rule', 0, Handle_Attribute_DeleteUniqueRule);
          // if lCanAddRule //and not lHasUnique
          //    then CreateMenuItem(ObjectPopupMenu, 'New Unique rule', 0, Handle_Attribute_AddUniqueRule);
          //
          // if lHasSort and lCanDeleteRule
          //    then CreateMenuItem(ObjectPopupMenu, 'Delete Sort rule', 0, Handle_Attribute_DeleteSortRule);
          //
          // // Can have multiple sort rules
          // if lCanAddRule
          //    then CreateMenuItem(ObjectPopupMenu, 'New Sort rule', 0, Handle_Attribute_AddSortRule);
          //
          // if lHasDisplay and lCanDeleteRule
          //    then CreateMenuItem(ObjectPopupMenu, 'Delete Display rule', 0, Handle_Attribute_DeleteDisplayRule);
          // if lCanAddRule and not lHasDisplay
          //    then CreateMenuItem(ObjectPopupMenu, 'New Display rule', 0, Handle_Attribute_AddDisplayRule);
          end;
  end;

function TcdClassDiagram.GetSimpleClassFromPopupMenuItem (aSender: TObject): TcdSimpleClass;
  var
    lObject: TObject;
  begin
  Result := nil;

  lObject := GetPopupComponentFromMenuItem(aSender);
  if lObject is TcdSimpleClass
     then Result := TcdSimpleClass(lObject)
     else EInt('TcdClassDiagram.GetSimpleClassFromPopupMenuItem', 'Unexpected sender');
  end;

function TcdClassDiagram.GetComplexClassFromPopupMenuItem (aSender: TObject): TcdComplexClass;
  var
    lObject: TObject;
  begin
  Result := nil;

  lObject := GetPopupComponentFromMenuItem(aSender);
  if lObject is TcdAttribute
     then Result := TcdAttribute(lObject).ComplexClass
  else if lObject is TcdComplexClass
     then Result := TcdComplexClass(lObject)
  else EInt('TcdClassDiagram.GetComplexClassFromPopupMenuItem', 'Unexpected sender');
  end;

//?! Editing
// procedure TcdClassDiagram.Handle_SimpleClass_Edit(aSender: TObject);
//   var
//     lcdSimpleClass: TcdSimpleClass;
//   begin
//   lcdSimpleClass := GetSimpleClassFromPopupMenuItem(aSender);
//   EditSimpleClass(lcdSimpleClass.SyncID);
//   end;

procedure TcdClassDiagram.Handle_SimpleClass_Remove(aSender: TObject);
  var
    lcdSimpleClass: TcdSimpleClass;
  begin
  lcdSimpleClass := GetSimpleClassFromPopupMenuItem(aSender);
  FreeAndNil(lcdSimpleClass);

  Scrollbox.Invalidate;
  end;

//?! Editing
// procedure TcdClassDiagram.Handle_SimpleClass_Delete(aSender: TObject);
//   var
//     lcdSimpleClass: TcdSimpleClass;
//   begin
//   if PromptForConfirmation('Are you sure you want to delete the %s?', [gModeler_SimpleClass_Name])
//      then begin
//           lcdSimpleClass := GetSimpleClassFromPopupMenuItem(aSender);
//           DeleteSimpleClass(lcdSimpleClass.SyncID);
//           end;
//   end;
//
// procedure TcdClassDiagram.Handle_ComplexClass_Edit(aSender: TObject);
//   var
//     lcdComplexClass: TcdComplexClass;
//   begin
//   lcdComplexClass := GetComplexClassFromPopupMenuItem(aSender);
//   EditComplexClass(lcdComplexClass.SyncID);
//   end;
//
// procedure TcdClassDiagram.Handle_ComplexClass_New(Sender: TObject);
//   var
//     lID: Integer;
//     lcdComplexClass: TcdComplexClass;
//     lPosition: TPoint;
//   begin
//   lID := NewComplexClass;
//   if lID > 0
//      then begin
//           lPosition := GetMenuPopupPosition(Sender);
//
//           lcdComplexClass := TcdComplexClass.Create(Self);
//
//           lcdComplexClass.LoadFromData(lID {=SyncID});
//           Add(lcdComplexClass, lPosition.X, lPosition.Y);
//           lcdComplexClass.Link;
//
//           Add(lcdComplexClass, lPosition.X, lPosition.Y);
//           end;
//   end;

procedure TcdClassDiagram.Handle_ComplexClass_Remove(aSender: TObject);
  var
    lcdComplexClass: TcdComplexClass;
  begin
  lcdComplexClass := GetComplexClassFromPopupMenuItem(aSender);
  FreeAndNil(lcdComplexClass);

  Scrollbox.Invalidate;
  end;

//?! Editing
// procedure TcdClassDiagram.Handle_ComplexClass_Delete(aSender: TObject);
//   var
//     lcdComplexClass: TcdComplexClass;
//   begin
//   if PromptForConfirmation('Are you sure you want to delete the %s?', [gModeler_ComplexClass_Name])
//      then begin
//           lcdComplexClass := GetComplexClassFromPopupMenuItem(aSender);
//           DeleteComplexClass(lcdComplexClass.SyncID);
//           end;
//   end;
//
// procedure TcdClassDiagram.Handle_ComplexClass_DefaultForm(aSender: TObject);
//   var
//     lcdComplexClass: TcdComplexClass;
//   begin
//   lcdComplexClass := GetComplexClassFromPopupMenuItem(aSender);
//   ComplexClass_DefaultForm(lcdComplexClass.SyncID);
//   end;
//
// procedure TcdClassDiagram.Handle_Attribute_AddIdentificationRule(aSender: TObject);
//   begin
//   AddRuleFromSelectedAttributes('I');
//   end;
//
// procedure TcdClassDiagram.Handle_Attribute_DeleteIdentificationRule(aSender: TObject);
//   begin
//   DeleteRuleFromSelectedAttributes('I');
//   end;
//
// procedure TcdClassDiagram.Handle_Attribute_AddUniqueRule(aSender: TObject);
//   begin
//   AddRuleFromSelectedAttributes('U');
//   end;
//
// procedure TcdClassDiagram.Handle_Attribute_DeleteUniqueRule(aSender: TObject);
//   begin
//   DeleteRuleFromSelectedAttributes('U');
//   end;
//
// procedure TcdClassDiagram.Handle_Attribute_AddSortRule(aSender: TObject);
//   begin
//   AddRuleFromSelectedAttributes('S');
//   end;
//
// procedure TcdClassDiagram.Handle_Attribute_DeleteSortRule(aSender: TObject);
//   begin
//   DeleteRuleFromSelectedAttributes('S');
//   end;
//
// procedure TcdClassDiagram.Handle_Attribute_AddDisplayRule(aSender: TObject);
//   begin
//   AddRuleFromSelectedAttributes('D');
//   end;
//
// procedure TcdClassDiagram.Handle_Attribute_DeleteDisplayRule(aSender: TObject);
//   begin
//   DeleteRuleFromSelectedAttributes('D');
//   end;
//
// procedure TcdClassDiagram.AddRuleFromSelectedAttributes(aType: string);
//   begin
//   ...
//   end;
//
// procedure TcdClassDiagram.DeleteRuleFromSelectedAttributes(aType: string);
//   begin
//   ...
//   end;

procedure TcdClassDiagram.CacheModules;
  begin
  fModules.Clear;
  end;

procedure TcdClassDiagram.DoSimpleClassPopupMenu(aX, aY: Integer; aSimpleClass: TcdSimpleClass);
  begin
  PopulateSimpleClassPopupMenu;
  DoPopupMenu(ObjectPopupMenu, aX, aY, aSimpleClass);
  end;

procedure TcdClassDiagram.DoComplexClassPopupMenu(aX, aY: Integer; aComplexClass: TcdComplexClass);
  begin
  PopulateComplexClassPopupMenu;
  DoPopupMenu(ObjectPopupMenu, aX, aY, aComplexClass);
  end;

procedure TcdClassDiagram.DoAttributePopupMenu(aX, aY: Integer; aAttribute: TcdAttribute);
  begin
  PopulateAttributePopupMenu;
  DoPopupMenu(ObjectPopupMenu, aX, aY, aAttribute);
  end;

constructor TcdClassDiagram.Create(aOwner: TComponent; aStylename: string);
  begin
  BeginChange;
  try
    inherited Create(aOwner, aStylename);

    Type_ := 'INF';
    fClasses := TcdClassList.Create;
    fReferenceLines := TcList.Create;
    fSubtypeLines := TcList.Create;
    fInterClassRules := TcList.Create;
    fModules := TcIntegerList.Create;

  finally
    EndChange;
    end;
  end;

destructor TcdClassDiagram.Destroy;
  begin
  // All members are components and will be destroyed by their owner

  Classes.RemoveAll;
  FreeAndNil(fClasses);

  ReferenceLines.RemoveAll;
  FreeAndNil(fReferenceLines);

  SubtypeLines.RemoveAll;
  FreeAndNil(fSubtypeLines);

  InterClassRules.RemoveAll;
  FreeAndNil(fInterClassRules);

  FreeAndNil(fModules);

  inherited;
  end;

procedure TcdClassDiagram.Init(aEngine: TcEngine; aKind: TcdDiagramKind);
  begin
  inherited Init;
  fEngine := aEngine;
  fKind := aKind;
  CacheModules;
  end;

procedure TcdClassDiagram.LoadFromJson(aJsonRoot: TJsonObject; aForcedStyle: TcdStyle = nil);
  var
    i: Integer;
    lSimpleClass: TcdSimpleClass;
    lComplexClass: TcdComplexClass;
    lFromClass, lToClass: TcdBaseClass;
    lReferenceLineData: TcdReferenceLineData;
    lReferenceLine: TcdReferenceLine;
    lSubtypeLine: TcdSubtypeLine;
    lJsonItems: TJsonArray;
    lJsonItem: TJsonObject;
    lJsonValue: TJsonValue;
    lStr: string;
    lInt: integer;
    lClassType: TcdClassType;
  begin
  BeginChange;
  try
    inherited LoadFromJson(aJsonRoot, aForcedStyle);

    // -- Version
    if not aJsonRoot.TryGetValue('classDiagramVersion', lInt) then InvalidJson_xExpected('classDiagramVersion');
    fClassDiagramVersion := lInt;

    // -- Classes
    if not aJsonRoot.TryGetValue('objects', lJsonItems) then InvalidJson_xExpected('objects');
    for i := 0 to lJsonItems.Count-1 do
      begin
      lJsonValue := lJsonItems[i];
      if not (lJsonValue is TJsonObject) then InvalidJson_xExpected('object');
      lJsonItem := lJsonValue as TJsonObject;

      if not lJsonItem.TryGetValue('type', lStr) then InvalidJson_xExpected('type');
      lClassType := StrToClassType(lStr);

      case lClassType of
        ctLabelType:
          begin
          lSimpleClass := TcdSimpleClass.Create(Self);
          lSimpleClass.LoadFromJson(lJsonItem);
          end;

        ctClaimType, ctAnnotationType, ctClass:
          begin
          lComplexClass := TcdComplexClass.Create(Self, lClassType);
          lComplexClass.LoadFromJson(lJsonItem);
          end;

        else InvalidJson(Format('Unexpected type: %s', [lStr]));
        end;
      end;

    // -- Reference lines
    if not aJsonRoot.TryGetValue('referenceLines', lJsonItems) then InvalidJson_xExpected('referenceLines');
    for i := 0 to lJsonItems.Count-1 do
      begin
      lJsonValue := lJsonItems[i];
      if not (lJsonValue is TJsonObject) then InvalidJson_xExpected('referenceLine');
      lJsonItem := lJsonValue as TJsonObject;

      if not lJsonItem.TryGetValue('from', lStr) then InvalidJson_xExpected('from');
      lFromClass := Classes.FindByName(lStr);
      if not Assigned(lFromClass) then continue;

      if not lJsonItem.TryGetValue('to', lStr) then InvalidJson_xExpected('to');
      lToClass := Classes.FindByName(lStr);
      if not Assigned(lToClass) then continue;

      lReferenceLineData := TcdReferenceLineData.Create;
      lReferenceLine := TcdReferenceLine.Create(Self, lFromClass, lToClass, lReferenceLineData);

      lReferenceLineData.Init(lReferenceLine);
      lReferenceLineData.LoadFromJson(lJsonItem);

      lReferenceLine.LoadFromJson(lJsonItem);
      lFromClass.AddLine(lReferenceLine);
      lToClass.AddLine(lReferenceLine);

      lReferenceLine.BringToFront;
      end;

    // -- Subtype lines
    if not aJsonRoot.TryGetValue('subtypeLines', lJsonItems) then InvalidJson_xExpected('subtypeLines');
    for i := 0 to lJsonItems.Count-1 do
      begin
      lJsonValue := lJsonItems[i];
      if not (lJsonValue is TJsonObject) then InvalidJson_xExpected('subtypeLine');
      lJsonItem := lJsonValue as TJsonObject;

      if not lJsonItem.TryGetValue('from', lStr) then InvalidJson_xExpected('from');
      lFromClass := Classes.FindByName(lStr);
      if not Assigned(lFromClass) then continue;

      if not lJsonItem.TryGetValue('to', lStr) then InvalidJson_xExpected('to');
      lToClass := Classes.FindByName(lStr);
      if not Assigned(lToClass) then continue;

      lSubtypeLine := TcdSubtypeLine.Create(Self, lFromClass, lToClass, nil);
      lSubtypeLine.LoadFromJson(lJsonItem);
      lFromClass.AddLine(lSubtypeLine);
      lToClass.AddLine(lSubtypeLine);
      lSubtypeLine.BringToFront;
      end;

    //?! Inter-Class Rules
    // aStream.Read(lCount, SizeOf(lCount));
    // for T := 1 to lCount do
    //   begin
    //   aStream.Read(lRuleType, SizeOf(lRuleType));
    //   lRule := nil;
    //   case lRuleType of
    //
    //     ctIdentification, ctUnicity, ctSortIndex:
    //        lRule := TcdIndex.Create(Self, nil, lRuleType);
    //
    //     ctTotality:
    //        lRule := TcdTotality.Create(Self, nil, lRuleType);
    //
    //     ctDisplay:
    //        lRule := TcdDisplayRule.Create(Self, nil, lRuleType);
    //
    //     else EInt('TcdClassDiagram.LoadFromStream', 'Unknown rule type');
    //     end;
    //   if Assigned(lRule)
    //      then lRule.LoadFromStream(aStream);
    //   end;

    // -- Check diagram with repository
    RefreshFromData;

    // No selected objects after loading
    Selection.Clear;

  finally
    EndChange;
    end;
  end;

procedure TcdClassDiagram.SaveToJson(aJsonRoot: TJsonObject);
  var
    i: Integer;
    lReferenceLine: TcdReferenceLine;
    lSubtypeLine: TcdSubtypeLine;
    lClass: TcdBaseClass;
    lJsonItems: TJsonArray;
    lJsonItem: TJsonObject;
    lStr: string;
  begin
  inherited SaveToJson(aJsonRoot);

  // -- Version
  fClassDiagramVersion := cCurrentClassDiagramVersion; // Upgrade to latest version
  aJsonRoot.AddPair('classDiagramVersion', TJsonIntegerNumber.Create(fClassDiagramVersion));

  // -- Classes
  lJsonItems := TJsonArray.Create;
  aJsonRoot.AddPair('objects', lJsonItems);
  for i := 0 to Classes.Count-1 do
    begin
    lJsonItem := TJsonObject.Create;
    lClass := TcdBaseClass(Classes[i]);
    lStr := ClassTypeToStr(lClass.Type_);
    lJsonItem.AddPair('type', lStr);
    lClass.SaveToJson(lJsonItem);
    lJsonItems.Add(lJsonItem);
    end;

  // -- Reference lines
  lJsonItems := TJsonArray.Create;
  aJsonRoot.AddPair('referenceLines', lJsonItems);
  for i := 0 to ReferenceLines.Count-1 do
    begin
    lJsonItem := TJsonObject.Create;
    lReferenceLine := TcdReferenceLine(ReferenceLines[i]);

    lStr := TcdBaseClass(lReferenceLine.FromObject).Name;
    lJsonItem.AddPair('from', lStr);

    lStr := TcdBaseClass(lReferenceLine.ToObject).Name;
    lJsonItem.AddPair('to', lStr);

    lReferenceLine.ConData.SaveToJson(lJsonItem);
    lReferenceLine.SaveToJson(lJsonItem);
    lJsonItems.Add(lJsonItem);
    end;

  // -- Subtype lines
  lJsonItems := TJsonArray.Create;
  aJsonRoot.AddPair('subtypeLines', lJsonItems);
  for i := 0 to SubtypeLines.Count-1 do
    begin
    lJsonItem := TJsonObject.Create;
    lSubtypeLine := TcdSubtypeLine(SubtypeLines[i]);

    lStr := TcdBaseClass(lSubtypeLine.FromObject).Name;
    lJsonItem.AddPair('from', lStr);

    lStr := TcdBaseClass(lSubtypeLine.ToObject).Name;
    lJsonItem.AddPair('to', lStr);

    // lSubtypeLine.ConData.SaveToJson(lJsonItem);
    lSubtypeLine.SaveToJson(lJsonItem);
    lJsonItems.Add(lJsonItem);
    end;

  //?! Inter-Class Rules
  // lCount := InterClassRules.Count;
  // aStream.Write(lCount, SizeOf(lCount));
  // for T := 0 to InterClassRules.Count-1 do
  //    TcdRule(InterClassRules[T]).SaveToStream(aStream);
  end;

procedure TcdClassDiagram.RefreshFromData;
  var
     i: Integer;
     lBaseClass: TcdBaseClass;
     lClassList: TcdClassList;
     lElement: TcElement;
     lLabelType: TcLabelType;
     lRelationType: TcRelationType;
  begin
  // -- Classes --

  // Create an ordered list of classes
  lClassList := TcdClassList.Create;
  lClassList.Assign(Classes);

  // Check the list
  for i := 0 to lClassList.Count-1 do
    begin
    lBaseClass := TcdBaseClass(lClassList[i]);

    // Check if this class is present in the Engine
    lElement := Engine.Elements.Find(lBaseClass.SyncID);
    if Assigned(lElement)
       then begin
            // Still present in the engine
            if lBaseClass is TcdSimpleClass
               then begin
                    // Check Element
                    if not (lElement is TcLabelType)
                       then EInt('TcdClassDiagram.RefreshFromData', 'Element should have been a LabelType');
                    lLabelType := lElement as TcLabelType;

                    // Reload data
                    TcdSimpleClass(lBaseClass).LoadFromObject(lLabelType);

                    // Relink the class to other diagram elements
                    TcdSimpleClass(lBaseClass).Link;
                    end
               else begin
                    // Check Element
                    if not (lElement is TcRelationType)
                       then EInt('TcdClassDiagram.RefreshFromData', 'Element should have been a RelationType. Type %s', [lElement.ClassName]);
                    lRelationType := lElement as TcRelationType;

                    // Reload data
                    TcdComplexClass(lBaseClass).LoadFromObject(lRelationType);

                    // Relink the class to other diagram elements
                    TcdComplexClass(lBaseClass).Link;
                    end;
            end
       else begin
            // No longer present in the engine
            FreeAndNil(lBaseClass);
            end;
    end;
  lClassList.RemoveAll;
  FreeAndNil(lClassList);

  ForceRedraw;
  end;

procedure TcdClassDiagram.EditSimpleClass(aClassSyncID: Integer);
  begin
  //?! editing
  end;

procedure TcdClassDiagram.DeleteSimpleClass(aClassSyncID: Integer);
  begin
  //?! editing
  end;

function TcdClassDiagram.NewSimpleClass: Integer;
  begin
  //?! editing
  result := 0;
  end;

procedure TcdClassDiagram.EditComplexClass(aClassSyncID: Integer);
  begin
  //?! editing
  end;

procedure TcdClassDiagram.DeleteComplexClass(aClassSyncID: Integer);
  begin
  //?! editing
  end;

function TcdClassDiagram.NewComplexClass: Integer;
  begin
  //?! editing
  result := 0;
  end;

procedure TcdClassDiagram.ComplexClass_DefaultForm(aClassSyncID: Integer);
  begin
  //?! editing
  end;

function TcdClassDiagram.Rule_CreateAndLoadFromObject(aComplexClass: TcdComplexClass;
  aRelationType: TcRelationType; aRuleType: TcdRuleType; aRoles: TcElementList<TcRole>; aRole: TcRole = nil): TcdRule;
  var
    lRule: TcdRule;
  begin
  lRule := nil;
  case aRuleType of
     ctIdentification, ctUnicity:
        begin
        lRule := TcdIndex.Create(Self, aComplexClass, aRuleType);
        lRule.LoadFromObject(aRelationType, aRuleType, aRoles);
        end;

     ctTotality:
        begin
        lRule := TcdTotality.Create(Self, aComplexClass, aRuleType);
        lRule.LoadFromObject(aRelationType, aRuleType, nil, aRole);
        end;

     ctSortIndex:
        EInt('TcdClassDiagram.Rule_CreateAndLoadFromData', 'Sorting indexes are not supported');

     ctDisplay:
        EInt('TcdClassDiagram.Rule_CreateAndLoadFromData', 'Display rules are not supported');

     else EInt('TcdClassDiagram.Rule_CreateAndLoadFromData', 'Unknown rule type');
     end;

  result := lRule;
  end;

function TcdClassDiagram.GetModuleColor(aModuleID: Integer): TColor;
  var
    lIndex: Integer;
  begin
  Result := 0;
  if aModuleID=0
     then Exit;

  lIndex := fModules.IndexOf(aModuleID);
  if lIndex<0
     then Exit;

  Result := cModuleColors[lIndex MOD cMaxModuleColors];
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdClassStyle
//
constructor TcdClassStyle.Create(aName: string);
  begin
  inherited Create(aName);

  // Main style

  fORWIndicators := TStringlist.Create;
  fORWIndicatorDisplayStrs := TStringlist.Create;

  // Customizable visual appearance within a style

  fClassFontName := 'ARIAL';
  feClassFontHeight := cFontSize8;
  fClassFontStyle := [];
  fClassFontColor := clBlack;

  fSimpleClassPenColor := clBlack;
  fSimpleClassFillColor := clWhite;
  fSimpleClassPenColor_Selected := clNavy;
  fSimpleClassFillColor_Selected := $E0E0E0;

  fSimpleClassTitleFontName := fClassFontName;
  feSimpleClassTitleFontHeight := cFontSize8;
  fSimpleClassTitleFontStyle := [];
  fSimpleClassTitleFontColor := clBlack;

  feSimpleClassTitleHorizontalMargin := 8;
  feSimpleClassTitleVerticalMargin := 4;

  fComplexClassPenColor := clBlack;
  fComplexClassFillColor := clWhite;
  fComplexClassPenColor_Selected := clNavy;
  fComplexClassFillColor_Selected := $E0E0E0;

  fComplexClassTitleFontName := fClassFontName;
  feComplexClassTitleFontHeight := cFontSize8;
  fComplexClassTitleFontStyle := [fsBold];
  fComplexClassTitleFontColor := clWhite;

  fComplexClassTitleAlignment := ctaLeft;
  feComplexClassTitleVerticalMargin := 12;

  feComplexClassTitleFrameHorizontalMargin := 4;
  feComplexClassTitleFrameVerticalMargin := 2;
  fComplexClassTitleFrameFillColor := clGray;
  fComplexClassTitleFrameFillColor_Selected := clNavy;

  feConnectorFrameMargin := 10;

  fAttributeFontColor_Selected := clWhite;
  fAttributeFillColor_Selected := clNavy;

  feAttributeFrameHorizontalMargin := 4;
  feAttributeFrameVerticalMargin := 2;

  fReferencingAttributeFontStyle := ClassFontStyle;

  fRuleFontName := fClassFontName;
  feRuleFontHeight := cFontSize8;
  fRuleFontStyle := [];
  fRuleFontColor := clBlack;

  feUnicityRuleMargin := 7;
  feUnicityArrowLength := 5;
  feUnicityArrowHeightDIV2 := 2;

  feDoubleIdentityLineMargin := 1;
  end;

destructor TcdClassStyle.Destroy;
  begin
  FreeAndNIL(fORWIndicators);
  FreeAndNIL(fORWIndicatorDisplayStrs);
  inherited Destroy;
  end;

procedure TcdClassStyle.DeriveProperties (aDiagram: TcdDiagram);
  var
    T: Integer;
    lNormalTextRowHeight, lSmallTextRowHeight: Integer;
  begin
  inherited DeriveProperties (aDiagram);
  fiClassFontHeight := MulDiv(feClassFontHeight, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiSimpleClassWidth := MulDiv(feSimpleClassWidth, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiSimpleClassHeight := MulDiv(feSimpleClassHeight, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiSimpleClassTitleFontHeight := MulDiv(feSimpleClassTitleFontHeight, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiSimpleClassTitleHorizontalMargin := MulDiv(feSimpleClassTitleHorizontalMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiSimpleClassTitleVerticalMargin := MulDiv(feSimpleClassTitleVerticalMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiComplexClassTitleFontHeight := MulDiv(feComplexClassTitleFontHeight, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiComplexClassTitleVerticalMargin := MulDiv(feComplexClassTitleVerticalMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiComplexClassTitleFrameHorizontalMargin := MulDiv(feComplexClassTitleFrameHorizontalMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiComplexClassTitleFrameVerticalMargin := MulDiv(feComplexClassTitleFrameVerticalMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiConnectorFrameMargin := MulDiv(feConnectorFrameMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiAttributeFrameHorizontalMargin := MulDiv(feAttributeFrameHorizontalMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiAttributeFrameVerticalMargin := MulDiv(feAttributeFrameVerticalMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiRuleFontHeight := MulDiv(feRuleFontHeight, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiUnicityRuleMargin := MulDiv(feUnicityRuleMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiUnicityArrowLength := MulDiv(feUnicityArrowLength, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiUnicityArrowHeightDIV2 := MulDiv(feUnicityArrowHeightDIV2, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiDoubleIdentityLineMargin := MulDiv(feDoubleIdentityLineMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);

  // Complex Classes are the basic objects
  fPluralnameOfObjects := fComplexClass_PluralName;

  with aDiagram.ScaleCanvas do
    begin
    Canvas := nil;

    // Attribute font
    Font_Name := ClassFontname;
    Font_Height := ClassFontHeight;
    Font_Style := ClassFontStyle;

    lNormalTextRowHeight := TextHeight(cHeightDeterminationStr);

    // Attribute frame width
    // Default width for roles with numbers: 5 digits
    fiAttributeFrameWidth := Diagrammer_RoundTo(TextWidth('00000') + AttributeFrameHorizontalMargin*2, ScaleMargin);

    if fAttributeOrientation=aoHorizontal
       then begin
            // Rule font
            Font_Name := RuleFontname;
            Font_Height := RuleFontHeight;
            Font_Style := RuleFontStyle;
            end;

    // Determine the right margin for the ORW Indicator
    fiORWIndicatorRightMargin := 0;
    for T := 0 to fORWIndicatorDisplayStrs.Count-1 do
       fiORWIndicatorRightMargin := Max(fiORWIndicatorRightMargin, TextWidth(fORWIndicatorDisplayStrs[T]));
    fiORWIndicatorRightMargin := fiORWIndicatorRightMargin + fiAttributeFrameHorizontalMargin;

    if fAttributeOrientation=aoHorizontal
       then begin
            lSmallTextRowHeight := TextHeight(cHeightDeterminationStr);

            // Attribute frame height
            if fDisplayAttributeTypeNames
               then fiAttributeFrameHeight := Diagrammer_RoundTo(lNormalTextRowHeight*2 + lSmallTextRowHeight + AttributeFrameVerticalMargin*2, ScaleMargin)
               else fiAttributeFrameHeight := Diagrammer_RoundTo(lNormalTextRowHeight + lSmallTextRowHeight + AttributeFrameVerticalMargin*2, ScaleMargin);

            if fORWIndicatorVerticalPosition = vpTop
               then fiAttributeFrameVerticalMargin_ORW := AttributeFrameVerticalMargin
               else fiAttributeFrameVerticalMargin_ORW := AttributeFrameHeight - AttributeFrameVerticalMargin - lSmallTextRowHeight;
            fiAttributeFrameVerticalMargin_Name := AttributeFrameVerticalMargin + (lSmallTextRowHeight DIV 2);
            fiAttributeFrameVerticalMargin_Type := AttributeFrameVerticalMargin + (lSmallTextRowHeight DIV 2) + lNormalTextRowHeight;
            end;
    end;
  end;

procedure TcdClassStyle.DeleteDisplayStrsForORWIndicators;
  begin
  fORWIndicators.Clear;
  fORWIndicatorDisplayStrs.Clear;
  end;

procedure TcdClassStyle.AddDisplayStrForORWIndicator(aORWIndicator, aDisplayStr: string);
  begin
  fORWIndicators.Add(aORWIndicator);
  fORWIndicatorDisplayStrs.Add(aDisplayStr);
  end;

function TcdClassStyle.GetDisplayStrForORWIndicator(aORWIndicator: string): string;
  var
    lPos: Integer;
  begin
  Result := '';
  lPos := fORWIndicators.IndexOf(aORWIndicator);
  if lPos>=0
     then Result := fORWIndicatorDisplayStrs[lPos];
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdClassStyle_UML
//
constructor TcdClassStyle_UML.Create(aName: string);
  begin
  inherited Create(aName);

  // Main style

  fComplexClass_Name       := 'Class';
  fComplexClass_PluralName := 'Classes';
  fSimpleClass_Name        := 'Attribute type';
  fSimpleClass_PluralName  := 'Attribute types';
  fAttribute_Name          := 'Attribute';
  fAttribute_PluralName    := 'Attributes';

  fDisplayReferencingAttributes := False;
  fAttributeOrientation := aoVertical;
  fReferenceStyle := rsClassToClass;
  fReferenceNamePosition := rnpAttribute;

  fReferenceLineType := ltStraightLine;
  fReferenceLineStyle := lsSolid;
  fReferenceLineFromObjectConType := octNone;
  fReferenceLineToObjectConType := octNone;

  fSubtypeLineType := ltStraightLine;
  fSubtypeLineStyle := lsSolid;
  fSubtypeLineFromObjectConType := octNone;
  fSubtypeLineToObjectConType := octOpenArrow;

  fAutoSizeSimpleClass := True;
  fSimpleClassShape := scsRect;
  feSimpleClassWidth := 80;
  feSimpleClassHeight := 35;

  fConnectorFrame := False;
  fConnectorFrameStyle := cfsRoundRect;

  fDisplayAttributeTypeNames := False;
  fAttributeTypeName_StartSymbol := '';
  fAttributeTypeName_EndSymbol := '';

  fDisplayORWIndicator := False;
  fORWIndicatorVerticalPosition := vpTop;

  fDerivableIndicatorPosition := hpLeft;
  fDerivableIndicator := '/';
  fInitialDerivableIndicator := 'i/';

  fDisplayIndexes := False;
  fDisplayTotality := False;
  fIndex_IdentificationStr := 'i';
  fIndex_UniqueStr := 'u';
  fIndex_SortStr := 's';
  fTotalityStr := 't';
  fDisplayRuleStr := 'd';

  fDisplayUnicityArrow := False;

  fDoubleIdentityLine := False;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdClassStyle_UML-IM
//
constructor TcdClassStyle_UMLIM.Create(aName: string);
  begin
  inherited Create(aName);

  // Inherited
  // Change 2024: Display associations as normal UML associations
  //
  // Old code:
  // fCardinalityManyChar := '*';
  // DeleteCardinalityStrReplacements;
  // AddCardinalityStrReplacement('0..1','');
  // AddCardinalityStrReplacement('1..1','');
  // AddCardinalityStrReplacement('0..*','');

  // Main style

  fComplexClass_Name       := 'Class';
  fComplexClass_PluralName := 'Classes';
  fSimpleClass_Name        := 'Attribute type';
  fSimpleClass_PluralName  := 'Attribute types';
  fAttribute_Name          := 'Attribute';
  fAttribute_PluralName    := 'Attributes';

  fDisplayReferencingAttributes := True;
  fAttributeOrientation := aoVertical;
  fReferenceStyle := rsClassToClass;
  fReferenceNamePosition := rnpAttribute;

  fReferenceLineType := ltStraightLine;
  fReferenceLineStyle := lsSolid;
  fReferenceLineFromObjectConType := octNone;
  fReferenceLineToObjectConType := octNone;

  fSubtypeLineType := ltStraightLine;
  fSubtypeLineStyle := lsSolid;
  fSubtypeLineFromObjectConType := octNone;
  fSubtypeLineToObjectConType := octOpenArrow;

  fAutoSizeSimpleClass := True;
  fSimpleClassShape := scsRect;
  feSimpleClassWidth := 80;
  feSimpleClassHeight := 35;

  fConnectorFrame := False;
  fConnectorFrameStyle := cfsRoundRect;

  fDisplayAttributeTypeNames := True;
  fAttributeTypeName_StartSymbol := '';
  fAttributeTypeName_EndSymbol := '';

  fDisplayORWIndicator := True;
  DeleteDisplayStrsForORWIndicators;
  AddDisplayStrForORWIndicator('O', '');
  AddDisplayStrForORWIndicator('R', 'r');
  AddDisplayStrForORWIndicator('W', 'w');

  fORWIndicatorVerticalPosition := vpTop;

  fDerivableIndicatorPosition := hpLeft;
  fDerivableIndicator := '/';
  fInitialDerivableIndicator := 'i/';

  fDisplayIndexes := True;
  fDisplayTotality := False;
  fIndex_IdentificationStr := 'i';
  fIndex_UniqueStr := 'u';
  fIndex_SortStr := 's';
  fTotalityStr := 't';
  fDisplayRuleStr := 'd';

  fDisplayUnicityArrow := False;

  fDoubleIdentityLine := False;

  // Customized visuals

  fReferencingAttributeFontStyle := [fsUnderline];
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdClassStyle_IM
//
constructor TcdClassStyle_IM.Create(aName: string);
  begin
  inherited Create(aName);

  // Inherited

  fDisplayCardinality := False;

  // Main style

  fComplexClass_Name       := 'Class';
  fComplexClass_PluralName := 'Classes';
  fSimpleClass_Name        := 'Attribute type';
  fSimpleClass_PluralName  := 'Attribute types';
  fAttribute_Name          := 'Attribute';
  fAttribute_PluralName    := 'Attributes';

  fDisplayReferencingAttributes := True;
  fAttributeOrientation := aoVertical;
  fReferenceStyle := rsAttributeToClass;
  fReferenceNamePosition := rnpAttribute;

  fReferenceLineType := lt3PLine;
  fReferenceLineStyle := lsSolid;
  fReferenceLineFromObjectConType := octNone;
  fReferenceLineToObjectConType := octNone;

  fSubtypeLineType := ltStraightLine;
  fSubtypeLineStyle := lsSolid;
  fSubtypeLineFromObjectConType := octNone;
  fSubtypeLineToObjectConType := octOpenArrow;

  fAutoSizeSimpleClass := True;
  fSimpleClassShape := scsRect;
  feSimpleClassWidth := 80;
  feSimpleClassHeight := 35;

  fConnectorFrame := False;
  fConnectorFrameStyle := cfsRoundRect;

  fDisplayAttributeTypeNames := True;
  fAttributeTypeName_StartSymbol := '';
  fAttributeTypeName_EndSymbol := '';

  fDisplayORWIndicator := True;
  DeleteDisplayStrsForORWIndicators;
  AddDisplayStrForORWIndicator('O', '');
  AddDisplayStrForORWIndicator('R', 'r');
  AddDisplayStrForORWIndicator('W', 'w');

  fORWIndicatorVerticalPosition := vpTop;

  fDerivableIndicatorPosition := hpLeft;
  fDerivableIndicator := '/';
  fInitialDerivableIndicator := 'i/';

  fDisplayIndexes := True;
  fDisplayTotality := False;
  fIndex_IdentificationStr := 'i';
  fIndex_UniqueStr := 'u';
  fIndex_SortStr := 's';
  fTotalityStr := 't';
  fDisplayRuleStr := 'd';

  fDisplayUnicityArrow := False;

  fDoubleIdentityLine := False;

  // Customized visuals

  fReferencingAttributeFontStyle := [fsUnderline];
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdClassStyle_ERD
//
constructor TcdClassStyle_ERD.Create(aName: string);
  begin
  inherited Create(aName);

  // Inherited

  fDisplayCardinality := False;

  // Main style

  fComplexClass_Name       := 'Entity';
  fComplexClass_PluralName := 'Entities';
  fSimpleClass_Name        := 'Attribute type';
  fSimpleClass_PluralName  := 'Attribute types';
  fAttribute_Name          := 'Attribute';
  fAttribute_PluralName    := 'Attributes';

  fDisplayReferencingAttributes := True;
  fAttributeOrientation := aoVertical;
  fReferenceStyle := rsAttributeToClass;
  fReferenceNamePosition := rnpAttribute;

  fReferenceLineType := lt3PLine;
  fReferenceLineStyle := lsSolid;
  fReferenceLineFromObjectConType := octNone;
  fReferenceLineToObjectConType := octLineArrow;

  fSubtypeLineType := ltStraightLine;
  fSubtypeLineStyle := lsSolid;
  fSubtypeLineFromObjectConType := octNone;
  fSubtypeLineToObjectConType := octOpenArrow;

  fAutoSizeSimpleClass := True;
  fSimpleClassShape := scsRect;
  feSimpleClassWidth := 80;
  feSimpleClassHeight := 35;

  fConnectorFrame := False;
  fConnectorFrameStyle := cfsRoundRect;

  fDisplayAttributeTypeNames := False;
  fAttributeTypeName_StartSymbol := '';
  fAttributeTypeName_EndSymbol := '';

  fDisplayORWIndicator := True;
  DeleteDisplayStrsForORWIndicators;
  AddDisplayStrForORWIndicator('O', '');
  AddDisplayStrForORWIndicator('R', 'nn');
  AddDisplayStrForORWIndicator('W', 'w');

  fORWIndicatorVerticalPosition := vpTop;

  fDerivableIndicatorPosition := hpRight;
  fDerivableIndicator := '*';
  fInitialDerivableIndicator := '*i';

  fDisplayIndexes := True;
  fDisplayTotality := False;
  fIndex_IdentificationStr := 'i';
  fIndex_UniqueStr := 'u';
  fIndex_SortStr := 's';
  fTotalityStr := 't';
  fDisplayRuleStr := 'd';

  fDisplayUnicityArrow := False;

  fDoubleIdentityLine := False;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdClassStyle_Record
//
constructor TcdClassStyle_Record.Create(aName: string);
  begin
  inherited Create(aName);

  // Inherited

  fDisplayCardinality := False;

  // Main style

  fComplexClass_Name       := 'Table';
  fComplexClass_PluralName := 'Tables';
  fSimpleClass_Name        := 'Domain';
  fSimpleClass_PluralName  := 'Domains';
  fAttribute_Name          := 'Column';
  fAttribute_PluralName    := 'Columns';

  fDisplayReferencingAttributes := True;
  fAttributeOrientation := aoHorizontal;
  fReferenceStyle := rsAttributeToClass;
  fReferenceNamePosition := rnpAttribute;

  fReferenceLineType := lt3PLine;
  fReferenceLineStyle := lsSolid;
  fReferenceLineFromObjectConType := octClaw;
  fReferenceLineToObjectConType := octClaw;

  fSubtypeLineType := ltStraightLine;
  fSubtypeLineStyle := lsSolid;
  fSubtypeLineFromObjectConType := octNone;
  fSubtypeLineToObjectConType := octOpenArrow;

  fAutoSizeSimpleClass := True;
  fSimpleClassShape := scsRect;
  feSimpleClassWidth := 80;
  feSimpleClassHeight := 35;

  fConnectorFrame := False;
  fConnectorFrameStyle := cfsRoundRect;

  fDisplayAttributeTypeNames := True;
  fAttributeTypeName_StartSymbol := '(';
  fAttributeTypeName_EndSymbol := ')';

  fDisplayORWIndicator := True;
  DeleteDisplayStrsForORWIndicators;
  AddDisplayStrForORWIndicator('O', 'op');
  AddDisplayStrForORWIndicator('R', '');
  AddDisplayStrForORWIndicator('W', 'w');

  fORWIndicatorVerticalPosition := vpTop;

  fDerivableIndicatorPosition := hpRight;
  fDerivableIndicator := '*';
  fInitialDerivableIndicator := '*i';

  fDisplayIndexes := True;
  fDisplayTotality := False;
  fIndex_IdentificationStr := 'i';
  fIndex_UniqueStr := 'u';
  fIndex_SortStr := 's';
  fTotalityStr := 't';
  fDisplayRuleStr := 'd';

  fDisplayUnicityArrow := False;

  fDoubleIdentityLine := False;

  // Customized visuals

  fSimpleClassFillColor_Selected := $D0D0D0;
  fComplexClassFillColor_Selected := $D0D0D0;

  feComplexClassTitleFontHeight := cFontSize10;
  fComplexClassTitleFontColor := clBlack;

  fAttributeFontColor_Selected := clBlack;
  fAttributeFillColor_Selected := clLime;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdClassStyle_ORM
//
constructor TcdClassStyle_ORM.Create(aName: string);
  begin
  inherited Create(aName);

  // Inherited

  //!! fCardinalityDotSide := lsFrom; (Display the Cardinality to the role's side of the line, with is much more clear, however not standard).
  fDisplayCardinality := False;

  // Main style

  fComplexClass_Name       := 'ClaimType/AnnotationType'; // 'Facttype/Objecttype';
  fComplexClass_PluralName := 'ClaimTypes/AnnotationTypes'; // 'Facttypes/Objecttypes';
  fSimpleClass_Name        := 'LabelType';
  fSimpleClass_PluralName  := 'LabelTypes';
  fAttribute_Name          := 'Role';
  fAttribute_PluralName    := 'Roles';

  fDisplayReferencingAttributes := True;
  fAttributeOrientation := aoHorizontal;
  fReferenceStyle := rsAttributeToClass;
  fReferenceNamePosition := rnpAttribute;

  fReferenceLineType := ltStraightLine;
  fReferenceLineStyle := lsSolid;
  fReferenceLineFromObjectConType := octCardDot;
  fReferenceLineToObjectConType := octCardDot;

  fSubtypeLineType := ltStraightLine;
  fSubtypeLineStyle := lsSolid;
  fSubtypeLineFromObjectConType := octNone;
  fSubtypeLineToObjectConType := octArrow;

  fAutoSizeSimpleClass := False;
  fSimpleClassShape := scsCircle;
  feSimpleClassWidth := 50;
  feSimpleClassHeight := 50;

  fConnectorFrame := True;
  fConnectorFrameStyle := cfsEllipse;

  fDisplayAttributeTypeNames := False;
  fAttributeTypeName_StartSymbol := '';
  fAttributeTypeName_EndSymbol := '';

  fDisplayORWIndicator := True;
  DeleteDisplayStrsForORWIndicators;
  AddDisplayStrForORWIndicator('O', 'op');
  AddDisplayStrForORWIndicator('R', '');
  AddDisplayStrForORWIndicator('W', 'w');

  fORWIndicatorVerticalPosition := vpBottom;

  fDerivableIndicatorPosition := hpRight;
  fDerivableIndicator := '*';
  fInitialDerivableIndicator := '*i';

  fDisplayIndexes := True;
  fDisplayTotality := True;
  fIndex_IdentificationStr := 'i';
  fIndex_UniqueStr := 'u';
  fIndex_SortStr := 's';
  fTotalityStr := 't';
  fDisplayRuleStr := 'd';

  fDisplayUnicityArrow := True;

  fDoubleIdentityLine := False;

  // Customized visuals

  fSimpleClassFillColor_Selected := $D0D0D0;
  fComplexClassFillColor_Selected := $D0D0D0;

  feComplexClassTitleFontHeight := cFontSize10;
  fComplexClassTitleFontColor := clBlack;

  fAttributeFontColor_Selected := clBlack;
  fAttributeFillColor_Selected := clLime;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdClassStyle_ORM2
//
constructor TcdClassStyle_ORM2.Create(aName: string);
  begin
  inherited Create(aName);

  // Inherited

  //!! fCardinalityDotSide := lsFrom; (Display the Cardinality to the role's side of the line, with is much more clear, however not standard).
  fDisplayCardinality := False;

  // Main style

  fComplexClass_Name       := 'ClaimType/AnnotationType'; // 'Facttype/Objecttype';
  fComplexClass_PluralName := 'ClaimTypes/AnnotationTypes'; // 'Facttypes/Objecttypes';
  fSimpleClass_Name        := 'LabelType';
  fSimpleClass_PluralName  := 'LabelTypes';
  fAttribute_Name          := 'Role';
  fAttribute_PluralName    := 'Roles';

  fDisplayReferencingAttributes := True;
  fAttributeOrientation := aoHorizontal;
  fReferenceStyle := rsAttributeToClass;
  fReferenceNamePosition := rnpAttribute;

  fReferenceLineType := ltStraightLine;
  fReferenceLineStyle := lsSolid;
  fReferenceLineFromObjectConType := octCardDot;
  fReferenceLineToObjectConType := octCardDot;

  fSubtypeLineType := ltStraightLine;
  fSubtypeLineStyle := lsSolid;
  fSubtypeLineFromObjectConType := octNone;
  fSubtypeLineToObjectConType := octArrow;

  fAutoSizeSimpleClass := False;
  fSimpleClassShape := scsRoundRect;
  feSimpleClassWidth := 50;
  feSimpleClassHeight := 50;

  fConnectorFrame := True;
  fConnectorFrameStyle := cfsRoundRect;

  fDisplayAttributeTypeNames := False;
  fAttributeTypeName_StartSymbol := '';
  fAttributeTypeName_EndSymbol := '';

  fDisplayORWIndicator := True;
  DeleteDisplayStrsForORWIndicators;
  AddDisplayStrForORWIndicator('O', 'op');
  AddDisplayStrForORWIndicator('R', '');
  AddDisplayStrForORWIndicator('W', 'w');

  fORWIndicatorVerticalPosition := vpBottom;

  fDerivableIndicatorPosition := hpRight;
  fDerivableIndicator := '*';
  fInitialDerivableIndicator := '*i';

  fDisplayIndexes := True;
  fDisplayTotality := True;
  fIndex_IdentificationStr := 'i';
  fIndex_UniqueStr := 'u';
  fIndex_SortStr := 's';
  fTotalityStr := 't';
  fDisplayRuleStr := 'd';

  fDisplayUnicityArrow := False;

  fDoubleIdentityLine := True;

  // Customized visuals

  fSimpleClassFillColor_Selected := $D0D0D0;
  fComplexClassFillColor_Selected := $D0D0D0;

  feComplexClassTitleFontHeight := cFontSize10;
  fComplexClassTitleFontColor := clBlack;

  fAttributeFontColor_Selected := clBlack;
  fAttributeFillColor_Selected := clLime;

  //!! Original value: 5
  feUnicityRuleMargin := 1;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdClassStyle_ORM2_Compact
//
constructor TcdClassStyle_ORM2_Compact.Create(aName: string);
  begin
  inherited Create(aName);

  // Inherited

  //!! fCardinalityDotSide := lsFrom; (Display the Cardinality to the role's side of the line, with is much more clear, however not standard).
  fDisplayCardinality := False;

  // Main style

  fComplexClass_Name       := 'ClaimType/AnnotationType'; // 'Facttype/Objecttype';
  fComplexClass_PluralName := 'ClaimTypes/AnnotationTypes'; // 'Facttypes/Objecttypes';
  fSimpleClass_Name        := 'LabelType';
  fSimpleClass_PluralName  := 'LabelTypes';
  fAttribute_Name          := 'Role';
  fAttribute_PluralName    := 'Roles';

  fDisplayReferencingAttributes := True;
  fAttributeOrientation := aoVertical;
  fReferenceStyle := rsAttributeToClass;
  fReferenceNamePosition := rnpAttribute;

  fReferenceLineType := lt3PLine;
  fReferenceLineStyle := lsSolid;
  fReferenceLineFromObjectConType := octCardDot;
  fReferenceLineToObjectConType := octCardDot;

  fSubtypeLineType := ltStraightLine;
  fSubtypeLineStyle := lsSolid;
  fSubtypeLineFromObjectConType := octNone;
  fSubtypeLineToObjectConType := octArrow;

  fAutoSizeSimpleClass := True;
  fSimpleClassShape := scsRoundRect;
  feSimpleClassWidth := 80;
  feSimpleClassHeight := 35;

  fConnectorFrame := True;
  fConnectorFrameStyle := cfsRoundRect;

  fDisplayAttributeTypeNames := False;
  fAttributeTypeName_StartSymbol := '';
  fAttributeTypeName_EndSymbol := '';

  fDisplayORWIndicator := True;
  DeleteDisplayStrsForORWIndicators;
  AddDisplayStrForORWIndicator('O', 'op');
  AddDisplayStrForORWIndicator('R', '');
  AddDisplayStrForORWIndicator('W', 'w');

  fORWIndicatorVerticalPosition := vpBottom;

  fDerivableIndicatorPosition := hpRight;
  fDerivableIndicator := '*';
  fInitialDerivableIndicator := '*i';

  fDisplayIndexes := True;
  fDisplayTotality := True;
  fIndex_IdentificationStr := 'i';
  fIndex_UniqueStr := 'u';
  fIndex_SortStr := 's';
  fTotalityStr := 't';
  fDisplayRuleStr := 'd';

  fDisplayUnicityArrow := False;

  fDoubleIdentityLine := False;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdClassList
//
function TcdClassList.Compare (const aObjectA, aObjectB: TObject): Integer;
  begin
  if TcdBaseClass(aObjectA).SyncID>TcdBaseClass(aObjectB).SyncID
     then Result := 1
  else if TcdBaseClass(aObjectA).SyncID<TcdBaseClass(aObjectB).SyncID
     then Result := -1
  else Result := 0;
  end;

function TcdClassList.Compare (const aObject: TObject; const aKeyValue: string): Integer;
  begin
  if TcdBaseClass(aObject).SyncID>StrToInt(aKeyValue)
     then Result := 1
  else if TcdBaseClass(aObject).SyncID<StrToInt(aKeyValue)
     then Result := -1
  else Result := 0;
  end;

function TcdClassList.Find(aSyncID: Integer): TcdBaseClass;
  var
     lPos: Integer;
  begin
  if Search(IntToStr(aSyncID), lPos)
     then Result := TcdBaseClass(Objects[lPos])
     else Result := nil;
  end;

function TcdClassList.FindByName(aName: string): TcdBaseClass;
  var
    i: integer;
    lBaseClass: TcdBaseClass;
  begin
  result := nil;
  for i := 0 to Count-1 do
    begin
    lBaseClass := TcdBaseClass(Objects[i]);
    if CompareText(lBaseClass.Name, aName)=0
       then begin
            result := lBaseClass;
            exit;
            end;
    end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdBaseClass
//
procedure TcdBaseClass.SetSyncID(aValue: Integer);
  begin
  if fSyncID<>-1
     then TcdClassDiagram(Diagram).Classes.Remove(Self);

  fSyncID := aValue;

  if fSyncID<>-1
     then TcdClassDiagram(Diagram).Classes.Add(Self);
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdSimpleClass
//

procedure TcdSimpleClass.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer);
  begin
  if aButton = mbRight
     then TcdClassDiagram(Diagram).DoSimpleClassPopupMenu(aX, aY, Self)
     else inherited MouseDown (aButton, aShiftState, aX, aY);
  end;

procedure TcdSimpleClass.DblClick;
  begin
  inherited;
  TcdClassDiagram(Diagram).EditSimpleClass(SyncID);
  end;

procedure TcdSimpleClass.ResetBounds_Internal;
  var
    A, B, C, D,
    lLeft, lTop, lRight, lBottom,
    X, Y,
    lWidth, lHeight,
    lCenter, lSafetyMargin: Integer;
    lStyle: TcdClassStyle;
  begin
  inherited;
  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := nil;

    // Calculate base size
    if lStyle.AutoSizeSimpleClass
       then begin
            // Set font
            Font_Name := lStyle.SimpleClassTitleFontname;
            Font_Height := lStyle.SimpleClassTitleFontHeight;
            Font_Style := lStyle.SimpleClassTitleFontStyle;

            // Calculate dimensions
            lWidth := TextWidth(DisplayName) + lStyle.SimpleClassTitleHorizontalMargin*2;
            lHeight := TextHeight(cHeightDeterminationStr) + lStyle.SimpleClassTitleVerticalMargin*2;

            lWidth := Max(lStyle.SimpleClassWidth, lWidth);
            lHeight := Max(lStyle.SimpleClassHeight, lHeight);
            end
       else begin
            // Fixed width / height
            lWidth := lStyle.SimpleClassWidth;
            lHeight := lStyle.SimpleClassHeight;
            end;

    // Set the internal bounds
    SetIntBounds(IntLeft, IntTop, lWidth, lHeight);

    // Prepare round rect and connection points
    A := 0;
    lLeft := 0;
    lTop := 0;
    lRight := IntWidth;
    lBottom := IntHeight;

    // Prepare round rect
    if lStyle.SimpleClassShape = scsRoundRect
       then begin
            // Calculate round rect
            A := 25; B := 10; C:= 2; D := 50;
            lLeft := lStyle.ScaleMargin;
            lTop := lStyle.ScaleMargin;
            lRight := IntWidth - lStyle.ScaleMargin;
            lBottom := IntHeight - lStyle.ScaleMargin;

            DeriveRoundRectPoints(fPoints, lLeft,lTop,lRight,lBottom, A,B,C,D);
            end;

    // Create connection points
    DeleteConnectionPoints;
    if lStyle.SimpleClassShape <> scsCircle
       then begin
            // Create connection points
            lSafetyMargin := Diagram.Style.ClawWidth + A;
            lCenter := Diagram.SnapToGridX((lRight-lLeft) DIV 2);
            X := lCenter;
            while X <= lRight-lSafetyMargin do
               begin
               AddConnectionPoint(X, lTop);
               AddConnectionPoint(X, lBottom);
               if X<>lCenter
                  then begin
                       AddConnectionPoint(lCenter-(X-lCenter), lTop);
                       AddConnectionPoint(lCenter-(X-lCenter), lBottom);
                       end;
               Inc(X, lStyle.GridX);
               end;

            lCenter := Diagram.SnapToGridY((lBottom-lTop) DIV 2);
            Y := lCenter;
            while Y <= lBottom-lSafetyMargin do
               begin
               AddConnectionPoint(lLeft, Y);
               AddConnectionPoint(lRight, Y);
               if Y<>lCenter
                  then begin
                       AddConnectionPoint(lLeft, lCenter-(Y-lCenter));
                       AddConnectionPoint(lRight, lCenter-(Y-lCenter));
                       end;
               Inc(Y, lStyle.GridY);
               end;
            end;
    end;
  end;

procedure TcdSimpleClass.ResetBounds_Children;
  begin
  inherited;
  dTitle.ResetBounds;
  end;

procedure TcdSimpleClass.CorrectZOrder;
  var
    T: Integer;
  begin
  // Lines
  for T := 0 to Lines.Count-1 do
     TcdLine(Lines[T]).CorrectZOrder;

  // Self
  inherited CorrectZOrder;

  // Contents
  dTitle.BringToFront;
  end;

function TcdSimpleClass.GetConnectionPoint(aLine: TcdLine; aPoint: TPoint): TPoint;
  var
    lStyle: TcdClassStyle;
    lDX, lDY, lnDX, lnDY, lCenterX, lCenterY: Integer;
    lAlfa: Double;
  begin
  result := Default(TPoint);
  lStyle := TcdClassStyle(Diagram.Style);

  if aLine.ToObject=Self
     then begin
          // The simple class should allways be at the end of the line
          if lStyle.SimpleClassShape = scsCircle
             then begin
                  lCenterX := IntLeft + (IntWidth DIV 2);
                  lCenterY := IntTop  + (IntHeight DIV 2);
                  lDX := Abs(aPoint.X - lCenterX);
                  lDY := Abs(aPoint.Y - lCenterY);

                  if lDX = 0
                     then lAlfa := 0.5*Pi
                     else lAlfa := ArcTan (lDY / lDX);

                  lnDX := Round( Cos(lAlfa) * (IntWidth DIV 2) );
                  lnDY := Round( Sin(lAlfa) * (IntHeight DIV 2) );

                  if lCenterY > aPoint.Y
                     then begin
                          if lCenterX > aPoint.X
                             then Result := Classes.Point(lCenterX - lnDX, lCenterY - lnDY)
                             else Result := Classes.Point(lCenterX + lnDX, lCenterY - lnDY)
                          end
                     else begin
                          if lCenterX > aPoint.X
                             then Result := Classes.Point(lCenterX - lnDX, lCenterY + lnDY)
                             else Result := Classes.Point(lCenterX + lnDX, lCenterY + lnDY)
                          end;

                  // Handle exception
                  if ((Result.X < 0) and (lCenterX >= 0)) or
                     ((Result.Y < 0) and (lCenterY >= 0))
                     then Result := Classes.Point(lCenterX, lCenterY)
                  end
             else Result := inherited GetConnectionPoint(aLine, aPoint);
          end
     else EInt('TcdSimpleClass.GetConnectionPoint', 'Simple classes should be at the end of a line.');
  end;

function TcdSimpleClass.GetDisplayName: string;
  begin
  Result := Name;
  end;

constructor TcdSimpleClass.Create(aDiagram: TcdDiagram);
  begin
  inherited Create(aDiagram, ooCentered);

  fSyncID := -1;
  fType := ctLabelType;

  fdTitle := TcdSimpleClassTitle.Create(Diagram, Self);
  end;

destructor TcdSimpleClass.Destroy;
  begin
  if SyncID<>-1
     then TcdClassDiagram(Diagram).Classes.Remove(Self);

  FreeAndNil(fdTitle);

  inherited Destroy;
  end;

procedure TcdSimpleClass.InitChildren;
  begin
  Diagram.Add(fdTitle);
  end;

procedure TcdSimpleClass.LoadFromJson(aJsonObject: TJsonObject);
  var
    lLeft, lTop: Integer;
    lLabelType: TcLabelType;
  begin
  inherited LoadFromJson(aJsonObject);

  if not aJsonObject.TryGetValue('name', fName) then InvalidJson_xExpected('name');
  if not aJsonObject.TryGetValue('left', lLeft) then InvalidJson_xExpected('left');
  if not aJsonObject.TryGetValue('top', lTop) then InvalidJson_xExpected('top');

  lLabelType := TcdClassDiagram(Diagram).Engine.ConceptualTypes.FindLabelType(fName);
  if not Assigned(lLabelType) then exit;
  LoadFromObject(lLabelType);

  Diagram.Add(Self, Diagram.ieXPos(lLeft), Diagram.ieYPos(lTop), False);
  end;

procedure TcdSimpleClass.SaveToJson(aJsonObject: TJsonObject);
  begin
  inherited SaveToJson(aJsonObject);

  aJsonObject.AddPair('name', fName);
  aJsonObject.AddPair('left', TJsonIntegerNumber.Create(IntLeft));
  aJsonObject.AddPair('top', TJsonIntegerNumber.Create(IntTop));
  end;

procedure TcdSimpleClass.LoadFromData(aSyncID: Integer);
 var
   lElement: TcElement;
 begin
 lElement := TcdClassDiagram(Diagram).Engine.Elements.Find(aSyncID);
 Assert(lElement is TcLabelType);
 LoadFromObject(lElement as TcLabelType);
 end;

procedure TcdSimpleClass.LoadFromObject(aLabelType: TcLabelType);
  begin
  SyncID := aLabelType.ID;
  Name := aLabelType.Name;
  Module := 0; // Not available in Engine

  if Assigned(Parent)
     then begin
          InitChildren;
          CorrectZOrder;
          end;
  end;

procedure TcdSimpleClass.Link;
  var
    t, r, a: Integer;
    lDiagram: TcdClassDiagram;
    lAttribute: TcdAttribute;
    lDetailComplexClass: TcdComplexClass;
    lOldLines: TcList;
    lLine: TcdLine;

    lType: TcType;
    lRelationType: TcRelationType;
    lRole: TcRole;
    lLabelType: TcLabelType;
    lClass: TcClass;
  begin
  lDiagram := TcdClassDiagram(Diagram);

  // -- Create list with current lines
  lOldLines := TcList.Create;
  lOldLines.Assign(Lines);

  // Find Types with Roles referencing this SimpleClass
  for t := 0 to lDiagram.Engine.ConceptualTypes.Count-1 do
    begin
    lType := lDiagram.Engine.ConceptualTypes[t];
    if lType is TcLabelType
       then continue;
    lRelationType := lType as TcRelationType;

    for r := 0 to lRelationType.Roles.Count-1 do
      begin
      lRole := lRelationType.Roles[r];
      if not (lRole.Type_ is TcLabelType)
         then continue; // Ignore referencing Roles

      lLabelType := lRole.Type_ as TcLabelType;
      if lLabelType.ID <> fSyncID
         then continue; // Ignore Roles referencing other LabelTypes/SimpleClasses

      // We have a Role, referencing this SimpleClass

      // Is the referencing Type displayed in the diagram?
      lDetailComplexClass := TcdComplexClass(lDiagram.Classes.Find(lRelationType.ID));
      if Assigned(lDetailComplexClass)
         then begin
              // Referencing Type is on the diagram
              for a := 0 to lDetailComplexClass.Attributes.Count-1 do
                begin
                lAttribute := TcdAttribute(lDetailComplexClass.Attributes[a]);

                // Determine existing link
                if lAttribute.RefersTo = fSyncID
                   then lLine := lDetailComplexClass.ConnectedToMaster(Self, lAttribute.SyncID)
                   else lLine := nil;

                if (lAttribute.RefersTo = fSyncID) and not Assigned(lLine)
                   then lDetailComplexClass.ConnectToMaster(Self, lAttribute)
                else if Assigned(lLine)
                   then begin
                        // I left this code, description is old, fenomenon might still occur

                        // Why this fuzzy check:
                        // We check on foreign keys, normally you should check for the foreign keys attribute,
                        // this means joining over Mat$Index and Mat$IndexColumn. Because we don't do this
                        // all lines between the two objects will pass for a single foreign key.
                        // If there are two foreign keys in one object, the lines will already be removed when the
                        // second foreign key is handled.
                        if lOldLines.IndexOf(lLine)>=0
                           then begin
                                lOldLines.Remove(lLine);
                                lDetailComplexClass.ReconnectToMaster(lLine, Self, lAttribute);
                                end;
                        end;
                end;
              end;
      end;
    end;

  // Find Classes with Roles referencing this SimpleClass
  for t := 0 to lDiagram.Engine.Classes.Count-1 do
    begin
    lClass := lDiagram.Engine.Classes[t];
    for r := 0 to lClass.Roles.Count-1 do
      begin
      lRole := lClass.Roles[r];
      if not (lRole.Type_ is TcLabelType)
         then continue; // Ignore referencing Roles

      lLabelType := lRole.Type_ as TcLabelType;
      if lLabelType.ID <> fSyncID
         then continue; // Ignore Roles referencing other LabelTypes/SimpleClasses

      // We have a Role, referencing this SimpleClass

      // Is the referencing Type displayed in the diagram?
      lDetailComplexClass := TcdComplexClass(lDiagram.Classes.Find(lClass.ID));
      if Assigned(lDetailComplexClass)
          then begin
               // Referencing Type is on the diagram
               for a := 0 to lDetailComplexClass.Attributes.Count-1 do
                 begin
                 lAttribute := TcdAttribute(lDetailComplexClass.Attributes[a]);

                 // Determine existing link
                 if lAttribute.RefersTo = fSyncID
                    then lLine := lDetailComplexClass.ConnectedToMaster(Self, lAttribute.SyncID)
                    else lLine := nil;

                 if (lAttribute.RefersTo = fSyncID) and not Assigned(lLine)
                    then lDetailComplexClass.ConnectToMaster(Self, lAttribute)
                 else if Assigned(lLine)
                    then begin
                         // I left this code, description is old, fenomenon might still occur

                         // Why this fuzzy check:
                         // We check on foreign keys, normally you should check for the foreign keys attribute,
                         // this means joining over Mat$Index and Mat$IndexColumn. Because we don't do this
                         // all lines between the two objects will pass for a single foreign key.
                         // If there are two foreign keys in one object, the lines will already be removed when the
                         // second foreign key is handled.
                         if lOldLines.IndexOf(lLine)>=0
                            then begin
                                 lOldLines.Remove(lLine);
                                 lDetailComplexClass.ReconnectToMaster(lLine, Self, lAttribute);
                                 end;
                         end;
                 end;
               end;
      end;
    end;

  // -- Remove unused old lines
  for t := lOldLines.Count-1 downto 0 do
    if TcdLine(lOldLines[t]).FromObject=Self
       then lOldLines.KillAt(t);

  lOldLines.RemoveAll;
  FreeAndNil(lOldLines);
  end;

procedure TcdSimpleClass.PaintOnCanvas(aCanvas: TCanvas);
  var
     lStyle: TcdClassStyle;
  begin
  lStyle := TcdClassStyle(Diagram.Style);
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Brush_Style := bsSolid;
    if Selected
       then Brush_Color := lStyle.SimpleClassFillColor_Selected
       else Brush_Color := lStyle.SimpleClassFillColor;

    case lStyle.SimpleClassShape of
      scsRect:
         begin
         FillRect(IntClientRect);

         Pen_Style := psDot;
         Pen_Color := lStyle.SimpleClassPenColor;
         Rectangle(IntClientRect);
         end;

      scsRoundRect:
         begin
         FillRect(IntClientRect);

         // Draw the correct round rect
         Pen_Style := psDot;
         Pen_Color := lStyle.SimpleClassPenColor;
         PolyBezier(fPoints);
         end;

      scsCircle:
         begin
         Pen_Style := psDot;
         Pen_Color := lStyle.SimpleClassPenColor;
         Ellipse(0,0,IntWidth,IntHeight);
         end;
      end;
    end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdComplexClass
//

procedure TcdComplexClass.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer);
  begin
  if aButton = mbRight
     then TcdClassDiagram(Diagram).DoComplexClassPopupMenu(aX, aY, Self)
     else inherited MouseDown (aButton, aShiftState, aX, aY);
  end;

procedure TcdComplexClass.DblClick;
  begin
  inherited;
  if ControlPressed
     then TcdClassDiagram(Diagram).ComplexClass_DefaultForm(SyncID)
     else TcdClassDiagram(Diagram).EditComplexClass(SyncID);
  end;

procedure TcdComplexClass.ResetBounds_Internal;
  var
    T, X, Y, lWidth, lHeight,
    lAttributeWidth, lTypeNameWidth, lRuleStrWidth,
    lCenter, lSafetyMargin: Integer;
    lStyle: TcdClassStyle;
    lAttribute: TcdAttribute;
  begin
  inherited;
  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := nil;

    if lStyle.AttributeOrientation = aoVertical
       then begin
            // Set font
            Font_Name := lStyle.ClassFontname;
            Font_Height := lStyle.ClassFontHeight;
            Font_Style := lStyle.ClassFontStyle;

            // Calculate attribute frame height
            fAttributeFrameHeight := TextHeight(cHeightDeterminationStr) + lStyle.AttributeFrameVerticalMargin*2;

            // Calculate the nr of attributes
            fNrAttributes := 0;
            lAttributeWidth := 0;
            lTypeNameWidth := 0;
            lRuleStrWidth := 0;
            for T := 0 to fAttributes.Count-1 do
              begin
              lAttribute := TcdAttribute(fAttributes[T]);
              if (not lStyle.DisplayReferencingAttributes) and (lAttribute.Type_='C')
                 then begin
                      lAttribute.RelativeNr := -1;
                      end
                 else begin
                      lAttribute.RelativeNr := fNrAttributes;
                      Inc(fNrAttributes);
                      lAttributeWidth := Max(lAttributeWidth, TextWidth(lAttribute.DisplayName));
                      lTypeNameWidth := Max(lTypeNameWidth, TextWidth(lStyle.AttributeTypeName_StartSymbol + lAttribute.TypeName + lStyle.AttributeTypeName_EndSymbol));
                      lRuleStrWidth := Max(lRuleStrWidth, TextWidth(lAttribute.DeriveRuleStr));
                      end;
              end;
            lWidth := lStyle.AttributeFrameHorizontalMargin + lAttributeWidth;

            // Should we create a column for the referstonames?
            if lStyle.DisplayAttributeTypeNames
               then begin
                    fXPos_AttributeTypeName := lWidth + TextWidth(cColumnSeparatorStr);
                    lWidth := lWidth + TextWidth(cColumnSeparatorStr) + lTypeNameWidth;
                    end;

            // Create a column for the ORWIndicator?
            if lRuleStrWidth>0
               then begin
                    fXPos_RuleStr := lWidth + TextWidth(cColumnSeparatorStr);
                    lWidth := lWidth + TextWidth(cColumnSeparatorStr) + lRuleStrWidth;
                    end;

            lWidth := lWidth + lStyle.AttributeFrameHorizontalMargin;

            // Set title font
            Font_Name := lStyle.ComplexClassTitleFontname;
            Font_Height := lStyle.ComplexClassTitleFontHeight;
            Font_Style := lStyle.ComplexClassTitleFontStyle;

            // Determine final width
            lWidth := Max(lWidth, TextWidth(DisplayName) + lStyle.ComplexClassTitleFrameHorizontalMargin*2);

            // Calculate title frame height
            fTitleFrameHeight := TextHeight(DisplayName) + lStyle.ComplexClassTitleFrameVerticalMargin*2;

            // Determine final height
            lHeight := fTitleFrameHeight + (fNrAttributes*fAttributeFrameHeight) + lStyle.ScaleMargin;

            if fNrAttributes=0
               then lHeight := lHeight + fAttributeFrameHeight;
            end
       else begin
            // Fixed height
            lHeight := lStyle.AttributeFrameHeight;

            // Calculate the nr of attributes
            fNrAttributes := 0;
            for T := 0 to fAttributes.Count-1 do
              begin
              lAttribute := TcdAttribute(fAttributes[T]);
              if (not lStyle.DisplayReferencingAttributes) and (lAttribute.Type_='C')
                 then begin
                      lAttribute.RelativeNr := -1;
                      end
                 else begin
                      lAttribute.RelativeNr := fNrAttributes;
                      Inc(fNrAttributes);
                      end;
              end;

            // Width
            lWidth := Attributes.TotalWidth;

            if fNrAttributes=0
               then begin
                    lWidth := lHeight;
                    dConnectorFrame.Clickable := True;
                    end
               else dConnectorFrame.Clickable := False;
            end;
    end;

  // Set the internal bounds
  SetIntBounds(IntLeft, IntTop, lWidth, lHeight);

  // Derive number of rules and relative rule numbers
  IntraRules.Derive;

  // Create connection points
  DeleteConnectionPoints;

  lSafetyMargin := Diagram.Style.ClawWidth;

  lCenter := Diagram.SnapToGridX(lWidth DIV 2);
  X := lCenter;
  while X <= lWidth-lSafetyMargin do
    begin
    AddConnectionPoint(X, 0);
    AddConnectionPoint(X, IntHeight);
    AddConnectionPoint(ConnectionPoints_Top, X, 0);
    AddConnectionPoint(ConnectionPoints_Bottom, X, IntHeight);
    if X<>lCenter
       then begin
            AddConnectionPoint(lCenter-(X-lCenter), 0);
            AddConnectionPoint(lCenter-(X-lCenter), IntHeight);
            AddConnectionPoint(ConnectionPoints_Top, lCenter-(X-lCenter), 0);
            AddConnectionPoint(ConnectionPoints_Bottom, lCenter-(X-lCenter), IntHeight);
            end;
    Inc(X, lStyle.GridX);
    end;

  lCenter := Diagram.SnapToGridY(lHeight DIV 2);
  Y := lCenter;
  while Y <= lHeight-lSafetyMargin do
    begin
    AddConnectionPoint(0, Y);
    AddConnectionPoint(IntWidth, Y);
    if Y<>lCenter
       then begin
            AddConnectionPoint(0, lCenter-(Y-lCenter));
            AddConnectionPoint(IntWidth, lCenter-(Y-lCenter));
            end;
    Inc(Y, lStyle.GridY);
    end;

  // TitleFrameSide
  if lStyle.AttributeOrientation = aoVertical
     then begin
          AddConnectionPoint(ConnectionPoints_TitleFrameSide, 0, Diagram.SnapToGridY(fTitleFrameHeight DIV 2));
          AddConnectionPoint(ConnectionPoints_TitleFrameSide, IntWidth, Diagram.SnapToGridY(fTitleFrameHeight DIV 2));

          // Extra connection points to prevent long lines
          for T := 0 to ConnectionPoints_Top.Count-1 do
             AddConnectionPoint(ConnectionPoints_TitleFrameSide, TcdPoint(ConnectionPoints_Top[T]).X, TcdPoint(ConnectionPoints_Top[T]).Y);

          // Prevent connection of normal lines to the bottom of a title frame
          // for T := 0 to ConnectionPoints_Bottom.Count-1 do
          //    AddConnectionPoint(ConnectionPoints_TitleFrameSide, TcdPoint(ConnectionPoints_Bottom[T]).X, TcdPoint(ConnectionPoints_Bottom[T]).Y);
          end
     else begin
          for T := 0 to ConnectionPoints_Bottom.Count-1 do
             AddConnectionPoint(ConnectionPoints_TitleFrameSide, TcdPoint(ConnectionPoints_Bottom[T]).X, TcdPoint(ConnectionPoints_Bottom[T]).Y);
          end;
  end;

procedure TcdComplexClass.ResetBounds_Children;
  var
     T: Integer;
  begin
  inherited;

  for T := 0 to Attributes.Count-1 do
     TcdAttribute(Attributes[T]).ResetBounds;

  LowestRuleTop := IntTop;
  for T := 0 to IntraRules.Count-1 do
     TcdRule(IntraRules[T]).ResetBounds;

  dTitle.ResetBounds;
  dConnectorFrame.ResetBounds;
  end;

procedure TcdComplexClass.Changed;
  begin
  inherited;
  dConnectorFrame.Changed;
  fdTitle.Changed;
  end;

procedure TcdComplexClass.CorrectZOrder;
  var
     T: Integer;
  begin
  // Lines
  for T := 0 to Lines.Count-1 do
     TcdLine(Lines[T]).CorrectZOrder;

  // Self
  inherited CorrectZOrder;

  // Contents
  dTitle.BringToFront;
  for T := 0 to Attributes.Count-1 do
     TcdAttribute(Attributes[T]).BringToFront;
  for T := 0 to IntraRules.Count-1 do
     TcdRule(IntraRules[T]).BringToFront;
  dConnectorFrame.BringToFront;
  end;

function TcdComplexClass.GetConnectionPoint(aLine: TcdLine; aPoint: TPoint): TPoint;
  var
    T: Integer;
    lStyle: TcdClassStyle;
    lAttribute: TcdAttribute;
  begin
  result := Default(TPoint);
  lStyle := TcdClassStyle(Diagram.Style);

  if aLine is TcdSubtypeLine
     then begin
          // ORM style, draw a line from connector frame to connector frame
          if lStyle.ConnectorFrame
             then begin
                  if lStyle.ConnectorFrameStyle=cfsEllipse
                     then Result := dConnectorFrame.GetConnectionPoint(aLine, aPoint)
                     else Result := GetConnectionPointFromList(dConnectorFrame, dConnectorFrame.ConnectionPoints, aPoint)
                  end

          // UML style, draw a line from the top of the title bar to the bottom of the class
          else if lStyle.AttributeOrientation = aoVertical
             then begin
                  if aLine.FromObject=Self
                     then Result := GetConnectionPointFromList(Self, ConnectionPoints_Top, aPoint)
                     else Result := GetConnectionPointFromList(Self, ConnectionPoints_Bottom, aPoint)
                  end

          // Record style
          else Result := GetConnectionPointFromList(Self, ConnectionPoints, aPoint);
          end

     else begin
          case lStyle.ReferenceStyle of
            rsClassToClass:
               begin
               Result := GetConnectionPointFromList(Self, ConnectionPoints, aPoint);
               end;

            rsAttributeToClass:
               begin
               if aLine.FromObject=Self
                  then begin
                       // Start point is the attribute it selve
                       lAttribute := nil;
                       for T := 0 to Attributes.Count-1 do
                          if TcdAttribute(Attributes[T]).SyncID=TcdReferenceLineData(aLine.ConData).AttributeSyncID
                             then begin
                                  lAttribute := TcdAttribute(Attributes[T]);
                                  Break;
                                  end;

                       if Assigned(lAttribute)
                          then Result := GetConnectionPointFromList(lAttribute, lAttribute.ConnectionPoints, aPoint)
                          else Result := GetConnectionPointFromList(Self, ConnectionPoints, aPoint);
                       end
                  else begin
                       // ORM Style, the end point is on the connector frame
                       if lStyle.ConnectorFrame
                          then begin
                               if lStyle.ConnectorFrameStyle=cfsEllipse
                                  then Result := dConnectorFrame.GetConnectionPoint(aLine, aPoint)
                                  else Result := GetConnectionPointFromList(dConnectorFrame, dConnectorFrame.ConnectionPoints, aPoint);
                               end

                       // UML Style, the end point is on the side of the title frame
                          else Result := GetConnectionPointFromList(Self, ConnectionPoints_TitleFrameSide, aPoint);
                       end;
               end;
            end;
          end;
  end;

function TcdComplexClass.GetDisplayName: string;
  var
    lStyle: TcdClassStyle;
  begin
  lStyle := TcdClassStyle(Diagram.Style);
  if Derivable='Y'
     then begin
          if lStyle.DerivableIndicatorPosition=hpLeft
             then Result := lStyle.DerivableIndicator + Name
             else Result := Name + lStyle.DerivableIndicator;
          end
  else if Derivable='I'
     then begin
          if lStyle.DerivableIndicatorPosition=hpLeft
             then Result := lStyle.InitialDerivableIndicator + Name
             else Result := Name + lStyle.InitialDerivableIndicator;
          end
     else Result := Name;
  end;

function TcdComplexClass.IsStartForSubtypeLine: Boolean;
  var
    T: Integer;
  begin
  Result := True;
  for T := 0 to Lines.Count-1 do
    begin
    if (Lines[T] is TcdSubtypeLine) and (TcdLine(Lines[T]).FromObject = Self)
       then Exit;
    end;
  Result := False;
  end;

constructor TcdComplexClass.Create(aDiagram: TcdDiagram; aType: TcdClassType);
  var
    lDiagram: TcdClassDiagram;
  begin
  inherited Create(aDiagram, ooUpperLeft);

  lDiagram := TcdClassDiagram(aDiagram);

  fSyncID := -1;
  fType := aType;

  fAttributes := TcdAttributeList.Create(lDiagram);
  fIntraRules := TcdRuleList.Create;

  fdTitle := TcdComplexClassTitle.Create(Diagram, Self);
  fdConnectorFrame := TcdComplexClassConnectorFrame.Create(Diagram, Self);

  fConnectionPoints_Top := TcList.Create;
  fConnectionPoints_Bottom := TcList.Create;
  fConnectionPoints_TitleFrameSide := TcList.Create;

  Diagram.Add(fdConnectorFrame);

  end;

destructor TcdComplexClass.Destroy;
  begin
  if SyncID<>-1
     then TcdClassDiagram(Diagram).Classes.Remove(Self);

  FreeAndNil(fdConnectorFrame);
  FreeAndNil(fdTitle);
  FreeAndNil(fAttributes);
  FreeAndNil(fIntraRules);

  FreeAndNil(fConnectionPoints_Top);
  FreeAndNil(fConnectionPoints_Bottom);
  FreeAndNil(fConnectionPoints_TitleFrameSide);

  inherited Destroy;
  end;

procedure TcdComplexClass.DeleteConnectionPoints;
  begin
  inherited;
  ConnectionPoints_Top.KillAll;
  ConnectionPoints_Bottom.KillAll;
  ConnectionPoints_TitleFrameSide.KillAll;
  end;

procedure TcdComplexClass.InitChildren;
  var
    T: Integer;
  begin
  Diagram.Add(fdTitle);
  for T := 0 to Attributes.Count-1 do
    Diagram.Add(TcdAttribute(Attributes[T]));
  for T := 0 to IntraRules.Count-1 do
    Diagram.Add(TcdRule(IntraRules[T]));
  end;

procedure TcdComplexClass.LoadFromJson(aJsonObject: TJsonObject);
  var
    lLeft, lTop: Integer;
    lRelationType: TcRelationType;
  begin
  inherited LoadFromJson(aJsonObject);

  if not aJsonObject.TryGetValue('name', fName) then InvalidJson_xExpected('name');
  if not aJsonObject.TryGetValue('left', lLeft) then InvalidJson_xExpected('left');
  if not aJsonObject.TryGetValue('top', lTop) then InvalidJson_xExpected('top');

  lRelationType := nil;
  case Type_ of
    ctClaimType:
      lRelationType := TcdClassDiagram(Diagram).Engine.ConceptualTypes.FindClaimType(fName);

    ctAnnotationType:
      lRelationType := TcdClassDiagram(Diagram).Engine.ConceptualTypes.FindAnnotationType(fName);

    ctClass:
      lRelationType := TcdClassDiagram(Diagram).Engine.Classes.Find(fName);

    else EInt('TcdComplexClass.LoadFromJson', 'Unexpected type');
  end;

  if not Assigned(lRelationType) then exit;
  LoadFromObject(lRelationType);

  Diagram.Add(Self, Diagram.ieXPos(lLeft), Diagram.ieYPos(lTop), False);
  end;

procedure TcdComplexClass.SaveToJson(aJsonObject: TJsonObject);
  begin
  inherited SaveToJson(aJsonObject);

  aJsonObject.AddPair('name', fName);
  aJsonObject.AddPair('left', TJsonIntegerNumber.Create(IntLeft));
  aJsonObject.AddPair('top', TJsonIntegerNumber.Create(IntTop));
  end;

procedure TcdComplexClass.LoadFromData(aSyncID: Integer);
 var
   lElement: TcElement;
 begin
 lElement := TcdClassDiagram(Diagram).Engine.Elements.Find(aSyncID);
 Assert(lElement is TcRelationType);
 LoadFromObject(lElement as TcRelationType);
 end;

procedure TcdComplexClass.LoadFromObject(aRelationType: TcRelationType);
 var
   r, i: Integer;
   lRole: TcRole;
   lAttribute: TcdAttribute;
   lRule: TcdRule;
 begin
 SyncID := aRelationType.ID;
 Name := aRelationType.Name;
 Derivable := 'N'; // Not available in Engine
 Module := 0; // Not available in Engine

 // -- Attributes
 Attributes.KillAll;
 for r := 0 to aRelationType.Roles.Count-1 do
   begin
   lRole := aRelationType.Roles[r];

   lAttribute := TcdAttribute.Create(Diagram, Self);
   lAttribute.SyncID := lRole.ID;
   lAttribute.Name := lRole.Name;
   case lRole.Necessity of
     mUnknown: lAttribute.ORWIndicator := 'R';
     mCouldHave: lAttribute.ORWIndicator := 'O';
     mShouldHave: lAttribute.ORWIndicator := 'W';
     mMustHave: lAttribute.ORWIndicator := 'R';
     else EInt('TcdComplexClass.LoadFromObject', 'Unknown TcMoSCoW value.');
     end;

   lAttribute.Module := 0; // Not available in Engine
   if not Assigned(lRole.Type_)
     then begin
          lAttribute.RefersTo := -1;
          lAttribute.TypeName := '';
          lAttribute.Type_ := 'S';
          lAttribute.Derivable := 'N';
          end
   else if (lRole.Type_ is TcLabelType)
     then begin
          lAttribute.RefersTo := lRole.Type_.ID;
          lAttribute.TypeName := lRole.Type_.Name;
          lAttribute.Type_ := 'S';
          lAttribute.Derivable := 'N';
          end
     else begin
          lAttribute.RefersTo := lRole.Type_.ID;
          lAttribute.TypeName := lRole.Type_.Name;
          lAttribute.Type_ := 'C';
          lAttribute.Derivable := 'N';
          end;
   Attributes.Add(lAttribute);
   end;

 // -- Rules
 // Intra rules are not compared, they are simply totally reloaded
 for r := 0 to Attributes.Count-1 do
    TcdAttribute(Attributes[r]).IntraRules.RemoveAll;
 IntraRules.KillAll;

 if Assigned(aRelationType.Identity)
    then begin
         lRule := TcdClassDiagram(Diagram).Rule_CreateAndLoadFromObject(Self, aRelationType, ctIdentification, aRelationType.Identity);
         IntraRules.Add(lRule);
         end;

 for i := 0 to aRelationType.UnicityList.Count-1 do
   begin
   lRule := TcdClassDiagram(Diagram).Rule_CreateAndLoadFromObject(Self, aRelationType, ctUnicity, aRelationType.UnicityList[i]);
   IntraRules.Add(lRule);
   end;

 for r := 0 to aRelationType.Roles.Count-1 do
   begin
   if aRelationType.Roles[r].IsTotalReference
      then begin
           lRule := TcdClassDiagram(Diagram).Rule_CreateAndLoadFromObject(Self, aRelationType, ctTotality, nil, aRelationType.Roles[r]);
           IntraRules.Add(lRule);
           end;
   end;

 if Assigned(Parent)
    then begin
         InitChildren;
         CorrectZOrder;
         end;
 end;

procedure TcdComplexClass.Link;
  var
    i, t, r, a: Integer;
    lDiagram: TcdClassDiagram;
    lAttribute: TcdAttribute;
    lMasterClass: TcdBaseClass;
    lDetailClass: TcdComplexClass;
    lSubtypeClass, lSupertypeClass: TcdComplexClass;
    lOldLines: TcList;
    lLine: TcdLine;

    lRelationType: TcRelationType;
    lRole: TcRole;
    lReferencedRelationType: TcRelationType;
    lElement: TcElement;
  begin
  lDiagram := TcdClassDiagram(Diagram);

  // -- Create list with current lines
  lOldLines := TcList.Create;
  lOldLines.Assign(Lines);

  // -- Check links from Attributes in this ComplexClass to other Classes
  for i := 0 to Attributes.Count-1 do
    begin
    lAttribute := TcdAttribute(Attributes[i]);

    if lAttribute.RefersTo > 0
       then begin
            // Is the referenced Class displayed in the diagram?
            lMasterClass := lDiagram.Classes.Find(lAttribute.RefersTo);

            // Determine existing link
            if Assigned(lMasterClass)
               then lLine := ConnectedToMaster(lMasterClass, lAttribute.SyncID)
               else lLine := nil;

            // Should there be a link?
            if Assigned(lMasterClass) and not Assigned(lLine)
               then ConnectToMaster(lMasterClass, lAttribute)
            else if Assigned(lLine)
                    then begin
                         lOldLines.Remove(lLine);
                         ReconnectToMaster(lLine, lMasterClass, lAttribute)
                         end;
            end;
    end;

   // -- Check links from other Types referencing this ComplexClass

   // Find RelationTypes with Roles referencing this ComplexClass
   for t := 0 to lDiagram.Engine.RelationTypes.Count-1 do
     begin
     lRelationType := lDiagram.Engine.RelationTypes[t];

     for r := 0 to lRelationType.Roles.Count-1 do
       begin
       lRole := lRelationType.Roles[r];
       if lRole.Type_ is TcLabelType
          then continue; // Ignore LabelTypes

       lReferencedRelationType := lRole.Type_ as TcRelationType;
       if lReferencedRelationType.ID <> fSyncID
          then continue; // Ignore Roles referencing other ComplexTypes/ComplexClasses

       // We have a role referencing this ComplexClass

       // Is the referencing Class displayed in the diagram?
       lDetailClass := TcdComplexClass(lDiagram.Classes.Find(lRelationType.ID));
       if Assigned(lDetailClass)
          then begin
               for a := 0 to lDetailClass.Attributes.Count-1 do
                 begin
                 lAttribute := TcdAttribute(lDetailClass.Attributes[a]);

                 // Determine existing link
                 if lAttribute.RefersTo = fSyncID
                    then lLine := ConnectedToDetail(lDetailClass, lAttribute.SyncID)
                    else lLine := nil;

                 if (lAttribute.RefersTo = fSyncID) and not Assigned(lLine)
                    then ConnectToDetail(lDetailClass, lAttribute)
                 else if Assigned(lLine)
                         then begin
                              //!! I left this code, description is old, fenomenon might still occur

                              // Why this fuzzy check:
                              // We check on foreign keys, normally you should check for the foreign keys attribute,
                              // this means joining over Mat$Index and Mat$IndexColumn. Because we don't do this
                              // all lines between the two objects will pass for a single foreign key.
                              // If there are two foreign keys in one object, the lines will already be removed when the
                              // second foreign key is handled.
                              if lOldLines.IndexOf(lLine)>=0
                                 then begin
                                      lOldLines.Remove(lLine);
                                      ReconnectToDetail(lLine, lDetailClass, lAttribute);
                                      end;
                              end;
                 end;
               end;
       end;
     end;

  // Check Subtype lines for Classes
  lElement := lDiagram.Engine.Elements.Find(fSyncID);
  if lElement is TcClass
     then begin
          // -- Find relations between this class and it's Supertype
          lRelationType := lElement as TcRelationType;
          if Assigned(lRelationType.SuperType)
             then begin
                  // Is the Supertype displayed in the diagram?
                  lSupertypeClass := TcdComplexClass(lDiagram.Classes.Find(lRelationType.SuperType.ID));
                  if Assigned(lSupertypeClass)
                     then begin
                          // Determine existing link
                          lLine := ConnectedToSupertype(lSupertypeClass);
                          if Assigned(lLine)
                             then begin
                                  lOldLines.Remove(lLine);
                                  ReconnectToSupertype(lLine, lSupertypeClass);
                                  end
                             else ConnectToSupertype(lSupertypeClass);
                          end;
                  end;

          // -- Find relations between this class and it's subtypes
          for t := 0 to lDiagram.Engine.RelationTypes.Count-1 do
            begin
            lRelationType := lDiagram.Engine.RelationTypes[t];
            if Assigned(lRelationType.SuperType) and (lRelationType.SuperType.ID=fSynciD)
               then begin
                    // Is the subtype displayed in the diagram?
                    lSubtypeClass := TcdComplexClass(lDiagram.Classes.Find(lRelationType.ID));
                    if Assigned(lSubtypeClass)
                       then begin
                            // Determine existing link
                            lLine := ConnectedToSubtype(lSubtypeClass);
                            if Assigned(lLine)
                               then begin
                                    lOldLines.Remove(lLine);
                                    ReconnectToSubtype(lLine, lSubtypeClass);
                                    end
                               else ConnectToSubtype(lSubtypeClass);
                            end;
                    end;
            end;
     end;

  // -- Find relations between inter-class rules
  // NYI

  // -- Remove unused old lines
  for t := lOldLines.Count-1 downto 0 do
    if TcdLine(lOldLines[t]).FromObject=Self
       then lOldLines.KillAt(t);

  lOldLines.RemoveAll;
  FreeAndNil(lOldLines);
  end;

function TcdComplexClass.ConnectToMaster(aMaster: TcdBaseClass; aDetailAttribute: TcdAttribute): TcdReferenceLine;
  var
    lNewLine: TcdReferenceLine;
    lNewLineData: TcdReferenceLineData;
  begin
  lNewLineData := TcdReferenceLineData.Create;
  lNewLine := TcdReferenceLine.Create(Diagram, Self, aMaster, lNewLineData);
  lNewLineData.Init(lNewLine);

  lNewLine.LoadFromData;
  lNewLineData.LoadFromData(aDetailAttribute);

  if aMaster is TcdComplexClass
     then begin
          lNewLine.SetCardinality(aMaster, aDetailAttribute.GetORWIndicatorAsCardinality, 1);
          if aDetailAttribute.HasSingleAttributeTotalityRule
             then lNewLine.SetCardinality(Self, 1, cCardMany)
             else lNewLine.SetCardinality(Self, 0, cCardMany);
          end;

  lNewLine.SetAttributeName;

  AddLine(lNewLine);
  aMaster.AddLine(lNewLine);
  lNewLine.BringToFront;

  aMaster.Changed;
  Result := lNewLine;
  end;

function TcdComplexClass.ConnectToDetail(aDetail: TcdComplexClass; aDetailAttribute: TcdAttribute): TcdReferenceLine;
  begin
  Result := aDetail.ConnectToMaster(Self, aDetailAttribute);
  end;

function TcdComplexClass.ConnectToSupertype(aSupertype: TcdComplexClass): TcdSubtypeLine;
  var
    lNewLine: TcdSubtypeLine;
  begin
  lNewLine := TcdSubtypeLine.Create(Diagram, Self, aSupertype, nil);
  lNewLine.LoadFromData;
  AddLine(lNewLine);
  aSupertype.AddLine(lNewLine);
  lNewLine.BringToFront;
  Result := lNewLine;
  end;

function TcdComplexClass.ConnectToSubtype(aSubtype: TcdComplexClass): TcdSubtypeLine;
  begin
  Result := aSubtype.ConnectToSuperType(Self);
  end;

function TcdComplexClass.ReconnectToMaster(aOldLine: TcdLine; aMaster: TcdBaseClass; aDetailAttribute: TcdAttribute): TcdReferenceLine;
  var
    lNewLine: TcdReferenceLine;
  begin
  lNewLine := ConnectToMaster(aMaster, aDetailAttribute);
  lNewLine.CopyConnectors(aOldLine);
  aOldLine.Free;
  Result := lNewLine;
  end;

function TcdComplexClass.ReconnectToDetail(aOldLine: TcdLine; aDetail: TcdComplexClass; aDetailAttribute: TcdAttribute): TcdReferenceLine;
  begin
  Result := aDetail.ReconnectToMaster(aOldLine, Self, aDetailAttribute);
  end;

function TcdComplexClass.ReconnectToSupertype(aOldLine: TcdLine; aSupertype: TcdComplexClass): TcdSubtypeLine;
  var
    lNewLine: TcdSubtypeLine;
  begin
  lNewLine := ConnectToSupertype(aSupertype);
  lNewLine.CopyConnectors(aOldLine);
  aOldLine.Free;
  Result := lNewLine;
  end;

function TcdComplexClass.ReconnectToSubtype(aOldLine: TcdLine; aSubtype: TcdComplexClass): TcdSubtypeLine;
  begin
  Result := aSubtype.ReconnectToSuperType(aOldLine, Self);
  end;

function TcdComplexClass.ConnectedToMaster(aMaster: TcdBaseClass; aDetailAttributeSyncID: Integer): TcdReferenceLine;
  var
    T: Integer;
    lLine: TcdLine;
  begin
  Result := nil;
  for T := 0 to Lines.Count-1 do
    begin
    lLine := TcdLine(Lines[T]);
    if (lLine is TcdReferenceLine) and (lLine.ToObject=aMaster) and (TcdReferenceLineData(lLine.ConData).AttributeSyncID=aDetailAttributeSyncID)
       then begin
            Result := TcdReferenceLine(lLine);
            Exit;
            end;
    end;
  end;

function TcdComplexClass.ConnectedToDetail(aDetail: TcdComplexClass; aDetailAttributeSyncID: Integer): TcdReferenceLine;
  var
    T: Integer;
    lLine: TcdLine;
  begin
  Result := nil;
  for T := 0 to Lines.Count-1 do
    begin
    lLine := TcdLine(Lines[T]);
    if (lLine is TcdReferenceLine) and (lLine.FromObject=aDetail) and (TcdReferenceLineData(lLine.ConData).AttributeSyncID=aDetailAttributeSyncID)
       then begin
            Result := TcdReferenceLine(lLine);
            Exit;
            end;
    end;
  end;

function TcdComplexClass.ConnectedToSupertype(aSupertype: TcdComplexClass): TcdSubtypeLine;
  var
    T: Integer;
    lLine: TcdLine;
  begin
  Result := nil;
  for T := 0 to Lines.Count-1 do
    begin
    lLine := TcdLine(Lines[T]);
    if (lLine is TcdSubtypeLine) and (lLine.ToObject=aSupertype)
       then begin
            Result := TcdSubtypeLine(lLine);
            Exit;
            end;
    end;
  end;

function TcdComplexClass.ConnectedToSubtype(aSubtype: TcdComplexClass): TcdSubtypeLine;
  var
    T: Integer;
    lLine: TcdLine;
  begin
  Result := nil;
  for T := 0 to Lines.Count-1 do
    begin
    lLine := TcdLine(Lines[T]);
    if (lLine is TcdSubtypeLine) and (lLine.FromObject=aSubtype)
       then begin
            Result := TcdSubtypeLine(lLine);
            Exit;
            end;
    end;
  end;

function TcdComplexClass.GetObjectSide(aLine: TcdLine; aPoint: TPoint): TcdObjectSide;
  var
    lStyle: TcdClassStyle;
  begin
  Result := osNone;

  lStyle := TcdClassStyle(Diagram.Style);

  if aLine is TcdSubtypeLine
     then begin
          if lStyle.ConnectorFrame
             then Result := dConnectorFrame.GetObjectSide(aLine, aPoint)
             else Result := inherited GetObjectSide(aLine, aPoint);
          end
     else begin
          case lStyle.ReferenceStyle of
             rsClassToClass:
                begin
                if lStyle.ConnectorFrame
                   then Result := dConnectorFrame.GetObjectSide(aLine, aPoint)
                   else Result := inherited GetObjectSide(aLine, aPoint);
                end;

             rsAttributeToClass:
                begin
                if aLine.FromObject=Self
                   then Result := inherited GetObjectSide(aLine, aPoint)
                   else begin
                        if lStyle.ConnectorFrame
                           then Result := dConnectorFrame.GetObjectSide(aLine, aPoint)
                           else Result := inherited GetObjectSide(aLine, aPoint);
                        end;
                end;
             end;
          end;
  end;

procedure TcdComplexClass.PaintOnCanvas(aCanvas: TCanvas);
  var
    lStyle: TcdClassStyle;
  begin
  lStyle := TcdClassStyle(Diagram.Style);
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    if lStyle.AttributeOrientation = aoVertical
       then begin
            // Master frame
            Brush_Style := bsSolid;
            if Selected
               then Brush_Color := lStyle.ComplexClassFillColor_Selected
               else Brush_Color := lStyle.ComplexClassFillColor;
            FillRect(IntClientRect);

            Brush_Style := bsSolid;
            Brush_Color := lStyle.ComplexClassPenColor;
            FrameRect(IntClientRect);

            // Title Frame
            Font_Name := lStyle.ComplexClassTitleFontname;
            Font_Color := lStyle.ComplexClassTitleFontColor;
            Font_Height := lStyle.ComplexClassTitleFontHeight;
            Font_Style := lStyle.ComplexClassTitleFontStyle;

            Brush_Style := bsSolid;
            if Selected
               then Brush_Color := lStyle.ComplexClassTitleFrameFillColor_Selected
               else Brush_Color := lStyle.ComplexClassTitleFrameFillColor;
            FillRect(Classes.Rect(0, 0, IntWidth, fTitleFrameHeight));

            Brush_Style := bsSolid;
            Brush_Color := lStyle.ComplexClassPenColor;
            FrameRect(Classes.Rect(0, 0, IntWidth, fTitleFrameHeight));
            end
       else begin
            if (fNrAttributes=0) and not (Diagram.PaintOnExternal or Diagram.Readonly)
               then begin
                    // The ConnectorFrame is empty
                    if Selected
                       then begin
                            Brush_Style := bsSolid;
                            Brush_Color := lStyle.ComplexClassFillColor_Selected;
                            FillRect(Classes.Rect(0, 0, IntWidth-lStyle.ScaleMargin, IntHeight-lStyle.ScaleMargin));
                            end
                       else begin
                            Brush_Style := bsSolid;
                            Brush_Color := clWhite;
                            FillRect(Classes.Rect(0, 0, IntWidth-lStyle.ScaleMargin, IntHeight-lStyle.ScaleMargin));
                            end;

                    Brush_Style := bsClear;
                    Pen_Style := psDot;
                    Pen_Color := lStyle.ComplexClassPenColor;
                    Polygon([
                       Classes.Point(0, 0),
                       Classes.Point(IntWidth-lStyle.ScaleMargin, 0),
                       Classes.Point(IntWidth-lStyle.ScaleMargin, IntHeight-lStyle.ScaleMargin),
                       Classes.Point(0, IntHeight-lStyle.ScaleMargin)
                    ]);
                    end;
            end;
    end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdSimpleClassTitle
//
procedure TcdSimpleClassTitle.ResetBounds_Internal;
  var
    lLeft, lTop, lWidth, lHeight: Integer;
    lStyle: TcdClassStyle;
  begin
  inherited;

  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := nil;

    Font_Name := lStyle.SimpleClassTitleFontName;
    Font_Height := lStyle.SimpleClassTitleFontHeight;
    Font_Style := lStyle.SimpleClassTitleFontStyle;

    if lStyle.AutoSizeSimpleClass
       then begin
            lWidth := TextWidth(SimpleClass.DisplayName) + lStyle.SimpleClassTitleHorizontalMargin*2;
            lHeight := TextHeight(SimpleClass.DisplayName) + lStyle.SimpleClassTitleVerticalMargin*2;
            end
       else begin
            lWidth := TextWidth(SimpleClass.DisplayName) + lStyle.ScaleMargin;
            lHeight := TextHeight(SimpleClass.DisplayName) + lStyle.ScaleMargin;
            end;

    lLeft := SimpleClass.IntLeft + (SimpleClass.IntWidth DIV 2) - (lWidth DIV 2);
    lTop := SimpleClass.IntTop + (SimpleClass.IntHeight DIV 2) - (lHeight DIV 2);

    SetIntBounds(lLeft, lTop, lWidth, lHeight);
    end;
  end;

constructor TcdSimpleClassTitle.Create(aDiagram: TcdDiagram; aSimpleClass: TcdSimpleClass);
  begin
  inherited Create(aDiagram, ooAuto);
  ControlStyle := ControlStyle - [csOpaque];
  Clickable := False;
  IsEmbeddedObject := true;
  fSimpleClass := aSimpleClass;
  end;

procedure TcdSimpleClassTitle.PaintOnCanvas(aCanvas: TCanvas);
  var
    lStyle: TcdClassStyle;
  begin
  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Brush_Style := bsClear;
    Font_Name := lStyle.SimpleClassTitleFontName;
    Font_Height := lStyle.SimpleClassTitleFontHeight;
    Font_Color := lStyle.SimpleClassTitleFontColor;
    Font_Style := lStyle.SimpleClassTitleFontStyle;

    if lStyle.AutoSizeSimpleClass
       then TextOut(lStyle.SimpleClassTitleHorizontalMargin, lStyle.SimpleClassTitleVerticalMargin, SimpleClass.DisplayName)
       else TextOut(lStyle.ScaleMargin DIV 2, lStyle.ScaleMargin DIV 2, SimpleClass.DisplayName);
    end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdComplexClassTitle
//
procedure TcdComplexClassTitle.ResetBounds_Internal;
  var
    lLeft, lTop, lWidth, lHeight: Integer;
    lStyle: TcdClassStyle;
  begin
  inherited;

  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := nil;

    Font_Name := lStyle.ComplexClassTitleFontName;
    Font_Height := lStyle.ComplexClassTitleFontHeight;
    Font_Style := lStyle.ComplexClassTitleFontStyle;

    if lStyle.AttributeOrientation = aoVertical
       then begin
            lWidth := TextWidth(ComplexClass.DisplayName) + lStyle.ComplexClassTitleFrameHorizontalMargin*2;
            lHeight := TextHeight(ComplexClass.DisplayName) + lStyle.ComplexClassTitleFrameVerticalMargin*2;

            lLeft := ComplexClass.IntLeft;
            lTop := ComplexClass.IntTop;
            end
       else begin
            lWidth := TextWidth(ComplexClass.DisplayName) + lStyle.ScaleMargin;
            lHeight := TextHeight(ComplexClass.DisplayName) + lStyle.ScaleMargin;

            case lStyle.ComplexClassTitleAlignment of
               ctaLeft:        lLeft := ComplexClass.IntLeft;
               ctaLeftCenter:  lLeft := ComplexClass.IntLeft - (lWidth DIV 2);
               ctaCenter:      lLeft := ComplexClass.IntLeft + (ComplexClass.IntWidth DIV 2) - (lWidth DIV 2);
               ctaRight:       lLeft := ComplexClass.IntLeft + ComplexClass.IntWidth - lWidth;
               ctaRightCenter: lLeft := ComplexClass.IntLeft + ComplexClass.IntWidth - (lWidth DIV 2);
               else lLeft := 0; // Prevent compiler warning
               end;

            lTop := ComplexClass.LowestRuleTop - lHeight - lStyle.ComplexClassTitleVerticalMargin;
            if not (ComplexClass.IsReferenced or ComplexClass.IsStartForSubtypeLine)
               then Inc(lTop, 30);
            end;

    SetIntBounds(lLeft, lTop, lWidth, lHeight);
    end;
  end;

constructor TcdComplexClassTitle.Create(aDiagram: TcdDiagram; aComplexClass: TcdComplexClass);
  begin
  inherited Create(aDiagram, ooAuto);
  ControlStyle := ControlStyle - [csOpaque];
  Clickable := False;
  IsEmbeddedObject := true;
  fComplexClass := aComplexClass;
  end;

procedure TcdComplexClassTitle.PaintOnCanvas(aCanvas: TCanvas);
  var
    lStyle: TcdClassStyle;
  begin
  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Brush_Style := bsClear;
    Font_Name := lStyle.ComplexClassTitleFontName;
    Font_Height := lStyle.ComplexClassTitleFontHeight;
    Font_Color := lStyle.ComplexClassTitleFontColor;
    Font_Style := lStyle.ComplexClassTitleFontStyle;

    if lStyle.AttributeOrientation = aoVertical
       then TextOut(lStyle.ComplexClassTitleFrameHorizontalMargin, lStyle.ComplexClassTitleFrameVerticalMargin, ComplexClass.DisplayName)
       else TextOut(lStyle.ScaleMargin DIV 2, lStyle.ScaleMargin DIV 2, ComplexClass.DisplayName);
    end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdComplexClassConnectorFrame
//
procedure TcdComplexClassConnectorFrame.ResetBounds_Internal;
  var
    A,B,C,D,
    lLeft, lTop, lRight, lBottom, lWidth, lHeight,
    X, Y, lCenter, lSafetyMargin,
    lMargin: Integer;
    lStyle: TcdClassStyle;
  begin
  inherited;

  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := nil;

    if lStyle.ConnectorFrame
       then begin
            if lStyle.ConnectorFrameStyle=cfsEllipse
               then begin
                    lHeight := lStyle.AttributeFrameHeight + Round(ComplexClass.IntWidth * 0.05);
                    if ComplexClass.IntWidth<=lStyle.AttributeFrameWidth
                       then lWidth := lHeight // Prevent horizontal egg
                       else lWidth := Round(lStyle.AttributeFrameWidth * 1.5);

                    lLeft   := ComplexClass.IntLeft - (lWidth DIV 2);
                    lTop    := ComplexClass.IntTop  - (lHeight DIV 2);
                    lWidth  := ComplexClass.IntWidth + lWidth;
                    lHeight := ComplexClass.IntHeight + lHeight;

                    // Prevent vertical egg
                    if lWidth<lHeight
                       then lWidth := lHeight;

                    SetIntBounds(lLeft, lTop, lWidth, lHeight);
                    end
               else begin
                    lMargin := lStyle.ConnectorFrameMargin;
                    lLeft := ComplexClass.IntLeft - lMargin;
                    lTop := ComplexClass.LowestRuleTop - lMargin;
                    lWidth := ComplexClass.IntWidth + lMargin*2;
                    lHeight := (ComplexClass.IntTop-ComplexClass.LowestRuleTop) + ComplexClass.IntHeight + lMargin*2;

                    SetIntBounds(lLeft, lTop, lWidth, lHeight);

                    // Calculate left, right etc
                    A := 50; B := 20; C:= 5;
                    D := 100;
                    lLeft := lStyle.ScaleMargin;
                    lTop := lStyle.ScaleMargin;
                    lRight := (ComplexClass.IntWidth + lMargin*2) -lStyle.ScaleMargin;
                    lBottom := ((ComplexClass.IntTop-ComplexClass.LowestRuleTop) + ComplexClass.IntHeight + lMargin*2) -lStyle.ScaleMargin;

                    DeriveRoundRectPoints(fPoints, lLeft,lTop,lRight,lBottom, A,B,C,D);

                    // Create connection points
                    DeleteConnectionPoints;
                    lSafetyMargin := Diagram.Style.ClawWidth + A;
                    lCenter := Diagram.SnapToGridX((lRight-lLeft) DIV 2);
                    X := lCenter;
                    while X <= lRight-lSafetyMargin do
                      begin
                      AddConnectionPoint(X, lTop);
                      AddConnectionPoint(X, lBottom);
                      if X<>lCenter
                         then begin
                              AddConnectionPoint(lCenter-(X-lCenter), lTop);
                              AddConnectionPoint(lCenter-(X-lCenter), lBottom);
                              end;
                      Inc(X, lStyle.GridX);
                      end;

                    lCenter := Diagram.SnapToGridY((lBottom-lTop) DIV 2);
                    Y := lCenter;
                    while Y <= lBottom-lSafetyMargin do
                      begin
                      AddConnectionPoint(lLeft, Y);
                      AddConnectionPoint(lRight, Y);
                      if Y<>lCenter
                         then begin
                              AddConnectionPoint(lLeft, lCenter-(Y-lCenter));
                              AddConnectionPoint(lRight, lCenter-(Y-lCenter));
                              end;
                      Inc(Y, lStyle.GridY);
                      end;
                    end;
            end
       else SetIntBounds(ComplexClass.IntLeft, ComplexClass.IntTop, ComplexClass.IntWidth, ComplexClass.IntHeight);
    end;
  end;

procedure TcdComplexClassConnectorFrame.SetSelected(aValue: Boolean);
  begin
  if aValue
     then ComplexClass.Select;
  end;

function TcdComplexClassConnectorFrame.GetConnectionPoint(aLine: TcdLine; aPoint: TPoint): TPoint;
  var
    lDX, lDY, lnDX, lnDY, lCenterX, lCenterY: Integer;
    lAlfa: Double;
  begin
  lCenterX := IntLeft + (IntWidth DIV 2);
  lCenterY := IntTop  + (IntHeight DIV 2);
  lDX := Abs(aPoint.X - lCenterX);
  lDY := Abs(aPoint.Y - lCenterY);

  if lDX = 0
     then lAlfa := 0.5*Pi
     else lAlfa := ArcTan (lDY / lDX);

  lnDX := Round( Cos(lAlfa) * (IntWidth DIV 2) );
  lnDY := Round( Sin(lAlfa) * (IntHeight DIV 2) );

  if lCenterY > aPoint.Y
     then begin
          if lCenterX > aPoint.X
             then Result := Classes.Point(lCenterX - lnDX, lCenterY - lnDY)
             else Result := Classes.Point(lCenterX + lnDX, lCenterY - lnDY)
          end
     else begin
          if lCenterX > aPoint.X
             then Result := Classes.Point(lCenterX - lnDX, lCenterY + lnDY)
             else Result := Classes.Point(lCenterX + lnDX, lCenterY + lnDY)
          end;

  // Handle exception
  if ((Result.X < 0) and (lCenterX >= 0)) or
     ((Result.Y < 0) and (lCenterY >= 0))
     then Result := Classes.Point(lCenterX, lCenterY)
  end;
   
constructor TcdComplexClassConnectorFrame.Create(aDiagram: TcdDiagram; aComplexClass: TcdComplexClass);
  begin
  inherited Create(aDiagram, ooAuto);
  ControlStyle := ControlStyle - [csOpaque];
  Clickable := False;
  IsEmbeddedObject := true;
  fComplexClass := aComplexClass;
  end;

procedure TcdComplexClassConnectorFrame.PaintOnCanvas(aCanvas: TCanvas);
  var
    lStyle: TcdClassStyle;
  begin
  if not ComplexClass.IsReferenced and not ComplexClass.IsStartForSubtypeLine
     then Exit;

  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    if lStyle.ConnectorFrame
       then begin
            Brush_Style := bsClear;
            Pen_Style := psSolid;
            Pen_Color := lStyle.ComplexClassPenColor;

            if lStyle.ConnectorFrameStyle = cfsRoundRect
               then PolyBezier(fPoints)
               else Ellipse(0,0,IntWidth,IntHeight);
            end;
    end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdAttribute
//
procedure TcdAttribute.ResetBounds_Internal;
  var
    lLeft, lTop, lWidth, lHeight: Integer;
    lStyle: TcdClassStyle;
    lPreviousAttribute: TcdAttribute;
  begin
  inherited;

  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := nil;

    // RuleStr
    fRuleStr := DeriveRuleStr;

    // Attribute font
    Font_Name := lStyle.ClassFontname;
    Font_Height := lStyle.ClassFontHeight;
    Font_Style := lStyle.ClassFontStyle;

    if fRelativeNr<0
       then begin
            // This attribute should be invisible
            lLeft := 0; lTop := 0; lWidth := 0; lHeight := 0;
            end
       else begin
            if lStyle.AttributeOrientation = aoVertical
               then begin
                    lLeft := ComplexClass.IntLeft;
                    lTop := ComplexClass.IntTop + ComplexClass.fTitleFrameHeight + (RelativeNr*ComplexClass.fAttributeFrameHeight);
                    lWidth := ComplexClass.IntWidth;
                    lHeight := ComplexClass.fAttributeFrameHeight;
                    end
               else begin
                    lPreviousAttribute := ComplexClass.Attributes.GetAttributeByRelativeNr(RelativeNr-1);
                    if (RelativeNr>0) and Assigned(lPreviousAttribute)
                       then lLeft := lPreviousAttribute.IntLeft + lPreviousAttribute.DeriveWidth -lStyle.ScaleMargin
                       else lLeft := ComplexClass.IntLeft;
                    lTop := ComplexClass.IntTop;
                    lWidth := DeriveWidth;
                    lHeight := lStyle.AttributeFrameHeight;
                    end;
            end;

    SetIntBounds(lLeft, lTop, lWidth, lHeight);

    // Connectionpoints
    DeleteConnectionpoints;
    if lStyle.AttributeOrientation = aoVertical
       then begin
            AddConnectionPoint(0, Diagram.SnapToGridY(IntHeight DIV 2));
            AddConnectionPoint(IntWidth, Diagram.SnapToGridY(IntHeight DIV 2));
            end
       else begin
            if Self = ComplexClass.Attributes.First
               then AddConnectionPoint(0, Diagram.SnapToGridY(IntHeight DIV 2));
            AddConnectionPoint(Diagram.SnapToGridX(IntWidth DIV 2), 0);
            AddConnectionPoint(Diagram.SnapToGridX(IntWidth DIV 2), IntHeight);
            if Self = ComplexClass.Attributes.Last
               then AddConnectionPoint(IntWidth, Diagram.SnapToGridY(IntHeight DIV 2));
            end;
    end;
  end;

constructor TcdAttribute.Create(aDiagram: TcdDiagram; aComplexClass: TcdComplexClass);
  begin
  inherited Create(aDiagram, ooAuto);
  fComplexClass := aComplexClass;
  IsEmbeddedObject := true;
  fSelectionGroup := 1;
  fIntraRules := TcdRuleList.Create;
  end;

destructor TcdAttribute.Destroy;
  begin
  fIntraRules.RemoveAll;
  FreeAndNil(fIntraRules);
  inherited Destroy;
  end;

procedure TcdAttribute.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer);
  begin
  if aButton = mbRight
     then TcdClassDiagram(Diagram).DoAttributePopupMenu(aX, aY, Self)
     else begin
          if (aButton <> mbLeft) or (ssAlt IN aShiftState)
             then Exit;

          if Diagram.Style.ChildSelectKey <= aShiftState
             then begin
                  if Diagram.Style.MultiSelectKey <= aShiftState
                     then begin
                          Diagram.Selection.Toggle(Self);
                          if not Selected
                             then ScrollBox.DragMode := dmNone;
                          end
                     else begin
                          if not Selected
                             then begin
                                  Diagram.Selection.Clear;
                                  Diagram.Selection.Add(Self);
                                  end;
                          end;
                  end
             else begin
                  if Diagram.Style.MultiSelectKey <= aShiftState
                     then begin
                          Diagram.Selection.Toggle(ComplexClass);
                          if not ComplexClass.Selected
                             then ScrollBox.DragMode := dmNone;
                          end
                     else begin
                          if not ComplexClass.Selected
                             then begin
                                  Diagram.Selection.Clear;
                                  Diagram.Selection.Add(ComplexClass);
                                  end;
                          end;

                  ScrollBox.DragMode := dmSelecting;
                  end;

          inherited MouseDown(aButton, aShiftState, aX, aY);
          end;

  Scrollbox.SetMouseDownPosition(Scrollbox.ScreenToClient(ClientToScreen(Classes.Point(aX, aY))));
  end;

procedure TcdAttribute.MouseUp(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer);
  begin
  ScrollBox.MouseUp(aButton, aShiftState, aX+Left, aY+Top);
  inherited MouseUp(aButton, aShiftState, aX, aY);
  end;

procedure TcdAttribute.MouseMove(aShiftState: TShiftState; aX, aY: Integer);
  begin
  if ScrollBox.DragMode = dmSelecting
     then ScrollBox.DragMode := dmDragging;
  ScrollBox.MouseMove(aShiftState, aX+Left, aY+Top);
  inherited MouseMove(aShiftState, aX, aY);
  end;

procedure TcdAttribute.PaintOnCanvas (aCanvas: TCanvas);
  var
    lStyle: TcdClassStyle;
    lColor: TColor;
  begin
  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    // Attribute font
    Font_Name := lStyle.ClassFontname;
    if Selected
       then Font_Color := lStyle.AttributeFontColor_Selected
       else Font_Color := lStyle.ClassFontColor;
    Font_Height := lStyle.ClassFontHeight;
    if Type_='C'
       then Font_Style := lStyle.ReferencingAttributeFontStyle
       else Font_Style := lStyle.ClassFontStyle;

    // Background fill color
    Brush_Style := bsSolid;
    if ComplexClass.Selected
       then Brush_Color := lStyle.ComplexClassFillColor_Selected
    else if Selected
       then Brush_Color := lStyle.AttributeFillColor_Selected
    else begin
         lColor := TcdClassDiagram(Diagram).GetModuleColor(Module);
         if lColor=0
            then lColor := TcdClassDiagram(Diagram).GetModuleColor(ComplexClass.Module);

         if lColor=0
            then Brush_Color := clWhite
            else Brush_Color := lColor;
         end;

    if lStyle.AttributeOrientation = aoHorizontal
       then begin
            // Background
            FillRect(IntClientRect);

            // Attribute frame
            Brush_Style := bsSolid;
            Brush_Color := lStyle.ComplexClassPenColor;
            FrameRect(IntClientRect);

            Brush_Style := bsClear;
            TextOut(lStyle.AttributeFrameHorizontalMargin, lStyle.AttributeFrameVerticalMargin_Name, DisplayName);

            if lStyle.DisplayAttributeTypeNames
               then TextOut(lStyle.AttributeFrameHorizontalMargin, lStyle.AttributeFrameVerticalMargin_Type,
                       lStyle.AttributeTypeName_StartSymbol + TypeName + lStyle.AttributeTypeName_EndSymbol);

            Font_Name := lStyle.RuleFontName;
            Font_Height := lStyle.RuleFontHeight;
            Font_Style := lStyle.RuleFontStyle;
            Font_Color := lStyle.RuleFontColor;

            if fRuleStr<>''
               then TextOut(IntWidth-lStyle.ORWIndicatorRightMargin, lStyle.AttributeFrameVerticalMargin_ORW, fRuleStr);
            end
       else begin
            // Background
            FillRect(
               Classes.Rect(
                 IntClientRect.Left+lStyle.ScaleMargin, IntClientRect.Top,
                 IntClientRect.Right-lStyle.ScaleMargin, IntClientRect.Bottom
               )
            );

            Brush_Style := bsClear;
            TextOut(lStyle.AttributeFrameHorizontalMargin, lStyle.AttributeFrameVerticalMargin, DisplayName);

            // Reset the font style
            Font_Style := lStyle.ClassFontStyle;

            if lStyle.DisplayAttributeTypeNames
               then TextOut(ComplexClass.fXPos_AttributeTypeName, lStyle.AttributeFrameVerticalMargin,
                       lStyle.AttributeTypeName_StartSymbol + TypeName + lStyle.AttributeTypeName_EndSymbol);

            if fRuleStr<>''
               then TextOut(ComplexClass.fXPos_RuleStr, lStyle.AttributeFrameVerticalMargin, fRuleStr);
            end;
    end;
  end;

function TcdAttribute.DeriveWidth: Integer;
  var
    lStyle: TcdClassStyle;
    lStr: string;
  begin
  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := nil;

    // Attribute font
    Font_Name := lStyle.ClassFontname;
    Font_Height := lStyle.ClassFontHeight;
    if Type_='C'
       then Font_Style := lStyle.ReferencingAttributeFontStyle
       else Font_Style := lStyle.ClassFontStyle;

    // Name
    Result := TextWidth(DisplayName);

    // ReferToName
    if lStyle.DisplayAttributeTypeNames
       then Result := Max(Result, TextWidth(lStyle.AttributeTypeName_StartSymbol + TypeName + lStyle.AttributeTypeName_EndSymbol));

    Font_Name := lStyle.RuleFontName;
    Font_Height := lStyle.RuleFontHeight;
    Font_Style := lStyle.RuleFontStyle;

    lStr := DeriveRuleStr;
    if (lStyle.AttributeOrientation=aoHorizontal) and (lStr<>'')
       then Result := Result + TextWidth(lStr);

    Result := Diagrammer_RoundTo(Result + lStyle.AttributeFrameHorizontalMargin*2, lStyle.ScaleMargin);

    if lStyle.AttributeOrientation = aoHorizontal
       then Result := Max(Result, lStyle.AttributeFrameWidth);
    end;
  end;

function TcdAttribute.DeriveRuleStr: string;

  procedure AddRule(aStr: string);
    begin
    if aStr=''
       then Exit;

    if Result=''
       then Result := aStr
       else Result := Result + ',' + aStr;
    end;

  var
    lStyle: TcdClassStyle;
    lRule: TcdRule;
    T: Integer;
  begin
  lStyle := TcdClassStyle(Diagram.Style);

  Result := '';
  if lStyle.AttributeOrientation=aoVertical
     then begin
          for T := 0 to IntraRules.Count-1 do
            begin
            lRule := TcdRule(IntraRules[T]);
            case lRule.Type_ of

               ctIdentification:
                  begin
                  if lStyle.DisplayIndexes
                     then AddRule(lStyle.Index_IdentificationStr);
                  end;

               ctUnicity:
                  begin
                  if lStyle.DisplayIndexes
                     then AddRule(lStyle.Index_UniqueStr + IntToStr(lRule.RelativeID));
                  end;

               ctSortIndex:
                  begin
                  if lStyle.DisplayIndexes
                     then AddRule(lStyle.Index_SortStr + IntToStr(lRule.RelativeID));
                  end;

               ctTotality:
                  begin
                  if lStyle.DisplayTotality
                     then AddRule(lStyle.TotalityStr + IntToStr(lRule.RelativeID));
                  end;

               ctDisplay:
                  AddRule(lStyle.DisplayRuleStr);

               else EInt('TcdAttribute.DeriveRuleStr', 'Unknown rule type');
               end;
            end;
          end;

  if lStyle.DisplayORWIndicator
     then AddRule(lStyle.GetDisplayStrForORWIndicator(ORWIndicator));
  end;

function TcdAttribute.GetDisplayName: string;
  var
    lStyle: TcdClassStyle;
  begin
  lStyle := TcdClassStyle(Diagram.Style);

  if Derivable='Y'
     then begin
          if lStyle.DerivableIndicatorPosition=hpLeft
             then Result := lStyle.DerivableIndicator + Name
             else Result := Name + lStyle.DerivableIndicator;
          end
  else if Derivable='I'
     then begin
          if lStyle.DerivableIndicatorPosition=hpLeft
             then Result := lStyle.InitialDerivableIndicator + Name
             else Result := Name + lStyle.InitialDerivableIndicator;
          end
     else Result := Name;
   end;

function TcdAttribute.GetORWIndicatorAsCardinality: Integer;
  begin
  if fORWIndicator='O'
     then Result := 0
  else if fORWIndicator='W'
     then Result := cCardWarning
  else if fORWIndicator='R'
     then Result := 1
  else Result := cCardNone;
  end;

function TcdAttribute.HasSingleAttributeTotalityRule: Boolean;
  var
    T: Integer;
    lRule: TcdRule;
  begin
  Result := True;
  for T := 0 to IntraRules.Count-1 do
    begin
    lRule := TcdRule(IntraRules[T]);
    if (lRule.Type_=ctTotality) and (lRule.Attributes.Count=1)
       then Exit;
    end;
  Result := False;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdAttributeList
//
constructor TcdAttributeList.Create(aDiagram: TcdClassDiagram);
  begin
  inherited Create;
  fDiagram := aDiagram;
  end;

function TcdAttributeList.First: TcdAttribute;
  var
    T: Integer;
  begin
  Result := nil;
  for T := 0 to Count-1 do
    if TcdAttribute(Objects[T]).RelativeNr>=0
       then begin
            Result := TcdAttribute(Objects[T]);
            Exit;
            end;
  end;

function TcdAttributeList.Last: TcdAttribute;
  var
    T: Integer;
  begin
  Result := nil;
  for T := Count-1 downto 0 do
    if TcdAttribute(Objects[T]).RelativeNr>=0
       then begin
            Result := TcdAttribute(Objects[T]);
            Exit;
            end;
  end;

function TcdAttributeList.GetAttributeByRelativenr (aRelativenr: Integer): TcdAttribute;
  var
    T: Integer;
  begin
  Result := nil;
  if aRelativeNr<0 then Exit;
  for T := 0 to Count-1 do
    begin
    if TcdAttribute(Objects[T]).RelativeNr=aRelativeNr
       then begin
            Result := TcdAttribute(Objects[T]);
            Exit;
            end;
    end;
  end;

function TcdAttributeList.GetAttributeBySyncID (aSyncID: Integer): TcdAttribute;
  var
    T: Integer;
  begin
  Result := nil;
  for T := 0 to Count-1 do
    begin
    if TcdAttribute(Objects[T]).SyncID=aSyncID
       then begin
            Result := TcdAttribute(Objects[T]);
            Exit;
            end;
    end;
  end;

function TcdAttributeList.GetAttributeByName (aName: string): TcdAttribute;
  var
    T: Integer;
  begin
  Result := nil;
  for T := 0 to Count-1 do
    begin
    if CompareText(TcdAttribute(Objects[T]).Name, aName)=0
       then begin
            Result := TcdAttribute(Objects[T]);
            Exit;
            end;
    end;
  end;

function TcdAttributeList.TotalWidth: Integer;
  var
    T: Integer;
  begin
  Result := 0;
  for T := 0 to Count-1 do
    Result := Result + TcdAttribute(Objects[T]).DeriveWidth;
  Result := Result - ((Count-1)*Diagram.Style.ScaleMargin)
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdRule
//
procedure TcdRule.SetSyncID(aValue: Integer);
  var
    lOldValue: Integer;
  begin
  lOldValue := fSyncID;
  fSyncID := aValue;
  if not Assigned(fClass)
     then begin
          if lOldValue<>-1
             then TcdClassDiagram(Diagram).InterClassRules.Remove(Self);
          if fSyncID<>-1
             then TcdClassDiagram(Diagram).InterClassRules.Add(Self);
          end;
  end;

procedure TcdRule.ResetBounds_Internal;
  var
    lStyle: TcdClassStyle;
    T, lLeft, lTop, lWidth, lHeight: Integer;
    lAttribute: TcdAttribute;
    lClass: TcdComplexClass;
  begin
  inherited;
  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := nil;

    if not (Type_ IN [ctIdentification, ctUnicity])
       then Exit;

    if lStyle.AttributeOrientation = aoVertical
       then Exit;

    if not Assigned(Class_)
       then Exit;

    lClass := TcdComplexClass(Class_);

    lLeft := MaxInt;
    lWidth := 0;
    for T := 0 to Attributes.Count-1 do
       begin
       lAttribute := TcdAttribute(Attributes[T]);

       lLeft := Min(lLeft, lAttribute.IntLeft);
       lWidth := Max(lWidth, lAttribute.IntLeft+lAttribute.IntWidth);
       end;
    lWidth := lWidth - lLeft;

    if Type_=ctIdentification
       then lTop := lClass.IntTop - (lClass.IntraRules.IdentityCount*lStyle.UnicityRuleMargin)
       else lTop := lClass.IntTop - ((RelativeID+lClass.IntraRules.IdentityCount)*lStyle.UnicityRuleMargin);
    lHeight := lStyle.UnicityRuleMargin-1;

    lClass.LowestRuleTop := Min(lClass.LowestRuleTop, lTop);

    SetIntBounds(lLeft, lTop, lWidth, lHeight);
    end;
  end;

constructor TcdRule.Create(aDiagram: TcdDiagram; aClass: TcdBaseClass; aType: TcdRuleType);
  begin
  inherited Create(aDiagram, ooAuto);
  fType := aType;
  fSyncID := -1;
  IsEmbeddedObject := true;
  fClass := aClass;
  fAttributes := TcdAttributeList.Create(TcdClassDiagram(aDiagram));
  fAttributeIDs := TcIntegerList.Create;
  end;

destructor TcdRule.Destroy;
  begin
  if not Assigned(fClass) and (fSyncID<>-1)
     then TcdClassDiagram(Diagram).InterClassRules.Remove(Self);

  fAttributes.RemoveAll;
  FreeAndNil(fAttributes);
  FreeAndNil(fAttributeIDs);
  inherited;
  end;

procedure TcdRule.LoadFromObject(aRelationType: TcRelationType; aType: TcdRuleType; aRoles: TcElementList<TcRole>; aRole: TcRole = nil);
  var
   lAttribute: TcdAttribute;
   lClass: TcdComplexClass;

   i: integer;
   lRole: TcRole;
  begin
  fType := aType;
  if Assigned(aRoles)
     then begin
          SyncID := aRoles.ID;
          Name := aRoles.AsString;

          Attributes.RemoveAll;
          AttributeIDs.Clear;

          for i := 0 to aRoles.Count-1 do
            begin
            lRole := aRoles[i];
            if Assigned(Class_)
               then begin
                    lClass := TcdComplexClass(Class_);
                    lAttribute := lClass.Attributes.GetAttributeBySyncID(lRole.ID);
                    if not Assigned(lAttribute)
                       then EInt('TcdConstaint.LoadFromData', 'Illegal rule definition');

                    lAttribute.IntraRules.Add(Self);
                    Attributes.Add(lAttribute);
                    end;
             AttributeIDs.Add(lRole.ID);
             end;
          end
     else begin
          if not Assigned(aRole) then EInt('TcdConstaint.LoadFromData', 'Illegal rule definition');
          SyncID := aRole.ID;
          Name := aRole.Name;

          Attributes.RemoveAll;
          AttributeIDs.Clear;

          if Assigned(Class_)
             then begin
                  lClass := TcdComplexClass(Class_);
                  lAttribute := lClass.Attributes.GetAttributeBySyncID(aRole.ID);
                  if not Assigned(lAttribute)
                     then EInt('TcdConstaint.LoadFromData', 'Illegal rule definition');

                  lAttribute.IntraRules.Add(Self);
                  Attributes.Add(lAttribute);
                  end;
          AttributeIDs.Add(aRole.ID);
          end;
  end;

procedure TcdRule.PaintOnCanvas(aCanvas: TCanvas);
  var
    lStyle: TcdClassStyle;
    T, lSegmentCounter, lPos, lBaseLineY: Integer;
    lAttribute: TcdAttribute;
    lTempList: TcList;
    lClass: TcdComplexClass;
  begin
  lStyle := TcdClassStyle(Diagram.Style);
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    if not (Type_ IN [ctIdentification, ctUnicity])
       then Exit;

    if lStyle.AttributeOrientation = aoVertical
       then Exit;

    if not Assigned(Class_)
       then Exit;

    lClass := TcdComplexClass(Class_);

    lBaseLineY := lStyle.UnicityArrowHeightDIV2;

    lSegmentCounter := 0;
    lTempList := TcList.Create;
    lTempList.Assign(Attributes);

    for T := 0 to lClass.Attributes.Count-1 do
       begin
       lAttribute := TcdAttribute(lClass.Attributes[T]);

       lPos := lTempList.IndexOf(lAttribute);
       if lPos >= 0
          then begin
               Inc(lSegmentCounter);
               lTempList.RemoveAt(lPos);
               end
          else if lSegmentCounter>0 then Inc(lSegmentCounter);

       if lSegmentCounter>0
          then begin
               Brush_Style := bsClear;
               Pen_Style := psSolid;
               Pen_Color := lStyle.ComplexClassPenColor;

               if lPos<0
                  then Pen_Style := psDot;

               if lStyle.DoubleIdentityLine and (Type_=ctIdentification)
                  then begin
                       MoveTo(lAttribute.IntLeft-IntLeft,                     lBaseLineY-lStyle.DoubleIdentityLineMargin);
                       LineTo(lAttribute.IntLeft+lAttribute.IntWidth-IntLeft, lBaseLineY-lStyle.DoubleIdentityLineMargin);
                       MoveTo(lAttribute.IntLeft-IntLeft,                     lBaseLineY+lStyle.DoubleIdentityLineMargin);
                       LineTo(lAttribute.IntLeft+lAttribute.IntWidth-IntLeft, lBaseLineY+lStyle.DoubleIdentityLineMargin);
                       end
                  else begin
                       MoveTo(lAttribute.IntLeft-IntLeft,                     lBaseLineY);
                       LineTo(lAttribute.IntLeft+lAttribute.IntWidth-IntLeft, lBaseLineY);
                       end;
               end;

       if lSegmentCounter=1
          then begin
               // Start of line
               if lStyle.DisplayUnicityArrow
                  then begin
                       Brush_Style := bsSolid;
                       Brush_Color := lStyle.ObjectConFillColor;
                       Pen_Style := psSolid;
                       Pen_Color := lStyle.ObjectConPenColor;
                       Polygon([
                          Classes.Point(lAttribute.IntLeft-IntLeft                          , lBaseLineY),
                          Classes.Point(lAttribute.IntLeft-IntLeft+lStyle.UnicityArrowLength, lBaseLineY-lStyle.UnicityArrowHeightDIV2),
                          Classes.Point(lAttribute.IntLeft-IntLeft+lStyle.UnicityArrowLength, lBaseLineY+lStyle.UnicityArrowHeightDIV2)
                       ]);
                       end;
               end;

       if lTempList.Count=0
          then begin
               // End of line
               if lStyle.DisplayUnicityArrow
                  then begin
                       Brush_Style := bsSolid;
                       Brush_Color := lStyle.ObjectConFillColor;
                       Pen_Style := psSolid;
                       Pen_Color := lStyle.ObjectConPenColor;
                       Polygon([
                          Classes.Point(lAttribute.IntLeft+lAttribute.IntWidth-IntLeft                          , lBaseLineY),
                          Classes.Point(lAttribute.IntLeft+lAttribute.IntWidth-IntLeft-lStyle.UnicityArrowLength, lBaseLineY-lStyle.UnicityArrowHeightDIV2),
                          Classes.Point(lAttribute.IntLeft+lAttribute.IntWidth-IntLeft-lStyle.UnicityArrowLength, lBaseLineY+lStyle.UnicityArrowHeightDIV2)
                       ]);
                       end;

               // Stop iteration
               Break;
               end;
       end;
    lTempList.Free;
    end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdRuleList
//
function TcdRuleList.Compare (const aObjectA, aObjectB: TObject): Integer;
  begin
  if TcdRule(aObjectA).SyncID>TcdRule(aObjectB).SyncID
     then Result := 1
  else if TcdRule(aObjectA).SyncID<TcdRule(aObjectB).SyncID
     then Result := -1
  else Result := 0;
  end;

function TcdRuleList.Compare (const aObject: TObject; const aKeyValue: string): Integer;
  begin
  if TcdRule(aObject).SyncID>StrToInt(aKeyValue)
     then Result := 1
  else if TcdRule(aObject).SyncID<StrToInt(aKeyValue)
     then Result := -1
  else Result := 0;
  end;

function TcdRuleList.Find(aSyncID: Integer): TcdRule;
  var
    lPos: Integer;
  begin
  if Search(IntToStr(aSyncID), lPos)
     then Result := TcdRule(Objects[lPos])
     else Result := nil;
  end;

procedure TcdRuleList.Derive;
  var
    lIdentityCounter,
    lUnicityCounter,
    lSortIndexCounter,
    lTotalityCounter : Integer;

    lRule: TcdRule;
    T: Integer;
  begin
  lIdentityCounter  := 0;
  lUnicityCounter   := 0;
  lSortIndexCounter := 0;
  lTotalityCounter  := 0;

  for T := 0 to Count-1 do
    begin
    lRule := TcdRule(Objects[T]);
    case lRule.Type_ of

      ctIdentification:
         begin
         Inc(lIdentityCounter);
         lRule.RelativeID := lIdentityCounter;
         end;

      ctUnicity:
         begin
         Inc(lUnicityCounter);
         lRule.RelativeID := lUnicityCounter;
         end;

      ctSortIndex:
         begin
         Inc(lSortIndexCounter);
         lRule.RelativeID := lSortIndexCounter;
         end;

      ctTotality:
         begin
         Inc(lTotalityCounter);
         lRule.RelativeID := lTotalityCounter;
         end;

      ctDisplay:
         begin
         // Ignore, there should be only one display rule.
         end;

      else EInt('TcdAttribute.DeriveRuleStr', 'Unknown rule type');
      end;
    end;

  fIdentityCount := lIdentityCounter;
  fUnicityCount := lUnicityCounter;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdReferenceLine
//
constructor TcdReferenceLine.Create (aDiagram: TcdDiagram; aFromObject, aToObject: TcdConObject; aConData: TcdLineConData);
  begin
  inherited Create(aDiagram,
     TcdClassStyle(aDiagram.Style).ReferenceLineType,
     TcdClassStyle(aDiagram.Style).ReferenceLineStyle,
     TcdClassStyle(aDiagram.Style).ReferenceLineFromObjectConType,
     TcdClassStyle(aDiagram.Style).ReferenceLineToObjectConType,
     aFromObject, aToObject, aConData);
  TcdClassDiagram(aDiagram).ReferenceLines.Add(Self);
  end;

destructor TcdReferenceLine.Destroy;
  begin
  TcdClassDiagram(Diagram).ReferenceLines.Remove(Self);
  inherited;
  end;

procedure TcdReferenceLine.LoadFromJson(aJsonObject: TJsonObject);
  begin
  inherited;
  SetAttributeName;
  end;

procedure TcdReferenceLine.SetAttributeName;
  var
    lStyle: TcdClassStyle;
    lMaster: TcdBaseClass;
    lDetail: TcdComplexClass;
    lAttribute: TcdAttribute;
    lName: string;
  begin
  lStyle := TcdClassStyle(Diagram.Style);

  if lStyle.DisplayReferencingAttributes and (lStyle.ReferenceStyle=rsAttributeToClass)
     then begin
          CommentStr := '';
          ToCommentStr := '';
          end
     else begin
          lMaster := TcdBaseClass(ToObject);
          lDetail := TcdComplexClass(FromObject);
          lAttribute := lDetail.Attributes.GetAttributeBySyncID(TcdReferenceLineData(ConData).AttributeSyncID);
          Assert(Assigned(lAttribute));
          if CompareText(lAttribute.Name, lMaster.Name)<>0
             then lName := lAttribute.Name
             else lName := '';

          if lStyle.ReFerenceNamePosition=rnpCenter
             then begin
                  if CommentStr=''
                     then begin
                          CommentStr := lName;
                          ToCommentStr := '';
                          end;
                  end
             else begin
                  if FromCommentStr=''
                     then begin
                          ToCommentStr := lName;
                          CommentStr := '';
                          end;
                  end;
          end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdReferenceLineData
//
procedure TcdReferenceLineData.Init(aLine: TcdReferenceLine);
  begin
  fReferenceLine := aLine;
  end;

procedure TcdReferenceLineData.LoadFromData(aDetailAttribute: TcdAttribute);
  begin
  fAttributeSyncID := aDetailAttribute.SyncID;
  end;

procedure TcdReferenceLineData.LoadFromJson(aJsonObject: TJsonObject);
  var
    lComplexClass: TcdComplexClass;
    lAttributeName: string;
    lcdAttribute: TcdAttribute;
  begin
  if not (ReferenceLine.FromObject is TcdComplexClass)
     then EInt('TcdReferenceLineData.LoadFromJson', 'Unexpected type');
  lComplexClass := TcdComplexClass(ReferenceLine.FromObject);

  if not aJsonObject.TryGetValue('referencingAttribute', lAttributeName) then InvalidJson_xExpected('referencingAttribute');
  lcdAttribute := lComplexClass.Attributes.GetAttributeByName(lAttributeName);
  if not Assigned(lcdAttribute) then exit;
  fAttributeSyncID := lcdAttribute.SyncID;
  end;

procedure TcdReferenceLineData.SaveToJson(aJsonObject: TJsonObject);
  var
    lComplexClass: TcdComplexClass;
    lcdAttribute: TcdAttribute;
  begin
  if not (ReferenceLine.FromObject is TcdComplexClass)
     then EInt('TcdReferenceLineData.LoadFromJson', 'Unexpected type');
  lComplexClass := TcdComplexClass(ReferenceLine.FromObject);
  lcdAttribute := lComplexClass.Attributes.GetAttributeBySyncID(fAttributeSyncID);

  aJsonObject.AddPair('referencingAttribute', lcdAttribute.Name);
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdSubtypeLine
//
constructor TcdSubtypeLine.Create (aDiagram: TcdDiagram; aFromObject, aToObject: TcdConObject; aConData: TcdLineConData);
  begin
  inherited Create(aDiagram,
     TcdClassStyle(aDiagram.Style).SubtypeLineType,
     TcdClassStyle(aDiagram.Style).SubtypeLineStyle,
     TcdClassStyle(aDiagram.Style).SubtypeLineFromObjectConType,
     TcdClassStyle(aDiagram.Style).SubtypeLineToObjectConType,
     aFromObject, aToObject, aConData);
  end;

initialization
DiagramStyles.Register_(TcdClassStyle_IM.Create('IM'));
DiagramStyles.Register_(TcdClassStyle_UMLIM.Create('UML-IM'));
DiagramStyles.Register_(TcdClassStyle_UML.Create('UML'));
DiagramStyles.Register_(TcdClassStyle_ORM.Create('ORM'));
DiagramStyles.Register_(TcdClassStyle_ORM2.Create('ORM2'));
DiagramStyles.Register_(TcdClassStyle_ORM2_Compact.Create('ORM2 Compact'));
DiagramStyles.Register_(TcdClassStyle_ERD.Create('ERD'));
DiagramStyles.Register_(TcdClassStyle_Record.Create('Record'));

end.
