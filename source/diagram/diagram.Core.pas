// -----------------------------------------------------------------------------
//
// diagram.Core
//
// -----------------------------------------------------------------------------
//
// Visualisation library (core).
// Implements a diagramming engine.
// Originally developed to show and edit datamodelling diagrams.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV
// Contains code snippets from public internet sites such as stackoverflow.
//
// -----------------------------------------------------------------------------

unit diagram.Core;

{$MODE Delphi}

interface

uses
  LCLType, LCLIntf, Types, Classes, Generics.Collections, SysUtils,
  Messages,
  Menus, Controls, Forms, ExtCtrls, Graphics,
  common.Json, fpjson,
  common.ErrorHandling, common.Utils, common.SimpleLists;


// -----------------------------------------------------------------------------
// Documentation
// -----------------------------------------------------------------------------
//
// Coordinate systems:
//    Two coordinate systems are used: An internal system, used by the paint
//    routines to draw the objects. and an external system, as seen by the user.
//    External can be a screen, bitmap or printer.
//
// Scale canvas:
//    The scale canvas is created to convert between the two coordinate systems.
//    The paint routine can paint in it's own (internal) coordinate system and the
//    canvas will convert the coordinates to the external coordinate system.
//    This keeps the paint routines clean, and prevents calculation mistakes.
//    to scale between the two systems a factor is stored in the ScaleMD property.
//
// MulDiv's:
//    Scaling percentages are stored using MulDiv's. A MulDiv's consist of two
//    integer values: a multiplier and a divisor. (integer calculations are
//    faster then floatingpoint calculations, the windows internal MulDiv function
//    is used).
//
// Objects:
//    Objects are created using rather large dimensions. (for example a entity
//    has a default with of 500 units, measured in the internal coordinate
//    system). Because of this the objects can be created with very high
//    resolution.
//    Objects are descendants from TGraphicControl. A TGraphicControl has no
//    handle (thus saving resources) and is initialized with a canvas for drawing
//    the object.
//
// Large resolutions and the problem of scaling:
//    Suppose we took a low resolution for drawing the objects. We paint a
//    square of 100 units. if we display this square 1:1 on the external canvas
//    it will be 100 pixels on the screen. if the user enlarges the view to 500%
//    and drops a new object on position (103,81), the coordinates of this object
//    must be translated to the internal coordinate system. The scaling factor
//    between the internal and external system is 5. Therefore the new position
//    will be (20, 16). The object is created on this position and drawn. while
//    drawing the ScaleCanvas will translate the internal position (20, 16)
//    back to the external coordinate system using the factor 5. So the object
//    will be displayed at position (100, 80)! to the user it seems as if we
//    have implemented a grid of 5 pixels. This problem is prevented by drawing
//    high-res objects. if we create objects with an internal dimensions of 500
//    units, and use a scaling factor of 1/5 the objects will appear with a width
//    of 100 pixels on the screen. Now we use a factor of 1/5 to display objects
//    as 100% objects. Zooming 500% means adding an extra factor of 5 for the
//    zooming the resulting factor will be 1. So the unwanted grid will be 1 pixel.
//    Zooming to 1000% will result in a factor 2, therefore the unwanted grid will
//    be 2 pixels.
//
// ScaleMD and ScaleMD100:
//    ScaleMD100 stores the factor between the internal coordinate system and
//    what user sees as 100%.
//    ScaleMD is the ScaleMD100 factor multiplied with the current zoom factor.
//    ScaleMD is used by the ScaleCanvas while painting objects. ScaleMD100
//    is only stored to recalculate ScaleMD when the zoom factor changes.
//
// Paint / PaintOnCanvas / PaintOnExternal:
//    Normally an object is painted by overriding the paint routine. Several problems
//    arise when we would like to print on an external canvas. First we need to
//    pass an other canvas to the object. This is done using the PaintOnCanvas
//    method. This method is called by the paint routine, passing the canvas of
//    the object. When painting on an external canvas the routine can be called
//    with an other canvas. A second problem arises because on the external canvas
//    the objects do not have their own canvas. Therefore we need to translate
//    the relative coordinates to absolute coordinates. When something is painted
//    at the relative position (0,0) it should now be translated to the position
//    (0+Left, 0+Top).
//    This is all handled by the ScaleCanvas as soon as the property PaintOnExternal
//    is set. The canvas draws using ieX and ieY, these routines check PaintOnExternal
//    to translate the coordinates.
//
// ScaleCanvas (Advanced):
//    Sometimes you need to calculate sizes based on text widths and heights,
//    when this has to be done in the ResetBounds_Internal a canvas might not be
//    available.
//    The scalecanvas solves this problem by using an internal canvas for font
//    calculations. Allways assign the canvas of the scalecanvas before working
//    with the scalecanvas. Even if there is no canvas, assign nil to the canvas!
//
// Object creation / loading
//    A typical diagram object is create by calling the constructor followed by
//    a call to LoadFromData. The LoadFromData should be programmed to load the
//    object from the repository data associated with the object. When loading
//    an object from Json first call the constructor followed by LoadFromJson.
//    After loading RefreshFromData can be called to refetch the object from the
//    repository.
//    Please take a good look at the existing diagrams such as TcdProcess or
//    TcdEntity to see how the objects are created, loaded, painted etc. It's not
//    that difficult, but the examples explain more than thousand lines of text.
//
// Connectionpoints
//    Connectionpoint must be corresponding to grid points. Therefore if for example
//    the top left position of an object is on a grid, and the connection points
//    are calculated at runtime by dividing the width and or height, this division
//    should result in a point on the grid as well.
// -----------------------------------------------------------------------------


// -----------------------------------------------------------------------------
// General constant and type declarations
// -----------------------------------------------------------------------------

const
  cCurrentDiagramVersion = 11;
  // 1 = First verion.
  // 2 = Diagram.Stylename, Diagram.Linetype
  // 3 = Line.FromObjectCon, Line.ToObjectCon (instead of Line.Arrow)
  //     Line.FromCardText, Line.ToCardText, Line.FromComment, Line.ToComment
  //     Connector.AutoPosition
  // 4 = Line.FromMinCard ... Line.ToMaxCard
  // 5 = Diagram.LineStyle
  // 6 = Position of image
  // 7 = Scale Print
  // 8 = Save text and Plane objects
  // 9 = AllowPaperChange
  // 10 = ScaleFactor for Image

  // Font sizes (expressed as the Font.Height)
  cFontSize6   = -8;
  cFontSize7   = -9;
  cFontSize7h  = -10;
  cFontSize8   = -11;
  cFontSize9   = -12;
  cFontSize10  = -13;
  cFontSize10h = -14;
  cFontSize11  = -15;
  cFontSize12  = -16;
  cFontSize13  = -17;
  cFontSize13h = -18;
  cFontSize14  = -19;
  cFontSize15  = -20;
  cFontSize16  = -21;

  // Cannot be placed inside the style class because of the creation order.
  cScrollBarRange = 7000;
  cScrollTimerIncrement = 4;
  cScrollTimerInterval = 1;

  cMouseWheelScrollLines = 30;

  cHeightDeterminationStr = 'lMpqg!1';

  cStyleNameDefault = 'Default';

type

  { TMulDiv }

  TMulDiv = record
    M, D: integer;
    constructor create(M, D: integer);
    class operator equal(a,b : TMulDiv) : boolean;
  end;

  TNotifyPrintProgress = procedure (Sender: TObject; NrSteps, CurrentStep: integer) of object;

  TcdObjectOrigin = (ooAuto, ooUpperLeft, ooCentered);

  // Selecting = Selecting objects
  // Dragging = Dragging selected objects
  // Resizing = Resizing selected objects

  TcdDragMode = (dmNone, dmSelecting, dmDragging, dmResizing);

  TcdLineSlope = (ldTopLeft_BottomRight, ldTopRight_BottomLeft);

  TcdLineType = (
     ltInherit,
     ltStraightLine,    // Simple, straight line
     lt3PLine           // Three part line. First and last part are small and perpendicular to the object
  );

  TcdLineStyle = (
     lsInherit,
     lsSolid,
     lsDash,
     lsDot,
     lsDashDot,
     lsDashDotDot
  );

  // ObjectCon = ObjectConnector
  TcdObjectConType = (
     octNone,           // No visible object connector
     octInherit,        // Looks at the Style to determine the type
     octArrow,          // Filled arrow
     octOpenArrow,      // Open arrow
     octLineArrow,      // Two line arrow
     octClaw,           // Two line claw
     octCardDot         // Display a cardinality dot when needed (ORM Style)
  );

  TcdObjectSide = ( osNone, osTop, osBottom, osLeft, osRight );

  TcdLineTextPosition = (
     ltpMiddle, ltpFromSide_LeftOrTop, ltpFromSide_RightOrBottom, ltpToSide_LeftOrTop, ltpToSide_RightOrBottom
  );

  TcdLineSide = (lsFrom, lsTo);

  TcdGradientDirection = (gdHorizontal, gdVertical);

  TfrSizeOption = (frsoNone, frsoHorizontally, frsoVertically, frsoBoth, frsoBothKeepRatio);

  TcdDefaultObjectType = (dotNone, dotText, dotPlane, dotImage, dotRectangle);

const
  // Cardinality
  cCardNone = -1;
  cCardMany = -2;
  cCardWarning = -3;


// -----------------------------------------------------------------------------
// Forward class declarations
// -----------------------------------------------------------------------------

type
  TcdDiagram = class;
  TcdStyle = class;
  TcdStyles = class;
  TcdScrollBox = class;
  TcdSelection = class;
  TcdFocusRect = class;
  TcdScrollTimer = class;
  TcdScaleCanvas = class;
  TcdPoint = class;

  TcdObject = class;
  TcdDragObject = class;
  TcdConObject = class;
  TcdLineConData = class;
  TcdLine = class;
  TcdLinePart = class;
  TcdLineCon = class;
  TcdStaticText = class;
  TcdLineText = class;
  TcdObjectCon = class;
  TcdPageSize = class;
  TcdText = class;
  TcdPlane = class;
  TcdGrid = class;
  TcdResizeableObject = class;

  TcPageSize = class;

  TcdDiagramClass = class of TcdDiagram;

  // __________________________________________________________________________
  //
  // TcdDiagram
  // __________________________________________________________________________
  //
  // Description:
  // Actual diagram. Containing the objects, scrollbox, ScaleCanvas etc.
  // __________________________________________________________________________
  //
  TcdDiagram =
    class (TComponent)
      private
        fVersion: integer;

        // If a diagram is loaded and the style of the diagram cannot be found
        // the style will be set to the base style. This style is passed to
        // the diagram object through the constructor.
        fBaseStyle,
        fStyle: TcdStyle;

        // Identification for the type of the diagram. Used for saving in database tables
        fType: string;

        fScrollbox: TcdScrollbox;
        fScaleCanvas: TcdScaleCanvas;
        fSelection: TcdSelection;
        fActiveObject: TcdObject;

        fReadOnly: Boolean;

        fScaleMD: TMulDiv;
        fScaleMD100: TMulDiv;
        fScaleMDPrint: TMulDiv;

        fSnapToGrid: Boolean;

        fPaintOnExternal: Boolean;
        fAbortPrinting: Boolean;
        fOnPrintProgress: TNotifyPrintProgress;

        // The default popup menu, should be used for everything
        fObjectPopupMenu: TPopupMenu;

        // Used to call Update and suppress it during adding object/loading
        fChangeCount: integer;

        fPageSize: TcPageSize;

        // Indicator to suppress the Modified setting
        fLoading: Boolean;
        fModified: Boolean;

        fAllowPaperChange: Boolean;
		
        // Allow printing in unprintable margin (else you get an error)
        fPrintOnUnprintableMargin: Boolean;

        // If you don't AllowPaperChange, we display the page and the unprintable area.
        // During printing, we subtract the left and top unprintable area. Somehow this
        // doesn't work on some printers (Dymo). With DoNotUseTopLeftUnprintableMargin
        // we force a non-top/left unprintable area even with a fixed page.
        fDoNotUseTopLeftUnprintableMargin: Boolean;

        // Sets the ScaleMD (and optimizes the MulDiv)
        procedure SetScaleMD(const Value: TMulDiv);

        // Sets the ScaleMD100 (and optimizes the MulDiv)
        procedure SetScaleMD100(const Value: TMulDiv);

        procedure SetScaleMDPrint(const Value: TMulDiv);

        // Tries to reduce the multiplier or the divisor to 1.
        function OptimizeMulDiv(aMulDiv: TMulDiv): TMulDiv;

        // Calculate the min and maximum positions of all combined objects
        procedure GetObjectsIntBounds(aPrinting: Boolean; out aRect: TRect);
        procedure GetObjectsExtBounds(out aRect: TRect);
        procedure SetAllowPaperChange(const Value: Boolean);

        procedure SetStyle(aValue: TcdStyle);
      protected
        // Can this object be persistent (saved to Json) by the TcdDiagram?
        // (If so this is a 'default' Object. See CreateDefaultObject below.)
        function CanPersistObject(aObject: TcdObject; out aTypeStr: string): Boolean; virtual;

        // Create a default object based on type specified typeStr
        function CreateDefaultObject(aTypeStr: string): TcdObject; virtual;

        // Stores the percentage in the ScaleMD property en
        // Resets the bounds of the objects. (Results in a redraw).
        procedure SetScalePercentage(const Value: integer);

        // Scaling for print paper
        procedure SetScalePercentagePrint(const Value: integer);

        // Returns the ScaleMD property as a percentage.
        function GetScalePercentage: integer;
        function GetScalePercentagePrint: integer;

        procedure Notification(aComponent: TComponent; aOperation: TOperation); override;

        procedure Handle_DefaultObject_Remove(aSender: TObject);
        procedure Handle_DefaultObject_SendToBack(aSender: TObject);

        procedure Handle_LineCon_Remove(aSender: TObject);
        procedure Handle_LineCon_StraightLine(aSender: TObject);

        procedure Handle_New_Text(Sender: TObject);
        procedure Handle_New_Plane(aSender: TObject);
        procedure Handle_New_Image(Sender: TObject);

        procedure Handle_Selection_AlignLeft(aSender: TObject);
        procedure Handle_Selection_AlignRight(aSender: TObject);
        procedure Handle_Selection_AlignTop(aSender: TObject);

        procedure Handle_Selection_SpaceEqualVert(aSender: TObject);

        procedure PopulateScrollBoxPopupMenu; virtual;
        procedure PopulateLineConnectorPopupMenu(aLineCon: TcdLineCon); virtual;
        procedure PopulateLinePopupMenu(aLinePart: TcdLinePart); virtual;
        procedure PopulateSelectionPopupMenu; virtual;

        procedure DoSelectionPopupMenu(aX, aY: integer; aClickedControl: TControl);

        // Initialize the non persistent (always available) objects
        procedure InitStaticDiagramObjects; virtual;

        // Sender should be a MenuItem of a TPopupMenu
        function GetMenuPopupPosition(Sender: TObject): TPoint;

        // Something has changed (move, add, delete). The Changed can be forced;
        // this means no matter if we're in changing state, set modified.
        procedure Changed(aForce: Boolean = False); virtual;

      public
        constructor Create(aOwner: TComponent; aStylename: string); reintroduce; virtual;
        destructor Destroy; override;

        // Initializes the diagram
        procedure Init; virtual;

        // Persistency methods
        procedure LoadFromJson(aJsonRoot: TJsonObject; aForcedStyle: TcdStyle = nil); virtual;
        procedure SaveToJson(aJsonRoot: TJsonObject); virtual;

        // New, empty diagram
        procedure NewDiagram;

        // Forces a redraw of all diagram elements
        procedure ForceRedraw;

        // Refreshes the diagram contents
        procedure RefreshFromData; virtual;

        // Adds an object to the diagram, duplicates will be ignored.
        // Assigns the parent (makes it visible).
        // Sets the position (except for ooAuto objects).
        // Selects the object if aSelect is true. do not select objects while loading!
        procedure Add(aObject: TcdObject; aX, aY: integer; aSelect: Boolean = True); overload; virtual;
        procedure Add(aObject: TcdObject); overload; virtual;

        // to remove an object from the diagram simply destroy the object.
        // if you want to keep the object use Diagram.RemoveComponent(aObject);
        // procedure Remove(aObject: TcdObject);

        // Exports the diagram to an PNG file.
        procedure ExportToPng(aFileName: String; aScalePercentage: Integer);

        // Prints the diagram.
        // Use OnPrintProgress to display a gauge, use AbortPrinting to cancel.
        procedure Print(aJobTitle: string = '');

        function PageFitsOnPrinter(aZoomPercentage: integer): Boolean;
        function ChangePrinterPaperToPageSize(aZoomPercentage: integer): Boolean;

        // The default printer has changed
        procedure PrinterChanged;

        class procedure GetPaperNames(aNames: TStrings);

        // Aborts printing
        procedure AbortPrinting;

        // Clears the entire diagram, resets the scaling factor to 100%
        procedure Clear; virtual;

        // Popup menu utility routines
        function CreateMenuItem(aPopupMenu: TPopupMenu; aCaption: string; aTag: integer; aHandler: TNotifyEvent): TMenuItem;
        function GetPopupComponentFromMenuItem(aObject: TObject): TObject;
        procedure DoPopupMenu(aPopupMenu: TPopupMenu; aX, aY: integer; aPopupComponent: TControl);

        procedure BeginChange;
        procedure EndChange;
        function IsChanging: Boolean;

        // Make sure the static and back-ground objects are 'in the back'
        procedure CorrectZOrder;

        procedure DoObjectPopup(aObject: TcdObject; aX, aY: integer);

        // -- Scaling routines: used for drawing ---------------------------
        // Routines map internal coordinates onto external coordinates

        // Note:
        // Don't mix up ieX and ieWidth. ieX looks at the value of
        // the PaintOnExternal property. It will add the position of the object
        // when painting on an external canvas. Routines like ieRect use
        // the ieX and ieY functions. The function ieWidth does not use
        // the ieX routine because there is no difference between a width on an
        // internal or an external canvas.

        // Converts X (adds the IntLeft while painting on an external canvas)
        // Used for relative positioning. Snaps to grid.
        function ieX(aX: integer): integer;

        // Converts Y (adds the IntTop while painting on an external canvas)
        // Used for relative positioning. Snaps to grid.
        function ieY(aY: integer): integer;

        // Converts rectangle (using ieX, ieY)
        function ieRect(aRect: TRect): TRect;

        // Converts an array of points (using ieX, ieY)
        procedure ieArrayOfPoints(var aPoints: array of TPoint);

        // Converts font height
        function ieFontHeight(aHeight: integer): integer;
        function eiFontHeight(aHeight: integer): integer;

        // Converts X position. Used for absolute positioning.
        function ieXPos(aX: integer): integer;

        // Converts Y position. Used for absolute positioning.
        function ieYPos(aY: integer): integer;

        // Converts Width
        function ieWidth(aWidth: integer): integer;

        // Converts Height
        function ieHeight(aHeight: integer): integer;

        // Millimeters to Internal, based on current Print Scale
        function MmToInt(aMm: integer): integer;

        // -- Scaling routines: used for positioning within scrollbox ------
        // Routines map external coordinates onto internal coordinates

        // Converts X
        function eiX(aX: integer): integer;

        // Converts Y
        function eiY(aY: integer): integer;

        // Converts X position (looks at scrollbar position)
        function eiXPos(aX: integer): integer;

        // Converts Y position (looks at scrollbar position)
        function eiYPos(aY: integer): integer;

        // -- Grid routines
        function SnapToGridX(aX: integer): integer;
        function SnapToGridY(aY: integer): integer;

        // Should have been:
        // function GetObjectsOfType<T: TComponent>: TArray<T>;
        // But gives an 'Undefined symbol' - linker error in FPC
        // https://gitlab.com/freepascal.org/fpc/source/-/issues/38827
        function GetObjectsOfType(T: TClass): TArray<TcdObject>;

        // -- Properties ---------------------------------------------------

        // Version
        property Version: integer read fVersion;

        // Style
        property Style: TcdStyle read fStyle write SetStyle;

        // Identification for the type of the diagram. Used for saving in database tables
        property Type_: string read fType write fType;

        // Scrolbox containing the objects
        property Scrollbox: TcdScrollBox read fScrollBox;

        // Scale Canvas used to draw the objects
        property ScaleCanvas: TcdScaleCanvas read fScaleCanvas;

        // List with selected objects
        property Selection: TcdSelection read fSelection;

        // Selected object
        property ActiveObject: TcdObject read fActiveObject write fActiveObject;

        property ReadOnly: Boolean read fReadOnly write fReadOnly;

        // Scaling factor (from internal to external - zooms to current zoom percentage)
        property ScaleMD: TMulDiv read fScaleMD write SetScaleMD;

        // Scaling factor (from internal to external - zooms to 100%)
        property ScaleMD100: TMulDiv read fScaleMD100 write SetScaleMD100;

        // Scaling factor for paper size
        property ScaleMDPrint: TMulDiv read fScaleMDPrint write SetScaleMDPrint;

        // ScaleMD represented as a percentage
        property ScalePercentage: integer read GetScalePercentage write SetScalePercentage;

        // ScalePrint represented as a percentage
        property ScalePercentagePrint: integer read GetScalePercentagePrint write SetScalePercentagePrint;

        // Mouse positions should be snapped to the grid
        property SnapToGrid: Boolean read fSnapToGrid write fSnapToGrid;

        // Change the paper 'type' if the page doesn't fit
        property AllowPaperChange: Boolean read fAllowPaperChange write SetAllowPaperChange;

        // If true, there is no check during printing if it fits on the page
        property PrintOnUnprintableMargin: Boolean read fPrintOnUnprintableMargin write fPrintOnUnprintableMargin;

    	// Paint on an external canvas?
        // Used to perform special actions when objects are painted on an external canvas
        property PaintOnExternal: Boolean read fPaintOnExternal write fPaintOnExternal;

        property OnPrintProgress: TNotifyPrintProgress read fOnPrintProgress write fOnPrintProgress;

        property PrintingAborted: Boolean read fAbortPrinting;

        property ObjectPopupMenu: TPopupMenu read fObjectPopupMenu;

        property Modified: Boolean read fModified;

        property PageSize: TcPageSize read fPageSize;

        property DoNotUseTopLeftUnprintableMargin: Boolean read fDoNotUseTopLeftUnprintableMargin write fDoNotUseTopLeftUnprintableMargin;
      end;


  // __________________________________________________________________________
  //
  // TcdStyle
  // __________________________________________________________________________
  //
  // Description:
  // Container for all the diagram settings.
  // Can be instantiated to create an object with all default settings.
  // __________________________________________________________________________
  //
  TcdStyle =
    class
      protected
        // You should set the fe... values in the create. do not set the
        // fi... values those will be calculated by the diagram.

        fName: string;

        fMultiSelectKey: TShiftState;
        fChildSelectKey: TShiftState;

        fTextFontName: string;
        fiTextFontHeight, feTextFontHeight: integer;
        fTextFontStyle: TFontStyles;
        fTextFontColor: TColor;

        fPenColor: TColor;
        fPenColor_Selected: TColor;
        fFillColor: TColor;
        fFillColor_Selected: TColor;

        fLineType: TcdLineType;
        fLineStyle: TcdLineStyle;
        fLinePenColor: TColor;
        fiLineClickMargin, feLineClickMargin: integer;
        fiLineDrawMargin, feLineDrawMargin: integer;

        fiFixedLinePartLength, feFixedLinePartLength: integer;

        fiConnectorWidth, feConnectorWidth: integer;
        fiConnectorHeight, feConnectorHeight: integer;
        fiConnectorClickArea, feConnectorClickArea: integer;
        fConnectorPenColor: TColor;
        fConnectorFillColor: TColor;
        fConnectorPenColor_Selected: TColor;
        fConnectorFillColor_Selected: TColor;

        fObjectConPenColor: TColor;
        fObjectConFillColor: TColor;
        fFromObjectConType: TcdObjectConType;
        fToObjectConType: TcdObjectConType;
        fArrowAngleRad: Double;
        fiArrowSide, feArrowSide: integer;
        fiClawWidth, feClawWidth: integer;

        fCardinalityManyChar: Char;
        fCardinalityStrInternal: TStringlist;
        fCardinalityStrExternal: TStringlist;
        fCardinalityDotSide: TcdLineSide;
        fiCardinalityDotSize, feCardinalityDotSize: integer;
        fCardinalityWarningDotFillColor: TColor;
        fDisplayCardinality: Boolean;

        fLineTextFontName: string;
        fiLineTextFontHeight, feLineTextFontHeight: integer;
        fLineTextFontColor: TColor;
        fiLineTextMargin, feLineTextMargin: integer;

        fSnapToGrid: Boolean;
        fGridX, fGridY: integer;

        fDefaultMultiplier, fDefaultDivisor: integer;

        fiScaleMargin, feScaleMargin: integer;

        fPluralnameOfObjects: string;

      public
        constructor Create(aName: string); virtual;
        destructor Destroy; override;

        procedure DeriveProperties (aDiagram: TcdDiagram); virtual;

        procedure DeleteCardinalityStrReplacements;
        procedure AddCardinalityStrReplacement(aInternalStr, aExternalStr: string);
        function ReplaceCardinalityStr(aInternalStr: string): string;

        property Name: string read fName;

        property MultiSelectKey: TShiftState read fMultiSelectKey;
        property ChildSelectKey: TShiftState read fChildSelectKey;

        // Default text style
        property TextFontName: string read fTextFontName;
        property TextFontHeight: integer read fiTextFontHeight;
        property TextFontStyle: TFontStyles read fTextFontStyle;
        property TextFontColor: TColor read fTextFontColor;

        // Default drawing style
        property PenColor: TColor read fPenColor;
        property PenColor_Selected: TColor read fPenColor_Selected;
        property FillColor: TColor read fFillColor;
        property FillColor_Selected: TColor read fFillColor_Selected;

        property LineType: TcdLineType read fLineType;
        property LineStyle: TcdLineStyle read fLineStyle;
        property LinePenColor: TColor read fLinePenColor;

        // Click margin is used for mouse down events. It allows the user to
        // click near the line instead of exactly on the line.
        property LineClickMargin: integer read fiLineClickMargin;

        // The draw margin is used to draw horizontal/vertical lines.
        // The draw margin allways corresponds to 1 pixel in the external
        // coordinate system. (Without the drawmargin a vertical or horizontal
        // line would be drawn using a with of 0 pixels and thus be invisible).
        property LineDrawMargin: integer read fiLineDrawMargin;

        property FixedLinePartLength: integer read fiFixedLinePartLength;

        // Line connector dimentions
        property ConnectorWidth: integer read fiConnectorWidth;
        property ConnectorHeight: integer read fiConnectorHeight;
        property ConnectorClickArea: integer read fiConnectorClickArea;
        property ConnectorPenColor: TColor read fConnectorPenColor;
        property ConnectorFillColor: TColor read fConnectorFillColor;
        property ConnectorPenColor_Selected: TColor read fConnectorPenColor_Selected;
        property ConnectorFillColor_Selected: TColor read fConnectorFillColor_Selected;

        property ObjectConPenColor: TColor read fObjectConPenColor;
        property ObjectConFillColor: TColor read fObjectConFillColor;
        property FromObjectConType: TcdObjectConType read fFromObjectConType;
        property ToObjectConType: TcdObjectConType read fToObjectConType;

        property ArrowAngleRad: Double read fArrowAngleRad;
        property ArrowSide: integer read fiArrowSide;

        // Distance between the point where the line crosses the object and
        // the points where a 'clawlines' crosses to the object.
        property ClawWidth: integer read fiClawWidth;

        property CardinalityManyChar: Char read fCardinalityManyChar;
        property CardinalityDotSide: TcdLineSide read fCardinalityDotSide;
        property CardinalityDotSize: integer read fiCardinalityDotSize;
        property CardinalityWarningDotFillColor: TColor read fCardinalityWarningDotFillColor;
        property DisplayCardinality: Boolean read fDisplayCardinality;

        property LineTextFontName: string read fLineTextFontName;
        property LineTextFontHeight: integer read fiLineTextFontHeight;
        property LineTextFontColor: TColor read fLineTextFontColor;
        property LineTextMargin: integer read fiLineTextMargin;

        property SnapToGrid: Boolean read fSnapToGrid;
        property GridX: integer read fGridX;
        property GridY: integer read fGridY;

        property DefaultMultiplier: integer read fDefaultMultiplier;
        property DefaultDivisor: integer read fDefaultDivisor;

        property ScaleMargin: integer read fiScaleMargin;

        // The plural name of the objects in the drawing
        property PluralNameOfObjects: string read fPluralNameOfObjects;
      end;


  // __________________________________________________________________________
  //
  // TcdStyles
  // __________________________________________________________________________
  //
  // Description:
  // Container for diagram styles
  // __________________________________________________________________________
  //
  TcdStyles =
    class (TStringlist)
      public
        constructor Create; reintroduce;
        destructor Destroy; override;

        procedure Register_(aStyle: TcdStyle);
        function GetStyle (aName: string): TcdStyle;
      end;


  // __________________________________________________________________________
  //
  // TcdScrollBox
  // __________________________________________________________________________
  //
  // Description:
  // Handles (de)selection of object, movement of objects and scrolling of
  // the diagram.
  // __________________________________________________________________________
  //
  TcdScrollBox =
    class (TScrollBox)
        procedure WMHScroll(var Message: TWMHScroll); message WM_HSCROLL;
        procedure WMVScroll(var Message: TWMVScroll); message WM_VSCROLL;

      private
        fDiagram: TcdDiagram;
        fDragMode: TcdDragMode;
        fDragRectStart: TPoint;
        fFocusRect: TcdFocusRect;
        fScrollTimer: TcdScrollTimer;

        fLastScreenCursor: TCursor;

        fOnHorizontalScroll: TNotifyEvent;
        fOnVerticalScroll: TNotifyEvent;

        // Position where the mouse was clicked. if moving the mouse while
        // clicking, the first MouseMove has a (very) different X,Y then
        // the MouseDown
        fMouseDownPosition: TPoint;
        function GetHorizontalScrollPosition: Integer;
        function GetVerticalScrollPosition: Integer;
        procedure SetHorizontalScrollPosition(aPosition: Integer);
        procedure SetVerticalScrollPosition(aPosition: Integer);
        procedure SetLastScreenCursor(const Value: TCursor);

      protected
        function DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean; override;
        function DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean; override;

        procedure KeyUp(var Key: Word; Shift: TShiftState); override;

      public
        constructor Create (aOwner : TComponent); override;
        destructor Destroy; override;

    	procedure StartResizing(aStartPoint: TPoint; aObject: TcdResizeableObject; aSizeOption: TfrSizeOption);

        // Cancel resizing, dragging, ...
        procedure CancelDragMode;

        procedure SetMouseDownPosition(aPoint: TPoint);

        // Handles deselection of old objects and drawing of the focusrect
        procedure MouseDown (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer); override;

        // Handles drawing of the focusrect
        procedure MouseMove (aShiftState: TShiftState; aX, aY: integer); override;

        // Handles hiding of the focusrect, selects objects or moves objects
        procedure MouseUp (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer); override;

        // Scrolls the specified position into the visible area (scrolltimer must be active)
        procedure Scroll(aX, aY: integer);

        property Diagram: TcdDiagram read fDiagram;
        property DragMode: TcdDragMode read fDragMode write fDragMode;
        property DragRectStart: TPoint read fDragRectStart write fDragRectStart;

        property FocusRect: TcdFocusRect read fFocusRect;

        property LastScreenCursor: TCursor read fLastScreenCursor write SetLastScreenCursor;
        property OnHorizontalScroll: TNotifyEvent read fOnHorizontalScroll write fOnHorizontalScroll;
        property OnVerticalScroll: TNotifyEvent read fOnVerticalScroll write fOnVerticalScroll;

        property VertScrollbarPosition: integer read GetVerticalScrollPosition write SetVerticalScrollPosition;
        property HorzScrollbarPosition: integer read GetHorizontalScrollPosition write SetHorizontalScrollPosition;
      end;


  // Using T here gives a Internal Error URW1154
  TAddObjectFunc = function(constref aObject: TcdObject): Boolean;

  // __________________________________________________________________________
  //
  // TcdSelection
  // __________________________________________________________________________
  //
  // Description:
  // List with the currently selected objects.
  // __________________________________________________________________________
  //
  TcdSelection =
    class (TList<TcdObject>)
      private
        fDiagram: TcdDiagram;
        fSelectionGroup: integer;

      protected
        // Create a rect spanning all selected objects
        function GetRect: TRect;

        // Should have been:
    	// function GetObjectsOfType<T: TComponent>: TArray<T>; overload;
    	// function GetObjectsOfType<T: TComponent>(aAddObject: TAddObjectFunc): TArray<T>; overload;
      	function GetObjectsOfType(T: TClass): TArray<TcdObject>; overload;
      	function GetObjectsOfType(T: TClass; aAddObject: TAddObjectFunc): TArray<TcdObject>; overload;

      public
        constructor Create(aDiagram: TcdDiagram); reintroduce;
        destructor Destroy; override;

        // Adds an object to the list
        procedure Add(aObject: TcdObject); reintroduce;

        // Removes an object from the list
        procedure Remove(aObject: TcdObject);

        // Removes all objects from the list
        procedure Clear;

        // if the object is in the list it will be removed, if not it will be added
        procedure Toggle(aObject: TcdObject);

        // Selects every object in the specified rectangle
        procedure SelectObjectsInRect(aRect: TRect);

        // is moves accepted by every object in the list
        function AcceptsTranslate(aDelta: TPoint): Boolean;

        // is resize accepted by every object in the list
        function AcceptsResize(aNewRect: TRect): Boolean;

        // Moves every object in the list
        procedure Translate(aDelta: TPoint);

        // Aligns the seletion to the left of the first selected object
        procedure AlignLeft;
        procedure AlignRight;
        procedure AlignTop;

        // Vertical spaces between objects equally
        procedure SpaceEqualVert;

        // Rectangle spanning all selected objects
        property Rect: TRect read GetRect;
      end;


  // __________________________________________________________________________
  //
  // TcdFocusRect
  // __________________________________________________________________________
  //
  // Description:
  // Rectangle drawn while selecting objects
  // __________________________________________________________________________
  //
  TcdFocusRect =
    class (TObject)
      private
        fScrollBox: TcdScrollBox;

        // The object which is resized
        fResizeObject: TcdResizeableObject;

        fVisible: Boolean;
        fPainted: Boolean;
        fActivated: Boolean;

        fCurrentRect: TRect;
        fPreviousRect: TRect;

        fStartPoint: TPoint;
        fEndPoint: TPoint;
        fDragPoint: TPoint;
        fDelta: TPoint;

        fStartScrollBar: TPoint;

        fWidth: integer;
        fHeight: integer;

        fSizeOption: TfrSizeOption;

      protected
        // Toggles the visible property and repaints
        procedure SetVisible(aValue: Boolean);

        // Paints the rectangle
        procedure Paint;

        // Derived the rectangle using the start and end points
        procedure DeriveRect;

      public
        constructor Create(aScrollBox: TcdScrollbox); reintroduce;
        destructor Destroy; override;

        // Activates the 'Selecting' mode.
        procedure ActivateSelecting(aStartPoint: TPoint);

        // Update the selection rectangle and selects every object in the
        // rectangle.
        procedure UpdateSelecting(aEndPoint: TPoint);

        // Activates the 'Dragging' mode.
        // The dragrect represents the rectangle being dragged away,
        // the dragpoint represents the new location of the rectangle.
        procedure ActivateDragging(aDragRect: TRect; aDragPoint: TPoint);

        // Update the dragging rectangle
        procedure UpdateDragging(aDragPoint: TPoint);

        procedure ActivateResizing(aStartPoint: TPoint; aObject: TcdResizeableObject; aSizeOption: TfrSizeOption);

        // Update the resizing rectangle
        procedure UpdateResizing(aEndPoint: TPoint);

        // Deactivates 'Selection' or 'Dragging' mode
        procedure Deactivate;

        // is the rectangle visible?
        property Visible: Boolean read fVisible write SetVisible;

        // is the rectangle activated?
        property Activated: Boolean read fActivated write fActivated;

        // The rectangle
        property Rect: TRect read fCurrentRect;

        // Drag distance
        property Delta: TPoint read fDelta;

        // How should we size
        property SizeOption: TfrSizeOption read fSizeOption write fSizeOption;
      end;


  // __________________________________________________________________________
  //
  // TcdScrollTimer
  // __________________________________________________________________________
  //
  // Description:
  // Used to scroll the ScrollBox.
  // __________________________________________________________________________
  //
  TcdScrollTimer =
    class (TObject)
      private
        fScrollBox: TcdScrollBox;
        fHInc : integer;
        fVInc : integer;
        fTimer: TTimer;

      protected
        // Scrolls the scrollbox
        procedure Scroll(Sender: TObject);

      public
        constructor Create(aScrollBox : TcdScrollbox);

        // Starts scrolling
        procedure Start;

        // Stops scrolling
        procedure Stop;

        // Horizontal movement per timer tick (in pixels)
        property HInc: integer read fHInc write fHInc;

        // Vertical movement per timer tick (in pixels)
        property VInc: integer read fVInc write fVInc;
      end;


  // __________________________________________________________________________
  //
  // TcdScaleCanvas
  // __________________________________________________________________________
  //
  // Description:
  // Wrapper around the Delphi Canvas. Handles scaling.
  // __________________________________________________________________________
  //
  TcdScaleCanvas =
    class (TObject)
      private
        fDiagram: TcdDiagram;
        fCanvas: TCanvas;
        fFontHeight: integer;
        fPenPos: TPoint;

        // Do not use this property. Use functions like TextWidth, TextHeight etc
        // these will map onto the FontCalcCanvas.
        fFontCalcCanvas: TCanvas;

      protected
        // Sets the real canvas and reinitializes some properties
        procedure SetCanvas(const Value: TCanvas);

        // Property mappings (Get routines)
        function  GetFontColor: TColor;
        function  GetFontName: TFontName;
        function  GetFontHeight: integer;
        function  GetFontStyle: TFontStyles;
        function  GetFontCharset: TFontCharset;
        function  GetFontPitch: TFontPitch;
        function  GetFontOrientation: integer;
        function  GetBrushColor: TColor;
        function  GetBrushStyle: TBrushStyle;
        function  GetPenColor: TCOLOR;
        function  GetPenMode: TPenMode;
        function  GetPenStyle: TPenStyle;
        function  GetPenWidth: integer;
        function  GetLockCount: integer;

        // Property mappings (Set routines)
        procedure SetFontColor(const Value: TColor);
        procedure SetFontName(const Value: TFontName);
        procedure SetFontHeight(const Value: integer);
        procedure SetFontStyle(const Value: TFontStyles);
        procedure SetFontCharset(const Value: TFontCharset);
        procedure SetFontPitch(const Value: TFontPitch);
        procedure SetFontOrientation(const Value: integer);
    	procedure SetFontIntercharacterSpacing(const Value: Integer);
        procedure SetBrushColor(const Value: TColor);
        procedure SetBrushStyle(const Value: TBrushStyle);
        procedure SetPenColor(const Value: TColor);
        procedure SetPenMode(const Value: TPenMode);
        procedure SetPenStyle(const Value: TPenStyle);
        procedure SetPenWidth(const Value: integer);
        procedure SetPenPos(const Value: TPoint);
      public
        constructor Create (aDiagram: TcdDiagram); reintroduce;
        destructor Destroy; override;

        // After assigning a canvas:
        // - The font size will be Reset to defCanvasFontSize
        // - The penpos is Reset to 0, 0
        property Canvas: TCanvas read fCanvas write SetCanvas;

        // Font (see TFont help for more information)
        property Font_Color: TColor read GetFontColor write SetFontColor;
        property Font_Name: TFontName read GetFontName write SetFontName;
        property Font_Height: integer read GetFontHeight write SetFontHeight;
        property Font_Style: TFontStyles read GetFontStyle write SetFontStyle;
        property Font_Charset: TFontCharset read GetFontCharset write SetFontCharset;
        property Font_Pitch: TFontPitch read GetFontPitch write SetFontPitch;
        property Font_Orientation: integer read GetFontOrientation write SetFontOrientation;
    	property Font_IntercharacterSpacing: Integer write SetFontIntercharacterSpacing;

        // Brush (see TBrush help for more information)
        property Brush_Color: TColor read GetBrushColor write SetBrushColor;
        property Brush_Style: TBrushStyle read GetBrushStyle write SetBrushStyle;

        // Pen (see TPen help for more information)
        property Pen_Color: TColor read GetPenColor write SetPenColor;
        property Pen_Mode: TPenMode read GetPenMode write SetPenMode;
        property Pen_Style: TPenStyle read GetPenStyle write SetPenStyle;
        property Pen_Width: integer read GetPenWidth write SetPenWidth;

        // Drawing routines (see TCanvas help for more information)
        procedure Arc(X1, Y1, X2, Y2, X3, Y3, X4, Y4: integer);
        procedure Chord(X1, Y1, X2, Y2, X3, Y3, X4, Y4: integer);
        procedure CopyRect(const Dest: TRect; Canvas: TCanvas; const Source: TRect);
        procedure DrawFocusRect(const Rect: TRect);
        procedure Ellipse(const Rect: TRect); overload;
        procedure Ellipse(X1, Y1, X2, Y2: integer); overload;
        procedure FillRect(const Rect: TRect);
        procedure FillRect_Gradient(const Rect: TRect; const FromColor, ToColor:TColor; const GradientDirection: TcdGradientDirection);
        procedure FloodFill(X, Y: integer; Color: TColor; FillStyle: TFillStyle);
        procedure FrameRect(const Rect: TRect);
        procedure LineTo(X, Y: integer);
        procedure Lock;
        procedure MoveTo(X, Y: integer);
        procedure Pie(X1, Y1, X2, Y2, X3, Y3, X4, Y4: integer);
        procedure Polygon(Points: array of TPoint);
        procedure Polyline(Points: array of TPoint);
        procedure PolyBezier(Points: array of TPoint);
    	procedure PolyBezierTo(Points: array of TPoint);
        procedure Rectangle(X1, Y1, X2, Y2: integer); overload;
        procedure Rectangle(const Rect: TRect); overload;
        procedure Refresh;
        // Warning: the RoundRect does not print as a round rect.
        procedure RoundRect(X1, Y1, X2, Y2, X3, Y3: integer);
        procedure StretchDraw(const Rect: TRect; Graphic: TGraphic);
        function  TextExtent(const Text: string): TSize;
        function  TextHeight(const Text: string): integer;
        procedure TextOut(X, Y: integer; const Text: string);
        procedure TextRect(var aRect: TRect; const Text: string; aTextStyle: TTextStyle);

        function  TextWidth(const Text: string): integer;
        function  TryLock: Boolean;
        procedure Unlock;

        // Used to calculate custom Round Rects
        // Needs an array [0..24] of TPoint
        // A, B, C, D define the arc. Points:  A := 25; B := 10; C:= 2; D := 50;
        // 0: 0, A   0, 25
        // 1  C, B   2, 10
        // 2  B, C   10, 2
        // 3  A, 0   25, 0
        procedure DeriveRoundRectPoints (var aPoints: array of TPoint; aLeft, aTop, aRight, aBottom, A, B, C, D: integer);

        property  LockCount: integer read GetLockCount;
        property  PenPos: TPoint read fPenPos write SetPenPos;

        // property  CopyMode:
        // property  Brush.Bitmap
        // property  Pixels[X, Y: integer]: TColor read GetPixel write SetPixel;
        // procedure BrushCopy(const Dest: TRect; Bitmap: TBitmap; const Source: TRect; Color: TColor);
        //    not supported. Cannot be used for scaling graphics.
        //
        // procedure Draw(X, Y: integer; Graphic: TGraphic);
        //    not supported. Cannot be user for scaling graphics. Use StrechDraw instead.
        //
        // property Handle: HDC read GetHandle write SetHandle;
        //    not supported. Get the handle directly from the real canvas.
        //
        // property  OnChange: TNotifyEvent read FOnChange write FOnChange;
        // property  OnChanging: TNotifyEvent read FOnChanging write FOnChanging;
        //    not supported. Directly assign to events of real canvas.
        //
        // property ClipRect: TRect read GetClipRect;
        //    not supported. Re-translation of coordinates might introduce errors.
        //
        // property Font.Height
        //    not Implemented. Use size instead.
        //
        // procedure CopyRect(const Dest: TRect; Canvas: TCanvas; const Source: TRect);
        //    not tested. The implementation just translated the rects.
        //

        // The penpos is affected by the routines LineTo and MoveTo.
        // PolyBezierTo does not affect the penpos!

      end;


  // __________________________________________________________________________
  //
  // TcdPoint
  // __________________________________________________________________________
  //
  // Description:
  // Stores a point
  // __________________________________________________________________________
  //
  TcdPoint =
    class (TObject)
      private
        fX, fY: integer;

      public
        constructor Create (aX, aY : integer);

        property X: integer read fX;
        property Y: integer read fY;

        function AsPoint : TPoint;
      end;


  // __________________________________________________________________________
  //
  // TcdObject
  // __________________________________________________________________________
  //
  // Description:
  // Ancestor for objects to be draw in the diagram.
  // __________________________________________________________________________
  //
  TcdObject =
    class (TGraphicControl)
      private
        fDiagram: TcdDiagram;
        fScrollbox: TcdScrollBox;
        fSelected: Boolean;
        fClickable: Boolean;
        fPrintable: Boolean;
        fIsEmbeddedObject: Boolean;

        fNeedsRepaint: Boolean;

        fIntLeft,
        fIntTop,
        fIntWidth,
        fIntHeight: integer;
        fIntClientRect: TRect;

        fOrigin: TcdObjectOrigin;

        // ZOrder of the item; 0 is don't care. How higher the ZOrder, the
        // 'lower' it will be pushed. So an object with ZOrder 1 will be behind
        // an object with ZOrder 10
        fZOrder: integer;

      protected
        fSelectionGroup: integer;

        // Calls the routine InActiveArea to test for clickable areas
        procedure CMHitTest (var Msg: TCMHITTEST); MESSAGE CM_HITTEST;

        // Sets the selected state and repaints the object
        procedure SetSelected(aValue: Boolean); virtual;

        // Returns true if the point falls within the objects area.
        // override this method to define more complex areas.
        // The method is called frequently, avoid complex calculations.
        // Uses internal coordinates!
        function InActiveArea (aX, aY: integer): Boolean; virtual;

        // This routine is called by the setposition method. SetPosition is
        // only called when an object is moved or placed the user.
        // Sometimes a child object needs to redraw its parent because it
        // has been remove (example TcdLineCon). Use the ResetBounds_External
        // to call the ResetBounds of the parent. if you would do this in the
        // normal ResetBounds or ResetBounds_Internal, it would result in a
        // recursive call.
        procedure ResetBounds_External; virtual;

        // override the ResetBounds_Internal routine to derive and set the
        // bounds of the object. do not call the ResetBounds of associated
        // objects in this method.
        procedure ResetBounds_Internal; virtual;

        // override the ResetBounds_Children routine to call the resetbounds
        // methods of the children.
        procedure ResetBounds_Children; virtual;

        // Returns the center position
        function GetCenterPosition: TPoint;

        // override this method to correct the Z order of a complex object.
        // for example by bringing it (and its subobjects) to the front of the canvas.
        procedure CorrectZOrder; virtual;

        // Event the object has been moved (Translate called)
        procedure Moved; virtual;

        // Load from/Save to json
        procedure LoadFromJson(aJsonObject: TJsonObject); virtual;
        procedure SaveToJson(aJsonObject: TJsonObject); virtual;

        // Handles Popupmenu
        procedure MouseDown (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer); override;

        // Fill the popupmenu
        procedure PopulatePopupMenu(aMenu: TPopupMenu); virtual;

        function BringToFrontOnSelect: Boolean; virtual;

        procedure SetZOrder(TopMost: Boolean); override;

      public
        constructor Create(aDiagram: TcdDiagram; aOrigin: TcdObjectOrigin); reintroduce; virtual;
        destructor Destroy; override;

        // if this object has child TcdObject's that are created during
        // the create method, and those childs should be visually on-top of
        // this object, you should add those children to the diagram during
        // the InitChildren and not during the create.
        procedure InitChildren; virtual;

        // Load the object from the 'data' associated with the diagram
        procedure LoadFromData; virtual;

        // Reload the object from the 'data' associated with the diagram
        procedure RefreshFromData; virtual;

        // Please consider using Resetbounds_Internal or ResetBounds_Children
        // before overriding this method.
        procedure ResetBounds; virtual;

        // The object changed. Use this method to set the changed of embedded
        // objects so everything gets invalidated when needed.
        procedure Changed; virtual;

        // Paint routine, calls PaintOnCanvas with the objects canvas.
        // do not override.
        procedure Paint; override;

        // override this method to draw the object. First set the Canvas of
        // the ScaleCanvas to the specified canvas, then use the functions
        // of the scale canvas, not of the passed canvas.
        procedure PaintOnCanvas(aCanvas: TCanvas); virtual; abstract;

        // Translates aX,aY from external to internal coordinates and redraws
        procedure SetPosition(aX, aY: integer);

        // Can the move be accepted
        function AcceptsTranslate(aDelta: TPoint): Boolean; virtual;

        // Can the resize be accepted
        function AcceptsResize(aNewRect: TRect): Boolean; virtual;

        // Moves the object
        procedure Translate(aDelta: TPoint); virtual;

        // Sizes the object
        procedure Resize(aRect: TRect); reintroduce; virtual;

        // Sets the IntLeft, IntTop, IntWidth, IntHeight and IntClientRect
        // properties and redraws.
        procedure SetIntBounds(aLeft, aTop, aWidth, aHeight: integer);

        // Make this the only selected object
        procedure Select; virtual;

        // Returns the (first) plane 'below' the object
        function GetPlane: TcdPlane;

        // Determines if the specified point is on one of the object sides.
        // if the point is on a corner the result will be either osLeft or osRigth
        // and not osTop or osBottom.
        // if the point is in or outside the object the result will be osNone.
        // Minimal differences in the position are accepted (using the ScaleMargin).
        function GetObjectSide(aLine: TcdLine; aPoint: TPoint): TcdObjectSide; virtual;

        property Diagram: TcdDiagram read fDiagram;
        property ScrollBox: TcdScrollBox read fScrollBox;

        // Indicates the object is currently selected
        property Selected: Boolean read fSelected;

        // Indicates the object should be clickable
        property Clickable: Boolean read fClickable write fClickable;

        // Indicates the object should be printed
        property Printable: Boolean read fPrintable;

        // Internal
        property IntLeft: integer read fIntLeft;
        property IntTop: integer read fIntTop;
        property IntWidth: integer read fIntWidth;
        property IntHeight: integer read fIntHeight;
        property IntClientRect: TRect read fIntClientRect; // = Rect(0,0,IntWidth,IntHeight)

        property Origin: TcdObjectOrigin read fOrigin;

        property CenterPosition: TPoint read GetCenterPosition;

        property NeedsRepaint: boolean read fNeedsRepaint write fNeedsRepaint;

        // The selection group value is used to distinguish between different
        // objects that can or cannot be selected at the same time.
        // Objects with the same selection group can be selected together,
        // while object with a different selection group cannot be selected
        // together.
        // The default selection group is 0.
        property SelectionGroup: integer read fSelectionGroup;

        // is this object embedded (and maintained) by another object).
        property IsEmbeddedObject: Boolean read fIsEmbeddedObject write fIsEmbeddedObject;
        property ZOrder: integer read fZOrder;
      end;


  // __________________________________________________________________________
  //
  // TcdConObject
  // __________________________________________________________________________
  //
  // Description:
  // Connectable object. Lines can be connected to the object.
  // __________________________________________________________________________
  //
  TcdConObject =
    class (TcdObject)
      private
        fLines,
        fConnectionPoints: TcList;

      protected
        // Selects a point from the specified list with a minimal distance to the specified point
        function GetConnectionPointFromList(aListContainer: TcdObject; aList: TcList; aDestinationPoint: TPoint): TPoint;

        // Fetches a point with a minimal distance to the specified point
        function GetConnectionPoint(aLine: TcdLine; aPoint: TPoint): TPoint; virtual;

      public
        constructor Create(aDiagram: TcdDiagram; aOrigin: TcdObjectOrigin); override;
        destructor Destroy; override;

        procedure ResetBounds; override;

        function IsReferenced: Boolean;

        // Deletes all the connectionpoints
        procedure DeleteConnectionPoints; virtual;

        // Adds a connection point
        procedure AddConnectionPoint(aList: TcList; aX, aY: integer); overload;
        procedure AddConnectionPoint(aX, aY: integer); overload;

        // Connects to the specified object (Creating a line).
        function ConnectTo(aConObject: TcdConObject; aConData: TcdLineConData = nil): TcdLine; overload;

        // Connects to the specified object (Creating a line and an additional comment object)
        function ConnectTo(aConObject: TcdConObject; aComment: string; aConData: TcdLineConData = nil): TcdLine; overload;

        // Checks the connection between two objects.
        // Can only be used for simple connections, without additional an
        // additional ConData object.
        function ConnectedTo(aConObject: TcdConObject): TcdLine; overload;
        function ConnectedTo(aConObject: TcdConObject; aCommentStr: string): TcdLIne; overload;

        // Disconnects from the specified object (Removes the line)
        procedure DisconnectFrom(aConObject: TcdConObject);

        // Disconnects from every object (Removing all lines)
        procedure DisconnectFromAll;

        // Adds the line to the internal list of lines, and to the diagrams
        // list of objects. (This will implicitly set the parent)
        procedure AddLine(aLine: TcdLine);

        // Removes the line from the internal list of lines.
        procedure RemoveLine(aLine: TcdLine);

        // Lines
        property Lines: TcList read fLines;

        // Connection points
        property ConnectionPoints: TcList read fConnectionPoints;
      end;


  // __________________________________________________________________________
  //
  // TcdDragObject
  // __________________________________________________________________________
  //
  // Description:
  // Draggable object.
  // __________________________________________________________________________
  //
  TcdDragObject =
    class (TcdConObject)
      private
      protected
        // Handles (de)selection of objects
        procedure MouseDown (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer); override;

        // Translates aX, aY to screen coordinates and calls MouseUp
        procedure MouseUp (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer); override;

        // Changes the state from selecting to dragging, translates aX, aY to screen coordinates and calls MouseMove
        procedure MouseMove (aShiftState: TShiftState; aX, aY: integer); override;
      public
      end;


  // __________________________________________________________________________
  //
  // TcdLineConData
  // __________________________________________________________________________
  //
  // Description:
  // Derive your own class from this class to store data into an object thats
  // associated with a line. The object will be passed to the line through its
  // constructor and will be destroyed by the line. The object can contain any
  // information your TcdConObject descendant can use to identify and connect lines.
  // __________________________________________________________________________
  //
  TcdLineConData =
    class (TObject)
      protected
      public
        procedure LoadFromJson(aJsonObject: TJsonObject); virtual; abstract;
        procedure SaveToJson(aJsonObject: TJsonObject); virtual; abstract;

        constructor Create; virtual;
      end;


  // __________________________________________________________________________
  //
  // TcdLine
  // __________________________________________________________________________
  //
  // Description:
  // Line will be drawn from connection point to connection point. Line will be
  // straight. User can insert breaks to route the line.
  // A line is defined by a FromObject and its associated FromObjectConnectionPoint,
  // a ToObject and its associated ToObjectConnectionPoint, a StartLinePart and
  // an EndLinePart.
  // __________________________________________________________________________
  //
  TcdLine =
    class(TcdObject)
      private
        fLineType: TcdLineType;
        fLineStyle: TcdLineStyle;

        fParts,
        fConnectors: TcList;

        // Objects
        fFromObject,
        fToObject: TcdConObject;

        // Connection points on the object
        fFromPoint,
        fToPoint: TPoint;

        // Start and end line parts
        fFromLinePart,
        fToLinePart: TcdLinePart;

        // Custom connection information object
        fConData: TcdLineConData;

        // Text objects
        fComment,
        fMasterCardText,
        fDetailCardText,
        fFromComment,
        fToComment: TcdLineText;

        // Objects connectors
        fFromObjectCon,
        fToObjectCon: TcdObjectCon;

        fFromObjectConType,
        fToObjectConType: TcdObjectConType;

        fMinMasters, fMaxMasters,
        fMinDetails, fMaxDetails: integer;

      protected
        procedure ResetBounds_Internal; override;
        procedure ResetBounds_Children; override;

        function GetTextStr(aLineText: TcdLineText): string;
        function GetCommentStr: string;
        function GetMasterCardTextStr: string;
        function GetDetailCardTextStr: string;
        function GetFromCommentStr: string;
        function GetToCommentStr: string;

        procedure SetTextStr(var aLineText: TcdLineText; aText: string; aLineTextPosition: TcdLineTextPosition);
        procedure SetCommentStr(aText: string);
        procedure SetMasterCardTextStr(aText: string);
        procedure SetDetailCardTextStr(aText: string);
        procedure SetFromCommentStr(aText: string);
        procedure SetToCommentStr(aText: string);

        function GetLineType: TcdLineType;
        function GetLineStyle: TcdLineStyle; virtual;
        function GetFromObjectConType: TcdObjectConType;
        function GetToObjectConType: TcdObjectConType;

      public
        constructor Create (aDiagram: TcdDiagram; aLineType: TcdLineType; aLineStyle: TcdLineStyle; aFromObjectConType, aToObjectConType: TcdObjectConType;
                            aFromObject, aToObject: TcdConObject; aConData: TcdLineConData); reintroduce; overload;
        constructor Create (aDiagram: TcdDiagram; aFromObject, aToObject: TcdConObject; aConData: TcdLineConData); reintroduce; overload;
        destructor Destroy; override;

        procedure InitChildren; override;

        procedure LoadFromData; override;

        procedure LoadFromJson(aJsonObject: TJsonObject); override;
        procedure SaveToJson(aJsonObject: TJsonObject); override;

        procedure PaintOnCanvas(aCanvas: TCanvas); override;

        procedure CorrectZOrder; override;

        procedure RemoveConnector(aLinePartConnector: TcdLineCon);

        // Removes all visible connectors (not the AutoPosition connectors)
        procedure RemoveConnectors;

        procedure SplitLinePart (aLinePart: TcdLinePart; aX, aY: integer);

        // Adds a line part to the internal parts list and to the
        // diagrams object list. (Will implicitly set the parent).
        procedure AddLinePart(aLinePart: TcdLinePart);

        // Removes a line part from the internal list.
        procedure RemoveLinePart(aLinePart: TcdLinePart);

        // Adds a line connector to the internal connectors list and to the
        // diagrams object list. (Will implicitly set the parent).
        procedure AddLineCon(aLineCon: TcdLineCon; aX, aY: integer);

        // Removes a connector from the internal list.
        procedure RemoveLineCon(aLineCon: TcdLIneCon);

        // Copies connection points from the specified line.
        // Assumes the destination line has no connectors or only autoposition connectors.
        procedure CopyConnectors (aOldLine: TcdLine);

        procedure SetCardinality(aConObject: TcdConObject; aMin, aMax: integer);

        // Objects
        property FromObject: TcdConObject read fFromObject;
        property ToObject: TcdConObject read fToObject;

        // Custom connection information object
        property ConData: TcdLineConData read fConData write fConData;

        // Connection points on the objects
        property FromPoint: TPoint read fFromPoint;
        property ToPoint: TPoint read fToPoint;

        // Start and end line parts
        property FromLinePart: TcdLinePart read fFromLinePart write fFromLinePart;
        property ToLinePart: TcdLinePart read fToLinePart write fToLinePart;

        // Texts associated with the line
        property Comment: TcdLineText read fComment write fComment;
        property CommentStr: string read GetCommentStr write SetCommentStr;
        property MasterCardText: TcdLineText read fMasterCardText write fMasterCardText;
        property MasterCardTextStr: string read GetMasterCardTextStr write SetMasterCardTextStr;
        property DetailCardText: TcdLineText read fDetailCardText write fDetailCardText;
        property DetailCardTextStr: string read GetDetailCardTextStr write SetDetailCardTextStr;
        property FromComment: TcdLineText read fFromComment write fFromComment;
        property FromCommentStr: string read GetFromCommentStr write SetFromCommentStr;
        property ToComment: TcdLineText read fToComment write fToComment;
        property ToCommentStr: string read GetToCommentStr write SetToCommentStr;

        property LineType: TcdLineType read GetLineType write fLineType;
        property LineStyle: TcdLineStyle read GetLineStyle write fLineStyle;

        // object connectors associated with the line
        property FromObjectCon: TcdObjectCon read fFromObjectCon write fFromObjectCon;
        property ToObjectCon: TcdObjectCon read fToObjectCon write fToObjectCon;

        property FromObjectConType: TcdObjectConType read GetFromObjectConType;
        property ToObjectConType: TcdObjectConType read GetToObjectConType;

        // Cardinality
        property MinMasters: integer read fMinMasters;
        property MaxMasters: integer read fMaxMasters;
        property MinDetails: integer read fMinDetails;
        property MaxDetails: integer read fMaxDetails;

        property Parts: TcList read fParts;
        property Connectors: TcList read fConnectors;
      end;


  // __________________________________________________________________________
  //
  // TcdLinePart
  // __________________________________________________________________________
  //
  // Description:
  // Part of a line. A line consists of one or more sections.
  // The linepart is defined by either a FromConnector or the FromObject
  // and the ToConnector or the ToObject. The FromObject and ToObject are line
  // properties.
  // __________________________________________________________________________
  //
  TcdLinePart =
    class (TcdObject)
      private
        // Connectors (might be nil!)
        fFromConnector,
        fToConnector: TcdLineCon;

        // Line Slope
        fSlope : TcdLineSlope;

        fLine: TcdLine;

      protected
        function GetFromPoint: TPoint;
        function GetToPoint: TPoint;

        procedure ResetBounds_Internal; override;

        function InActiveArea (aX, aY: integer): Boolean; override;
        procedure MouseDown (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer); override;

      public
        constructor Create(aDiagram: TcdDiagram; aLine: TcdLine); reintroduce;
        destructor Destroy; override;


        procedure PaintOnCanvas(aCanvas: TCanvas); override;

        property Line: TcdLine read fLine;

        property FromPoint: TPoint read GetFromPoint;
        property ToPoint: TPoint read GetToPoint;

        // Derived information
        property FromConnector: TcdLineCon read fFromConnector write fFromConnector;
        property ToConnector: TcdLineCon read fToConnector write fToConnector;
      end;


  // __________________________________________________________________________
  //
  // TcdLineCon
  // __________________________________________________________________________
  //
  // Description:
  // Connector between two lines.
  // The line connector is defined by its FromLinePart and its ToLinePart
  // __________________________________________________________________________
  //
  TcdLineCon =
    class (TcdDragObject)
      private
        // Line parts
        fFromLinePart,
        fToLinePart: TcdLinePart;

        fLine: TcdLine;

        fAutoPosition: Boolean;

        procedure SetAutoPosition(aValue: Boolean);

      protected
        procedure ResetBounds_External; override;
        procedure ResetBounds_Internal; override;
        procedure ResetBounds_Children; override;

        procedure MouseDown (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer); override;

      public
        constructor Create (aDiagram: TcdDiagram; aLine: TcdLine; aAutoPosition: Boolean = False); reintroduce;
        destructor Destroy; override;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;

        property Line: TcdLine read fLine;

        property FromLinePart: TcdLinePart read fFromLinePart write fFromLinePart;
        property ToLinePart: TcdLinePart read fToLinePart write fToLinePart;
        property AutoPosition: Boolean read fAutoPosition write SetAutoPosition;
      end;


  // __________________________________________________________________________
  //
  // TcdStaticText
  // __________________________________________________________________________
  //
  // Description:
  // Displays a text.
  // __________________________________________________________________________
  //
  TcdStaticText =
    class (TcdObject)
      private
        fText: string;

      protected
        procedure SetText(aText: string);

      public
        constructor Create(aDiagram: TcdDiagram); reintroduce;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;

        property Text: string read fText write SetText;
      end;


  // __________________________________________________________________________
  //
  // TcdLineText
  // __________________________________________________________________________
  //
  // Description:
  // Displays a comment text next to a line.
  //
  // Derived from:
  // TcdStaticText
  // __________________________________________________________________________
  //
  TcdLineText =
    class (TcdStaticText)
      private
        fLine: TcdLine;
        fPosition: TcdLineTextPosition;

      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aLine: TcdLine; aPosition: TcdLineTextPosition); reintroduce;

        property Line: TcdLine read fLine;
      end;


  // __________________________________________________________________________
  //
  // TcdObjectCon
  // __________________________________________________________________________
  //
  // Description:
  // Arrow to be drawn at the end point of the line.
  // __________________________________________________________________________
  //
  TcdObjectCon =
    class (TcdObject)
      private
        fLine: TcdLine;
        fToPoint, fP1, fP2, fP3, fP4, fP5, fP6: TPoint;
        fType: TcdObjectConType;
        fMinCardA, fMaxCardA, fMinCardB, fMaxCardB: integer;

      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aLine: TcdLine); reintroduce;
        destructor Destroy; override;

        procedure PaintOnCanvas(aCanvas: TCanvas); override;

        property Line: TcdLine read fLine;
      end;


  // __________________________________________________________________________
  //
  // TcdPageSize
  // __________________________________________________________________________
  //
  // Description:
  // Page size marker
  // __________________________________________________________________________
  //
  TcdPageSize =
    class (TcdObject)
      private
      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram); reintroduce;

        procedure PaintOnCanvas(aCanvas: TCanvas); override;
      end;


  // __________________________________________________________________________
  //
  // TcPageSize
  // __________________________________________________________________________
  //
  // Description:
  // Page size helper
  // __________________________________________________________________________
  //
  TcPageSize =
    class
      private
        fDiagram: TcdDiagram;

        fWidth,
        fHeight: integer;

        fLandscape: Boolean;
        fPaperSize: Smallint;
        fAsize: integer;
        fText: string;

        fPrintArea: TRect;

        fLastRequiredRect: TRect;
        fLastScalePercentagePrint: integer;
        fRequiredScalePercentagePrint: integer;

        // Current printing margin (transposed upper left). External coordinates
        fPrintMargin: TPoint;
        procedure SetLandscape(const Value: Boolean);

      protected
        // Result will be true if a redraw is performed
        function Reinit(aForceUpdate: Boolean = False): boolean;

      public
        constructor Create(aDiagram: TcdDiagram);

        property Landscape: Boolean read fLandscape write SetLandscape;
        property Asize: integer read fAsize write fASize;
        // Default paper size of printer. Used to force the paper
        property PaperSize: SmallInt read fPaperSize write fPaperSize;
        property Text: string read fText;
        // Pixels of complete paper (including unprintable margins)
        property Width: integer read fWidth;
        property Height: integer read fHeight;
        property RequiredScalePercentagePrint: integer read fRequiredScalePercentagePrint;

        // Printable area, transposed to 0,0
        property PrintArea: TRect read fPrintArea;
        // Current printing margin (transposed upper left). External coordinates
        property PrintMargin: TPoint read fPrintMargin;
      end;

  // __________________________________________________________________________
  //
  // TcdText
  // __________________________________________________________________________
  //
  // Description:
  // Draggable and editale text
  // __________________________________________________________________________
  //
  TcdText =
    class (TcdDragObject)
      private
        fText: string;
        fFixedWidth: integer;
        fCentered: Boolean;
        fTextStyle: TTextStyle;
        fIntercharacterSpacing: Integer;
        // Store the FontHeight because DPI changes (switch between 2 monitors)
        // will update the Font.Height and that is undesired.
        fFontHeight: integer;

        procedure SetText(const Value: string);

      protected
        procedure ResetBounds_Internal; override;

        procedure LoadFromJson(aJsonObject: TJsonObject); override;
        procedure SaveToJson(aJsonObject: TJsonObject); override;

        procedure PopulatePopupMenu(aMenu: TPopupMenu); override;
        procedure Handle_Edit(aSender: TObject);
        procedure Handle_Font(aSender: TObject);

        // Update the fFontHeight. do not use the OnChange of the font, this is
        // used by the VCL
        procedure CMFontChanged(var Message: TMessage); message CM_FONTCHANGED;

        class destructor Destroy;

      public
        // The font used for new texts. Will be set to the last 'changed font'
        class var DefaultFont: TFont;

        // User updated the font
        procedure UpdateFont(aFont: TFont);

        constructor Create(aDiagram: TcdDiagram); reintroduce;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;

        property Text: string read fText write SetText;
        property Font;
        // in mm
        property FixedWidth: integer read fFixedWidth write fFixedWidth;

        property Centered: Boolean read fCentered write fCentered;
        property TextStyle: TTextStyle read fTextStyle  write fTextStyle;
    	property IntercharacterSpacing: Integer read fIntercharacterSpacing write fIntercharacterSpacing;
      end;


  // __________________________________________________________________________
  //
  // TcdResizeableObject
  // __________________________________________________________________________
  //
  // Description:
  // object that can be resized
  // __________________________________________________________________________
  //
  TcdResizeableObject =
    class (TcdDragObject)
      private
      protected
        fAspectRatio: Double;

        function InResizeArea(aX, aY: integer): TfrSizeOption;

        procedure MouseLeave; override;

      public
        constructor Create(aDiagram: TcdDiagram; aOrigin: TcdObjectOrigin); override;

        procedure MouseMove (aShiftState: TShiftState; aX, aY: integer); override;
        procedure MouseDown (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer); override;

        property AspectRatio: Double read fAspectRatio;
      end;


  // __________________________________________________________________________
  //
  // TcdPlane
  // __________________________________________________________________________
  //
  // Description:
  // Plane used for area's
  // __________________________________________________________________________
  //
  TcdPlane =
    class (TcdResizeableObject)
      private
        procedure Handle_Print(aSender: TObject);
        procedure Handle_SelectColor(aSender: TObject);

      protected
        fFillColor: TColor;

        procedure ResetBounds_Internal; override;
        procedure SetSelected(aValue: Boolean); override;

        procedure UpdateColor(aColor: TColor);

        function GetObjectsInPlane(aExcludeObjects: TList<TcdObject> = nil): TList<TcdDragObject>;

        type
    	  {$if FPC_fullVersion > 30202}
          // In future versions the 'reference to procedure' can be used and we can create local fucntions
    	  TFuncProcessObjectList = reference to procedure(aObject: TcdObject);
          {$endif}
          TFuncProcessObjectList = procedure(constref aObject: TcdObject; constref Args: array of const);
        procedure ProcessObjectList(aList: TList<TcdDragObject>; const aFunc: TFuncProcessObjectList; const aFuncArgs: array of const);

        procedure LoadFromJson(aJsonObject: TJsonObject); override;
        procedure SaveToJson(aJsonObject: TJsonObject); override;

        procedure PopulatePopupMenu(aMenu: TPopupMenu); override;

      public
        class var DefaultColor: TColor;

        constructor Create(aDiagram: TcdDiagram); reintroduce;

        function AcceptsTranslate(aDelta: TPoint): Boolean; override;
        function AcceptsResize(aNewRect: TRect): Boolean; override;

        procedure Translate(aDelta: TPoint); override;
        procedure Resize(aRect: TRect); override;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;

        procedure MouseDown (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer); override;
      end;


  // __________________________________________________________________________
  //
  // TcdImage
  // __________________________________________________________________________
  //
  // Description:
  // Image
  // __________________________________________________________________________
  //
  TcdImage =
    class (TcdResizeableObject)
      private
      protected
        fImage: TImage;
        fScaleFactor: Double;

        procedure ResetBounds_Internal; override;

        procedure LoadFromJson(aJsonObject: TJsonObject); override;
        procedure SaveToJson(aJsonObject: TJsonObject); override;

        procedure PopulatePopupMenu(aMenu: TPopupMenu); override;
        procedure Handle_Load(Sender: TObject);
        procedure Handle_ResetSize(Sender: TObject);
        procedure ResetSize;

        function BringToFrontOnSelect: Boolean; override;
        procedure SetScaleFactor(const aValue: Double);

      public
        constructor Create(aDiagram: TcdDiagram); reintroduce;
        destructor Destroy; override;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;

        procedure LoadFromFile(const aFileName: string);
        procedure AssignImage(aSource: TBitmap);

        property ScaleFactor: Double read fScaleFactor write SetScaleFactor;
      end;


  // __________________________________________________________________________
  //
  // TcdGrid
  // __________________________________________________________________________
  //
  // Description:
  //
  // __________________________________________________________________________
  //
  TcdGrid =
    class (TcdObject)
      private
        // in 0.1 MM
        fSideMargin,
        fTopMargin,
        fLabelWidth,
        fLabelHeight,
        fLabelMarginHor,
        fLabelMarginVert: integer;

        fRects: TList<TRect>;

      protected
        procedure ResetBounds_Internal; override;

        procedure LoadFromJson(aJsonObject: TJsonObject); override;
        procedure SaveToJson(aJsonObject: TJsonObject); override;

        procedure PopulatePopupMenu(aMenu: TPopupMenu); override;
    	procedure Handle_Clear(Sender: TObject);
    	function BringToFrontOnSelect: Boolean; override;
      public

        constructor Create(aDiagram: TcdDiagram); reintroduce;
        destructor Destroy; override;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;
        property Rects: TList<TRect> read fRects;

    	property SideMargin: Integer read fSideMargin write fSideMargin;
    	property TopMargin: Integer read fTopMargin write fTopMargin;
    	property LabelWidth: Integer read fLabelWidth write fLabelWidth;
    	property LabelHeight: Integer read fLabelHeight write fLabelHeight;
    	property LabelMarginHor: Integer read fLabelMarginHor write fLabelMarginHor;
    	property LabelMarginVert: Integer read fLabelMarginVert write fLabelMarginVert;
      end;


  // __________________________________________________________________________
  //
  // TcdRectangle
  // __________________________________________________________________________
  //
  // Description:
  // Draggable and editale rectangle
  // __________________________________________________________________________
  //
  TcdRectangle =
    class (TcdResizeableObject)
      private
        fFillColor: TColor;
        fPenColor: TColor;
        procedure SetFillColor(const Value: TColor);
        procedure SetPenColor(const Value: TColor);

      protected
        procedure ResetBounds_Internal; override;

        procedure LoadFromJson(aJsonObject: TJsonObject); override;
        procedure SaveToJson(aJsonObject: TJsonObject); override;

      public
        constructor Create(aDiagram: TcdDiagram); reintroduce;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;

        property PenColor: TColor read fPenColor write SetPenColor;
        property FillColor: TColor read fFillColor write SetFillColor;
      end;


// -----------------------------------------------------------------------------
// Utility routines
// -----------------------------------------------------------------------------

function Distance(aPoint1, aPoint2: TPoint): Real;

procedure SwapInt(var aInt1, aInt2: integer);

function LineStyleToPenStyle(aLineStyle: TcdLineStyle): TPenStyle;


// -----------------------------------------------------------------------------
// Persistency errors
// -----------------------------------------------------------------------------

// To be called from descendant objects
procedure InvalidJson(aMessage: string);
procedure InvalidJson_xExpected(aName: string);


// -----------------------------------------------------------------------------
// Globals
// -----------------------------------------------------------------------------
var
   DiagramStyles: TcdStyles;

implementation

uses
   System.UITypes, Generics.Defaults, Printers, Math, GraphMath,
   Dialogs;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Common routines
//

function Distance(aPoint1, aPoint2: TPoint): Real;
  var
    DX, DY: integer;
  begin
  DX := aPoint2.X-aPoint1.X;
  DY := aPoint2.Y-aPoint1.Y;
  result := Sqrt(DX*DX+DY*DY);
  end;

procedure SwapInt(var aInt1, aInt2: integer);
  var
    T: integer;
  begin
  T := aInt1;
  aInt1 := aInt2;
  aInt2 := T;
  end;

function LineStyleToPenStyle(aLineStyle: TcdLineStyle): TPenStyle;
  begin
  result := psSolid;
  case aLineStyle of
    // lsInherit: Ignore, handled by getter
    lsSolid      : result := psSolid;
    lsDash       : result := psDash;
    lsDot        : result := psDot;
    lsDashDot    : result := psDashDot;
    lsDashDotDot : result := psDashDotDot;
    end;
  end;

// Create a Rect from a EndPoint and EndPoint. But make sure the Rect is 'positive'
// if EndPoint X or Y are less than StartPoint
function PointsToPositiveRect(aStartPoint, aEndPoint: TPoint): TRect;
  var
    X1, X2, Y1, Y2 : integer;
  begin
  if aStartPoint.X > aEndPoint.X
     then begin X1 := aEndPoint.X;   X2 := aStartPoint.X; end
     else begin X1 := aStartPoint.X; X2 := aEndPoint.X;   end;

  if aStartPoint.Y > aEndPoint.Y
     then begin Y1 := aEndPoint.Y;   Y2 := aStartPoint.Y; end
     else begin Y1 := aStartPoint.Y; Y2 := aEndPoint.Y;   end;

  result := Classes.Rect(X1, Y1, X2, Y2);
  end;


function IntersectRect(const R1, R2: TRect): Boolean; overload;
  var
    lIntersectedRect: TRect;
  begin
  lIntersectedRect := TRect.Empty;
  Result := IntersectRect(lIntersectedRect, R1, R2);
  end;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Display / persistency helpers
//

procedure InvalidJson(aMessage: string);
  begin
  EUsr('Error while loading diagram: %s', [aMessage]);
  end;

procedure InvalidJson_xExpected(aName: string);
  begin
  EUsr('Error while loading diagram. Value ''%s'' expected.', [aName]);
  end;

function StringToDefaultObjectType(aTypeStr: string): TcdDefaultObjectType;
  begin
  if CompareText(aTypeStr, 'text')=0
     then result := dotText
  else if CompareText(aTypeStr, 'plane')=0
     then result := dotPlane
  else if CompareText(aTypeStr, 'image')=0
     then result := dotImage
  else if CompareText(aTypeStr, 'rectangle')=0
     then result := dotRectangle
     else result := dotNone;
  end;

function ObjectToDefaultObjectTypeStr(aObject: TcdObject): string;
  begin
  if aObject is TcdText
     then result := 'text'
  else if aObject is TcdPlane
     then result := 'plane'
  else if aObject is TcdImage
     then result := 'image'
  else if aObject is TcdRectangle
     then result := 'rectangle'
  else result := '';
  end;

function LineTypeToStr(aLineType: TcdLineType): string;
  begin
  case aLineType of
   ltInherit: result := 'inherit';
   ltStraightLine: result := 'straight';
   lt3PLine: result := 'threepart';
   else result := '';
   end;
  end;

function StrToLineType(aStr: string): TcdLineType;
  begin
  result := ltInherit;
  if CompareText(aStr, 'inherit')=0
     then result := ltInherit
  else if CompareText(aStr, 'straight')=0
     then result := ltStraightLine
  else if CompareText(aStr, 'threepart')=0
     then result := lt3PLine
  else EInt('StrToLineType', 'Unexpected line type: %s.', [aStr]);
  end;

function LineStyleToStr(aLineStyle: TcdLineStyle): string;
  begin
  case aLineStyle of
    lsInherit: result := 'inherit';
    lsSolid: result := 'solid';
    lsDash: result := 'dash';
    lsDot: result := 'dot';
    lsDashDot: result := 'dashdot';
    lsDashDotDot: result := 'dashdotdot';
    else result := '';
    end;
  end;

function StrToLineStyle(aStr: string): TcdLineStyle;
  begin
  result := lsInherit;
  if CompareText(aStr, 'inherit')=0
     then result := lsInherit
  else if CompareText(aStr, 'solid')=0
     then result := lsSolid
  else if CompareText(aStr, 'dash')=0
     then result := lsDash
  else if CompareText(aStr, 'dot')=0
     then result := lsDot
  else if CompareText(aStr, 'dashdot')=0
     then result := lsDashDot
  else if CompareText(aStr, 'dashdotdot')=0
     then result := lsDashDotDot
  else EInt('StrToLineStyle', 'Unexpected line style: %s.', [aStr]);
  end;

function StrToFontStyles(aStr: string): TFontStyles;
  var
    i: integer;
    lStyleStrs: TStringlist;
  begin
  result := [];
  lStyleStrs := StringToStringlist(aStr, '_,');
  try
    for i := 0 to lStyleStrs.Count-1 do
      begin
      if CompareText(aStr, 'bold')=0
         then result := result + [fsBold]
      else if CompareText(aStr, 'italic')=0
         then result := result + [fsItalic]
      else if CompareText(aStr, 'underline')=0
         then result := result + [fsUnderline]
      else if CompareText(aStr, 'strikeout')=0
         then result := result + [fsStrikeout]
      else EInt('StrToFontStyles', 'Unexpected font style: %s.', [aStr]);
      end;
  finally
    lStyleStrs.Free;
    end;
  end;

function FontStylesToStr(aStyles: TFontStyles): string;
  var
    lStyleStrs: TStringlist;
  begin
  lStyleStrs := TStringlist.Create;
  try
    if fsBold in aStyles then lStyleStrs.Add('bold');
    if fsItalic in aStyles then lStyleStrs.Add('italic');
    if fsUnderline in aStyles then lStyleStrs.Add('underline');
    if fsStrikeout in aStyles then lStyleStrs.Add('strikeout');

    result := StringListToString(lStyleStrs, '_');
  finally
    lStyleStrs.Free;
    end;
  end;


type
  // Helper to access the RequiredState
  TCanvasHelper = class helper for TCanvas
  protected
    procedure MakeSureHandleIsAlllocated;
  end;

procedure TCanvasHelper.MakeSureHandleIsAlllocated;
begin
  // Force the scrollbox to get a handle. On Lazarus if we scroll before doing anything
  // with the canvas and next call e.g. the DrawFocusRect, the positions are out of sync and
  // weird things happen. That's because there was no handle.
  // TControlScrollBar.SetPosition:  if HandleAllocated and (GetScrollPos(ControlHandle, IntfBarKind[Kind]) <> FPosition) then
  // --> false.
  // So the wincontrol and the scrollbox are unlinked the DrawFocusRect behaves weird. So make sure we
  // have a handle.
  RequiredState([csHandleValid]);
end;

{ TMulDiv }

constructor TMulDiv.create(M, D: integer);
  begin
  self.M := M;
  self.D := D;
  end;

class operator TMulDiv.equal(a, b: TMulDiv): boolean;
  begin
  result := (a.D = b.D) and (a.M = b.M);
  end;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdDiagram
//
procedure TcdDiagram.SetAllowPaperChange(const Value: Boolean);
  begin
  fAllowPaperChange := Value;
  fPageSize.Reinit(True);
  end;

procedure TcdDiagram.SetStyle(AValue: TcdStyle);
  begin
  if fStyle = AValue then Exit;
  fStyle := AValue;
  if Assigned(fStyle) then
    fStyle.DeriveProperties(Self);
  end;

procedure TcdDiagram.SetScaleMD(const Value: TMulDiv);
  begin
  if (fScaleMD.M=Value.M) and (fScaleMD.D=Value.D) then exit;
  fScaleMD := OptimizeMulDiv(Value);
  end;

procedure TcdDiagram.SetScaleMD100(const Value: TMulDiv);
  begin
  if (fScaleMD100.M=Value.M) and (fScaleMD100.D=Value.D) then exit;
  fScaleMD100 := OptimizeMulDiv(Value);
  fStyle.DeriveProperties(Self);
  end;

procedure TcdDiagram.SetScaleMDPrint(const Value: TMulDiv);
  begin
  if (fScaleMDPrint.M=Value.M) and (fScaleMDPrint.D=Value.D) then exit;
  fScaleMDPrint := OptimizeMulDiv(Value);
  end;

function TcdDiagram.OptimizeMulDiv(aMulDiv: TMulDiv): TMulDiv;
  begin
  if (aMulDiv.M mod aMulDiv.D) = 0
     then begin
          aMulDiv.M := aMulDiv.M div aMulDiv.D;
          aMulDiv.D := 1;
          end;

  if (aMulDiv.D mod aMulDiv.M) = 0
     then begin
          aMulDiv.D := aMulDiv.D div aMulDiv.M;
          aMulDiv.M := 1;
          end;
  result := aMulDiv;
  end;

procedure TcdDiagram.SetScalePercentage(const Value: integer);
  var
    lNewScaleMD: TMulDiv;
  begin
  lNewScaleMD := TMulDiv.Create(ScaleMD100.M*Value, ScaleMD100.D*100);

  if lNewScaleMD <> ScaleMD
    then begin
         ScaleMD := lNewScaleMD;
         Changed(true);
         end;
  end;

procedure TcdDiagram.SetScalePercentagePrint(const Value: integer);
  begin
  ScaleMDPrint := TMulDiv.Create(Value, 100);
  Changed;
  end;

function TcdDiagram.GetScalePercentage: integer;
  begin
  result := Round(ScaleMD.M*ScaleMD100.D*100/ScaleMD.D/ScaleMD100.M);
  end;

function TcdDiagram.GetScalePercentagePrint: integer;
  begin
  result := Round(ScaleMDPrint.M*100/ScaleMDPrint.D);
  end;

constructor TcdDiagram.Create(aOwner: TComponent; aStylename: string);
  begin
  BeginChange;
  try
    inherited Create(AOwner);

    fVersion := cCurrentDiagramVersion;

    // Do not use the SetStyle method, this would force a redraw.
    fStyle := DiagramStyles.GetStyle(aStylename);
    if not Assigned(Style)
       then EInt('TcdDiagram.Create', 'Invalid diagram style ''%s''', [aStylename]);
    fBaseStyle := fStyle;

    if aOwner is TcdScrollBox
       then begin
            fScrollBox := TcdScrollbox(aOwner);
            ScrollBox.fDiagram := Self;
            end
       else fScrollBox := nil;

    fSelection := TcdSelection.Create(Self);
    fScaleCanvas := TcdScaleCanvas.Create(Self);
    fScaleMD100 := TMulDiv.Create(1,1);
    fScaleMD := TMulDiv.Create(1,1);
    fScaleMDPrint := TMulDiv.Create(1,1);

    fSnapToGrid := Style.SnapToGrid;
    fReadOnly := False;

    fOnPrintProgress := nil;
    fPaintOnExternal := False;

    fObjectPopupMenu := TPopupMenu.Create(nil);

    fAllowPaperChange := True;

    // Update the style
    fStyle.DeriveProperties(Self);

    fPageSize := TcPageSize.Create(Self);

    InitStaticDiagramObjects;
  finally
    EndChange;
    end;
  end;

destructor TcdDiagram.Destroy;
  begin
  BeginChange; // No end change
  try
    Clear;
    FreeAndNil(fSelection);
    FreeAndNil(fScaleCanvas);
    FreeAndNil(fObjectPopupMenu);
    FreeAndNil(fPageSize);
  finally
    inherited Destroy;
    end;
  end;

procedure TcdDiagram.NewDiagram;
  begin
  Clear;
  InitStaticDiagramObjects;

  fModified := False;
  end;

procedure TcdDiagram.Notification(aComponent: TComponent; aOperation: TOperation);
  begin
  inherited;
  if Assigned(aComponent)
     then begin
          if (aOperation = opRemove) and Assigned(fSelection) and (aComponent IS TcdObject)
             then Selection.Remove(TcdObject(aComponent));
          end;
  end;

procedure TcdDiagram.Handle_LineCon_Remove(aSender: TObject);
  var
    lLine: TcdLine;
    lConnector: TcdLineCon;
  begin
  BeginChange;
  lConnector := TcdLineCon(GetPopupComponentFromMenuItem(aSender));
  lLine := lConnector.Line;
  lLine.RemoveConnector(lConnector);
  EndChange;
  end;

procedure TcdDiagram.Handle_LineCon_StraightLine(aSender: TObject);
  var
    lLine: TcdLine;
    lConnector: TcdLineCon;
  begin
  BeginChange;
  lConnector := TcdLineCon(GetPopupComponentFromMenuItem(aSender));
  lLine := lConnector.Line;
  lLine.RemoveConnectors;
  EndChange;
  end;

procedure TcdDiagram.Handle_New_Text(Sender: TObject);
  var
    lText: TcdText;
    lTextStr: string;
    lPosition: TPoint;
  begin
  lTextStr := '';
  if InputQuery('Text', 'New text', lTextStr)
     then begin
          lText := TcdText.Create(Self);
          lText.Text := lTextStr;

          lPosition := GetMenuPopupPosition(Sender);

          Add(lText, lPosition.X, lPosition.Y, False);
          end;
  end;

procedure TcdDiagram.Handle_New_Image(Sender: TObject);
  var
    lImage: TcdImage;
    lFileName: string = '';
    lPosition: TPoint;
  begin
  if PromptForOpenFileName('Load file', '*.*', '', 'All files (*.*)|*.*', lFileName)
     then begin
          lImage := TcdImage.Create(Self);
          lImage.LoadFromFile(lFileName);

          lPosition := GetMenuPopupPosition(Sender);
          Add(lImage, lPosition.X, lPosition.Y, False);
          end;
  end;

procedure TcdDiagram.Handle_DefaultObject_Remove(aSender: TObject);
  var
    lObject: TcdObject;
  begin
  lObject := GetPopupComponentFromMenuItem(aSender) AS TcdObject;
  lObject.Free;
  end;

procedure TcdDiagram.Handle_DefaultObject_SendToBack(aSender: TObject);
  var
    lObject: TcdObject;
  begin
  lObject := GetPopupComponentFromMenuItem(aSender) AS TcdObject;
  lObject.SendToBack;
  CorrectZOrder;
  end;

procedure TcdDiagram.Handle_Selection_AlignLeft(aSender: TObject);
  begin
  Selection.AlignLeft;
  end;

procedure TcdDiagram.Handle_Selection_AlignRight(aSender: TObject);
  begin
  Selection.AlignRight;
  end;

procedure TcdDiagram.Handle_Selection_AlignTop(aSender: TObject);
  begin
  Selection.AlignTop;
  end;

procedure TcdDiagram.Handle_Selection_SpaceEqualVert(aSender: TObject);
  begin
  Selection.SpaceEqualVert;
  end;

procedure TcdDiagram.Handle_New_Plane(aSender: TObject);
  var
    lPlane: TcdPlane;
    lPosition: TPoint;
  begin
  lPlane := TcdPlane.Create(Self);
  lPosition := GetMenuPopupPosition(aSender);

  Add(lPlane, lPosition.X, lPosition.Y, False);
  end;

function TcdDiagram.CreateMenuItem(aPopupMenu: TPopupMenu; aCaption: string; aTag: integer; aHandler: TNotifyEvent): TMenuItem;
  begin
  result := TMenuItem.Create(aPopupMenu);
  with result do
    begin
    Caption := aCaption;
    Tag := aTag;
    OnClick := aHandler;
    end;
  aPopupMenu.Items.Add(result);
  end;

class procedure TcdDiagram.GetPaperNames(aNames: TStrings);
  begin
  aNames.Assign(Printer.PaperSize.SupportedPapers);
  end;

function TcdDiagram.GetPopupComponentFromMenuItem(aObject: TObject): TObject;
  begin
  result := TPopupMenu(TMenuItem(aObject).GetParentComponent).PopupComponent;
  end;

procedure TcdDiagram.DoObjectPopup(aObject: TcdObject; aX, aY: integer);
  begin
  ObjectPopupMenu.Items.Clear;
  aObject.PopulatePopupMenu(ObjectPopupMenu);
  if ObjectPopupMenu.Items.Count > 0
     then begin
          aObject.Select;
          DoPopupMenu(ObjectPopupMenu, aX, aY, aObject);
          end;
  end;

procedure TcdDiagram.DoPopupMenu(aPopupMenu: TPopupMenu; aX, aY: integer; aPopupComponent: TControl);
  var
    P: TPoint;
  begin
  // Only pop if the menu has items
  if aPopupMenu.Items.Count > 0
     then begin
          P := aPopupComponent.ClientToScreen(Types.Point(aX, aY));
          aPopupMenu.PopupComponent := aPopupComponent;
          aPopupMenu.Popup (P.X, P.Y);
          end;
  end;

procedure TcdDiagram.PopulateScrollBoxPopupMenu;
  begin
  ObjectPopupMenu.Items.Clear;

  if Selection.Count = 0
     then begin
          CreateMenuItem(ObjectPopupMenu, 'New &Text', 0,  Handle_New_Text);
          CreateMenuItem(ObjectPopupMenu, 'New &Plane', 0,  Handle_New_Plane);
          CreateMenuItem(ObjectPopupMenu, 'New &Image', 0,  Handle_New_Image);
          end
     else PopulateSelectionPopupMenu();
  end;

procedure TcdDiagram.PopulateLineConnectorPopupMenu(aLineCon: TcdLineCon);
  begin
  ObjectPopupMenu.Items.Clear;
  CreateMenuItem(ObjectPopupMenu, '&Delete connector', 0, Handle_LineCon_Remove);
  CreateMenuItem(ObjectPopupMenu, '&Straight line', 0, Handle_LineCon_StraightLine);
  end;

procedure TcdDiagram.PopulateLinePopupMenu(aLinePart: TcdLinePart);
  begin
  ObjectPopupMenu.Items.Clear;
  end;

procedure TcdDiagram.PopulateSelectionPopupMenu;
  begin
  ObjectPopupMenu.Items.Clear;
  if Selection.Count > 1
     then begin
          CreateMenuItem(ObjectPopupMenu, '&Align Left', 0, Handle_Selection_AlignLeft);
          CreateMenuItem(ObjectPopupMenu, '&Align Right', 0, Handle_Selection_AlignRight);
          CreateMenuItem(ObjectPopupMenu, '&Align Top', 0, Handle_Selection_AlignTop);
          CreateMenuItem(ObjectPopupMenu, '&Space Equally Vertical', 0, Handle_Selection_SpaceEqualVert);
          end;
  end;

procedure TcdDiagram.DoSelectionPopupMenu(aX, aY: integer; aClickedControl: TControl);
  begin
  PopulateSelectionPopupMenu;
  DoPopupMenu(ObjectPopupMenu, aX, aY, aClickedControl);
  end;

procedure TcdDiagram.Init;
  begin
  ScaleMD100   := TMulDiv.Create(Style.DefaultMultiplier, Style.DefaultDivisor);
  ScaleMD      := TMulDiv.Create(Style.DefaultMultiplier, Style.DefaultDivisor);
  ScaleMDPrint := TMulDiv.Create(1, 1);
  end;

procedure TcdDiagram.InitStaticDiagramObjects;
  begin
  // Add(TcdPageSize.Create(Self));
  end;
   
function TcdDiagram.IsChanging: Boolean;
  begin
  result := fChangeCount > 0;
  end;

procedure TcdDiagram.LoadFromJson(aJsonRoot: TJsonObject; aForcedStyle: TcdStyle = nil);
  var
    lMulDiv: TMulDiv;
    lStyleName: string;
    lPosition: TRect;
    lMultiplier, lDivisor: integer;
    i: integer;
    lJsonItems: TJsonArray;
    lJsonItem: TJsonObject;
    lJsonValue: TJsonValue;
    lDefaultObjectTypeStr: string;
    lObject: TcdObject;
  begin
  fLoading := True;

  BeginChange;

  try
    InitStaticDiagramObjects;
    // Version
    if not aJsonRoot.TryGetValue('version', fVersion) then InvalidJson_xExpected('version');

    // Style name
    if not aJsonRoot.TryGetValue('style', lStyleName) then InvalidJson_xExpected('style');
    Style := DiagramStyles.GetStyle(lStylename);

    // Style is no longer present, reset to the base style
    if not Assigned(fStyle)
       then Style := fBaseStyle;

    // Overrule the loaded style?
    if Assigned(aForcedStyle)
       then Style := aForcedStyle;

    // Scale 100
    if not aJsonRoot.TryGetValue('scale100Multiplier', lMultiplier) then InvalidJson_xExpected('scale100Multiplier');
    if not aJsonRoot.TryGetValue('scale100Divisor', lDivisor) then InvalidJson_xExpected('scale100Divisor');
    lMulDiv.M := lMultiplier;
    lMulDiv.D := lDivisor;
    SetScaleMD100(lMulDiv);

    // Scale Zoom
    if not aJsonRoot.TryGetValue('scaleZoomMultiplier', lMultiplier) then InvalidJson_xExpected('scaleZoomMultiplier');
    if not aJsonRoot.TryGetValue('scaleZoomDivisor', lDivisor) then InvalidJson_xExpected('scaleZoomDivisor');
    lMulDiv.M := lMultiplier;
    lMulDiv.D := lDivisor;
    SetScaleMD(lMulDiv);

    // Scale Print
    if not aJsonRoot.TryGetValue('scalePrintMultiplier', lMultiplier) then InvalidJson_xExpected('scalePrintMultiplier');
    if not aJsonRoot.TryGetValue('scalePrintDivisor', lDivisor) then InvalidJson_xExpected('scalePrintDivisor');
    lMulDiv.M := lMultiplier;
    lMulDiv.D := lDivisor;
    SetScaleMDPrint(lMulDiv);

    // Allow Paper Change
    if not aJsonRoot.TryGetValue('allowPaperChange', fAllowPaperChange) then InvalidJson_xExpected('allowPaperChange');

    // Position
    if not aJsonRoot.TryGetValue('scrollBarLeft', lPosition.Left) then InvalidJson_xExpected('scrollBarLeft');
    if not aJsonRoot.TryGetValue('scrollBarTop', lPosition.Top) then InvalidJson_xExpected('scrollBarTop');
    fScrollBox.HorzScrollbarPosition := lPosition.Left;
    fScrollBox.VertScrollbarPosition := lPosition.Top;

    // Default Objects
    if not aJsonRoot.TryGetValue('defaultObjects', lJsonItems) then InvalidJson_xExpected('defaultObjects');
    for i := 0 to lJsonItems.Count-1 do
      begin
      lJsonValue := lJsonItems[i];
      if not (lJsonValue is TJsonObject) then InvalidJson_xExpected('object');
      lJsonItem := lJsonValue as TJsonObject;

      // bug? if not lJsonItems.TryGetValue('type', lDefaultObjectTypeStr) then InvalidJson_xExpected('type');
      if not lJsonItem.TryGetValue('type', lDefaultObjectTypeStr) then InvalidJson_xExpected('type');

      lObject := CreateDefaultObject(lDefaultObjectTypeStr);
      Add(lObject, 0, 0, False);
      if Assigned(lObject)
         then lObject.LoadFromJson(lJsonItem)
         else InvalidJson('Unknown default object type');
      end;

    CorrectZOrder;

  finally
    EndChange;

    fModified := False;
    end;
  end;

function TcdDiagram.MmToInt(aMm: integer): integer;
  begin
  result := MulDiv(MulDiv(Round(Screen.PixelsPerInch*aMM/10/2.54), ScaleMD100.D, ScaleMD100.M), ScaleMDPrint.D, ScaleMDPrint.M);
  end;


procedure TcdDiagram.SaveToJson(aJsonRoot: TJsonObject);
  var
    lStylename: string;
    I: integer;
    lObject: TcdObject;
    lStr: string;
    lJsonObjects: TJsonArray;
    lJsonObject: TJsonObject;
  begin
  fModified := False;

  // Version
  fVersion := cCurrentDiagramVersion; // Upgrade to latest version
  aJsonRoot.AddPair('version', TJsonIntegerNumber.Create(fVersion));

  // Style name
  lStylename := Style.Name;
  aJsonRoot.AddPair('style', lStylename);

  // Scale 100
  aJsonRoot.AddPair('scale100Multiplier', TJsonIntegerNumber.Create(fScaleMD100.M));
  aJsonRoot.AddPair('scale100Divisor', TJsonIntegerNumber.Create(fScaleMD100.D));

  // Scale Zoom
  aJsonRoot.AddPair('scaleZoomMultiplier', TJsonIntegerNumber.Create(fScaleMD.M));
  aJsonRoot.AddPair('scaleZoomDivisor', TJsonIntegerNumber.Create(fScaleMD.D));

  // Scale Print
  aJsonRoot.AddPair('scalePrintMultiplier', TJsonIntegerNumber.Create(fScaleMDPrint.M));
  aJsonRoot.AddPair('scalePrintDivisor', TJsonIntegerNumber.Create(fScaleMDPrint.D));

  // Allow paper change
  aJsonRoot.AddPair('allowPaperChange', TJsonBoolean.Create(fAllowPaperChange));

  // Position
  aJsonRoot.AddPair('scrollBarLeft', TJsonIntegerNumber.Create(fScrollBox.HorzScrollbarPosition));
  aJsonRoot.AddPair('scrollBarTop', TJsonIntegerNumber.Create(fScrollBox.VertScrollbarPosition));

  // Default Objects
  lJsonObjects := TJsonArray.Create;
  aJsonRoot.AddPair('defaultObjects', lJsonObjects);
  for I := 0 to ComponentCount - 1 do
    begin
    lObject := Components[I] AS TcdObject;
    if CanPersistObject(lObject, lStr)
       then begin
            if lStr<>''
               then begin
                    lJsonObject := TJsonObject.Create;
                    lJsonObject.AddPair('type', lStr);
                    lObject.SaveToJson(lJsonObject);
                    lJsonObjects.Add(lJsonObject);
                    end;
            end;
    end;
  end;


function TcdDiagram.CanPersistObject(aObject: TcdObject; out aTypeStr: string): Boolean;
  begin
  if aObject is TcdText
     then aTypeStr := 'text'
  else if aObject is TcdPlane
     then aTypeStr := 'plane'
  else if aObject is TcdImage
     then aTypeStr := 'image'
  else if aObject is TcdRectangle
     then aTypeStr := 'rectangle'
  else aTypeStr := '';

  result := aTypeStr <> '';
  end;

function TcdDiagram.CreateDefaultObject(aTypeStr: string): TcdObject;
  var
    lType: TcdDefaultObjectType;
  begin
  lType := StringToDefaultObjectType(aTypeStr);
  case lType of
    dotText: result := TcdText.Create(Self);
    dotPlane: result := TcdPlane.Create(Self);
    dotImage: result := TcdImage.Create(Self);
    dotRectangle: result := TcdRectangle.Create(Self);
    else result := nil;
    end;
  end;

procedure TcdDiagram.ForceRedraw;
  var
    T: integer;
  begin
  for T := 0 to ComponentCount-1 do
    (Components[T] AS TcdObject).ResetBounds;

  if Assigned(Scrollbox)
    then Scrollbox.Invalidate;
  end;

procedure TcdDiagram.RefreshFromData;
  begin
  end;

function TcdDiagram.GetMenuPopupPosition(Sender: TObject): TPoint;
  var
    lMenu: TMenu;
  begin
  result := Types.Point(0, 0);
  if Sender is TMenuItem
     then begin
          lMenu := TMenuItem(Sender).GetParentMenu;
          if lMenu is TPopupMenu
             then result := ScrollBox.ScreenToClient(TPopupMenu(lMenu).PopupPoint);
          end;
  end;

procedure TcdDiagram.Add(aObject: TcdObject; aX, aY: integer; aSelect: Boolean = True);
  var
    lScrollPos: TPoint;
  begin
  BeginChange;
  try
    lScrollPos := TPoint.Zero;

    if assigned(Scrollbox) 
      then begin
           Scrollbox.Canvas.MakeSureHandleIsAlllocated;
           lScrollPos.Offset(Scrollbox.GetHorizontalScrollPosition, Scrollbox.GetVerticalScrollPosition);
           end;

    // First set parent, next position. Setting the parent can change the size
    // on different dpi's
    aObject.Parent := ScrollBox;
    aObject.SetPosition(aX + lScrollPos.X, aY + lScrollPos.Y);
    aObject.InitChildren;

    // The new object is the only selected object
    if aSelect
       then begin
            Selection.Clear;
            Selection.Add(aObject);
            end;
  finally
    EndChange;
    end;
  end;

procedure TcdDiagram.BeginChange;
  begin
  Inc(fChangeCount);
  end;

procedure TcdDiagram.EndChange;
  begin
  Dec(fChangeCount);
  if fChangeCount <= 0
     then begin
          fChangeCount := 0;
          Changed;
          end;
  end;

procedure TcdDiagram.Add(aObject: TcdObject);
  begin
  Add(aObject, 0, 0, False);
  end;

procedure TcdDiagram.GetObjectsIntBounds(aPrinting: Boolean; out aRect: TRect);
  var
    T: integer;
    lObject: TcdObject;
    lRect: TRect;
    lAnyItemProcessed: Boolean;
  begin
  lAnyItemProcessed := False;
  aRect := Classes.Rect(MaxInt, MaxInt, 0, 0);
  for T := 0 to ComponentCount - 1 do
     begin
     lObject := TcdObject(Components[T]);
     if (not aPrinting OR lObject.Printable) and (lObject.Clickable) // Fuzy patch to exclude the PageSize
        then begin
             lRect := Classes.Rect(lObject.IntLeft, lObject.IntTop, lObject.IntLeft + lObject.IntWidth, lObject.IntTop + lObject.IntHeight);

             // Ignore empty items at 0.0
             if not lRect.IsEmpty
               then begin
                    aRect.Left := Min(lRect.Left, aRect.Left);
                    aRect.Top := Min(lRect.Top, aRect.Top);
                    aRect.Right := Max(lRect.Right, aRect.Right);
                    aRect.Bottom := Max(lRect.Bottom, aRect.Bottom);

                    lAnyItemProcessed := True;
                    end;
        end;
     end;
  if not lAnyItemProcessed
     then aRect := Classes.Rect(0, 0, 0, 0);
  end;

//function TcdDiagram.GetObjectsOfType<T>: TArray<T>;
function TcdDiagram.GetObjectsOfType(T: TClass): TArray<TcdObject>;
  var
    I: integer;
    lList: TList<TcdObject>;
  begin
  lList := TList<TcdObject>.Create;
    for I := 0 to ComponentCount - 1 do
      if Components[I] is T
         then lList.Add(Components[I] as TcdObject);
    result := lList.ToArray;
    lList.Free;
  end;

procedure TcdDiagram.GetObjectsExtBounds(out aRect: TRect);
  begin
  GetObjectsIntBounds(True, aRect);
  aRect := ieRect(aRect);
  end;

procedure TcdDiagram.ExportToPng(aFileName: string; aScalePercentage: integer);
  var
    T: integer;
    lObject: TcdObject;
    lBitmap: TBitmap;
    lPNG: TPortableNetworkGraphic;
    lOldScalePercentage: integer;
    lBounds: TRect;
    lWidth, lHeight: integer;
    lPreviousAllowPaperChange: Boolean;
  begin
  // Fetch current scale percentage
  lOldScalePercentage := ScalePercentage;

  // Convert screen resolution (default 96 dots/inch) to required resolution
  // ScalePercentage := Round(300 / Screen.PixelsPerInch)*100; -- Original
  ScalePercentage := aScalePercentage;
  lPreviousAllowPaperChange := AllowPaperChange;
  // Do not 'reinit'
  fAllowPaperChange := True;

  lBitmap := TBitmap.Create;
  lBitmap.PixelFormat := pf32bit;
  lPNG := TPortableNetworkGraphic.Create;
  lPNG.Transparent := False;

  try
    // Calculate and set canvas dimensions
    GetObjectsExtBounds(lBounds);

    // Center the images
    lBounds.Right := lBounds.Right + lBounds.Left;
    lBounds.Bottom := lBounds.Bottom + lBounds.Top;

    lWidth := lBounds.BottomRight.X+1;
    lHeight := lBounds.BottomRight.Y+1;
    lBitmap.SetSize(lWidth, lHeight);

    // White background
    lBitmap.Canvas.Brush.Color := clWhite;
    lBitmap.canvas.Rectangle(0, 0, lWidth, lHeight);

    // Diagram should be painted on the external canvas
    PaintOnExternal := True;

    // Deselect objects (selected objects might have a different color)
    Selection.Clear;

    // Set external canvas, make sure this is the last call before painting
    // or else the canvas may be changed.
    ScaleCanvas.Canvas := lBitmap.Canvas;
    // Rewraw every object
    for T := 0 to ComponentCount - 1 do
       begin
       lObject := TcdObject(Components[T]);
       if lObject.Printable
          then begin
               ActiveObject := lObject;
               lObject.PaintOnCanvas(ScaleCanvas.Canvas);
               end;
       end;

    // Assign doesn't do the thing in Lazarus, so draw the bitmap
    lPNG.SetSize(lWidth, lHeight);
    lPNG.Canvas.Draw(0, 0, lBitmap);
    lPNG.SaveToFile(aFilename);

  finally
    lBitmap.Free;
    lPNG.Free;

    fAllowPaperChange := lPreviousAllowPaperChange;

    // Stop printing on the external canvas
    PaintOnExternal := False;

    // Restore the scalepercentage
    ScalePercentage := lOldScalePercentage;
    end;
  end;

function TcdDiagram.PageFitsOnPrinter(aZoomPercentage: integer): Boolean;
  var
    lOldScalePercentage: integer;
    lBounds: TRect;
  begin
  // Fetch current scale percentage
  lOldScalePercentage := ScalePercentage;

  // Convert screen resolution (default 96 dots/inch) to printer resolution
  ScalePercentage := Trunc((aZoomPercentage/Screen.PixelsPerInch) * Printer.YDPI);

  try
    // Calculate and set canvas dimensions
    GetObjectsExtBounds(lBounds);

    // Bounds contain the margins, remove them
    OffsetRect(lBounds, -fPageSize.PrintMargin.X, -fPageSize.PrintMargin.Y);
    // Fits within printable page size?
    Result := (lBounds.Left >= 0) and (lBounds.Top >= 0) and (lBounds.Right <= Printer.PageWidth) and
      (lBounds.Bottom <= Printer.PageHeight)

  finally
    // Restore the scalepercentage
    ScalePercentage := lOldScalePercentage;
    end;
  end;

function TcdDiagram.ChangePrinterPaperToPageSize(aZoomPercentage: integer): Boolean;

  function GetPaperSize: Smallint;
    begin
    if fPageSize.PaperSize > 0 then
      Result := fPageSize.PaperSize
    else
    if (fPageSize.Asize <= 5) and (fPageSize.Asize >= 0) then
      Result := fPageSize.Asize
    else
      Result := -1;
    end;

  var
    lPaperSize: Smallint;
    lPaperName: String;
  begin
  result := False;

  lPaperSize := GetPaperSize;

  // Unsupported default paper
  if (lPaperSize = -1) then
    Exit;

  lPaperName := Format('A%d', [lPaperSize]);
  if Printer.PaperSize.SupportedPapers.IndexOf(lPaperName) >= 0
    then begin
         // Set printer papier/orientation
         Printer.PaperSize.PaperName := lPaperName;

         if fPageSize.Landscape
           then Printer.Orientation := poLandscape
           else Printer.Orientation := poPortrait;
         Printer.Canvas.Refresh;
         end;

  Result := True;
  end;

procedure TcdDiagram.Print(aJobTitle: string = '');
  var
    T: integer;
    lObject: TcdObject;
    lOldScalePercentage, lOldScalePercentagePrint: Integer;
    lWasPrinting: Boolean;
    lCount: integer;
  begin
  fAbortPrinting := False;

  if Assigned (fOnPrintProgress)
     then fOnPrintProgress (Self, ComponentCount, 0);

  with Printer do
    begin
    if Printer.XDPI <> Printer.YDPI
       then EUSR('Unsupported unequal X and Y DPI (%d - %d)', [Printer.XDPI, Printer.YDPI]);

    // Fetch current scale percentage
    lOldScalePercentage := ScalePercentage;
    lOldScalePercentagePrint := ScalePercentagePrint;

    try
      if aJobTitle = ''
         then Title := 'Untitled diagram'
         else Title := aJobTitle;

      // Deselect objects (selected objects might have a different color)
      Selection.Clear;

      lCount := 0;
      // Make sure the page fits and rescale
      while not PrintOnUnprintableMargin and not PageFitsOnPrinter(ScalePercentagePrint) do
        begin
        ScalePercentagePrint := fPageSize.RequiredScalePercentagePrint;
        if AllowPaperChange and not ChangePrinterPaperToPageSize(ScalePercentagePrint)
           then begin
                ScalePercentagePrint := fPageSize.RequiredScalePercentagePrint;
                end;
        Inc(lCount);

        if lCount > 5
           then EUsr('Unable to print the diagram on the selected paper. Please set the papersize of the printer manually.');
        end;

      // Convert screen resolution (default 96 dots/inch) to printer resolution
      ScalePercentage := Round((ScalePercentagePrint/Screen.PixelsPerInch) * Printer.YDPI);

      lWasPrinting := Printer.Printing;

      if not lWasPrinting
         then BeginDoc;

      // Diagram should be painted on the external canvas
      PaintOnExternal := True;

      // Set external canvas, make sure this is the last call before painting
      // or else the canvas may be changed.
      ScaleCanvas.Canvas := Printer.Canvas;

      // Reprint every object
      for T := 0 to ComponentCount - 1 do
        begin
        if fAbortPrinting
           then Break;

        lObject := TcdObject(Components[T]);
        if lObject.Printable
           then begin
                ActiveObject := lObject;
                lObject.PaintOnCanvas(ScaleCanvas.Canvas);

                if Assigned (fOnPrintProgress)
                   then fOnPrintProgress (Self, ComponentCount, T+1);
                end;
        end;

      if Assigned (fOnPrintProgress)
         then fOnPrintProgress (Self, ComponentCount, ComponentCount);

      if not lWasPrinting
         then EndDoc;

    finally
      // Stop printing on the external canvas
      PaintOnExternal := False;

      // Restore the scalepercentage
      ScalePercentage := lOldScalePercentage;

      ScalePercentagePrint := lOldScalePercentagePrint;
      end;
    end;
  end;

procedure TcdDiagram.PrinterChanged;
  begin
  if AllowPaperChange and not PageFitsOnPrinter(ScalePercentagePrint)
     then ChangePrinterPaperToPageSize(ScalePercentagePrint);
  fPageSize.Reinit(True);
  end;

procedure TcdDiagram.AbortPrinting;
  begin
  fAbortPrinting := True;
  end;

procedure TcdDiagram.Changed(aForce: Boolean = False);
  var
    lObject: TcdObject;
    I: Integer;

  begin
  if not aForce and IsChanging
     then Exit;

  //
  if not fPageSize.Reinit and aForce
     then ForceRedraw
     else for I := 0 to ComponentCount - 1 do
            begin
            lObject := Components[I] AS TcdObject;
            if lObject.NeedsRepaint
              then begin
                   lObject.Invalidate;
                   lObject.NeedsRepaint := False;
                   end;
            end;

  if fLoading
     then fLoading := False
     else begin
          fModified := True;
          end;
  end;

procedure TcdDiagram.Clear;
  var
    lObject: TcdObject;
  begin
  BeginChange;
  try
    Selection.Clear;

    try
      while ComponentCount>0 do
         begin
         lObject := TcdObject(Components[0]);
         //Debug('x', 'Destroy: ' + lObject.ClassName);
         if lObject.IsEmbeddedObject
            then RemoveComponent(lObject)
            else lObject.Free;
         end;
    except
      // Make sure everything is empty to allow another close attempt
      while ComponentCount>0 do
         RemoveComponent(Components[0]);
      raise;
      end;

    SetScalePercentage(100);

  finally
    EndChange;

    fModified := False;
    end;

  fPageSize.Reinit(True);
  end;

procedure TcdDiagram.CorrectZOrder;

  function CorrectZOrder_Comparer(constref aLeft, aRight: TcdObject): Integer;
    begin
    Result := aLeft.ZOrder - aRight.ZOrder;
    end;

  var
    lObject: TcdObject;
    I: integer;
    lObjects: TList<TcdObject>;
  begin
  lObjects := TList<TcdObject>.Create;

  for I := 0 to ComponentCount - 1 do
    begin
    lObject := TcdObject(Components[I]);
    if lObject.ZOrder > 0
       then lObjects.Add(lObject);
    end;

  lObjects.Sort(TComparer<TcdObject>.Construct(@CorrectZOrder_Comparer));

  for lObject in lObjects do
    begin
    lObject.SetZOrder(False);
    end;

  lObjects.Free;
  end;

function TcdDiagram.ieX(aX: integer): integer;
  begin
  if PaintOnExternal
     then begin
          if not Assigned(ActiveObject)
             then EInt('TcdDiagram.ieX', 'No ActiveObject assigned.');
          Inc(aX, ActiveObject.IntLeft);
          end;
  result := MulDiv(aX, fScaleMD.M, fScaleMD.D);

  // If we don't allow paperchange, the draw the unprintable margins at left and top.
  // But if we print, 0 is the first printable location. So we need to translate everything
  // to a new location. Unless the DoNotUseTopLeftUnprintableMargin is set
  if PaintOnExternal and not AllowPaperChange and not DoNotUseTopLeftUnprintableMargin
     then Dec(Result, fPageSize.PrintMargin.X);
  end;

function TcdDiagram.ieY(aY: integer): integer;
  begin
  if PaintOnExternal
      then begin
           if not Assigned(ActiveObject)
              then EInt('TcdDiagram.ieY', 'No ActiveObject assigned.');
           Inc(aY, ActiveObject.IntTop);
           end;
  result := MulDiv(aY, fScaleMD.M, fScaleMD.D);
  // See ieX
  if PaintOnExternal and not AllowPaperChange and not DoNotUseTopLeftUnprintableMargin
     then Dec(Result, fPageSize.PrintMargin.Y);
  end;

function TcdDiagram.ieRect(aRect: TRect): TRect;
  begin
  result := Classes.Rect(ieX(aRect.Left), ieY(aRect.Top), ieX(aRect.Right), ieY(aRect.Bottom));
  end;

procedure TcdDiagram.ieArrayOfPoints(var aPoints: array of TPoint);
  var
    T: integer;
  begin
  for T := 0 to High(aPoints) do
    begin
    aPoints[T].X := ieX(aPoints[T].X);
    aPoints[T].Y := ieY(aPoints[T].Y);
    end;
  end;

function TcdDiagram.ieFontHeight(aHeight: integer): integer;
  begin
  result := MulDiv(aHeight, fScaleMD.M, fScaleMD.D);
  end;

function TcdDiagram.eiFontHeight(aHeight: integer): integer;
  begin
  result := MulDiv(aHeight, fScaleMD.D, fScaleMD.D);
  end;

function TcdDiagram.ieXPos(aX: integer): integer;
  begin
  result := MulDiv(aX, fScaleMD.M, fScaleMD.D);
  if Assigned(Scrollbox)
     then Dec(result, Scrollbox.HorzScrollbarPosition);
  end;

function TcdDiagram.ieYPos(aY: integer): integer;
  begin
  result := MulDiv(aY, fScaleMD.M, fScaleMD.D);
  if Assigned(Scrollbox)
     then Dec(result, Scrollbox.VertScrollbarPosition);
  end;

function TcdDiagram.ieWidth(aWidth: integer): integer;
  begin
  result := MulDiv(aWidth, fScaleMD.M, fScaleMD.D);
  end;

function TcdDiagram.ieHeight(aHeight: integer): integer;
  begin
  result := MulDiv(aHeight, fScaleMD.M, fScaleMD.D);
  end;

function TcdDiagram.eiX(aX: integer): integer;
  begin
  result := MulDiv(aX, fScaleMD.D, fScaleMD.M);
  end;

function TcdDiagram.eiY(aY: integer): integer;
  begin
  result := MulDiv(aY, fScaleMD.D, fScaleMD.M);
  end;

function TcdDiagram.eiXPos(aX: integer): integer;
  begin
  aX := MulDiv(aX, fScaleMD.D, fScaleMD.M);
  if fSnapToGrid
     then result := SnapToGridX(aX)
     else result := aX;
  end;

function TcdDiagram.eiYPos(aY: integer): integer;
  begin
  aY := MulDiv(aY, fScaleMD.D, fScaleMD.M);
  if fSnapToGrid
     then result := SnapToGridY(aY)
     else result := aY;
  end;

function TcdDiagram.SnapToGridX(aX: integer): integer;
  begin
  result := (aX div Style.GridX) * Style.GridX;
  if (aX mod Style.GridX) > (Style.GridX div 2)
     then Inc(result, Style.GridX);
  end;

function TcdDiagram.SnapToGridY(aY: integer): integer;
  begin
  result := (aY div Style.GridY) * Style.GridY;
  if (aY mod Style.GridY) > (Style.GridY div 2)
     then Inc(result, Style.GridY);
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdStyle
//
constructor TcdStyle.Create (aName: string);
  begin
  inherited Create;

  fName := aName;

  fMultiSelectKey := [ssCtrl];
  fChildSelectKey := [ssShift];

  fTextFontName := 'ARIAL';
  feTextFontHeight := cFontSize8;
  fTextFontStyle := [];
  fTextFontColor := clBlack;

  fPenColor := clBlack;
  fPenColor_Selected := clBlack;
  fFillColor := clWhite;
  fFillColor_Selected := $F0F0F0;

  fLineType := ltStraightLine;
  fLineStyle := lsSolid;
  fLinePenColor := clBlack;
  feLineClickMargin := 10;
  feLineDrawMargin := 4;

  feFixedLinePartLength := 15;
  feConnectorWidth := 7;
  feConnectorHeight := 7;
  feConnectorClickArea := 2;

  fConnectorPenColor := fLinePenColor;
  fConnectorFillColor := fLinePenColor;
  fConnectorPenColor_Selected := fLinePenColor;
  fConnectorFillColor_Selected := clAqua;

  fObjectConPenColor := fLinePenColor;
  fObjectConFillColor := fLinePenColor;
  fFromObjectConType := octNone;
  fToObjectConType := octNone;
  fArrowAngleRad := DegToRad(25);
  feArrowSide := 10;
  feClawWidth := 8;

  fCardinalityManyChar := '*';
  fCardinalityDotSide := lsTo;
  feCardinalityDotSize := 8;
  fCardinalityWarningDotFillColor := clWhite;
  fCardinalityStrInternal := TStringlist.Create;
  fCardinalityStrExternal := TStringlist.Create;
  AddCardinalityStrReplacement('1..1','1');
  AddCardinalityStrReplacement('0..*','*');
  fDisplayCardinality := True;

  fLineTextFontName := 'ARIAL';
  feLineTextFontHeight := cFontSize8;
  fLineTextFontColor := clBlack;
  feLineTextMargin := 4;

  fSnapToGrid := True;
  fGridX := 10;
  fGridY := 10;

  // Internal coordinate system is five times larger then the external system
  fDefaultMultiplier := 1;
  fDefaultDivisor := 5;

  feScaleMargin := 2;

  fPluralnameOfObjects := 'Objects';
  end;

destructor TcdStyle.Destroy;
  begin
  FreeAndNil(fCardinalityStrInternal);
  FreeAndNil(fCardinalityStrExternal);
  inherited;
  end;

procedure TcdStyle.DeriveProperties(aDiagram: TcdDiagram);
  begin
  fiTextFontHeight := MulDiv(feTextFontHeight, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiLineClickMargin := MulDiv(feLineClickMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiLineDrawMargin := MulDiv(feLineDrawMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiFixedLinePartLength := MulDiv(feFixedLinePartLength, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiConnectorWidth := MulDiv(feConnectorWidth, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiConnectorHeight := MulDiv(feConnectorHeight, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiConnectorClickArea := MulDiv(feConnectorClickArea, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiArrowSide := MulDiv(feArrowSide, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiClawWidth := MulDiv(feClawWidth, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiCardinalityDotSize := MulDiv(feCardinalityDotSize, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiScaleMargin := MulDiv(feScaleMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiLineTextFontHeight := MulDiv(feLineTextFontHeight, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  fiLineTextMargin := MulDiv(feLineTextMargin, aDiagram.ScaleMD100.D, aDiagram.ScaleMD100.M);
  end;

procedure TcdStyle.DeleteCardinalityStrReplacements;
  begin
  fCardinalityStrInternal.Clear;
  fCardinalityStrExternal.Clear;
  end;

procedure TcdStyle.AddCardinalityStrReplacement(aInternalStr, aExternalStr: string);
  begin
  fCardinalityStrInternal.Add(aInternalStr);
  fCardinalityStrExternal.Add(aExternalStr);
  end;

function TcdStyle.ReplaceCardinalityStr(aInternalStr: string): string;
  var
    lPos: integer;
  begin
  result := aInternalStr;
  lPos := fCardinalityStrInternal.IndexOf(aInternalStr);
  if lPos>=0
     then result := fCardinalityStrExternal[lPos];
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdStyles
//
constructor TcdStyles.Create;
  begin
  inherited Create;
  //Sorted := True;
  end;

destructor TcdStyles.Destroy;
  var
    T: integer;
  begin
  try
    for T := 0 to Count-1 do
      Objects[T].Free;
  finally
    inherited Destroy;
    end;
  end;

procedure TcdStyles.Register_(aStyle: TcdStyle);
  begin
  if Assigned(GetStyle(aStyle.Name))
     then EInt('TcdStyles.Register_', 'Style ''%s'' is already registered', [aStyle.Name]);
  AddObject(Uppercase(aStyle.Name), aStyle);
  end;

function TcdStyles.GetStyle(aName: string): TcdStyle;
  var
    lPos: integer;
  begin
  lPos := IndexOf(Uppercase(aName));
  if lPos<0
     then result := nil
     else result := TcdStyle(Objects[lPos]);
  end;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdScrollBox
//

procedure TcdScrollBox.WMHScroll(var Message: TWMHScroll);
  begin
  Canvas.MakeSureHandleIsAlllocated;

  inherited;
  if Assigned(fOnHorizontalScroll) then fOnHorizontalScroll(Self);
  end;

procedure TcdScrollBox.WMVScroll(var Message: TWMVScroll);
  begin
  Canvas.MakeSureHandleIsAlllocated;

  inherited;
  if Assigned(fOnVerticalScroll) then fOnVerticalScroll(Self);
  end;

procedure TcdScrollBox.SetLastScreenCursor(const Value: TCursor);
  begin
  fLastScreenCursor := Value;
  Screen.Cursor := LastScreenCursor;
  end;

procedure TcdScrollBox.SetMouseDownPosition(aPoint: TPoint);
  begin
  Canvas.MakeSureHandleIsAlllocated;

  fMouseDownPosition := aPoint;
  end;

procedure TcdScrollBox.MouseDown (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer);
  begin
  if Diagram.ReadOnly then Exit;

  if aButton = mbLeft
     then begin
          if not (Diagram.Style.MultiSelectKey <= aShiftState)
             then Diagram.Selection.Clear;

          fDragMode := dmSelecting;
          fDragRectStart := Types.Point(aX, aY);
          end
  else if aButton = mbRight
     then begin
          Diagram.PopulateScrollBoxPopupMenu;
          Diagram.DoPopupMenu(Diagram.ObjectPopupMenu, aX, aY, Self);
          end
  else inherited MouseDown (aButton, aShiftState, aX, aY);
  end;

procedure TcdScrollBox.MouseMove (aShiftState: TShiftState; aX, aY: integer);
  var
    lPoint: TPoint;
  begin
  if Diagram.ReadOnly then Exit;

  case DragMode of
    dmSelecting:
       begin
       if not FocusRect.Activated
          then FocusRect.ActivateSelecting(fDragRectStart);

       if not ptInRect(ClientRect, Types.Point(aX, aY))
          then begin
               FocusRect.Visible := False;
               Scroll(aX, aY);
               Exit;
               end;

       fScrollTimer.Stop;
       FocusRect.UpdateSelecting(Types.Point(aX, aY));
       end;

    dmDragging:
       begin
       if not FocusRect.Activated
          then begin
               // Start calculation from the mouse down position
               FocusRect.ActivateDragging(Diagram.Selection.Rect, fMouseDownPosition);
               MouseCapture := True;
               end;

       // if Position is out of ClientRect then auto-scroll
       if not ptInRect (ClientRect, Types.Point(aX, aY))
          then begin
               FocusRect.Visible := False;
               Scroll(aX, aY);
               Exit;
               end;

       fScrollTimer.Stop;

       FocusRect.UpdateDragging(Types.Point(aX, aY));
       end;

    dmResizing:
       begin
       if not FocusRect.Activated
          then fFocusRect.ActivateSelecting(fDragRectStart);

       lPoint := Types.Point(aX - GetHorizontalScrollPosition, aY - GetVerticalScrollPosition);

       // if Position is out of ClientRect then auto-scroll
       if not ptInRect(ClientRect, lPoint)
          then begin
               FocusRect.Visible := False;
               Scroll(lPoint.X, lPoint.Y);
               Exit;
               end;

       fScrollTimer.Stop;
       FocusRect.UpdateResizing(Types.Point(aX, aY));
       end;
    end;

  inherited MouseMove(aShiftState, aX, aY);
  end;

procedure TcdScrollBox.MouseUp (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer);
  begin
  if Diagram.ReadOnly then Exit;

  MouseCapture := False;
  fScrollTimer.Stop;

  case DragMode of
    dmSelecting:
       begin
       if FocusRect.Activated and FocusRect.Visible
          then Diagram.Selection.SelectObjectsInRect(FocusRect.Rect);
       end;

    dmDragging:
       begin
       if FocusRect.Activated and FocusRect.Visible
          then begin
               if Diagram.Selection.AcceptsTranslate(FocusRect.Delta)
                  then Diagram.Selection.Translate(FocusRect.Delta);
               end;
       end;

    dmResizing:
       begin
       if FocusRect.Activated and FocusRect.Visible
          then begin
               if Diagram.Selection.AcceptsResize(FocusRect.Rect)
                  then Diagram.Selection[0].Resize(
                          PointsToPositiveRect(FocusRect.Rect.TopLeft, FocusRect.Rect.BottomRight));
               end;
       end;
    end;

  fDragMode := dmNone;
  FocusRect.Deactivate;

  inherited MouseUp(aButton, aShiftState, aX, aY);
  end;

procedure TcdScrollBox.StartResizing(aStartPoint: TPoint; aObject: TcdResizeableObject; aSizeOption: TfrSizeOption);
  begin
  DragMode := dmResizing;
  DragRectStart := aStartPoint;

  FocusRect.ActivateResizing(aStartPoint, aObject, aSizeOption);
  end;
   
procedure TcdScrollBox.CancelDragMode;
  begin
  // Cancel dragging on Escape press
  if DragMode <> dmNone
     then begin
          MouseCapture := False;
          fScrollTimer.Stop;

          fDragMode := dmNone;
          FocusRect.Deactivate;
          end;
  end;

constructor TcdScrollBox.Create(aOwner: TComponent);
  begin
  inherited Create(AOwner);

  fDragMode := dmNone;

  // for some weird reason the clWindow will not be set if the color is not
  // first set to clWhite ? (Beats me!)
  Color := clWhite;
  Color := clWindow;

  fFocusRect := TcdFocusRect.Create(Self);
  fScrollTimer := TcdScrollTimer.Create(Self);

  HorzScrollBar.Range := cScrollBarRange;
  VertScrollBar.Range := cScrollBarRange;
  end;

destructor TcdScrollBox.Destroy;
  begin
  try
    FreeAndNil(fScrollTimer);
    FreeAndNil(fFocusRect);
  finally
    inherited Destroy;
    end;
  end;

function TcdScrollBox.GetVerticalScrollPosition: Integer;
  begin
  Result := VertScrollbar.Position;
  end;

procedure TcdScrollBox.SetVerticalScrollPosition(aPosition: Integer);
  begin
  Canvas.MakeSureHandleIsAlllocated;

  VertScrollbar.Position := aPosition;
  end;

function TcdScrollBox.GetHorizontalScrollPosition: Integer;
  begin
  Result := HorzScrollbar.Position;
  end;

procedure TcdScrollBox.SetHorizontalScrollPosition(aPosition: Integer);
begin
  Canvas.MakeSureHandleIsAlllocated;

  HorzScrollbar.Position := aPosition;
end;

function TcdScrollBox.DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean;
  begin
  result := inherited DoMouseWheelDown(Shift, Mousepos);
  end;

function TcdScrollBox.DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean;
  begin
  result := inherited DoMouseWheelDown(Shift, Mousepos);
  end;

procedure TcdScrollBox.KeyUp(var Key: Word; Shift: TShiftState);
  begin
  inherited;
  end;

procedure TcdScrollBox.Scroll(aX, aY: integer);
  begin
  fScrollTimer.Start;

  if (aX<0)
     then fScrollTimer.fHInc := -cScrollTimerIncrement
  else if (aX>ClientWidth-1)
     then fScrollTimer.fHInc := cScrollTimerIncrement
  else fScrollTimer.fHInc := 0;

  if (aY<0)
     then fScrollTimer.VInc := -cScrollTimerIncrement
  else if (aY>ClientHeight-1)
     then fScrollTimer.VInc := cScrollTimerIncrement
  else fScrollTimer.VInc := 0;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdSelection
//
function TcdSelection.GetRect: TRect;
  var
    T: integer;
  begin
  if Count > 0
     then result := Items[0].BoundsRect
     else result := TRect.Empty;

  for T := 1 to Count-1 do
    UnionRect(result, result, Items[T].BoundsRect);
  end;

constructor TcdSelection.Create(aDiagram: TcdDiagram);
  begin
  inherited Create;
  fDiagram := aDiagram;
  fSelectionGroup := 0;
  end;

destructor TcdSelection.Destroy;
  begin
  try
    Clear;
  finally
    inherited Destroy;
    end;
  end;

procedure TcdSelection.Add(aObject: TcdObject);
  begin
  if aObject.SelectionGroup<>fSelectionGroup
     then begin
          Clear;
          fSelectionGroup := aObject.SelectionGroup;
          end;

  if IndexOf(aObject) = -1
     then inherited Add(aObject);
  aObject.SetSelected(True);
  end;

procedure TcdSelection.AlignLeft;
  var
    lLeft: integer;
    lObject: TcdObject;
    lObjects: TArray<TcdObject>;
    lDelta: TPoint;
  begin
  if Count > 1
     then begin
          lLeft := Items[0].Left;
          lObjects := GetObjectsOfType(TcdDragObject);

          for lObject in lObjects do
            if (lObject <> Items[0])
               then begin
                    lDelta := Types.Point(lLeft - lObject.Left, 0);
                    if lObject.AcceptsTranslate(lDelta)
                       then lObject.Translate(lDelta);
                    end;
          end;
  end;

procedure TcdSelection.AlignRight;
  var
    lRight: integer;
    lObject: TcdObject;
    lObjects: TArray<TcdObject>;
    lDelta: TPoint;
  begin
  if Count > 1
     then begin
          lRight := Items[0].Left + Items[0].Width;
          lObjects := GetObjectsOfType(TcdDragObject);

          for lObject in lObjects do
            if (lObject <> Items[0])
               then begin
                    lDelta := Types.Point(lRight - (lObject.Left + lObject.Width), 0);
                    if lObject.AcceptsTranslate(lDelta)
                       then lObject.Translate(lDelta);
                    end;
          end;
  end;

procedure TcdSelection.AlignTop;
  var
    lTop: integer;
    lObject: TcdObject;
    lObjects: TArray<TcdObject>;
    lDelta: TPoint;
  begin
  if Count > 1
     then begin
          lTop := Items[0].Top;
          lObjects := GetObjectsOfType(TcdDragObject);

          for lObject in lObjects do
            if (lObject <> Items[0])
               then begin
                    lDelta := Types.Point(0, lTop - (lObject.Top));
                    if lObject.AcceptsTranslate(lDelta)
                       then lObject.Translate(lDelta);
                    end;
          end;
  end;

procedure TcdSelection.Remove(aObject: TcdObject);
  begin
  if IndexOf(aObject)>=0
     then begin
          inherited Remove(aObject);
          aObject.SetSelected(False);

          if Count=0
             then fSelectionGroup := 0;
          end;
  end;

procedure TcdSelection.Toggle(aObject: TcdObject);
  begin
  if aObject.Selected
     then Remove(aObject)
     else Add(aObject);
  end;

procedure TcdSelection.SelectObjectsInRect(aRect: TRect);
  var
     T: integer;
     lObject: TcdObject;
  begin
  for T := 0 to fDiagram.ComponentCount-1 do
    begin
    lObject := TcdObject(fDiagram.Components[T]);

    if not lObject.Visible OR // Ignore invisible objects
       not (lObject is TcdDragObject) // Ignore non draggable objects
       then Continue;

    if InterSectRect(lObject.BoundsRect, aRect)
       then Add(lObject)
       else Remove(lObject);
    end;
  end;

function TcdSelection.GetObjectsOfType(T: TClass): TArray<TcdObject>; 
  begin
  result := GetObjectsOfType(T, nil);
  end;

function TcdSelection.GetObjectsOfType(T: TClass; aAddObject: TAddObjectFunc): TArray<TcdObject>;

  var
    lObject: TcdObject;
    lList: TList<TcdObject>;
  begin
  lList := TList<TcdObject>.Create;
  for lObject in Self do
    if (lObject is T) and (not assigned(aAddObject) or aAddObject(lObject))
       then lList.Add(lObject as TcdObject);
  result := lList.ToArray;
  lList.Free;
  end;

function SpaceEqualVert_GetObjects(constref aObject: TcdObject): Boolean;
  begin
  result := not (aObject is TcdLineCon) OR not TcdLineCon(aObject).AutoPosition;
  end;

procedure TcdSelection.SpaceEqualVert;

  function SpaceEqualVert_Comparer(constref Left, Right: TcdObject): Integer;
    begin
    Result := Left.Top - Right.Top;
    end;

  var
  lMinTop, lMaxBottom, lTotalHeight, lSpace: Integer;
    lObject: TcdObject;
    lObjects: TArray<TcdObject>;
  begin
  lObjects := GetObjectsOfType(TcdDragObject, SpaceEqualVert_GetObjects);

  TArrayHelper<TcdObject>.Sort(lObjects, TComparer<TcdObject>.Construct(@SpaceEqualVert_Comparer));

  lMinTop := MAXINT;
  lMaxBottom := 0;
  lTotalHeight := 0;
  for lObject in lObjects do
    begin
    lMinTop := Min(lMinTop, lObject.Top);
    lMaxBottom := Max(lMaxBottom, lObject.Top + lObject.Height);
    lTotalHeight := lTotalHeight + lObject.Height;
    end;

  lSpace := (lMaxBottom - lMinTop - lTotalHeight) div (Length(lObjects) - 1);
  for lObject in lObjects do
    begin
    if lObject <> lObjects[0]
       then lObject.Translate(Types.Point(0, lMinTop - lObject.Top));

    lMinTop := lObject.Top + lObject.Height + lSpace;
    end;
  end;

procedure TcdSelection.Clear;
  var
    T: integer;
  begin
  for T := 0 to Count-1 do
    Items[T].SetSelected(False);

  inherited Clear;

  fSelectionGroup := 0;
  end;

procedure TcdSelection.Translate(aDelta: TPoint);
  var
    lList: TList<TcdObject>;
    lObject: TcdObject;
  begin
  fDiagram.BeginChange;
  // During the translate, items move and (e.g.) connectionpoints are destroyed.
  // So the selection list dynamically changes. to make sure we move everything
  // once, create a copy of the list and compare if the item is still valid.
  lList := TList<TcdObject>.Create(Self);
  try
    for lObject in lList do
      if IndexOf(lObject) > -1
         then lObject.Translate(aDelta);
  finally
    lList.Free;
    fDiagram.EndChange;
    end;
  end;

function TcdSelection.AcceptsTranslate(aDelta: TPoint): Boolean;
  var
    lObject: TcdObject;
  begin
  result := True;
  for lObject in Self do
    result := result and lObject.AcceptsTranslate(aDelta);
  end;

function TcdSelection.AcceptsResize(aNewRect: TRect): Boolean;
  var
    lObject: TcdObject;
  begin
  result := True;
  for lObject in Self do
    result := result and lObject.AcceptsResize(aNewRect);
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdFocusRect
//
procedure TcdFocusRect.SetVisible(aValue: Boolean);
  begin
  if fVisible <> aValue
     then begin
          fVisible := aValue;
          Paint;
          end;
  end;

procedure TcdFocusRect.Paint;
  var
    lExternalRect: TRect;
  begin
  if fPainted
     then begin
    	  fScrollBox.Canvas.DrawFocusRect(fPreviousRect);
          end;

  if fVisible
     then begin
          lExternalRect := fCurrentRect;
          OffsetRect(lExternalRect, -fScrollBox.HorzScrollBarPosition, -fScrollBox.VertScrollBarPosition);
          fPreviousRect := lExternalRect;

          fScrollBox.Canvas.DrawFocusRect(lExternalRect);

          fPainted := True;
          end
     else fPainted := False;
  end;

procedure TcdFocusRect.DeriveRect;
  begin
  fCurrentRect := PointsToPositiveRect(fStartPoint, fEndPoint);

  // Keep ratio?
  if (fSizeOption = frsoBothKeepRatio) and Assigned(fResizeObject) and (fResizeObject.AspectRatio > 0)
     then fCurrentRect.Right := fCurrentRect.Left + Round((fCurrentRect.Bottom - fCurrentRect.Top) * fResizeObject.AspectRatio);
  end;

constructor TcdFocusRect.Create(aScrollBox: TcdScrollbox);
  begin
  inherited Create;
  fActivated := False;
  fScrollBox := aScrollBox;
  fVisible := False;
  fPainted := False;

  fCurrentRect := TRect.Empty;

  fStartPoint := Types.Point(0,0);
  fEndPoint := Types.Point(0,0);
  end;

destructor TcdFocusRect.Destroy;
  begin
  try
    SetVisible(False);
  finally
    inherited Destroy;
    end;
  end;

procedure TcdFocusRect.ActivateSelecting(aStartPoint: TPoint);
  begin
  fStartPoint := Types.Point(aStartPoint.X + fScrollBox.HorzScrollBarPosition, aStartPoint.Y + fScrollBox.VertScrollBarPosition);
  fEndPoint := fStartPoint;

  DeriveRect;

  Visible := True;
  Activated := True;
  end;

procedure TcdFocusRect.UpdateSelecting(aEndPoint: TPoint);
  begin
  Visible := False;
  fEndPoint := Types.Point(aEndPoint.X + fScrollBox.HorzScrollBarPosition, aEndPoint.Y + fScrollBox.VertScrollBarPosition);
  DeriveRect;
  // Update bug:
  // 1. (un)Selecting objects will cause the 'changed' objects to receive a 'invalidate' message.
  // So they will redraw after this method.
  // 2. The 'visible := True' below will draw the FocusRect. The redraw of the selected object will
  // overwrite this FocusRect so the next 'DrawFocusRect' which should delete the rect, will draw the rect.
  // Only way to fix this is to make the focusrect a Control itself so it can receive messages and draw the FocusRect last
  fScrollBox.Diagram.Selection.SelectObjectsInRect(fCurrentRect);

  Visible := TRUE;
  end;

procedure TcdFocusRect.ActivateDragging(aDragRect: TRect; aDragPoint: TPoint);
  begin
  fStartPoint := aDragRect.TopLeft;
  fEndPoint   := aDragRect.BottomRight;

  fStartScrollBar.X := fScrollBox.HorzScrollBarPosition;
  fStartScrollBar.Y := fScrollBox.VertScrollBarPosition;

  fDragPoint := Types.Point(aDragPoint.X-aDragRect.Left, aDragPoint.Y-aDragRect.Top);

  fWidth  := aDragRect.Right - aDragRect.Left;
  fHeight := aDragRect.Bottom - aDragRect.Top;

  fCurrentRect := aDragRect;
  fDelta := Types.Point(0, 0);

  Activated := True;
  end;

procedure TcdFocusRect.UpdateDragging(aDragPoint: TPoint);
  var
    lRect: TRect;
    lDelta: TPoint;
  begin
  Visible := False;

  aDragPoint := Types.Point(aDragPoint.X - fDragPoint.X, aDragPoint.Y - fDragPoint.Y);

  lRect := Classes.Rect(aDragPoint.X, aDragPoint.Y, aDragPoint.X+fWidth, aDragPoint.Y+fHeight);
  lDelta.X := lRect.Left-fStartPoint.X+fScrollBox.HorzScrollBarPosition-fStartScrollBar.X;
  lDelta.Y := lRect.Top-fStartPoint.Y+fScrollBox.VertScrollBarPosition-fStartScrollBar.Y;

  if fScrollBox.Diagram.Selection.AcceptsTranslate(lDelta)
     then Screen.Cursor := crDrag
     else Screen.Cursor := crNo;

  fCurrentRect := lRect;
  fDelta := lDelta;

  Visible := True;
  end;

procedure TcdFocusRect.UpdateResizing(aEndPoint: TPoint);
  begin
  Visible := False;
  if SizeOption in [frsoHorizontally, frsoBoth, frsoBothKeepRatio]
     then fEndPoint.X := aEndPoint.X;
  if SizeOption in [frsoVertically, frsoBoth, frsoBothKeepRatio]
     then fEndPoint.Y := aEndPoint.Y;
  DeriveRect;

  if fScrollBox.Diagram.Selection.AcceptsResize(fCurrentRect)
     then Screen.Cursor := fScrollBox.LastScreenCursor
     else Screen.Cursor := crNo;

  Visible := True;
  end;

procedure TcdFocusRect.ActivateResizing(aStartPoint: TPoint; aObject: TcdResizeableObject; aSizeOption: TfrSizeOption);
  begin
  SizeOption := aSizeOption;
  fResizeObject := aObject;
  if SizeOption in [frsoHorizontally,frsoBoth, frsoBothKeepRatio] then
  begin
    if aStartPoint.X < aObject.Width div 2 then
    begin
      fStartPoint.X := aObject.BoundsRect.Right;
      fEndPoint.X := aObject.BoundsRect.Left;
    end
    else
    begin
      fStartPoint.X := aObject.BoundsRect.Left;
      fEndPoint.X := aObject.BoundsRect.Right;
    end;
    if SizeOption = frsoHorizontally then
    begin
      fStartPoint.Y := aObject.BoundsRect.Top;
      fEndPoint.Y := aObject.BoundsRect.Bottom;
    end;
  end;
  if SizeOption in [frsoVertically, frsoBoth, frsoBothKeepRatio] then
  begin
    if aStartPoint.Y < aObject.Height div 2 then
    begin
      fStartPoint.Y := aObject.BoundsRect.Bottom;
      fEndPoint.Y := aObject.BoundsRect.Top;
    end
    else
    begin
      fStartPoint.Y := aObject.BoundsRect.Top;
      fEndPoint.Y := aObject.BoundsRect.Bottom;
    end;
    if SizeOption = frsoVertically then
    begin
      fStartPoint.X := aObject.BoundsRect.Left;
      fEndPoint.X := aObject.BoundsRect.Right;
    end;
  end;

  DeriveRect;
  Visible := True;
  Activated := True;
  end;

procedure TcdFocusRect.Deactivate;
  begin
  Visible := False;
  Activated := False;
  fResizeObject := nil;

  Screen.Cursor := crDefault;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdScrollTimer
//
procedure TcdScrollTimer.Scroll(Sender: TObject);
  begin
  if fHInc<>0
     then with fScrollBox.HorzScrollBar do
             Position := Position + fHInc;
  if fVInc<>0
     then with fScrollBox.VertScrollBar do
             Position := Position + fVInc;
  end;

constructor TcdScrollTimer.Create (aScrollBox: TcdScrollbox);
  begin
  inherited Create;

  fScrollBox := aScrollBox;
  fHInc      := 0;
  fVInc      := 0;

  fTimer := TTimer.Create(fScrollBox);
  fTimer.Enabled := False;
  fTimer.Interval := cScrollTimerInterval;
  fTimer.OnTimer := Scroll;
  end;

procedure TcdScrollTimer.Start;
  begin
  fTimer.Enabled := True;
  end;

procedure TcdScrollTimer.Stop;
  begin
  fTimer.Enabled := False;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdScaleCanvas
//
procedure TcdScaleCanvas.SetCanvas(const Value: TCanvas);
  begin
  fCanvas := Value;

  // Reset values
  fFontHeight := cFontSize8;
  fPenPos := Types.Point(0,0);

  if Assigned(fCanvas)
     then begin
          // Set translated font height
          fCanvas.Font.Height := fDiagram.ieFontHeight(fFontHeight);
          // Assign the font to the FontCalcCanvas
          fFontCalcCanvas.Font.Assign(fCanvas.Font);
          end;
  end;

function TcdScaleCanvas.GetFontColor: Graphics.TColor;
  begin result := fCanvas.Font.Color; end;

function TcdScaleCanvas.GetFontName: TFontName;
  begin result := fCanvas.Font.Name; end;

function TcdScaleCanvas.GetFontHeight: integer;
  begin result := fFontHeight; end;

function TcdScaleCanvas.GetFontStyle: TFontStyles;
  begin result := fCanvas.Font.Style; end;

function TcdScaleCanvas.GetFontCharset: TFontCharset;
  begin result := fCanvas.Font.Charset; end;

function TcdScaleCanvas.GetFontPitch: TFontPitch;
  begin result := fCanvas.Font.Pitch; end;

function TcdScaleCanvas.GetFontOrientation: integer;
  begin result := fCanvas.Font.Orientation; end;

function TcdScaleCanvas.GetBrushColor: Graphics.TColor;
  begin result := fCanvas.Brush.Color; end;

function TcdScaleCanvas.GetBrushStyle: TBrushStyle;
  begin result := fCanvas.Brush.Style; end;

function TcdScaleCanvas.GetPenColor: Graphics.TColor;
  begin result := fCanvas.Pen.Color; end;

function TcdScaleCanvas.GetPenMode: TPenMode;
  begin result := fCanvas.Pen.Mode; end;

function TcdScaleCanvas.GetPenStyle: TPenStyle;
  begin result := fCanvas.Pen.Style; end;

function TcdScaleCanvas.GetPenWidth: integer;
  begin result := fCanvas.Pen.Width; end;

function TcdScaleCanvas.GetLockCount: integer;
  begin result := fCanvas.LockCount; end;

// Extra checks for assigned canvas is needed, because font properties can be set
// just for the FontCalcCanvas.
procedure TcdScaleCanvas.SetFontColor(const Value: Graphics.TColor);
  begin if Assigned(fCanvas) and Assigned(fCanvas.Font) then fCanvas.Font.Color := Value; end;

procedure TcdScaleCanvas.SetFontName(const Value: TFontName);
  begin
  if Assigned(fCanvas) and Assigned(fCanvas.Font) then fCanvas.Font.Name := Value;
  fFontCalcCanvas.Font.Name := Value;
  end;

procedure TcdScaleCanvas.SetFontHeight(const Value: integer);
  begin
  fFontHeight := Value;
  if Assigned(fCanvas) and Assigned(fCanvas.Font) then fCanvas.Font.Height := fDiagram.ieFontHeight(Value);
  fFontCalcCanvas.Font.Height := fDiagram.ieFontHeight(Value);
  end;

procedure TcdScaleCanvas.SetFontIntercharacterSpacing(const Value: Integer);
  begin
  if Assigned(fCanvas)
     then SetTextCharacterExtra(fCanvas.Handle, fDiagram.ieFontHeight(Value));
  SetTextCharacterExtra(fFontCalcCanvas.Handle, fDiagram.ieFontHeight(Value));
  end;

procedure TcdScaleCanvas.SetFontStyle(const Value: TFontStyles);
  begin
  if Assigned(fCanvas) and Assigned(fCanvas.Font) then fCanvas.Font.Style := Value;
  fFontCalcCanvas.Font.Style := Value;
  end;

procedure TcdScaleCanvas.SetFontCharset(const Value: TFontCharset);
  begin
  if Assigned(fCanvas) and Assigned(fCanvas.Font) then fCanvas.Font.CharSet := Value;
  fFontCalcCanvas.Font.CharSet := Value;
  end;

procedure TcdScaleCanvas.SetFontPitch(const Value: TFontPitch);
  begin
  if Assigned(fCanvas) and Assigned(fCanvas.Font) then fCanvas.Font.Pitch := Value;
  fFontCalcCanvas.Font.Pitch := Value;
  end;

procedure TcdScaleCanvas.SetFontOrientation(const Value: integer);
  begin
  if Assigned(fCanvas) and Assigned(fCanvas.Font) then fCanvas.Font.Orientation := Value;
  fFontCalcCanvas.Font.Orientation := Value;
  end;

procedure TcdScaleCanvas.SetBrushColor(const Value: Graphics.TColor);
  begin fCanvas.Brush.Color := Value; end;

procedure TcdScaleCanvas.SetBrushStyle(const Value: TBrushStyle);
  begin fCanvas.Brush.Style := Value; end;

procedure TcdScaleCanvas.SetPenColor(const Value: Graphics.TColor);
  begin fCanvas.Pen.Color := Value; end;

procedure TcdScaleCanvas.SetPenMode(const Value: TPenMode);
  begin fCanvas.Pen.Mode := Value; end;

procedure TcdScaleCanvas.SetPenStyle(const Value: TPenStyle);
  begin fCanvas.Pen.Style := Value; end;

procedure TcdScaleCanvas.SetPenWidth(const Value: integer);
  begin fCanvas.Pen.Width := Value; end;

procedure TcdScaleCanvas.SetPenPos(const Value: TPoint);
  begin MoveTo(Value.X, Value.Y); end;

constructor TcdScaleCanvas.Create(aDiagram: TcdDiagram);
  begin
  inherited Create;
  fDiagram := aDiagram;
  fCanvas := nil;
  fFontCalcCanvas := TCanvas.Create;
  // 20030605 use the devicecontext of the mainform, if the form on which the scrollbox
  // is placed is not yet initialized (=visible), the results of the Api calls might be
  // fuzzy, TextWidth returning 0.... This fixes this problem
  fFontCalcCanvas.Handle := CreateCompatibleDC(0);
//    fFontCalcCanvas.Handle := GetDC(Application.MainForm.Handle);
//  fFontCalcCanvas.Font.Name := 'ARIAL';
  end;

destructor TcdScaleCanvas.Destroy;
  begin
  DeleteDC(fFontCalcCanvas.Handle);
  FreeAndNil(fFontCalcCanvas);
  inherited Destroy;
  end;

procedure TcdScaleCanvas.Arc(X1, Y1, X2, Y2, X3, Y3, X4, Y4: integer);
  begin with fDiagram do fCanvas.Arc(ieX(X1), ieY(Y1), ieX(X2), ieY(Y2), ieX(X3), ieY(Y3), ieX(X4), ieY(Y4)); end;

procedure TcdScaleCanvas.Chord(X1, Y1, X2, Y2, X3, Y3, X4, Y4: integer);
  begin with fDiagram do fCanvas.Chord(ieX(X1), ieY(Y1), ieX(X2), ieY(Y2), ieX(X3), ieY(Y3), ieX(X4), ieY(Y4)); end;

procedure TcdScaleCanvas.CopyRect(const Dest: TRect; Canvas: TCanvas; const Source: TRect);
  begin with fDiagram do fCanvas.CopyRect(ieRect(Dest), Canvas, ieRect(Source)); end;

procedure TcdScaleCanvas.DrawFocusRect(const Rect: TRect);
  begin with fDiagram do fCanvas.DrawFocusRect(ieRect(Rect)); end;

procedure TcdScaleCanvas.Ellipse(const Rect: TRect);
  begin with fDiagram do fCanvas.Ellipse(ieRect(Rect)); end;

procedure TcdScaleCanvas.Ellipse(X1, Y1, X2, Y2: integer);
  begin with fDiagram do fCanvas.Ellipse(ieX(X1), ieY(Y1), ieX(X2), ieY(Y2)); end;

procedure TcdScaleCanvas.FillRect(const Rect: TRect);
  begin with fDiagram do fCanvas.FillRect(ieRect(Rect)); end;

procedure TcdScaleCanvas.FillRect_Gradient(const Rect: TRect; const FromColor, ToColor: Graphics.TColor; const GradientDirection: TcdGradientDirection);
  var
    X,Y: integer;
    dr,dg,db: Extended;
    C1,C2: TColor;
    R1,R2,G1,G2,B1,B2: Byte;
    R,G,B: Byte;
    T: integer;
    lRect: TRect;
  begin
  lRect := fDiagram.ieRect(Rect);
  if GradientDirection = gdHorizontal
     then begin
          C1 := FromColor;
          R1 := GetRValue(C1);
          G1 := GetGValue(C1);
          B1 := GetBValue(C1);

          C2 := ToColor;
          R2 := GetRValue(C2);
          G2 := GetGValue(C2);
          B2 := GetBValue(C2);

          dr := (R2-R1) / (lRect.Right-lRect.Left);
          dg := (G2-G1) / (lRect.Right-lRect.Left);
          db := (B2-B1) / (lRect.Right-lRect.Left);

          T := 0;
          for X := lRect.Left to lRect.Right-1 do
              begin
              R := R1+Ceil(dr*T);
              G := G1+Ceil(dg*T);
              B := B1+Ceil(db*T);

              with fDiagram do
                 begin
                 fCanvas.Pen.Color := RGB(R, G, B);
                 fCanvas.MoveTo(X, lRect.Top);
                 fCanvas.LineTo(X, lRect.Bottom);
                 end;

              Inc(T);
              end;
          end
  else if GradientDirection = gdVertical
     then begin
          C1 := FromColor;
          R1 := GetRValue(C1);
          G1 := GetGValue(C1);
          B1 := GetBValue(C1);

          C2 := ToColor;
          R2 := GetRValue(C2);
          G2 := GetGValue(C2);
          B2 := GetBValue(C2);

          dr := (R2-R1) / (lRect.Bottom-lRect.Top);
          dg := (G2-G1) / (lRect.Bottom-lRect.Top);
          db := (B2-B1) / (lRect.Bottom-lRect.Top);

          T := 0;
          for Y := lRect.Top to lRect.Bottom-1 do
             begin
             R := R1+Ceil(dr*T);
             G := G1+Ceil(dg*T);
             B := B1+Ceil(db*T);

             with fDiagram do
                begin
                fCanvas.Pen.Color := RGB(R, G, B);
                fCanvas.MoveTo(lRect.Left, Y);
                fCanvas.LineTo(lRect.Right, Y);
                end;

             Inc(T);
             end;
          end;
  end;

procedure TcdScaleCanvas.FloodFill(X, Y: integer; Color: Graphics.TColor; FillStyle: TFillStyle);
  begin with fDiagram do fCanvas.FloodFill(ieX(X), ieY(Y), Color, FillStyle); end;

procedure TcdScaleCanvas.FrameRect(const Rect: TRect);
  begin with fDiagram do fCanvas.FrameRect(ieRect(Rect)); end;

procedure TcdScaleCanvas.LineTo(X, Y: integer);
  begin
  fPenPos := Types.Point(X, Y);
  with fDiagram do fCanvas.LineTo(ieX(X), ieY(Y));
  end;

procedure TcdScaleCanvas.Lock;
  begin fCanvas.Lock; end;

procedure TcdScaleCanvas.MoveTo(X, Y: integer);
  begin
  fPenPos := Types.Point(X, Y);
  with fDiagram do fCanvas.MoveTo(ieX(X), ieY(Y));
  end;

procedure TcdScaleCanvas.Pie(X1, Y1, X2, Y2, X3, Y3, X4, Y4: integer);
  begin with fDiagram do fCanvas.Pie(ieX(X1), ieY(Y1), ieX(X2), ieY(Y2), ieX(X3), ieY(Y3), ieX(X4), ieY(Y4)); end;

procedure TcdScaleCanvas.PolyBezier(Points: array of TPoint);
  begin
  fDiagram.ieArrayOfPoints(Points);
  fCanvas.PolyBezier(Points);
end;

procedure TcdScaleCanvas.PolyBezierTo(Points: array of TPoint);
  begin
  fDiagram.ieArrayOfPoints(Points);
  fCanvas.PolyBezier(Points);
  end;

procedure TcdScaleCanvas.Polygon(Points: array of TPoint);
  begin
  fDiagram.ieArrayOfPoints(Points);
  fCanvas.Polygon(Points);
  end;

procedure TcdScaleCanvas.Polyline(Points: array of TPoint);
  begin
  fDiagram.ieArrayOfPoints(Points);
  fCanvas.Polyline(Points);
  end;

procedure TcdScaleCanvas.Rectangle(X1, Y1, X2, Y2: integer);
  begin with fDiagram do fCanvas.Rectangle(ieX(X1), ieY(Y1), ieX(X2), ieY(Y2)); end;

procedure TcdScaleCanvas.Rectangle(const Rect: TRect);
  begin with fDiagram do fCanvas.Rectangle(ieRect(Rect)); end;

procedure TcdScaleCanvas.Refresh;
  begin fCanvas.Refresh; end;

procedure TcdScaleCanvas.RoundRect(X1, Y1, X2, Y2, X3, Y3: integer);
  begin with fDiagram do fCanvas.RoundRect(ieX(X1), ieY(Y1), ieX(X2), ieY(Y2), ieX(X3), ieY(Y3)); end;

procedure TcdScaleCanvas.StretchDraw(const Rect: TRect; Graphic: TGraphic);
  begin with fDiagram do fCanvas.StretchDraw(ieRect(Rect), Graphic); end;

function TcdScaleCanvas.TextExtent(const Text: string): TSize;
  begin
  result := fFontCalcCanvas.TextExtent(Text);
  result.cx := fDiagram.eiX(result.cx);
  result.cy := fDiagram.eiY(result.cy);
  end;

function TcdScaleCanvas.TextHeight(const Text: string): integer;
  begin result := fDiagram.eiY(fFontcalcCanvas.TextHeight(Text)); end;

procedure TcdScaleCanvas.TextOut(X, Y: integer; const Text: string);
  begin with fDiagram do fCanvas.TextOut(ieX(X), ieY(Y), Text); end;

procedure TcdScaleCanvas.TextRect(var aRect: TRect; const Text: string; aTextStyle: TTextStyle);

  function eiRect(aRect: TRect): TRect;
    begin
    result := Classes.Rect(fDiagram.eiX(aRect.Left), fDiagram.eiY(aRect.Top), fDiagram.eiX(aRect.Right), fDiagram.eiY(aRect.Bottom));
    end;

  var
    lText: string;
    lRect: TRect;
  begin
  lText := Text;
  lRect := fDiagram.ieRect(aRect);
  fCanvas.TextRect(lRect, lRect.Left, lRect.Top, lText, aTextStyle);
  aRect := eiRect(lRect);
  end;

function TcdScaleCanvas.TextWidth(const Text: string): integer;
  begin result := fDiagram.eiX(fFontCalcCanvas.TextWidth(Text)); end;

function TcdScaleCanvas.TryLock: Boolean;
  begin result := fCanvas.TryLock; end;

procedure TcdScaleCanvas.Unlock;
  begin fCanvas.Unlock; end;

procedure TcdScaleCanvas.DeriveRoundRectPoints(var aPoints: array of TPoint; aLeft, aTop, aRight, aBottom, A, B, C, D: integer);
  begin
  // Left upper corner
  aPoints[0] := Types.Point(aLeft, aTop+A); aPoints[1] := Types.Point(aLeft+C, aTop+B); aPoints[2] := Types.Point(aLeft+B, aTop+C); aPoints[3] := Types.Point(aLeft+A, aTop);

  // Two points on top line
  aPoints[4] := Types.Point(aLeft+D, aTop); aPoints[5] := Types.Point(aRight-D, aTop);

  // Right upper corner
  aPoints[6] := Types.Point(aRight-A, aTop); aPoints[7] := Types.Point(aRight-B, aTop+C); aPoints[8] := Types.Point(aRight-C, aTop+B); aPoints[9] := Types.Point(aRight, aTop+A);

  // Two points on the right side
  aPoints[10] := Classes.Point(aRight, aTop+D); aPoints[11] := Types.Point(aRight, aBottom-D);

  // Right lower corner
  aPoints[12] := Classes.Point(aRight, aBottom-A); aPoints[13] := Types.Point(aRight-C, aBottom-B); aPoints[14] := Types.Point(aRight-B, aBottom-C); aPoints[15] := Types.Point(aRight-A, aBottom);

  // Two points on the bottom line
  aPoints[16] := Types.Point(aRight-D, aBottom); aPoints[17] := Types.Point(aLeft+D, aBottom);

  // Left lower corner
  aPoints[18] := Types.Point(aLeft+A, aBottom); aPoints[19] := Types.Point(aLeft+B, aBottom-C); aPoints[20] := Types.Point(aLeft+C, aBottom-B); aPoints[21] := Types.Point(aLeft, aBottom-A);

  // Two points on the left line
  aPoints[22] := Types.Point(aLeft, aBottom-D); aPoints[23] := Types.Point(aLeft, aTop+D);

  // Final point (equal to start point)
  aPoints[24] := Types.Point(aLeft, aTop+A);
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdPoint
//
constructor TcdPoint.Create (aX, aY: integer);
  begin
  inherited Create;
  fX := aX;
  fY := aY;
  end;

function TcdPoint.AsPoint : TPoint;
  begin
  result := Types.Point(fX, fY);
  end;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdObject
//
procedure TcdObject.CMHitTest(var Msg: TCMHITTEST);
  begin
  if not fDiagram.ReadOnly and Clickable and InActiveArea (Diagram.eiX(Msg.XPos), Diagram.eiY(Msg.YPos))
     then Msg.result := 1
     else Msg.result := 0;
  end;

procedure TcdObject.SetSelected(aValue: Boolean);
  begin
  if fSelected <> aValue
     then begin
          fSelected := aValue;
          // Only correct order if only 1 object is selected and the object gets selected
          if (Diagram.Selection.Count=1) and aValue and BringToFrontOnSelect
             then begin
                  CorrectZOrder;
                  end;
          Invalidate;
          RePaint;
          end;
  end;

function TcdObject.InActiveArea (aX, aY: integer): Boolean;
  begin
  result := ptInRect(IntClientRect, Types.Point(aX, aY));
  end;

procedure TcdObject.ResetBounds_External;
  begin
  ResetBounds;
  end;

procedure TcdObject.ResetBounds_Internal;
  begin
  end;

procedure TcdObject.ResetBounds_Children;
  begin
  end;

procedure TcdObject.ResetBounds;
  begin
  ResetBounds_Internal;
  ResetBounds_Children;
  end;

procedure TcdObject.Changed;
  begin
  NeedsRepaint := True;
  end;

function TcdObject.GetCenterPosition: TPoint;
  begin
  result.X := IntLeft + IntWidth div 2;
  result.Y := IntTop + IntHeight div 2;
  end;

procedure TcdObject.CorrectZOrder;
  begin
  BringToFront;
  end;

constructor TcdObject.Create(aDiagram: TcdDiagram; aOrigin: TcdObjectOrigin);
  begin
  inherited Create(aDiagram);
  ControlStyle := ControlStyle + [csClickEvents, csDoubleClicks, csOpaque];

  fDiagram := aDiagram;
  //Font.PixelsPerInch := Windows.USER_DEFAULT_SCREEN_DPI;

  fScrollBox := aDiagram.ScrollBox; // Could be nil
  fClickable := True;
  fPrintable := True;
  fOrigin := aOrigin;
  fSelectionGroup := 0;
  end;

destructor TcdObject.Destroy;
  begin
  try
    Diagram.Selection.Remove(self);
    SetSelected(False);

  finally
    inherited Destroy;
  end;
end;

procedure TcdObject.InitChildren;
  begin
  end;

procedure TcdObject.LoadFromData;
  begin
  end;

procedure TcdObject.LoadFromJson(aJsonObject: TJsonObject);
  begin
  end;


procedure TcdObject.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer);
  begin
  inherited;
  if (aButton = mbRight) and (Diagram.Selection.Count <= 1)
     then Diagram.DoObjectPopup(Self, aX, aY);
  end;

procedure TcdObject.Moved;
  begin
  Diagram.Changed;
  end;

procedure TcdObject.SaveToJson(aJsonObject: TJsonObject);
  begin
  end;

procedure TcdObject.RefreshFromData;
  begin
  end;

procedure TcdObject.Paint;
  begin
  // Precaution, prevents redrawing while the object is destroyed
  if csDestroying in ComponentState
     then Exit;

  fDiagram.ActiveObject := Self;
  PaintOnCanvas(Canvas);
  end;

procedure TcdObject.PopulatePopupMenu(aMenu: TPopupMenu);
  begin
  end;

procedure TcdObject.SetPosition(aX, aY: integer);
  begin
  with Diagram do
    begin
    case Origin of
      ooAuto: ; // Ignore, ResetBounds knows or calculates the position

      ooUpperLeft:
         Self.SetIntBounds(eiXPos(aX), eiYPos(aY), IntWidth, IntHeight);

      ooCentered:
         Self.SetIntBounds(
            eiXPos(aX) - (IntWidth div 2),
            eiYPos(aY) - (IntHeight div 2),
            IntWidth, IntHeight
         );
      end;
    end;

  ResetBounds_External;
  end;

function TcdObject.AcceptsTranslate(aDelta: TPoint): Boolean;
  begin
  result := True;
  end;

function TcdObject.BringToFrontOnSelect: Boolean;
  begin
  result := True;
  end;

procedure TcdObject.SetZOrder(TopMost: Boolean);
  begin
  inherited SetZOrder(TopMost);
  if TopMost
     then ComponentIndex := MAXINT
     else ComponentIndex := 0;
  end;

function TcdObject.AcceptsResize(aNewRect: TRect): Boolean;
  begin
  result := True;
  end;

procedure TcdObject.Translate(aDelta: TPoint);
  begin
  with Diagram do
    begin
    case Origin of
      ooAuto: ; // Ignore, ResetBounds knows or calculates the position

      ooUpperLeft:
         Self.SetPosition(Self.Left+aDelta.X, Self.Top+aDelta.Y);

      ooCentered:
         Self.SetPosition(Self.Left+aDelta.X+(Width div 2), Self.Top+aDelta.Y+(Height div 2));
      end;
    end;

  Moved;
  end;

procedure TcdObject.Resize(aRect: TRect);
  begin
  with Diagram do
    begin
    case Origin of
      ooAuto: ; // Ignore, ResetBounds knows or calculates the position

      ooUpperLeft,
      ooCentered:
         Self.SetIntBounds(eiXPos(aRect.Left), eiYPos(aRect.Top), eiX(aRect.Width), eiY(aRect.Height));
      end;
    end;

  Moved;
  end;

procedure TcdObject.SetIntBounds(aLeft, aTop, aWidth, aHeight: integer);
  begin
  fIntLeft := aLeft;
  fIntTop := aTop;
  fIntWidth := aWidth;
  fIntHeight := aHeight;
  fIntClientRect := Bounds(0, 0, fIntWidth, fIntHeight);
  with Diagram do
    // Ignore the scrollbar position, this is already done in the LCL. So abuse the
    // ieWidth/ieHeight for X/Y
    Self.SetBounds(ieWidth(aLeft), ieHeight(aTop), ieWidth(aWidth), ieHeight(aHeight));
  end;

procedure TcdObject.Select;
  begin
  Diagram.Selection.Clear;
  Diagram.Selection.Add(Self);
  end;

function TcdObject.GetObjectSide(aLine: TcdLine; aPoint: TPoint): TcdObjectSide;
  begin
  if Abs(aPoint.X-IntLeft)<=Diagram.Style.ScaleMargin
     then result := osLeft
  else if Abs(aPoint.X-(IntLeft+IntWidth))<=Diagram.Style.ScaleMargin
     then result := osRight
  else if Abs(aPoint.Y-IntTop)<=Diagram.Style.ScaleMargin
     then result := osTop
  else if Abs(aPoint.Y-(IntTop+IntHeight))<=Diagram.Style.ScaleMargin
     then result := osBottom
     else result := osNone;
  end;

function TcdObject.GetPlane: TcdPlane;
  var
    lPlanes: TArray<TcdObject>;
    lPlane: TcdObject;
  begin
  lPlanes := Diagram.GetObjectsOfType(TcdPlane);
  for lPlane in lPlanes do
    if IntersectRect(lPlane.BoundsRect, BoundsRect) 
       then begin
            Result := lPlane as TcdPlane;
            Exit;
            end;
  result := nil;
  end;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdDragObject
//
procedure TcdDragObject.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer);
  begin
  // if (aButton <> mbLeft) OR (Diagram.Style.ChildSelectKey <= aShiftState) OR (ssAlt in aShiftState)
  //    then Exit;

  if (aButton = mbLeft)
     then begin
          if Diagram.Style.MultiSelectKey <= aShiftState
             then begin
                  Diagram.Selection.Toggle(Self);
                  if not Selected
                     then ScrollBox.DragMode := dmNone;
                  end
             else begin
                  if Selected
                     then begin
                          (*
                          if Scrollbox.DragMode = dmNone
                             then Diagram.Selection.Clear;
                          *)
                          end
                     else begin
                          Diagram.Selection.Clear;
                          Diagram.Selection.Add(Self);
                          end;
                  end;

          if not (ssDouble in aShiftState) and (ScrollBox.DragMode = dmNone)
             then ScrollBox.DragMode := dmSelecting;
          end;

  // Store the position
  Scrollbox.SetMouseDownPosition(Scrollbox.ScreenToClient(ClientToScreen(Types.Point(aX, aY))));

  // do not call the ScrollBox.MouseDown because this will deselect the object
  inherited MouseDown(aButton, aShiftState, aX, aY);
  end;

procedure TcdDragObject.MouseUp(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer);
  begin
  ScrollBox.MouseUp(aButton, aShiftState, aX+Left, aY+Top);
  inherited MouseUp(aButton, aShiftState, aX, aY);
  end;

procedure TcdDragObject.MouseMove(aShiftState: TShiftState; aX, aY: integer);
  begin
  if ScrollBox.DragMode = dmSelecting
     then ScrollBox.DragMode := dmDragging;
  ScrollBox.MouseMove(aShiftState, aX+Left, aY+Top);
  inherited MouseMove(aShiftState, aX, aY);
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdConObject
//
procedure TcdConObject.ResetBounds;
  var
    T: integer;
  begin
  inherited;

  // Reset the lines after the resetbounds_internal and the resetbounds_children
  for T := 0 to Lines.Count-1 do
    TcdLine(Lines[T]).ResetBounds;
  end;

function TcdConObject.IsReferenced: Boolean;
  var
    T: integer;
  begin
  result := True;
  for T := 0 to Lines.Count-1 do
    if TcdLine(Lines[T]).ToObject = Self
       then Exit;
  result := False;
  end;

function TcdConObject.GetConnectionPointFromList(aListContainer: TcdObject; aList: TcList; aDestinationPoint: TPoint): TPoint;
  var
    lClosestPoint,
    lTempPoint: TPoint;
    lClosestPointDistance: Real;
    T: integer;
  begin
  lClosestPointDistance := 0; // Prevents warning
  lClosestPoint := Types.Point(0,0); // Just in case no point was found
  for T := 0 to aList.Count-1 do
    begin
    lTempPoint := TcdPoint(aList[T]).AsPoint;

    Inc(lTempPoint.X, aListContainer.IntLeft);
    Inc(lTempPoint.Y, aListContainer.IntTop);

    if (T = 0) OR (Distance(lTempPoint, aDestinationPoint) < lClosestPointDistance)
       then begin
            lClosestPoint := lTempPoint;
            lClosestPointDistance := Abs(Distance(lClosestPoint, aDestinationPoint));
            end;
    end;

  result := lClosestPoint;
  end;

function TcdConObject.GetConnectionPoint(aLine: TcdLine; aPoint: TPoint): TPoint;
  begin
  result := GetConnectionPointFromList(Self, ConnectionPoints, aPoint);
  end;

constructor TcdConObject.Create(aDiagram: TcdDiagram; aOrigin: TcdObjectOrigin);
  begin
  inherited Create (aDiagram, aOrigin);
  fLines := TcList.Create;
  fConnectionPoints := TcList.Create;
  end;

destructor TcdConObject.Destroy;
  var
    lLines: TcList;
  begin
  try
    // Create a local copy of the Lines. The destruction of a line will update
    // the fLines of the From and to objects of the line. Kill the local lLines.
    // After the KillAll, the fLines has a count of 0.
    lLines := TcList.Create;
    lLines.Assign(fLines);
    lLines.Free;
    fLines.Free;
    FreeAndNil(fConnectionPoints);
 finally
    inherited Destroy;
    end;
 end;

procedure TcdConObject.DeleteConnectionPoints;
  begin
  ConnectionPoints.KillAll;
  end;

procedure TcdConObject.AddConnectionPoint(aList: TcList; aX, aY: integer);
  begin
  aList.Add(TcdPoint.Create(aX, aY));
  end;

procedure TcdConObject.AddConnectionPoint(aX, aY: integer);
  begin
  AddConnectionPoint(ConnectionPoints, aX, aY);
  end;

function TcdConObject.ConnectTo(aConObject: TcdConObject; aConData: TcdLineConData = nil): TcdLine;
  begin
  result := ConnectTo(aConObject, '', aConData);
  end;

function TcdConObject.ConnectTo(aConObject: TcdConObject; aComment: string; aConData: TcdLineConData = nil): TcdLine;
  var
    lNewLine: TcdLine;
  begin
  lNewLine := TcdLine.Create(Diagram, Self, aConObject, aConData);
  lNewLine.LoadFromData;
  AddLine(lNewLine);
  aConObject.AddLine(lNewLine);
  lNewLine.CommentStr := aComment;
  lNewLine.CorrectZOrder;

  result := lNewLine;
  end;

function TcdConObject.ConnectedTo(aConObject: TcdConObject): TcdLIne;
  var
    T: integer;
    lLine: TcdLine;
  begin
  result := nil;
  for T := 0 to Lines.Count-1 do
    begin
    lLine := TcdLine(Lines[T]);
    if (lLine.ToObject=aConObject)
       then begin
            result := lLine;
            Exit;
            end;
    end;
  end;

function TcdConObject.ConnectedTo(aConObject: TcdConObject; aCommentStr: string): TcdLIne;
  var
    T: integer;
    lLine: TcdLine;
  begin
  result := nil;
  for T := 0 to Lines.Count-1 do
    begin
    lLine := TcdLine(Lines[T]);
    if (lLine.ToObject=aConObject) and (lLine.CommentStr=aCommentStr)
       then begin
            result := lLine;
            Exit;
            end;
    end;
  end;

procedure TcdConObject.DisconnectFrom(aConObject: TcdConObject);
  var
    T: integer;
    lLine: TcdLine;
  begin
  T := 0;
  while T < Lines.Count do
    begin
    lLine := TcdLine(Lines[T]);
    if (lLine.FromObject = aConObject) OR (lLine.ToObject = aConObject)
       then lLine.Free
       else Inc (T);
    end;
  end;

procedure TcdConObject.DisconnectFromAll;
  var
    lLine: TcdLine;
  begin
  while Lines.Count>0 do
    begin
    lLine := TcdLine(Lines[0]);
    lLine.Free;
    end;
  end;

procedure TcdConObject.AddLine(aLine: TcdLine);
  begin
  if Lines.IndexOf(aLine) >= 0
     then Exit;
  Lines.Add(aLine);
  Diagram.Add(aLine);
  end;

procedure TcdConObject.RemoveLine(aLine: TcdLine);
  begin
  if Lines.IndexOf (aLine) < 0
     then Exit;
  Lines.Remove(aLine);
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdLineConData
//
constructor TcdLineConData.Create;
  begin
  inherited Create;
  end;

   
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdLine
//
procedure TcdLine.ResetBounds_Internal;
  var
    lDistance, lMinDistance: Double;
    lContinue: Boolean;
    lFromPoint, lToPoint: TPoint;
  begin
  inherited;

  if ((LineType=lt3PLine) and (Parts.Count<=3)) OR // while creating a line, a 3P line might have less than 3 parts
     ((LineType<>lt3PLine) and (Parts.Count=1))
     then begin
          fFromPoint := FromObject.GetConnectionPoint(Self, ToObject.CenterPosition);
          fToPoint := ToObject.GetConnectionPoint(Self, FromPoint);

          lMinDistance := Distance(FromPoint, ToPoint);
          lContinue := True;
          while lContinue do
            begin
            lContinue := False;

            lFromPoint := FromObject.GetConnectionPoint(Self, ToPoint);
            lDistance := Distance(lFromPoint, ToPoint);
            if lDistance<lMinDistance
               then begin
                    fFromPoint := lFromPoint;
                    lMinDistance := lDistance;
                    lContinue := True;
                    end;

            lToPoint := ToObject.GetConnectionPoint(Self, FromPoint);
            lDistance := Distance(FromPoint, lToPoint);
            if lDistance<lMinDistance
               then begin
                    fToPoint := lToPoint;
                    lMinDistance := lDistance;
                    lContinue := True;
                    end;
            end;
          end
     else begin
          // Lines with parts
          if LineType=lt3PLine
             then begin
                  fFromPoint := FromObject.GetConnectionPoint(Self, TcdLineCon(Connectors[1]).CenterPosition);
                  fToPoint := ToObject.GetConnectionPoint(Self, TcdLineCon(Connectors[Connectors.Count-2]).CenterPosition);
                  end
             else begin
                  fFromPoint := FromObject.GetConnectionPoint(Self, FromLinePart.ToConnector.CenterPosition);
                  fToPoint := ToObject.GetConnectionPoint(Self, ToLinePart.FromConnector.CenterPosition);
                  end;
          end;
  end;

procedure TcdLine.ResetBounds_Children;
  var
     T: integer;
  begin
  inherited;

  if LineType=lt3PLine
     then begin
          TcdLineCon(Connectors[0]).ResetBounds;
          TcdLineCon(Connectors[Connectors.Count-1]).ResetBounds;
          end;

  for T := 0 to Parts.Count-1 do
    TcdLinePart(Parts[T]).ResetBounds;

  if Assigned(Comment) then Comment.ResetBounds;
  if Assigned(MasterCardText) then MasterCardText.ResetBounds;
  if Assigned(DetailCardText) then DetailCardText.ResetBounds;
  if Assigned(FromComment) then FromComment.ResetBounds;
  if Assigned(ToComment) then ToComment.ResetBounds;

  if Assigned(FromObjectCon) then FromObjectCon.ResetBounds;
  if Assigned(ToObjectCon) then ToObjectCon.ResetBounds;
  end;

function TcdLine.GetTextStr(aLineText: TcdLineText): string;
  begin
  result := '';
  if Assigned(aLineText)
     then result := aLineText.Text;
  end;

function TcdLine.GetCommentStr: string;
  begin result := GetTextStr(Comment); end;
function TcdLine.GetMasterCardTextStr: string;
  begin result := GetTextStr(MasterCardText); end;
function TcdLine.GetDetailCardTextStr: string;
  begin result := GetTextStr(DetailCardText); end;
function TcdLine.GetFromCommentStr: string;
  begin result := GetTextStr(FromComment); end;
function TcdLine.GetToCommentStr: string;
  begin result := GetTextStr(ToComment); end;

procedure TcdLine.SetTextStr(var aLineText: TcdLineText; aText: string; aLineTextPosition: TcdLineTextPosition);
  begin
  if aText=''
     then begin
          if Assigned(aLineText)
             then FreeAndNil(aLineText);
          end
     else begin
          if not Assigned(aLineText)
             then begin
                  aLineText := TcdLineText.Create(Diagram, Self, aLineTextPosition);
                  aLineText.Text := aText;
                  Diagram.Add(aLineText);
                  CorrectZOrder;
                  end
             else aLineText.Text := aText;
          end;
  end;

procedure TcdLine.SetCommentStr(aText: string);
  begin SetTextStr(fComment, aText, ltpMiddle); end;
procedure TcdLine.SetMasterCardTextStr(aText: string);
  begin SetTextStr(fMasterCardText, aText, ltpToSide_LeftOrTop); end;
procedure TcdLine.SetDetailCardTextStr(aText: string);
  begin SetTextStr(fDetailCardText, aText, ltpFromSide_LeftOrTop); end;
procedure TcdLine.SetFromCommentStr(aText: string);
  begin SetTextStr(fFromComment, aText, ltpFromSide_RightOrBottom); end;
procedure TcdLine.SetToCommentStr(aText: string);
  begin SetTextStr(fToComment, aText, ltpToSide_RightOrBottom); end;

function TcdLine.GetLineType: TcdLineType;
  begin
  if fLineType=ltInherit
     then result := Diagram.Style.LineType
     else result := fLineType;
  end;

function TcdLine.GetLineStyle: TcdLineStyle;
  begin
  if fLineStyle=lsInherit
     then result := Diagram.Style.LineStyle
     else result := fLineStyle;
  end;

function TcdLine.GetFromObjectConType: TcdObjectConType;
  begin
  if fFromObjectConType=octInherit
     then result := Diagram.Style.FromObjectConType
     else result := fFromObjectConType;
  end;

function TcdLine.GetToObjectConType: TcdObjectConType;
  begin
  if fToObjectConType=octInherit
     then result := Diagram.Style.ToObjectConType
     else result := fToObjectConType;
  end;

constructor TcdLine.Create (aDiagram: TcdDiagram; aLineType: TcdLineType; aLineStyle: TcdLineStyle; aFromObjectConType, aToObjectConType: TcdObjectConType;
                               aFromObject, aToObject: TcdConObject; aConData: TcdLineConData);
  begin
  Create(aDiagram, aFromObject, aToObject, aConData);
  fLineType := aLineType;
  fLineStyle := aLineStyle;
  fFromObjectConType := aFromObjectConType;
  fToObjectConType := aToObjectConType;
  end;

constructor TcdLine.Create (aDiagram: TcdDiagram; aFromObject, aToObject: TcdConObject; aConData: TcdLineConData);
  begin
  inherited Create(aDiagram, ooAuto);
  IsEmbeddedObject := True;

  fLineType := ltInherit;
  fLineStyle := lsInherit;
  fFromObjectConType := octInherit;
  fToObjectConType := octInherit;

  fParts := TcList.Create;
  fConnectors := TcList.Create;

  fConData := aConData;

  fFromObject := aFromObject;
  fToObject := aToObject;

  fComment := nil;
  fMasterCardText := nil;
  fDetailCardText := nil;
  fFromComment := nil;
  fToComment := nil;

  fFromObjectCon := TcdObjectCon.Create(Diagram, Self);
  fToObjectCon := TcdObjectCon.Create(Diagram, Self);
  fConData := aConData;

  fMinMasters := cCardNone;
  fMaxMasters := cCardNone;
  fMinDetails := cCardNone;
  fMaxDetails := cCardNone;

  fFromLinePart := nil;
  fToLinePart := nil;
  end;

destructor TcdLine.Destroy;
  begin
  FromObject.RemoveLine(Self);
  ToObject.RemoveLine(Self);

  FreeAndNil(fComment);
  FreeAndNil(fMasterCardText);
  FreeAndNil(fDetailCardText);
  FreeAndNil(fFromComment);
  FreeAndNil(fToComment);

  FreeAndNil(fFromObjectCon);
  FreeAndNil(fToObjectCon);

  FreeAndNil(fConData);

  FreeAndNil(fParts);
  FreeAndNil(fConnectors);

  inherited Destroy;
  end;

procedure TcdLine.InitChildren;
  begin
  Diagram.Add(fFromObjectCon);
  Diagram.Add(fToObjectCon);
  end;

procedure TcdLine.LoadFromData;
  var
    lFromConnector, lToConnector: TcdLineCon;
    lFromLinePart, lMainLinePart, lToLinePart: TcdLinePart;
  begin
  if LineType=lt3PLine
     then begin
          lFromConnector := TcdLineCon.Create(Diagram, Self, True {AutoPosition} );
          lToConnector := TcdLineCon.Create(Diagram, Self, True {AutoPosition} );
          lFromLinePart := TcdLinePart.Create(Diagram, Self);
          lMainLinePart := TcdLinePart.Create(Diagram, Self);
          lToLinePart := TcdLinePart.Create(Diagram, Self);

          lFromConnector.FromLinePart := lFromLinePart;
          lFromConnector.ToLinePart := lMainLinePart;
          lToConnector.FromLinePart := lMainLinePart;
          lToConnector.ToLinePart := lToLinePart;
          lFromLinePart.FromConnector := nil;
          lFromLinePart.ToConnector := lFromConnector;
          lMainLinePart.FromConnector := lFromConnector;
          lMainLinePart.ToConnector := lToConnector;
          lToLinePart.FromConnector := lToConnector;
          lToLinePart.ToConnector := nil;

          fFromLinePart := lFromLinePart;
          fToLinePart := lToLinePart;

          // Connector positions will be calculated during the resetbounds_internal of the connector
          AddLineCon(lFromConnector,0,0);
          AddLineCon(lToConnector,0,0);
          AddLinePart(lFromLinePart);
          AddLinePart(lMainLinePart);
          AddLinePart(lToLinePart);
          end
     else begin
          fFromLinePart := TcdLinePart.Create(Diagram, Self);
          fToLinePart := FromLinePart;
          AddLinePart(FromLinePart);
          end;
  CorrectZOrder;
  end;

procedure TcdLine.LoadFromJson(aJsonObject: TJsonObject);
  var
    i, lX, lY: integer;
    lLinePart, lTempLinePart: TcdLinePart;
    lConnector: TcdLineCon;
    lParts, lConnectors: TcList;
    lAutoPosition: Boolean;
    lJsonConnectors: TJsonArray;
    lJsonConnector: TJsonObject;
    lJsonValue: TJsonValue;
    lStr: string;
  begin
  lParts := TcList.Create;
  lConnectors := TcList.Create;

  // Type
  if not aJsonObject.TryGetValue('type', lStr) then InvalidJson_xExpected('type');
  fLineType := StrToLineType(lStr);

  // Style
  if not aJsonObject.TryGetValue('style', lStr) then InvalidJson_xExpected('style');
  fLineStyle := StrToLineStyle(lStr);

  // Create the first linepart
  fFromLinePart := TcdLinePart.Create(Diagram, Self);
  lLinePart := fFromLinePart;
  lParts.Add(lLinePart);

  // Create the line connectors and line parts
  if not aJsonObject.TryGetValue('connectors', lJsonConnectors) then InvalidJson_xExpected('connectors');
  for i := 0 to lJsonConnectors.Count-1 do
    begin
    lTempLinePart := lLinePart;
    lLinePart := TcdLinePart.Create(Diagram, Self);
    lParts.Add(lLinePart);

    lConnector := TcdLineCon.Create(Diagram, Self);
    lConnector.FromLinePart := lTempLinePart;
    lConnector.ToLinePart := lLinePart;
    lConnectors.Add(lConnector);

    lLinePart.FromConnector := lConnector;
    lTempLinePart.ToConnector := lConnector;
    end;

  // Connect end linepart
  fToLinePart := lLinePart;

  // Add connectors
  for i := 0 to lJsonConnectors.Count-1 do
    begin
    lJsonValue := lJsonConnectors[i];
    if not (lJsonValue is TJsonObject) then InvalidJson_xExpected('connector');
    lJsonConnector := lJsonValue as TJsonObject;

    if not lJsonConnector.TryGetValue('x', lX) then InvalidJson_xExpected('x');
    lX := Diagram.ieXPos(lX);
    if not lJsonConnector.TryGetValue('y', lY) then InvalidJson_xExpected('y');
    lY := Diagram.ieYPos(lY);

    lConnector := TcdLineCon(lConnectors[i]);
    if not lJsonConnector.TryGetValue('autoPosition', lAutoPosition) then InvalidJson_xExpected('autoPosition');
    lConnector.AutoPosition := lAutoPosition;

    AddLineCon(lConnector, lX, lY);
    end;

  // Add parts
  for i := 0 to lParts.Count-1 do
    begin
    lLinePart := TcdLinePart(lParts[i]);
    AddLinePart(lLinePart);
    end;

  // read and create comment
  if not aJsonObject.TryGetValue('comment', lStr) then InvalidJson_xExpected('comment');
  CommentStr := lStr;
  if not aJsonObject.TryGetValue('masterCardText', lStr) then InvalidJson_xExpected('masterCardText');
  MasterCardTextStr := lStr;
  if not aJsonObject.TryGetValue('detailCardText', lStr) then InvalidJson_xExpected('detailCardText');
  DetailCardTextStr := lStr;
  if not aJsonObject.TryGetValue('fromComment', lStr) then InvalidJson_xExpected('fromComment');
  FromCommentStr := lStr;
  if not aJsonObject.TryGetValue('toComment', lStr) then InvalidJson_xExpected('toComment');
  ToCommentStr := lStr;

  // Cardinalities
  if not aJsonObject.TryGetValue('minMasters', fMinMasters) then InvalidJson_xExpected('minMasters');
  if not aJsonObject.TryGetValue('maxMasters', fMaxMasters) then InvalidJson_xExpected('maxMasters');
  if not aJsonObject.TryGetValue('minDetails', fMinDetails) then InvalidJson_xExpected('minDetails');
  if not aJsonObject.TryGetValue('maxDetails', fMaxDetails) then InvalidJson_xExpected('maxDetails');
  SetCardinality(FromObject {Detail}, fMinDetails, fMaxDetails);
  SetCardinality(ToObject {Master}, fMinMasters, fMaxMasters);

  // Destroy temp lists
  lParts.RemoveAll;
  lConnectors.RemoveAll;
  FreeAndNil(lParts);
  FreeAndNil(lConnectors);

  CorrectZOrder;
  end;

procedure TcdLine.SaveToJson(aJsonObject: TJsonObject);
  var
    lX, lY: integer;
    lPart: TcdLinePart;
    lConnector: TcdLineCon;
    lJsonConnectors: TJsonArray;
    lJsonConnector: TJsonObject;
    lStr: string;
  begin
  // Type
  lStr := LineTypeToStr(fLineType);
  aJsonObject.AddPair('type', lStr);

  // Style
  lStr := LineStyleToStr(fLineStyle);
  aJsonObject.AddPair('style', lStr);

  // Write array for connectors
  lJsonConnectors := TJsonArray.Create;
  aJsonObject.AddPair('connectors', lJsonConnectors);

  // Walk through points (instead of saving the list sequential)
  lPart := FromLinePart;
  while Assigned(lPart) do
    begin
    lConnector := lPart.ToConnector;
    if not Assigned(lConnector)
       then Break;

    lJsonConnector := TJsonObject.Create;

    lX := lConnector.IntLeft + (lConnector.IntWidth div 2);
    lJsonConnector.AddPair('x', TJsonIntegerNumber.Create(lX));

    lY := lConnector.IntTop + (lConnector.IntHeight div 2);
    lJsonConnector.AddPair('y', TJsonIntegerNumber.Create(lY));

    lJsonConnector.AddPair('autoPosition', TJsonBoolean.Create(lConnector.AutoPosition));

    lJsonConnectors.Add(lJsonConnector);

    // Next line part
    lPart := lConnector.ToLinePart;
    end;

  // Write comment
  lStr := CommentStr;
  aJsonObject.AddPair('comment', lStr);
  lStr := MasterCardTextStr;
  aJsonObject.AddPair('masterCardText', lStr);
  lStr := DetailCardTextStr;
  aJsonObject.AddPair('detailCardText', lStr);
  lStr := FromCommentStr;
  aJsonObject.AddPair('fromComment', lStr);
  lStr := ToCommentStr;
  aJsonObject.AddPair('toComment', lStr);

  // Cardinalities
  aJsonObject.AddPair('minMasters', TJsonIntegerNumber.Create(fMinMasters));
  aJsonObject.AddPair('maxMasters', TJsonIntegerNumber.Create(fMaxMasters));
  aJsonObject.AddPair('minDetails', TJsonIntegerNumber.Create(fMinDetails));
  aJsonObject.AddPair('maxDetails', TJsonIntegerNumber.Create(fMaxDetails));
  end;


procedure TcdLine.PaintOnCanvas(aCanvas: TCanvas);
  begin
  // The line is a container object for line parts and part-connectors,
  // therefore this routine is empty.
  end;

procedure TcdLine.CorrectZOrder;
  var
    T: integer;
  begin
  inherited CorrectZOrder;

  for T := 0 to Parts.Count-1 do
    TcdLinePart(Parts[T]).BringToFront;

  if Assigned(Comment) then Comment.BringToFront;
  if Assigned(MasterCardText) then MasterCardText.BringToFront;
  if Assigned(DetailCardText) then DetailCardText.BringToFront;
  if Assigned(FromComment) then FromComment.BringToFront;
  if Assigned(ToComment) then ToComment.BringToFront;

  FromObjectCon.BringToFront;
  ToObjectCon.BringToFront;

  for T := 0 to Connectors.Count-1 do
    TcdLineCon(Connectors[T]).BringToFront;
  end;

procedure TcdLine.RemoveConnector (aLinePartConnector: TcdLineCon);
  var
    lFromLinePart,
    lToLinePart: TcdLinePart;
  begin
  lFromLinePart := aLinePartConnector.FromLinePart;
  lToLinePart := aLinePartConnector.ToLinePart;

  lFromLinePart.ToConnector := lToLinePart.ToConnector;
  if not Assigned(lFromLinePart.ToConnector)
     then fToLinePart := lFromLinePart
     else lFromLinePart.ToConnector.FromLinePart := lFromLinePart;

  RemoveLineCon(aLinePartConnector);
  aLinePartConnector.Free;

  RemoveLinePart(lToLinePart);
  lToLinePart.Free;

  ResetBounds;
  end;

procedure TcdLine.RemoveConnectors;
  var
    T: integer;
    lConnector: TcdLineCon;
  begin
  (*
  // This code would removes all connectors:

  fParts.KillAll;
  fConnectors.KillAll;
  fFromLinePart := TcdLinePart.Create(Diagram, Self);
  fToLinePart := FromLinePart;
  AddLinePart(FromLinePart);
  *)

  Diagram.BeginChange;
  try
    // Remove only the visible connectors
    T := 0;
    while T<Connectors.Count do
      begin
      lConnector := TcdLineCon(Connectors[T]);
      if lConnector.AutoPosition
         then Inc(T)
         else RemoveConnector(lConnector);
      end;

    CorrectZOrder;
    ResetBounds;
  finally
    Diagram.EndChange;
    end;
  end;

procedure TcdLine.SplitLinePart (aLinePart: TcdLinePart; aX, aY: integer);
  var
    lConnector: TcdLineCon;
    lNewLinePart1,
    lNewLinePart2: TcdLinePart;
  begin
  // do not split the first and last line part of a three part Line
  if (LineType=lt3PLine) and ((aLinePart=FromLinePart) OR (aLinePart=ToLinePart))
     then Exit;

  Diagram.BeginChange;
  try
    RemoveLinePart(aLinePart);

    lConnector := TcdLineCon.Create(Diagram, Self);
    lNewLinePart1 := TcdLinePart.Create(Diagram, Self);
    lNewLinePart2 := TcdLinePart.Create(Diagram, Self);

    lConnector.FromLinePart := lNewLinePart1;
    lConnector.ToLinePart := lNewLinePart2;

    lNewLinePart1.FromConnector := aLinePart.FromConnector;
    lNewLinePart1.ToConnector := lConnector;

    lNewLinePart2.FromConnector := lConnector;
    lNewLinePart2.ToConnector := aLinePart.ToConnector;

    if Assigned(aLinePart.FromConnector)
       then aLinePart.FromConnector.ToLinePart := lNewLinePart1;

    if Assigned(aLinePart.ToConnector)
       then ALinePart.ToConnector.FromLinePart := lNewLinePart2;

    if FromLinePart = aLinePart
       then fFromLinePart := lNewLinePart1;

    if ToLinePart = aLinePart
       then fToLinePart := lNewLinePart2;

    AddLinePart(lNewLinePart1);
    AddLinePart(lNewLinePart2);
    AddLineCon(lConnector, aX, aY);

    aLinePart.Free;

    CorrectZOrder;
    ResetBounds;
  finally
    Diagram.EndChange;
    end;
  end;

procedure TcdLine.AddLinePart(aLinePart: TcdLinePart);
  var
    T: integer;
  begin
  if Parts.IndexOf(aLinePart) >= 0
     then Exit;

  if not Assigned(aLinePart.FromConnector)
     then Parts.Insert(0, aLinePart)
  else if not Assigned(aLinePart.ToConnector)
     then Parts.Insert(Parts.Count, aLinePart)
     else begin
          for T := 0 to Parts.Count-1 do
            if TcdLinePart(Parts[T]).ToConnector=aLinePart.FromConnector
               then begin
                    Parts.Insert(T+1, aLinePart);
                    Break;
                    end;
          end;

  Diagram.Add(aLinePart);
  end;

procedure TcdLine.RemoveLinePart(aLinePart: TcdLinePart);
  begin
  if Parts.IndexOf (aLinePart) < 0
     then Exit;
  Parts.Remove(aLinePart);
  end;

procedure TcdLine.AddLineCon(aLineCon: TcdLineCon; aX, aY: integer);
  var
     T: integer;
  begin
  if Connectors.IndexOf(aLineCon) >= 0
     then Exit;

  if aLineCon.FromLinePart=FromLinePart
     then Connectors.Insert(0, aLineCon)
  else if aLineCon.ToLinePart=ToLinePart
     then Connectors.Insert(Connectors.Count, aLineCon)
     else begin
          for T := 0 to Connectors.Count-1 do
            if TcdLineCon(Connectors[T]).ToLinePart=aLineCon.FromLinePart
               then begin
                    Connectors.Insert(T+1, aLineCon);
                    Break;
                    end;
          end;

  Diagram.Add(aLineCon, aX, aY, False);
  end;

procedure TcdLine.RemoveLineCon(aLineCon: TcdLIneCon);
  begin
  if Connectors.IndexOf (aLineCon) < 0
     then Exit;
  Connectors.Remove(aLineCon);
  end;

procedure TcdLine.CopyConnectors (aOldLine: TcdLine);
  var
    lPart: TcdLinePart;
    lConnector: TcdLineCon;
    T: integer;
  begin
  for T := 0 to aOldLine.Connectors.Count-1 do
    begin
    lConnector := TcdLineCon(aOldLine.Connectors[T]);
    if not lConnector.AutoPosition
       then begin
            if LineType = lt3PLine
               then lPart := TcdLinePart(Parts[Parts.Count-2])
               else lPart := TcdLinePart(Parts[Parts.Count-1]);
            SplitLinePart(lPart, Diagram.ieXPos(lConnector.CenterPosition.X), Diagram.ieYPos(lConnector.CenterPosition.Y));
            end;
    end;
  end;

procedure TcdLine.SetCardinality(aConObject: TcdConObject; aMin, aMax: integer);

  function GetCardStr(aMin, aMax: integer): string;
    var
      lMinStr, lMaxStr: string;
    begin
    result := '';
    if not Diagram.Style.DisplayCardinality
       then Exit;

    if aMin=cCardWarning
       then aMin := 0;

    if aMin=cCardNone
       then lMinStr := ''
    else if aMin=cCardMany
       then lMinStr := Diagram.Style.CardinalityManyChar
    else lMinStr := IntToStr(aMin);

    if aMax=cCardNone
       then lMaxStr := ''
    else if aMax=cCardMany
       then lMaxStr := Diagram.Style.CardinalityManyChar
    else lMaxStr := IntToStr(aMax);

    if (lMinStr='') and (lMaxStr='')
       then Exit;

    if lMinStr=''
       then result := lMaxStr
    else if lMaxStr=''
       then result := lMinStr
    else result := lMinStr + '..' + lMaxStr;

    result := Diagram.Style.ReplaceCardinalityStr(result);
    end;

  begin
  if FromObject=aConObject
     then begin
          fMinDetails := aMin;
          fMaxDetails := aMax;
          DetailCardTextStr := GetCardStr(aMin, aMax);
          end
     else begin
          fMinMasters := aMin;
          fMaxMasters := aMax;
          MasterCardTextStr := GetCardStr(aMin, aMax);
          end;

  ResetBounds;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdLinePart
//
function TcdLinePart.GetFromPoint: TPoint;
  begin
  if Assigned(FromConnector)
     then result := FromConnector.CenterPosition
     else begin
          if Assigned(ToConnector)
             then result := Line.FromObject.GetConnectionPoint(Line, ToConnector.CenterPosition)
             else result := Line.FromPoint;
          end;
  end;

function TcdLinePart.GetToPoint: TPoint;
  begin
  if Assigned (ToConnector)
     then result := ToConnector.CenterPosition
     else begin
          if Assigned(FromConnector)
             then result := Line.ToObject.GetConnectionPoint(Line, FromConnector.CenterPosition)
             else result := Line.ToPoint;
          end;
  end;

procedure TcdLinePart.ResetBounds_Internal;
  var
    Bounds: TRect;
  begin
  inherited;
  Bounds := TRect.Empty;

  // Create boundsrect
  Bounds.TopLeft := FromPoint;
  Bounds.BottomRight := ToPoint;
  if Bounds.Bottom < Bounds.Top
     then begin
          SwapInt(Bounds.Bottom, Bounds.Top);
          if Bounds.Right < Bounds.Left
             then begin
                  SwapInt(Bounds.Right, Bounds.Left);
                  fSlope := ldTopLeft_BottomRight;
                  end
             else fSlope := ldTopRight_BottomLeft;
          end
     else begin
          if Bounds.Right < Bounds.Left
             then begin
                  SwapInt(Bounds.Right, Bounds.Left);
                  fSlope := ldTopRight_BottomLeft;
                  end
             else fSlope := ldTopLeft_BottomRight;
          end;

  // Add draw margin for horizontal or vertical lines
  Dec(Bounds.Left, Diagram.Style.LineDrawMargin);
  Dec(Bounds.Top, Diagram.Style.LineDrawMargin);
  Inc(Bounds.Right, Diagram.Style.LineDrawMargin);
  Inc(Bounds.Bottom, Diagram.Style.LineDrawMargin);

  SetIntBounds(Bounds.Left, Bounds.Top, Bounds.Right-Bounds.Left, Bounds.Bottom-Bounds.Top);
  end;

function TcdLinePart.InActiveArea (aX, aY: integer): Boolean;
  var
    lp, d: Real;
    x0, y0, x1, y1, xp, yp: integer;
    lDivisor: integer;
  begin
  result := False;

  xp := aX;
  yp := aY;

  if fSlope = ldTopLeft_BottomRight
     then begin
          x0 := Diagram.Style.LineDrawMargin;
          y0 := Diagram.Style.LineDrawMargin;
          x1 := IntWidth-Diagram.Style.LineDrawMargin;
          y1 := IntHeight-Diagram.Style.LineDrawMargin;
          end
     else begin
          x0 := IntWidth-Diagram.Style.LineDrawMargin;
          y0 := Diagram.Style.LineDrawMargin;
          x1 := Diagram.Style.LineDrawMargin;
          y1 := IntHeight-Diagram.Style.LineDrawMargin;
          end;

  // 20100905 Division by zero patch
  lDivisor := sqr(x1-x0)+sqr(y1-y0);
  if lDivisor=0
     then lp := 0
     else lp := ( (x1-x0)*(xp-x0)+(y1-y0)*(yp-y0) ) / lDivisor;
  if (lp < 0) OR (lp > 1)
     then Exit;

  d := sqrt( sqr(xp-x0-lp*(x1-x0)) + sqr(yp-y0-lp*(y1-y0)) );
  result := abs(d) <= Diagram.Style.LineClickMargin; // Trust me ;-)
  end;

procedure TcdLinePart.MouseDown (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer);
  begin
  if aButton = mbLeft
     then Line.SplitLinePart (Self, Left+aX-Scrollbox.HorzScrollbarPosition, Top+aY-Scrollbox.VertScrollbarPosition)
  else if aButton = mbRight
     then begin
          Diagram.PopulateLinePopupMenu(Self);
          Diagram.DoPopupMenu(Diagram.ObjectPopupMenu, aX, aY, Self);
          end
  else inherited MouseDown (aButton, aShiftState, aX, aY);
  end;

constructor TcdLinePart.Create(aDiagram: TcdDiagram; aLine: TcdLine);
  begin
  inherited Create (aDiagram, ooAuto);
  IsEmbeddedObject := True;

  fLine := aLine;
  ControlStyle := ControlStyle - [csOpaque];
  fSlope := ldTopLeft_BottomRight;
  end;

destructor TcdLinePart.Destroy;
  begin
  inherited Destroy;
  end;

procedure TcdLinePart.PaintOnCanvas(aCanvas: TCanvas);
  begin
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    // The rectangle is oversized with the LineDrawMargin for drawing vertical/horizontal lines
    // Because of this the sizes are corrected with the LineDrawMargin

    Pen_Style := LineStyleToPenStyle(Line.LineStyle);
    Pen_Color := Diagram.Style.LinePenColor;
    if fSlope = ldTopLeft_BottomRight
       then begin
            MoveTo (Diagram.Style.LineDrawMargin,Diagram.Style.LineDrawMargin);
            LineTo (IntWidth-Diagram.Style.LineDrawMargin, IntHeight-Diagram.Style.LineDrawMargin);
            end
       else begin
            MoveTo (IntWidth-Diagram.Style.LineDrawMargin, Diagram.Style.LineDrawMargin);
            LineTo (Diagram.Style.LineDrawMargin, IntHeight-Diagram.Style.LineDrawMargin);
            end;
    end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdLineCon
//
procedure TcdLineCon.SetAutoPosition(aValue: Boolean);
  begin
  fAutoPosition := aValue;
  Clickable := not AutoPosition;
  end;

procedure TcdLineCon.PaintOnCanvas (aCanvas: TCanvas);
  begin
  // do not print dots on external canvasses or in readonly diagrams
  if (aCanvas<>Canvas) OR Diagram.Readonly then Exit;

  // do not draw autoposition connectors.
  if AutoPosition then Exit;

  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Brush_Style := bsSolid;
    if Selected
       then Brush_Color := Diagram.Style.ConnectorFillColor_Selected
       else Brush_Color := Diagram.Style.ConnectorFillColor;
    Pen_Style := psSolid;
    if Selected
       then Pen_Color := Diagram.Style.ConnectorPenColor_Selected
       else Pen_Color := Diagram.Style.ConnectorPenColor;

    Rectangle (Diagram.Style.ConnectorClickArea, Diagram.Style.ConnectorClickArea,
       IntWidth-Diagram.Style.ConnectorClickArea, IntHeight-Diagram.Style.ConnectorClickArea);
    end;
  end;

procedure TcdLineCon.ResetBounds_External;
  begin
  // do not call the inherited, reset the line, this will reset the object
  Line.ResetBounds_External;
  end;

procedure TcdLineCon.ResetBounds_Internal;
  var
    lLeft, lTop, lWidth, lHeight, lPartLength: integer;
    lObject: TcdObject;
    lPoint: TPoint;
  begin
  inherited;

  lLeft := IntLeft;
  lTop := IntTop;
  lWidth := Diagram.Style.ConnectorWidth + Diagram.Style.ConnectorClickArea*2;
  lHeight := Diagram.Style.ConnectorHeight + Diagram.Style.ConnectorClickArea*2;
  lPartLength := Line.Diagram.Style.FixedLinePartLength;

  if AutoPosition
     then begin
          // Determine position of the connector
          lObject := nil; // Anti warning
          if Line.FromLinePart=FromLinePart
             then begin
                  lPoint := Line.FromPoint;
                  lObject := Line.FromObject;
                  end
          else if Line.ToLinePart=ToLinePart
             then begin
                  lPoint := Line.ToPoint;
                  lObject := Line.ToObject;
                  end
          else begin
               lPoint := Default(TPoint);
               EInt('TcdLineCon.ResetBounds_Internal', 'Invalid autoposition line connector.');
               end;

          case lObject.GetObjectSide(Line, lPoint) of
            osTop:    begin lLeft := lPoint.X; lTop := lPoint.Y-lPartLength; end;
            osBottom: begin lLeft := lPoint.X; lTop := lPoint.Y+lPartLength; end;
            osLeft:   begin lLeft := lPoint.X-lPartLength; lTop := lPoint.Y; end;
            osRight:  begin lLeft := lPoint.X+lPartLength; lTop := lPoint.Y; end;
            end;

          lLeft := lLeft - (lWidth div 2);
          lTop := lTop - (lHeight div 2);
          end;

  SetIntBounds (lLeft, lTop, lWidth, lHeight);
  end;

procedure TcdLineCon.ResetBounds_Children;
  begin
  inherited;
  FromLinePart.ResetBounds;
  ToLinePart.ResetBounds;
  end;

procedure TcdLineCon.MouseDown (aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer);
  begin
  if aButton = mbRight
     then if Diagram.Selection.Count <= 1
             then begin
                  Diagram.PopulateLineConnectorPopupMenu(Self);
                  Diagram.DoPopupMenu(Diagram.ObjectPopupMenu, aX, aY, Self);
                  end
             else Diagram.DoSelectionPopupMenu(aX, aY, Self)
     else inherited MouseDown (aButton, aShiftState, aX, aY);
  end;

constructor TcdLineCon.Create (aDiagram: TcdDiagram; aLine: TcdLine; aAutoPosition: Boolean = False);
  begin
  inherited Create (aDiagram, ooCentered);
  IsEmbeddedObject := True;

  fLine := aLine;
  AutoPosition := aAutoPosition; // Invoke property setter

  // Sizes needed for SetCenterPosition
  fIntWidth := Diagram.Style.ConnectorWidth + Diagram.Style.ConnectorClickArea*2;
  fIntHeight := Diagram.Style.ConnectorHeight + Diagram.Style.ConnectorClickArea*2;
  end;

destructor TcdLineCon.Destroy;
  begin
  inherited Destroy;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdStaticText
//
procedure TcdStaticText.SetText(aText: string);
  begin
  fText := aText;
  ResetBounds;
  end;

constructor TcdStaticText.Create(aDiagram: TcdDiagram);
  begin
  inherited Create(aDiagram, ooAuto);
  ControlStyle := ControlStyle - [csOpaque];
  end;

procedure TcdStaticText.PaintOnCanvas(aCanvas: TCanvas);
  begin
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Brush_Style := bsClear;
    Font_Name := Diagram.Style.TextFontName;
    Font_Height := Diagram.Style.TextFontHeight;
    Font_Color := Diagram.Style.TextFontColor;
    TextOut(2, 2, Text);
    end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdLineText
//
procedure TcdLineText.ResetBounds_Internal;
  var
    lLeft, lTop, lWidth, lHeight: integer;
    lMargin, lObjectConWidth, lObjectConHeight, lIndex: integer;
    lLinePart: TcdLinePart;
    lPoint: TPoint;
    lObject: TcdConObject;
    lObjectCon: TcdObjectCon;
    lHasObjectCon: Boolean;
  begin
  inherited;

  with Diagram.ScaleCanvas do
    begin
    Canvas := nil;

    Font_Name := Diagram.Style.LineTextFontName;
    Font_Height := Diagram.Style.LineTextFontHeight;
    Font_Color := Diagram.Style.LineTextFontColor;

    lLeft := 0;
    lTop := 0;
    lWidth := TextWidth(Text)+4;
    lHeight := TextHeight(Text)+4;

    if fPosition=ltpMiddle
       then begin
            lIndex := (Line.Parts.Count-1) div 2;
            lLinePart := TcdLinePart(Line.Parts[lIndex]);
            lLeft := lLinePart.IntLeft + (lLinePart.IntWidth div 2) - (lWidth div 2);
            lTop := lLinePart.IntTop + (lLinePart.IntHeight div 2) - (lHeight div 2);
            end
    else begin
         if (fPosition=ltpFromSide_LeftOrTop) OR (fPosition=ltpFromSide_RightOrBottom)
            then begin
                 lPoint := Line.FromPoint;
                 lObject := Line.FromObject;
                 lHasObjectCon := Line.FromObjectConType<>octNone;
                 lObjectCon := Line.FromObjectCon;
                 end
            else begin
                 lPoint := Line.ToPoint;
                 lObject := Line.ToObject;
                 lHasObjectCon := Line.ToObjectConType<>octNone;
                 lObjectCon := Line.ToObjectCon;
                 end;

         lMargin := Diagram.Style.LineTextMargin;
         if lHasObjectCon
            then begin
                 lObjectConWidth := lObjectCon.IntWidth;
                 lObjectConHeight := lObjectCon.IntHeight;
                 end
            else begin
                 lObjectConWidth := 0;
                 lObjectConHeight := 0;
                 end;

         if (fPosition=ltpFromSide_LeftOrTop) OR (fPosition=ltpToSide_LeftOrTop)
            then begin
                 case lObject.GetObjectSide(Line, lPoint) of
                   osLeft:   begin
                             lLeft := lPoint.X-lWidth-lMargin-lObjectConWidth;
                             lTop := lPoint.Y-lHeight-lMargin;
                             end;
                   osRight:  begin
                             lLeft := lPoint.X+lMargin+lObjectConWidth;
                             lTop := lPoint.Y-lHeight-lMargin;
                             end;
                   osTop:    begin
                             lLeft := lPoint.X-lWidth-lMargin;
                             lTop := lPoint.Y-lHeight-lMargin-lObjectConHeight;
                             end;
                   osBottom: begin
                             lLeft := lPoint.X-lWidth-lMargin;
                             lTop := lPoint.Y+lMargin+lObjectConHeight;
                             end;
                   end;
                 end
            else begin
                 case lObject.GetObjectSide(Line, lPoint) of
                   osLeft:   begin
                             lLeft := lPoint.X-lWidth-lMargin-lObjectConWidth;
                             lTop := lPoint.Y+lMargin;
                             end;
                   osRight:  begin
                             lLeft := lPoint.X+lMargin+lObjectConWidth;
                             lTop := lPoint.Y+lMargin;
                             end;
                   osTop:    begin
                             lLeft := lPoint.X+lMargin;
                             lTop := lPoint.Y-lHeight-lMargin-lObjectConHeight;
                             end;
                   osBottom: begin
                             lLeft := lPoint.X+lMargin;
                             lTop := lPoint.Y+lMargin+lObjectConHeight;
                             end;
                   end;
                 end;
         end;

    SetIntBounds(lLeft, lTop, lWidth, lHeight);
    end;
  end;

constructor TcdLineText.Create(aDiagram: TcdDiagram; aLine: TcdLine; aPosition: TcdLineTextPosition);
  begin
  inherited Create(aDiagram);
  IsEmbeddedObject := True;
  fLine := aLine;
  fPosition := aPosition;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdObjectCon
//
procedure TcdObjectCon.ResetBounds_Internal;
  var
    lStyle: TcdStyle;
    RC, d: Double;
    lArrowAngleRad: Double;
    lLeft, lTop, lWidth, lHeight,
    lMinX, lMinY, lMaxX, lMaxY,
    lArrowSide, lRadius: integer;
    lFromPoint, lToPoint, lEdgeOfCardDot: TPoint;
    lObject: TcdConObject;
    lSide: TcdObjectSide;
  begin
  inherited;

  lStyle := Diagram.Style;

  if Line.FromObjectCon=Self
     then begin
          lFromPoint := Line.FromLinePart.ToPoint;
          lToPoint := Line.FromPoint;
          lObject := Line.FromObject;
          fType := Line.FromObjectConType;
          fMinCardA := Line.MinMasters;
          fMaxCardA := Line.MaxMasters;
          fMinCardB := Line.MinDetails;
          fMaxCardB := Line.MaxDetails;
          end
     else begin
          lFromPoint := Line.ToLinePart.FromPoint;
          lToPoint := Line.ToPoint;
          lObject := Line.ToObject;
          fType := Line.ToObjectConType;
          fMinCardA := Line.MinDetails;
          fMaxCardA := Line.MaxDetails;
          fMinCardB := Line.MinMasters;
          fMaxCardB := Line.MaxMasters;
          end;

  lArrowSide := lStyle.ArrowSide;
  lArrowAngleRad := lStyle.ArrowAngleRad;
  fToPoint := lToPoint;

  lEdgeOfCardDot := TPoint.Zero;

  // There is no object connector
  if fType=octNone
     then begin
          SetIntBounds(lToPoint.X, lToPoint.Y, 0, 0);
          Exit;
          end;

  // Cardinality Dot
  if fType=octCardDot
     then begin
          if ( (lStyle.CardinalityDotSide=lsTo)   and (Line.ToObjectCon=Self)   and ((fMinCardA=1) OR (fMinCardA=cCardWarning)) ) OR
             ( (lStyle.CardinalityDotSide=lsFrom) and (Line.FromObjectCon=Self) and ((fMinCardA=1) OR (fMinCardA=cCardWarning)) )
             then begin
                  lLeft := lToPoint.X-(lStyle.CardinalityDotSize div 2);
                  lTop := lToPoint.Y-(lStyle.CardinalityDotSize div 2);
                  lWidth := lStyle.CardinalityDotSize;
                  lHeight := lStyle.CardinalityDotSize;

                  if lStyle.CardinalityDotSide=lsFrom
                     then begin
                          case lObject.GetObjectSide(Line, lToPoint) of
                             osLeft:    lLeft := lToPoint.X-lStyle.CardinalityDotSize;
                             osRight:   lLeft := lToPoint.X;
                             osTop:     lTop := lToPoint.Y-lStyle.CardinalityDotSize;
                             osBottom:  lTop := lToPoint.Y;
                             end;
                          end;

                  SetIntBounds(lLeft, lTop, lWidth, lHeight);
                  end
             else begin
                  SetIntBounds(lToPoint.X, lToPoint.Y, 0, 0);
                  end;

          Exit;
          end;

  // Claw
  if fType=octClaw
     then begin
          if lFromPoint.X<>lToPoint.X
             then begin
                  RC := (lFromPoint.Y-lToPoint.Y)/(lFromPoint.X-lToPoint.X);
                  d := arctan(RC);
                  end
             else begin
                  // Vertical line (no RC)
                  if lFromPoint.Y>lToPoint.Y
                     then d := (Pi/2)
                     else d := -(Pi/2);
                  end;
          if lFromPoint.X<lToPoint.X then d := -Pi+d; // Quadrant correction

          fToPoint.X := Round(cos(d)*lArrowSide) + lToPoint.X;
          fToPoint.Y := Round(sin(d)*lArrowSide) + lToPoint.Y;

          lRadius := lStyle.CardinalityDotSize div 2;

          // The Claw uses 7 points to draw all possible cardinalities:
          // P1 and P2 are on the object and define the connection points of the
          // claw.
          // P3 and P4 define the line that should be drawn when the max card = 1.
          // P5 and P6 define the line that should be drawn when the min card = 1.
          // ToPoint is the start of the claw and the center of the circle to be
          // drawn when the min card = 0 or Warning.

          lSide := lObject.GetObjectSide(Line, lToPoint);
          case lSide of
            osLeft, osRight:
                 begin
                 fP1.X := lToPoint.X;
                 fP1.Y := lToPoint.Y - lStyle.ClawWidth;
                 fP2.X := lToPoint.X;
                 fP2.Y := lToPoint.Y + lStyle.ClawWidth;

                 fP3.Y := fP1.Y;
                 fP4.Y := fP2.Y;
                 fP5.X := fToPoint.X;
                 fP5.Y := fP1.Y;
                 fP6.X := fToPoint.X;
                 fP6.Y := fP2.Y;
                 lEdgeOfCardDot.Y := fToPoint.Y;

                 if lSide=osLeft
                    then begin
                         fP3.X := fToPoint.X + ((fP1.X-fToPoint.X) div 2);
                         fP4.X := fP3.X;
                         lEdgeOfCardDot.X := fToPoint.X-lRadius;
                         end
                    else begin
                         fP3.X := fP1.X + ((fToPoint.X-fP1.X) div 2);
                         fP4.X := fP3.X;
                         lEdgeOfCardDot.X := fToPoint.X+lRadius;
                         end;
                 end;
            osTop, osBottom:
                 begin
                 fP1.X := lToPoint.X - lStyle.ClawWidth;
                 fP1.Y := lToPoint.Y;
                 fP2.X := lToPoint.X + lStyle.ClawWidth;
                 fP2.Y := lToPoint.Y;

                 fP3.X := fP1.X;
                 fP4.X := fP2.X;
                 fP5.X := fP1.X;
                 fP5.Y := fToPoint.Y;
                 fP6.X := fP2.X;
                 fP6.Y := fToPoint.Y;
                 lEdgeOfCardDot.X := fToPoint.X;

                 if lSide=osTop
                    then begin
                         fP3.Y := fToPoint.Y + ((fP1.Y-fToPoint.Y) div 2);
                         fP4.Y := fP3.Y;
                         lEdgeOfCardDot.Y := fToPoint.Y-lRadius;
                         end
                    else begin
                         fP3.Y := fP1.Y + ((fToPoint.Y-fP1.Y) div 2);
                         fP4.Y := fP3.Y;
                         lEdgeOfCardDot.Y := fToPoint.Y+lRadius;
                         end;
                 end;
            end;
          end

  // Arrow
     else begin
          if lFromPoint.X<>lToPoint.X
             then begin
                  RC := (lFromPoint.Y-lToPoint.Y)/(lFromPoint.X-lToPoint.X);
                  d := arctan(RC)-lArrowAngleRad;
                  end
             else begin
                  // Vertical line (no RC)
                  if lFromPoint.Y>lToPoint.Y
                     then d := (Pi/2)-lArrowAngleRad
                     else d := -(Pi/2)-lArrowAngleRad;
                  end;
          if lFromPoint.X<lToPoint.X then d := -Pi+d; // Quadrant correction

          fP1.X := Round(cos(lArrowAngleRad*2+d)*lArrowSide)+lToPoint.X;
          fP1.Y := Round(sin(lArrowAngleRad*2+d)*lArrowSide)+lToPoint.Y;
          fP2.X := Round(cos(d)*lArrowSide)+lToPoint.X;
          fP2.Y := Round(sin(d)*lArrowSide)+lToPoint.Y;

          // The EdgeOfCardDot is used to determine the bounds of the connector.
          // Since a arrow does not use a CardDot, just assign P1.
          lEdgeOfCardDot.X := fP1.X;
          lEdgeOfCardDot.Y := fP1.Y;
          end;

  // Arrow or Claw

  // Derive drawbox
  lMinX := Min(Min(Min(fToPoint.X, fP1.X), fP2.X), lEdgeOfCardDot.X);
  lMinY := Min(Min(Min(fToPoint.Y, fP1.Y), fP2.Y), lEdgeOfCardDot.Y);
  lMaxX := Max(Max(Max(fToPoint.X, fP1.X), fP2.X), lEdgeOfCardDot.X);
  lMaxY := Max(Max(Max(fToPoint.Y, fP1.Y), fP2.Y), lEdgeOfCardDot.Y);
  SetIntBounds(lMinX, lMinY, Abs(lMinX-lMaxX)+lStyle.ScaleMargin, Abs(lMinY-lMaxY)+lStyle.ScaleMargin);

  // Derive relative coordinates
  fToPoint.X := fToPoint.X-lMinX;
  fToPoint.Y := fToPoint.Y-lMinY;
  fP1.X := fP1.X-lMinX; fP1.Y := fP1.Y-lMinY; fP2.X := fP2.X-lMinX; fP2.Y := fP2.Y-lMinY;
  fP3.X := fP3.X-lMinX; fP3.Y := fP3.Y-lMinY; fP4.X := fP4.X-lMinX; fP4.Y := fP4.Y-lMinY;
  fP5.X := fP5.X-lMinX; fP5.Y := fP5.Y-lMinY; fP6.X := fP6.X-lMinX; fP6.Y := fP6.Y-lMinY;
  end;

constructor TcdObjectCon.Create(aDiagram: TcdDiagram; aLine: TcdLine);
  begin
  inherited Create(aDiagram, ooAuto);
  IsEmbeddedObject := True;
  ControlStyle := ControlStyle - [csOpaque];
  fLine := aLine;
  end;

destructor TcdObjectCon.Destroy;
  begin
  inherited Destroy;
  end;

procedure TcdObjectCon.PaintOnCanvas(aCanvas: TCanvas);
  var
    lStyle: TcdStyle;
    lRadius: integer;
  begin
  lStyle := Diagram.Style;
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    case fType of
      octNone:
         begin
         // do not paint
         end;

      octCardDot:
         begin
         if ( (lStyle.CardinalityDotSide=lsTo)   and (Line.ToObjectCon=Self)   and ((fMinCardA=1) or (fMinCardA=cCardWarning)) ) or
            ( (lStyle.CardinalityDotSide=lsFrom) and (Line.FromObjectCon=Self) and ((fMinCardA=1) or (fMinCardA=cCardWarning)) )
            then begin
                 if fMinCardA=1
                    then begin
                         Brush_Style := bsSolid;
                         Brush_Color := lStyle.ObjectConFillColor;
                         end
                    else begin
                         Brush_Style := bsSolid;
                         Brush_Color := lStyle.CardinalityWarningDotFillColor;
                         end;
                 Pen_Style := psSolid;
                 Pen_Color := lStyle.ObjectConPenColor;
                 Ellipse(IntClientRect);
                 end;
         end;

      octArrow:
         begin
         Brush_Style := bsSolid;
         Brush_Color := lStyle.ObjectConFillColor;
         Pen_Style := psSolid;
         Pen_Color := lStyle.ObjectConPenColor;
         Polygon([fToPoint, fP1, fP2]);
         end;

      octOpenArrow:
         begin
         Brush_Style := bsSolid;
         Brush_Color := clWhite;
         Pen_Style := psSolid;
         Pen_Color := lStyle.ObjectConPenColor;
         Polygon([fToPoint, fP1, fP2]);
         end;

      octLineArrow:
         begin
         Pen_Style := psSolid;
         Pen_Color := lStyle.ObjectConPenColor;
         MoveTo(fP1.X, fP1.Y);
         LineTo(fToPoint.X, fToPoint.Y);
         MoveTo(fP2.X, fP2.Y);
         LineTo(fToPoint.X, fToPoint.Y);
         end;

      octClaw:
         begin
         Pen_Style := psSolid;
         Pen_Color := lStyle.ObjectConPenColor;

         if fMaxCardB=1
            then begin
                 MoveTo(fP3.X, fP3.Y);
                 LineTo(fP4.X, fP4.Y);
                 end
            else begin
                 MoveTo(fP1.X, fP1.Y);
                 LineTo(fToPoint.X, fToPoint.Y);
                 MoveTo(fP2.X, fP2.Y);
                 LineTo(fToPoint.X, fToPoint.Y);
                 end;

         if (fMinCardB=0) OR (fMinCardB=cCardWarning)
            then begin
                 lRadius := (lStyle.CardinalityDotSize div 2);
                 Ellipse(fToPoint.X-lRadius, fToPoint.Y-lRadius, fToPoint.X+lRadius, fToPoint.Y+lRadius);
                 end
            else begin
                 MoveTo(fP5.X, fP5.Y);
                 LineTo(fP6.X, fP6.Y);
                 end;
         end;

      end;
    end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdPageSize
//
constructor TcdPageSize.Create(aDiagram: TcdDiagram);
  begin
  inherited Create(aDiagram, ooUpperLeft);

  Clickable := False;
  fPrintable := False;

  fZOrder := 1000;
  end;

procedure TcdPageSize.PaintOnCanvas(aCanvas: TCanvas);
  var
    lText: string;
  begin
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    // Page
    Pen_Style := psDot;
    Pen_Color := clBlack;
    Brush_Color := clLtGray;
    Rectangle(IntClientRect);

    // Printable area
    Pen_Style := psDot;
    Brush_Color := Diagram.Scrollbox.Color;
    Pen_Color := clBlack;
    Rectangle(Diagram.fPageSize.PrintArea);

    // Paper name
    Font_Name := Diagram.Style.TextFontName;
    Font_Height := Diagram.Style.TextFontHeight;
    Font_Color := clGray;
    lText := Diagram.fPageSize.Text;
    TextOut(IntClientRect.Right-TextWidth(lText) - Diagram.Style.ScaleMargin - 1, IntClientRect.Bottom - 
	   TextHeight(lText) - Diagram.Style.ScaleMargin, lText);
    end;
  end;

procedure TcdPageSize.ResetBounds_Internal;
  begin
  inherited;

  SetIntBounds(0, 0, Diagram.fPageSize.Width, Diagram.fPageSize.Height);
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcPageSize
//
constructor TcPageSize.Create(aDiagram: TcdDiagram);
  begin
  inherited Create;
  fDiagram := aDiagram;
  end;

function TcPageSize.Reinit(aForceUpdate: Boolean = False): boolean;

  function MulDivRect(aRect: TRect; aMD: TMulDiv): TRect;
    begin
    result.Left := MulDiv(aRect.Left, aMD.D, aMD.M);
    result.Top:= MulDiv(aRect.Top, aMD.D, aMD.M);
    result.Right := MulDiv(aRect.Right, aMD.D, aMD.M);
    result.Bottom := MulDiv(aRect.Bottom, aMD.D, aMD.M);
    end;

  var
    lDPIX,
    lDPIY: integer;

  function CalculateRequiredPaperSize(aRequiredRect: TRect; aUnprintableMargin: TPoint): Boolean;
    var
      lWidth, lHeight, lAsize: integer;
      lLandscape: Boolean;
    begin
    if not fDiagram.AllowPaperChange
       then begin
            lWidth := Printer.PaperSize.Width;
            lHeight := Printer.PaperSize.Height;

            fWidth := Round(lWidth / lDPIX * Screen.PixelsPerInch);
            fHeight := Round(lHeight / lDPIY * Screen.PixelsPerInch);

            fText := Format('%d mm x %d mm', [Round(lWidth / lDPIX * 2.54 * 10), Round(lHeight/ lDPIY * 2.54 * 10)]);
            result := true;
            Exit;
            end;
    result := False;

    // Start with A4
    lWidth := MulDiv(Round(8.26*Screen.PixelsPerInch), fDiagram.ScaleMD100.D, fDiagram.ScaleMD100.M);
    lHeight := MulDiv(Round(11.69*Screen.PixelsPerInch), fDiagram.ScaleMD100.D, fDiagram.ScaleMD100.M);
    lAsize := 4;
    lLandscape := False;

    lWidth := MulDiv(lWidth, fDiagram.ScaleMDPrint.D, fDiagram.ScaleMDPrint.M);
    lHeight := MulDiv(lHeight, fDiagram.ScaleMDPrint.D, fDiagram.ScaleMDPrint.M);

    // Fits in current paper?
    while lAsize >= 0 do
      begin
      if (aRequiredRect.Right <= (lWidth - aUnprintableMargin.X)) and (aRequiredRect.Bottom <= (lHeight - aUnprintableMargin.Y)) and
         (
          // if we requested another paper because of the unprintable area, make
          // sure we don't select the same paper again
          (lAsize < fASize) or ((lASize = fASize) and not fLandscape and lLandscape)
         )
         then begin
              fWidth := lWidth;
              fHeight := lHeight;
              fAsize := lASize;
              fLandScape := lLandscape;
              fText := 'A' + IntToStr(lAsize);
              if lWidth > lHeight
                 then fText := fText + ' L';

              result := True;
              Break;
              end;
      // Make landscape
      if lWidth < lHeight
         then begin
              SwapInt(lWidth, lHeight);
              lLandscape := True;
              end
         else begin
              Dec(lASize);

              lHeight := lHeight * 2;
              lLandscape := False;
              end;
      end;
    end;

  var
    lRequiredRect,
    lRect: TRect;
    lPrintArea: TRect;
    lChanged: Boolean;
    lPaperSupported: Boolean;
    lPrevWidth,
    lPrevHeight: integer;
    lUnprintableMargin: TPoint;
  begin
  result := false;
  (*
   TPrinter:
   PaperWidth:  Physical width of paper
   PaperHeight: Physical height of paper
   PageWidth:   Printable width on page
   PageHeight:  Printable height of paper
  *)

  //!! sometimes this method is slow if we change the paper.
  //!! Currently this is in the call to:     lPrintArea := Printer.PaperSize.PaperRect.WorkRect;
  //!! It happens when you move an object to a new area outside the 'LastRequiredRect'. We
  //!! could cache more (store the maximum/minmum used rect for this papersi or something with the printer)
  //!! and only call the printer API on a printer switch or initial request of the size or a size beyond the cache.

  if fDiagram.IsChanging and not aForceUpdate
    then Exit;

  lChanged := False;
  lPrevWidth := fWidth;
  lPrevHeight := fHeight;


  // Get current area of objects
  fDiagram.GetObjectsIntBounds(False, lRequiredRect);

  // Major changes, then update
  if not aForceUpdate and (fLastScalePercentagePrint = fDiagram.ScalePercentagePrint)
    then begin
         // Check for subtile changes

         // Required rect not changed? Skip update
         if (lRequiredRect.Right = fLastRequiredRect.Right) and (lRequiredRect.Bottom = fLastRequiredRect.Bottom) 
		   then Exit;

         // Paper is static, so no change needed
         if not fDiagram.AllowPaperChange
           then begin
                fRequiredScalePercentagePrint := fDiagram.ScalePercentagePrint;
                Exit;
                end;
         end;

  // Init at high values
  if fDiagram.AllowPaperChange
    then begin
         fAsize := 5;
         fLandscape := False;
         end;

  fLastRequiredRect := lRequiredRect;
  fLastScalePercentagePrint := fDiagram.ScalePercentagePrint;

  // First, try paper paper with no margins to get a initial paper
  lUnprintableMargin := Types.Point(0, 0);
  while True do
    begin
    lDPIX := Printer.XDPI;// GetDeviceCaps(Printer.Handle, LOGPIXELSX);
    lDPIY := Printer.YDPI;// GetDeviceCaps(Printer.Handle, LOGPIXELSY);

    // Set paper to printer
    lPaperSupported := fDiagram.ChangePrinterPaperToPageSize(fDiagram.ScalePercentagePrint);

    // Get paper
    if not CalculateRequiredPaperSize(lRequiredRect, lUnprintableMargin)
      then lPaperSupported := False;

    // Printable area of the printer
    lPrintArea := Printer.PaperSize.PaperRect.WorkRect;

    // Store print margins
    fPrintMargin := Types.Point(lPrintArea.Left, lPrintArea.Top);

    // Calculate back to inches and thus pixels on the screen
    lRect.Left := Round((lPrintArea.Left / lDPIX) * Screen.PixelsPerInch);
    lRect.Right := Round((lPrintArea.Right / lDPIX) * Screen.PixelsPerInch);
    lRect.Top := Round((lPrintArea.Top / lDPIY) * Screen.PixelsPerInch);
    lRect.Bottom := Round((lPrintArea.Bottom / lDPIY) * Screen.PixelsPerInch);

    // Use MulDivs to calculate pixels in current viewport
    lRect := MulDivRect(lRect, fDiagram.ScaleMD100);
    lPrintArea := MulDivRect(lRect, fDiagram.ScaleMDPrint);

    // Paper not supported, use real paper size and the unprintable area
    if not lPaperSupported and fDiagram.AllowPaperChange
       then begin
            lPrintArea.Right := fWidth - lPrintArea.Left * 2;
            lPrintArea.Bottom := fHeight - lPrintArea.Top * 2;
            end;

    // If we're changing papers, the printable top-left can shift. This
    // is a bit of a problem. So if we can change paper, move all unprintable
    // margins to the right and bottom and keep the top-left stable
    if fDiagram.AllowPaperChange or fDiagram.DoNotUseTopLeftUnprintableMargin 
       then begin
            lPrintArea.Right := lPrintArea.Right - lPrintArea.Left;
            lPrintArea.Left := 0;
            lPrintArea.Bottom := lPrintArea.Bottom - lPrintArea.Top;
            lPrintArea.Top := 0;
            end;

    // Objects fit on complete page, but does it fit on this printer?
    lUnprintableMargin := Types.Point(0, 0);
    if (lPrintArea.Right < lRequiredRect.Right)
       then lUnprintableMargin.X := fWidth - lPrintArea.Right;

    if lPrintArea.Bottom < lRequiredRect.Bottom
       then lUnprintableMargin.Y := fHeight - lPrintArea.Bottom;

    // if this size wasn't supported or it just fits, use the current paper
    if not lPaperSupported or (lUnprintableMargin.X = 0) and (lUnprintableMargin.Y = 0) OR not fDiagram.AllowPaperChange
       then Break;
    end;

  // if it had to fit, how much should we have scaled
  if not lPaperSupported
     then fRequiredScalePercentagePrint := Round(Min(
                (fLastRequiredRect.Right / lPrintArea.Right),
                (fLastRequiredRect.Bottom / lPrintArea.Bottom))*100)
     else fRequiredScalePercentagePrint := fDiagram.ScalePercentagePrint;

  // Make sure the page size line is always visible
  if lPrintArea.Right >= fWidth - 1
     then lPrintArea.Right := fWidth - 1;
  if lPrintArea.Bottom >= fHeight
     then lPrintArea.Bottom := fHeight - 1;

  // Change since previous calculation
  if (lPrevWidth <> fWidth) or (lPrevHeight <> fHeight) or (lPrintArea.Left <> fPrintArea.Left) or
     (lPrintArea.Top <> fPrintArea.Top) or (lPrintArea.Right <> fPrintArea.Right) or (lPrintArea.Bottom <> fPrintArea.Bottom)
     then lChanged := True;

  // Set area
  fPrintArea := lPrintArea;

  if lChanged
     then begin
          fDiagram.ForceRedraw;
          result := true;
          end;
  end;

procedure TcPageSize.SetLandscape(const Value: Boolean);
  begin
  fLandscape := Value;
  Reinit(True);
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdText
//
procedure TcdText.CMFontChanged(var Message: TMessage);
  begin
  {$IFDEF DELPHI_SIDNEY_UP}
  if not IScaling
     then fFontHeight := Font.Height;
  {$else}
  fFontHeight := Font.Height;
  {$ENDIF}
  end;

constructor TcdText.Create(aDiagram: TcdDiagram);
  begin
  inherited Create(aDiagram, ooUpperLeft);

  if Assigned(DefaultFont)
     then begin
          Font.Assign(DefaultFont);
          end
     else begin
          Font.Name := Diagram.Style.TextFontName;
          Font.Height := cFontSize12;
          Font.Color := Diagram.Style.TextFontColor;
          end;
  end;

procedure TcdText.PopulatePopupMenu(aMenu: TPopupMenu);
  begin
  Diagram.CreateMenuItem(aMenu, '&Edit Text', 0, Handle_Edit);
  Diagram.CreateMenuItem(aMenu, '&Edit Font', 0, Handle_Font);
  Diagram.CreateMenuItem(aMenu, '&Remove', 0, Diagram.Handle_DefaultObject_Remove);
  end;

class destructor TcdText.Destroy;
  begin
  if Assigned(TcdText.DefaultFont)
     then FreeAndNil(TcdText.DefaultFont);
  end;

procedure TcdText.Handle_Edit(aSender: TObject);
  var
    lTextStr: string;
  begin
  lTextStr := Text;
  lTextStr := StringReplace(lTextStr, #13#10, '<br>', [rfReplaceAll]);
  if InputQuery('Text', 'New text', lTextStr)
     then begin
	      Text := lTextStr;
		  Invalidate;
		  end;
  end;

procedure TcdText.Handle_Font(aSender: TObject);
  var
    lFontDialog: TFontDialog;
  begin
  lFontDialog := TFontDialog.Create(Self);
  lFontDialog.Font.Assign(Font);
  if lFontDialog.Execute
     then begin
          UpdateFont(lFontDialog.Font);
          ResetBounds;
          end;
  lFontDialog.Free;
  end;

procedure TcdText.PaintOnCanvas(aCanvas: TCanvas);
  var
    lRect: TRect;
    lTextStyle: TTextStyle;
    lCalcRect: TRect;
    lSelectedRect: TRect;
    lAngle: double;
  begin
  Diagram.ScaleCanvas.Canvas := aCanvas;

  lTextStyle := fTextStyle;

  Diagram.ScaleCanvas.Brush_Style := bsClear;
  Diagram.ScaleCanvas.Font_Name := Font.Name;
  Diagram.ScaleCanvas.Font_Height := Diagram.eiFontHeight(fFontHeight);
  Diagram.ScaleCanvas.Font_Color := Font.Color;
  Diagram.ScaleCanvas.Font_Style := Font.Style;
  Diagram.ScaleCanvas.Font_Orientation := Font.Orientation;
  Diagram.ScaleCanvas.Font_IntercharacterSpacing := IntercharacterSpacing;

  if Diagram.ScaleCanvas.Brush_Color <> clNone then
    lTextStyle.Opaque := True;

  if Centered
     then lTextStyle.Alignment := taCenter;

  lRect := IntClientRect;

  if Font.Orientation > 0
    then begin
         lCalcRect := IntClientRect;
         DrawText(Diagram.ScaleCanvas.Canvas.Handle, PChar(Text), Length(Text), lCalcRect, DT_LEFT or DT_EXPANDTABS or
           DT_NOCLIP or DT_CALCRECT);

        lRect := IntClientRect;

        lAngle := Font.Orientation / 10 * pi / 180;
        lCalcRect := RotateRect(lCalcRect.Width, lCalcRect.Height, lAngle);
        OffsetRect(lRect, -lCalcRect.Left, -lCalcRect.Top);

        lTextStyle.Alignment := taLeftJustify;
        lTextStyle.ExpandTabs := True;
        lTextStyle.Clipping := False;
        end;

  if Selected 
    then begin
         lSelectedRect := lRect;
         InflateRect(lSelectedRect, 1, 1);
         Diagram.ScaleCanvas.Brush_Color := Diagram.Style.FillColor_Selected;
         Diagram.ScaleCanvas.Rectangle(lSelectedRect);
         end;

  Diagram.ScaleCanvas.TextRect(lRect, Text, lTextStyle);
  end;

procedure TcdText.ResetBounds_Internal;

  function TextStyleToOptions(aStyle: TTextStyle): Longint;
    begin
    Result := 0;
    case aStyle.Alignment of
      taRightJustify: Result := DT_RIGHT;
      taCenter: Result := DT_CENTER;
    end;
    case aStyle.Layout of
      tlCenter: Result := Result or DT_VCENTER;
      tlBottom: Result := Result or DT_BOTTOM;
    end;
    if aStyle.EndEllipsis then
      Result := Result or DT_END_ELLIPSIS;
    if aStyle.WordBreak then
    begin
      Result := Result or DT_WORDBREAK;
      if aStyle.EndEllipsis then
        Result := Result and not DT_END_ELLIPSIS;
    end;

    if aStyle.SingleLine then
      Result := Result or DT_SINGLELINE;

    if not aStyle.Clipping then
      Result := Result or DT_NOCLIP;

    if aStyle.ExpandTabs then
      Result := Result or DT_EXPANDTABS;

    if not aStyle.ShowPrefix then
      Result := Result or DT_NOPREFIX;

    if aStyle.RightToLeft then
      Result := Result or DT_RTLREADING;
    end;

  var
    lWidth: integer;
    lRect: TRect;
    lText: string;
    lStrLst: TStringList;

    lAngle: Double;
    lCalcRect: TRect;

    lOptions: Integer;
    lMultiLine: Boolean;
  begin
  inherited;

  Diagram.ScaleCanvas.Canvas := nil;

  Diagram.ScaleCanvas.Font_Name := Font.Name;
  Diagram.ScaleCanvas.Font_Height := Diagram.eiFontHeight(fFontHeight);

  Diagram.ScaleCanvas.Font_Color := Font.Color;
  Diagram.ScaleCanvas.Font_Style := Font.Style;
  Diagram.ScaleCanvas.Font_Orientation := Font.Orientation;
  Diagram.ScaleCanvas.Font_IntercharacterSpacing := IntercharacterSpacing;

  lRect := IntClientRect;
  if FixedWidth > 0
    then begin
         lRect.Width := fDiagram.ieX(Diagram.MmToInt(fFixedWidth));
         fTextStyle.Wordbreak := True;
         end
  else begin
       lStrLst := TStringList.Create;
       lStrLst.Text := Text;
       lWidth := 0;
       for lText in lStrLst do
         lWidth := Max(lWidth, Diagram.ScaleCanvas.fFontCalcCanvas.TextWidth(lText));
       lStrLst.Free;
       lRect.Width := lWidth;
       fTextStyle.Wordbreak := False;
  end;
  lText := Text;

  // TextRect doesn't support IntercharacterSpacing using tfCalcRect
  lMultiLine := lText.Contains(#10) or (FixedWidth > 0);
  if lMultiLine 
    then begin
         lOptions := TextStyleToOptions(fTextStyle);

         DrawText(Diagram.ScaleCanvas.fFontCalcCanvas.Handle, PChar(lText), Length(lText), lRect, DT_CALCRECT or lOptions);
         end
  else begin
       lRect.Height := Diagram.ScaleCanvas.fFontCalcCanvas.TextHeight(lText);
       end;
  if Centered 
    then lRect.Width := fDiagram.ieX(Diagram.MmToInt(fFixedWidth));

  if Font.Orientation = 0 
    then SetIntBounds(IntLeft, IntTop, fDiagram.eiX(lRect.Width), fDiagram.eiX(lRect.Height))
  else begin
       lAngle := Font.Orientation * 0.1 * pi/180;
       lCalcRect := RotateRect(lRect.Width, lRect.Height, lAngle);
       OffsetRect(lCalcRect, -lCalcRect.Left, -lCalcRect.Top);

       SetIntBounds(IntLeft + fDiagram.eiX(lCalcRect.Left), IntTop + fDiagram.eiY(lCalcRect.Top), fDiagram.eiX(lCalcRect.Right),
         fDiagram.eiY(lCalcRect.Bottom));
       end;
  end;

procedure TcdText.SetText(const Value: string);
  begin
  fText := Value;
  fText := StringReplace(fText, '<br>', #13#10, [rfReplaceAll]);
  ResetBounds;
  end;


procedure TcdText.LoadFromJson(aJsonObject: TJsonObject);
  var
    lLeft, lTop: integer;
    lStr: string;
  begin
  inherited;

  if not aJsonObject.TryGetValue('text', lStr) then InvalidJson_xExpected('text');
  Text := lStr;

  if not aJsonObject.TryGetValue('left', lLeft) then InvalidJson_xExpected('left');
  if not aJsonObject.TryGetValue('top', lTop) then InvalidJson_xExpected('top');
  SetIntBounds(lLeft, lTop, 0, 0);

  if not aJsonObject.TryGetValue('fontName', lStr) then InvalidJson_xExpected('fontName');
  Font.Name := lStr;
  if not aJsonObject.TryGetValue('fontHeight', fFontHeight) then InvalidJson_xExpected('fontHeight');
  Font.Height := fFontHeight;
  if not aJsonObject.TryGetValue('fontStyles', lStr) then InvalidJson_xExpected('fontStyles');
  Font.Style := StrToFontStyles(lStr);
  end;

procedure TcdText.SaveToJson(aJsonObject: TJsonObject);
  var
    lStyle: TFontStyles;
    lStr: string;
  begin
  inherited;

  lStr := Text;
  aJsonObject.AddPair('text', lStr);

  aJsonObject.AddPair('left', TJsonIntegerNumber.Create(IntLeft));
  aJsonObject.AddPair('top', TJsonIntegerNumber.Create(IntTop));

  lStr := Font.Name;
  aJsonObject.AddPair('fontName', lStr);
  aJsonObject.AddPair('fontHeight', TJsonIntegerNumber.Create(fFontHeight));
  lStyle := Font.Style;
  lStr := FontStylesToStr(lStyle);
  aJsonObject.AddPair('fontStyles', lStr);
  end;


procedure TcdText.UpdateFont(aFont: TFont);
  begin
  Font.Assign(aFont);
  if not Assigned(DefaultFont)
     then DefaultFont := TFont.Create;
  DefaultFont.Assign(aFont);
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdResizeableObject
//
constructor TcdResizeableObject.Create(aDiagram: TcdDiagram; aOrigin: TcdObjectOrigin);
  begin
  inherited Create(aDiagram, aOrigin);

  //OnMouseLeave := MouseLeave;
  end;

function TcdResizeableObject.InResizeArea(aX, aY: integer): TfrSizeOption;
  const
    cMargin = 2;
  var
    lRect: TRect;
  begin
  result := frsoNone;
  lRect := ClientRect;
  InflateRect(lRect, -cMargin * 2, -cMargin * 2);
  if not PtInRect(lRect, Types.Point(aX, aY))
     then begin
          if (aX <= lRect.Left) OR (aX >= lRect.Right)
             then if (aY <= lRect.Top) OR (aY >= lRect.Bottom)
                     then if AspectRatio > 0
                             then result := frsoBothKeepRatio
                             else result := frsoBoth
                     else result := frsoHorizontally
             else if (aY <= lRect.Top) OR (aY >= lRect.Bottom)
                     then result := frsoVertically;
          end;
  end;

procedure TcdResizeableObject.MouseLeave;
  begin
  inherited;
  Screen.Cursor := crDefault;
  end;

procedure TcdResizeableObject.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer);
  var
    lSizeOption: TfrSizeOption;
  begin
  if (aButton = mbLeft) and (aShiftState = [ssLeft]) and Selected
     then begin
          lSizeOption := InResizeArea(aX, aY);
          if lSizeOption <> frsoNone
             then begin
                  Diagram.Scrollbox.StartResizing(Types.Point(aX, aY), Self, lSizeOption);
                  Exit;
                  end;
          end;
  inherited MouseDown (aButton, aShiftState, aX, aY);
  end;

procedure TcdResizeableObject.MouseMove(aShiftState: TShiftState; aX, aY: integer);
  var
    lSizeOption: TfrSizeOption;
  begin
  if Selected
     then begin
          if ScrollBox.FocusRect.Activated
            then lSizeOption := Scrollbox.FocusRect.SizeOption
            else lSizeOption := InResizeArea(aX, aY);
          case lSizeOption of
             frsoHorizontally: ScrollBox.LastScreenCursor := crSizeWE;
             frsoVertically: ScrollBox.LastScreenCursor := crSizeNS;
             frsoBoth: ScrollBox.LastScreenCursor := crSizeAll;
             frsoBothKeepRatio: ScrollBox.LastScreenCursor := crSizeNWSE;
             else Screen.Cursor := crDefault;
             end;
          end;

  inherited;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdPlane
//
constructor TcdPlane.Create(aDiagram: TcdDiagram);
  begin
  inherited Create(aDiagram, ooUpperLeft);

  fIntWidth := 400;
  fIntHeight := 400;

  fFillColor := DefaultColor;

  fZOrder := 500;
  end;


procedure TcdPlane.LoadFromJson(aJsonObject: TJsonObject);
  var
    lRect: TRect;
    lInt: integer;
  begin
  inherited;

  if not aJsonObject.TryGetValue('left', lInt) then InvalidJson_xExpected('left');
  lRect.Left := lInt;
  if not aJsonObject.TryGetValue('top', lInt) then InvalidJson_xExpected('top');
  lRect.Top := lInt;
  if not aJsonObject.TryGetValue('width', lInt) then InvalidJson_xExpected('width');
  lRect.Width := lInt;
  if not aJsonObject.TryGetValue('height', lInt) then InvalidJson_xExpected('height');
  lRect.Height := lInt;

  if not aJsonObject.TryGetValue('printable', fPrintable) then InvalidJson_xExpected('printable');
  if not aJsonObject.TryGetValue('fillColor', fFillColor) then InvalidJson_xExpected('fillColor');

  SetIntBounds(lRect.Left, lRect.Top, lRect.Width, lRect.Height);
  end;

procedure TcdPlane.SaveToJson(aJsonObject: TJsonObject);
  begin
  inherited;

  aJsonObject.AddPair('left', TJsonIntegerNumber.Create(IntLeft));
  aJsonObject.AddPair('top', TJsonIntegerNumber.Create(IntTop));
  aJsonObject.AddPair('width', TJsonIntegerNumber.Create(IntWidth));
  aJsonObject.AddPair('height', TJsonIntegerNumber.Create(IntHeight));

  aJsonObject.AddPair('printable', TJsonBoolean.Create(Printable));
  aJsonObject.AddPair('fillColor', TJsonIntegerNumber.Create(fFillColor));
  end;


procedure TcdPlane.SetSelected(aValue: Boolean);
  begin
  inherited;
  if not Selected
     then Diagram.CorrectZOrder;
  end;

function TcdPlane.AcceptsTranslate(aDelta: TPoint): Boolean;
  var
    lNewRect: TRect;
    lObjects: TArray<TcdObject>;
    lObject: TcdObject;
  begin
  result := True;

  lNewRect := BoundsRect;
  OffsetRect(lNewRect, aDelta.X, aDelta.Y);

  lObjects := Diagram.GetObjectsOfType(TcdPlane);
  // Do we intersect with a plane? Do not allow
  for lObject in lObjects do
    if (lObject <> Self) and (IntersectRect(lObject.BoundsRect, lNewRect)
         // Unless we already were intersected (else we undo the situation)
        and not IntersectRect(lObject.BoundsRect, BoundsRect)) 
       then begin
            result := False;
            Break;
            end;
  end;

function TcdPlane.AcceptsResize(aNewRect: TRect): Boolean;
  var
    lObjects: TArray<TcdObject>;
    lObject: TcdObject;
  begin
  result := True;
  lObjects := Diagram.GetObjectsOfType(TcdPlane);
  // Do we intersect with a plane? Do not allow
  for lObject in lObjects do
    if (lObject <> Self) and (IntersectRect(lObject.BoundsRect, aNewRect)
      // Unless we already were intersected (else we undo the situation)
      and not IntersectRect(lObject.BoundsRect, BoundsRect)) 
       then begin
            result := False;
            Break;
            end;
  end;


procedure Objects_Moved(constref aObject: TcdObject; constref aArgs: array of const);
  begin
  aObject.Moved;
  end;

procedure Objects_Translate(constref aObject: TcdObject; constref aArgs: array of const);
  begin
  aObject.Translate(Point(aArgs[0].VInteger, aArgs[1].VInteger));
  end;

procedure TcdPlane.Translate(aDelta: TPoint);

  var
    lObjectsBefore,
    lObjectsAfter,
    lObjects: TList<TcdDragObject>;
    lObject: TcdDragObject;
  begin
  // First move objects or else the objects from the 'new' position will be moved
  // too
  lObjectsBefore := GetObjectsInPlane;

  inherited;

  // Make this 1 change (shortest route finding is a problem because not all
  // objects are moved at the same time)
  Diagram.BeginChange;
  try
    ProcessObjectList(lObjectsBefore, Objects_Translate, [aDelta.X, aDelta.Y]);
  finally
    Diagram.EndChange;
  end;

  lObjectsAfter := GetObjectsInPlane;
  lObjects := TList<TcdDragObject>.Create;

  // New objects have to be 'linked'. Force a 'moved'
  for lObject in lObjectsAfter do
    if not lObjectsBefore.Contains(lObject)
       then lObjects.Add(lObject);
  lObjectsAfter.Free;
  lObjectsBefore.Free;

  ProcessObjectList(lObjects, Objects_Moved, []);

  lObjects.Free;
  end;

procedure TcdPlane.UpdateColor(aColor: Graphics.TColor);
  begin
  fFillColor := aColor;
  DefaultColor := aColor;
  end;

procedure TcdPlane.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: integer);
  begin
  inherited MouseDown (aButton, aShiftState, aX, aY);
  end;

procedure TcdPlane.PopulatePopupMenu(aMenu: TPopupMenu);
  var
    lMenuItem: TMenuItem;
  begin
  lMenuItem := Diagram.CreateMenuItem(aMenu, '&Print', 0, Handle_Print);
  lMenuItem.Checked := Printable;
  Diagram.CreateMenuItem(aMenu, '&Select color', 0, Handle_SelectColor);
  Diagram.CreateMenuItem(aMenu, '&Remove', 0, Diagram.Handle_DefaultObject_Remove);
  end;

procedure TcdPlane.Handle_Print(aSender: TObject);
  begin
  // Swap current value
  fPrintable := not Printable;
  end;

procedure TcdPlane.Handle_SelectColor(aSender: TObject);
  var
    lColorDialog: TColorDialog;
  begin
  lColorDialog := TColorDialog.Create(Self);
  lColorDialog.Color := fFillColor;
  lColorDialog.CustomColors.Add('ColorA='+Format('%.6x', [TcdPlane.DefaultColor]));
  if lColorDialog.Execute
     then begin
          UpdateColor(lColorDialog.Color);
          Invalidate;
          end;
  lColorDialog.Free;
  end;

procedure TcdPlane.PaintOnCanvas(aCanvas: TCanvas);
  begin
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Brush_Color := fFillColor;
    Pen_Color := clBlack;

    if Selected
       then begin
            Brush_Style := bsClear;
            Pen_Style := psDashDotDot;
            Brush_Style := bsDiagCross;
            end
       else begin
            Brush_Style := bsSolid;

            Pen_Style := psSolid;
            end;

    Rectangle(IntClientRect);
    end;
  end;

procedure TcdPlane.ResetBounds_Internal;
  begin
  inherited;

  with Diagram.ScaleCanvas do
    begin
    Canvas := nil;

    SetIntBounds(IntLeft, IntTop, IntWidth, IntHeight);
    end;
  end;

function TcdPlane.GetObjectsInPlane(aExcludeObjects: TList<TcdObject> = nil): TList<TcdDragObject>;
  var
    lObjects: TArray<TcdObject>;
    lObject: TcdObject;
  begin
  result := TList<TcdDragObject>.Create;

  lObjects := Diagram.GetObjectsOfType(TcdDragObject);
  for lObject in lObjects do
     // not processed in a previous iteration?
     if not Assigned(aExcludeObjects) or (aExcludeObjects.IndexOf(lObject) = -1)
        then begin
             if not (lObject is TcdPlane) and (lObject.GetPlane = Self)
                then begin
                     Result.Add(lObject as TcdDragObject);
                     end;
             end;
  end;

procedure TcdPlane.ProcessObjectList(aList: TList<TcdDragObject>; const aFunc: TFuncProcessObjectList; const aFuncArgs: array of const);
  var
    lObject: TcdDragObject;
    lReprocess: Boolean;
    lProcessed: TList;
    I, lCount: integer;
    lComponents: TList<TcdDragObject>;
  begin
  lProcessed := TList.Create;
  repeat
    lComponents := TList<TcdDragObject>.Create;

    // Get every valid TcdObject
    for I := 0 to Diagram.ComponentCount - 1  do
      if Diagram.Components[I] is TcdDragObject
         then lComponents.Add(TcdDragObject(Diagram.Components[I]));

    lReprocess := False;
    for lObject in aList do
      // not processed in a previous iteration?
      if (lProcessed.IndexOf(lObject) = -1) and lComponents.Contains(lObject)
         then begin
              lProcessed.Add(lObject);

              lCount := Diagram.ComponentCount;
              aFunc(lObject, aFuncArgs);

              // if the CompontentCount has changed, objects are destroyed
              // and the currenct list is invalid. Reprocess
              if lCount <> Diagram.ComponentCount
                 then begin
                      lReprocess := True;
                      Break;
                      end;
              end;
    lComponents.Free;
    until not lReprocess;
  lProcessed.Free;
  end;

procedure TcdPlane.Resize(aRect: TRect);
  var
    lObjectsBefore, lObjectsAfter: TList<TcdDragObject>;
    lObjects: TList<TcdDragObject>;
    lObject: TcdDragObject;
  begin
  lObjectsBefore := GetObjectsInPlane;
  inherited;
  lObjectsAfter := GetObjectsInPlane;

  lObjects := TList<TcdDragObject>.Create;
   
  for lObject in lObjectsBefore do
    if not lObjectsAfter.Contains(lObject)
       then lObjects.Add(lObject);
  for lObject in lObjectsAfter do
    if not lObjectsBefore.Contains(lObject)
       then lObjects.Add(lObject);
  lObjectsBefore.Free;
  lObjectsAfter.Free;

  ProcessObjectList(lObjects, Objects_Moved, []);
  lObjects.Free;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdImage
//
procedure TcdImage.AssignImage(aSource: TBitmap);
  begin
  fImage.Picture.Assign(aSource);
  end;

function TcdImage.BringToFrontOnSelect: Boolean;
  begin
  result := False;
  end;

constructor TcdImage.Create(aDiagram: TcdDiagram);
  begin
  inherited Create(aDiagram, ooUpperLeft);
  fImage := TImage.Create(nil);
  end;

destructor TcdImage.Destroy;
  begin
  FreeAndNil(fImage);
  inherited;
  end;

procedure TcdImage.PopulatePopupMenu(aMenu: TPopupMenu);
  begin
  Diagram.CreateMenuItem(aMenu, '&Load file', 0, Handle_Load);
  Diagram.CreateMenuItem(aMenu, '&Reset size', 0, Handle_ResetSize);
  Diagram.CreateMenuItem(aMenu, '&Send to back', 0, Diagram.Handle_DefaultObject_SendToBack);
  Diagram.CreateMenuItem(aMenu, '&Remove', 0, Diagram.Handle_DefaultObject_Remove);
  end;

procedure TcdImage.Handle_Load(Sender: TObject);
  var
    lFileName: string = '';
  begin
  if PromptForOpenFileName('Load file', '*.*', '', 'All files (*.*)|*.*', lFileName)
     then LoadFromFile(lFileName);
  end;

procedure TcdImage.ResetSize;
  var
    lMaxRect: TRect;
    lPlane: TcdPlane;
    lFactor: Double;
  begin
  lPlane := GetPlane;
  if Assigned(lPlane)
     then lMaxRect := lPlane.ClientRect
     else lMaxRect := Diagram.fPageSize.PrintArea;

  fIntWidth := fImage.Picture.Width;
  fIntHeight := fImage.Picture.Height;

  if (fIntWidth > lMaxRect.Width) or  (fIntHeight > lMaxRect.Height) or (ScaleFactor <> 1)
     then begin
          lFactor := Min(lMaxRect.Width / fIntWidth, lMaxRect.Height / fIntHeight);

          fScaleFactor := Min(ScaleFactor, lFactor);
          fIntWidth := Round(fIntWidth * ScaleFactor);
          fIntHeight := Round(fIntHeight * ScaleFactor);
          end;

  fAspectRatio := fIntWidth / fIntHeight;
  ResetBounds;
  end;

procedure TcdImage.Handle_ResetSize(Sender: TObject);
  begin
  ResetSize;
  end;

procedure TcdImage.LoadFromFile(const aFileName: string);
  begin
  fImage.Picture.LoadFromFile(aFileName);
  fScaleFactor := 1;
  ResetSize;
  end;

procedure TcdImage.PaintOnCanvas(aCanvas: TCanvas);
  begin
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    StretchDraw(IntClientRect, fImage.Picture.Graphic);
    // Focus rect (for transparent images)
    if Selected
       then begin
            Brush_Style := bsClear;
            Pen_Style := psDashDotDot;
            Brush_Style := bsDiagCross;
            Rectangle(IntClientRect);
            end;

    end;
  end;

procedure TcdImage.ResetBounds_Internal;
  begin
  inherited;

  SetIntBounds(IntLeft, IntTop, IntWidth, IntHeight);
  end;


procedure TcdImage.LoadFromJson(aJsonObject: TJsonObject);
  var
    lRect: TRect;
    lInt: integer;
  begin
  inherited;

  if not aJsonObject.TryGetValue('left', lInt) then InvalidJson_xExpected('left');
  lRect.Left := lInt;
  if not aJsonObject.TryGetValue('top', lInt) then InvalidJson_xExpected('top');
  lRect.Top := lInt;
  if not aJsonObject.TryGetValue('width', lInt) then InvalidJson_xExpected('width');
  lRect.Width := lInt;
  if not aJsonObject.TryGetValue('height', lInt) then InvalidJson_xExpected('height');
  lRect.Height := lInt;

  SetIntBounds(lRect.Left, lRect.Top, lRect.Width, lRect.Height);

  if not aJsonObject.TryGetValue('scaleFactor', fScaleFactor) then InvalidJson_xExpected('scaleFactor');

  EInt('TcdImage.LoadFromJson', 'Loading images from Json not yet supported.');

  ResetSize;
  end;

procedure TcdImage.SaveToJson(aJsonObject: TJsonObject);
  begin
  inherited;

  aJsonObject.AddPair('left', TJsonIntegerNumber.Create(IntLeft));
  aJsonObject.AddPair('top', TJsonIntegerNumber.Create(IntTop));
  aJsonObject.AddPair('width', TJsonIntegerNumber.Create(IntWidth));
  aJsonObject.AddPair('height', TJsonIntegerNumber.Create(IntHeight));

  aJsonObject.AddPair('scaleFactor', TJsonFloatNumber.Create(ScaleFactor));

  EInt('TcdImage.SaveToJson', 'Saving images to Json not yet supported.');
  (*
  lStream := TMemoryStream.Create;
  fImage.Picture.SaveToStream(lStream);
  lSize := lStream.Size;
  aStream.write(lSize, SizeOf(lSize));
  aStream.CopyFrom(lStream, 0);
  lS1tream.Free;
  *)
  end;


procedure TcdImage.SetScaleFactor(const aValue: Double);
  begin
  fScaleFactor := aValue;
  ResetSize;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdGrid
//
constructor TcdGrid.Create(aDiagram: TcdDiagram);
  begin
  inherited Create(aDiagram, ooAuto);
  Clickable := True;//!!False;

  aDiagram.PrintOnUnprintableMargin := True;
  fSideMargin := 46;
  fTopMargin := 95;
  fLabelWidth := 991;
  fLabelHeight := 1390;
  fLabelMarginHor := 25;
  fLabelMarginVert := 0;

  fRects := TList<TRect>.Create;

  fZOrder := 750;
end;

destructor TcdGrid.Destroy;
  begin
  fRects.Free;
  inherited;
  end;


procedure TcdGrid.LoadFromJson(aJsonObject: TJsonObject);
  begin
  inherited;
  end;

procedure TcdGrid.SaveToJson(aJsonObject: TJsonObject);
  begin
  inherited;
  end;

function TcdGrid.BringToFrontOnSelect: Boolean;
  begin
  Result := False;
  end;

procedure TcdGrid.PaintOnCanvas(aCanvas: TCanvas);
  var
    lRect: TRect;
  begin
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    //Brush_Color :=  fFillColor;
    Pen_Color := clBlack;

    if not Diagram.PaintOnExternal then
      for lRect in Rects do
         Rectangle(lRect);

    end;
  end;

procedure TcdGrid.PopulatePopupMenu(aMenu: TPopupMenu);
  begin
  Diagram.CreateMenuItem(aMenu, '&Clear grid', 0, Handle_Clear);
  end;

procedure TcdGrid.Handle_Clear(Sender: TObject);

  procedure ClearRect(aRect: TRect);
    var
      lObject: TcdObject;
      lObjects: TArray<TcdObject>;
    begin
      Diagram.BeginChange;
      try
        lObjects := Diagram.GetObjectsOfType(TcdObject);
        for lObject in lObjects do
          if (lObject <> Self) and not lObject.IsEmbeddedObject and IntersectRect(Rect(lObject.IntLeft,
            lObject.IntTop, lObject.IntLeft + lObject.IntWidth, lObject.IntTop + lObject.IntHeight), aRect) 
			then lObject.Free;
      finally
        Diagram.EndChange;
      end;
    end;

  var
    lRect: TRect;
    lPoint: TPoint;
  begin
  lPoint := Diagram.GetMenuPopupPosition(Sender);
  lPoint := Types.Point(Diagram.eiXPos(lPoint.X), Diagram.eiYPos(lPoint.Y));
  for lRect in Rects do
    if PtInRect(lRect, lPoint) 
	  then begin
           ClearRect(lRect);
           end;
end;


procedure TcdGrid.ResetBounds_Internal;
  var
    lSideMargin, lTopMargin, lLabelWidth, lLabelHeight, lLabelMarginHor, lLabelMarginVert: Integer;
    lX, lY: Integer;
  begin
  inherited;
  SetIntBounds(0, 0, fDiagram.fPageSize.Width, fDiagram.fPageSize.Height);

  lTopMargin := Diagram.MmToInt(fTopMargin);
  lSideMargin := Diagram.MmToInt(fSideMargin);

  if fLabelWidth = -1 then
    lLabelWidth := Diagram.fPageSize.PrintArea.Width * 10
  else
    lLabelWidth := Diagram.MmToInt(fLabelWidth);
  if fLabelHeight = -1 then
    lLabelHeight := Diagram.fPageSize.PrintArea.Height * 10
  else
    lLabelHeight := Diagram.MmToInt(fLabelHeight);
  lLabelMarginHor := Diagram.MmToInt(fLabelMarginHor);
  lLabelMarginVert := Diagram.MmToInt(fLabelMarginVert);

  lX := lSideMargin;
  fRects.Clear;

  if (lLabelWidth > 0) and (lLabelHeight > 0)
    then begin
         while (lX + lLabelWidth) <= Diagram.fPageSize.Width * 10 do
           begin
           lY := lTopMargin;

           while (lY + lLabelHeight) <= Diagram.fPageSize.Height * 10 do
             begin
             fRects.Add(Classes.Rect(lX div 10, lY div 10, (lX + lLabelWidth) div 10, (lY + lLabelHeight) div 10));

             lY := lY + lLabelHeight + lLabelMarginVert;
             end;

           lX := lX + lLabelWidth + lLabelMarginHor;
           end;
		 end;
  end;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// TcdRectangle
//
constructor TcdRectangle.Create(aDiagram: TcdDiagram);
  begin
  inherited Create(aDiagram, ooUpperLeft);
  fPenColor := clBlack;
  end;

procedure TcdRectangle.PaintOnCanvas(aCanvas: TCanvas);
  var
    lRect: TRect;
    lDrawFrameRect: Boolean;
  begin
  with Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Pen_Color := PenColor;

    lDrawFrameRect := False;
    if Selected
       then begin
            Brush_Style := bsClear;
            Brush_Color := Diagram.Style.FillColor_Selected;
            end
       else begin
            Brush_Style := bsSolid;
            if FillColor = clNone 
			  then begin
                   lDrawFrameRect := True;
                   // FrameRect uses the Brush to draw the border
                   Brush_Color := PenColor;
                   end
              else Brush_Color := FillColor;
            end;

    lRect := IntClientRect;
    if lRect.Height = 1
       then begin
            MoveTo (lRect.Left, lRect.Top);
            LineTo (lRect.Right, lRect.Top);
            end
       else if lDrawFrameRect
               then FrameRect(IntClientRect)
               else Rectangle(IntClientRect);
    end;
  end;

procedure TcdRectangle.ResetBounds_Internal;
  begin
  inherited;
  SetIntBounds(IntLeft, IntTop, IntWidth, IntHeight);
  end;


procedure TcdRectangle.LoadFromJson(aJsonObject: TJsonObject);
  begin
  inherited;
  end;

procedure TcdRectangle.SaveToJson(aJsonObject: TJsonObject);
  begin
  inherited;
  end;


procedure TcdRectangle.SetFillColor(const Value: Graphics.TColor);
  begin
  fFillColor := Value;
  Invalidate;
  end;

procedure TcdRectangle.SetPenColor(const Value: Graphics.TColor);
  begin
  fPenColor := Value;
  Invalidate;
  end;

initialization
  DiagramStyles := TcdStyles.Create;
  DiagramStyles.Register_(TcdStyle.Create(cStyleNameDefault));

  TcdPlane.DefaultColor := $FCFCFF;

finalization
  FreeAndNil(DiagramStyles);
end.

