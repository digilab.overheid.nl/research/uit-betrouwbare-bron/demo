// -----------------------------------------------------------------------------
// diagram.Transaction
// -----------------------------------------------------------------------------
//
// Visualisation of Transactions. Used in ace.Forms.Transactions.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit diagram.Transaction;

{$MODE Delphi}

interface

uses
  Classes, Generics.Collections, Graphics, Controls,
  diagram.Core, diagram.Settings, ace.Core.Engine;

const
  cInfinite = '∞';

  cRadioEvent_Transaction_Selected = 'TransactionDiagram.Transaction.Selected'; // Object = Transaction.ID (as TObject)

type
  // Forward
  TcdTransaction = class;

  TcdTransactionDiagram =
    class(TcdObject)
      private
        fEngine: TcEngine;
        fDiagramSettings: TcDiagramSettings;

        fcdTransactions: TList<TcdTransaction>; // Does not own the cdTransactions
        fSelectedTransaction: TcTransaction;

        fMinSeqNr, fMaxSeqNr: integer;
        fMinTT, fMaxTT: TcTime;
        fxLabels: TStringlist;

        fxAxisDelta, fyAxisDelta,
        fAxisHeight, fAxisWidth,
        fAxisTop, fAxisRight
        : integer;

        // Data cached for fast repainting
        Engine_StableReadSnapshotTime: TcTime;

        function GetXByPos(aPos: integer): integer;
        function GetXByTime(aTT: integer): integer;
        // function GetXStartOfTime: integer;
        function GetXEndOfTime: integer;

        function GetYByPos(aPos: integer): integer;

        property cdTransactions: TList<TcdTransaction> read fcdTransactions;

        property MinTT: integer read fMinTT;
        property MaxTT: integer read fMaxTT;
        property xLabels: TStringlist read fxLabels;
        property xAxisDelta: integer read fxAxisDelta;
        property yAxisDelta: integer read fyAxisDelta;
        //property AxisHeight: integer read fAxisHeight;
        property AxisWidth: integer read fAxisWidth;
        property AxisTop: integer read fAxisTop;
        property AxisRight: integer read fAxisRight;

      protected
        procedure ResetBounds_Internal; override;
        procedure ResetBounds_Children; override;
        procedure CorrectZOrder; override;

      public
        constructor Create(aEngine: TcEngine; aDiagram: TcdDiagram); reintroduce;
        destructor Destroy; override;

        procedure InitChildren; override;
        procedure PaintOnCanvas(aCanvas: TCanvas); override;

        procedure SelectTransaction(aTransaction: TcTransaction); overload;
        procedure SelectTransaction(aTransactionID: integer); overload;

        property Engine: TcEngine read fEngine;
        property ds: TcDiagramSettings read fDiagramSettings;
        property SelectedTransaction: TcTransaction read fSelectedTransaction;
      end;

  TcdTransaction =
    class(TcdObject)
      private
        fTransactionDiagram: TcdTransactionDiagram;
        fEngine: TcEngine;
        fDiagramSettings: TcDiagramSettings;

        // Data cached from TcTransaction for fast repainting
        Transaction_ID: integer;
        Transaction_Text: string;
        Transaction_Selected: boolean;

        Color: TColor;

        property Engine: TcEngine read fEngine;

      protected
        function GetSeqNr(aTransaction: TcTransaction): Integer;
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aTransactionDiagram: TcdTransactionDiagram; aTransaction: TcTransaction); reintroduce;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;
        procedure MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer); override;

        property TransactionDiagram: TcdTransactionDiagram read fTransactionDiagram;
        property ds: TcDiagramSettings read fDiagramSettings;
      end;

implementation

uses
  SysUtils, Forms, Dialogs,
  common.RadioStation;

{ TcdTransactionDiagram }

constructor TcdTransactionDiagram.Create(aEngine: TcEngine; aDiagram: TcdDiagram);
  var
    i: integer;
    lTransaction: TcTransaction;
    lcdTransaction: TcdTransaction;
  begin
  inherited Create(aDiagram, ooUpperLeft);
  fEngine := aEngine;
  fDiagramSettings := gDiagramSettings;

  fcdTransactions := TList<TcdTransaction>.Create;
  for i := 0 to aEngine.Transactions.Count-1 do
    begin
    lTransaction := aEngine.Transactions[i];
    if not lTransaction.Implicit
       then begin
            lcdTransaction:= TcdTransaction.Create(Diagram, Self, lTransaction);
            cdTransactions.Add(lcdTransaction);
            end;
    end;

  fxLabels := TStringlist.Create;
  end;

destructor TcdTransactionDiagram.Destroy;
  begin
  fcdTransactions.Free;
  fxLabels.Free;
  inherited
  end;

procedure TcdTransactionDiagram.InitChildren;
  var
    i: integer;
  begin
  for i := 0 to cdTransactions.Count-1 do
    Diagram.Add(cdTransactions[i]);
  end;

procedure TcdTransactionDiagram.ResetBounds_Internal;
  var
    i: integer;
    lAxisTop, lAxisRight,
    lAxisMaxHeight, lAxisMaxWidth: integer;
    lTransaction: TcTransaction;
  begin
  SetIntBounds(IntLeft, IntTop, ScrollBox.Width, ScrollBox.Height);

  // Calculations
  fMinSeqNr := 1;
  fMaxSeqNr := cdTransactions.Count;
  if fMaxSeqNr<5 then fMaxSeqNr := 5;

  Engine_StableReadSnapshotTime := Engine.StableReadSnapshotTime;

  fMinTT := Engine.SystemTime;
  if Engine_StableReadSnapshotTime < fMinTT
     then fMinTT := Engine_StableReadSnapshotTime;

  for i := 0 to cdTransactions.Count-1 do
    begin
    lTransaction := Engine.Transactions.FindByID(cdTransactions[i].Transaction_ID);
    if not Assigned(lTransaction) then continue;
    if lTransaction.StartTime < fMinTT
       then fMinTT := lTransaction.StartTime;
//    if lTransaction.StableRead and (lTransaction.StableReadSnapshotTime < fMinTT)
//       then fMinTT := lTransaction.StableReadSnapshotTime;
    end;

  fMaxTT := Engine.MaxSystemTime+1; // Allways show the latest transactions with a length of 1
  if fMaxTT<(fMinTT+8) then fMaxTT := fMinTT+8;

  xLabels.Clear;
  for i := MinTT to MaxTT do xLabels.Add('T'+IntToStr(i));
  xLabels.Add(cInfinite);

  lAxisTop := ds.AxisTop;
  lAxisMaxHeight := (IntHeight  - lAxisTop - ds.AxisBottom);
  fyAxisDelta := lAxisMaxHeight div fMaxSeqNr;
  fAxisHeight := fyAxisDelta * fMaxSeqNr;
  fAxisTop := lAxisTop + lAxisMaxHeight - fAxisHeight;

  lAxisRight := ds.AxisRight;
  lAxisMaxWidth := (IntWidth - ds.AxisLeft - lAxisRight);
  fxAxisDelta := lAxisMaxWidth div (xLabels.Count-1);
  fAxisWidth :=  fxAxisDelta * (xLabels.Count-1);
  fAxisRight := lAxisRight + lAxisMaxWidth - fAxisWidth;
  end;

function TcdTransactionDiagram.GetXByPos(aPos: integer): integer;
  begin
  result := ds.AxisLeft + (aPos * xAxisDelta);
  end;

function TcdTransactionDiagram.GetXByTime(aTT: integer): integer;
  var
    lPos: integer;
  begin
  lPos := aTT - MinTT;
  result := ds.AxisLeft + (lPos * xAxisDelta);
  end;

//function TcdTransactionDiagram.GetXStartOfTime: integer;
//  begin
//  result := ds.AxisLeft;
//  end;

function TcdTransactionDiagram.GetXEndOfTime: integer;
  begin
  result := GetXByTime(MaxTT+1);
  end;

function TcdTransactionDiagram.GetYByPos(aPos: integer): integer;
  var
    lxMinSeqNr: integer;
  begin
  lxMinSeqNr := IntHeight - ds.AxisBottom;
  result := lxMinSeqNr - (aPos * yAxisDelta);
  end;

procedure TcdTransactionDiagram.ResetBounds_Children;
  var
    i: integer;
  begin
  inherited;
  for i := 0 to cdTransactions.Count-1 do
    cdTransactions[i].ResetBounds;
  end;

procedure TcdTransactionDiagram.CorrectZOrder;
  var
    i: integer;
  begin
  inherited;
  for i := 0 to cdTransactions.Count-1 do
    cdTransactions[i].BringToFront; // Should be on top of temporal grid
  end;

procedure TcdTransactionDiagram.PaintOnCanvas(aCanvas: TCanvas);
  var
    lStr: string;
    i, x, y: integer;
  begin
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    // Y-axis
    Brush_Style := bsClear;
    Pen_Style := psSolid;
    Pen_Color := clGray;
    Pen_Width := 2;
    MoveTo(ds.AxisLeft, AxisTop);
    LineTo(ds.AxisLeft, IntHeight - ds.AxisBottom);

    Font_Orientation := 900;
    Font_Height := ds.FontHeight_AxisLabels;
    Font_Style := [fsBold];
    Font_Name := ds.FontName_Axis;
    Font_Color := clGray;
    lStr := 'Explicit Transactions';
    TextOut(ds.AxisLabelMargin, AxisTop + TextWidth(lStr), lStr);

    //Font_Style := [];
    //lStr := 'second line';
    //Font_Height := ds.FontHeight_AxisLabels + 2;
    //TextOut(ds.AxisLabelMargin + 20, AxisTop + TextWidth(lStr), lStr);

    Brush_Style := bsClear;
    Pen_Width := 1;
    Font_Orientation := 0;
    Font_Height := ds.FontHeight_AxisTime;
    Font_Style := [];
    Font_Name := ds.FontName_Axis;
    Font_Color := clGray;
    for i := 0 to fMaxSeqNr-1 do
      begin
      x := ds.AxisLeft - ds.AxisTick;
      y := GetYByPos(i);
      MoveTo(x, y);
      x := ds.AxisLeft + AxisWidth;
      LineTo(x, y);
      end;

    // X-axis
    Brush_Style := bsClear;
    Pen_Style := psSolid;
    Pen_Color := clGray;
    Pen_Width := 2;
    MoveTo(ds.AxisLeft, IntHeight - ds.AxisBottom);
    LineTo(IntWidth - AxisRight, IntHeight - ds.AxisBottom);

    Font_Height := ds.FontHeight_AxisLabels;
    Font_Style := [fsBold];
    Font_Name := ds.FontName_Axis;
    Font_Color := clGray;
    lStr := 'System time';
    TextOut(IntWidth - TextWidth(lStr) - AxisRight, IntHeight - ds.AxisLabelMargin - TextHeight(lStr) + 4, lStr);

    Brush_Style := bsClear;
    Pen_Width := 1;
    Font_Orientation := 0;
    Font_Height := ds.FontHeight_AxisTime;
    Font_Style := [];
    Font_Name := ds.FontName_Axis;
    Font_Color := clGray;
    for i := 0 to xLabels.Count-1 do
      begin
      if (xLabels[i] = 'T'+IntToStr(Engine_StableReadSnapshotTime))
         then begin
              Pen_Color := clGreen;
              Pen_Width := 3;
              end
         else begin
              Pen_Color := clGray;
              Pen_Width := 1;
              end;

      x := GetXByPos(i);
      y := IntHeight - ds.AxisBottom + ds.AxisTick;
      MoveTo(x, y);
      y := AxisTop;
      LineTo(x, y);

      lStr := xLabels[i];
      x := x - TextWidth(lStr) div 2;
      y := IntHeight - ds.AxisBottom + ds.AxisTimeMarginY;

      if (xLabels[i] = 'T'+IntToStr(Engine_StableReadSnapshotTime))
         then begin
              Font_Height := ds.FontHeight_AxisTime;
              Font_Style := [fsBold];
              Font_Name := ds.FontName_Axis;
              Font_Color := clGreen;
              end
      else
         if (i = xLabels.Count-1)
         then begin
              y := y - 3;
              if i = 0 then x := x - 9 else x := x - 1;
              Font_Height := ds.FontHeight_InfinitySymbol;
              Font_Style := [];
              Font_Name := ds.FontName_InfinitySymbol;
              Font_Color := clGray;
              end
         else begin
              Font_Height := ds.FontHeight_AxisTime;
              Font_Style := [];
              Font_Name := ds.FontName_Axis;
              Font_Color := clGray;
              end;

      if (xLabels[i] = 'T'+IntToStr(Engine.SystemTime))
         then begin
              Brush_Color := clTimeColor;
              Font_Color := clBlack;
              end
         else Brush_Style := bsClear;

      TextOut(x, y, lStr);
      end;
    end;
  end;

procedure TcdTransactionDiagram.SelectTransaction(aTransaction: TcTransaction);
  begin
  fSelectedTransaction := aTransaction;
  Diagram.ForceRedraw;
  end;

procedure TcdTransactionDiagram.SelectTransaction(aTransactionID: integer);
  var
    lTransaction: TcTransaction;
  begin
  lTransaction := Engine.Transactions.FindByID(aTransactionID);
  SelectTransaction(lTransaction);
  end;


{ TcdTransaction }

constructor TcdTransaction.Create(aDiagram: TcdDiagram; aTransactionDiagram: TcdTransactionDiagram; aTransaction: TcTransaction);
  begin
  inherited Create(aDiagram, ooAuto);
  fSelectionGroup := 1;
  Clickable := True;

  fTransactionDiagram := aTransactionDiagram;
  fEngine := aTransactionDiagram.Engine;
  fDiagramSettings := aTransactionDiagram.ds;

  Transaction_ID := aTransaction.ID;
  end;

procedure TcdTransaction.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer);
  begin
  TransactionDiagram.SelectTransaction(Transaction_ID);
  if Assigned(gRadioStation)
     then gRadioStation.BroadCast(cRadioEvent_Transaction_Selected, TObject(Transaction_ID));
  end;

function TcdTransaction.GetSeqNr(aTransaction: TcTransaction): Integer;
  var
    i, s: integer;
    lTransaction: TcTransaction;
  begin
  result := -1; // Not found

  s := 0;
  for i := 0 to Engine.Transactions.Count-1 do
    begin
    lTransaction := Engine.Transactions[i];
    if not lTransaction.Implicit
       then inc(s);

    if Engine.Transactions[i] = aTransaction
       then begin
            result := s;
            exit;
            end;
    end;
  end;

procedure TcdTransaction.ResetBounds_Internal;
  var
    lLeft, lRight, lTop, lBottom, lWidth, lHeight: integer;
    lTransaction: TcTransaction;
    lSeqNr: integer;
  begin
  inherited;

  lTransaction := Engine.Transactions.FindByID(Transaction_ID);
  if not Assigned(lTransaction) then exit;

  Transaction_Text := Format('%s%d: %d - ', [
    cRefChar, lTransaction.ID, lTransaction.StartID
  ]);

  if lTransaction.EndID = cNoID
     then Transaction_Text := Transaction_Text + '?'
     else Transaction_Text := Transaction_Text + IntToStr(lTransaction.EndID);

  lSeqNr := GetSeqNr(lTransaction);

  lLeft := TransactionDiagram.GetXByTime(lTransaction.StartTime);

  if lTransaction.EndTime = cEndOfTime
    then lRight := TransactionDiagram.GetXEndofTime
    else lRight := TransactionDiagram.GetXByTime(lTransaction.EndTime);

  lBottom := TransactionDiagram.GetYByPos(lSeqNr-1);
  lTop := TransactionDiagram.GetYByPos(lSeqNr);

  lWidth := lRight - lLeft;
  lHeight := lBottom - lTop;

  SetIntBounds(lLeft, lTop, lWidth, lHeight);

  // Retrieve data required for painting
  Transaction_Selected := lTransaction = TransactionDiagram.SelectedTransaction;

  Color := lTransaction.Session.GetColor;
  end;

procedure TcdTransaction.PaintOnCanvas (aCanvas: TCanvas);
  var
    x, y: integer;
    lStr: String;
  begin
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Brush_Style := bsSolid;
    Pen_Style := psSolid;
    Pen_Width := 1;

    // https://docwiki.embarcadero.com/RADStudio/Alexandria/en/Colors_in_the_VCL
    if Transaction_Selected
       then begin
            Brush_Color :=  clWebLightYellow;
            Pen_Color := clWebOrange;
            end
       else begin
            Brush_Color := Color;
            Pen_Color := clGray;
            end;
(*
    else if Transaction_IsOperation
       then begin
            Brush_Color := clMoneyGreen;
            Pen_Color := clWebOliveDrab;
            end
    else begin
         Brush_Color := clWebLightSteelBlue;
         Pen_Color := clWebCornFlowerBlue;
         end;
*)

    Rectangle(IntClientRect);

    Brush_Style := bsClear;
    Font_Color := clBlack;
    Font_Height := ds.FontHeight_Tuple;
    Font_Style := [];
    Font_Name := ds.FontName_Tuple;
    lStr := Transaction_Text;
    x := (IntWidth div 2) - (TextWidth(lStr) div 2);
    y := (IntHeight div 2) - (TextHeight(lStr) div 2);
    TextOut(x, y, lStr);
    end;

  end;

end.
