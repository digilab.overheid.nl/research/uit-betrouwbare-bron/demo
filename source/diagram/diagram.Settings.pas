// -----------------------------------------------------------------------------
//
// diagram.Settings
//
// -----------------------------------------------------------------------------
//
// Settings used to draw graphs with claims, operations etc.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit diagram.Settings;

{$MODE Delphi}

interface

uses
  Classes, Graphics;

const
  clWebLightYellow = $E0FFFF;
  clWebOrange = $00A5FF;
  clWebDarkGray = $A9A9A9;
  clWebGainsboro = $DCDCDC;
  clWebLightSteelBlue = $DEC4B0;
  clWebDarkOrange = $008CFF;
  clWebPaleTurquoise = $EEEEAF;
  clWebWhiteSmoke = $F5F5F5;
  clWebIvory = $F0FFFF;
  clWebLavender = $FAE6E6;
  clWebPaleGreen = $98FB98;
  clWebDarkgreen = $006400;
  clWebMidnightBlue = $701919;
  clWebLemonChiffon = $CDFAFF;
  clWebLightCyan = $FFFFE0;
  clWebPink = $CBC0FF;

type
  TcDiagramSettings =
    class
      private
        fAxisLeft: integer;
        fAxisRight: integer;
        fAxisTop: integer;
        fAxisBottom: integer;
        fAxisTick: integer;
        fAxisLabelMargin: integer;
        fAxisTimeMarginX: integer;
        fAxisTimeMarginY: integer;
        fAxisClickMargin: integer;
        fAxisClickHorizontalOffset: integer;
        fAxisClickVerticalOffset: integer;
        fFontName_Axis: string;
        fFontSize_AxisLabels: integer;
        fFontSize_AxisTime: integer;
        fFontName_InfinitySymbol: string;
        fFontSize_InfinitySymbol: integer;
        fFontName_Tuple: string;
        fFontSize_Tuple: integer;
        fFontName_Annotations: string;
        fFontSize_Annotations: integer;
        fFontName_Info: string;
        fFontSize_Info: integer;

        function GetFontHeight(aFontSize: integer): integer;

        function GetFontHeight_AxisLabels: integer;
        function GetFontHeight_AxisTime: integer;
        function GetFontHeight_InfinitySymbol: integer;
        function GetFontHeight_Tuple: integer;
        function GetFontHeight_Annotations: integer;
        function GetFontHeight_Info: integer;

        procedure Init;

      public
        constructor Create;
        destructor Destroy; override;

        function ColorToLightVariant(aColor: TColor; aLuminance: Integer): TColor;

        property AxisLeft: integer read fAxisLeft;
        property AxisRight: integer read fAxisRight;
        property AxisTop: integer read fAxisTop;
        property AxisBottom: integer read fAxisBottom;
        property AxisTick: integer read fAxisTick;
        property AxisLabelMargin: integer read fAxisLabelMargin;
        property AxisTimeMarginX: integer read fAxisTimeMarginX;
        property AxisTimeMarginY: integer read fAxisTimeMarginY;
        property AxisClickMargin: integer read fAxisClickMargin;
        property AxisClickHorizontalOffset: integer read fAxisClickHorizontalOffset;
        property AxisClickVerticalOffset: integer read fAxisClickVerticalOffset;
        property FontName_Axis: string read fFontName_Axis;
        property FontHeight_AxisLabels: integer read GetFontHeight_AxisLabels;
        property FontHeight_AxisTime: integer read GetFontHeight_AxisTime;
        property FontName_InfinitySymbol: string read fFontName_InfinitySymbol;
        property FontHeight_InfinitySymbol: integer read GetFontHeight_InfinitySymbol;
        property FontName_Tuple: string read fFontName_Tuple;
        property FontHeight_Tuple: integer read GetFontHeight_Tuple;
        property FontName_Annotations: string read fFontName_Annotations;
        property FontHeight_Annotations: integer read GetFontHeight_Annotations;
        property FontName_Info: string read fFontName_Info;
        property FontHeight_Info: integer read GetFontHeight_Info;
      end;

var
  // Created by the ace.Settings
  gDiagramSettings: TcDiagramSettings;

implementation

uses
  SysUtils, Forms, lcltype, GraphUtil,
  ace.Settings;

{ TcDiagramSettings }

constructor TcDiagramSettings.Create;
  begin
  inherited Create;
  Init;
  end;

destructor TcDiagramSettings.Destroy;
  begin
  inherited;
  end;

function TcDiagramSettings.GetFontHeight(aFontSize: integer): integer;
  begin
  result := -MulDiv(aFontSize, Screen.PixelsPerInch, 96);
  if gSettings.FontSize_ScaleFactor<>100
     then result := MulDiv(result, gSettings.FontSize_ScaleFactor, 100);
  end;

function TcDiagramSettings.GetFontHeight_AxisLabels: integer;
  begin
  result := GetFontHeight(fFontSize_AxisLabels);
  end;

function TcDiagramSettings.GetFontHeight_AxisTime: integer;
  begin
  result := GetFontHeight(fFontSize_AxisTime);
  end;

function TcDiagramSettings.GetFontHeight_InfinitySymbol: integer;
  begin
  result := GetFontHeight(fFontSize_InfinitySymbol);
  end;

function TcDiagramSettings.GetFontHeight_Tuple: integer;
  begin
  result := GetFontHeight(fFontSize_Tuple);
  end;

function TcDiagramSettings.GetFontHeight_Annotations: integer;
  begin
  result := GetFontHeight(fFontSize_Annotations);
  end;

function TcDiagramSettings.GetFontHeight_Info: integer;
  begin
  result := GetFontHeight(fFontSize_Info);
  end;

procedure TcDiagramSettings.Init;
  begin
  fAxisLeft := 80;
  fAxisRight := 10;
  fAxisTop := 15;
  fAxisBottom := 60;
  fAxisTick := 5;
  fAxisLabelMargin := 10;
  fAxisTimeMarginX := 40;
  fAxisTimeMarginY := 10;
  fAxisClickMargin := 16;
  fAxisClickHorizontalOffset := 22;
  fAxisClickVerticalOffset := 34;
  fFontName_Axis := 'Calibri';
  fFontSize_AxisLabels := 13;
  fFontSize_AxisTime := 13;
  fFontName_InfinitySymbol := 'Consolas';
  fFontSize_InfinitySymbol := 16;
  fFontName_Tuple := 'Calibri';
  fFontSize_Tuple := 13;
  fFontName_Annotations := 'Calibri';
  fFontSize_Annotations := 12;
  fFontName_Info := 'Calibri';
  fFontSize_Info := 13;
  end;

function TcDiagramSettings.ColorToLightVariant(aColor: TColor; aLuminance: Integer): TColor;
  var
    H: Word=0;
    S: Word=0;
    L: Word=0;
  begin
  ColorRGBToHLS(AColor, H, L, S);
  result := ColorHLSToRGB(H, aLuminance, S);
  end;

end.
