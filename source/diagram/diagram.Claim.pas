// -----------------------------------------------------------------------------
//
// diagram.Claim
//
// -----------------------------------------------------------------------------
//
// Visualisation of a Claim. Used in diagram.SourceClaims and diagram.Temporal.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic B.V.
//
// -----------------------------------------------------------------------------

unit diagram.Claim;

interface

uses
  System.UITypes, System.Classes, System.Types, System.Generics.Collections,
  Vcl.Graphics,
  diagram.Core, diagram.Settings, diagram.ClaimContainer, diagram.Selection,
  ace.Core.Engine;

type
  TcdClaim =
    class(TcdObject)
      private
        fClaimContainer: TcdClaimContainer;
        fEngine: TcEngine;
        fDiagramSettings: TcDiagramSettings;

        // Data cached from TcClaim for fast repainting
        Claim_ID: integer;
        Claim_TimeLineInterpretation: boolean;
        Claim_Original: boolean;
        Claim_RegisteredAt: integer;
        Claim_ExpiredAt: integer;
        Claim_ValidFrom: integer;
        Claim_ValidUntil: integer;
        Claim_Text: string;
        Claim_InTransaction: boolean;
        Claim_InResult: boolean;
        Claim_Selected: boolean;
        Claim_Related: boolean;

        StartedAnnotation_ID: integer;
        StartedAnnotation_Selected: boolean;
        StartedAnnotation_Related: boolean;
        EndedAnnotation_ID: integer;
        EndedAnnotation_Selected: boolean;
        EndedAnnotation_Related: boolean;
        ExpiredAnnotation_ID: integer;
        ExpiredAnnotation_Selected: boolean;
        ExpiredAnnotation_Related: boolean;
        RegularAnnotations: boolean;
        RegularAnnotations_Related: boolean;
        InDoubt: boolean;
        InDoubt_Related: boolean;

        property Engine: TcEngine read fEngine;

      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aClaimContainer: TcdClaimContainer; aClaim: TcClaim); reintroduce;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;
        procedure MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer); override;

        property ClaimContainer: TcdClaimContainer read fClaimContainer;
        property ds: TcDiagramSettings read fDiagramSettings;
      end;

implementation

{ TcdClaim }

constructor TcdClaim.Create(aDiagram: TcdDiagram; aClaimContainer: TcdClaimContainer; aClaim: TcClaim);
  begin
  inherited Create(aDiagram, ooAuto);
  fSelectionGroup := 1;
  Clickable := True;

  fClaimContainer := aClaimContainer;
  fEngine := aClaim.Engine;
  fDiagramSettings := aClaimContainer.ds;

  Claim_ID := aClaim.ID;
  end;

procedure TcdClaim.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer);
  begin
  TemporalDiagram.SelectClaim(Claim_ID);
  if Assigned(gRadioStation)
     then gRadioStation.BroadCast(cRadioEvent_TemporalDiagram_Claim_Selected, TemporalDiagram);
  end;

procedure TcdClaim.ResetBounds_Internal;
  var
    a: integer;
    lLeft, lRight, lTop, lBottom, lWidth, lHeight: integer;
    lSelection: TcSelection;
    lClaim: TcClaim;
    lAnnotation: TcAnnotation;
    lAnnotationType: TcAnnotationType;
  begin
  inherited;

  lClaim := Engine.GetClaim(Claim_ID);
  if not Assigned(lClaim) then exit;

  lSelection := TemporalDiagram.Selection;

  if lClaim.RegisteredAt = cStartOfTime
    then lBottom := TemporalDiagram.GetYStartOfTime
    else lBottom := TemporalDiagram.GetYByTime(lClaim.RegisteredAt);

  if lClaim.ExpiredAt = cEndOfTime
    then lTop := TemporalDiagram.GetYEndOfTime+1
    else lTop := TemporalDiagram.GetYByTime(lClaim.ExpiredAt)+1;

  if lClaim.ValidFrom = cStartOfTime
    then lLeft := TemporalDiagram.GetXStartOfTime+1 // Assume T1
    else lLeft := TemporalDiagram.GetXByTIme(lClaim.ValidFrom)+1;

  if lClaim.ValidUntil = cEndOfTime
    then lRight := TemporalDiagram.GetXEndofTime
    else lRight := TemporalDiagram.GetXByTime(lClaim.ValidUntil);

  lWidth := lRight - lLeft;
  lHeight := lBottom - lTop;

  SetIntBounds(
    TemporalDiagram.Left + lLeft,
    TemporalDiagram.Top + lTop,
    lWidth, lHeight);

  // Retrieve data required for painting
  Claim_TimeLineInterpretation := lClaim.ClaimType.TimelineInterpretation;
  Claim_Original := not lClaim.IsCopy;
  Claim_RegisteredAt := lClaim.RegisteredAt;
  Claim_ExpiredAt := lClaim.ExpiredAt;
  Claim_ValidFrom := lClaim.ValidFrom;
  Claim_ValidUntil := lClaim.ValidUntil;
  Claim_Text := lClaim.GetTupleStrWithinSet;

  Claim_InTransaction :=
    Assigned(TemporalDiagram.Query)
      and
    TemporalDiagram.Query.InTransaction(lClaim);

  Claim_InResult :=
    Assigned(TemporalDiagram.Query)
      and
    TemporalDiagram.Query.InResult(lClaim);

  Claim_Selected := lSelection.DerivedClaimSelected(lClaim);
  Claim_Related := lSelection.IsRelated(lClaim.SourceClaim);

  StartedAnnotation_ID := cNoID;
  StartedAnnotation_Selected := false;
  StartedAnnotation_Related := false;
  EndedAnnotation_ID := cNoID;
  EndedAnnotation_Selected := false;
  EndedAnnotation_Related := false;
  ExpiredAnnotation_ID := cNoID;
  ExpiredAnnotation_Selected := false;
  ExpiredAnnotation_Related := false;
  RegularAnnotations := false;
  RegularAnnotations_Related := false;
  InDoubt := false;
  InDoubt_Related := false;

  for a := 0 to lClaim.Annotations.Count-1 do
    begin
    lAnnotation := lClaim.Annotations[a];
    lAnnotationType := lAnnotation.AnnotationType;
    if lAnnotationType.Kind = akStarted
       then begin
            StartedAnnotation_ID := lAnnotation.ID;
            StartedAnnotation_Selected := lSelection.DerivedClaimSelected(lAnnotation);
            StartedAnnotation_Related := lSelection.IsRelated(lAnnotation.CreatedBy);
            end
    else if lAnnotationType.Kind = akEnded
       then begin
            EndedAnnotation_ID := lAnnotation.ID;
            EndedAnnotation_Selected := lSelection.DerivedClaimSelected(lAnnotation);
            EndedAnnotation_Related := lSelection.IsRelated(lAnnotation.CreatedBy);
            end
    else if lAnnotationType.Kind = akExpired
       then begin
            ExpiredAnnotation_ID := lAnnotation.ID;
            ExpiredAnnotation_Selected := lSelection.DerivedClaimSelected(lAnnotation);
            ExpiredAnnotation_Related := lSelection.IsRelated(lAnnotation.CreatedBy);
            end
    else if lAnnotationType.Kind = akDoubt
       then begin
            InDoubt := true;
            InDoubt_Related := lSelection.IsRelated(lAnnotation.CreatedBy);
            end
    else begin
         RegularAnnotations := true;
         if not RegularAnnotations_Related
            then RegularAnnotations_Related := lSelection.IsRelated(lAnnotation.CreatedBy);
         end;
    end;
  end;

function ColorToLightVariant(aColor: TColor; aLuminance: Integer): TColor;
  var
    H, S, L: Word;
  begin
  ColorRGBToHLS(AColor, H, L, S);
  result := ColorHLSToRGB(H, aLuminance, S);
  end;

procedure TcdClaim.PaintOnCanvas (aCanvas: TCanvas);
  var
    x, y: integer;
    lStr, lStr1, lStr2: String;
  begin
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    if Claim_TimeLineInterpretation
      then Brush_Style := bsSolid
      else Brush_Style := bsBDiagonal; // Make overlap visible
    Pen_Style := psSolid;
    Pen_Width := 1;

    // https://docwiki.embarcadero.com/RADStudio/Alexandria/en/Colors_in_the_VCL
    if Claim_Selected or (TemporalDiagram.Query.Active and Claim_InResult)
       then begin
            Brush_Color :=  clWebLightYellow;
            Pen_Color := clWebOrange;
            end
    else if Claim_Related
       then begin
            Brush_Color :=  clWebIvory;
            Pen_Color := clWebDarkGray;
            end
    else if Claim_ExpiredAt <> cEndOfTime
       then begin
            Brush_Color := ColorToLightVariant(clWebGainsboro, 216);
            Pen_Color := clWebDarkGray;
            end
    else if Claim_ValidUntil <> cEndOfTime
       then begin
            Brush_Color := ColorToLightVariant(clWebLightSteelBlue, 210);
            Pen_Color := clWebDarkGray; //clWebCornFlowerBlue;
            end
    else begin
         Brush_Color := ColorToLightVariant(clMoneyGreen, 210);
         Pen_Color := clWebDarkGray; //clWebOliveDrab;
         end;

    Rectangle(IntClientRect);

    Brush_Style := bsClear;
    Font_Color := clBlack;
    Font_Height := ds.FontHeight_Tuple;
    Font_Style := [];
    Font_Name := ds.FontName_Tuple;
    lStr1 := Claim_Text;
    lStr2 := '  (' + cRefChar+IntToStr(Claim_ID) + ')';
    lStr := lStr1 + lStr2;
    x := (IntWidth div 2) - (TextWidth(lStr) div 2);
    y := (IntHeight div 2) - (TextHeight(lStr) div 2);

    Font_Color := clBlack;
    TextOut(x, y, lStr1);
    x := x + TextWidth(lStr1);
    Font_Color := clGray;
    TextOut(x, y, lStr2);

    //if Claim_Original
    //  then begin
    //       Font_Color := clGray;
    //       Font_Height := ds.FontHeight_Annotations;
    //       Font_Style := [];
    //       Font_Name := ds.FontName_Annotations;
    //       TextOut(6, 3, 'o');
    //       end;

    if InDoubt or RegularAnnotations
      then begin
           lStr := '@';

           if RegularAnnotations_Related or InDoubt_Related
              then begin
                   Brush_Color :=  clWebIvory;
                   Pen_Color := clWebDarkGray;
                   end;

           if InDoubt
              then begin
                   Font_Color := clRed;
                   Font_Style := [];
                   x := width - 20;
                   end
              else begin
                   Font_Color := clGray;
                   Font_Style := [];
                   x := width - 16;
                   end;
           Font_Height := ds.FontHeight_Annotations;
           Font_Name := ds.FontName_Annotations;
           TextOut(x, 3, lStr);
           end;
    end;
  end;

end.
