// -----------------------------------------------------------------------------
//
// diagram.ClaimSets
//
// -----------------------------------------------------------------------------
//
// Visualisation of ClaimSets in engine. Used in ace.Forms.Transactions.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit diagram.ClaimSets;

{$MODE Delphi}

interface

uses
  Classes, Types, Graphics,
  common.SimpleLists, common.RadioStation,
  diagram.Core, diagram.Settings, ace.Core.Engine;

const
  ccsTop: integer = 10;
  ccsLeft: integer = 10;
  ccsHMarginClaimTypes: integer = 8;
  ccsHMarginInner = 8;
  ccsHMarginOuter = 8;
  ccsHMarginOuterClaimSet = 4;
  ccsVMarginInner = 2;
  ccsVMarginOuter = 4;
  ccsFontName_ClaimType: string = 'Calibri';

type
  // Simple caching classes for fast redrawing
  TcdClaim =
    class
      private
        fSelected: Boolean;
        fColor: TColor;
        fText: string;
        fLeft, fTop, fWidth, fHeight: integer;
        function GetRight: integer;
        function GetBottom: integer;

      public
        function AsString: string;

        property Selected: boolean read fSelected write fSelected;
        property Color: TColor read fColor write fColor;
        property Text: string read fText write fText;
        property Left: integer read fLeft write fLeft;
        property Top: integer read fTop write fTop;
        property Width: integer read fWidth write fWidth;
        property Height: integer read fHeight write fHeight;
        property Right: integer read GetRight;
        property Bottom: integer read GetBottom;
      end;

  TcdClaimSet =
    class
      private
        fcdClaims: TcStringObjectList<TcdClaim>;
        fLeft, fTop, fWidth, fHeight: integer;
        function GetRight: integer;
        function GetBottom: integer;

      public
        constructor Create;
        destructor Destroy; override;

        property cdClaims: TcStringObjectList<TcdClaim> read fcdClaims;

        property Left: integer read fLeft write fLeft;
        property Top: integer read fTop write fTop;
        property Width: integer read fWidth write fWidth;
        property Height: integer read fHeight write fHeight;
        property Right: integer read GetRight;
        property Bottom: integer read GetBottom;
      end;

  TcdClaimType =
    class
      private
        fcdClaimSets: TcStringObjectList<TcdClaimSet>;
        fLeft, fTop, fWidth, fHeight: integer;
        function GetRight: integer;
        function GetBottom: integer;

      public
        constructor Create;
        destructor Destroy; override;

        property cdClaimSets: TcStringObjectList<TcdClaimSet> read fcdClaimSets;

        property Left: integer read fLeft write fLeft;
        property Top: integer read fTop write fTop;
        property Width: integer read fWidth write fWidth;
        property Height: integer read fHeight write fHeight;
        property Right: integer read GetRight;
        property Bottom: integer read GetBottom;
      end;

  // Diagram
  TcdClaimSetsDiagram =
    class(TcdObject)
      private
        fEngine: TcEngine;
        fRadioOnline: boolean;
        fDiagramSettings: TcDiagramSettings;
        fcdClaimTypes: TcStringObjectList<TcdClaimType>;
        fSelectedTransactionID: integer;

      protected
        procedure ListenToRadio(aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);

        procedure ResetBounds_Internal; override;

      public
        constructor Create(aEngine: TcEngine; aDiagram: TcdDiagram); reintroduce;
        destructor Destroy; override;

        procedure PaintOnCanvas(aCanvas: TCanvas); override;

        property Engine: TcEngine read fEngine;
        property ds: TcDiagramSettings read fDiagramSettings;
        property cdClaimTypes: TcStringObjectList<TcdClaimType> read fcdClaimTypes;
      end;

implementation

uses
  SysUtils, Forms, Dialogs,
  common.ErrorHandling,
  ace.Core.Query,

  diagram.Transaction,
  ace.Settings;


{ TcdClaimType }

constructor TcdClaimType.Create;
  begin
  inherited;
  fcdClaimSets := TcStringObjectList<TcdClaimSet>.Create(true {owner}, true {sorted});
  end;

destructor TcdClaimType.Destroy;
  begin
  fcdClaimSets.Free;
  inherited;
  end;

function TcdClaimType.GetRight: integer;
  begin
  result := Left + Width;
  end;

function TcdClaimType.GetBottom: integer;
  begin
  result := Top + Height;
  end;


{ TcdClaimSet }

constructor TcdClaimSet.Create;
  begin
  inherited;
  fcdClaims := TcStringObjectList<TcdClaim>.Create(true {owner}, false {not sorted});
  end;

destructor TcdClaimSet.Destroy;
  begin
  fcdClaims.Free;
  inherited;
  end;

function TcdClaimSet.GetRight: integer;
  begin
  result := Left + Width;
  end;

function TcdClaimSet.GetBottom: integer;
  begin
  result := Top + Height;
  end;


{ TcdClaim }

function TcdClaim.GetRight: integer;
  begin
  result := Left + Width;
  end;

function TcdClaim.GetBottom: integer;
  begin
  result := Top + Height;
  end;

function TcdClaim.AsString: string;
  begin
  result := fText;
  end;


{ TcdClaimSetsDiagram }

constructor TcdClaimSetsDiagram.Create(aEngine: TcEngine; aDiagram: TcdDiagram);
  begin
  inherited Create(aDiagram, ooUpperLeft);
  fEngine := aEngine;
  fDiagramSettings := gDiagramSettings;
  fcdClaimTypes := nil;
  fSelectedTransactionID := cNoID;

  // RadioStation
  gRadioStation.AddRadio(Self, ListenToRadio);
  fRadioOnline := true;
  end;

destructor TcdClaimSetsDiagram.Destroy;
  begin
  if fRadioOnline
     then gRadioStation.RemoveRadio(Self);

  if Assigned(fcdClaimTypes)
     then fcdClaimTypes.Free;

  inherited
  end;

procedure TcdClaimSetsDiagram.ListenToRadio(aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);
  begin
  if aRadioEvent = cRadioEvent_Offline
    then begin
         fRadioOnline := false;
         exit;
         end;

  if (aRadioEvent = cRadioEvent_Transaction_Selected)
     then begin
          fSelectedTransactionID := {%H-}Integer(aData); // Ignore warning: Conversion between ordinals and pointers is not portable
          ResetBounds_Internal;
          end;
  end;

procedure TcdClaimSetsDiagram.ResetBounds_Internal;
  var
    t,cs,c: integer;
    lType: TcType;
    lClaimType: TcClaimType;
    lcdClaimType: TcdClaimType;
    lClaimSet: TcClaimSet;
    lcdClaimSet: TcdClaimSet;
    lClaim: TcClaim;
    lcdClaim: TcdClaim;
    lStr, lDebugStr: string;
    lLeft, lWidth, lTop: integer;
    lMaxWidth: integer;
    lDefaultHeight: integer;
    lTransaction: TcTransaction;
    lQuery: TcTemporalQuery;
  begin
  fcdClaimTypes.Free;
  fcdClaimTypes := TcStringObjectList<TcdClaimType>.Create(true {owned}, true {sorted});

  lMaxWidth := 0;
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := nil;

    lLeft := ccsLeft;
    lDefaultHeight := TextHeight(cHeightDeterminationStr) + (ccsVMarginInner * 2);

    // Collect all ClaimTypes
    for t := 0 to Engine.ConceptualTypes.Count-1 do
      begin
      lType := Engine.ConceptualTypes[t];
      if not (lType is TcClaimType)
         then continue;
      lClaimType := lType as TcClaimType;
      lStr := lClaimType.Name;
      if (Length(lStr) > 0) and (lStr[1] = cMetaChar)
         then continue;

      lTop := ccsTop;

      lcdClaimType := TcdClaimType.Create;
      lcdClaimType.Left := lLeft;
      lcdClaimType.Width := TextWidth(lStr);
      lcdClaimType.Top := lTop;
      cdClaimTypes.Add(lStr, lcdClaimType);

      lTop := lTop + ccsVMarginInner + lDefaultHeight;

      // Collect ClaimSets within the ClaimTypes
      for cs := 0 to lClaimType.ClaimSetList.Count-1 do
        begin
        lClaimSet := lClaimType.ClaimSetList[cs];

        lStr := cRefChar + IntToStr(lClaimSet.ID) + ': ' + lClaimSet.IdentityStr;
        lWidth := TextWidth(lStr) + (ccsHMarginInner * 2);
        if lWidth > lcdClaimType.Width
           then lcdClaimType.Width := lWidth;

        lcdClaimSet := TcdClaimSet.Create;
        lcdClaimSet.Left := lLeft;
        lcdClaimSet.Width := lcdClaimType.Width;
        lcdClaimSet.Top := lTop;
        lcdClaimType.cdClaimSets.Add(lStr, lcdClaimSet);

        lTop := lTop + ccsVMarginInner + lDefaultHeight + ccsVMarginInner;

        // Execute query when neccessary
        lQuery := nil;
        lTransaction := nil;
        if fSelectedTransactionID<>cNoID
           then begin
                lTransaction := Engine.Transactions.FindByID(fSelectedTransactionID);
                if not Assigned(lTransaction)
                   then EInt('TcdClaimSetsDiagram.ResetBounds_Internal', 'Transaction not assigned');

                if lTransaction.State = tsActive
                   then begin
                        lQuery := TcTemporalQuery.Create(lTransaction.Session);
                        lQuery.AssignClaimSet(lClaimSet);
                        lQuery.Execute;
                        end;
                end;

        // Collect Claims within the ClaimSets
        for c := 0 to lClaimSet.DerivedClaims.Count-1 do
          begin
          lcdClaim := TcdClaim.Create;

          lClaim := lClaimSet.DerivedClaims[c];

          lcdClaim.Selected := Assigned(lQuery) and lQuery.InTransaction(lClaim);
          if lcdClaim.Selected and Assigned(lTransaction)
             then lcdClaim.Color := lTransaction.Session.GetColor;

          lcdClaim.Text := lClaim.GetValueStrWithoutIdentity;

          lDebugStr := Format(' CR:%s%d SC:%s%d V:%s%d', [
             cRefChar, lClaim.TransactionID,
             cRefChar, lClaim.SourceClaim,
             cRefChar, lClaim.MVCCVersionID
          ]);
          if lClaim.MVCCMarkedForCleaningBy <> cNoID
             then lDebugStr := lDebugStr + Format(' MC:%s%d', [
             cRefChar, lClaim.MVCCMarkedForCleaningBy
          ]);

          lStr := Format('%s%d: %s [%s%s]', [
            cRefChar, lClaim.ID, lcdClaim.AsString,
            lClaim.GetTimeAspectsCompact, lDebugStr
          ]);

          lWidth := TextWidth(lStr) + (ccsHMarginInner * 2) + (ccsHMarginOuterClaimSet * 2);
          if lWidth > lcdClaimType.Width
             then begin
                  lcdClaimType.Width := lWidth;
                  lcdClaimSet.Width := lWidth;
                  end;

          lcdClaim.Left := lLeft +   ccsHMarginOuterClaimSet;
          lcdClaim.Width := lcdClaimType.Width - (ccsHMarginOuterClaimSet * 2);
          lcdClaim.Top := lTop;
          lcdClaim.Height := ccsVMarginInner + lDefaultHeight;
          lcdClaimSet.cdClaims.Add(lStr, lcdClaim);

          lTop := lTop + lcdClaim.Height + ccsVMarginOuter;
          end;

        // Calc height of ClaimSet
        lcdClaimSet.Height := lTop - lcdClaimSet.Top;

        // Calc top for next ClaimSet
        lTop := lTop + ccsVMarginOuter;

        // Close Query
        if Assigned(lQuery)
           then lQuery.Free;

        end;

      // Calc height of ClaimType
      lcdClaimType.Height := lTop;

      // Calc left for next ClaimType
      lLeft := lLeft + lcdClaimType.Width + ccsHMarginClaimTypes;

      if lLeft + lcdClaimType.Width > lMaxWidth
         then lMaxWidth := lLeft + lcdClaimType.Width;
      end;
    end;

  if ScrollBox.Width > lMaxWidth
     then lMaxWidth := ScrollBox.Width;

  SetIntBounds(IntLeft, IntTop, lMaxWidth, ScrollBox.Height);
  end;

procedure TcdClaimSetsDiagram.PaintOnCanvas(aCanvas: TCanvas);
  var
    t, cs, c: integer;
    x, y: integer;
    lStr: string;
    lcdClaimType: TcdClaimType;
    lcdClaimSet: TcdClaimSet;
    lcdClaim: TcdClaim;
  begin
  if not Assigned(cdClaimTypes) then exit;

  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    // Display all ClaimTypes
    for t := 0 to cdClaimTypes.Count-1 do
      begin
      lStr := cdClaimTypes[t];
      lcdClaimType := cdClaimTypes.Objects[t];

      Brush_Style := bsClear;
      Font_Height := gSettings.FontHeight_TransactionsClaimSets;
      Font_Style := [fsBold];
      Font_Name := ccsFontName_ClaimType;
      Font_Color := clBlack;
      x := lcdClaimType.Left + ((lcdClaimType.Width - TextWidth(lStr)) div 2);
      y := lcdClaimType.Top + ccsVMarginInner;
      TextOut(x, y, lStr);

      // Display all ClaimSets
      for cs := 0 to lcdClaimType.cdClaimSets.Count-1 do
        begin
        lStr := lcdClaimType.cdClaimSets[cs];
        lcdClaimSet := lcdClaimType.cdClaimSets.Objects[cs];

        Brush_Style := bsSolid;
        Brush_Color := clWebGainsboro;
        Pen_Style := psSolid;
        Pen_Color := clGray;
        Pen_Width := 1;
        Rectangle(TRect.Create(lcdClaimSet.Left, lcdClaimSet.Top, lcdClaimSet.Right, lcdClaimSet.Bottom));

        x := lcdClaimSet.Left + ((lcdClaimSet.Width - TextWidth(lStr)) div 2);
        y := lcdClaimSet.Top + ccsVMarginInner;
        TextOut(x, y, lStr);

        // Display all Claims
        for c := 0 to lcdClaimSet.cdClaims.Count-1 do
          begin
          lStr := lcdClaimSet.cdClaims[c];
          lcdClaim := lcdClaimSet.cdClaims.Objects[c];

          Brush_Style := bsSolid;
          if lcdClaim.Selected
             then Brush_Color := lcdClaim.Color
             else Brush_Color := clWebWhiteSmoke;
          Pen_Style := psSolid;
          Pen_Color := clGray;
          Pen_Width := 1;
          Rectangle(TRect.Create(lcdClaim.Left, lcdClaim.Top, lcdClaim.Right, lcdClaim.Bottom));

          x := lcdClaim.Left + ((lcdClaim.Width - TextWidth(lStr)) div 2);
          y := lcdClaim.Top + ccsVMarginInner;
          TextOut(x, y, lStr);
          end;
        end;
      end;
    end;
  end;

end.
