// -----------------------------------------------------------------------------
//
// diagram.SourceClaims
//
// -----------------------------------------------------------------------------
//
// Visualisation of a stack of Source Claims. Used in ace.Forms.ClaimSet.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit diagram.SourceClaims;

{$MODE Delphi}

interface

uses
  Classes, Types, Generics.Collections, Graphics, Controls,
  diagram.Core, diagram.Settings, diagram.Selection, ace.Core.Engine;

const
  cRadioEvent_SourceClaimStack_SourceClaim_Selected = 'SourceClaimStack.SourceClaim.Selected'; // Object = SourceClaimStack

const
  cAxisLeft: integer = 40;
  cAxisBottom: integer = 20;

  cMinItems: integer = 8;
  cMaxItems: integer = 12;
  cRowHeight: integer = 32;

  cHorizontalCorrection: integer = 32;

  cHorzImplicitAnnotationWidth: integer = 32;
  cHorzImplicitAnnotationHeight: integer = 12;
  cVertImplicitAnnotationWidth: integer = 12;
  cVertImplicitAnnotationHeight: integer = 32;

  cAnnotationMargin: integer = 8;
  cAnnotationSide: integer = 12;

type
  // Forward
  TcdClaim = class;

  TcdSourceClaimStack =
    class(TcdObject)
      private
        fEngine: TcEngine;
        fSession: TcSession;
        fDiagramSettings: TcDiagramSettings;

        fClaimSet: TcClaimSet;
        fSelection: TcSelection;
        fShowTemporalAnnotations: boolean;

        fcdClaims: TList<TcdClaim>; // Does not own the claims

        fMinY, fMaxY: integer;

        fyAxisDelta,
        fAxisWidth,
        fAxisTop
        : integer;

        fConnectionPoint: TPoint;

        function GetYByPos(aPos: integer): integer;

        property cdClaims: TList<TcdClaim> read fcdClaims;

        property yAxisDelta: integer read fyAxisDelta;
        property AxisWidth: integer read fAxisWidth;
        property AxisTop: integer read fAxisTop;

      protected
        procedure ResetBounds_Internal; override;
        procedure ResetBounds_Children; override;
        procedure CorrectZOrder; override;

      public
        constructor Create(aDiagram: TcdDiagram; aSession: TcSession; aClaimSet: TcClaimSet; aSelection: TcSelection); reintroduce;
        destructor Destroy; override;

        procedure InitChildren; override;
        procedure PaintOnCanvas(aCanvas: TCanvas); override;

        procedure SelectClaim(aClaim: TcClaim); overload;
        procedure SelectClaim(aClaimID: integer); overload;
        procedure UnselectClaim;

        property Engine: TcEngine read fEngine;
        property Session: TcSession read fSession;
        property ds: TcDiagramSettings read fDiagramSettings;

        property ClaimSet: TcClaimSet read fClaimSet;
        property Selection: TcSelection read fSelection;
        property ShowTemporalAnnotations: boolean read fShowTemporalAnnotations write fShowTemporalAnnotations;
        property ConnectionPoint: TPoint read fConnectionPoint write fConnectionPoint;
      end;

  TcdClaim =
    class(TcdObject)
      private
        fClaimStack: TcdSourceClaimStack;
        fEngine: TcEngine;
        fDiagramSettings: TcDiagramSettings;

        // Data cached from TcClaim for fast repainting
        Claim_ID: integer;
        Claim_SeqNr: integer;
        Claim_Text: string;
        Claim_Selected: boolean;
        Claim_Related: boolean;

        StartedAnnotation_ID: integer;
        StartedAnnotation_Selected: boolean;
        StartedAnnotation_Related: boolean;
        EndedAnnotation_ID: integer;
        EndedAnnotation_Selected: boolean;
        EndedAnnotation_Related: boolean;
        ExpiredAnnotation_ID: integer;
        ExpiredAnnotation_Selected: boolean;
        ExpiredAnnotation_Related: boolean;
        RegularAnnotations: boolean;
        RegularAnnotations_Related: boolean;
        InDoubt: boolean;
        InDoubt_Related: boolean;

        property Engine: TcEngine read fEngine;

      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aClaimStack: TcdSourceClaimStack; aSeqNr: integer; aClaim: TcClaim); reintroduce;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;
        procedure MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer); override;

        property ClaimStack: TcdSourceClaimStack read fClaimStack;
        property ds: TcDiagramSettings read fDiagramSettings;
      end;

implementation

uses
  SysUtils,
  common.RadioStation;

{ TcdSourceClaimStack }

constructor TcdSourceClaimStack.Create(aDiagram: TcdDiagram; aSession: TcSession; aClaimSet: TcClaimSet; aSelection: TcSelection);
  var
    i: Integer;
    lClaim: TcClaim;
    lcdClaim: TcdClaim;
  begin
  inherited Create(aDiagram, ooUpperLeft);
  fEngine := aSession.Engine;
  fSession := aSession;
  fDiagramSettings := gDiagramSettings;
  fSelection := aSelection;

  fcdClaims := TList<TcdClaim>.Create;
  fClaimSet := aClaimSet;
  for i := 0 to ClaimSet.SourceClaims.Count-1 do
    begin
    lClaim := ClaimSet.SourceClaims[i];
    lcdClaim := TcdClaim.Create(Diagram, Self, i, lClaim);
    cdClaims.Add(lcdClaim);
    end;
  end;

destructor TcdSourceClaimStack.Destroy;
  begin
  fcdClaims.Free;
  inherited
  end;

procedure TcdSourceClaimStack.InitChildren;
  var
    i: integer;
  begin
  for i := 0 to cdClaims.Count-1 do
    Diagram.Add(cdClaims[i]);
  end;

procedure TcdSourceClaimStack.ResetBounds_Internal;
  var
    lAxisTop, lAxisHeight, lAxisMaxHeight: integer;
    i, lHeight, lWidth, lMaxWidth: integer;
    lcdClaim: TcdClaim;
  begin
  // Calculations
  fMinY := 1;
  fMaxY := cdClaims.Count;
  if fMaxY < cMinItems then fMaxY := cMinItems;
  if fMaxY > cMaxItems then fMaxY := cMaxItems;
  lHeight := cAxisBottom + (fMaxY * cRowHeight);

  with ScrollBox.Diagram.ScaleCanvas do
    begin
    lMaxWidth := 0;

    Font_Height := ds.FontHeight_Tuple;
    Font_Style := [];
    Font_Name := ds.FontName_Tuple;

    for i := 0 to cdClaims.Count-1 do
      begin
      lcdClaim := cdClaims[i];
      lWidth := TextWidth(Format('%s (%s%d)', [lcdClaim.Claim_Text, cRefChar, lcdClaim.Claim_ID]));
      if lWidth > lMaxWidth then lMaxWidth := lWidth;
      end;
    end;
  lWidth := lMaxWidth + cAxisLeft + cHorizontalCorrection + 32 {room for annotations};

  SetIntBounds(IntLeft, IntTop, lWidth, lHeight);

  // Boundaries
  lAxisTop := ds.AxisTop;
  lAxisMaxHeight := (IntHeight - lAxisTop - cAxisBottom);
  fyAxisDelta := lAxisMaxHeight DIV fMaxY;
  lAxisHeight := fyAxisDelta * fMaxY;
  fAxisTop := lAxisTop + lAxisMaxHeight - lAxisHeight;
  fAxisWidth := (IntWidth - cAxisLeft - ds.AxisRight);

  ConnectionPoint := Point(-1,-1);
  end;

function TcdSourceClaimStack.GetYByPos(aPos: integer): integer;
  var
    lxMinTT: integer;
  begin
  lxMinTT := IntHeight - cAxisBottom;
  result := lxMinTT - (aPos * yAxisDelta);
  end;

procedure TcdSourceClaimStack.ResetBounds_Children;
  var
    i: integer;
  begin
  inherited;
  for i := 0 to cdClaims.Count-1 do
    cdClaims[i].ResetBounds;
  end;

procedure TcdSourceClaimStack.CorrectZOrder;
  var
    i: integer;
  begin
  inherited;
  for i := 0 to cdClaims.Count-1 do
    cdClaims[i].BringToFront; // Should be on top of temporal grid
  end;

procedure TcdSourceClaimStack.PaintOnCanvas(aCanvas: TCanvas);
  var
    lStr: string;
    i, x, y: integer;
  begin
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    // Background
    Brush_Style := bsSolid;
    Brush_Color := clWhite;
    Pen_Style := psClear;
    Rectangle(cAxisLeft, AxisTop, IntWidth - ds.AxisRight, IntHeight - cAxisBottom);

    // Axis
    Brush_Style := bsClear;
    Pen_Style := psSolid;
    Pen_Color := clGray;
    Font_Color := clGray;

    // Y-axis Left and Right
    Pen_Width := 2;
    MoveTo(cAxisLeft, AxisTop);
    LineTo(cAxisLeft, IntHeight - cAxisBottom);
    MoveTo(IntWidth - ds.AxisRight, AxisTop);
    LineTo(IntWidth - ds.AxisRight, IntHeight - cAxisBottom);

    Font_Orientation := 900;
    Font_Height := ds.FontHeight_AxisLabels;
    Font_Style := [fsBold];
    Font_Name := ds.FontName_Axis;
    lStr := 'Source Claims & Annotations';
    TextOut(ds.AxisLabelMargin, AxisTop + TextWidth(lStr), lStr);

    // Horizontal Lines
    Pen_Width := 1;
    Font_Orientation := 0;
    Font_Height := ds.FontHeight_AxisTime;
    Font_Style := [];
    Font_Name := ds.FontName_Axis;
    for i := 0 to fMaxY do
      begin
      x := cAxisLeft;
      y := GetYByPos(i);
      MoveTo(x, y);
      x := cAxisLeft + AxisWidth;
      LineTo(x, y);
      end;

    // X-axis
    Pen_Width := 2;
    MoveTo(cAxisLeft, IntHeight - cAxisBottom);
    LineTo(IntWidth - ds.AxisRight, IntHeight - cAxisBottom);
    end;
  end;

procedure TcdSourceClaimStack.SelectClaim(aClaim: TcClaim);
  begin
  Selection.SelectSourceClaim(aClaim);
  Diagram.ForceRedraw;
  end;

procedure TcdSourceClaimStack.SelectClaim(aClaimID: integer);
  begin
  SelectClaim(Engine.FindClaim(aClaimID));
  end;

procedure TcdSourceClaimStack.UnselectClaim;
  begin
  SelectClaim(nil);
  end;


{ TcdClaim }

constructor TcdClaim.Create(aDiagram: TcdDiagram; aClaimStack: TcdSourceClaimStack; aSeqNr: integer; aClaim: TcClaim);
  begin
  inherited Create(aDiagram, ooAuto);
  fSelectionGroup := 1;
  Clickable := True;

  fClaimStack := aClaimStack;
  fEngine := aClaim.Engine;
  fDiagramSettings := aClaimStack.ds;

  Claim_ID := aClaim.ID;
  Claim_SeqNr := aSeqNr;
  end;

procedure TcdClaim.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer);
  begin
  ClaimStack.SelectClaim(Claim_ID);
  if Assigned(gRadioStation)
     then gRadioStation.BroadCast(cRadioEvent_SourceClaimStack_SourceClaim_Selected, ClaimStack);
  end;

procedure TcdClaim.ResetBounds_Internal;
  var
    a: integer;
    lLeft, lRight, lTop, lBottom, lWidth, lHeight: integer;
    lSelection: TcSelection;
    lClaim: TcClaim;
    lAnnotation: TcAnnotation;
    lAnnotationType: TcAnnotationType;
  begin
  inherited;

  lClaim := Engine.FindClaim(Claim_ID);
  if not Assigned(lClaim) then exit;

  lSelection := ClaimStack.Selection;

  lBottom := ClaimStack.GetYByPos(Claim_SeqNr);
  lTop := ClaimStack.GetYByPos(Claim_SeqNr+1);
  lLeft := cAxisLeft;
  lRight := ClaimStack.IntWidth - ds.AxisRight;

  lWidth := lRight - lLeft;
  lHeight := lBottom - lTop;

  SetIntBounds(ClaimStack.Left + lLeft, ClaimStack.Top + lTop, lWidth, lHeight);

  // Retrieve data required for painting
  if lClaim.ClaimType.Roles.Count = lClaim.ClaimType.Identity.Count
     then Claim_Text := lClaim.ValueStr
     else Claim_Text := lClaim.GetValueStrWithoutIdentity;

  Claim_Selected := lSelection.SourceClaimSelected(lClaim);
  Claim_Related := lSelection.IsRelated(lClaim.ID);

  StartedAnnotation_ID := cNoID;
  StartedAnnotation_Selected := false;
  StartedAnnotation_Related := false;
  EndedAnnotation_ID := cNoID;
  EndedAnnotation_Selected := false;
  EndedAnnotation_Related := false;
  ExpiredAnnotation_ID := cNoID;
  ExpiredAnnotation_Selected := false;
  ExpiredAnnotation_Related := false;
  RegularAnnotations := false;
  RegularAnnotations_Related := false;
  InDoubt := false;
  InDoubt_Related := false;

  for a := 0 to lClaim.Annotations.Count-1 do
    begin
    lAnnotation := lClaim.Annotations[a];
    lAnnotationType := lAnnotation.AnnotationType;
    if lAnnotationType.Kind = akStarted
       then begin
            StartedAnnotation_ID := lAnnotation.ID;
            StartedAnnotation_Selected := lSelection.DerivedClaimSelected(lAnnotation);
            StartedAnnotation_Related := lSelection.IsRelated(lAnnotation.DerivedFrom);
            end
    else if lAnnotationType.Kind = akEnded
       then begin
            EndedAnnotation_ID := lAnnotation.ID;
            EndedAnnotation_Selected := lSelection.DerivedClaimSelected(lAnnotation);
            EndedAnnotation_Related := lSelection.IsRelated(lAnnotation.DerivedFrom);
            end
    else if lAnnotationType.Kind = akExpired
       then begin
            ExpiredAnnotation_ID := lAnnotation.ID;
            ExpiredAnnotation_Selected := lSelection.DerivedClaimSelected(lAnnotation);
            ExpiredAnnotation_Related := lSelection.IsRelated(lAnnotation.DerivedFrom);
            end
    else if lAnnotationType.Kind = akDoubt
       then begin
            InDoubt := true;
            InDoubt_Related := lSelection.IsRelated(lAnnotation.ID);
            end
    else begin
         RegularAnnotations := true;
         if not RegularAnnotations_Related
            then RegularAnnotations_Related := lSelection.IsRelated(lAnnotation.ID);
         end;
    end;

  if Claim_Selected
     then ClaimStack.ConnectionPoint := Point(IntLeft + 20, IntTop + (IntHeight div 2));
  end;

procedure TcdClaim.PaintOnCanvas (aCanvas: TCanvas);
  var
    x, y: integer;
    lStr, lStr1, lStr2: String;
  begin
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    // -- Main Rectangle

    Brush_Style := bsSolid;
    Pen_Style := psSolid;
    Pen_Width := 1;

    // https://docwiki.embarcadero.com/RADStudio/Alexandria/en/Colors_in_the_VCL
    if Claim_Selected 
       then begin
            Brush_Color :=  clWebLightYellow;
            Pen_Color := clWebOrange;
            end
    else if Claim_Related
       then begin
            Brush_Color :=  clWebLightYellow; //clWebIvory;
            Pen_Color := clWebDarkGray;
            end
       else begin
            Brush_Color := clWebLavender;
            Pen_Color := clWebDarkGray;
            end;

    Rectangle(IntClientRect);


    // -- Text

    Brush_Style := bsClear;
    Font_Color := clBlack;
    Font_Height := ds.FontHeight_Tuple;
    Font_Style := [];
    Font_Name := ds.FontName_Tuple;
    lStr1 := Claim_Text;
    lStr2 := '  (' + cRefChar+IntToStr(Claim_ID) + ')';
    lStr := lStr1 + lStr2;
    x := (IntWidth div 2) - (TextWidth(lStr) div 2);
    y := (IntHeight div 2) - (TextHeight(lStr) div 2);

    Font_Color := clBlack;
    TextOut(x, y, lStr1);
    x := x + TextWidth(lStr1);
    Font_Color := clGray;
    TextOut(x, y, lStr2);


    // -- Annotations

    if ClaimStack.ShowTemporalAnnotations
       then begin
            if StartedAnnotation_ID <> cNoID
               then begin
                    if StartedAnnotation_Related
                       then begin
                            Brush_Color :=  clWebLightYellow; //clWebIvory;
                            Pen_Color := clWebDarkGray;
                            end
                       else begin
                            Brush_Color := clWebLavender;
                            Pen_Color := clWebDarkGray;
                            end;

                    x := 0;
                    y := (IntHeight - cVertImplicitAnnotationHeight) div 2;
                    Rectangle(TRect.Create(x, y, x + cVertImplicitAnnotationWidth, y + cVertImplicitAnnotationHeight));
                    end;

            if EndedAnnotation_ID <> cNoID
               then begin
                    if EndedAnnotation_Related
                       then begin
                            Brush_Color :=  clWebLightYellow; //clWebIvory;
                            Pen_Color := clWebDarkGray;
                            end
                       else begin
                            Brush_Color := clWebLavender;
                            Pen_Color := clWebDarkGray;
                            end;

                    x := IntWidth - cVertImplicitAnnotationWidth;
                    y := (IntHeight - cVertImplicitAnnotationHeight) div 2;
                    Rectangle(TRect.Create(x, y, x + cVertImplicitAnnotationWidth, y + cVertImplicitAnnotationHeight));
                    end;

            if ExpiredAnnotation_ID <> cNoID
               then begin
                    if ExpiredAnnotation_Related
                       then begin
                            Brush_Color :=  clWebLightYellow; //clWebIvory;
                            Pen_Color := clWebDarkGray;
                            end
                       else begin
                            Brush_Color := clWebLavender;
                            Pen_Color := clWebDarkGray;
                            end;

                    x := (IntWidth - cHorzImplicitAnnotationWidth) div 2;
                    y := 0;
                    Rectangle(TRect.Create(x, y, x + cHorzImplicitAnnotationWidth, y + cHorzImplicitAnnotationHeight));
                    end;
            end;

    if InDoubt or RegularAnnotations
      then begin
           Pen_Color := clWebDarkGray;
           if RegularAnnotations_Related or InDoubt_Related
              then Brush_Color := clWebLightYellow //clWebIvory
           else if InDoubt
              then Brush_Color := clWebDarkOrange
              else Brush_Color := clWebPaleTurquoise;
           Rectangle(TRect.Create(
             IntWidth -cAnnotationSide -cAnnotationMargin, cAnnotationMargin,
             IntWidth -cAnnotationMargin, cAnnotationMargin + cAnnotationSide
           ));
           end;
    end;
  end;

end.
