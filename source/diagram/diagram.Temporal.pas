// -----------------------------------------------------------------------------
//
// diagram.Temporal
//
// -----------------------------------------------------------------------------
//
// Visualisation of Temporal ClaimSet diagram. Used in ace.Forms.ClaimSet.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit diagram.Temporal;

{$MODE Delphi}

interface

uses
  Classes, Types, Generics.Collections, Graphics, Controls,
  common.SimpleLists,
  diagram.Core, diagram.Settings, diagram.Selection, ace.Core.Engine, ace.Core.Query;

const
  cWidth = 600;
  cHeight = 400;

  cScrollBarWidth = 20;

  cInfinite = '∞';

  cHorzImplicitAnnotationWidth: integer = 32;
  cHorzImplicitAnnotationHeight: integer = 12;
  cVertImplicitAnnotationWidth: integer = 12;
  cVertImplicitAnnotationHeight: integer = 32;

  cAnnotationMargin: integer = 8;
  cAnnotationSide: integer = 14;

  cRadioEvent_TemporalChart_Claim_Selected = 'TemporalChart.Claim.Selected'; // Object = TemporalChart
  cRadioEvent_TemporalChart_QueryCriteria_Changed = 'TemporalChart.QueryCriteria.Changed'; // Object = TemporalChart

type
  // Forward
  TcdClaim = class;
  TcdQuery = class;

  TcdTemporalChart =
    class(TcdObject)
      private
        fEngine: TcEngine;
        fSession: TcSession;
        fDiagramSettings: TcDiagramSettings;

        fSelection: TcSelection;
        fShowTemporalAnnotations: boolean;
        fQuery: TcTemporalQuery;

        fcdClaims: TList<TcdClaim>; // Does not own the claims
        fQueryTT: TcdQuery;
        fQueryVT: TcdQuery;

        fMinTT, fMaxTT, fMinVT, fMaxVT: TcTime;
        fxLabels, fyLabels: TStringlist;

        fxAxisDelta, fyAxisDelta,
        fAxisHeight, fAxisWidth,
        fAxisTop, fAxisRight
        : integer;

        fConnectionPoint: TPoint;

        function GetXByPos(aPos: integer): integer;
        function GetXByTime(aVT: integer): integer;
        function GetXStartOfTime: integer;
        function GetXEndOfTime: integer;

        function GetYByPos(aPos: integer): integer;
        //function GetYByTime(aTT: integer): integer;
        function GetYStartOfTime: integer;
        function GetYEndOfTime: integer;

        procedure ExecuteQuery;

        property cdClaims: TList<TcdClaim> read fcdClaims;

        //property MinTT: integer read fMinTT;
        //property MaxTT: integer read fMaxTT;
        property MinVT: integer read fMinVT;
        property MaxVT: integer read fMaxVT;
        property xLabels: TStringlist read fxLabels;
        property yLabels: TStringlist read fyLabels;
        property xAxisDelta: integer read fxAxisDelta;
        property yAxisDelta: integer read fyAxisDelta;
        property AxisHeight: integer read fAxisHeight;
        property AxisWidth: integer read fAxisWidth;
        property AxisTop: integer read fAxisTop;
        property AxisRight: integer read fAxisRight;

      protected
        procedure ResetBounds_Internal; override;
        procedure ResetBounds_Children; override;
        procedure CorrectZOrder; override;

      public
        constructor Create(aDiagram: TcdDiagram; aSession: TcSession; aSelection: TcSelection); reintroduce;
        procedure Init(aClaimSet: TcClaimSet; aQuery: TcTemporalQuery); overload;
        procedure Init(aAnnotation: TcAnnotation); overload;
        destructor Destroy; override;

        procedure InitChildren; override;
        procedure PaintOnCanvas(aCanvas: TCanvas); override;
        procedure MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer); override;

        procedure ClearQuery;

        procedure SelectClaim(aClaim: TcClaim); overload;
        procedure SelectClaim(aClaimID: integer); overload;
        procedure UnselectClaim;

        property Engine: TcEngine read fEngine;
        property Session: TcSession read fSession;
        property ds: TcDiagramSettings read fDiagramSettings;

        property Selection: TcSelection read fSelection;
        property ShowTemporalAnnotations: boolean read fShowTemporalAnnotations write fShowTemporalAnnotations;
        property ConnectionPoint: TPoint read fConnectionPoint write fConnectionPoint;

        property Query: TcTemporalQuery read fQuery;
        property QueryTT: TcdQuery read fQueryTT;
        property QueryVT: TcdQuery read fQueryVT;
      end;

  TcdClaim =
    class(TcdObject)
      private
        fTemporalChart: TcdTemporalChart;
        fEngine: TcEngine;
        fDiagramSettings: TcDiagramSettings;

        // Data cached from TcClaim for fast repainting
        Claim_ID: integer;
        Claim_yLayer: integer;
        Claim_TimeLineInterpretation: boolean;
        Claim_Original: boolean;
        Claim_RegisteredAt: integer;
        Claim_ExpiredAt: integer;
        Claim_ValidFrom: integer;
        Claim_ValidUntil: integer;
        Claim_Text: string;
        Claim_InTransaction: boolean;
        Claim_InResult: boolean;
        Claim_Selected: boolean;
        Claim_Related: boolean;

        StartedAnnotation_ID: integer;
        StartedAnnotation_Selected: boolean;
        StartedAnnotation_Related: boolean;
        EndedAnnotation_ID: integer;
        EndedAnnotation_Selected: boolean;
        EndedAnnotation_Related: boolean;
        ExpiredAnnotation_ID: integer;
        ExpiredAnnotation_Selected: boolean;
        ExpiredAnnotation_Related: boolean;
        RegularAnnotations: boolean;
        RegularAnnotations_Related: boolean;
        InDoubt: boolean;
        InDoubt_Related: boolean;

        property Engine: TcEngine read fEngine;

      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aTemporalChart: TcdTemporalChart; aClaim: TcClaim); reintroduce;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;
        procedure MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer); override;

        property TemporalChart: TcdTemporalChart read fTemporalChart;
        property ds: TcDiagramSettings read fDiagramSettings;
      end;

  TcdQuery =
    class(TcdObject)
      private
        fMinPos, fMaxPos: integer;
        fTemporalChart: TcdTemporalChart;
        fDiagramSettings: TcDiagramSettings;

        procedure SwapPos;
        function GetTimeByPos(aPos: integer): TcTime;
        function GetPosByTime(aTime: TcTime): integer;
        function GetMin: TcTime;
        function GetMax: TcTime;
        procedure SetMin(aTime: TcTime);
        procedure SetMax(aTime: TcTime);

      public
        constructor Create(aDiagram: TcdDiagram; aTemporalChart: TcdTemporalChart); reintroduce;
        procedure PaintOnCanvas (aCanvas: TCanvas); override;

        procedure Clear;
        procedure SetPos(aPos: integer);
        procedure SetMinPos(aPos: integer);
        procedure SetMaxPos(aPos: integer);

        property TemporalChart: TcdTemporalChart read fTemporalChart;
        property ds: TcDiagramSettings read fDiagramSettings;
        property MinPos: integer read fMinPos write SetMinPos;
        property MaxPos: integer read fMaxPos write SetMaxPos;
        property Min: TcTime read GetMin write SetMin;
        property Max: TcTime read GetMax write SetMax;
     end;

  TcdQueryTT =
    class(TcdQuery)
      protected
        procedure ResetBounds_Internal; override;
     end;

  TcdQueryVT =
    class(TcdQuery)
      protected
        procedure ResetBounds_Internal; override;
     end;

implementation

uses
  SysUtils, Forms, Dialogs,
  common.RadioStation, common.ErrorHandling;

{ TcdTemporalChart }

constructor TcdTemporalChart.Create(aDiagram: TcdDiagram; aSession: TcSession; aSelection: TcSelection);
  begin
  inherited Create(aDiagram, ooUpperLeft);
  fEngine := aSession.Engine;
  fSession := aSession;
  fDiagramSettings := gDiagramSettings;

  fSelection := aSelection;
  fQuery := nil;

  fcdClaims := TList<TcdClaim>.Create;
  fxLabels := TStringlist.Create;
  fyLabels := TStringlist.Create;
  end;

procedure TcdTemporalChart.Init(aClaimSet: TcClaimSet; aQuery: TcTemporalQuery);
  var
    i, lIndex: Integer;
    lClaim: TcClaim;
    lcdClaim: TcdClaim;
    lTransactionTimes, lValidTimes: TcIntegerList;
  begin
  // Init Query
  fQuery := aQuery;
  fQuery.Execute;
  fQueryTT := TcdQueryTT.Create(Diagram, Self);
  fQueryVT := TcdQueryVT.Create(Diagram, Self);

  // Add claims, determine transaction and valid times
  lTransactionTimes := TcIntegerList.Create;
  lTransactionTimes.Sorted := true;
  lTransactionTimes.IgnoreDuplicates := true;

  lValidTimes := TcIntegerList.Create;
  lValidTimes.Sorted := true;
  lValidTimes.IgnoreDuplicates := true;

  try
    // Determine Transaction and Valid times
    for i := 0 to aClaimSet.DerivedClaims.Count-1 do
      begin
      lClaim := aClaimSet.DerivedClaims[i];

      if lClaim.RegisteredAt <> cStartOfTime
         then lTransactionTimes.Add(lClaim.RegisteredAt);
      if lClaim.ExpiredAt <> cEndOfTime
         then lTransactionTimes.Add(lClaim.ExpiredAt);
      if lClaim.ValidFrom <> cStartOfTime
         then lValidTimes.Add(lClaim.ValidFrom);
      if lClaim.ValidUntil <> cEndOfTime
         then lValidTimes.Add(lClaim.ValidUntil);
      end;

    // y-Axis
    if lTransactionTimes.Count>0
       then begin
            fMinTT := lTransactionTimes[0];
            fMaxTT := lTransactionTimes[lTransactionTimes.Count-1];
            end
       else begin
            fMinTT := 1;
            fMaxTT := 1;
            end;
    if fMinTT<1 then fMinTT := 1;

    yLabels.Add('-'+cInfinite);
    for i := 0 to lTransactionTimes.Count-1 do
      yLabels.Add('T'+IntToStr(lTransactionTimes[i]));
    yLabels.Add(cInfinite);

    // x-Axis
    if lValidTimes.Count>0
       then begin
            fMinVT := lValidTimes[0];
            fMaxVT := lValidTimes[lValidTimes.Count-1];
            end
       else begin
            fMinVT := 1;
            fMaxVT := 1;
            end;
    if fMinVT<1 then fMinVT := 1;
    if fMaxVT<9 then fMaxVT := 9;

    xLabels.Add('-'+cInfinite);
    for i := MinVT to MaxVT do xLabels.Add('T'+IntToStr(i));
    xLabels.Add(cInfinite);

    // Add claims, determine yLayers
    for i := 0 to aClaimSet.DerivedClaims.Count-1 do
      begin
      lClaim := aClaimSet.DerivedClaims[i];

      if fQuery.InTransaction(lClaim)
         then begin
              lcdClaim := TcdClaim.Create(Diagram, Self, lClaim);

              if not lTransactionTimes.Find(lClaim.RegisteredAt, lIndex)
                 then lcdClaim.Claim_yLayer := 0
                 else lcdClaim.Claim_yLayer := lIndex + 1;

              cdClaims.Add(lcdClaim);
              end;
      end;

  finally
    lTransactionTimes.Free;
    lValidTimes.Free;
    end;
  end;

procedure TcdTemporalChart.Init(aAnnotation: TcAnnotation);
  var
    i, lIndex: Integer;
    lcdClaim: TcdClaim;
    lTransactionTimes, lValidTimes: TcIntegerList;
  begin
  // Add annotation, determine transaction and valid times
  lTransactionTimes := TcIntegerList.Create;
  lTransactionTimes.Sorted := true;
  lTransactionTimes.IgnoreDuplicates := true;

  lValidTimes := TcIntegerList.Create;
  lValidTimes.Sorted := true;
  lValidTimes.IgnoreDuplicates := true;

  try
    // Determine Transaction and Valid times
    if aAnnotation.RegisteredAt <> cStartOfTime
       then lTransactionTimes.Add(aAnnotation.RegisteredAt);
    if aAnnotation.ExpiredAt <> cEndOfTime
       then lTransactionTimes.Add(aAnnotation.ExpiredAt);
    if aAnnotation.ValidFrom <> cStartOfTime
       then lValidTimes.Add(aAnnotation.ValidFrom);
    if aAnnotation.ValidUntil <> cEndOfTime
       then lValidTimes.Add(aAnnotation.ValidUntil);

    // y-Axis
    if lTransactionTimes.Count>0
       then begin
            fMinTT := lTransactionTimes[0];
            fMaxTT := lTransactionTimes[lTransactionTimes.Count-1];
            end
       else begin
            fMinTT := 1;
            fMaxTT := 1;
            end;
    if fMinTT<1 then fMinTT := 1;

    yLabels.Add('-'+cInfinite);
    for i := 0 to lTransactionTimes.Count-1 do
      yLabels.Add('T'+IntToStr(lTransactionTimes[i]));
    yLabels.Add(cInfinite);

    // x-Axis
    if lValidTimes.Count>0
       then begin
            fMinVT := lValidTimes[0];
            fMaxVT := lValidTimes[lValidTimes.Count-1];
            end
       else begin
            fMinVT := 1;
            fMaxVT := 1;
            end;
    if fMinVT<1 then fMinVT := 1;
    if fMaxVT<9 then fMaxVT := 9;

    xLabels.Add('-'+cInfinite);
    for i := MinVT to MaxVT do xLabels.Add('T'+IntToStr(i));
    xLabels.Add(cInfinite);

    // Add annotation, determine yLayer
    lcdClaim := TcdClaim.Create(Diagram, Self, aAnnotation);
    if not lTransactionTimes.Find(aAnnotation.RegisteredAt, lIndex)
       then EInt('TcdTemporalChart.Init', 'Unexpected condition');
    lcdClaim.Claim_yLayer := lIndex + 1;

    cdClaims.Add(lcdClaim);

  finally
    lTransactionTimes.Free;
    lValidTimes.Free;
    end;
  end;

destructor TcdTemporalChart.Destroy;
  begin
  fcdClaims.Free;
  fxLabels.Free;
  fyLabels.Free;
  inherited
  end;

procedure TcdTemporalChart.InitChildren;
  var
    i: integer;
  begin
  for i := 0 to cdClaims.Count-1 do
    Diagram.Add(cdClaims[i]);
  if Assigned(fQuery)
     then begin
          Diagram.Add(QueryTT);
          Diagram.Add(QueryVT);
          end;
  end;

procedure TcdTemporalChart.ResetBounds_Internal;
  var
    lMaxWidth: integer;
    lAxisTop, lAxisRight,
    lAxisMaxHeight, lAxisMaxWidth: integer;
  begin
  lMaxWidth := ScrollBox.Width - cScrollBarWidth - IntLeft;
  SetIntBounds(IntLeft, IntTop, lMaxWidth, cHeight);

  // Calculations
  lAxisTop := ds.AxisTop;
  lAxisMaxHeight := (IntHeight - lAxisTop - ds.AxisBottom);
  fyAxisDelta := lAxisMaxHeight DIV (yLabels.Count-1);
  fAxisHeight := fyAxisDelta * (yLabels.Count-1);
  fAxisTop := lAxisTop + lAxisMaxHeight - fAxisHeight;

  lAxisRight := ds.AxisRight;
  lAxisMaxWidth := (IntWidth - ds.AxisLeft - lAxisRight);
  fxAxisDelta := lAxisMaxWidth DIV (xLabels.Count-1);
  fAxisWidth :=  fxAxisDelta * (xLabels.Count-1);
  fAxisRight := lAxisRight + lAxisMaxWidth - fAxisWidth;

  if Assigned(fQuery)
     then begin
          QueryTT.Min := Query.FromTT;
          QueryTT.Max := Query.UntilTT;
          QueryVT.Min := Query.FromVT;
          QueryVT.Max := Query.UntilVT;
          end;

  ConnectionPoint := Classes.Point(-1,-1);
  end;

function TcdTemporalChart.GetXByPos(aPos: integer): integer;
  begin
  result := ds.AxisLeft + (aPos * xAxisDelta);
  end;

function TcdTemporalChart.GetXByTime(aVT: integer): integer;
  var
    lPos: integer;
  begin
  // Assume the minimal VT = 5 and aVT = 5
  // This should be the second position on the axis.
  // The first position = - infinity
  lPos := aVT - MinVT + 1;
  result := ds.AxisLeft + (lPos * xAxisDelta);
  end;

function TcdTemporalChart.GetXStartOfTime: integer;
  begin
  result := ds.AxisLeft;
  end;

function TcdTemporalChart.GetXEndOfTime: integer;
  begin
  result := GetXByTime(MaxVT+1);
  end;

function TcdTemporalChart.GetYByPos(aPos: integer): integer;
  var
    lxMinTT: integer;
  begin
  lxMinTT := IntHeight - ds.AxisBottom;
  result := lxMinTT - (aPos * yAxisDelta);
  end;

(*
function TcdTemporalChart.GetYByTime(aTT: integer): integer;
  var
    lPos, lyMinTT: integer;
  begin
  lPos := aTT - MinTT + 1;
  lyMinTT := IntHeight - ds.AxisBottom;
  result := lyMinTT - (lPos * yAxisDelta);
  end;
*)

function TcdTemporalChart.GetYStartOfTime: integer;
  begin
  result := IntHeight - ds.AxisBottom;
  end;

function TcdTemporalChart.GetYEndOfTime: integer;
  begin
  result := GetYByPos(yLabels.Count-1); //GetYByTime(MaxTT+1);
  end;

procedure TcdTemporalChart.ResetBounds_Children;
  var
    i: integer;
  begin
  inherited;
  for i := 0 to cdClaims.Count-1 do
    cdClaims[i].ResetBounds;
  if Assigned(fQuery)
     then begin
          QueryTT.ResetBounds;
          QueryVT.ResetBounds;
          end;
  end;

procedure TcdTemporalChart.CorrectZOrder;
  var
    i: integer;
  begin
  inherited;
  for i := 0 to cdClaims.Count-1 do
    cdClaims[i].BringToFront; // Should be on top of temporal grid
  if Assigned(fQuery)
     then begin
          QueryTT.BringToFront;
          QueryVT.BringToFront;
          end;
  end;

procedure TcdTemporalChart.PaintOnCanvas(aCanvas: TCanvas);
  var
    lStr: string;
    i, x, y: integer;
    lModulo: integer;
  begin
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    // Background
    Brush_Style := bsSolid;
    Brush_Color := clWhite;
    Pen_Style := psClear;
    Rectangle(ds.AxisLeft, AxisTop, ds.AxisLeft + AxisWidth, IntHeight - ds.AxisBottom);

    // Axis
    Brush_Style := bsClear;
    Pen_Style := psSolid;
    Pen_Color := clGray;
    Font_Color := clGray;

    // Y-axis
    Pen_Width := 2;
    MoveTo(ds.AxisLeft, AxisTop);
    LineTo(ds.AxisLeft, IntHeight - ds.AxisBottom);

    Font_Orientation := 900;
    Font_Height := ds.FontHeight_AxisLabels;
    Font_Style := [fsBold];
    Font_Name := ds.FontName_Axis;
    lStr := 'Transaction time';
    TextOut(ds.AxisLabelMargin, AxisTop + TextWidth(lStr), lStr);

    Pen_Width := 1;
    Font_Orientation := 0;
    Font_Height := ds.FontHeight_AxisTime;
    Font_Style := [];
    Font_Name := ds.FontName_Axis;
    for i := 0 to yLabels.Count-1 do
      begin
      x := ds.AxisLeft - ds.AxisTick;
      y := GetYByPos(i);
      MoveTo(x, y);
      x := ds.AxisLeft + AxisWidth;
      LineTo(x, y);

      lStr := yLabels[i];
      y := y - TextHeight(lStr) DIV 2;

      if Assigned(Session.Transaction) and (Session.Transaction.StableRead) and (i = Session.Transaction.StableReadSnapshotTime)
         then begin
              Font_Style := [fsBold];
              Font_Color := clGreen;
              end
      else if Assigned(Session.Transaction) and (i = Session.Transaction.StartTime)
         then begin
              Font_Style := [fsBold];
              Font_Color := clBlue;
              end
         else begin
              Font_Style := [];
              Font_Color := clGray;
              end;

      Brush_Color := clWhite;
      Brush_Style := bsClear;
      if yLabels[i] = 'T' + IntToStr(Engine.SystemTime)
         then begin
              Brush_Color := clTimeColor;
              Font_Color := clBlack;
              end;

      if Assigned(fQuery) and (i >= QueryTT.MinPos) and (i <= QueryTT.MaxPos)
         then Font_Color := clRed;

      if (i = 0) or (i = yLabels.Count-1)
        then begin
             if i = 0 then y := y - 5 else y := y - 2;
             if i = 0 then x := -8 else x := 5;
             Font_Height := ds.FontHeight_InfinitySymbol;
             Font_Style := [];
             Font_Name := ds.FontName_InfinitySymbol;
             end
        else begin
             x := 0;
             Font_Height := ds.FontHeight_AxisTime;
             Font_Style := [];
             Font_Name := ds.FontName_Axis;
             end;

      if yLabels.Count<=12
         then lModulo := 1
         else lModulo := yLabels.Count DIV 12;

      if i mod lModulo = 0
         then TextOut(ds.AxisLeft - ds.AxisTimeMarginX + x, y, lStr);
      end;

    Brush_Color := clWhite;
    Brush_Style := bsClear;

    // X-axis
    Pen_Width := 2;
    MoveTo(ds.AxisLeft, IntHeight - ds.AxisBottom);
    LineTo(IntWidth - AxisRight, IntHeight - ds.AxisBottom);

    Font_Height := ds.FontHeight_AxisLabels;
    Font_Style := [fsBold];
    Font_Name := ds.FontName_Axis;
    lStr := 'Valid time';
    TextOut(IntWidth - TextWidth(lStr) - AxisRight, IntHeight - ds.AxisLabelMargin - TextHeight(lStr) + 4, lStr);

    Pen_Width := 1;
    Font_Height := ds.FontHeight_AxisTime;
    Font_Style := [];
    Font_Name := ds.FontName_Axis;
    Font_Color := clGray;
    for i := 0 to xLabels.Count-1 do
      begin
      x := GetXByPos(i);
      y := IntHeight - ds.AxisBottom + ds.AxisTick;
      MoveTo(x, y);
      y := AxisTop;
      LineTo(x, y);

      lStr := xLabels[i];
      x := x - TextWidth(lStr) DIV 2;
      y := IntHeight - ds.AxisBottom + ds.AxisTimeMarginY;

      if Assigned(fQuery) and (i >= QueryVT.MinPos) and (i <= QueryVT.MaxPos)
         then Font_Color := clRed
         else Font_Color := clGray;

      if (i = 0) or (i = xLabels.Count-1)
        then begin
             y := y - 3;
             if i = 0 then x := x - 9 else x := x - 1;
             Font_Height := ds.FontHeight_InfinitySymbol;
             Font_Style := [];
             Font_Name := ds.FontName_InfinitySymbol;
             end
        else begin
             Font_Height := ds.FontHeight_AxisTime;
             Font_Style := [];
             Font_Name := ds.FontName_Axis;
             end;

      TextOut(x, y, lStr);
      end;
    end;
  end;

procedure TcdTemporalChart.ExecuteQuery;
  begin
  Selection.Clear;
  if not Assigned(fQuery) then exit;

  with Query do
     begin
     FromTT := QueryTT.Min;
     UntilTT := QueryTT.Max;
     FromVT := QueryVT.Min;
     UntilVT := QueryVT.Max;
     Execute;
     end;
  ResetBounds;

  if Assigned(gRadioStation)
     then gRadioStation.BroadCast(cRadioEvent_TemporalChart_QueryCriteria_Changed, Self);
  end;

procedure TcdTemporalChart.ClearQuery;
  begin
  if not Assigned(fQuery) then exit;
  QueryTT.Clear;
  QueryVT.Clear;
  ExecuteQuery;
  end;

procedure TcdTemporalChart.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer);
  var
    i, c, x1, x2, y1, y2: integer;
    lShiftState: TShiftState;
  begin
  if not Assigned(fQuery) then exit;

  // Only inspect Shift, Alt and Ctrl
  lShiftState := aShiftState*[ssShift, ssAlt, ssCtrl];

  if lShiftState = [ssCtrl]
     then begin
          ClearQuery;
          exit;
          end;

  // y Axis click
  for i := 0 to yLabels.Count-1 do
    begin
    c := GetYByPos(i);
    x1 := ds.AxisLeft - ds.AxisClickMargin - ds.AxisClickHorizontalOffset;
    y1 := c - (ds.AxisClickMargin DIV 2);
    x2 := ds.AxisLeft;
    y2 := c + (ds.AxisClickMargin DIV 2);

    if (aX>=x1) and (aX<=x2) and (aY>=y1) and (aY<=y2)
       then begin
            if lShiftState = [] then QueryTT.SetPos(i)
            else if lShiftState = [ssShift] then QueryTT.MaxPos := i;
            ExecuteQuery;
            exit;
            end;
    end;

  // x Axis click
  for i := 0 to xLabels.Count-1 do
    begin
    c := GetXByPos(i);

    x1 := c - (ds.AxisClickMargin DIV 2);
    x2 := c + (ds.AxisClickMargin DIV 2);
    y1 := IntHeight - ds.AxisBottom;
    y2 := IntHeight - ds.AxisBottom + ds.AxisClickVerticalOffset;

    if (aX>=x1) and (aX<=x2) and (aY>=y1) and (aY<=y2)
       then begin
            if lShiftState = [] then QueryVT.SetPos(i)
            else if lShiftState = [ssShift] then QueryVT.MaxPos := i;
            ExecuteQuery;
            exit;
            end;
    end;
  end;

procedure TcdTemporalChart.SelectClaim(aClaim: TcClaim);
  begin
  Selection.SelectDerivedClaim(aClaim);
//  ResetBounds;
//  Diagram.ForceRedraw;
  end;

procedure TcdTemporalChart.SelectClaim(aClaimID: integer);
  var
    lElement: TcElement;
  begin
  lElement := Engine.Elements.Find(aClaimID);
  if lElement is TcClaim
     then Selection.SelectDerivedClaim(lElement as TcClaim)
  else if lElement is TcAnnotation
     then Selection.SelectAnnotation(lElement as TcAnnotation);
  end;

procedure TcdTemporalChart.UnselectClaim;
  begin
  SelectClaim(nil);
  end;


{ TcdClaim }

constructor TcdClaim.Create(aDiagram: TcdDiagram; aTemporalChart: TcdTemporalChart; aClaim: TcClaim);
  begin
  inherited Create(aDiagram, ooAuto);
  fSelectionGroup := 1;
  Clickable := True;

  fTemporalChart := aTemporalChart;
  fEngine := aClaim.Engine;
  fDiagramSettings := aTemporalChart.ds;

  Claim_ID := aClaim.ID;
  end;

procedure TcdClaim.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer);
  begin
  TemporalChart.SelectClaim(Claim_ID);
  if Assigned(gRadioStation)
     then gRadioStation.BroadCast(cRadioEvent_TemporalChart_Claim_Selected, TemporalChart);
  end;

procedure TcdClaim.ResetBounds_Internal;
  var
    a: integer;
    lLeft, lRight, lTop, lBottom, lWidth, lHeight: integer;
    lSelection: TcSelection;
    lElement: TcElement;
    lClaim: TcClaim;
    lAnnotation: TcAnnotation;
    lAnnotationType: TcAnnotationType;
  begin
  inherited;

  lElement := Engine.Elements.Find(Claim_ID);
  if not Assigned(lElement) then EInt('TcdClaim.ResetBounds_Internal', 'Claim expected');
  if not (lElement is TcClaim) then EInt('TcdClaim.ResetBounds_Internal', 'Claim expected');
  lClaim := lElement as TcClaim;

  lSelection := TemporalChart.Selection;

  if lClaim.RegisteredAt = cStartOfTime
    then lBottom := TemporalChart.GetYStartOfTime
    else lBottom := TemporalChart.GetYByPos(Claim_yLayer); //TemporalChart.GetYByTime(lClaim.RegisteredAt);

  if lClaim.ExpiredAt = cEndOfTime
    then lTop := TemporalChart.GetYEndOfTime+1
    else lTop := TemporalChart.GetYByPos(Claim_yLayer+1); //TemporalChart.GetYByTime(lClaim.ExpiredAt)+1;

  if lClaim.ValidFrom = cStartOfTime
    then lLeft := TemporalChart.GetXStartOfTime+1 // Assume T1
    else lLeft := TemporalChart.GetXByTIme(lClaim.ValidFrom)+1;

  if lClaim.ValidUntil = cEndOfTime
    then lRight := TemporalChart.GetXEndofTime
    else lRight := TemporalChart.GetXByTime(lClaim.ValidUntil);

  lWidth := lRight - lLeft;
  lHeight := lBottom - lTop;

  SetIntBounds(
    TemporalChart.Left + lLeft,
    TemporalChart.Top + lTop,
    lWidth, lHeight);

  // Retrieve data required for painting
  if Assigned(lClaim)
     then Claim_TimeLineInterpretation := lClaim.ClaimType.TimelineInterpretation
     else Claim_TimeLineInterpretation := true;
  Claim_Original := lClaim.IsSource;
  Claim_RegisteredAt := lClaim.RegisteredAt;
  Claim_ExpiredAt := lClaim.ExpiredAt;
  Claim_ValidFrom := lClaim.ValidFrom;
  Claim_ValidUntil := lClaim.ValidUntil;
  if lClaim.ClaimType.Roles.Count = lClaim.ClaimType.Identity.Count
     then Claim_Text := lClaim.ValueStr
     else Claim_Text := lClaim.GetValueStrWithoutIdentity;

  if Assigned(lClaim) and Assigned(TemporalChart.Query)
     then begin
          Claim_InTransaction :=
            Assigned(TemporalChart.Query)
              and
            TemporalChart.Query.InTransaction(lClaim);

          Claim_InResult :=
            Assigned(TemporalChart.Query)
              and
            TemporalChart.Query.InResult(lClaim);
          end
     else begin
          Claim_InTransaction := false;
          Claim_InResult := false;
          end;

  if lClaim is TcClaim
     then Claim_Selected := lSelection.DerivedClaimSelected(lClaim)
  else if lClaim is TcAnnotation
     then Claim_Selected := lSelection.AnnotationSelected(lClaim as TcAnnotation)
  else EInt('TcdClaim.ResetBounds_Internal', 'Unexpected Claim type.');

  Claim_Related := lSelection.IsRelated(lClaim.SourceClaim);

  StartedAnnotation_ID := cNoID;
  StartedAnnotation_Selected := false;
  StartedAnnotation_Related := false;
  EndedAnnotation_ID := cNoID;
  EndedAnnotation_Selected := false;
  EndedAnnotation_Related := false;
  ExpiredAnnotation_ID := cNoID;
  ExpiredAnnotation_Selected := false;
  ExpiredAnnotation_Related := false;
  RegularAnnotations := false;
  RegularAnnotations_Related := false;
  InDoubt := false;
  InDoubt_Related := false;

  for a := 0 to lClaim.Annotations.Count-1 do
    begin
    lAnnotation := lClaim.Annotations[a];
    lAnnotationType := lAnnotation.AnnotationType;
    if lAnnotationType.Kind = akStarted
       then begin
            StartedAnnotation_ID := lAnnotation.ID;
            StartedAnnotation_Selected := lSelection.DerivedClaimSelected(lAnnotation);
            StartedAnnotation_Related := lSelection.IsRelated(lAnnotation.DerivedFrom);
            end
    else if lAnnotationType.Kind = akEnded
       then begin
            EndedAnnotation_ID := lAnnotation.ID;
            EndedAnnotation_Selected := lSelection.DerivedClaimSelected(lAnnotation);
            EndedAnnotation_Related := lSelection.IsRelated(lAnnotation.DerivedFrom);
            end
    else if lAnnotationType.Kind = akExpired
       then begin
            ExpiredAnnotation_ID := lAnnotation.ID;
            ExpiredAnnotation_Selected := lSelection.DerivedClaimSelected(lAnnotation);
            ExpiredAnnotation_Related := lSelection.IsRelated(lAnnotation.DerivedFrom);
            end
    else if lAnnotationType.Kind = akDoubt
       then begin
            InDoubt := true;
            InDoubt_Related := lSelection.IsRelated(lAnnotation.ID) or lSelection.IsRelated(lAnnotation.DerivedFrom);
            end
    else begin
         RegularAnnotations := true;
         if not RegularAnnotations_Related
            then RegularAnnotations_Related := lSelection.IsRelated(lAnnotation.ID) or lSelection.IsRelated(lAnnotation.DerivedFrom);
         end;
    end;

  if Claim_Selected
     then TemporalChart.ConnectionPoint := Classes.Point(IntLeft + 20, IntTop + (IntHeight div 2));
  end;

procedure TcdClaim.PaintOnCanvas (aCanvas: TCanvas);

  var
    x, y: integer;
    lStr, lStr1, lStr2: String;
  begin
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    // -- Main Rectangle

    if Claim_TimeLineInterpretation
      then Brush_Style := bsSolid
      else Brush_Style := bsBDiagonal; // Make overlap visible
    Pen_Style := psSolid;
    Pen_Width := 1;

    // https://docwiki.embarcadero.com/RADStudio/Alexandria/en/Colors_in_the_VCL
    if Claim_Selected or (Assigned(TemporalChart.Query) and TemporalChart.Query.Active and Claim_InResult)
       then begin
            Brush_Color :=  clWebLightYellow;
            Pen_Color := clWebOrange;
            end
    else if Claim_Related
       then begin
            Brush_Color :=  clWebLightYellow; //clWebIvory;
            Pen_Color := clWebDarkGray;
            end
    else if Claim_ExpiredAt <> cEndOfTime
       then begin
            Brush_Color := gDiagramSettings.ColorToLightVariant(clWebGainsboro, 216);
            Pen_Color := clWebDarkGray;
            end
    else if Claim_ValidUntil <> cEndOfTime
       then begin
            Brush_Color := gDiagramSettings.ColorToLightVariant(clWebLightSteelBlue, 210);
            Pen_Color := clWebDarkGray; //clWebCornFlowerBlue;
            end
    else begin
         Brush_Color := gDiagramSettings.ColorToLightVariant(clMoneyGreen, 210);
         Pen_Color := clWebDarkGray; //clWebOliveDrab;
         end;

    Rectangle(IntClientRect);


    // -- Text

    Brush_Style := bsClear;
    Font_Color := clBlack;
    Font_Height := ds.FontHeight_Tuple;
    Font_Style := [];
    Font_Name := ds.FontName_Tuple;
    lStr1 := Claim_Text;
    lStr2 := '  (' + cRefChar+IntToStr(Claim_ID) + ')';
    lStr := lStr1 + lStr2;
    x := (IntWidth div 2) - (TextWidth(lStr) div 2);
    y := (IntHeight div 2) - (TextHeight(lStr) div 2);

    Font_Color := clBlack;
    TextOut(x, y, lStr1);
    x := x + TextWidth(lStr1);
    Font_Color := clGray;
    TextOut(x, y, lStr2);


    // -- Annotations

    if TemporalChart.ShowTemporalAnnotations
       then begin
            if StartedAnnotation_ID <> cNoID
               then begin
                    if StartedAnnotation_Related
                       then begin
                            Brush_Color :=  clWebLightYellow; //clWebIvory;
                            Pen_Color := clWebDarkGray;
                            end
                    else if Claim_ExpiredAt <> cEndOfTime
                       then begin
                            Brush_Color := gDiagramSettings.ColorToLightVariant(clWebGainsboro, 216);
                            Pen_Color := clWebDarkGray;
                            end
                    else if Claim_ValidUntil <> cEndOfTime
                       then begin
                            Brush_Color := gDiagramSettings.ColorToLightVariant(clWebLightSteelBlue, 210);
                            Pen_Color := clWebDarkGray; //clWebCornFlowerBlue;
                            end
                       else begin
                            Brush_Color := gDiagramSettings.ColorToLightVariant(clMoneyGreen, 210);
                            Pen_Color := clWebDarkGray; //clWebOliveDrab;
                            end;

                    x := 0;
                    y := (IntHeight - cVertImplicitAnnotationHeight) div 2;
                    Rectangle(TRect.Create(x, y, x + cVertImplicitAnnotationWidth, y + cVertImplicitAnnotationHeight));
                    end;

            if EndedAnnotation_ID <> cNoID
               then begin
                    if EndedAnnotation_Related
                       then begin
                            Brush_Color :=  clWebLightYellow; //clWebIvory;
                            Pen_Color := clWebDarkGray;
                            end
                    else if Claim_ExpiredAt <> cEndOfTime
                       then begin
                            Brush_Color := gDiagramSettings.ColorToLightVariant(clWebGainsboro, 216);
                            Pen_Color := clWebDarkGray;
                            end
                    else if Claim_ValidUntil <> cEndOfTime
                       then begin
                            Brush_Color := gDiagramSettings.ColorToLightVariant(clWebLightSteelBlue, 210);
                            Pen_Color := clWebDarkGray; //clWebCornFlowerBlue;
                            end
                       else begin
                            Brush_Color := gDiagramSettings.ColorToLightVariant(clMoneyGreen, 210);
                            Pen_Color := clWebDarkGray; //clWebOliveDrab;
                            end;

                    x := IntWidth - cVertImplicitAnnotationWidth;
                    y := (IntHeight - cVertImplicitAnnotationHeight) div 2;
                    Rectangle(TRect.Create(x, y, x + cVertImplicitAnnotationWidth, y + cVertImplicitAnnotationHeight));
                    end;

            if ExpiredAnnotation_ID <> cNoID
               then begin
                    if ExpiredAnnotation_Related
                       then begin
                            Brush_Color :=  clWebLightYellow; //clWebIvory;
                            Pen_Color := clWebDarkGray;
                            end
                       else begin
                            Brush_Color := gDiagramSettings.ColorToLightVariant(clWebGainsboro, 216);
                            Pen_Color := clWebDarkGray;
                            end;

                    x := (IntWidth - cHorzImplicitAnnotationWidth) div 2;
                    y := 0;
                    Rectangle(TRect.Create(x, y, x + cHorzImplicitAnnotationWidth, y + cHorzImplicitAnnotationHeight));
                    end;
            end;

    if InDoubt or RegularAnnotations
      then begin
           Pen_Color := clWebDarkGray;
           if RegularAnnotations_Related or InDoubt_Related
              then Brush_Color := clWebLightYellow //clWebIvory
           else if InDoubt
              then Brush_Color := clWebDarkOrange
              else Brush_Color := clWebPaleTurquoise;
           Rectangle(TRect.Create(
             IntWidth -cAnnotationSide -cAnnotationMargin, cAnnotationMargin,
             IntWidth -cAnnotationMargin, cAnnotationMargin + cAnnotationSide
           ));
           end;
    end;
  end;


{ TcdQuery }

constructor TcdQuery.Create(aDiagram: TcdDiagram; aTemporalChart: TcdTemporalChart);
  begin
  inherited Create(aDiagram, ooAuto);
  fSelectionGroup := 1;
  Clickable := False;
  fTemporalChart := aTemporalChart;
  fDiagramSettings := aTemporalChart.ds;
  Clear;
  end;

procedure TcdQuery.SwapPos;
  var
    T: TcTime;
  begin
  T := fMaxPos;
  fMaxPos := fMinPos;
  fMinPos := T;
  end;

function TcdQuery.GetTimeByPos(aPos: integer): TcTime;
  var
    lStr: string;
    lLabels: TStringlist;
  begin
  result := cNoTime;
  if aPos = cNoTime then exit;

  if Self is TcdQueryTT
    then lLabels := TemporalChart.yLabels
    else lLabels := TemporalChart.xLabels;

  lStr := lLabels[aPos];
  if lStr[1]='T'
     then result := StrToInt(Copy(lStr, 2, Length(lStr)-1))
  else if lStr = '-'+cInfinite
     then result := cStartOfTime
  else if lStr = cInfinite
     then result := cEndOfTime;
  end;

function TcdQuery.GetPosByTime(aTime: TcTime): integer;
  var
    i: integer;
    lStr: string;
    lLabels: TStringlist;
  begin
  result := cNoTime;
  if aTime = cNoTime then exit;

  if Self is TcdQueryTT
    then lLabels := TemporalChart.yLabels
    else lLabels := TemporalChart.xLabels;

  if aTime = cStartOfTime
    then result := 0
  else if aTime = cEndOfTime
    then result := lLabels.Count-1
  else begin
       lStr := 'T' + IntToStr(aTime);
       for i := 1 to lLabels.Count-2 do
         begin
         if lLabels[i] = lStr
            then begin
                 result := i;
                 exit;
                 end;
         end;
       end;
  end;

function TcdQuery.GetMin: TcTime;
  begin
  result := GetTimeByPos(MinPos);
  end;

function TcdQuery.GetMax: TcTime;
  begin
  result := GetTimeByPos(MaxPos);
  end;

procedure TcdQuery.SetMin(aTime: TcTime);
  begin
  fMinPos := GetPosByTime(aTime);
  end;

procedure TcdQuery.SetMax(aTime: TcTime);
  begin
  fMaxPos := GetPosByTime(aTime);
  end;

procedure TcdQuery.Clear;
  begin
  fMinPos := cNoTime;
  fMaxPos := cNoTime;
  end;

procedure TcdQuery.SetPos(aPos: integer);
  begin
  fMinPos := aPos;
  fMaxPos := aPos;
  end;

procedure TcdQuery.SetMinPos(aPos: integer);
  begin
  fMinPos := aPos;
  if fMaxPos = cNoTime then fMaxPos := fMinPos;
  if fMaxPos < fMinPos then SwapPos;
  end;

procedure TcdQuery.SetMaxPos(aPos: integer);
  begin
  fMaxPos := aPos;
  if fMinPos = cNoTime then fMinPos := fMaxPos;
  if fMaxPos < fMinPos then SwapPos;
  end;

procedure TcdQuery.PaintOnCanvas(aCanvas: TCanvas);
  begin
  if (MinPos = cNoTime) or (MaxPos = cNoTime)
     then Exit;

  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Pen_Style := psSolid;
    Pen_Width := 1;
    Pen_Color := clRed;
    if Self is TcdQueryTT
       then Brush_Style := bsBDiagonal
       else Brush_Style := bsFDiagonal;
    Brush_Color := clRed;

    Rectangle(IntClientRect);
    end;
  end;


{ TcdQueryTT }

procedure TcdQueryTT.ResetBounds_Internal;
  var
    x, y, y2, width, height: integer;
  begin
  inherited;
  if (MinPos = cNoTime) or (MaxPos = cNoTime)
     then begin
          x := 0;
          y := 0;
          width := 0;
          height := 0;
          end
     else begin
          x := ds.AxisLeft;
          y := TemporalChart.GetYByPos(MaxPos);

          width := TemporalChart.AxisWidth;
          y2 := TemporalChart.GetYByPos(MinPos);
          height := Abs(y2-y);

          if height = 0
             then begin
                  height := 2;
                  dec(y);
                  end;
          end;

  SetIntBounds(TemporalChart.Left + x, TemporalChart.Top + y, width, height);
  end;


{ TcdQueryVT }

procedure TcdQueryVT.ResetBounds_Internal;
  var
    x, x2, y, width, height: integer;
  begin
  inherited;
  if (MinPos = cNoTime) or (MaxPos = cNoTime)
     then begin
          x := 0;
          y := 0;
          width := 0;
          height := 0;
          end
     else begin
          x := TemporalChart.GetXByPos(MinPos);
          y := TemporalChart.AxisTop;

          x2 := TemporalChart.GetXByPos(MaxPos);
          width := Abs(x2-x);
          if width = 0 then width := 2;
          height := TemporalChart.AxisHeight;
          end;

  SetIntBounds(TemporalChart.Left + x, TemporalChart.Top + y, width, height);
  end;

end.
