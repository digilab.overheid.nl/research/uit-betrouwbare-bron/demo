// -----------------------------------------------------------------------------
//
// diagram.Lines
//
// -----------------------------------------------------------------------------
//
// Lines between objects in inspector.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit diagram.Lines;

{$MODE Delphi}

interface

uses
  Classes, Graphics,
  diagram.Core, diagram.Settings, diagram.Operations, diagram.InfoPanel,
  diagram.SourceClaims, diagram.Temporal;

const
  cConnectorWidth = 8;

type
  TcdOperationLines =
    class(TcdObject)
      private
        fOperationStack: TcdOperationStack;
        fInfoPanel: TcdInfoPanel;
      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram); reintroduce;
        destructor Destroy; override;

        procedure Init(aOperationStack: TcdOperationStack; aInfoPanel: TcdInfoPanel);
        procedure PaintOnCanvas(aCanvas: TCanvas); override;
      end;

  TcdContentLines =
    class(TcdObject)
      private
        fSourceClaimStack: TcdSourceClaimStack;
        fTemporalChart: TcdTemporalChart;
        fInfoPanel: TcdInfoPanel;
      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram); reintroduce;
        destructor Destroy; override;

        procedure Init(aSourceClaimStack: TcdSourceClaimStack; aTemporalChart: TcdTemporalChart; aInfoPanel: TcdInfoPanel);
        procedure PaintOnCanvas(aCanvas: TCanvas); override;
      end;

implementation

uses
  SysUtils,
  common.RadioStation;


{ TcdInspectorLines }

constructor TcdOperationLines.Create(aDiagram: TcdDiagram);
  begin
  inherited Create(aDiagram, ooUpperLeft);
  Clickable := false;
  end;

destructor TcdOperationLines.Destroy;
  begin
  inherited
  end;

procedure TcdOperationLines.Init(aOperationStack: TcdOperationStack; aInfoPanel: TcdInfoPanel);
  begin
  fOperationStack := aOperationStack;
  fInfoPanel := aInfoPanel;
  end;

procedure TcdOperationLines.ResetBounds_Internal;
  begin
  SetIntBounds(0, 0, ScrollBox.Width, ScrollBox.Height);
  end;

procedure TcdOperationLines.PaintOnCanvas(aCanvas: TCanvas);
  begin
  if not Assigned(fOperationStack) then exit;

  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Brush_Style := bsClear;
    Pen_Color := clWebDarkGray;
    Pen_Width := 2;
    Pen_Style := psSolid; // Note: For dashed or dotted lines the pen width must be 1
    MoveTo(fOperationStack.ConnectionPoint_Stack_Bottom.X, fOperationStack.ConnectionPoint_Stack_Bottom.Y);
    LineTo(fOperationStack.ConnectionPoint_Stack_Bottom.X, Height);

    if Assigned(fInfoPanel)
       then begin
            Brush_Style := bsClear;
            Pen_Style := psSolid;
            Pen_Color := clWebDarkGray;
            Pen_Width := 2;
            MoveTo(fOperationStack.ConnectionPoint_SelectedClaim_Right.X, fOperationStack.ConnectionPoint_SelectedClaim_Right.Y);
            LineTo(fOperationStack.ConnectionPoint_SelectedClaim_Right.X + cConnectorWidth, fOperationStack.ConnectionPoint_SelectedClaim_Right.Y);
            LineTo(fInfoPanel.ConnectionPoint.X - cConnectorWidth, fInfoPanel.ConnectionPoint.Y);
            LineTo(fInfoPanel.ConnectionPoint.X, fInfoPanel.ConnectionPoint.Y);
            end;
    end;
  end;



{ TcdContentLines }

constructor TcdContentLines.Create(aDiagram: TcdDiagram);
  begin
  inherited Create(aDiagram, ooUpperLeft);
  Clickable := false;
  end;

destructor TcdContentLines.Destroy;
  begin
  inherited
  end;

procedure TcdContentLines.Init(aSourceClaimStack: TcdSourceClaimStack; aTemporalChart: TcdTemporalChart; aInfoPanel: TcdInfoPanel);
  begin
  fSourceClaimStack := aSourceClaimStack;
  fTemporalChart := aTemporalChart;
  fInfoPanel := aInfoPanel;
  end;

procedure TcdContentLines.ResetBounds_Internal;
  begin
  SetIntBounds(0, 0, ScrollBox.Width, ScrollBox.Height);
  end;

procedure TcdContentLines.PaintOnCanvas(aCanvas: TCanvas);
  var
    x, y: integer;
  begin
  if not Assigned(fInfoPanel) then exit;
  if not ((fInfoPanel.Kind = ipkQuery) or (fInfoPanel.Kind = ipkClaim)) then exit;

  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Brush_Style := bsClear;
    Pen_Color := clWebDarkGray;
    Pen_Width := 1;
    Pen_Style := psDot; // Note: For dashed or dotted lines the pen width must be 1

    if fSourceClaimStack.ConnectionPoint.x>=0
       then begin
            x := fSourceClaimStack.ConnectionPoint.x;
            y := fSourceClaimStack.ConnectionPoint.y;
            end
    else if fTemporalChart.ConnectionPoint.x>=0
       then begin
            x := fTemporalChart.ConnectionPoint.x;
            y := fTemporalChart.ConnectionPoint.y;
            end
       else exit;

    if (x >= fInfoPanel.Left) and (x <= fInfoPanel.Left + fInfoPanel.Width)
       then begin
            MoveTo(x, y);
            LineTo(x, fInfoPanel.ConnectionPoint.y);
            MoveTo(x+1, y);
            LineTo(x+1, fInfoPanel.ConnectionPoint.y);
            end
       else begin
            MoveTo(x, y);
            LineTo(fInfoPanel.ConnectionPoint.x, fInfoPanel.ConnectionPoint.y);
            MoveTo(x+1, y);
            LineTo(fInfoPanel.ConnectionPoint.x+1, fInfoPanel.ConnectionPoint.y);
            end;
    end;
  end;


end.
