// -----------------------------------------------------------------------------
//
// diagram.Operations
//
// -----------------------------------------------------------------------------
//
// Visualisation of a stack of Operations. Used in ace.Forms.ClaimSet.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit diagram.Operations;

{$MODE Delphi}

interface

uses
  SysUtils, Classes, Types, Generics.Collections, Graphics, Controls,
  diagram.Core, diagram.Settings, diagram.Selection, ace.Core.Engine;

const
  cRadioEvent_OperationStack_Operation_Selected = 'OperationStack.Operation.Selected'; // Object = OperationStack

const
  cAxisLeft: integer = 40;
  cAxisBottom: integer = 20;

  cMinItems: integer = 5;
  cMaxItems: integer = 10;
  cRowHeight: integer = 32;

  cHorizontalCorrection: integer = 32;

type
  // Forward
  TcdOperation = class;

  TcdOperationStack =
    class(TcdObject)
      private
        fEngine: TcEngine;
        fSession: TcSession;
        fDiagramSettings: TcDiagramSettings;

        fSelection: TcSelection;

        fcdOperations: TList<TcdOperation>; // Does not own the Operations

        fMinY, fMaxY: integer;

        fyAxisDelta,
        fAxisWidth,
        fAxisTop
        : integer;

        fConnectionPoint_Stack_Bottom: TPoint;
        fConnectionPoint_SelectedClaim_Right: TPoint;

        function GetYByPos(aPos: integer): integer;

        property cdOperations: TList<TcdOperation> read fcdOperations;

        property yAxisDelta: integer read fyAxisDelta;
        property AxisWidth: integer read fAxisWidth;
        property AxisTop: integer read fAxisTop;

      protected
        procedure ResetBounds_Internal; override;
        procedure ResetBounds_Children; override;
        procedure CorrectZOrder; override;

      public
        constructor Create(aDiagram: TcdDiagram; aSession: TcSession; aSelection: TcSelection); reintroduce;
        destructor Destroy; override;

        procedure InitChildren; override;
        procedure PaintOnCanvas(aCanvas: TCanvas); override;

        function GetOldestOperationID: integer;
        procedure SelectOperation(aOperation: TcOperation); overload;
        procedure SelectOperation(aOperationID: integer); overload;
        procedure UnselectOperation;

        property Engine: TcEngine read fEngine;
        property Session: TcSession read fSession;
        property ds: TcDiagramSettings read fDiagramSettings;
        property Selection: TcSelection read fSelection;

        property ConnectionPoint_Stack_Bottom: TPoint read fConnectionPoint_Stack_Bottom write fConnectionPoint_Stack_Bottom;
        property ConnectionPoint_SelectedClaim_Right: TPoint read fConnectionPoint_SelectedClaim_Right write fConnectionPoint_SelectedClaim_Right;
      end;

  TcdOperation =
    class(TcdObject)
      private
        fOperationStack: TcdOperationStack;
        fEngine: TcEngine;
        fDiagramSettings: TcDiagramSettings;

        // Data cached from TcOperation for fast repainting
        Operation_ID: integer;
        Operation_SeqNr: integer;
        Operation_Name: string;
        Operation_Selected: boolean;
        Operation_Related: boolean;

        property Engine: TcEngine read fEngine;

      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aOperationStack: TcdOperationStack; aSeqNr: integer; aOperation: TcOperation); reintroduce;

        procedure PaintOnCanvas (aCanvas: TCanvas); override;
        procedure MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer); override;

        property OperationStack: TcdOperationStack read fOperationStack;
        property ds: TcDiagramSettings read fDiagramSettings;
      end;

implementation

uses
  common.RadioStation;

{ TcdOperationStack }

constructor TcdOperationStack.Create(aDiagram: TcdDiagram; aSession: TcSession; aSelection: TcSelection);
  var
    i: Integer;
    lOperation: TcOperation;
    lcdOperation: TcdOperation;
    lFirstOperation, lSeqNr: integer;
  begin
  inherited Create(aDiagram, ooUpperLeft);
  fEngine := aSession.Engine;
  fSession := aSession;
  fDiagramSettings := gDiagramSettings;
  fSelection := aSelection;

  fcdOperations := TList<TcdOperation>.Create;
  if Engine.Operations.Count <= cMaxItems
     then lFirstOperation := 0
     else lFirstOperation := Engine.Operations.Count - cMaxItems;
  lSeqNr := 0;
  for i := lFirstOperation to Engine.Operations.Count-1 do
    begin
    lOperation := Engine.Operations[i];
    lcdOperation := TcdOperation.Create(Diagram, Self, lSeqNr, lOperation);
    cdOperations.Add(lcdOperation);
    inc(lSeqNr);
    end;
  end;

destructor TcdOperationStack.Destroy;
  begin
  fcdOperations.Free;
  inherited
  end;

procedure TcdOperationStack.InitChildren;
  var
    i: integer;
  begin
  for i := 0 to cdOperations.Count-1 do
    Diagram.Add(cdOperations[i]);
  end;

procedure TcdOperationStack.ResetBounds_Internal;
  var
    lAxisTop, lAxisHeight, lAxisMaxHeight: integer;
    i, lHeight, lWidth, lMaxWidth: integer;
    lcdOperation: TcdOperation;
  begin
  // Calculations
  fMinY := 1;
  fMaxY := cdOperations.Count;
  if fMaxY < cMinItems then fMaxY := cMinItems;
  if fMaxY > cMaxItems then fMaxY := cMaxItems;
  lHeight := cAxisBottom + (fMaxY * cRowHeight);

  with ScrollBox.Diagram.ScaleCanvas do
    begin
    lMaxWidth := 0;

    Font_Height := ds.FontHeight_Tuple;
    Font_Style := [];
    Font_Name := ds.FontName_Tuple;

    for i := 0 to cdOperations.Count-1 do
      begin
      lcdOperation := cdOperations[i];
      lWidth := TextWidth(Format('%s (%s%d)', [lcdOperation.Operation_Name, cRefChar, lcdOperation.Operation_ID]));
      if lWidth > lMaxWidth then lMaxWidth := lWidth;
      end;
    end;
  lWidth := lMaxWidth + cAxisLeft + cHorizontalCorrection;

  SetIntBounds(IntLeft, IntTop, lWidth, lHeight);

  // Boundaries
  lAxisTop := ds.AxisTop;
  lAxisMaxHeight := (IntHeight - lAxisTop - cAxisBottom);
  fyAxisDelta := lAxisMaxHeight DIV fMaxY;
  lAxisHeight := fyAxisDelta * fMaxY;
  fAxisTop := lAxisTop + lAxisMaxHeight - lAxisHeight;
  fAxisWidth := (IntWidth - cAxisLeft - ds.AxisRight);
  end;

function TcdOperationStack.GetYByPos(aPos: integer): integer;
  var
    lxMinTT: integer;
  begin
  lxMinTT := IntHeight - cAxisBottom;
  result := lxMinTT - (aPos * yAxisDelta);
  end;

procedure TcdOperationStack.ResetBounds_Children;
  var
    i: integer;
  begin
  inherited;
  for i := 0 to cdOperations.Count-1 do
    cdOperations[i].ResetBounds;
  end;

procedure TcdOperationStack.CorrectZOrder;
  var
    i: integer;
  begin
  inherited;
  for i := 0 to cdOperations.Count-1 do
    cdOperations[i].BringToFront; // Should be on top of temporal grid
  end;

procedure TcdOperationStack.PaintOnCanvas(aCanvas: TCanvas);
  var
    lStr: string;
    i, x, y: integer;
  begin
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    // Background
    Brush_Style := bsSolid;
    Brush_Color := clWhite;
    Pen_Style := psClear;
    Rectangle(cAxisLeft, AxisTop, IntWidth - ds.AxisRight, IntHeight - cAxisBottom);

    // Axis
    Brush_Style := bsClear;
    Pen_Style := psSolid;
    Pen_Color := clGray;
    Font_Color := clGray;

    // Y-axis Left and Right
    Pen_Width := 2;
    MoveTo(cAxisLeft, AxisTop);
    LineTo(cAxisLeft, IntHeight - cAxisBottom);
    MoveTo(IntWidth - ds.AxisRight, AxisTop);
    LineTo(IntWidth - ds.AxisRight, IntHeight - cAxisBottom);

    Font_Orientation := 900;
    Font_Height := ds.FontHeight_AxisLabels;
    Font_Style := [fsBold];
    Font_Name := ds.FontName_Axis;
    lStr := 'Operations';
    TextOut(ds.AxisLabelMargin, AxisTop + TextWidth(lStr), lStr);

    // Horizontal Lines
    Pen_Width := 1;
    Font_Orientation := 0;
    Font_Height := ds.FontHeight_AxisTime;
    Font_Style := [];
    Font_Name := ds.FontName_Axis;
    for i := 0 to fMaxY do
      begin
      x := cAxisLeft;
      y := GetYByPos(i);
      MoveTo(x, y);
      x := cAxisLeft + AxisWidth;
      LineTo(x, y);
      end;

    // X-axis
    Pen_Width := 2;
    MoveTo(cAxisLeft, IntHeight - cAxisBottom);
    LineTo(IntWidth - ds.AxisRight, IntHeight - cAxisBottom);
    end;
  end;

function TcdOperationStack.GetOldestOperationID: integer;
  begin
  if cdOperations.Count>0
     then result := cdOperations[0].Operation_ID
     else result := cNoID;
  end;

procedure TcdOperationStack.SelectOperation(aOperation: TcOperation);
  begin
  Selection.SelectOperation(aOperation);
  ResetBounds;
  end;

procedure TcdOperationStack.SelectOperation(aOperationID: integer);
  begin
  SelectOperation(Engine.Operations.FindByID(aOperationID));
  end;

procedure TcdOperationStack.UnselectOperation;
  begin
  SelectOperation(nil);
  end;


{ TcdOperation }

constructor TcdOperation.Create(aDiagram: TcdDiagram; aOperationStack: TcdOperationStack; aSeqNr: integer; aOperation: TcOperation);
  begin
  inherited Create(aDiagram, ooAuto);
  fSelectionGroup := 1;
  Clickable := True;

  fOperationStack := aOperationStack;
  fEngine := aOperation.Engine;
  fDiagramSettings := aOperationStack.ds;

  Operation_ID := aOperation.ID;
  Operation_SeqNr := aSeqNr;
  Operation_Name := aOperation.Name;
  end;

procedure TcdOperation.MouseDown(aButton: TMouseButton; aShiftState: TShiftState; aX, aY: Integer);
  begin
  OperationStack.SelectOperation(Operation_ID);
  if Assigned(gRadioStation)
     then gRadioStation.BroadCast(cRadioEvent_OperationStack_Operation_Selected, OperationStack);
  end;

procedure TcdOperation.ResetBounds_Internal;
  var
    lLeft, lRight, lTop, lBottom, lWidth, lHeight: integer;
    lOperation: TcOperation;
  begin
  inherited;

  lOperation := Engine.Operations.FindByID(Operation_ID);
  if not Assigned(lOperation) then exit;

  lBottom := OperationStack.GetYByPos(Operation_SeqNr);
  lTop := OperationStack.GetYByPos(Operation_SeqNr+1);
  lLeft := cAxisLeft;
  lRight := OperationStack.IntWidth - ds.AxisRight;

  lWidth := lRight - lLeft;
  lHeight := lBottom - lTop;

  SetIntBounds(OperationStack.Left + lLeft, OperationStack.Top + lTop, lWidth, lHeight);

  // Retrieve data required for painting
  Operation_Selected := OperationStack.Selection.OperationSelected(lOperation);
  Operation_Related := OperationStack.Selection.IsRelated(lOperation.ID);

  if Operation_Selected
     then begin
          // Right of Operation
          OperationStack.ConnectionPoint_SelectedClaim_Right := Point(IntLeft + IntWidth, IntTop + (IntHeight div 2));
          end;

  if Operation_SeqNr = 0
     then begin
          // Connection point: Bottom of stack
          OperationStack.ConnectionPoint_Stack_Bottom := Point(IntLeft + (IntWidth div 2), IntTop + IntHeight);
          end;
  end;

procedure TcdOperation.PaintOnCanvas (aCanvas: TCanvas);
  var
    x, y: integer;
    lStr, lStr1, lStr2: String;
  begin
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    Brush_Style := bsSolid;
    Pen_Style := psSolid;
    Pen_Width := 1;

    // https://docwiki.embarcadero.com/RADStudio/Alexandria/en/Colors_in_the_VCL
    if Operation_Selected
       then begin
            Brush_Color := clWebLightYellow;
            Pen_Color := clWebOrange;
            end
    else if Operation_Related
       then begin
            Brush_Color := clWebIvory;
            Pen_Color := clWebDarkGray;
            end
       else begin
            Brush_Color := clWebLavender;
            Pen_Color := clWebDarkGray;
            end;

    Rectangle(IntClientRect);

    Brush_Style := bsClear;
    Font_Height := ds.FontHeight_Tuple;
    Font_Style := [];
    Font_Name := ds.FontName_Tuple;
    lStr1 := Operation_Name;
    lStr2 := '  (' + cRefChar+IntToStr(Operation_ID) + ')';
    lStr := lStr1 + lStr2;
    x := (IntWidth div 2) - (TextWidth(lStr) div 2);
    y := (IntHeight div 2) - (TextHeight(lStr) div 2);

    Font_Color := clBlack;
    TextOut(x, y, lStr1);
    x := x + TextWidth(lStr1);
    Font_Color := clGray;
    TextOut(x, y, lStr2);
    end;
  end;

end.
