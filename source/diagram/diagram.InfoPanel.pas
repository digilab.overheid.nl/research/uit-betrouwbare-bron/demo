// -----------------------------------------------------------------------------
//
// diagram.InfoPanel
//
// -----------------------------------------------------------------------------
//
// Visualisation of a panel with information about an operation, claim, etc.
// Used in ace.Forms.ClaimSet.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit diagram.InfoPanel;

{$MODE Delphi}

interface

uses
  Classes, Types, Graphics,
  diagram.Core, diagram.Settings, ace.Core.Engine, ace.Core.Query;

const
  cHorizontalMargin = 10;
  cVerticalMargin = 6;
  cScrollBarWidth = 20;

type
  TcInfoPanelKind = (ipkNone, ipkOperation, ipkQuery, ipkClaim);

  TcdInfoPanel =
    class(TcdObject)
      private
        fEngine: TcEngine;
        fSession: TcSession;
        fDiagramSettings: TcDiagramSettings;
        fShowTemporalAnnotations: boolean;
        fKind: TcInfoPanelKind;
        fMaxWidth: integer;
        fAutoIndent: boolean;
        fConnectionPoint: TPoint;

        // Data cached for fast repainting
        fLines: TStringlist;
        fPaintLines: TStringlist;
        fLineHeight: integer;

      protected
        procedure ResetBounds_Internal; override;

      public
        constructor Create(aDiagram: TcdDiagram; aSession: TcSession); reintroduce;
        destructor Destroy; override;

        procedure Init(aOperation: TcOperation); overload;
        procedure Init(aQuery: TcTemporalQuery; aMaxWidth: integer); overload;
        procedure Init(aClaim: TcClaim; aMaxWidth: integer); overload;

        procedure PaintOnCanvas(aCanvas: TCanvas); override;

        property Engine: TcEngine read fEngine;
        property Session: TcSession read fSession;
        property ds: TcDiagramSettings read fDiagramSettings;
        property ShowTemporalAnnotations: boolean read fShowTemporalAnnotations write fShowTemporalAnnotations;
        property Kind: TcInfoPanelKind read fKind;
        property ConnectionPoint: TPoint read fConnectionPoint write fConnectionPoint;
      end;

implementation

uses
  SysUtils,
  common.Utils, common.RadioStation, common.ErrorHandling;


{ TcdInfoPanel }

constructor TcdInfoPanel.Create(aDiagram: TcdDiagram; aSession: TcSession);
  begin
  inherited Create(aDiagram, ooUpperLeft);
  fEngine := aSession.Engine;
  fSession := aSession;
  fDiagramSettings := gDiagramSettings;
  fLines := TStringlist.Create;
  fPaintLines := TStringlist.Create;
  fShowTemporalAnnotations := false;
  end;

destructor TcdInfoPanel.Destroy;
  begin
  fLines.Free;
  fPaintLines.Free;
  inherited
  end;

procedure TcdInfoPanel.ResetBounds_Internal;
  var
    i, w: integer;
    lTextWidth: integer;
    lLine, lNewLine: string;
    lWords: TStringlist;
    lMaxWidth, lWidthRequired: integer;
    lIndented: boolean;
  begin
  lMaxWidth := 0;
  case fKind of
    ipkOperation:
       lMaxWidth := ScrollBox.Width - cScrollBarWidth - IntLeft - (cHorizontalMargin * 2);
    ipkQuery: lMaxWidth := fMaxWidth;
    ipkClaim: lMaxWidth := fMaxWidth;
    else EInt('TcdInfoPanel.ResetBounds_Internal', 'Unknown Info Panel Kind');
    end;
  lWidthRequired := 0;

  fPaintLines.Clear;
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := ScrollBox.Diagram.ScaleCanvas.Canvas;
    Font_Height := ds.FontHeight_Info;
    Font_Style := [];
    Font_Name := ds.FontName_Info;

    fLineHeight := 22;
    for i := 0 to fLines.Count-1 do
      begin
      lLine := fLines[i];

      lIndented := (lLine<>'') and (lLine[1]='>');
      if lIndented
         then lLine := '  ' + Copy(lLine, 2, Length(lLine));

      lTextWidth := TextWidth(lLine);
      if lTextWidth > lWidthRequired
         then lWidthRequired := lTextWidth;

      if lTextWidth < lMaxWidth
         then fPaintLines.Add(lLine)
         else begin
              lWords := StringToStringlist(Trim(lLine), ' ');
              if lIndented then lLine := ' ' else lLine := '';
              for w := 0 to lWords.Count-1 do
                begin
                lNewLine := lLine + ' ' + lWords[w];

                if (Trim(lLine)<>'') and (TextWidth(lNewLine) > lMaxWidth)
                   then begin
                        fPaintLines.Add(lLine);
                        if fAutoIndent
                           then begin
                                if lIndented
                                   then lLine := '    ' + lWords[w]
                                   else lLine := '  ' + lWords[w];
                                end
                           else lLine := ' ' + lWords[w];
                        end
                   else begin
                        // Accept the NewLine, and continue to add words
                        lLine := lNewLine;

                        // Note: If the Line is empty the newline is allways
                        // excepted regardless of the length, this is to prevent
                        // an infinite loop when words are longer that the with.
                        end;
                end;
              if lLine<>''
                 then fPaintLines.Add(lLine);
              end;
      end;
    end;

  if lMaxWidth > (lWidthRequired + (cHorizontalMargin * 2))
     then lMaxWidth := lWidthRequired + (cHorizontalMargin * 2);

  SetIntBounds(
    IntLeft, IntTop,
    lMaxWidth,
    (fPaintLines.Count * fLineHeight) + (cVerticalMargin * 2)
  );

  if (fKind = ipkQuery) or (fKind = ipkClaim)
     then begin
          // Connection point on the top, a bit to the left
          ConnectionPoint := Point(IntLeft + 20, IntTop);
          end
  else if fKind = ipkOperation
     then begin
          // Connection point on the left, a bit lower than the top
          ConnectionPoint := Point(IntLeft, IntTop + 20);
          end
     else EInt('TcdInfoPanel.ResetBounds_Internal', 'Unexpected kind');
  end;

procedure TcdInfoPanel.PaintOnCanvas(aCanvas: TCanvas);
  var
    i, x, y: integer;
    lStr: string;
  begin
  with ScrollBox.Diagram.ScaleCanvas do
    begin
    Canvas := aCanvas;

    // https://docwiki.embarcadero.com/RADStudio/Alexandria/en/Colors_in_the_VCL
    Brush_Style := bsSolid;
    Brush_Color := clWebIvory;
    Pen_Style := psSolid;
    Pen_Color := clWebDarkGray;
    Pen_Width := 1;
    Rectangle(IntClientRect);

    Brush_Style := bsClear;
    Font_Color := clBlack;
    Font_Height := ds.FontHeight_Info;
    Font_Style := [];
    Font_Name := ds.FontName_Info;
    x := cHorizontalMargin;
    y := cVerticalMargin;
    for i := 0 to fPaintLines.Count-1 do
      begin
      lStr := fPaintLines[i];
      if (lStr<>'') and (lStr[1]<>' ')
         then Font_Style := [fsItalic]
         else Font_Style := [];
      TextOut(x, y, lStr);
      y := y + fLineHeight;
      end;
    end;
  end;

procedure TcdInfoPanel.Init(aOperation: TcOperation);
  var
    i: integer;
    lClaim: TcClaim;
    lPreviousLinesCount: integer;
  begin
  fKind := ipkOperation;
  fAutoIndent := true;

  // Meta Claims
  fLines.Add('Meta Claims:');
  lPreviousLinesCount := fLines.Count;
  for i := 0 to aOperation.Claims.Count-1 do
    begin
    lClaim := aOperation.Claims[i];
    if not Assigned(Engine.OperationClaimTypes.Find(lClaim.ClaimType.Name)) then continue;
    fLines.Add(Format('>%s (%s: %s%d)', [
      lClaim.GetExpressionFmt,
      lClaim.ClaimType.Name,
      cRefChar, lClaim.ID
    ]));
    end;
  if lPreviousLinesCount = fLines.Count
     then begin
          // There were no meta claims, remove header
          fLines.Delete(fLines.Count-1);
          end
     else fLines.Add('');

  // Claims & Annotations
  fLines.Add('Claims & Annotations:');
  lPreviousLinesCount := fLines.Count;
  for i := 0 to aOperation.Claims.Count-1 do
    begin
    lClaim := aOperation.Claims[i];
    if Assigned(Engine.OperationClaimTypes.Find(lClaim.ClaimType.Name)) then continue;
    fLines.Add(Format('>%s (%s: %s%d)', [
      lClaim.GetExpressionFmt,
      lClaim.ClaimType.Name,
      cRefChar, lClaim.ID
    ]));
    end;
  if lPreviousLinesCount = fLines.Count
     then begin
          // There were no claims, remove header
          fLines.Delete(fLines.Count-1);
          end;

  // Could end up with an empty line at the end, if so remove it
  if (fLines.Count>0) and (fLines[fLines.Count-1]='')
     then fLines.Delete(fLines.Count-1);
  end;

procedure TcdInfoPanel.Init(aQuery: TcTemporalQuery; aMaxWidth: integer);
  begin
  fKind := ipkQuery;
  fAutoIndent := false;
  fMaxWidth := aMaxWidth;

  fLines.Add(aQuery.Expression);
  end;

procedure TcdInfoPanel.Init(aClaim: TcClaim; aMaxWidth: integer);
  var
    i: integer;
    lAnnotation: TcAnnotation;
    lPreviousLinesCount: integer;
  begin
  fKind := ipkClaim;
  fAutoIndent := true;
  fMaxWidth := aMaxWidth;

  fLines.Add(aClaim.GetExpressionFmt);
  fLines.Add(aClaim.GetTimeAspects);
  if aClaim.ID <> aClaim.SourceClaim
     then fLines.Add(Format('Source claim: %s%d', [cRefChar, aClaim.SourceClaim]));

  if aClaim.Annotations.Count > 0
     then begin
          fLines.Add('');
          fLines.Add('Annotations');
          lPreviousLinesCount := fLines.Count;
          for i := 0 to aClaim.Annotations.Count-1 do
            begin
            lAnnotation := aClaim.Annotations[i];
            if (lAnnotation.Purpose = cpTemporalAnnotation) and not ShowTemporalAnnotations then continue;

            fLines.Add(Format('  %s (%s%d - %s)',
              [lAnnotation.GetExpressionFmt, cRefChar, lAnnotation.ID, lAnnotation.AnnotationType.KindAsString]));
            fLines.Add('    '+lAnnotation.GetTimeAspects);
            if lAnnotation.DerivedFrom <> cNoID
               then fLines.Add(Format('    Derived from: %d', [lAnnotation.DerivedFrom]));
            end;

          if lPreviousLinesCount = fLines.Count
             then begin
                  // There were no claims, remove header
                  fLines.Delete(fLines.Count-1);
                  fLines.Delete(fLines.Count-1);
                  end;
          end;
  end;

end.
