// -----------------------------------------------------------------------------
//
// diagram.Selection
//
// -----------------------------------------------------------------------------
//
// Class to manage selected items in a diagram. Used in ace.Forms.ClaimSet.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit diagram.Selection;

{$MODE Delphi}

interface

uses
  SysUtils, Classes,
  common.SimpleLists,
  ace.Core.Engine;

type
  TcSelectedKind = (skNone, skOperation, skSourceClaim, skDerivedClaim, skAnnotation);

  TcSelection =
     class
       private
         fEngine: TcEngine;
         fID: integer;
         fKind: TcSelectedKind;
         fRelatedIDs: TcIntegerList;

       protected
         function GetID: integer;
         function GetKind: TcSelectedKind;
         function GetOperation: TcOperation;
         function GetClaim(aKind: TcSelectedKind): TcClaim;
         function GetSourceClaim: TcClaim;
         function GetDerivedClaim: TcClaim;

       public
         constructor Create(aEngine: TcEngine);
         destructor Destroy; override;

         procedure Clear;

         procedure SelectOperation(aOperation: TcOperation);
         function OperationSelected(aOperation: TcOperation): boolean;

         procedure SelectSourceClaim(aSourceClaim: TcClaim);
         function SourceClaimSelected(aSourceClaim: TcClaim): boolean;

         procedure SelectDerivedClaim(aDerivedClaim: TcClaim);
         function DerivedClaimSelected(aDerivedClaim: TcClaim): boolean;

         procedure SelectAnnotation(aAnnotation: TcAnnotation);
         function AnnotationSelected(aAnnotation: TcAnnotation): boolean;

         function IsRelated(aElementID: integer): boolean;

         property Engine: TcEngine read fEngine;
         property ID: integer read GetID;
         property Kind: TcSelectedKind read GetKind;
         property RelatedIDs: TcIntegerList read fRelatedIDs;

         // Selected objects (mutually exclusive, see kind)
         property Operation: TcOperation read GetOperation;
         property SourceClaim: TcClaim read GetSourceClaim;
         property DerivedClaim: TcClaim read GetDerivedClaim;
       end;

implementation

{ TcSelection }

constructor TcSelection.Create(aEngine: TcEngine);
  begin
  inherited Create;
  fEngine := aEngine;
  fRelatedIDs := TcIntegerList.Create;
  fRelatedIDs.Sorted := true;
  fRelatedIDs.IgnoreDuplicates := true;
  end;

destructor TcSelection.Destroy;
  begin
  FreeAndNil(fRelatedIDs);
  inherited;
  end;

procedure TcSelection.Clear;
  begin
  fID := cNoID;
  fKind := skNone;
  fRelatedIDs.Clear;
  end;

function TcSelection.GetID: integer;
  var
    lElement: TcElement;
  begin
  result := cNoID;
  lElement := Engine.Elements.Find(fID);
  if Assigned(lElement)
     then result := fID
     else Clear;
  end;

function TcSelection.GetKind: TcSelectedKind;
  begin
  result := skNone;
  if GetID <> cNoID
     then result := fKind;
  end;

function TcSelection.GetOperation: TcOperation;
  begin
  result := nil;
  if fKind <> skOperation then exit;

  result := Engine.Operations.FindByID(fID);
  if not Assigned(result) then Clear;
  end;

function TcSelection.GetClaim(aKind: TcSelectedKind): TcClaim;
  var
    lElement: TcElement;
  begin
  result := nil;
  if fKind <> aKind then exit;

  lElement := Engine.Elements.Find(fID);
  if not Assigned(lElement)
     then begin
          Clear;
          exit;
          end;
  if not (lElement is TcClaim)
     then begin
          Clear;
          exit;
          end;
  result := lElement as TcClaim;
  end;

function TcSelection.GetSourceClaim: TcClaim;
  begin
  result := GetClaim(skSourceClaim);
  end;

function TcSelection.GetDerivedClaim: TcClaim;
  begin
  result := GetClaim(skDerivedClaim);
  end;

procedure TcSelection.SelectOperation(aOperation: TcOperation);
  var
    i: integer;
    lClaim: TcClaim;
  begin
  Clear;
  if Assigned(aOperation)
     then begin
          fID := aOperation.ID;
          fKind := skOperation;
          fRelatedIDs.Clear;

          for i := 0 to aOperation.Claims.Count-1 do
            begin
            lClaim := aOperation.Claims.Objects[i];
            RelatedIDs.Add(lClaim.ID);
            end;
          end;
  end;

function TcSelection.OperationSelected(aOperation: TcOperation): boolean;
  var
    lOperation: TcOperation;
  begin
  lOperation := GetOperation;
  result :=
    (Kind = skOperation) and
    Assigned(lOperation) and
    (lOperation = aOperation);
  end;

procedure TcSelection.SelectSourceClaim(aSourceClaim: TcClaim);
  begin
  Clear;
  if Assigned(aSourceClaim)
     then begin
          fID := aSourceClaim.ID;
          fKind := skSourceClaim;
          fRelatedIDs.Clear;

          RelatedIDs.Add(aSourceClaim.ID); // Used to find related derived claims
          if Assigned(aSourceClaim.Operation)
             then RelatedIDs.Add(aSourceClaim.Operation.ID);
          end;
  end;

function TcSelection.SourceClaimSelected(aSourceClaim: TcClaim): boolean;
  var
    lSourceClaim: TcClaim;
  begin
  lSourceClaim := GetSourceClaim;
  result :=
    (Kind = skSourceClaim) and
    Assigned(lSourceClaim) and
    (lSourceClaim = aSourceClaim);
  end;

procedure TcSelection.SelectDerivedClaim(aDerivedClaim: TcClaim);
  var
    lElement: TcElement;
    lSourceClaim: TcClaim;
  begin
  Clear;
  if Assigned(aDerivedClaim)
     then begin
          fID := aDerivedClaim.ID;
          fKind := skDerivedClaim;
          fRelatedIDs.Clear;

          RelatedIDs.Add(aDerivedClaim.SourceClaim);
          lElement := Engine.Elements.Find(aDerivedClaim.SourceClaim);
          if Assigned(lElement) and (lElement is TcClaim)
             then begin
                  lSourceClaim := lElement as TcClaim;
                  if Assigned(lSourceClaim.Operation)
                     then RelatedIDs.Add(lSourceClaim.Operation.ID);
                  end;
          end;
  end;

function TcSelection.DerivedClaimSelected(aDerivedClaim: TcClaim): boolean;
  var
    lDerivedClaim: TcClaim;
  begin
  lDerivedClaim := GetDerivedClaim;
  result :=
    (Kind = skDerivedClaim) and
    Assigned(lDerivedClaim) and
    (lDerivedClaim = aDerivedClaim);
  end;

procedure TcSelection.SelectAnnotation(aAnnotation: TcAnnotation);
  begin
  Clear;
  if Assigned(aAnnotation)
     then begin
          fID := aAnnotation.ID;
          fKind := skAnnotation;
          fRelatedIDs.Clear;

          RelatedIDs.Add(aAnnotation.TransactionID);
          end;
  end;

function TcSelection.AnnotationSelected(aAnnotation: TcAnnotation): boolean;
  var
    lElement: TcElement;
  begin
  result := false;

  if fKind <> skAnnotation then exit;

  lElement := Engine.Elements.Find(fID);
  if not Assigned(lElement)
     then exit;
  if not (lElement is TcAnnotation)
     then exit;
  if (lElement as TcAnnotation) <> aAnnotation
     then exit;

  result := true;
  end;

function TcSelection.IsRelated(aElementID: integer): boolean;
  var
    lIndex: integer;
  begin
  result := RelatedIDs.Find(aElementID, lIndex);
  end;

end.
