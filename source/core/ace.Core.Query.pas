// -----------------------------------------------------------------------------
//
// ace.Core.Query
//
// -----------------------------------------------------------------------------
//
// Queries on claimsets (incl. time travel queries).
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV, VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Core.Query;

{$MODE Delphi}

interface

uses
  Classes, SysUtils,
  common.SimpleLists, common.RadioStation,
  ace.Core.Engine;

// Debug
const
  dtQueryMVCC = 'QueryMVCC';

type
  TcTimeFilter = (tfNone, tfMoment, tfPeriod);

  TcTemporalQuery =
    class
      private
        fSession: TcSession;
        fStableRead: boolean;

        fClaimSet: TcClaimSet;

        fFromTT,
        fUntilTT,
        fFromVT,
        fUntilVT: TcTime;

        fFilterTT,
        fFilterVT: TcTimeFilter;

        fItemsInTransaction: TcStringObjectList<TcClaim>; // ClaimID, Claim - Not owned, Sorted
        fItemsInResult: TcStringObjectList<TcClaim>; // ClaimID, Claim - Not owned, Sorted

        fRadioOnline: boolean;

        //function GetItem (const aIndex: integer): TcClaim;
        //function GetCount: integer;
        procedure DeriveTimeFilters;
        procedure SetFromTT(aTime: TcTime);
        procedure SetUntilTT(aTime: TcTime);
        procedure SetFromVT(aTime: TcTime);
        procedure SetUntilVT(aTime: TcTime);

        procedure iExecute;

      public
        constructor Create(aSession: TcSession);
        destructor Destroy; override;

        procedure AssignClaimSet(aClaimSet: TcClaimSet);

        procedure ListenToRadio (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);

        function Active: boolean;
        function Expression: string;

        procedure SetDefaultFilter(aVT: TcTime = cNoTime; aTT: TcTime = cNoTime);
        procedure Execute;

        function InTransaction(aClaim: TcClaim): boolean;
        function InResult(aClaim: TcClaim): boolean;

        property FromTT: TcTime read fFromTT write SetFromTT;
        property UntilTT: TcTime read fUntilTT write SetUntilTT;
        property FromVT: TcTime read fFromVT write SetFromVT;
        property UntilVT: TcTime read fUntilVT write SetUntilVT;

        property FilterTT: TcTimeFilter read fFilterTT;
        property FilterVT: TcTimeFilter read fFilterVT;

        property Session: TcSession read fSession;
        property ClaimSet: TcClaimSet read fClaimSet;

        property ItemsInTransaction: TcStringObjectList<TcClaim> read fItemsInTransaction;
        property ItemsInResult: TcStringObjectList<TcClaim> read fItemsInResult;
      end;


implementation

uses
  {System.SysUtils,}
  common.ErrorHandling, common.Utils,
  ace.Debug;

{ TcTemporalQuery }

constructor TcTemporalQuery.Create(aSession: TcSession);
  begin
  inherited Create;
  fSession := aSession;
  if not Assigned(fSession) then EInt('TcTemporalQuery.Create', 'Session should be assigned');
  fStableRead := false;

  fClaimSet := nil;
  fFromTT := cNoTime;
  fUntilTT := cNoTime;
  fFromVT := cNoTime;
  fUntilVT := cNoTime;
  fItemsInTransaction := TcStringObjectList<TcClaim>.Create(false {Not owned}, true {Sorted});
  fItemsInResult := TcStringObjectList<TcClaim>.Create(false {Not owned}, true {Sorted});

  gRadioStation.AddRadio(Self, ListenToRadio);
  fRadioOnline := true;
  end;

destructor TcTemporalQuery.Destroy;
  begin
  if fRadioOnline
    then gRadioStation.RemoveRadio(Self);

  FreeAndNil(fItemsInTransaction);
  FreeAndNil(fItemsInResult);
  inherited;
  end;

procedure TcTemporalQuery.AssignClaimSet(aClaimSet: TcClaimSet);
  begin
  fClaimSet := aClaimSet;
  end;

procedure TcTemporalQuery.DeriveTimeFilters;
  begin
  if (FromTT = cNoTime) or (UntilTT = cNoTime)
     then fFilterTT := tfNone
  else if (FromTT = UntilTT)
     then fFilterTT := tfMoment
  else fFilterTT := tfPeriod;

  if (FromVT = cNoTime) or (UntilVT = cNoTime)
     then fFilterVT := tfNone
  else if (FromVT = UntilVT)
     then fFilterVT := tfMoment
  else fFilterVT := tfPeriod;
  end;

procedure TcTemporalQuery.SetFromTT(aTime: TcTime);
  begin
  fFromTT := aTime;
  DeriveTimeFilters;
  end;

procedure TcTemporalQuery.SetUntilTT(aTime: TcTime);
  begin
  fUntilTT := aTime;
  DeriveTimeFilters;
  end;

procedure TcTemporalQuery.SetFromVT(aTime: TcTime);
  begin
  fFromVT := aTime;
  DeriveTimeFilters;
  end;

procedure TcTemporalQuery.SetUntilVT(aTime: TcTime);
  begin
  fUntilVT := aTime;
  DeriveTimeFilters;
  end;

procedure TcTemporalQuery.ListenToRadio (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);
  var
    lClaimSet: TcClaimSet;
  begin
  if not Assigned(fClaimSet) then exit;

  if aRadioEvent = cRadioEvent_Offline
    then begin
         fRadioOnline := false;
         exit;
         end;

  if aRadioEvent <> cRadioEvent_ClaimSet_Changed
    then exit;

  lClaimSet := TcClaimSet(aData);
  if not Assigned(lClaimSet)
    then exit;

  if lClaimSet.ID<>ClaimSet.ID
    then exit; // Another claimset

  Debug(dtEventHandling, '  - TemporalQuery.Execute');
  Execute;
  end;

function TcTemporalQuery.Active: boolean;
  begin
  result := Assigned(fClaimSet) and (FilterTT <> tfNone) or (FilterVT <> tfNone);
  end;

function TcTemporalQuery.Expression: string;
  begin
  result := '';
  if not Active then exit;

  if (FilterTT = tfMoment) and (FilterVT = tfMoment) // Specific moment in time
     then result := Format('Which claim was valid at T%d with the knownledge of T%d?', [FromVT, FromTT])

  else if (FilterTT = tfNone) and (FilterVT = tfMoment) // Valid line
     then result := Format('Which claims were registered at T%d?', [FromVT])

  else if (FilterTT = tfNone) and (FilterVT = tfPeriod) // Valid period
     then result := Format('Which claims were registered from T%d until T%d?', [FromVT, UntilVT])

  else if (FilterTT = tfMoment) and (FilterVT = tfNone) // Transaction line
     then result := Format('What where the current claims at T%d?', [FromTT])

  else if (FilterTT = tfPeriod) and (FilterVT = tfNone) // Transaction period
     then result := Format('What where the current claims from T%d until T%d?', [FromTT, UntilTT])

  else if (FilterTT = tfMoment) and (FilterVT = tfPeriod) // Transaction moment, Valid period
     then result := Format('Which claims were registered from T%d until T%d, ' +
       'with the knowledge of T%d?', [FromVT, UntilVT, FromTT])

  else if (FilterTT = tfPeriod) and (FilterVT = tfMoment) // Transaction period, Valid moment
     then result := Format('Which claims were registered at T%d, ' +
       'with the knowledge from T%d until T%d?', [FromVT, FromTT, UntilTT])

  else // Both are periods
    result := Format('What where the current claims from T%d until T%d, ' +
      'with the knowledge from T%d until T%d?', [FromVT, UntilVT, FromTT, UntilTT]);
  end;

procedure TcTemporalQuery.SetDefaultFilter(aVT: TcTime = cNoTime; aTT: TcTime = cNoTime);
  begin
  if aVT = cNoTime then aVT := Session.Engine.SystemTime;
  if aTT = cNoTIme then aTT := Session.Engine.SystemTime;

  FromTT  := aTT;
  UntilTT := aTT;
  FromVT  := aVT;
  UntilVT := aVT;
  end;

procedure TcTemporalQuery.Execute;
  var
    lForceTransaction: boolean;
  begin
  lForceTransaction := not Assigned(Session.Transaction);

  if lForceTransaction
     then Session.StartTransaction(true {Implicit}, true {Readonly}, false {no StableRead});

  fStableRead := Session.Transaction.StableRead;

  iExecute;

  if lForceTransaction
     then Session.Commit;
  end;

procedure TcTemporalQuery.iExecute;

  function MomentInClaim(aClaimFrom, aClaimUntil, aMoment: TcTime): boolean;
    begin
    result :=
      (aMoment >= aClaimFrom)
      and
      (
        (aMoment < aClaimUntil)
        or
        ( (aMoment = cEndOfTime) and (aClaimUntil = cEndOfTime) )
      );
    end;

  function PeriodOverlapsWithClaim(aClaimFrom, aClaimUntil, aQueryFrom, aQueryUntil: TcTime): boolean;
    var
      lCompleteOverlap, lStartWithinPeriod, lEndWithinPeriod: boolean;
    begin
    lCompleteOverlap := (aClaimFrom <= aQueryFrom) and (aClaimUntil >= aQueryUntil);

    lStartWithinPeriod :=
      (
        (aClaimFrom >= aQueryFrom)
        and
        (
          (aClaimFrom < aQueryUntil)
          or
          ( (aClaimFrom = cStartOfTime) and (aQueryUntil = cStartOfTime) )
        )
      );

    lEndWithinPeriod :=
      (
        (
          (aClaimUntil > aQueryFrom)
          or
          ( (aClaimUntil = cEndOfTime) and (aQueryUntil = cEndOfTime) )
        )
        and
        (aClaimUntil <= aQueryUntil)
      );

    result := lCompleteOverlap or lStartWithinPeriod or lEndWithinPeriod;
    end;

  function MVCCVersionPresent(aMVCCVersionID: integer): boolean;
    var
      i: integer;
      lClaim: TcClaim;
    begin
    result := true;
    for i := fItemsInTransaction.Count-1 downto 0 do // More efficient
      begin
      lClaim := fItemsInTransaction.Objects[i];
      if lClaim.MVCCVersionID = aMVCCVersionID
         then exit;
      end;
    result := false;
    end;

  function VisibleInTransaction(aClaim: TcClaim): boolean;
    var
      lTransaction: TcTransaction;
    begin
    result := false;

    Debug(dtQueryMVCC, 'Claim %d', [aClaim.ID]);

    // Is the original claim already present?
    Debug(dtQueryMVCC, '  MVCC Version ID %d', [aClaim.MVCCVersionID]);
    if MVCCVersionPresent(aClaim.MVCCVersionID)
       then begin
            // Keep at most one version of each MVCC version
            Debug(dtQueryMVCC, '    MVCC version already present');
            Debug(dtQueryMVCC, '  Conclusion: Invisible to Query');
            exit;
            end;

    // Is this claim marked for cleaning by this session's active transaction?
    Debug(dtQueryMVCC, '  MarkedForCleaningBy %d', [aClaim.MVCCMarkedForCleaningBy]);
    if aClaim.MVCCMarkedForCleaningBy = Session.Transaction.ID
       then begin
            Debug(dtQueryMVCC, '    MarkedForCleaning by this session''s transaction');
            Debug(dtQueryMVCC, '  Conclusion: Invisible to Query');
            exit;
            end;

    lTransaction := Session.Engine.Transactions.FindByID(aClaim.TransactionID);
    if not Assigned(lTransaction)
       then EInt('TcTemporalQuery.iExecute.VisibleInTransaction', 'Claim.CreatedBy has invalid reference.');

    // Is this claim created by this session's active transaction?
    Debug(dtQueryMVCC, '  Claim.CreatedBy %d', [aClaim.TransactionID]);
    Debug(dtQueryMVCC, '  CreatedBy -> Transaction %d', [lTransaction.ID]);
    if lTransaction.ID = Session.Transaction.ID
       then begin
            Debug(dtQueryMVCC, '    Created by this sessions''s transaction');
            // (and not deleted within session, otherwise it would have been marked for deletion)
            Debug(dtQueryMVCC, '  Conclusion: Visible to Query');
            result := true;
            exit;
            end;

    // Is this claim created by (another) transaction that is still running?
    Debug(dtQueryMVCC, '  Claim.CreatedBy Transaction is still active: %s', [BooleanToYesNo(lTransaction.State = tsActive)]);
    if lTransaction.State = tsActive
       then begin
            Debug(dtQueryMVCC, '    CreatedBy a transaction that is still active.');
            Debug(dtQueryMVCC, '  Conclusion: Invisible to Query');
            exit;
            end;

    // Is this claim created by (another) transaction that has ended after the start of the running transaction?
    // (No need to check if the transaction ended. Already covered by previous check.)
    Debug(dtQueryMVCC, '  Claim.CreatedBy Transaction.EndID %d - Session.Transaction.StartID', [lTransaction.EndID, Session.Transaction.StartID]);
    if (lTransaction.EndID > Session.Transaction.StartID)
       then begin
            Debug(dtQueryMVCC, '    CreatedBy a transaction that ended after the session''s transaction started.');
            Debug(dtQueryMVCC, '  Conclusion: Invisible to Query');
            exit;
            end;

    // Is a StableRead required and is this claim created by a transaction that ended after the StableReadSnapshotID?
    // Additional filters for stable reads
    Debug(dtQueryMVCC, '  StableRead requested?: %s', [BooleanToYesNo(Session.Transaction.StableRead)]);
    if Session.Transaction.StableRead and (lTransaction.EndID > Session.Transaction.StableReadSnapshotID)
       then begin
            Debug(dtQueryMVCC, '    CreatedBy a transaction that ended after the StableReadSnapshotID.');
            Debug(dtQueryMVCC, '  Conclusion: Invisible to Query');
            exit;
            end;

    // Claim has met all conditions, include in result set
    Debug(dtQueryMVCC, '  Conclusion: Visible to Query');
    result := true
    end;

  var
    i: integer;
    lClaim: TcClaim;
    lVisibleInTransaction: boolean;
  begin
  fItemsInTransaction.Clear;
  fItemsInResult.Clear;

  if not Assigned(fClaimSet) then exit;

  Debug(dtQueryMVCC, '---');
  Debug(dtQueryMVCC, '');
  Debug(dtQueryMVCC, 'Session %d', [Session.ID]);
  Debug(dtQueryMVCC, '  Transaction %d', [Session.Transaction.ID]);

  // Filter Claims
  for i := ClaimSet.DerivedClaims.Count-1 downto 0 do // Iterate list backwards: Starting with most recent claims
    begin
    lClaim := ClaimSet.DerivedClaims[i];

    // Claim visible for current transaction?
    lVisibleInTransaction := VisibleInTransaction(lClaim);

    if lVisibleInTransaction
       then begin
            fItemsInTransaction.Add(IntToStr(lClaim.ID), lClaim);

            if
               // Claim visible given query parameters?
               (
                 (FilterTT = tfNone) or
                 ( (FilterTT = tfMoment) and MomentInClaim(lClaim.RegisteredAt, lClaim.ExpiredAt, FromTT) ) or
                 ( (FilterTT = tfPeriod) and PeriodOverlapsWithClaim(lClaim.RegisteredAt, lClaim.ExpiredAt, FromTT, UntilTT) )
               )
               and
               (
                 (FilterVT = tfNone) or
                 ( (FilterVT = tfMoment) and MomentInClaim(lClaim.ValidFrom, lClaim.ValidUntil, FromVT) ) or
                 ( (FilterVT = tfPeriod) and PeriodOverlapsWithClaim(lClaim.ValidFrom, lClaim.ValidUntil, FromVT, UntilVT) )
               )

               then fItemsInResult.Add(IntToStr(lClaim.ID), lClaim);
            end;
    end;

  Debug(dtQueryMVCC, '');
  end;

function TcTemporalQuery.InTransaction(aClaim: TcClaim): boolean;
  var
    lIndex: integer;
  begin
  if Assigned(fClaimSet)
     then result := fItemsInTransaction.Find(IntToStr(aClaim.ID), lIndex)
     else result := true;
  end;

function TcTemporalQuery.InResult(aClaim: TcClaim): boolean;
  var
    lIndex: integer;
  begin
  if Assigned(fClaimSet)
     then result := fItemsInResult.Find(IntToStr(aClaim.ID), lIndex)
     else result := true;
  end;

end.
