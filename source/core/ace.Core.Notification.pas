// -----------------------------------------------------------------------------
//
// ace.Core.Notification
//
// -----------------------------------------------------------------------------
//
// Routines to broadcast a CloudEvent.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Core.Notification;

{$MODE Delphi}

interface

uses
  common.Json, fpjson,
  Classes,
  ace.Core.Engine;

const
  cNotificationHubUrl = 'http://127.0.0.1:4300/Notification';

// Create CloudEvent and BroadCast
procedure BroadCastCloudEvent(aTypeStr: string; aRegisteredAt: TcTime; aObjectName, aObjectKey: string); overload;
procedure BroadCastCloudEvent(aCloudEventJson: TJsonObject); overload;

implementation

uses
  SysUtils,
  IdBaseComponent, IdComponent, IdCustomHTTPServer,
  IdHTTP, IdUri;

// -- TcHttpPostThread

type
  TcHttpPostThread =
    class(TThread)
      private
        fURL: String;
        fContent: TStream;
        fResponseText: String;
        fCallback: TGetStrProc;

      protected
        procedure Execute; override;
        procedure Callback;

      public
        constructor Create(aURL: String; aContent: TStream; aCallback: TGetStrProc);
        destructor Destroy; override;
      end;

constructor TcHttpPostThread.Create(aURL: String; aContent: TStream; aCallback: TGetStrProc);
  begin
  inherited Create(False);
  FreeOnTerminate := True;
  fURL := aURL;
  fContent := TMemoryStream.Create;
  fContent.CopyFrom(aContent, 0);
  fCallback := aCallback;
  end;

destructor TcHttpPostThread.Destroy;
  begin
  fContent.Free;
  inherited;
  end;

procedure TcHttpPostThread.Execute;
  var
    lHttp: TIdHttp;
  begin
  lHttp := TIdHttp.Create(nil);
  try
    lHttp.Request.ContentType := 'application/json';
    lHttp.Request.ContentEncoding := 'utf-8';
    lHttp.Post(fUrl, fContent);
    fResponseText := lHttp.ResponseText;
  finally
    lHttp.Free;
    end;
  Synchronize(Callback);
  end;

procedure TcHttpPostThread.Callback;
  begin
  if Assigned(fCallback)
    then fCallback(fResponseText);
  end;


procedure BroadCastCloudEvent(aTypeStr: string; aRegisteredAt: TcTime; aObjectName, aObjectKey: string);
  var
    jCloudEvent, jData: TJsonObject;
    lhResult: longint;
    lUUID: TGUID;
  begin
  lhResult := CreateGUID(lUUID);
  Assert(lhResult = s_ok);

  jCloudEvent := TJsonObject.Create;
  with jCloudEvent do
    begin
    AddPair('id', GUIDToString(lUUID));
    AddPair('source', 'urn:nld:oin:00000001821002193000:systeem:ACE-component');
    AddPair('specversion', TJsonIntegerNumber.Create(1));
    AddPair('type', aTypeStr);
    AddPair('time', TJsonIntegerNumber.Create(aRegisteredAt));
    AddPair('datacontenttype', 'application/json');
    jData := TJsonObject.Create;
    jData.AddPair(aObjectName, aObjectKey);
    AddPair('data', jData);
    end;

  BroadCastCloudEvent(jCloudEvent);
  end;

procedure BroadCastCloudEvent(aCloudEventJson: TJsonObject);
  var
    lRequestContent: TStringStream;
    lURL: string;
  begin
  lRequestContent := TStringStream.Create(aCloudEventJson.Format(2));
  try
    lURL := TIdURI.URLEncode(cNotificationHubUrl);
    TcHttpPostThread.Create(lUrl, lRequestContent, nil);
  finally
    lRequestContent.Free;
    end;
  end;

end.
