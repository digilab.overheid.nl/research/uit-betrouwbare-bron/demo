// -----------------------------------------------------------------------------
//
// ace.core.Tokenizer
//
// -----------------------------------------------------------------------------
//
// Tokenizer used to tokenize claimtype expressions.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV, VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Core.Tokenizer;

{$MODE Delphi}

interface

uses
  Classes;

const
  cRoleOpen = '<';
  cRoleClose = '>';

// Convert an expression template string to tokens.
procedure ExpressionTemplateStrToTokens (aTemplateStr: String; aTemplateAsTokens: TStringlist);

implementation

uses
  SysUtils,
  common.ErrorHandling;

const
  cNoChar = #0;

procedure ExpressionTemplateStrToTokens (aTemplateStr: String; aTemplateAsTokens: TStringlist);
  var
    i: integer;
    character: Char;
    inToken: boolean;
    bracket: char;
    token: string;
  begin
  // Replace escaped characters by used characters
  aTemplateStr := StringReplace(aTemplateStr, cRoleOpen+cRoleOpen, #1, [rfReplaceAll]);
  aTemplateStr := StringReplace(aTemplateStr, cRoleClose+cRoleClose, #2, [rfReplaceAll]);

  // Parse string
  i := 1;
  inToken := false;
  bracket := cNoChar;
  token := '';
  while i <= Length(aTemplateStr) do
    begin
    character := aTemplateStr[i];

    if not inToken // Not yet in token
      then begin
           inToken := true;

           if (character = cRoleOpen)
             then bracket := cRoleOpen;
           token := character;
           end

    else if bracket = cNoChar // In token without brackets
      then begin
           if (character = cRoleClose) // No closing bracket allowed in token without brackets
             then EUsr('Unexpected character %s at position %d.', [character, i+1]);

           if character = cRoleOpen
             then begin
                  bracket := cRoleOpen;
                  aTemplateAsTokens.Add(token); // Add token
                  token := cRoleOpen;
                  end
           else token := token + character; // Add character to token
           end

    else begin // In token with brackets
         if (character = cRoleOpen) // No opening bracket allowed in token with brackets
             then EUsr('Unexpected character %s at position %d.', [character, i+1]);

         if character = cRoleClose
           then begin
                inToken := false;
                bracket := cNoChar;
                token := token + character;
                aTemplateAsTokens.Add(token);
                token := '';
                end
           else token := token + character; // Add character to token
         end;

    inc(i);
    end;

  if inToken
    then begin
         if bracket <> cNoChar // Tokens with brackets should be closed properly
           then EUsr('Unexpected end of TemplateStr. Closing bracket ''%s'' expected.', [cRoleClose]);

         aTemplateAsTokens.Add(token);
         end;

  // Replace escaped characters
  for i := 0 to aTemplateAsTokens.Count-1 do
    begin
    aTemplateAsTokens[i] := StringReplace(aTemplateAsTokens[i], #1, cRoleOpen, [rfReplaceAll]);
    aTemplateAsTokens[i] := StringReplace(aTemplateAsTokens[i], #2, cRoleClose, [rfReplaceAll]);
    end;
  end;

end.
