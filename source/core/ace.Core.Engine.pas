// -----------------------------------------------------------------------------
//
// ace.Core.Engine
//
// -----------------------------------------------------------------------------
//
// The Atomic Claim Engine.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: Mattic BV, VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Core.Engine;

{$MODE Delphi}

interface

uses
  Classes, Generics.Collections, Graphics, common.SimpleLists;

// Debug
const
  dtTuples = 'Tuples';
  dtStableReadSnapshot = 'StableReadSnapshot';

// Events
const
  cRadioEvent_SystemTime_Changed = 'Core.SystemTime.Changed'; // Object = Engine

  cRadioEvent_LabelType_Created = 'Core.LabelType.Created'; // Object = LabelType
  cRadioEvent_ClaimType_Created = 'Core.ClaimType.Created'; // Object = ClaimType
  cRadioEvent_AnnotationType_Created = 'Core.AnnotationType.Created'; // Object = AnnotationType
  cRadioEvent_Class_Created = 'Core.Class.Created'; // Object = Class or nil ! when multiple classes were created

  cRadioEvent_Session_ClaimSet_Changed = 'Core.ClaimSet.Changed'; // Object = Session
  cRadioEvent_ClaimSet_Changed = 'Core.ClaimSet.Changed'; // Object = ClaimSet

  // ! No events are broadcasted for implicit, readonly events
  cRadioEvent_Transaction_Start = 'Core.Transaction.Start'; // Object = Session
  cRadioEvent_Transaction_BeforeEnd = 'Core.Transaction.BeforeEnd'; // Object = Session
  cRadioEvent_Transaction_AfterEnd = 'Core.Transaction.AfterEnd'; // Object = Session

// Time
type
  // Implemented as a simple integer
  TcTime = integer;
const
  cNoTime = -1;
  cStartOfTime = 0;
  cEndOfTime = MaxInt;

// Colors
const
  clTimeColor = $00D7FF; //clWebGold;

// Engine
const
  cEngine_DefaultName = 'default';

// Session
var
  gSessionSeqNr : Integer = 0;

// IDs
const
  cNoID = 0;

// Prefixes
const
  cRefChar = '#';
  cMetaChar = '$';

// Identity
const
  cIdentityValueSeparator = '·'; // #250

// Exposed meta structure names
const
  cMetaLabelType_ID = cMetaChar+'id';

  cMetaClaimType_Claim = cMetaChar+'claim';
  cMetaClaimType_Operation = cMetaChar+'operation';
  cMetaClaimType_OperationClaim = cMetaChar+'operation/claim';

  cMetaAnnotationType_Started = cMetaChar+'started';
  cMetaAnnotationType_Ended = cMetaChar+'ended';
  cMetaAnnotationType_Expired = cMetaChar+'expired';
  cMetaAnnotationType_Deleted = cMetaChar+'deleted';

  // !! Add cMetaRoleNames to the stringlist in the initialization !!
  cMetaRoleName_ID = cMetaChar+'id';
  cMetaRoleName_Claim = cMetaChar+'claim';
  cMetaRoleName_Operation = cMetaChar+'operation';

  cMetaValueName_CurrentOperation = cMetaChar+'operation';
  cMetaValueName_Ask = cMetaChar+'ask'; // Asks the value during script execution

var
  gMetaRoleNames: TStringlist;

type
  // -- Enumerations
  TcAnnotationKind = (akNone, akInfo, akDoubt, akStarted, akEnded, akExpired, akDeleted);
  TcExpressionTemplateType = (ettFull, ettNested);
  TcMoSCoW = (mUnknown, mCouldHave, mShouldHave, mMustHave);
  TcTransactionKind = (tkExplicit, tkImplicit);
  TcTransactionState = (tsActive, tsSuccess, tsFailed);

  // -- Forward declarations

  // Types
  TcType = class;
  TcDataType = class;
  TcLabelType = class;
  TcCompositeType = class;
  TcOperationType = class;
  TcRelationType = class;
  TcClaimType = class;
  TcAnnotationType = class;
  TcClass = class;

  // Internal structures for types
  TcRole = class;
  TcRoles = class;
  TcUnicity = class;
  TcIdentity = class;
  TcExpressionTemplate = class;

  // Instances and internal structures
  TcInstance = class;
  TcOperation = class;
  TcTuple = class;
  TcClaim = class;
  TcAnnotation = class;
  TcObject = class;

  // TupleSet, TupleSetList and internal structures
  TcTupleSet = class;
  TcClaimSet = class;
  TcClaimSetAction = class;
  TcObjectSet = class;
  TcClaimSetList = class;
  TcObjectSetList = class;

  // Engine and internal structures
  TcSession = class;
  TcTransaction = class;
  TcEngine = class;
  TcElements = class;
  TcConceptualTypes = class;


  // -- Hierarchy --------------------------------------------------------------
  //
  //   Element
  //   │
  //   ├─ ElementList
  //   │  ├─ Roles
  //   │  ├─ Unicity
  //   │  │  └─ Identity
  //   │  ├─ ClaimSetList
  //   │  ├─ ObjectSetList
  //   │  └─ ConceptualTypes
  //   │
  //   ├─ NamedElement
  //   │  │
  //   │  ├─ Type
  //   │  │  ├─ DataType
  //   │  │  ├─ LabelType
  //   │  │  └─ CompositeType
  //   │  │     ├─ OperationType
  //   │  │     └─ RelationType - Roles, Identity, ClaimSets and History properties
  //   │  │        ├─ ClaimType - (Nested)ExpressionTemplate
  //   │  │        │  └─ AnnotationType - Kind
  //   │  │        └─ Class - MainClaimType and GroupedClaimTypes
  //   │  └─ Role
  //   │
  //   ├─ ExpressionTemplate
  //   │
  //   ├─ Instance
  //   │  ├─ Operation
  //   │  └─ Tuple
  //   │     ├─ Claim
  //   │     │  └─ Annotation
  //   │     └─ Object
  //   ├─ Annotations
  //   ├─ TupleSet
  //   │  ├─ ClaimSet
  //   │  └─ ObjectSet
  //   │
  //   ├─ Session
  //   ├─ Transaction
  //   └─ Engine
  //


  // -- Namespaces -------------------------------------------------------------
  //
  //   DataTypes: DataType
  //   ConceptualTypes: LabelType, ClaimType and AnnotationType
  //   Classes: Classes
  //


  // -- Elements ---------------------------------------------------------------

  // Element
  //
  //   Ancestor for all elements such as types, instances and transactions.
  //
  //   ID are assigned automaticly.
  //   Elements are added to the engine on creation and removed on destruction.
  //
  TcElement =
    class
      private
        fEngine: TcEngine;
        fID: integer;

      protected
        procedure AssignID;
        function GetIDStr: string;
        function GetRefIDStr: string;

      public
        constructor Create(aEngine: TcEngine);
        destructor Destroy; override;

        // Parent
        property Engine: TcEngine read fEngine;

        // Unique ID within the engine.
        property ID: integer read fID;

        // Returns ID as string
        property IDStr: string read GetIDStr;

        // Returns the ID prefixed with the reference character (cRefChar)
        property RefIDStr: string read GetRefIDStr;
      end;

  // Element List
  //
  //   Wrapper around a generic list with elements of type T with
  //   TcElement as it's supertype (and therefore having an ID).
  //
  TcElementList<T> =
    class (TcElement)
      private
        fList: TcStringObjectList<T>;

        function GetObject(const aIndex: integer): T;
        function GetString(const aIndex: integer): string;
        function GetCount: integer;

      public
        constructor Create(aEngine: TcEngine; aOwned, aSorted: boolean);
        destructor Destroy; override;

        procedure Add(aString: string; aObject: T); overload;
        procedure Add(aID: integer; aObject: T); overload;
        procedure Delete(aIndex: integer);
        procedure Clear;
        procedure FreeAndClear;

        function AsString: string;

        // Sorted list: Binary search
        // Unsorted list: Linear search (case insensitive)
        function Find(aString: string; var aIndex: integer): boolean; overload;
        function Find(aString: string): T; overload;
        function FindByID(aID: integer): T;

        function IndexOf(const S: string): integer;
        function IndexOfObject(aObject: T): integer;

        property Strings[const aIndex: integer]: string read GetString;
        property Objects[const aIndex: integer]: T read GetObject; default;

        property Count: integer read GetCount;
      end;

  // Named Element
  //
  //   Most named elements are part of a namespace.
  //   The namespaces are defined in the general comments above.
  //
  TcNamedElement =
    class (TcElement)
      private
        fName: string;
        fPluralName: string;

      protected

      public
        constructor Create(aEngine: TcEngine; aName: string);

        function GetOrDerivePluralName: string;

        property Name: string read fName;

        // When required, the pluralname has to be set after creation.
        property PluralName: string read fPluralName write fPluralName;
      end;


  // -- Types ------------------------------------------------------------------

  // Type
  //
  //   Ancestor for all types
  //
  TcType =
    class (TcNamedElement)
      end;

  // Data Type
  //
  //   See Engine.InitDataTypes for the default datatypes
  //
  TcDataType =
    class (TcType)
      end;

  // Label Type
  //
  //   Type that represents a non-composite value.
  //
  //   Examples: firstname, description
  //
  TcLabelType =
    class (TcType)
      private
        fDataType: TcDataType;
      public
        constructor Create(aEngine: TcEngine; aName, aDataTypeName: String);

        property DataType: TcDataType read fDataType;
      end;

  // Composite Type
  //
  //   Ancestor for all structured types composed of instances.
  //
  TcCompositeType =
    class (TcType)
      end;

  // Operation Type
  //
  //   Types operations
  //
  TcOperationType =
    class (TcCompositeType)
      // -- Future concept. Added to understand the subtype hierarchy.
      end;

  // Relation Type
  //
  //   Ancestor for all structured types that have roles, an identity
  //   and tuples.
  //   (Similar to a relation in the relational database theory:
  //   https://en.wikipedia.org/wiki/Relation_(database).
  //
  TcRelationType =
    class (TcCompositeType)
      private
        fRoles: TcRoles;
        fIdentity: TcIdentity;
        fUnicityList: TcElementList<TcUnicity>; // ID, Unicity - Owned, Not sorted
        fSuperType: TcRelationType;

        // Temporal settings
        fAllowChange: boolean;
        fTimelineInterpretation: boolean;
        fRecordTransactionTime: boolean;
        fKeepExpired: boolean;
        fRecordValidTime: boolean;
        fKeepEnded: boolean;

      protected
        function GetIsMetaType: boolean;
        function GetHasMetaRole: boolean;

        function GetTupleSet(const aIndex: integer): TcTupleSet; virtual; abstract;
        function GetTupleSetListCount: integer; virtual; abstract;

      public
        constructor Create(aEngine: TcEngine; aName: string); virtual;
        destructor Destroy; override;

        // Call after creating roles and identity
        procedure AfterInit; virtual;

        // Explicitly set the identity
        procedure SetIdentity(aCommaSeparatedListOfRoleNames: string);

        // Add a unicity rule
        procedure AddUnicity(aCommaSeparatedListOfRoleNames: string);

        property IsMetaType: boolean read GetIsMetaType;
        property HasMetaRole: boolean read GetHasMetaRole;

        // Roles of the relation
        property Roles: TcRoles read fRoles;

        // Combination of roles whose values are unique and used to identify
        property Identity: TcIdentity read fIdentity;

        // List with unicities. A unicity is a combination of roles whose values
        // are unique (but not used to identify)
        property UnicityList: TcElementList<TcUnicity> read fUnicityList;

        property SuperType: TcRelationType read fSuperType;

        // When grouping ClaimTypes into a Class the SuperType has to be changed
        // to the corresponding Class. For example: The SuperType was
        // referencing the ClaimType Person, after grouping it should reference
        // the Class Person.
        // (This feature has deliberately not been implemented by a writable
        // SuperType property).
        procedure ChangeSuperType(aSuperType: TcRelationType);

        // Tuples
        //
        //   A TupleSet is a set of Tuples that have the same identity.
        //   (basically a set of historical versions of the same tuple).
        //   The TupleSetList is the list of all TupleSets.
        //
        //  The TupleSetList is implemented as a property that has virtual
        //  abstract methods. The descendents have to implement those methods.
        //  This allows the descendents to implement their own structures
        //  to store tuples. (For example by storing Claims in ClaimSets).
        //
        property TupleSetList[const aIndex: integer]: TcTupleSet read GetTupleSet;
        property TupleSetListCount: integer read GetTupleSetListCount;

        // The Temporal Settings
        //
        //   Determine the rules that should be applied when deriving tuples
        //   from source tuples.
        //

        // Is it allowed to change the initial value of a ClaimSet? Default: true
        property AllowChange: boolean read fAllowChange write fAllowChange;

        // Should adding new claims lead to ending and/or expiration of existing claims? Default: true.
        property TimelineInterpretation: boolean read fTimelineInterpretation write fTimelineInterpretation;

        // Should the attributes RegisteredAt and ExpiredAt be set? Default: true
        property RecordTransactionTime: boolean read fRecordTransactionTime write fRecordTransactionTime;

        // Should we keep expired claims? Default: true
        property KeepExpired: boolean read fKeepExpired write fKeepExpired;

        // Should the attributes ValidFrom and ValidUntil be set? Default: true
        property RecordValidTime: boolean read fRecordValidTime write fRecordValidTime;

        // Should we keep ended claims? Default: true
        property KeepEnded: boolean read fKeepEnded write fKeepEnded;
      end;

  // Claim Type
  //
  //   Atomic relation type. Atomic means the Claim Type cannot be split without
  //   loosing meaning.
  //
  //   Example: If the ClaimType "<Person> owns <Building>." was split into
  //   a ClaimType about Persons and one about Buildings we would loose the
  //   semantic relation between the two.
  //
  TcClaimType =
    class (TcRelationType)
      private
        fClaimSetList: TcClaimSetList;
        fAnnotationKind: TcAnnotationKind;
        fExpressionTemplate: TcExpressionTemplate;
        fNestedExpressionTemplate: TcExpressionTemplate;

      protected
        function GetTupleSet(const aIndex: integer): TcTupleSet; override;
        function GetTupleSetListCount: integer; override;

        // True when there is a NestedExpressionTemplate
        function GetIsObjectType: boolean;

        // True when AnnotationKind <> akNone
        function GetIsAnnotation: boolean;

      public
        constructor Create(aEngine: TcEngine; aName: string); reintroduce;
        destructor Destroy; override;
        procedure AfterInit; override;

        procedure SetExpressionTemplate(aTemplate: string);
        procedure SetNestedExpressionTemplate(aTemplate: string);

        // Derive the identity from the roles and their types.
        // Should be called after the NestedExpresionTemplate has been added (if any).
        procedure DeriveIdentity(out aInfoMessage: String);

        procedure AddTotalityConstraint(aRole: TcRole);
        procedure AddTotalityConstraints(aCommaSeparatedListWithRoles: string);

        // Defers adding to the TupleSetList
        function Add(aSession: TcSession; aTupleStr: string; aValidFrom, aValidUntil: TcTime): TcClaim;

        // ObjectTypes have an Identity that spans all Roles and therefore have a
        // TupleSet for each instance.
        //
        // As a result we can suffice by searching for a TupleSet with an identityStr
        // that matches the current Tuple (expressed as IdentityStr)
        //
        // The tuple should be the only instance in this set.
        // (Fetched from the Source list).
        function GetClaimFromObjectType(aIdentityStr: string): TcClaim;

        property ClaimSetList: TcClaimSetList read fClaimSetList;
        property IsAnnotation: boolean read GetIsAnnotation;
        property ExpressionTemplate: TcExpressionTemplate read fExpressionTemplate;
        property NestedExpressionTemplate: TcExpressionTemplate read fNestedExpressionTemplate;
        property IsObjectType: boolean read GetIsObjectType;
      end;

  // Annotation Type
  //
  //   AnnotationTypes are ClaimTypes where one of the Roles refers to a ClaimType.
  //
  //   Example: "<$claim> is doubted." or "<Check> failed for <$claim>."
  //
  TcAnnotationType =
    class (TcClaimType)
      protected
        function GetKind: TcAnnotationKind;
        function GetKindAsString: string;

      public
        constructor Create(aEngine: TcEngine; aName: string; aKind: TcAnnotationKind);
        procedure AfterInit; override;

        // Defers adding to the TupleSetList
        function Add(aSession: TcSession; aTupleStr: string; aValidFrom, aValidUntil: TcTime): TcAnnotation;

        property Kind: TcAnnotationKind read GetKind;
        property KindAsString: string read GetKindAsString;
      end;


  // Class
  //
  //   A relation type that is composed of a single main ClaimType with zero or
  //   more grouped ClaimTypes. The result is what is commonly referred to as a
  //   class, objecttype or entity (and after lexicalisation of the referencing
  //   roles a relational table).
  //
  //   Example: MainClaimType = Person, GroupedClaimType = Person\Title,
  //   Person\FirstName, Person\LastName
  //
  TcClass =
    class (TcRelationType)
      private
        fObjectSetList: TcObjectSetList;
        fMainClaimType: TcClaimType;
        fGroupedClaimTypes: TcElementList<TcClaimType>; // Name, ClaimType - Not owned, Not sorted

      protected
        function GetTupleSet(const aIndex: integer): TcTupleSet; override;
        function GetTupleSetListCount: integer; override;

        procedure iProcessMainClaimType;
        procedure iProcessGroupedClaimType(aClaimType: TcClaimType; aReferencingRole: TcRole);

      public
        constructor Create(aEngine: TcEngine; aMainClaimType: TcClaimType); reintroduce;
        destructor Destroy; override;
        procedure AfterInit; override;

        // Use this method to add ClaimTypes into the class.
        // The role (in the ClaimType) that references the MainClaimType has
        // to be specified. ClaimTypes might have multiple roles referencing
        // the MainClaimType. Decisions about which role to include should
        // be made by the grouping algorithm, not by this routine. (That's
        // why the role has to be specified).
        procedure AddClaimType(aClaimType: TcClaimType; aReferencingRole: TcRole);

        // Returns the order of the ClaimType in the class.
        // The MainClaimType = 0.
        // GroupedClaimTypes start from 1 based on their order in the GroupedClaimTypes list.
        // Returns -1 if the ClaimType wasn't found.
        function SequenceNrOfClaimType(aClaimType: TcClaimType): integer;

        property ObjectSetList: TcObjectSetList read fObjectSetList;
        property MainClaimType: TcClaimType read fMainClaimType;
        property GroupedClaimTypes: TcElementList<TcClaimType> read fGroupedClaimTypes;
      end;


  // -- Internal structures of Types -------------------------------------------

  // Role
  //
  //   Role in a RelationType.
  //
  //   Example: The ClaimType "<Person> is assigned <socialSecurityNr>"
  //   has two roles: Person and socialSecurityNr. The roles represent variables
  //   in the ExpressionTemplate of the ClaimType.
  //
  TcRole =
    class (TcNamedElement)
      private
        fRelationType: TcRelationType;
        fType: TcType;
        fUnresolvedTypeName: string;
        fSequenceNr: integer;
        fIsTotal: boolean;
        fNecessity: TcMoSCoW;

      protected
        function GetIsMetaRole: boolean;
        function GetIsReference: boolean;
        function GetIsTotalReference: boolean;
        procedure SetIsTotalReference(aState: boolean);
        procedure ResolveType;

      public
        constructor Create(aRelationType: TcRelationType; aName, aTypeName: string; aSequenceNr: integer);

        // When grouping ClaimTypes into a Class the Type has to be changed
        // to the corresponding Class. For example: A role was referencing
        // the ClaimType Person, after grouping it should reference the
        // Class Person.
        // (This feature has deliberately not been implemented by a writable
        // Type_ property).
        procedure ChangeType(aNewType: TcRelationType);

        property RelationType: TcRelationType read fRelationType;
        property Type_: TcType read fType;
        property SequenceNr: integer read fSequenceNr;

        // A meta role is a role referencing meta artifacts. For example
        // an operation or a claim. The name of meta roles are prefixed
        // with a special character (cMetaChar). Such roles are predefined
        // and cannot be added manually.
        property IsMetaRole: boolean read GetIsMetaRole;

        // False when the type of the role is a LabelType. True for other types.
        property IsReference: boolean read GetIsReference;

        // A total reference means every instance from the references type
        // should in the role.
        // For example: Assume we have the ClaimType ColorPreference defined as
        // "<Person>'s favorite color is <color>" and we make Person a total
        // role every tuple from Person should appear in the ClaimType
        // ColorPreference. After grouping ColorPreference into Person this
        // would lead to a required role for the color. (Since every Person
        // should have a preference).
        property IsTotalReference: boolean read GetIsTotalReference write SetIsTotalReference;

        // Normally we would state the role is Optional or Required.
        // It would be practical to have three variants:
        // Optional = mCouldHave,
        // Required = mMustHave
        // Required unless it really is unavailable = mShouldHave.
        // The last state is, from the perspective of the register 'optional',
        // the client however should implement some process to check if the
        // value really isn't available. (warning, four eyes, different
        // authorisation...)
        property Necessity: TcMoSCoW read fNecessity write fNecessity;
      end;

  // Roles
  //
  //   The set of roles of a relation type.
  //
  TcRoles =
    class (TcElementList<TcRole>)
      private
        fRelationType: TcRelationType;
      public
        constructor Create(aRelationType: TcRelationType);

        procedure Add(aRole: TcRole);

        property RelationType: TcRelationType read fRelationType;
      end;

  // Unicity
  //
  //   A combination of roles (minimum 1 role) whose combined values for a
  //   unique combination within the RelationType.
  //   (Similar to the unicity constraint concept in relational databases).
  //
  //   The combination is only unique for one moment in time. See InstanceSet
  //   for more information.
  //
  TcUnicity =
    class (TcElementList<TcRole>)
      private
        fRelationType: TcRelationType;

      public
        constructor Create(aRelationType: TcRelationType);

        procedure Add(aRole: TcRole);

        // Adds the specified roles to unicity (and checks the existance of
        // their type)
        procedure AddRoles(aCommaSeparatedListOfRoleNames: string);

        function AsString: string;

        property RelationType: TcRelationType read fRelationType;
      end;

  // Identity
  //
  //   A unicity that is used to identify tuples.
  //
  //   The combination is only unique for one moment in time. See InstanceSet
  //   for more information.
  //
  TcIdentity =
    class (TcUnicity)
      end;

  // Expression Template
  //
  //   Template expression of a ClaimType.
  //
  //   Example: <Person> owns <Building>
  //
  TcExpressionTemplate =
    class (TcElement)
      private
        fClaimType: TcClaimType;

        // The roles in ClaimTypes are defined by templates.
        // For example: <Foo:TypeA> has <Bar:TypeB>
        // This template contains placeholders with the name of the Role
        // followed by the Type of the Role. This information is used to create
        // the Roles. After creation, in the init routine, the type information
        // is removed: <Foo> has <Bar>.
        // This property stores the original template.
        fOriginalTemplate: string;

        // Template without Type information
        fTemplate: string;

        // Type of template full or nested.
        fType: TcExpressionTemplateType;

        // Parts of the template.
        // Tekst parts have no object.
        // Placeholder parts have the corresponding Role as object.
        fParts: TcElementList<TcRole>; // Text, Role - Not owned, Not sorted

      protected
        // This routine parses the ExpressionTemplate.
        // If the ExpressionTemplate is a full expression (not nested), it will
        // create the Roles in the RelationType.
        // If the ExpressionTemplate is a nested expression, it will validate
        // the Roles in the ExpresionTemplate.
        procedure Init;

        // Routine called by GetExpresion to subsitue tuple values into
        // expressionTemplates. Called itself recursivly to resolve
        // NestedExpressionTemplates.
        procedure iGetExpression(aTemplate: TcExpressionTemplate; aClaim: TcClaim; var aExpression: string);

      public
        constructor Create(aClaimType: TcClaimType; aTemplate: string; aType: TcExpressionTemplateType);
        destructor Destroy; override;

        // Substitues the tuples of the specified Claim to create
        // an expression.
        function GetExpression(aClaim: TcClaim): string;

        property ClaimType: TcClaimType read fClaimType;
        property Template: string read fTemplate;
        property Type_: TcExpressionTemplateType read fType;
        property Parts: TcElementList<TcRole> read fParts;
      end;


  // -- Instances --------------------------------------------------------------

  // Instance
  //
  //   Ancestor for instances.
  //   Instances can be annotated by adding an annotation.
  //
  TcInstance =
    class (TcElement)
      private
        fAnnotations: TcElementList<TcAnnotation>;

      public
        constructor Create(aSession: TcSession);
        destructor Destroy; override;

        procedure Add(aSession: TcSession; aAnnotation: TcAnnotation); virtual;

        property Annotations: TcElementList<TcAnnotation> read fAnnotations;
      end;

  // Operation
  //
  //   Instance defined by its corresponding TcOperationType.
  //
  //   Represents a change in the engine that is meaningful from the users
  //   perspective. It consists of a set of related atomic claims/annotations.
  //
  TcOperation =
    class (TcInstance)
      private
        fIdentifier: string; // -- Derived from Name
        fName: string; // -- Name is used as long as TcOperationType is not implemented
        fForValidation: boolean;
        fRegisteredAt: TcTime;
        fValidFrom: TcTime;
        fValidUntil: TcTime; // Exclusive - https://ell.stackexchange.com/questions/33340/is-until-inclusive-or-exclusive
        fClaims: TcElementList<TcClaim>;

      public
        constructor Create(aSession: TcSession; aTransaction: TcTransaction; aName: string);
        destructor Destroy; override;

        procedure ImmediateDeleteClaim(aClaim: TcClaim);

        function FindClaim(aClaimTypeName, aIdentityStr: string): TcClaim;

        property Claims: TcElementList<TcClaim> read fClaims;
        property Identifier: string read fIdentifier;
        property Name: string read fName;
        property ForValidation: boolean read fForValidation write fForValidation;
        property RegisteredAt: TcTime read fRegisteredAt;
        property ValidFrom: TcTime read fValidFrom write fValidFrom;
        property ValidUntil: TcTime read fValidUntil write fValidUntil;
      end;

  // Tuple
  //
  //   Instance defined by its corresponding TcRelationType.
  //
  TcTuple =
    class (TcInstance)
      private
        fRelationType: TcRelationType;
        fTransactionID: integer;
        fOperation: TcOperation;

        fIdentityStr: String; // Values in identity in order of roles in identity
        fValueStr: string; // Values as comma separated string (in order of the roles)
        fRoleValues: TcKeyValueObjectList<TcRole>; // Rolename=Value, Role - Not owned, Sorted

        fRegisteredAt: TcTime;
        fExpiredAt: TcTime;
        fValidFrom: TcTime;
        fValidUntil: TcTime; // Exclusive - https://ell.stackexchange.com/questions/33340/is-until-inclusive-or-exclusive

      protected
        function TupleSet: TcTupleSet; virtual; abstract;

      public
        constructor Create(aSession: TcSession; aRelationType: TcRelationType);
        destructor Destroy; override;

        function GetTimeAspects: string;
        function GetTimeAspectsCompact: string;

        // Tuple values without values in the Identity.
        // So "<Person> has <firstname>" with Identity on <Person>, will only return the value for <firstname>.
        function GetValueStrWithoutIdentity: string;

        function TupleStrInRoleOrder(aRelationType: TcRelationType; aTuple: TcKeyValueObjectList<TcRole>): string;

        property RelationType: TcRelationType read fRelationType;
        property TransactionID: integer read fTransactionID;
        property Operation: TcOperation read fOperation;

        property IdentityStr: string read fIdentityStr write fIdentityStr;
        property ValueStr: string read fValueStr write fValueStr;
        property RoleValues: TcKeyValueObjectList<TcRole> read fRoleValues;

        // In the first version this was a default property Values[aIndex]...
        // Several bugs were caused by assuming the values are in the order
        // of the Roles. They are NOT! They are sorted by the name of the Role.
        function GetValueBySortedRoleOrder (const aIndex: integer): string;

        property RegisteredAt: TcTime read fRegisteredAt write fRegisteredAt;
        property ExpiredAt: TcTime read fExpiredAt write fExpiredAt;
        property ValidFrom: TcTime read fValidFrom write fValidFrom;
        property ValidUntil: TcTime read fValidUntil write fValidUntil;
      end;

  // Claim Purpose
  TcClaimPurpose = (cpSource, cpTemporalCopy, cpMVCCCopy, cpTemporalAnnotation);

  // Claim
  //
  //   Instance defined by its corresponding TcClaimType
  //
  TcClaim =
    class (TcTuple)
      private
        fClaimType: TcClaimType;
        fClaimSet: TcClaimSet;

        fPurpose: TcClaimPurpose;
        fSourceClaim: integer;
        fMVCCVersionID: integer;
        fMVCCMarkedForCleaningBy: integer;

        fOriginalValidFrom,
        fOriginalValidUntil: TcTime;

      protected
        function TupleSet: TcTupleSet; override;

        function GetIsSource: boolean;
        function GetIsTemporalCopy: boolean;
        function GetIsTemporalAnnotation: boolean;
        function GetIsMVCCCopy: boolean;

        function GetInDoubt: boolean;

        function GetSourceClaimRefIDStr: string;

        function iGetOperationRefIDStr(aSession: TcSession): string;
        function iTupleToIdentityStr(aClaimType: TcClaimType; aTuple: TcKeyValueObjectList<TcRole>): string;
        function iProcessNestedValues(aSession: TcSession; aClaimType: TcClaimType; aValues: TStringlist; var aValueIndex: integer): TcClaim;
        procedure iProcessValues(aSession: TcSession; aValueStr: string);

        // Adds the annotation. In contrast with the Add method it does not interpret it (started/ended/expired)
        procedure iAdd(aSession: TcSession; aAnnotation: TcAnnotation);

        function iCopy(aSession: TcSession; aValidFrom, aValidUntil: TcTime; aPurpose: TcClaimPurpose): TcClaim;

      public
        // There are two ways to specify a Tuple: by conrete values or by references:
        //
        // Say we have the folowing ClaimTypes:
        //   Person:
        //     ET: There is a person <firstname> <lastname>
        //     NET: person <firstname> <lastname>
        //   Building:
        //     ET: There is a building <building id>
        //     NET: building <building id>
        //   Ownership:
        //     ET: <Person> owns <Building>
        //
        // A concrete tuple for Ownership would be: John, Smith, b1
        // A reference tuple for Ownership would be: #29, #45
        //
        // The two types cannot be mixed!
        //
        // The method iProcessValues detects if the Tuple contains reference
        // values. If there are references the iProcessValues method will check
        // those references. If there are only concrete values the method will
        // call the recursive method iProcessNestedValues.
        //
        constructor Create(aSession: TcSession; aClaimType: TcClaimType; aValueStr: string;
          aValidFrom, aValidUntil: TcTime; aPurpose: TcClaimPurpose);

        procedure Add(aSession: TcSession; aAnnotation: TcAnnotation); override;

        function GetExpression: string;
        function GetExpressionFmt: string; // Ending with '.', wrapped in "".

        property ClaimType: TcClaimType read fClaimType;
        property ClaimSet: TcClaimSet read fClaimSet write fClaimSet;

        property Purpose: TcClaimPurpose read fPurpose;

        // ID of the Claim that's the source of this Claim.
        // When a Claim is copied this attribute will still reference the
        // original Claim.
        property SourceClaim: integer read fSourceClaim write fSourceClaim;
        property SourceClaimRefIDStr: string read GetSourceClaimRefIDStr;

        property IsSource: boolean read GetIsSource;
        property IsTemporalCopy: boolean read GetIsTemporalCopy;
        property IsTemporalAnnotation: boolean read GetIsTemporalAnnotation;

        property InDoubt: boolean read GetInDoubt;

        // -- MVCC

        // When a copy is made for the MVCC mechanism, this copy retains the
        // versionID of the original Claim. The original Claim can however be
        // a copy (not for MVCC reasons) of a SourceClaim.
        // For example: SourceClaim #42 contains the claim "Person P1 has first
        // name John.". While deriving the temporal view a "normal" copy is made.
        // Lets assume this is Claim #56. After a while a transaction starts
        // and an MVCC copy is made of Claim #56, say Claim #91.
        // Claim #91 now refers to SourceClaim #42 and has MVCCVersionID #56.
        // (Because it is a version of #56).
        property MVCCVersionID: integer read fMVCCVersionID write fMVCCVersionID;

        // When a claim is marked for cleaning it will be deleted as soon
        // as all transactions are ended that were depending on the claim.
        property MVCCMarkedForCleaningBy: integer read fMVCCMarkedForCleaningBy write fMVCCMarkedForCleaningBy; // Transaction ID

        property IsMVCCCopy: boolean read GetIsMVCCCopy;
      end;

  // Annotation
  //
  //   Instance defined by its corresponding TcAnnotationType
  //   Derived from TcClaim
  //
  TcAnnotation =
    class (TcClaim)
      private
        fAnnotationType: TcAnnotationType;
        fDerivedFrom: integer;

      protected
        function GetKind: TcAnnotationKind;

        procedure ChangeReferenceInValueStr(var aValueStr: string; aOldID, aNewID: integer);
        function iCopy(aSession: TcSession; aOldClaim, aNewClaim: TcClaim;
          aValidFrom, aValidUntil: TcTime; aPurpose: TcClaimPurpose): TcAnnotation;

      public
        constructor Create(aSession: TcSession; aAnnotationType: TcAnnotationType; aValueStr: string;
          aValidFrom, aValidUntil: TcTime; aPurpose: TcClaimPurpose);

        property AnnotationType: TcAnnotationType read fAnnotationType;
        property Kind: TcAnnotationKind read GetKind;

        // When a Claim is added to a TupleSet it will impact existing Claims.
        // Existing Claims can be expired and/or ended because the Claim was
        // added. When the option CreateTemporalAnnotations is active (Engine
        // property) Annotations (with purpose TemporalAnnotation) will be
        // created for each ExpiredAt or ValidUntil this is set.
        // The DerivedFrom property contains the ID of the Claim that was added
        // and caused the creation of this Temporal Annotation.
        property DerivedFrom: integer read fDerivedFrom write fDerivedFrom;
      end;

  // Object
  //
  //   Instances defined by its corresponding TcClass
  //
  TcObject =
    class (TcTuple)
      // -- Future concept. Added to understand the subtype hierarchy.
      private
        fClass: TcClass;
        fObjectSet: TcObjectSet;
        fRoleValueClaims: TcKeyValueObjectList<TcClaim>; // Rolename=Value, Claim - Not owned, Sorted

      protected
        function TupleSet: TcTupleSet; override;

      public
        constructor Create(aSession: TcSession; aClass: TcClass);
        destructor Destroy; override;

        property Class_: TcClass read fClass;
        property ObjectSet: TcObjectSet read fObjectSet;
        property RoleValueClaims: TcKeyValueObjectList<TcClaim> read fRoleValueClaims;
      end;


  // -- TupleSet, TupleSetList and internal structues --------------------

  // TupleSet
  //
  //   Set of Tuples of a RelationType.
  //   Ancestor for TcClaimSet and TcObjectSet.
  //
  TcTupleSet =
    class (TcElement)
      private
        fRelationType: TcRelationType;
        fIdentityStr: String;

      protected
        function GetTuple(const aIndex: integer): TcTuple; virtual; abstract;
        function GetTupleCount: integer; virtual; abstract;

      public
        constructor Create (aRelationType: TcRelationType; aIdentityStr: String);

        property RelationType: TcRelationType read fRelationType;
        property IdentityStr: string read fIdentityStr;

        // Virtual property mapped onto either a ClaimSet or ObjectSet in
        // the descendant by implementing GetTuple and GetTupleCount.
        property Tuples[const aIndex: integer]: TcTuple read GetTuple;
        property TupleCount: integer read GetTupleCount;
      end;

  // ClaimSet
  //
  //   Set of Claims/Annotations of a ClaimType/AnnotationType.
  //
  TcClaimSet =
    class (TcTupleSet)
      private
        fClaimType: TcClaimType;
        fSourceClaims: TcElementList<TcClaim>;
        fDerivedClaims: TcElementList<TcClaim>;
        fActions: TList<TcClaimSetAction>;
        fLockedByTransactionID: integer;

      protected
        // Mapped on the SourceClaims
        function GetTuple(const aIndex: integer): TcTuple; override;
        function GetTupleCount: integer; override;

        // 1.
        // Called from Add after the SourceClaim has been added to SourceClaims.
        // It will make a copy of the SourceClaim. This copy, a DerivedClaim,
        // will be added to the list of DerivedClaims. Claims in this list
        // are part of a temporal diagram. This means that new claims that are
        // added to the list result in changes to existing claims. This routine
        // determines those changes and will call the methods below to change
        // existing claims (and the iAdd to finally add the copy of the source
        // claim).
        procedure iAddAndInterpret(aSession: TcSession; aSourceClaim: TcClaim);

        // 2.
        // Called from iAddAndInterpret. Each function creates a TcClaimSetAction
        // instance that stores all information to perform the operation.
        // This instances is added to the action list to be executed by
        // iProcessChanges (that is called at the end of iAddAndInterpret).
        procedure iAdd       (aClaim: TcClaim);
        procedure iStart     (aClaim, aNewClaim: TcClaim; aAnnotation: TcAnnotation; aStartTime: TcTime); overload;
        procedure iEnd       (aClaim, aNewClaim: TcClaim; aAnnotation: TcAnnotation; aEndTime: TcTime); overload;
        procedure iExpire    (aClaim, aNewClaim: TcClaim; aAnnotation: TcAnnotation = nil);
        procedure iDelete    (aClaim: TcClaim; aAnnotation: TcAnnotation = nil);
        procedure iCopyEnd   (aClaim, aNewClaim: TcClaim; aEndTime: TcTime);
        procedure iCopyStart (aClaim, aNewClaim: TcClaim; aStartTime: TcTime);

        // 3.
        // Executes all actions in the Actions list. The methods listed under 4
        // can be called to perform tasks.
        procedure iProcessChanges(aSession: TcSession);

        // 4.
        function iCreateMetaAnnotation(aSession: TcSession; aAnnotationTypeName: string; aDerivedFrom: TcClaim; aClaimRefIDStr: string): TcAnnotation;
        procedure iImmediateDeleteSourceClaim(aClaim: TcClaim);
        procedure iImmediateDeleteDerivedClaim(aClaim: TcClaim);

      public
        constructor Create(aClaimType: TcClaimType; aIdentityStr: String);
        destructor Destroy; override;

        procedure Lock(aSession: TcSession);
        procedure Unlock;
        property LockedByTransactionID: integer read fLockedByTransactionID write fLockedByTransactionID;

        // Add a SourceClaim to the ClaimSet and call iAddAndInterpret.
        procedure Add(aSession: TcSession; aSourceClaim: TcClaim);

        // -- Currently these functions work on the Derived Claims !
        // You'd expect them to work on the Source Claims.
        // Future experimentation has to point out how this should be done.
        procedure ExpireAll (aSession: TcSession);
        procedure Expire    (aSession: TcSession; aClaim: TcClaim; aAnnotation: TcAnnotation); overload;
        procedure Expire    (aSession: TcSession; aClaim: TcClaim); overload;
        procedure Start     (aSession: TcSession; aClaim: TcClaim; aAnnotation: TcAnnotation; aStartTime: TcTime); overload;
        procedure Start     (aSession: TcSession; aClaim: TcClaim; aStartTime: TcTime); overload;
        procedure End_      (aSession: TcSession; aClaim: TcClaim; aAnnotation: TcAnnotation; aEndTime: TcTime); overload;
        procedure End_      (aSession: TcSession; aClaim: TcClaim; aEndTime: TcTime); overload;
        procedure Delete    (aSession: TcSession; aClaim: TcClaim; aAnnotation: TcAnnotation); overload;
        procedure Delete    (aSession: TcSession; aClaim: TcClaim); overload;

        // Clear all Derived Claims from the ClaimSet and Derive them from the Original Claim.
        procedure DeriveClaims(aSession: TcSession);

        property ClaimType: TcClaimType read fClaimType;
        property SourceClaims: TcElementList<TcClaim> read fSourceClaims;
        property DerivedClaims: TcElementList<TcClaim> read fDerivedClaims;
      end;

  // ClaimSet Action Kind
  TcClaimSetActionKind = (cakAdd, cakStart, cakEnd, cakExpire, cakDelete, cakCopyEnd, cakCopyStart);

  // ClaimSet Action
  TcClaimSetAction =
    class
      private
        fKind: TcClaimSetActionKind;
        fClaim: TcClaim;
        fNewClaim: TcClaim;
        fAnnotation: TcAnnotation;
        fTime: TcTime;
      public
        constructor Create(aKind: TcClaimSetActionKind;
          aClaim, aNewClaim: TcClaim;
          aAnnotation: TcAnnotation = nil;
          aTime: TcTime = cNoTime);

        property Kind: TcClaimSetActionKind read fKind;
        property Claim: TcClaim read fClaim;
        property NewClaim: TcClaim read fNewClaim;
        property Annotation: TcAnnotation read fAnnotation;
        property Time: TcTime read fTime;
      end;

  // ObjectSet
  TcObjectSet =
    class (TcTupleSet)
      private
        fClass: TcClass;
        fObjects: TcElementList<TcObject>;

      protected
        function GetTuple(const aIndex: integer): TcTuple; override;
        function GetTupleCount: integer; override;

      public
        constructor Create(aClass: TcClass; aIdentityStr: String);
        destructor Destroy; override;

        property Class_: TcClass read fClass;
        property Objects: TcElementList<TcObject> read fObjects;
      end;

  // ClaimSet List
  //
  //   List with all ClaimSets of a ClaimType
  //

  { TcClaimSetList }

  TcClaimSetList =
    class (TcElementList<TcClaimSet>)
      private
        fClaimType: TcClaimType;

      protected
        procedure iAdd(aSession: TcSession; aClaim: TcClaim);

      public
        constructor Create (aClaimType: TcClaimType);

        function Find(aIdentityStr: string): TcClaimSet;

        // Creates the Claim. Finds the matching ClaimSet or creates a new one and adds the claim.
        function AddClaim(aSession: TcSession; aTupleStr: string; aValidFrom, aValidUntil: TcTime): TcClaim;

        // Creates the Annotation. Finds the matching ClaimSet or creates a new one and adds the claim.
        function AddAnnotation(aSession: TcSession; aTupleStr: string; aValidFrom, aValidUntil: TcTime): TcAnnotation;

        // Derive the set of DerivedClaims from the set of SourceClaims.
        // Existing derived claims will be cleared.
        procedure DeriveClaims(aSession: TcSession);

        property ClaimType: TcClaimType read fClaimType;
      end;

  // ObjectSet List
  //
  //   List with all ObjectSets of a Class
  //
  TcObjectSetList =
    class (TcElementList<TcObjectSet>)
      private
        fClass: TcClass;
      public
        constructor Create(aClass: TcClass);

        property Class_: TcClass read fClass;
      end;


  // -- Session, Transaction ---------------------------------------------------

  // Session
  //
  //   Isolated session on the engine.
  //
  TcSession =
    class (TcElement)
      private
        fSeqNr: integer;
        fTransaction: TcTransaction;
        fOperation: TcOperation;
        function GetInTransaction: boolean;
        function GetInOperation: boolean;
        procedure iEndTransaction(aCommit: boolean);

      public
        constructor Create(aEngine: TcEngine);
        destructor Destroy; override;

        function GetColor: TColor;
        function GetInfoStr: string;

        procedure StartTransaction(aImplicit, aReadonly, aStableRead: boolean);
        procedure Commit;
        procedure Rollback;

        property Transaction: TcTransaction read fTransaction;
        property InTransaction: boolean read GetInTransaction;

        procedure StartOperation(aName: string);
        property Operation: TcOperation read fOperation;
        property InOperation: boolean read GetInOperation;
      end;

  // Transaction
  //
  //   Isolated transaction on the engine.
  //
  TcTransaction =
    class (TcNamedElement)
      private
        fSession: TcSession;
        fImplicit: boolean;
        fReadonly: boolean;
        fStableRead: boolean;
        fStableReadSnapshotID: integer;
        fStartID: integer;
        fEndID: integer;
        fStartTime: TcTime;
        fEndTime: TcTime;
        fState: TcTransactionState;
        fLockedClaimSets: TcElementList<TcClaimSet>; // ID, ClaimSet

        procedure iRollbackChanges;
        procedure iReleaseLocks;
        function GetStableReadSnapshotTime: TcTime;

      public
        constructor Create(aSession: TcSession; aName: string; aImplicit, aReadonly, aStableRead: boolean);
        destructor Destroy; override;

        procedure End_(aCommit: boolean);

        property Session: TcSession read fSession;
        property Implicit: boolean read fImplicit;
        property Readonly_: boolean read fReadonly;
        property StableRead: boolean read fStableRead;
        property StableReadSnapshotID: integer read fStableReadSnapshotID;
        property StableReadSnapshotTime: TcTime read GetStableReadSnapshotTime;
        property StartID: integer read fStartID;
        property EndID: integer read fEndID;
        property StartTime: TcTime read fStartTime;
        property EndTime: TcTime read fEndTime;
        property State: TcTransactionState read fState;
        property LockedClaimSets: TcElementList<TcClaimSet> read fLockedClaimSets;
      end;


  // -- Engine and internal structures -----------------------------------------

  // Engine
  //
  //   The Atomic Claim Engine
  //
  TcEngine =
    class
      private
        fName: string;

        fElements: TcElements;
        fMaxElementID: integer;
        fMaxTransactionID: integer;

        fDataTypes: TcElementList<TcDataType>;
        fRelationTypes: TcElementList<TcRelationType>; // ID, RelationType - Redundant list, not owned
        fConceptualTypes: TcConceptualTypes;
        fClasses: TcElementList<TcClass>;

        fTransactions: TcElementList<TcTransaction>;
        fOperations: TcElementList<TcOperation>;
        fOperationClaimTypes: TcElementList<TcClaimType>;

        fUseMVCC: boolean;
        fCreateTemporalAnnotations: boolean;
        fClaimSetsToClean: TcElementList<TcClaimSet>;

        fAutoIncrementSystemTime: boolean;
        fSystemTime: TcTime;
        fReplayMode: boolean;
        fReplayTime: TcTime;

        fMinSystemTime: TcTime;
        fMaxSystemTime: TcTime;
        fMinValidTime: TcTime;
        fMaxValidTime: TcTime;

        fGroupTuples: Boolean;

      protected
        procedure iInitDataTypes;
        procedure iExposeMetaStructures;

        procedure iMVCCClean;

        function GetNewElementID: integer;

        function GetSystemTime: integer;
        procedure SetSystemTime(aTime: integer);

        function GetInTransaction: boolean;

        // Every Claim up to and including this ID can be read without
        // the risk of later change
        function GetStableReadSnapshotID: integer;

        // StableReadSnapshotTime (derived from StableReadSnapshotID)
        function GetStableReadSnapshotTime: TcTime;

        // Translates a StableReadSnapshotID to time
        function StableReadSnapshotIDToTime(aStableReadSnapshotID: integer): TcTime;

        // Every claim that is marked for cleaning and older that this
        // ID can be cleaned
        function GetOldestRequiredTransactionID: integer;

      public
        constructor Create(aName: string = '');
        destructor Destroy; override;

        // Elements
        property Elements: TcElements read fElements;
        function FindElementByID(aID: integer): TcElement;
        function FindElementByRefIDStr(aIDStr: string): TcElement;

        // Structure
        property DataTypes: TcElementList<TcDataType> read fDataTypes;
        property RelationTypes: TcElementList<TcRelationType> read fRelationTypes;
        property ConceptualTypes: TcConceptualTypes read fConceptualTypes;
        property Classes: TcElementList<TcClass> read fClasses;
        function FindClassByPluralName(aPluralName: string): TcClass; // Linear search

        // Content
        function FindClaim(aID: integer): TcClaim;
        function FindClaimSet(aID: integer): TcClaimSet;

        // SystemTime
        procedure SetAutoIncrementSystemTime(aEnabled: boolean);
        property AutoIncrementSystemTime: boolean read fAutoIncrementSystemTime;
        procedure TriggerAutoIncrementSystemTime;
        procedure IncrementSystemTime;
        property SystemTime: TcTime read GetSystemTime write SetSystemTime;

        property MinSystemTime: TcTime read fMinSystemTime;
        property MaxSystemTime: TcTime read fMaxSystemTime;

        procedure RegisterValidPeriod(aValidFrom, aValidUntil: TcTime);
        property MinValidTime: TcTime read fMinValidTime;
        property MaxValidTime: TcTime read fMaxValidTime;

        // Settings
        property GroupTuples: boolean read fGroupTuples write fGroupTuples;

        // Replay
        procedure StartReplay;
        procedure StopReplay;
        property ReplayMode: boolean read fReplayMode;
        property ReplayTime: TcTime read fReplayTime write fReplayTime;

        // Transactions
        function GetNewTransactionID: integer;
        property Transactions: TcElementList<TcTransaction> read fTransactions;
        property InTransaction: boolean read GetInTransaction;
        property StableReadSnapshotID: integer read GetStableReadSnapshotID;
        property StableReadSnapshotTime: TcTime read GetStableReadSnapshotTime;

        // Operations
        property Operations: TcElementList<TcOperation> read fOperations;
        property OperationClaimTypes: TcElementList<TcClaimType> read fOperationClaimTypes;

        // Derivation
        property CreateTemporalAnnotations: boolean read fCreateTemporalAnnotations write fCreateTemporalAnnotations;

        // MVCC
        property UseMVCC: boolean read fUseMVCC write fUseMVCC;
        property ClaimSetsToClean: TcElementList<TcClaimSet> read fClaimSetsToClean;
      end;

  // Elements
  //
  //   Wrapper around a list of elements.
  //   Cannot be an TcElementList because that's an TcElement decendent and
  //   TcElements will add themselves to the fElements property of the Engine.
  //
  TcElements =
    class
      private
        fItems: TcStringObjectList<TcElement>;

      protected
        function GetItem(const aIndex: integer): TcElement;
        function GetCount: integer;

      public
        constructor Create;
        destructor Destroy; override;

        procedure Add(aElement: TcElement);
        procedure Delete(aElement: TcElement);
        function Find(aID: integer): TcElement;
        property Items[const aIndex: integer]: TcElement read GetItem; default;
        property Count: integer read GetCount;
      end;

  // Conceptual Types
  TcConceptualTypes =
    class (TcElementList<TcType>)
      private
      public
        constructor Create(aEngine: TcEngine);

        function Find(aName: string): TcType;
        function FindLabelType(aName: string): TcLabelType;
        function FindClaimType(aName: string): TcClaimType;
        function FindAnnotationType(aName: string): TcAnnotationType;
      end;

// Misc
function CheckOrAskClaimReference(aIDStr: string): integer;

// Conversion functions
function AnnotationKindStrToKind(aKindStr: string): TcAnnotationKind;
function AnnotationKindToKindStr(aKind: TcAnnotationKind): string;

implementation

uses
  SysUtils,
  common.ErrorHandling, common.Utils, common.RadioStation,
  ace.Core.Tokenizer,
  ace.Settings, diagram.Settings, ace.Debug, Dialogs;


// -- Misc

function CheckOrAskClaimReference(aIDStr: string): integer;
  begin
  if CompareText(aIDStr, cMetaValueName_Ask)=0
     then begin
          // Ask
          aIDStr := '';
          InputQuery('Claim ID', 'Enter the ID of the claim', aIDStr);
          aIDStr := Trim(aIDStr);
          if aIDStr='' then EUsr('Claim ID value missing.');
          if aIDStr[1] = cRefChar then aIDStr := Copy(aIDStr, 2, MaxInt);
          result := StrToIntDef(aIDStr, -1);
          end
     else begin
          // Check
          if (Length(aIDStr)>0) and (aIDStr[1]<>cRefChar)
             then EUsr('Invalid ID. ID should start with a ''%s''.', [cRefCHar]);
          aIDStr := Copy(aIDStr, 2, Length(aIDStr));
          result := StrToIntDef(aIDStr, -1);
          end;
  if result = -1 then EUsr('Invalid Claim ID.');
  end;


// -- Conversion

function AnnotationKindStrToKind(aKindStr: string): TcAnnotationKind;
  begin
  result := akNone;
  if CompareText(aKindStr, 'none')=0
     then result := akNone
  else if CompareText(aKindStr, 'info')=0
     then result := akInfo
  else if CompareText(aKindStr, 'doubt')=0
     then result := akDoubt
  else if CompareText(aKindStr, 'started')=0
     then result := akStarted
  else if CompareText(aKindStr, 'ended')=0
     then result := akEnded
  else if CompareText(aKindStr, 'expired')=0
     then result := akExpired
  else if CompareText(aKindStr, 'deleted')=0
     then result := akDeleted
  else EUsr('Unknown annotation kind: %s', [aKindStr]);
  end;

function AnnotationKindToKindStr(aKind: TcAnnotationKind): string;
  begin
  case aKind of
    akNone:    result := 'none';
    akInfo:    result := 'info';
    akDoubt:   result := 'doubt';
    akStarted: result := 'started';
    akEnded:   result := 'ended';
    akExpired: result := 'expired';
    akDeleted: result := 'deleted';
    else begin
         result := '';
         EInt('AnnotationKindToKindStr', 'Unknown annotation kind');
         end;
    end;
  end;


// -- Misc

function IsMetaRoleName(aRoleName: string): boolean;
  var
    i: integer;
  begin
  for i := 0 to gMetaRoleNames.Count-1 do
    if CompareText(aRoleName, gMetaRoleNames[i])=0
       then begin
            result := true;
            exit;
            end;
  result := false;
  end;


{ TcElement }

constructor TcElement.Create(aEngine: TcEngine);
  begin
  inherited Create;
  fEngine := aEngine;
  AssignID;
  Engine.Elements.Add(Self);

  // -- Code to add UUID:
  // var
  //   lhResult: longint;
  // begin
  // lhResult := CreateGUID(fUUID); -- fUUID is the internal property
  // Assert(lhResult = s_ok);
  // end;
  end;

destructor TcElement.Destroy;
  begin
  // During destruction of the Engine the Elements list will be nil to prevent
  // deletion while destroying.
  if Assigned(Engine.Elements)
    then Engine.Elements.Delete(Self);
  inherited Destroy;
  end;

procedure TcElement.AssignID;
  begin
  fID := Engine.GetNewElementID;
  end;

function TcElement.GetIDStr: string;
  begin
  result := IntToStr(ID);
  end;

function TcElement.GetRefIDStr: string;
  begin
  result := cRefChar + IntToStr(ID);
  end;


{ TcElementList }

constructor TcElementList<T>.Create(aEngine: TcEngine; aOwned, aSorted: boolean);
  begin
  inherited Create(aEngine);
  fList := TcStringObjectList<T>.Create(aOwned, aSorted);
  end;

destructor TcElementList<T>.Destroy;
  begin
  FreeAndNil(fList);
  inherited;
  end;

function TcElementList<T>.GetString(const aIndex: integer): string;
  begin
  result := fList[aIndex];
  end;

function TcElementList<T>.GetObject(const aIndex: integer): T;
  begin
  result := fList.Objects[aIndex];
  end;

function TcElementList<T>.GetCount: integer;
  begin
  result := fList.Count;
  end;

procedure TcElementList<T>.Add(aString: string; aObject: T);
  begin
  fList.Add(aString, aObject);
  end;

procedure TcElementList<T>.Add(aID: integer; aObject: T);
  begin
  fList.Add(IntToStr(aID), aObject);
  end;

procedure TcElementList<T>.Delete(aIndex: integer);
  begin
  fList.Delete(aIndex);
  end;

procedure TcElementList<T>.Clear;
  begin
  fList.Clear;
  end;

procedure TcElementList<T>.FreeAndClear;
  begin
  fList.FreeAndClear;
  end;

function TcElementList<T>.AsString: string;
  begin
  result := fList.AsString;
  end;

function TcElementList<T>.Find(aString: string; var aIndex: integer): boolean;
  begin
  result := fList.Find(aString, aIndex);
  end;

function TcElementList<T>.Find(aString: string): T;
  begin
  result := fList.Find(aString);
  end;

function TcElementList<T>.FindByID(aID: integer): T;
  begin
  result := Find(IntToStr(aID));
  end;

function TcElementList<T>.IndexOf(const S: string): integer;
  begin
  result := fList.IndexOf(S);
  end;

function TcElementList<T>.IndexOfObject(aObject: T): integer;
  begin
  result := fList.IndexOfObject(aObject);
  end;


{ TcNamedElement }

constructor TcNamedElement.Create(aEngine: TcEngine; aName: string);
  begin
  inherited Create(aEngine);
  fName := aName;
  fPluralName := '';
  end;

function TcNamedElement.GetOrDerivePluralName: string;
  begin
  if fPluralName = ''
     then result := fName + 'List'
     else result := fPluralName;
  end;


{ TcLabelType }

constructor TcLabelType.Create(aEngine: TcEngine; aName, aDataTypeName: String);
  var
    lDataType: TcDataType;
  begin
  inherited Create(aEngine, aName);
  lDataType := aEngine.DataTypes.Find(aDataTypeName);
  if not Assigned(lDataType)
    then EUsr('Unknown DataType %s', [aDataTypeName])
    else fDataType := lDataType;
  end;


{ TcRelationType }

constructor TcRelationType.Create (aEngine: TcEngine; aName: string);
  begin
  inherited Create(aEngine, aName);
  fRoles := TcRoles.Create(Self);
  fIdentity := TcIdentity.Create(Self);
  fUnicityList := TcElementList<TcUnicity>.Create(aEngine, true {Owned}, false {Not sorted});
  fSuperType := nil;

  fAllowChange := true;
  fTimelineInterpretation := true;
  fRecordTransactionTime := true;
  fKeepExpired := true;
  fRecordValidTime := true;
  fKeepEnded := true;
  end;

destructor TcRelationType.Destroy;
  begin
  FreeAndNil(fRoles);
  FreeAndNil(fIdentity);
  FreeAndNil(fUnicityList);
  inherited Destroy;
  end;

function TcRelationType.GetIsMetaType: boolean;
  begin
  result := StartsWithChar(Name, cMetaChar);
  end;

function TcRelationType.GetHasMetaRole: boolean;
  var
    r: integer;
  begin
  result := true;
  for r := 0 to Roles.Count-1 do
    if Roles[r].IsMetaRole
       then exit;
  result := false;
  end;

procedure TcRelationType.AfterInit;
  begin
  if not Assigned(fIdentity) then EInt('TcRelationType.AfterInit', 'Call after creating identity.');
  if Identity.Count=0 then EInt('TcRelationType.AfterInit', 'Call after creating identity.');

  if (Identity.Count=1) and (Identity[0].Type_ is TcRelationType)
     then fSuperType := (Identity[0].Type_ as TcRelationType);
  end;

procedure TcRelationType.SetIdentity(aCommaSeparatedListOfRoleNames: string);
  begin
  Identity.AddRoles(aCommaSeparatedListOfRoleNames);
  end;

procedure TcRelationType.AddUnicity(aCommaSeparatedListOfRoleNames: string);
  var
    lUnicity: TcUnicity;
  begin
  lUnicity := TcUnicity.Create(Self);
  lUnicity.AddRoles(aCommaSeparatedListOfRoleNames);
  UnicityList.Add(lUnicity.ID, lUnicity);
  end;

procedure TcRelationType.ChangeSuperType(aSuperType: TcRelationType);
  begin
  fSuperType := aSuperType;
  end;


{ TcClaimType }

constructor TcClaimType.Create(aEngine: TcEngine; aName: string);
  begin
  inherited Create(aEngine, aName);
  fClaimSetList := TcClaimSetList.Create(Self);
  fAnnotationKind := akNone;
  fExpressionTemplate := nil;
  fNestedExpressionTemplate := nil;
  end;

destructor TcClaimType.Destroy;
  begin
  FreeAndNil(fClaimSetList);
  FreeAndNil(fExpressionTemplate);
  FreeAndNil(fNestedExpressionTemplate);
  inherited Destroy;
  end;

procedure TcClaimType.AfterInit;
  begin
  inherited;
  end;

function TcClaimType.GetTupleSet(const aIndex: integer): TcTupleSet;
  begin
  result := fClaimSetList[aIndex];
  end;

function TcClaimType.GetTupleSetListCount: integer;
  begin
  result := fClaimSetList.Count;
  end;

function TcClaimType.GetIsObjectType: boolean;
  begin
  result := Assigned(fNestedExpressionTemplate);
  end;

function TcClaimType.GetIsAnnotation: boolean;
  begin
  result := fAnnotationKind <> akNone;
  end;

procedure TcClaimType.SetExpressionTemplate(aTemplate: string);
  begin
  if Assigned(fExpressionTemplate) then EInt('TcClaimType.SetExpressionTemplate', 'Template already assigned.');
  fExpressionTemplate := TcExpressionTemplate.Create(Self, aTemplate, ettFull);
  end;

procedure TcClaimType.SetNestedExpressionTemplate(aTemplate: string);
  begin
  if Assigned(fNestedExpressionTemplate)
     then FreeAndNil(fNestedExpressionTemplate);

  // Check if every Role is present in the Identity.
  if Roles.Count <> Identity.Count
     then EUsr('Cannot add an NestedExpressionTemplate to %s. '+
            'The Identity of the %s does not cover every Role.',
            [Name, Name]);

  fNestedExpressionTemplate := TcExpressionTemplate.Create(Self, aTemplate, ettNested);
  end;

procedure TcClaimType.DeriveIdentity(out aInfoMessage: String);
  var
    i: integer;
    lRole1, lRole2, lRole: TcRole;
  begin
  if IsAnnotation
     then EInt('TcClaimType.DeriveIdentity', 'Annotations have a fixed identity on the %sclaim role.', [cMetaChar]);

  // Situation 1: The Type is an ObjectType -> Include all Roles
  if IsObjectType
     then begin
          for i := 0 to Roles.Count-1 do
            begin
            lRole := Roles[i];
            Identity.Add(lRole);
            aInfoMessage := 'Identity derived. ObjectExpressionTemplate found. Identity spans all Roles.';
            end;
          end

  // Situation 2: There is only on Role -> Identity is this one Role.
  else if Roles.Count=1
    then begin
         Identity.Add(Roles[0]);
         aInfoMessage := Format('Identity derived. Role: %s', [Identity.AsString]);
         end

  // Situation 3: There are two Roles. One of them is a LabelType the other an ObjectType. -> Identity is the ObjectType
  else if Roles.Count=2
    then begin
         lRole1 := Roles[0];
         lRole2 := Roles[1];
         if lRole1.Type_.ClassName <> lRole2.Type_.ClassName
           then begin
                if lRole1.Type_.ClassType = TcClaimType
                  then lRole := lRole1
                  else lRole := lRole2;
                Identity.Add(lRole);
                aInfoMessage := Format('Identity derived. Role: %s', [Identity.AsString]);
                end;
         end;

  // -- No longer generate the fallback below. Several 'bugs' were actually
  //    cases of unexpected behaviour because the implicit identity
  //    covered all roles (while this should not have been the case).
  //
  // Fallback: Allways create an identity
  // if Identity.Count=0
  //    then begin
  //         for i := 0 to Roles.Count-1 do
  //           begin
  //           lRole := Roles[i];
  //           Identity.Add(lRole);
  //           aInfoMessage := 'Identity derived. Identity spans all Roles.';
  //           end;
  //         end;

  // Identity not derived -> Error
  if Identity.Count=0
     then EUsr('Identity missing for %s.', [Name]);

  // Identity derived
  AfterInit;
  end;

procedure TcClaimType.AddTotalityConstraint(aRole: TcRole);
  begin
  if not Assigned(aRole)
     then EInt('TcClaimType.AddTotalityConstraint', 'Role not assigned');

  aRole.IsTotalReference := true;
  end;

procedure TcClaimType.AddTotalityConstraints(aCommaSeparatedListWithRoles: string);
  var
    i: integer;
    lRoleNames: TStringlist;
    lRole: TcRole;
  begin
  lRoleNames := StringToStringlist(aCommaSeparatedListWithRoles, ',');
  try
    for i := 0 to lRoleNames.Count-1 do
      begin
      lRole := Roles.Find(lRoleNames[i]);
      if not Assigned(lRole)
         then EUsr('Role % not found in ClaimType %s', [lRoleNames[i], Name]);
      lRole.IsTotalReference := true;
      end;
  finally
    lRoleNames.Free;
    end;
  end;

function TcClaimType.Add(aSession: TcSession; aTupleStr: string; aValidFrom, aValidUntil: TcTime): TcClaim;
  begin
  result := ClaimSetList.AddClaim(aSession, aTupleStr, aValidFrom, aValidUntil);
  end;

function TcClaimType.GetClaimFromObjectType(aIdentityStr: string): TcClaim;
  var
    lClaimSet: TcClaimSet;
    lClaim: TcClaim;
  begin
  result := nil;

  if not IsObjectType
     then EInt('TcClaimType.GetClaimFromObjectType', '%s is not an ObjectType', [Name]);

  lClaimSet := ClaimSetList.Find(aIdentityStr);
  if not Assigned(lClaimSet)
     then exit; // ClaimSet not present (an therefore Claim not present)

  if lClaimSet.SourceClaims.Count>1
     then EInt('TcClaimType.GetClaimFromObjectType', 'TupleSet of ObjectType %s contains more than one Tuple.', [Name]);

  // This represents an exceptional case where the Claim has been deleted from the ClaimSet
  if lClaimSet.SourceClaims.Count=0
     then exit; // The ClaimSet still exists. But there is no Claim.

  // Return the only Claim in the set
  lClaim := lClaimSet.SourceClaims[0];
  if not (lClaim is TcClaim) then EInt('TcClaimType.GetClaimFromObjectType', 'Unexpected type');
  result := lClaim as TcClaim;
  end;


{ TcAnnotationType }

constructor TcAnnotationType.Create(aEngine: TcEngine; aName: string; aKind: TcAnnotationKind);
  begin
  inherited Create(aEngine, aName);
  fAnnotationKind := aKind;
  end;

procedure TcAnnotationType.AfterInit;
  var
    lRole: TcRole;
  begin
  inherited;

  // At least one Role should reference the cMetaName_Claim
  lRole := Roles.Find(cMetaRoleName_Claim);
  if not Assigned(lRole)
     then EUsr('AnnotationTypes should have a Role referencing %s.', [cMetaClaimType_Claim]);

  // Maximum of two Roles
  if Roles.Count>2
     then EUsr('AnnotationTypes cannot have more than two Roles.');
  end;

function TcAnnotationType.GetKind: TcAnnotationKind;
  begin
  result := fAnnotationKind;
  end;

function TcAnnotationType.GetKindAsString: string;
  begin
  result := AnnotationKindToKindStr(fAnnotationKind);
  end;

function TcAnnotationType.Add(aSession: TcSession; aTupleStr: string; aValidFrom, aValidUntil: TcTime): TcAnnotation;
  begin
  result := ClaimSetList.AddAnnotation(aSession, aTupleStr, aValidFrom, aValidUntil);
  end;


{ TcClass }

constructor TcClass.Create(aEngine: TcEngine; aMainClaimType: TcClaimType);
  begin
  inherited Create(aEngine, aMainClaimType.Name);
  fObjectSetList := TcObjectSetList.Create(Self);
  fMainClaimType := aMainClaimType;
  fGroupedClaimTypes := TcElementList<TcClaimType>.Create(aEngine, false {Not owned}, false {Not sorted});
  iProcessMainClaimType;
  fPluralName := aMainClaimType.PluralName;
  end;

destructor TcClass.Destroy;
  begin
  FreeAndNil(fObjectSetList);
  FreeAndNil(fGroupedClaimTypes);
  inherited;
  end;

procedure TcClass.AfterInit;
  begin
  inherited;
  end;

function TcClass.GetTupleSet(const aIndex: integer): TcTupleSet;
  begin
  result := fObjectSetList[aIndex];
  end;

function TcClass.GetTupleSetListCount: integer;
  begin
  result := fObjectSetList.Count;
  end;

procedure TcClass.iProcessMainClaimType;
  var
    i: integer;
    lOriginalRole, lNewRole: TcRole;
    lUnicity: TcUnicity;
  begin
  // Clone Roles from MainClaimType
  for i := 0 to fMainClaimType.Roles.Count-1 do
    begin
    lOriginalRole := fMainClaimType.Roles[i];
    lNewRole := TcRole.Create(Self, lOriginalRole.Name, lOriginalRole.Type_.Name, i);
    lNewRole.Necessity := mMustHave;
    Roles.Add(lNewRole);
    end;

  // Clone Identity constraint
  Identity.AddRoles(fMainClaimType.Identity.AsString);

  // Clone Unicity constraints
  for i := 0 to fMainClaimType.UnicityList.Count-1 do
    begin
    lUnicity := TcUnicity.Create(Self);
    lUnicity.AddRoles(fMainClaimType.UnicityList[i].AsString);
    UnicityList.Add(lUnicity.ID, lUnicity);
    end;
  end;

procedure TcClass.iProcessGroupedClaimType(aClaimType: TcClaimType; aReferencingRole: TcRole);
  var
    i: integer;
    lOriginalRole, lNewRole: TcRole;
    lUnicity: TcUnicity;
  begin
  if aClaimType.Roles.Count>2
     then EInt('TcClass.iProcessGroupedClaimType', 'Cannot group ClaimTypes with more than two roles.');

  // Clone the role from the ClaimType that does not reference the MainClaimType
  for i := 0 to aClaimType.Roles.Count-1 do
    begin
    lOriginalRole := aClaimType.Roles[i];
    if lOriginalRole = aReferencingRole then continue; // Ignore the referencing role
    lNewRole := TcRole.Create(Self, lOriginalRole.Name, lOriginalRole.Type_.Name, Roles.Count);
    if aReferencingRole.IsTotalReference
       then lNewRole.Necessity := mMustHave
       else lNewRole.Necessity := mCouldHave;

    Roles.Add(lNewRole);
    end;

  // Clone Unicity constraints
  //
  // The identity is on the Referencing Role.
  // Unicity's must be on other Roles. So every unicity can be added.
  for i := 0 to aClaimType.UnicityList.Count-1 do
    begin
    lUnicity := TcUnicity.Create(Self);
    lUnicity.AddRoles(aClaimType.UnicityList[i].AsString);
    UnicityList.Add(lUnicity.ID, lUnicity);
    end;
  end;

procedure TcClass.AddClaimType(aClaimType: TcClaimType; aReferencingRole: TcRole);
  begin
  iProcessGroupedClaimType(aClaimType, aReferencingRole);
  fGroupedClaimTypes.Add(aClaimType.Name, aClaimType);
  end;

function TcClass.SequenceNrOfClaimType(aClaimType: TcClaimType): integer;
  begin
  // Sequence nr of MainClaimType is 0
  if MainClaimType = aClaimType
     then begin
          result := 0;
          exit;
          end;

  // Grouped ClaimType?
  result := GroupedClaimTypes.IndexOfObject(aClaimType); // Returns -1 if not found
  if result>=0
     then result := result + 1; // Sequence nr of GroupedClaimType is index in list + 1
  end;


{ TcRole }

constructor TcRole.Create(aRelationType: TcRelationType; aName, aTypeName: string; aSequenceNr: integer);
  begin
  inherited Create(aRelationType.Engine, aName);

  if Trim(aName)=''
     then EUsr('Role name cannot be empty');

  if (aName[1]=cMetaChar) and not IsMetaRoleName(aName)
     then EUsr('The prefix ''%s'' is reserved to reference meta constructs.', [cMetaChar]);

  fRelationType := aRelationType;
  fUnresolvedTypeName := aTypeName;
  fSequenceNr := aSequenceNr;
  fIsTotal := false;
  fNecessity := mUnknown;
  ResolveType;
  end;

function TcRole.GetIsMetaRole: boolean;
  begin
  result := StartsWithChar(Name, cMetaChar);
  end;

procedure TcRole.ResolveType;
  var
    lType: TcType;
    lClaimType: TcClaimType;
  begin
  if CompareText(Name, cMetaRoleName_Claim)=0
     then begin
          if not ( (RelationType is TcAnnotationType) or RelationType.IsMetaType )
             then EUsr('References to %s are only allowed in AnnotationTypes and Meta ClaimTypes.', [Type_.Name]);
          end;

  lType := Engine.ConceptualTypes.Find(fUnresolvedTypeName);
  if not Assigned(lType)
    then EUsr('Role %s of %s references unknown type %s.',
           [Name, RelationType.Name, fUnresolvedTypeName])
    else begin
         fType := lType;
         if lType is TcClaimType
            then lClaimType := lType as TcClaimType
            else lClaimType := nil;

         if Assigned(lClaimType)
            then fUnresolvedTypeName := '';

         if Assigned(lClaimType) and (not lClaimType.IsObjectType)
            then EUsr('Role %s of %s references %s. ' +
                   'This type cannot be referenced since a NestedExpressionTemplate is missing',
                   [RelationType.Name, Name, fUnresolvedTypeName]);

         // If this is a ClaimType with information about Operations,
         // add it to the ClaimTypes of the Operation.
         if CompareText(Name, cMetaRoleName_Operation)=0
            then begin
                 if (RelationType is TcAnnotationType)
                    then EUsr('AnnotationsTypes are not supported on Operations.');

                 if not RelationType.IsMetaType // Prevent adding meta claimtypes
                    and (RelationType is TcClaimType) // Do not add classes
                    then Engine.OperationClaimTypes.Add(RelationType.Name, RelationType as TcClaimType);
                 end;
         end;
  end;

procedure TcRole.ChangeType(aNewType: TcRelationType);
  begin
  fType := aNewType;
  if not IsReference
     then fIsTotal := false;
  end;

function TcRole.GetIsReference: boolean;
  begin
  result := not (fType is TcLabelType);
  end;

function TcRole.GetIsTotalReference: boolean;
  begin
  result := IsReference and (fIsTotal);
  end;

procedure TcRole.SetIsTotalReference(aState: boolean);
  begin
  if aState = fIsTotal then exit;

  if not IsReference
     then EUsr('Cannot make a lexical role total.');

  fIsTotal := aState;
  end;


{ TcRoles }

constructor TcRoles.Create(aRelationType: TcRelationType);
  begin
  inherited Create(aRelationType.Engine, true {Owned}, false {Not sorted});
  fRelationType := aRelationType;
  end;

procedure TcRoles.Add(aRole: TcRole);
  begin
  fList.Add(aRole.Name, aRole);
  end;


{ TcUnicity }

constructor TcUnicity.Create(aRelationType: TcRelationType);
  begin
  inherited Create(aRelationType.Engine, false {Not owned}, false {Not sorted});
  fRelationType := aRelationType;
  end;

procedure TcUnicity.Add(aRole: TcRole);
  begin
  fList.Add(aRole.Name, aRole);
  end;

procedure TcUnicity.AddRoles(aCommaSeparatedListOfRoleNames: string);
  var
    i: integer;
    lRoleNames: TStringlist;
    lRoleName: String;
    lRole: TcRole;
    lStr: string;
  begin
  lRoleNames := StringToStringlistTrimmed(aCommaSeparatedListOfRoleNames, ',');
  try
    for i := 0 to lRoleNames.Count-1 do
      begin
      lRoleName := lRoleNames[i];
      lRole := RelationType.Roles.Find(lRoleName);
      if Assigned(lRole)
        then Self.Add(lRole)
        else begin
             lStr := '';
             if Self is TcUnicity
                then lStr := 'unicity rule'
             else if Self is TcIdentity
                then lStr := 'identity';

             EUsr('Could not create %s for %s. Unknown Role: %s.', [lStr, RelationType.Name, lRoleName]);
             end;
      end;
  finally
    lRoleNames.Free;
    end;
  end;

function TcUnicity.AsString: string;
  var
    i: integer;
  begin
  result := '';
  for i := 0 to Count-1 do
    begin
    if result <>'' then result := result + ', ';
    result := result + Strings[i];
    end;
  end;


{ TcExpressionTemplate }

constructor TcExpressionTemplate.Create(aClaimType: TcClaimType; aTemplate: string; aType: TcExpressionTemplateType);
  begin
  inherited Create(aClaimType.Engine);
  fClaimType := aClaimType;
  fOriginalTemplate := aTemplate;
  fTemplate := aTemplate;
  fType := aType;
  fParts := TcElementList<TcRole>.Create(aClaimType.Engine, false {Not owned}, false {Not sorted});
  Init;
  end;

destructor TcExpressionTemplate.Destroy;
  begin
  FreeAndNil(fParts);
  inherited Destroy;
  end;

procedure TcExpressionTemplate.Init;
  var
    i: integer;
    lTokens: TStringlist;
    lToken, lRoleName, lTypeName: string;
    lRole: TcRole;
    lRoleSequenceNr: integer;
  begin
  lTokens := TStringlist.Create;
  try
    // Tokenize
    ExpressionTemplateStrToTokens (Template, lTokens);

    // Create or find Roles
    lRoleSequenceNr := 0;
    for i := 0 to lTokens.Count-1 do
      begin
      lToken := lTokens[i];
      if lToken[1] <> cRoleOpen // Textual part
        then begin
             Parts.Add(lToken, nil);
             continue;
             end;

      lToken := Copy(lToken, 2, Length(lToken)-2); // Remove brackets

      if Pos(':', lToken)=0
        then begin
             // No type name specified, assume Type name = Role name
             lRoleName := lToken;
             lTypeName := lToken;
             end
        else SplitString(lToken, ':', lRoleName, lTypeName);

      // Handle Roles
      if Type_ = ettFull
         then begin
              // Create Role
              lRole := TcRole.Create(ClaimType, lRoleName, lTypeName, lRoleSequenceNr);
              ClaimType.Roles.Add(lRole);
              inc(lRoleSequenceNr);
              end
         else begin
              // Find Role
              lRole := ClaimType.Roles.Find(lRoleName);
              if not Assigned(lRole) then EUsr('Undefined Role %s in NestedExpressionTemplate "%s".', [lRoleName, Template]);
              end;

      // Update token
      lTokens[i] := cRoleOpen + lRoleName + cRoleClose;
      Parts.Add(lRoleName, lRole);
      end;

    // Recreate template from tokenlist
    fTemplate := StringlistToString(lTokens, '');

  finally
    lTokens.Free;
    end;
  end;

procedure TcExpressionTemplate.iGetExpression(aTemplate: TcExpressionTemplate; aClaim: TcClaim; var aExpression: string);

  function iGetTupleValueForRole(aRoleName: string): string;
    begin
    result := aClaim.RoleValues.Values[aRoleName];
    end;

  var
    i: integer;
    lRole: TcRole;
    lTupleValue: string;
    lElement: TcElement;
    lClaim: TcClaim;
    lClaimType: TcClaimType;
  begin
  if aClaim.RoleValues.Count <> aClaim.RelationType.Roles.Count
     then EInt('TcExpressionTemplate.iGetExpression', 'Number of Tuples does not match the number of Roles');

  for i := 0 to aTemplate.Parts.Count-1 do
    begin
    if Assigned(aTemplate.Parts.Objects[i])
       then begin
            // Part is a Role
            lRole := TcRole(aTemplate.Parts.Objects[i]);
            lTupleValue := iGetTupleValueForRole(lRole.Name);
            if (lRole.Type_ is TcLabelType)
               then begin
                    // LabelType -> Substitute Tuple value
                    aExpression := aExpression + lTupleValue;
                    end
            else if lRole.IsMetaRole
               then begin
                    // Meta Role: Do not resolve, simple include the reference
                    aExpression := aExpression + lTupleValue;
                    end
               else begin
                    // ClaimType -> Tuple value is reference to a Claim
                    lElement := Engine.FindElementByRefIDStr(lTupleValue);
                    if not (lElement is TcClaim)
                       then EInt('TcExpressionTemplate.iGetExpression',
                         'Role %s was expected to reference a Claim.', [lRole.Name]);
                    lClaim := lElement as TcClaim;

                    // Recursion: Get nested expression
                    if not (lClaim.RelationType is TcClaimType)
                       then EInt('TcExpressionTemplate.iGetExpression',
                         'Role %s was expected to reference a Claim.', [lRole.Name]);

                    lClaimType := (lClaim.RelationType as TcClaimType);
                    iGetExpression(lClaimType.NestedExpressionTemplate, lClaim, aExpression);
                    end;
            end
       else begin
            // Part is text
            aExpression := aExpression + aTemplate.Parts.Strings[i];
            end;
    end;
  end;

function TcExpressionTemplate.GetExpression(aClaim: TcClaim): string;
  var
    lExpression: String = '';
  begin
  // Resolve expression (recursive)
  iGetExpression(Self, aClaim, lExpression);

  // Format expression
  if (Length(lExpression)>0)
    then begin
         // Uppercase first character
         lExpression[1] := Uppercase(lExpression[1])[1];

         // End with colon
         if lExpression[Length(lExpression)]<>'.'
           then lExpression := lExpression + '.';
         end;

  result := lExpression;
  end;


{ TcInstance }

constructor TcInstance.Create(aSession: TcSession);
  begin
  inherited Create(aSession.Engine);
  fAnnotations := TcElementList<TcAnnotation>.Create(aSession.Engine, false {Not owned}, true {Sorted});

  if not Assigned(aSession.Transaction)
     then EInt('TcInstance.Create', 'Transaction required');
  end;

destructor TcInstance.Destroy;
  begin
  FreeAndNil(fAnnotations);
  inherited;
  end;

procedure TcInstance.Add(aSession: TcSession; aAnnotation: TcAnnotation);
  begin
  fAnnotations.Add(aAnnotation.ID, aAnnotation);
  end;

{ TcOperation }

constructor TcOperation.Create(aSession: TcSession; aTransaction: TcTransaction; aName: string);
  begin
  inherited Create(aSession);
  fIdentifier := ToIdentifier(aName);
  fName := aName;
  fForValidation := false;
  fRegisteredAt := aTransaction.StartTime;
  fValidFrom := cNoTime;
  fValidUntil := cNoTime;
  fClaims := TcElementList<TcClaim>.Create(aSession.Engine, false {Not owned}, false {Not sorted});
  end;

destructor TcOperation.Destroy;
  begin
  FreeAndNil(fClaims);
  inherited;
  end;

procedure TcOperation.ImmediateDeleteClaim(aClaim: TcClaim);
  var
    lIndex: integer;
  begin
  lIndex := Claims.IndexOfObject(aClaim);
  if (lIndex>=0)
     then Claims.Delete(lIndex);
  end;

function TcOperation.FindClaim(aClaimTypeName, aIdentityStr: string): TcClaim;
  var
    c: integer;
    lClaim: TcClaim;
  begin
  result := nil;
  for c := 0 to fClaims.Count-1 do
    begin
    lClaim := fClaims[c];
    if CompareText(lClaim.ClaimType.Name, aClaimTypeName)=0
       then begin
            if (aIdentityStr='') or (lClaim.IdentityStr = aIdentityStr)
               then begin
                    result := lClaim;
                    exit;
                    end;
            end;
    end;
  end;


{ TcTuple }

constructor TcTuple.Create(aSession: TcSession; aRelationType: TcRelationType);
  begin
  inherited Create(aSession);
  fRelationType := aRelationType;
  Assert(Assigned(aSession.Transaction)); // Ancestor requires a transaction
  fTransactionID := aSession.Transaction.ID;
  fOperation := aSession.Operation;
  fIdentityStr := '';
  fValueStr := '';
  fRoleValues := TcKeyValueObjectList<TcRole>.Create(false {Not owned}, true {Sorted});

  fRegisteredAt := cNoTime;
  fExpiredAt := cNoTime;
  fValidFrom := cNoTime;
  fValidUntil := cNoTime;
  end;

destructor TcTuple.Destroy;
  begin
  FreeAndNil(fRoleValues);
  inherited;
  end;

function TcTuple.GetValueBySortedRoleOrder (const aIndex: integer): string;
  begin
  result := RoleValues.ValueByIndex(aIndex);
  end;

function TcTuple.GetTimeAspects: string;
  begin
  result := 'Registered at: ' + IntToStr(RegisteredAt);
  if ExpiredAt <> cEndOfTime then result := result + ', expired at: ' + IntToStr(ExpiredAt);
  if ValidFrom <> cStartOfTime then result := result + ', valid from: ' + IntToStr(ValidFrom);
  if ValidUntil <> cEndOfTime then result := result + ' valid until: ' + IntToStr(ValidUntil);
  end;

function TcTuple.GetTimeAspectsCompact: string;
  var
    R, E, VF, VU: string;
  begin
  R := IntToStr(RegisteredAt);
  if ExpiredAt = cEndOfTime then E := '-' else E := IntToStr(ExpiredAt);
  if ValidFrom = cStartOfTime then VF := '-' else VF := IntToStr(ValidFrom);
  if ValidUntil = cEndOfTime then VU := '-' else VU := IntToStr(ValidUntil);

  result := Format('R:%s E:%s VF:%s VU:%s', [R, E, VF, VU]);;
  end;

function TcTuple.GetValueStrWithoutIdentity: string;
  var
    i: integer;
    lRole, lRoleInIdentity: TcRole;
    lStrs: TStringlist;
  begin
  result := '';
  lStrs := TStringlist.Create;
  try
    for i := 0 to RoleValues.Count-1 do
      begin
      lRole := TcRole(RoleValues.Objects[i]);
      lRoleInIdentity := RelationType.Identity.Find(lRole.Name);
      if lRole <> lRoleInIdentity
        then // Only add roles NOT in identity
             lStrs.Add(GetValueBySortedRoleOrder(i));
      end;

    if lStrs.Count>0
      then result := StringlistToString(lStrs, ', ');

  finally
    lStrs.Free;
    end;
  end;

function TcTuple.TupleStrInRoleOrder(aRelationType: TcRelationType; aTuple: TcKeyValueObjectList<TcRole>): string;
  var
    r: integer;
    lRole: TcRole;
    lStringlist: TStringlist;
  begin
  result := '';
  lStringlist := TStringlist.Create;
  try
    for r := 0 to aRelationType.Roles.Count-1 do
      begin
      lRole := aRelationType.Roles[r];
      lStringlist.Add(aTuple.Values[lRole.Name]);
      end;
    result := StringListToString(lStringlist, ',');
  finally
    lStringlist.Free;
    end;
  end;


{ TcClaim }

constructor TcClaim.Create(aSession: TcSession; aClaimType: TcClaimType; aValueStr: string;
              aValidFrom, aValidUntil: TcTime; aPurpose: TcClaimPurpose);
  var
    i: integer;
    lValues: TStringlist;
    lValue: string;
    lIdentity: TcIdentity;
    lRole: TcRole;
    lValidFrom, lValidUntil: TcTime;
  begin
  inherited Create(aSession, aClaimType);
  fClaimType := aClaimType;
  fPurpose := aPurpose;
  fSourceClaim := ID;
  fMVCCVersionID := ID;
  fMVCCMarkedForCleaningBy := cNoID;

  lValidFrom := aValidFrom;
  if (lValidFrom = cStartOfTime) and Assigned(aSession.Operation) and (aSession.Operation.ValidFrom <> cNoTime)
     then lValidFrom := aSession.Operation.ValidFrom;
  if lValidFrom = cNoTime
     then lValidFrom := cStartOfTime;

  lValidUntil := aValidUntil;
  if (lValidUntil= cEndOfTime) and Assigned(aSession.Operation) and (aSession.Operation.ValidUntil <> cNoTime)
     then lValidUntil := aSession.Operation.ValidUntil;
  if lValidUntil = cNoTime
     then lValidUntil := cEndOfTime;

  // OriginalValidFrom and ValidUntil need to be registered before handling the
  // time aspects of the tuple (see code below). The original values are passed
  // to nested tuples that are created during iProcessValueStr. Nested
  // ClaimTypes may have different temporal Settings and should use the original
  // values.
  fOriginalValidFrom := lValidFrom;
  fOriginalValidUntil := lValidUntil;

  // Process Values and derives the ValueStr
  iProcessValues(aSession, aValueStr);

  // Generate Identity Str
  lValues := TStringlist.Create;
  try
    lIdentity := RelationType.Identity;
    for i := 0 to lIdentity.Count-1 do
      begin
      lRole := lIdentity[i];
      lValue := RoleValues.Values[lRole.Name];
      lValues.Add(lValue);
      end;
    fIdentityStr := StringListToString(lValues, cIdentityValueSeparator);
  finally
    lValues.Free;
    end;

  // Store Claim within the Operation that created it
  if Assigned(fOperation) and (fPurpose = cpSource)
     then fOperation.Claims.Add(fIdentityStr, Self);

  // Time aspects
  if RelationType.RecordTransactionTime
    then begin
         if aSession.Engine.ReplayMode
            then fRegisteredAt := aSession.Engine.SystemTime
            else fRegisteredAt := aSession.Transaction.StartTime;
         fExpiredAt := cEndOfTime;
         end
    else begin
         fRegisteredAt := cStartOfTime;
         fExpiredAt := cEndOfTime;
         end;

  if RelationType.RecordValidTime
    then begin
         fValidFrom := lValidFrom;
         fValidUntil := lValidUntil;
         end
    else begin
         fValidFrom := cStartOfTime;
         fValidUntil := cEndOfTime;
         end;

  Engine.RegisterValidPeriod(lValidFrom, lValidUntil);
  end;

function TcClaim.GetIsSource: boolean;
  begin
  result := fPurpose = cpSource;
  end;

function TcClaim.GetIsTemporalCopy: boolean;
  begin
  result := fPurpose = cpTemporalCopy;
  end;

function TcClaim.GetIsTemporalAnnotation: boolean;
  begin
  result := fPurpose = cpTemporalAnnotation;
  end;

function TcClaim.GetIsMVCCCopy: boolean;
  begin
  result := fPurpose = cpMVCCCopy;
  end;

function TcClaim.iGetOperationRefIDStr(aSession: TcSession): string;
  var
    lOperation: TcOperation;
  begin
  lOperation := aSession.Operation;
  if not Assigned(lOperation)
     then EUsr('No operation active.');
  result := cRefChar + IntToStr(lOperation.ID);
  end;

function TcClaim.iTupleToIdentityStr(aClaimType: TcClaimType; aTuple: TcKeyValueObjectList<TcRole>): string;
  var
    r: integer;
    lRole: TcRole;
    lStringlist: TStringlist;
  begin
  result := '';
  lStringlist := TStringlist.Create;
  try
    for r := 0 to aClaimType.Identity.Count-1 do
      begin
      lRole := aClaimType.Identity[r];
      lStringlist.Add(aTuple.Values[lRole.Name]);
      end;
    result := StringListToString(lStringlist, cIdentityValueSeparator);
  finally
    lStringlist.Free;
    end;
  end;

function TcClaim.iProcessNestedValues(aSession: TcSession; aClaimType: TcClaimType; aValues: TStringlist; var aValueIndex: integer): TcClaim;
  var
    i: integer;
    lTuple: TcKeyValueObjectList<TcRole>;
    lTemplate: TcExpressionTemplate;
    lRole: TcRole;
    lClaim: TcClaim;
    lClaimType: TcClaimType;
    lValue: string;
    lIdentityStr: string;
    lTupleStr: string;
  begin
  lTemplate := aClaimType.NestedExpressionTemplate;
  if not Assigned(lTemplate) then EUsr('Referenced ClaimType %s needs a NestedExpressionTemplate.', [aClaimType.Name]);

  DebugIncIndent(dtTuples);
  Debug(dtTuples, lTemplate.Template);

  lTuple := TcKeyValueObjectList<TcRole>.Create(false, false);
  try
    // Walk through every part of the template
    for i := 0 to lTemplate.Parts.Count-1 do
      begin
      // Ignore text parts
      if not Assigned(lTemplate.Parts.Objects[i])
         then continue;

      // Process roles
      lRole := TcRole(lTemplate.Parts.Objects[i]);
      if (lRole.Type_ is TcLabelType)
         then begin
              // Labeltype -> Concrete tuple value
              if aValueIndex>aValues.Count-1
                 then EUsr('Value missing for Role %s in ClaimType %s.', [lRole.Name, aClaimType.Name]);

              Debug(dtTuples, '  '+lRole.Name+': '+aValues[aValueIndex]);
              lTuple.Add(lRole.Name, aValues[aValueIndex], lRole);
              Inc(aValueIndex);
              end
         else begin
              // Role is a concrete value that belongs to another ClaimType
              lClaimType := (lRole.Type_ as TcClaimType);

              Debug(dtTuples, '  '+lRole.Name+': resolving ...');
              lClaim := iProcessNestedValues(aSession, lClaimType, aValues, aValueIndex);
              Debug(dtTuples, '    ClaimID: '+IntToStr(lClaim.ID));

              if not Assigned(lClaim)
                 then EUsr('Invalid Claim reference. Role %s in ClaimType %s.', [lRole.Name, aClaimType.Name]);

              // Add a reference to the claim
              lValue := cRefChar + IntToStr(lClaim.ID);
              lTuple.Add(lRole.Name, lValue, lRole);
              end;
      end;

    // Is this tuple already present in the claimtype?
    lIdentityStr := iTupleToIdentityStr(aClaimType, lTuple);
    result := aClaimType.GetClaimFromObjectType(lIdentityStr);

    // Add the tuple if its not present
    if not Assigned(result)
       then begin
            lTupleStr := TupleStrInRoleOrder(aClaimType, lTuple);
            Debug(dtTuples, '  Adding tuple: '+lTupleStr);
            DebugIncIndent(dtTuples);
            result := aClaimType.Add(aSession, lTupleStr, fOriginalValidFrom, fOriginalValidUntil);
            DebugDecIndent(dtTuples);
            end;

  finally
    lTuple.Free;
    DebugDecIndent(dtTuples);
    end;
  end;

procedure TcClaim.iProcessValues(aSession: TcSession; aValueStr: string);
  var
    i, lValueIndex: integer;
    lID: integer;
    lValues: TStringlist;
    lReferencingTuple: boolean;
    lTemplate: TcExpressionTemplate;
    lRole: TcRole;
    lElement: TcElement;
    lClaim: TcClaim;
    lClaimType: TcClaimType;
    lValue: string;
    lOperationID: integer;
  begin
  lValueIndex := 0;
  lValues := StringToStringlistTrimmed(aValueStr, ',');
  try
    // Are there values?
    if lValues.Count=0
       then EUsr('Tuple without values.');

    // Are there reference values in the tuple?
    lReferencingTuple := false;
    for i := 0 to lValues.Count-1 do
      if (Length(lValues[i])>0) and (lValues[i][1]=cRefChar)
         then begin
              lReferencingTuple := true;
              break;
              end;

    // Resolve Meta Values
    for i := 0 to lValues.Count-1 do
      if (Length(lValues[i])>0) and (lValues[i][1]=cMetaChar)
         then begin
              if CompareText(lValues[i], cMetaValueName_CurrentOperation)=0
                 then begin
                      lValues[i] := iGetOperationRefIDStr(aSession);
                      // This would make the tuple a referencing tuple.
                      // That would require all other values to be referencing as well.
                      // We do not change lReferencingTuple and handle meta roles
                      // in both the referencing and the concrete value loop.
                      end
              else if CompareText(lValues[i], cMetaValueName_Ask)=0
                 then begin
                      lID := CheckOrAskClaimReference(cMetaValueName_Ask);
                      lValues[i] := cRefChar + IntToStr(lID);
                      lReferencingTuple := true; // This make the tuple a referencing tuple
                      end
              else EUsr('Invalid meta value: %s', [lValues[i]]);
              end;

    // Walk through every part of the template
    lTemplate := ClaimType.ExpressionTemplate;
    Debug(dtTuples, lTemplate.Template);
    DebugIncIndent(dtTuples);
    for i := 0 to lTemplate.Parts.Count-1 do
      begin
      // Ignore text parts
      if not Assigned(lTemplate.Parts.Objects[i])
         then continue;

      // Process Roles
      lRole := TcRole(lTemplate.Parts.Objects[i]);
      if lValueIndex>lValues.Count-1
         then EUsr('Value missing for Role %s.', [lRole.Name]);

      lValue := lValues[lValueIndex];
      if (lRole.Type_ is TcLabelType)
         then begin
              // LabelType -> Requires a concrete Tuple value
              Debug(dtTuples, '  '+lRole.Name+': '+lValue);
              RoleValues.Add(lRole.Name, lValue, lRole);
              Inc(lValueIndex);
              end
      else if lReferencingTuple
         then begin
              // Role references an Element

              // Validate the reference
              lElement := Engine.FindElementByRefIDStr(lValue);
              if not Assigned(lElement)
                 then EUsr('Invalid reference. Role %s.', [lRole.Name]);

              if (lElement is TcClaim)
                 then begin
                      // Reference to Claim
                      lClaim := lElement as TcClaim;
                      if (lClaim.ClaimType <> lRole.Type_) and not lRole.IsMetaRole
                         then EUsr('Tuple references a Claim of type %s, while Role is of type %s.',
                                [lClaim.ClaimType.Name, lRole.Type_.Name]);
                      end
              else if (lElement is TcOperation) and (CompareText(lRole.Name, cMetaRoleName_Operation)=0)
                 then begin
                      // Reference to current operation
                      lOperationID := StrToIntDef(Copy(lValue, 2, MaxInt), -1); // Skip the RefChar
                      if lOperationID<0 then EInt('TcClaim.iProcessValues', 'Invalid operation reference');
                      if not Assigned(aSession.Operation) then EInt('TcClaim.iProcessValues', 'No operation active');
                      if lOperationID <> aSession.Operation.ID then EInt('TcClaim.iProcessValues', 'Can only reference current operation');
                      end
                 else EUsr('Invalid reference. Role %s.', [lRole.Name]);

              // Do not resolve the reference. Just add it.
              RoleValues.Add(lRole.Name, lValue, lRole);
              Inc(lValueIndex);
              end
      else begin
           if (CompareText(lRole.Name, cMetaRoleName_Operation)=0)
              then begin
                   // Reference to current operation
                   lOperationID := StrToIntDef(Copy(lValue, 2, MaxInt), -1); // Skip the RefChar
                   if lOperationID<0 then EInt('TcClaim.iProcessValues', 'Invalid operation reference');
                   if not Assigned(aSession.Operation) then EInt('TcClaim.iProcessValues', 'No operation active');
                   if lOperationID <> aSession.Operation.ID then EInt('TcClaim.iProcessValues', 'Can only reference current operation');

                   // Do not resolve the reference. Just add it.
                   RoleValues.Add(lRole.Name, lValue, lRole);
                   Inc(lValueIndex);
                   end
              else begin
                   // Role is a concrete value that belongs to another ClaimType
                   lClaimType := (lRole.Type_ as TcClaimType);

                   Debug(dtTuples, '  '+lRole.Name+': resolving ...');
                   DebugIncIndent(dtTuples);
                   lClaim := iProcessNestedValues(aSession, lClaimType, lValues, lValueIndex);
                   DebugDecIndent(dtTuples);
                   Debug(dtTuples, '    ClaimID: '+IntToStr(lClaim.ID));

                   if not Assigned(lClaim)
                      then EUsr('Invalid Claim reference. Role %s.', [lRole.Name]);

                   // Add a reference to the claim
                   lValue := cRefChar + IntToStr(lClaim.ID);
                   RoleValues.Add(lRole.Name, lValue, lRole);
                   end;

           end;
      end;

    // Build TupleStr
    fValueStr := TupleStrInRoleOrder(ClaimType, RoleValues);
    Debug(dtTuples, '  Stored tuple: '+ValueStr);
    DebugDecIndent(dtTuples);

  finally
    lValues.Free;
    end;
  end;

function TcClaim.GetExpression: string;
  begin
  result := ClaimType.ExpressionTemplate.GetExpression(self);
  end;

function TcClaim.GetExpressionFmt: string;
  begin
  result := '"'+CheckTrailingDot(GetExpression)+'"';
  end;

function TcClaim.TupleSet: TcTupleSet;
  begin
  result := fClaimSet;
  end;

procedure TcClaim.iAdd(aSession: TcSession; aAnnotation: TcAnnotation);
  begin
  Annotations.Add(aAnnotation.ID, aAnnotation);

  // Propagate annotations on SourceClaims to DerivedClaims
  if IsSource
     then ClaimSet.DeriveClaims(aSession);
  end;

procedure TcClaim.Add(aSession: TcSession; aAnnotation: TcAnnotation);
  begin
  try
    if IsSource
       then begin
            iAdd(aSession, aAnnotation);
            exit; // Do not apply start/end/expire etc to source claims
            end;

    case aAnnotation.Kind of
      akExpired:
         ClaimSet.Expire(aSession, Self, aAnnotation);

      akStarted:
        begin
        if aAnnotation.ValidFrom = cEndOfTime
           then EUsr('The ''From'' parameter is required.');
        ClaimSet.Start(aSession, Self, aAnnotation, aAnnotation.ValidFrom);
        end;

      akEnded:
        begin
        if aAnnotation.ValidUntil = cEndOfTime
           then EUsr('The ''Until'' parameter is required.');
        ClaimSet.End_(aSession, Self, aAnnotation, aAnnotation.ValidUntil);
        end;

      akDeleted:
        begin
        ClaimSet.Delete(aSession, Self, aAnnotation);
        exit; // Do not add Annotation
        end;
      end;

    iAdd(aSession, aAnnotation);

  finally
    if Assigned(gRadioStation)
       then begin
            gRadioStation.BroadCast(cRadioEvent_ClaimSet_Changed, ClaimSet);
            gRadioStation.BroadCast(cRadioEvent_Session_ClaimSet_Changed, aSession);
            end;
    end;
  end;

function TcClaim.iCopy(aSession: TcSession; aValidFrom, aValidUntil: TcTime; aPurpose: TcClaimPurpose): TcClaim;
  var
    i: integer;
    lCopy: TcClaim;
    lAnnotation, lAnnotationCopy: TcAnnotation;
  begin
  lCopy := TcClaim.Create(aSession, ClaimType, ValueStr, aValidFrom, aValidUntil, aPurpose);
  lCopy.ClaimSet := ClaimSet;
  lCopy.fOperation := Self.Operation; // Retain the original operation

  lCopy.SourceClaim := Self.SourceClaim;
  if aPurpose = cpMVCCCopy
     then begin
          lCopy.MVCCVersionID := Self.MVCCVersionID;
          lCopy.RegisteredAt := RegisteredAt;
          lCopy.ExpiredAt := ExpiredAt;
          end;

  for i := 0 to Annotations.Count-1 do
    begin
    lAnnotation := Annotations[i];
    lAnnotationCopy := lAnnotation.iCopy(aSession, Self, lCopy, lAnnotation.ValidFrom, lAnnotation.ValidUntil, aPurpose);
    lCopy.iAdd(aSession, lAnnotationCopy);
    end;
  result := lCopy;
  end;

function TcClaim.GetInDoubt: boolean;
  var
    i: integer;
    lAnnotation: TcAnnotation;
  begin
  result := false;
  for i := 0 to Annotations.Count-1 do
    begin
    lAnnotation := Annotations[i];
    if lAnnotation.Kind = akDoubt
       then begin
            result := true;
            exit;
            end;
    end;
  end;

function TcClaim.GetSourceClaimRefIDStr: string;
  begin
  result := Format('%s%d', [cRefChar, SourceClaim]);
  end;


{ TcAnnotation }

constructor TcAnnotation.Create(aSession: TcSession; aAnnotationType: TcAnnotationType; aValueStr: string;
              aValidFrom, aValidUntil: TcTime; aPurpose: TcClaimPurpose);
  begin
  inherited Create(aSession, aAnnotationType, aValueStr, aValidFrom, aValidUntil, aPurpose);
  fAnnotationType := aAnnotationType;
  fDerivedFrom := cNoID;
  end;

function TcAnnotation.GetKind: TcAnnotationKind;
  begin
  result := fAnnotationType.Kind;
  end;

procedure TcAnnotation.ChangeReferenceInValueStr(var aValueStr: string; aOldID, aNewID: integer);
  var
    i: integer;
    lOldRef, lNewRef: string;
    lValues: TStringlist;
  begin
  lOldRef := cRefChar + IntToStr(aOldID);
  lNewRef := cRefChar + IntToStr(aNewID);

  lValues := StringToStringlistTrimmed(aValueStr, ',');
  for i := 0 to lValues.Count-1 do
    begin
    if CompareText(lValues[i], lOldRef)=0
       then lValues[i] := lNewRef;
    end;

  aValueStr := StringlistToString(lValues, ', ');
  end;

function TcAnnotation.iCopy(aSession: TcSession; aOldClaim, aNewClaim: TcClaim;
           aValidFrom, aValidUntil: TcTime; aPurpose: TcClaimPurpose): TcAnnotation;
  var
    i: integer;
    lCopy: TcAnnotation;
    lAnnotation: TcAnnotation;
    lNewValueStr: string;
  begin
  // Correct the ValueStr: References to the OldClaim should be changed into the NewClaim
  lNewValueStr := ValueStr;
  ChangeReferenceInValueStr(lNewValueStr, aOldClaim.ID, aNewClaim.ID);

  lCopy := TcAnnotation.Create(aSession, AnnotationType, lNewValueStr, aValidFrom, aValidUntil, aPurpose);

  if DerivedFrom <> cNoID
     then lCopy.DerivedFrom := DerivedFrom
     else lCopy.DerivedFrom := ID;

  // Copy nested annotations
  for i := 0 to Annotations.Count-1 do
    begin
    lAnnotation := Annotations[i];
    lCopy.Add(aSession, lAnnotation.iCopy(aSession, Self, lCopy, lAnnotation.ValidFrom, lAnnotation.ValidUntil, aPurpose));
    end;
  result := lCopy;
  end;


{ TcObject }

function TcObject.TupleSet: TcTupleSet;
  begin
  result := fObjectSet;
  end;

constructor TcObject.Create(aSession: TcSession; aClass: TcClass);
  begin
  inherited Create(aSession, aClass);
  fClass := aClass;
  fRoleValueClaims := TcKeyValueObjectList<TcClaim>.Create(false {Not owned}, true {Sorted});
  end;

destructor TcObject.Destroy;
  begin
  FreeAndNil(fRoleValueClaims);
  inherited;
  end;


{ TcTupleSet }

constructor TcTupleSet.Create (aRelationType: TcRelationType; aIdentityStr: String);
  begin
  inherited Create(aRelationType.Engine);
  fRelationType := aRelationType;
  fIdentityStr := aIdentityStr;
  end;


{ TcClaimSet }

constructor TcClaimSet.Create(aClaimType: TcClaimType; aIdentityStr: string);
  begin
  inherited Create(aClaimType, aIdentityStr);
  fClaimType := aClaimType;
  fIdentityStr := aIdentityStr;
  fSourceClaims := TcElementList<TcClaim>.Create(aClaimType.Engine, true {Owned}, false {Not sorted});
  fDerivedClaims := TcElementList<TcClaim>.Create(aClaimType.Engine, true {Owned}, false {Not sorted});
  fActions := TList<TcClaimSetAction>.Create;
  fLockedByTransactionID := cNoID;
  end;

destructor TcClaimSet.Destroy;
  var
    i: integer;
  begin
  for i := 0 to fActions.Count-1 do
    fActions[i].Free;
  FreeAndNil(fActions);
  FreeAndNil(fSourceClaims);
  FreeAndNil(fDerivedClaims);
  inherited Destroy;
  end;

function TcClaimSet.GetTuple(const aIndex: integer): TcTuple;
  begin
  result := fSourceClaims[aIndex];
  end;

function TcClaimSet.GetTupleCount: integer;
  begin
  result := fSourceClaims.Count;
  end;

procedure TcClaimSet.iAddAndInterpret(aSession: TcSession; aSourceClaim: TcClaim);
  var
    i: integer;
    lClaim, lNewClaim: TcClaim;
  begin
  // Is change allowed?
  if not ClaimType.AllowChange and (DerivedClaims.Count>0)
    then exit;

  // Replay?
  if aSession.Engine.ReplayMode
     then aSession.Engine.ReplayTime := aSourceClaim.RegisteredAt;

  // Use a copy of the Source Claim
  lNewClaim := aSourceClaim.iCopy(aSession, aSourceClaim.ValidFrom, aSourceClaim.ValidUntil, cpTemporalCopy);

  // Interpretation
  try
    // Interpretation on timeline allowed?
    if not ClaimType.TimeLineInterpretation
      then exit;

    // First record?
    if DerivedClaims.Count=0
        then exit;

    // Check all Claims and make 'room' for the new Claim
    for i := 0 to DerivedClaims.Count-1 do
      begin
      lClaim := DerivedClaims[i];

      // Ignore expired claims
      if lClaim.ExpiredAt < cEndOfTime
         then Continue;

      // Ignore claims marked for cleaning
      if lClaim.MVCCMarkedForCleaningBy <> cNoID
         then Continue;

      // New Claim fully overlaps existing Claim?
      // The new Claim fully overlaps the existing Claim.
      if ( (lNewClaim.ValidFrom <= lClaim.ValidFrom) and (lNewClaim.ValidUntil >= lClaim.ValidUntil) )
         then iExpire(lClaim, lNewClaim)

      // New Claim within existing Claim?
      // The new Claim is 'smaller' than the existing claim.
      else if ( (lNewClaim.ValidFrom > lClaim.ValidFrom) and (lNewClaim.ValidUntil < lClaim.ValidUntil) )
         then begin
              // Validity of the existing Claim should end at the start of the new Claim
              iCopyEnd(lClaim, lNewClaim, lNewClaim.ValidFrom);

              // Validity of the existing Claim should start at the end of the new Claim
              iCopyStart(lClaim, lNewClaim, lNewClaim.ValidUntil);

              iExpire(lClaim, lNewClaim)
              end

      // Start of new Claim in valid period of existing Claim?
      else if ( (lNewClaim.ValidFrom > lClaim.ValidFrom) and (lNewClaim.ValidFrom < lClaim.ValidUntil) )
         then begin
              // Validity of the existing Claim should end at the start of the new Claim
              iCopyEnd(lClaim, lNewClaim, lNewClaim.ValidFrom);

              iExpire(lClaim, lNewClaim);
              end

      // End of new Claim in valid period of existing Claim?
      else if ( (lNewClaim.ValidUntil > lClaim.ValidFrom) and (lNewClaim.ValidUntil < lClaim.ValidUntil) )
         then begin
              // Validity of the existing Claim should start at the end of the new Claim
              iCopyStart(lClaim, lNewClaim, lNewClaim.ValidUntil);
              iExpire(lClaim, lNewClaim);
              end;
      end;

  finally
    // Add the new Claim
    iAdd(lNewClaim);

    iProcessChanges(aSession);
    end;
  end;

procedure TcClaimSet.iAdd(aClaim: TcClaim);
  begin
  fActions.Add(TcClaimSetAction.Create(cakAdd, aClaim, nil));
  end;

procedure TcClaimSet.iStart(aClaim, aNewClaim: TcClaim; aAnnotation: TcAnnotation; aStartTime: TcTime);
  begin
  fActions.Add(TcClaimSetAction.Create(cakStart, aClaim, aNewClaim, aAnnotation, aStartTime));
  end;

procedure TcClaimSet.iEnd(aClaim, aNewClaim: TcClaim; aAnnotation: TcAnnotation; aEndTime: TcTime);
  begin
  fActions.Add(TcClaimSetAction.Create(cakEnd, aClaim, aNewClaim, aAnnotation, aEndTime));
  end;

procedure TcClaimSet.iExpire(aClaim, aNewClaim: TcClaim; aAnnotation: TcAnnotation = nil);
  begin
  fActions.Add(TcClaimSetAction.Create(cakExpire, aClaim, aNewClaim, aAnnotation));
  end;

procedure TcClaimSet.iDelete(aClaim: TcClaim; aAnnotation: TcAnnotation = nil);
  begin
  fActions.Add(TcClaimSetAction.Create(cakDelete, aClaim, nil, aAnnotation));
  end;

procedure TcClaimSet.iCopyEnd(aClaim, aNewClaim: TcClaim; aEndTime: TcTime);
  begin
  fActions.Add(TcClaimSetAction.Create(cakCopyEnd, aClaim, aNewClaim, nil, aEndTime));
  end;

procedure TcClaimSet.iCopyStart(aClaim, aNewCLaim: TcClaim; aStartTime: TcTime);
  begin
  fActions.Add(TcClaimSetAction.Create(cakCopyStart, aClaim, aNewClaim, nil, aStartTime));
  end;

procedure TcClaimSet.iProcessChanges(aSession: TcSession);
  var
    i: integer;
    lAction: TcClaimSetAction;
    lClaim: TcClaim;
    lAnnotation: TcAnnotation;
    lClaimIDStr: string;
    lCopy: TcClaim;
    lStartTime: TcTime;
  begin
  try
    Lock(aSession);

    // Make changes
    for i := 0 to fActions.Count-1 do
      begin
      lAction := fActions[i];
      lClaim := lAction.Claim;
      lAnnotation := lAction.Annotation;
      lClaimIDStr := IntToStr(lAction.Claim.ID);
      case lAction.Kind of
        cakAdd:
          begin
          // Add to derived claims
          if (lClaim.ValidUntil = cEndOfTime) // Always add if the Claim is valid
               or
             ClaimType.KeepEnded // Only add ended Claims when we keep those
             then fDerivedClaims.Add(lClaimIDStr, lClaim);
          end;

        cakStart:
          begin
          // At three points in this secion you'll find a condition:
          //   ClaimType.KeepEnded and (lClaim.ValidUntil <> lAction.Time)
          // The second part of this condition prevents creation of claims that span no interval.
          if Engine.UseMVCC
             then begin
                  if lClaim.TransactionID = aSession.Transaction.ID
                     then begin
                          // Created by session's transaction
                          if ClaimType.KeepEnded and (lClaim.ValidUntil <> lAction.Time)
                             then begin
                                  lClaim.ValidFrom := lAction.Time;

                                  if Engine.CreateTemporalAnnotations and not Assigned(lAnnotation)
                                     then begin
                                          lAnnotation := iCreateMetaAnnotation(aSession, cMetaAnnotationType_Started, lAction.NewClaim, lClaim.RefIDStr);
                                          lClaim.iAdd(aSession, lAnnotation);
                                          end;
                                  end
                             else iImmediateDeleteDerivedClaim(lClaim); // Really delete, has only been visible to this session
                          end
                     else begin
                          if ClaimType.KeepEnded and (lClaim.ValidUntil <> lAction.Time)
                             then begin
                                  // Create a copy with the correct ValidFrom
                                  lCopy := lClaim.iCopy(aSession, lAction.Time, lClaim.ValidUntil, cpMVCCCopy);

                                  if Engine.CreateTemporalAnnotations and not Assigned(lAnnotation)
                                     then begin
                                          lAnnotation := iCreateMetaAnnotation(aSession, cMetaAnnotationType_Started, lAction.NewClaim, lCopy.RefIDStr);
                                          lCopy.iAdd(aSession, lAnnotation);
                                          end;

                                  fDerivedClaims.Add(lCopy.IdentityStr, lCopy);
                                  end;

                          // Mark existing claim for later deletion
                          lClaim.MVCCMarkedForCleaningBy := aSession.Transaction.ID;
                          end;
                  end
             else begin
                  if ClaimType.KeepEnded and (lClaim.ValidUntil <> lAction.Time)
                     then begin
                          lClaim.ValidFrom := lAction.Time;

                          if Engine.CreateTemporalAnnotations and not Assigned(lAnnotation)
                             then begin
                                  lAnnotation := iCreateMetaAnnotation(aSession, cMetaAnnotationType_Started, lAction.NewClaim, lClaim.RefIDStr);
                                  lClaim.iAdd(aSession, lAnnotation);
                                  end;
                          end
                     else iImmediateDeleteDerivedClaim(lClaim);
                  end;
          end;

        cakEnd:
          begin
          // At three points in this secion you'll find a condition:
          //   ClaimType.KeepEnded and (lClaim.ValidFrom <> lAction.Time)
          // The second part of this condition prevents creation of claims that span no interval.
          if Engine.UseMVCC
             then begin
                  if lClaim.TransactionID = aSession.Transaction.ID
                     then begin
                          // Created by session's transaction
                          if ClaimType.KeepEnded and (lClaim.ValidFrom <> lAction.Time)
                             then begin
                                  lClaim.ValidUntil := lAction.Time;

                                  if Engine.CreateTemporalAnnotations and not Assigned(lAnnotation)
                                     then begin
                                          lAnnotation := iCreateMetaAnnotation(aSession, cMetaAnnotationType_Ended, lAction.NewClaim, lClaim.RefIDStr);
                                          lClaim.iAdd(aSession, lAnnotation);
                                          end;
                                  end
                             else iImmediateDeleteDerivedClaim(lClaim); // Really delete, has only been visible to this session
                          end
                     else begin
                          if ClaimType.KeepEnded and (lClaim.ValidFrom <> lAction.Time)
                             then begin
                                  // Create a copy with the correct ValidUntil
                                  lCopy := lClaim.iCopy(aSession, lClaim.ValidFrom, lAction.Time, cpMVCCCopy);

                                  if Engine.CreateTemporalAnnotations and not Assigned(lAnnotation)
                                     then begin
                                          lAnnotation := iCreateMetaAnnotation(aSession, cMetaAnnotationType_Ended, lAction.NewClaim, lCopy.RefIDStr);
                                          lCopy.iAdd(aSession, lAnnotation);
                                          end;

                                  fDerivedClaims.Add(lCopy.IdentityStr, lCopy);
                                  end;

                          // Mark existing claim for later deletion
                          lClaim.MVCCMarkedForCleaningBy := aSession.Transaction.ID;
                          end;
                  end
             else begin
                  if ClaimType.KeepEnded and (lClaim.ValidFrom <> lAction.Time)
                     then begin
                          lClaim.ValidUntil := lAction.Time;

                          if Engine.CreateTemporalAnnotations and not Assigned(lAnnotation)
                             then begin
                                  lAnnotation := iCreateMetaAnnotation(aSession, cMetaAnnotationType_Ended, lAction.NewClaim, lClaim.RefIDStr);
                                  lClaim.iAdd(aSession, lAnnotation);
                                  end;
                          end
                     else iImmediateDeleteDerivedClaim(lClaim);
                  end;
          end;

        cakExpire:
          begin
          // At three points in this secion you'll find a condition:
          //   ClaimType.KeepExpired and (lClaim.RegisteredAt <> lTransaction.StartTime)
          // The second part of this condition prevents creation of claims that span no interval.
          if aSession.Engine.ReplayMode
             then lStartTime := aSession.Engine.ReplayTime
             else lStartTime := aSession.Transaction.StartTime;

          if Engine.UseMVCC
             then begin
                  if lClaim.TransactionID = aSession.Transaction.ID
                     then begin
                          // Created by session's transaction
                          if ClaimType.KeepExpired and (lClaim.RegisteredAt <> lStartTime)
                             then begin
                                  lClaim.ExpiredAt := lStartTime;

                                  if Engine.CreateTemporalAnnotations and not Assigned(lAnnotation)
                                     then begin
                                          lAnnotation := iCreateMetaAnnotation(aSession, cMetaAnnotationType_Expired, lAction.NewClaim, lClaim.RefIDStr);
                                          lClaim.iAdd(aSession, lAnnotation);
                                          end;
                                  end
                             else iImmediateDeleteDerivedClaim(lClaim); // Really delete, has only been visible to this session
                          end
                     else begin
                          if ClaimType.KeepExpired and (lClaim.RegisteredAt <> lStartTime)
                             then begin
                                  // Create a copy
                                  lCopy := lClaim.iCopy(aSession, lClaim.ValidFrom, lClaim.ValidUntil, cpMVCCCopy);
                                  lCopy.ExpiredAt := lStartTime;

                                  if Engine.CreateTemporalAnnotations and not Assigned(lAnnotation)
                                     then begin
                                          lAnnotation := iCreateMetaAnnotation(aSession, cMetaAnnotationType_Expired, lAction.NewClaim, lCopy.RefIDStr);
                                          lCopy.iAdd(aSession, lAnnotation);
                                          end;

                                  fDerivedClaims.Add(lCopy.IdentityStr, lCopy);
                                  end;

                          // Mark existing claim for later deletion
                          lClaim.MVCCMarkedForCleaningBy := aSession.Transaction.ID;
                          end;
                  end
             else begin
                  if ClaimType.KeepExpired and (lClaim.RegisteredAt <> lStartTime)
                     then begin
                          lClaim.ExpiredAt := lStartTime;

                          if Engine.CreateTemporalAnnotations and not Assigned(lAnnotation)
                             then begin
                                  lAnnotation := iCreateMetaAnnotation(aSession, cMetaAnnotationType_Expired, lAction.NewClaim, lClaim.RefIDStr);
                                  lClaim.iAdd(aSession, lAnnotation);
                                  end;
                          end
                     else iImmediateDeleteDerivedClaim(lClaim);
                  end;
          end;

        cakDelete:
          begin
          if Engine.UseMVCC
             then begin
                  if lClaim.TransactionID = aSession.Transaction.ID
                     then begin
                          // Created by session's transaction
                          iImmediateDeleteDerivedClaim(lClaim); // Really delete, has only been visible to this session
                          end
                     else begin
                          // Mark existing claim for later deletion
                          lClaim.MVCCMarkedForCleaningBy := aSession.Transaction.ID;
                          end;
                  end
             else iImmediateDeleteDerivedClaim(lClaim);
          end;

        cakCopyStart:
          begin
          if ClaimType.KeepEnded or (lClaim.ValidUntil = cEndOfTime)
             then begin
                  lCopy := lClaim.iCopy(aSession, lAction.Time, lClaim.ValidUntil, cpTemporalCopy);

                  if Engine.CreateTemporalAnnotations and not Assigned(lAnnotation)
                     then begin
                          lAnnotation := iCreateMetaAnnotation(aSession, cMetaAnnotationType_Started, lAction.NewClaim, lCopy.RefIDStr);
                          lCopy.iAdd(aSession, lAnnotation);
                          end;

                  fDerivedClaims.Add(lCopy.IdentityStr, lCopy);
                  end;
          end;

        cakCopyEnd:
          begin
          if ClaimType.KeepEnded or (lAction.Time = cEndOfTime)
             then begin
                  lCopy := lClaim.iCopy(aSession, lClaim.ValidFrom, lAction.Time, cpTemporalCopy);

                  if Engine.CreateTemporalAnnotations and not Assigned(lAnnotation)
                     then begin
                          lAnnotation := iCreateMetaAnnotation(aSession, cMetaAnnotationType_Ended, lAction.NewClaim, lCopy.RefIDStr);
                          lCopy.iAdd(aSession, lAnnotation);
                          end;

                  fDerivedClaims.Add(lCopy.IdentityStr, lCopy);
                  end;
          end;

        else EInt('TcClaimSet.iProcessChanges', 'Unkown claim action');
        end;
      end;

  finally
    for i := 0 to fActions.Count-1 do
      fActions[i].Free;
    fActions.Clear;

    if Assigned(gRadioStation)
       then begin
            gRadioStation.BroadCast(cRadioEvent_ClaimSet_Changed, Self);
            gRadioStation.BroadCast(cRadioEvent_Session_ClaimSet_Changed, aSession);
            end;
    end;
  end;

function TcClaimSet.iCreateMetaAnnotation(aSession: TcSession; aAnnotationTypeName: string; aDerivedFrom: TcClaim; aClaimRefIDStr: string): TcAnnotation;
  var
    lAnnotationType: TcAnnotationType;
  begin
  lAnnotationType := aSession.Engine.ConceptualTypes.FindAnnotationType(aAnnotationTypeName);
  if not Assigned(lAnnotationType) then EInt('TcClaimSet.iCreateMetaAnnotation', 'AnnotationType unknown.');
  result := TcAnnotation.Create(aSession, lAnnotationType, aClaimRefIDStr, cStartOfTime, cEndOfTime, cpTemporalAnnotation);
  if Assigned(aDerivedFrom) then result.DerivedFrom := aDerivedFrom.SourceClaim;
  end;

procedure TcClaimSet.iImmediateDeleteSourceClaim(aClaim: TcClaim);
  var
    i: integer;
  begin
  i := fSourceClaims.IndexOfObject(aClaim);
  fSourceClaims.Delete(i);
  aClaim.Operation.ImmediateDeleteClaim(aClaim);
  aClaim.Free;
  end;

procedure TcClaimSet.iImmediateDeleteDerivedClaim(aClaim: TcClaim);
  var
    i: integer;
  begin
  i := fDerivedClaims.IndexOfObject(aClaim);
  fDerivedClaims.Delete(i);
  aClaim.Free;
  end;

procedure TcClaimSet.Lock(aSession: TcSession);
  begin
  // Try to lock claimset
  if LockedByTransactionID = cNoID
     then begin
          LockedByTransactionID := aSession.Transaction.ID;
          aSession.Transaction.LockedClaimSets.Add(ID, Self);
          end

  // Check existing lock
  else if LockedByTransactionID <> aSession.Transaction.ID
     then EUsr('ClaimSet locked by transaction %s%d.', [cRefChar, LockedByTransactionID]);
  end;

procedure TcClaimSet.Unlock;
  begin
  fLockedByTransactionID := cNoID;
  end;

procedure TcClaimSet.Add(aSession: TcSession; aSourceClaim: TcClaim);
  begin
  aSession.Engine.TriggerAutoIncrementSystemTime;

  if aSourceClaim.IdentityStr <> IdentityStr
    then EInt('TcClaimSet.Add', 'Claim added to wrong ClaimSet.');

  // Is change allowed?
  if not ClaimType.AllowChange and (SourceClaims.Count>0)
    then EUsr('Cannot change Claims of type %s', [ClaimType.Name]);

  // Remember the Source Claim
  fSourceClaims.Add(aSourceClaim.IdentityStr, aSourceClaim);

  // Internal Add with TimeLineInterpretation
  iAddAndInterpret(aSession, aSourceClaim);
  end;

procedure TcClaimSet.ExpireAll(aSession: TcSession);
  var
    i: integer;
    lClaim: TcClaim;
  begin
  aSession.Engine.TriggerAutoIncrementSystemTime;

  for i := 0 to DerivedClaims.Count-1 do
    begin
    lClaim := DerivedClaims[i];
    if lClaim.ExpiredAt < cEndOfTime
      then Continue;
    iExpire(lClaim, nil);
    end;

  iProcessChanges(aSession);
  end;

procedure TcClaimSet.Expire(aSession: TcSession; aClaim: TcClaim; aAnnotation: TcAnnotation);
  var
    lIndexSource, lIndexDerived: integer;
  begin
  aSession.Engine.TriggerAutoIncrementSystemTime;

  lIndexSource := fSourceClaims.IndexOfObject(aClaim);
  lIndexDerived := fDerivedClaims.IndexOfObject(aClaim);
  if (lIndexSource>=0) or (lIndexDerived>=0)
     then iExpire(aClaim, nil, aAnnotation)
     else EUsr('Claim %d not found in ClaimSet', [aClaim.ID]);

  iProcessChanges(aSession);
  end;

procedure TcClaimSet.Expire(aSession: TcSession; aClaim: TcClaim);
  begin
  aSession.Engine.TriggerAutoIncrementSystemTime;
  Expire(aSession, aClaim, nil);
  end;

procedure TcClaimSet.Start(aSession: TcSession; aClaim: TcClaim; aAnnotation: TcAnnotation; aStartTime: TcTime);
  var
    lIndexSource, lIndexDerived: integer;
  begin
  aSession.Engine.TriggerAutoIncrementSystemTime;

  lIndexSource := fSourceClaims.IndexOfObject(aClaim);
  lIndexDerived := fDerivedClaims.IndexOfObject(aClaim);
  if (lIndexSource>=0) or (lIndexDerived>=0)
     then iStart(aClaim, nil, aAnnotation, aStartTime)
     else EUsr('Claim %d not found in ClaimSet', [aClaim.ID]);

  iProcessChanges(aSession);
  end;

procedure TcClaimSet.Start(aSession: TcSession; aClaim: TcClaim; aStartTime: TcTime);
  begin
  aSession.Engine.TriggerAutoIncrementSystemTime;
  Start(aSession, aClaim, nil, aStartTime);
  end;

procedure TcClaimSet.End_(aSession: TcSession; aClaim: TcClaim; aAnnotation: TcAnnotation; aEndTime: TcTime);
  var
    lIndexSource, lIndexDerived: integer;
  begin
  aSession.Engine.TriggerAutoIncrementSystemTime;

  lIndexSource := fSourceClaims.IndexOfObject(aClaim);
  lIndexDerived := fDerivedClaims.IndexOfObject(aClaim);
  if (lIndexSource>=0) or (lIndexDerived>=0)
     then iEnd(aClaim, nil, aAnnotation, aEndTime)
     else EUsr('Claim %d not found in ClaimSet', [aClaim.ID]);

  iProcessChanges(aSession);
  end;

procedure TcClaimSet.End_(aSession: TcSession; aClaim: TcClaim; aEndTime: TcTime);
  begin
  aSession.Engine.TriggerAutoIncrementSystemTime;
  End_(aSession, aClaim, nil, aEndTime);
  end;

procedure TcClaimSet.Delete(aSession: TcSession; aClaim: TcClaim; aAnnotation: TcAnnotation);
  var
    lIndexSource, lIndexDerived: integer;
  begin
  aSession.Engine.TriggerAutoIncrementSystemTime;

  lIndexSource := fSourceClaims.IndexOfObject(aClaim);
  lIndexDerived := fDerivedClaims.IndexOfObject(aClaim);
  if (lIndexSource>=0) or (lIndexDerived>=0)
     then iDelete(aClaim, aAnnotation)
     else EUsr('Claim %d not found in ClaimSet', [aClaim.ID]);

  iProcessChanges(aSession);
  end;

procedure TcClaimSet.Delete(aSession: TcSession; aClaim: TcClaim);
  begin
  aSession.Engine.TriggerAutoIncrementSystemTime;
  Delete(aSession, aClaim, nil);
  end;

procedure TcClaimSet.DeriveClaims(aSession: TcSession);
  var
    sc: integer;
    lClaim: TcClaim;
  begin
  // Clear all Derived Claims from the ClaimSet
  fDerivedClaims.FreeAndClear;

  // Derive them again from the Original Claim
  aSession.Engine.StartReplay;
  try
    for sc := 0 to fSourceClaims.Count-1 do
      begin
      lClaim := fSourceClaims.Objects[sc];
      iAddAndInterpret(aSession, lClaim);
      end;

  finally
    aSession.Engine.StopReplay;
    end;
  end;


{ TcClaimSetAction }

constructor TcClaimSetAction.Create(aKind: TcClaimSetActionKind; aClaim, aNewClaim: TcClaim; aAnnotation: TcAnnotation = nil; aTime: TcTime = cNoTime);
  begin
  inherited Create;
  fKind := aKind;
  fClaim := aClaim;
  fNewClaim := aNewClaim;
  fAnnotation := aAnnotation;
  fTime := aTime;
  end;


{ TcObjectSet }

constructor TcObjectSet.Create(aClass: TcClass; aIdentityStr: String);
  begin
  inherited Create(aClass, aIdentityStr);
  fClass := aClass;
  fObjects := TcElementList<TcObject>.Create(aClass.Engine, true {Owned}, false {Not sorted});
  end;

destructor TcObjectSet.Destroy;
  begin
  FreeAndNil(fObjects);
  inherited;
  end;

function TcObjectSet.GetTuple(const aIndex: integer): TcTuple;
  begin
  result := fObjects[aIndex];
  end;

function TcObjectSet.GetTupleCount: integer;
  begin
  result := fObjects.Count;
  end;


{ TcClaimSetList }

constructor TcClaimSetList.Create(aClaimType: TcClaimType);
  begin
  inherited Create(aClaimType.Engine, true {Owned}, true {Sorted});
  fClaimType := aClaimType;
  end;

function TcClaimSetList.Find(aIdentityStr: string): TcClaimSet;
  var
    i: integer;
  begin
  if fList.Find(aIdentityStr, i)
     then result := Objects[i]
     else result := nil;
  end;

procedure TcClaimSetList.iAdd(aSession: TcSession; aClaim: TcClaim);
  var
    lIndex: integer;
    lClaimSet: TcClaimSet;
  begin
  if fList.Find(aClaim.IdentityStr, lIndex)
    then // Add to existing ClaimSet
         lClaimSet := Objects[lIndex]
    else begin
         // Create new ClaimSet
         lClaimSet := TcClaimSet.Create(ClaimType, aClaim.IdentityStr);
         fList.Add(lClaimSet.IdentityStr, lClaimSet);
         end;
  aClaim.ClaimSet := lClaimSet;
  lClaimSet.Add(aSession, aClaim);
  end;

function TcClaimSetList.AddClaim(aSession: TcSession; aTupleStr: string; aValidFrom, aValidUntil: TcTime): TcClaim;
  var
    lClaim: TcClaim;
  begin
  lClaim := TcClaim.Create(aSession, ClaimType, aTupleStr, aValidFrom, aValidUntil, cpSource);
  iAdd(aSession, lClaim);
  result := lClaim;
  end;

function TcClaimSetList.AddAnnotation(aSession: TcSession; aTupleStr: string; aValidFrom, aValidUntil: TcTime): TcAnnotation;
  var
    lAnnotationType: TcAnnotationType;
    lIDStr: string;
    lElement: TcElement;
    lClaim: TcClaim;
    lAnnotation: TcAnnotation;
  begin
  if not (ClaimType is TcAnnotationType)
     then EInt('TcClaimSetList.AddAnnotation', 'Cannot add an annoptation to a %s', [ClaimType.ClassName]);
  lAnnotationType := ClaimType as TcAnnotationType;

  lAnnotation := TcAnnotation.Create(aSession, lAnnotationType, aTupleStr, aValidFrom, aValidUntil, cpSource);
  iAdd(aSession, lAnnotation);
  result := lAnnotation;

  // Add to correct ClaimType
  lIDStr := lAnnotation.RoleValues.Values[cMetaRoleName_Claim];
  lElement := aSession.Engine.FindElementByRefIDStr(lIDStr);

  // Find referenced BaseClaim
  if not (lElement is TcClaim)
     then EInt('TcClaimSetList.AddAnnotation', '$claim Role does not reference a Claim.');
  lClaim := lElement as TcClaim;

  // Add annotation to Claim
  lClaim.Add(aSession, lAnnotation);
  end;

procedure TcClaimSetList.DeriveClaims(aSession: TcSession);
  var
    i: integer;
  begin
  for i := 0 to Count-1 do
    Objects[i].DeriveClaims(aSession);
  end;


constructor TcObjectSetList.Create(aClass: TcClass);
  begin
  inherited Create(aClass.Engine, true {Owned}, true {Sorted});
  fClass := aClass;
  end;


{ TcSession }

constructor TcSession.Create(aEngine: TcEngine);
  begin
  inherited Create(aEngine);
  fSeqNr := gSessionSeqNr;
  inc(gSessionSeqNr);
  end;

destructor TcSession.Destroy;
  begin
  inherited;
  end;

function TcSession.GetColor: TColor;
  begin
  case fSeqNr mod 4 of
    0: result := clWebLemonChiffon; // Yellow
    1: result := clWebLightCyan; // Blue
    2: result := clWebPaleGreen; // Green
    3: result := clWebPink; // Pink
    else result := clWebWhiteSmoke; // Gray
    end;
  end;

function TcSession.GetInfoStr: string;
  var
    lStr: string;
  begin
  if Assigned(Transaction)
     then begin
          if Transaction.StableRead
             then lStr := IntToStr(Transaction.StableReadSnapshotID)
             else lStr := '-';

          result := Format('Session:%s%d Write:%d Stable:%s', [
             cRefChar, ID, Transaction.StartTime, lStr
          ])
          end
     else result := Format('Session:%s%d', [cRefChar, ID]);
  end;

function TcSession.GetInTransaction: boolean;
  begin
  result := Assigned(fTransaction);
  end;

function TcSession.GetInOperation: boolean;
  begin
  result := Assigned(fOperation);
  end;

procedure TcSession.StartTransaction(aImplicit, aReadonly, aStableRead: boolean);
  var
    lTransaction: TcTransaction;
  begin
  lTransaction := TcTransaction.Create(Self, '', aImplicit, aReadonly, aStableRead);
  Engine.Transactions.Add(lTransaction.ID, lTransaction);
  fTransaction := lTransaction;

  if Assigned(gRadioStation) and not (aImplicit and aReadonly)
     then gRadioStation.BroadCast(cRadioEvent_Transaction_Start, Self);
  end;

procedure TcSession.Commit;
  begin
  iEndTransaction(true {commit});
  end;

procedure TcSession.Rollback;
  begin
  iEndTransaction(false {rollback});
  end;

procedure TcSession.iEndTransaction(aCommit: boolean);
  var
    lBroadcast: boolean;
  begin
  if not Assigned(fTransaction)
     then EUsr('No transaction active.');

  lBroadcast := Assigned(gRadioStation) and not (fTransaction.Implicit and fTransaction.Readonly_);
  if lBroadcast
     then gRadioStation.BroadCast(cRadioEvent_Transaction_BeforeEnd, Self);

  fTransaction.End_(aCommit);
  fTransaction := nil;
  fOperation := nil;

  Engine.iMVCCClean;

  if lBroadcast
     then gRadioStation.BroadCast(cRadioEvent_Transaction_AfterEnd, Self);
  end;

procedure TcSession.StartOperation(aName: string);
  var
    lOperation: TcOperation;
  begin
  StartTransaction(false {Not implicit}, false {Not readonly}, false {Not stable});
  lOperation := TcOperation.Create(Self, Transaction, aName);
  Engine.Operations.Add(lOperation.ID, lOperation);
  fOperation := lOperation;
  end;


{ TcTransaction }

constructor TcTransaction.Create(aSession: TcSession; aName: string; aImplicit, aReadonly, aStableRead: boolean);
  begin
  if not aReadOnly
     then aSession.Engine.TriggerAutoIncrementSystemTime;

  inherited Create(aSession.Engine, aName);
  fSession := aSession;

  fImplicit := aImplicit;
  fReadonly := aReadonly;
  fStableRead := aStableRead;

  if aStableRead and not aReadonly
     then EUsr('StableRead transactions must be readonly.');
  if aStableRead
     then fStableReadSnapshotID := aSession.Engine.StableReadSnapshotID
     else fStableReadSnapshotID := cNoID;

  fStartID := aSession.Engine.GetNewTransactionID;
  fEndID := cNoID;
  fStartTime := aSession.Engine.SystemTime;
  fEndTime := cEndOfTime;
  fState := tsActive;
  fLockedClaimSets := TcElementList<TcClaimSet>.Create(aSession.Engine, false {Not owned}, false {Not sorted});
  end;

destructor TcTransaction.Destroy;
  begin
  FreeAndNil(fLockedClaimSets);
  inherited;
  end;

procedure TcTransaction.iRollbackChanges;
  var
    i, c: integer;
    lClaimSet: TcClaimSet;
    lClaim: TcClaim;
  begin
  for i := 0 to LockedClaimSets.Count-1 do
    begin
    lClaimSet := LockedClaimSets.Objects[i];

    for c := lClaimSet.DerivedClaims.Count-1 downto 0 do // Backwards so deletion is possible
      begin
      lClaim := lClaimSet.DerivedClaims[c];

      // Delete all claims created by this transaction
      if lClaim.TransactionID = ID
         then lClaimSet.iImmediateDeleteDerivedClaim(lClaim)

      // Remove cleaning marks set by this transaction
      else if lClaim.MVCCMarkedForCleaningBy = ID
         then lClaim.MVCCMarkedForCleaningBy := cNoID;
      end;

    for c := lClaimSet.SourceClaims.Count-1 downto 0 do // Backwards so deletion is possible
      begin
      lClaim := lClaimSet.SourceClaims[c];

      // Delete all claims created by this transaction
      if lClaim.TransactionID = ID
         then lClaimSet.iImmediateDeleteSourceClaim(lClaim)

      // Remove cleaning marks set by this transaction
      else if lClaim.MVCCMarkedForCleaningBy = ID
         then lClaim.MVCCMarkedForCleaningBy := cNoID;
      end;

    end
  end;

procedure TcTransaction.iReleaseLocks;
  var
    i: integer;
    lClaimSet: TcClaimSet;
  begin
  for i := 0 to LockedClaimSets.Count-1 do
    begin
    lClaimSet := LockedClaimSets.Objects[i];
    lClaimSet.Unlock;
    Engine.ClaimSetsToClean.Add(lClaimSet.ID, lClaimSet);
    end;
  end;

function TcTransaction.GetStableReadSnapshotTime: TcTime;
  begin
  result := Engine.StableReadSnapshotIDToTime(StableReadSnapshotID);
  end;

procedure TcTransaction.End_(aCommit: boolean);
  begin
  if not Readonly_
     then Session.Engine.TriggerAutoIncrementSystemTime;

  try
    fEndID := Engine.GetNewTransactionID;
    fEndTime := Engine.SystemTime;
    if aCommit
      then fState := tsSuccess
      else begin
           iRollbackChanges;
           fState := tsFailed;
           end;

  finally
    iReleaseLocks;
    end;
  end;


{ TcEngine }

constructor TcEngine.Create(aName: string = '');
  begin
  inherited Create;

  fName := aName;
  if fName = '' then fName := cEngine_DefaultName;

  fElements := TcElements.Create;
  fMaxElementID := 0;
  fMaxTransactionID := 0;

  fDataTypes := TcElementList<TcDataType>.Create(Self, true {Owned}, true {Sorted});
  fRelationTypes := TcElementList<TcRelationType>.Create(Self, false {Not owned}, true {Sorted});
  fConceptualTypes := TcConceptualTypes.Create(Self);
  fClasses := TcElementList<TcClass>.Create(Self, true {Owned}, true {Sorted});
  fTransactions := TcElementList<TcTransaction>.Create(Self, true {Owned}, true {Sorted});
  fOperations := TcElementList<TcOperation>.Create(Self, true {Owned}, true {Sorted});
  fOperationClaimTypes := TcElementList<TcClaimType>.Create(Self, false {Not owned}, true {Sorted});

  fUseMVCC := true;
  fCreateTemporalAnnotations := true;
  fClaimSetsToClean := TcElementList<TcClaimSet>.Create(Self, false {Not owned}, true {Sorted});

  fMinSystemTime := cEndOfTime;
  fAutoIncrementSystemTime := true;
  fSystemTime := cStartOfTime;
  fReplayMode := false;
  fReplayTime := cStartOfTime;

  iInitDataTypes;
  iExposeMetaStructures;
  end;

destructor TcEngine.Destroy;
  begin
  // This will empty the elements list before destruction.
  // Elements will normally remove them selves from the element list
  // during their destruction. Since fElements will be nil, the
  // elements will not search and remove (done for better performance)
  FreeAndNil(fElements);

  // Destroy elements by destroying the different lists that own them
  FreeAndNil(fDatatypes);
  FreeAndNil(fRelationTypes);
  FreeAndNil(fConceptualTypes);
  FreeAndNil(fClasses);
  FreeAndNil(fTransactions);
  FreeAndNil(fOperations);
  FreeAndNil(fOperationClaimTypes);
  FreeAndNil(fClaimSetsToClean);

  inherited Destroy;
  end;

procedure TcEngine.iInitDatatypes;

  procedure iAddDataType(aName: string);
    var
      lDataType: TcDataType;
    begin
    lDataType := TcDataType.Create(Self, aName);
    fDataTypes.Add(lDataType.Name, lDataType);
    end;

  begin
  // DataTypes (allways hardcoded)
  // See also: https://dba.stackexchange.com/questions/53317/databases-are-there-universal-datatypes
  iAddDataType('integer');
  iAddDataType('boolean');
  iAddDataType('date');
  iAddDataType('timestamp');
  iAddDataType('string');
  iAddDataType('uuid');
  end;

procedure TcEngine.iExposeMetaStructures;
  var
    lLabelType: TcLabeltype;
    lClaimType: TcClaimType;
    lAnnotationType: TcAnnotationType;
  begin
  // Create LabelType: $id
  lLabelType := TcLabelType.Create(Self, cMetaLabelType_ID, 'integer');
  ConceptualTypes.Add(lLabelType.Name, lLabelType);

  // Create ClaimType for claims: $claim
  lClaimType :=  TcClaimType.Create(Self, cMetaClaimType_Claim);
  lClaimType.SetExpressionTemplate(Format('There is a claim <%s>', [cMetaRoleName_ID]));
  lClaimType.SetIdentity(cMetaRoleName_ID);
  lClaimType.SetNestedExpressionTemplate(Format('claim <%s>', [cMetaRoleName_ID]));
  lClaimType.AfterInit;
  RelationTypes.Add(lClaimType.ID, lClaimType);
  ConceptualTypes.Add(lClaimType.Name, lClaimType);

  // Create ClaimType for operations: $operation
  lClaimType := TcClaimType.Create(Self, cMetaClaimType_Operation);
  lClaimType.SetExpressionTemplate(Format('There is an operation <%s>', [cMetaRoleName_ID]));
  lClaimType.SetIdentity(cMetaRoleName_ID);
  lClaimType.SetNestedExpressionTemplate(Format('operation <%s>', [cMetaRoleName_ID]));
  lClaimType.AfterInit;
  RelationTypes.Add(lClaimType.ID, lClaimType);
  ConceptualTypes.Add(lClaimType.Name, lClaimType);

  // Create AnnotationType for starting: $started
  lAnnotationType := TcAnnotationType.Create(Self, cMetaAnnotationType_Started, akStarted);
  lAnnotationType.SetExpressionTemplate(Format('Claim <%s> started', [cMetaRoleName_Claim]));
  lAnnotationType.SetIdentity(cMetaRoleName_Claim);
  lAnnotationType.AfterInit;
  RelationTypes.Add(lAnnotationType.ID, lAnnotationType);
  ConceptualTypes.Add(lAnnotationType.Name, lAnnotationType);

  // Create AnnotationType for expiration: $ended
  lAnnotationType := TcAnnotationType.Create(Self, cMetaAnnotationType_Ended, akEnded);
  lAnnotationType.SetExpressionTemplate(Format('Claim <%s> is no longer valid', [cMetaRoleName_Claim]));
  lAnnotationType.SetIdentity(cMetaRoleName_Claim);
  lAnnotationType.AfterInit;
  RelationTypes.Add(lAnnotationType.ID, lAnnotationType);
  ConceptualTypes.Add(lAnnotationType.Name, lAnnotationType);

  // Create AnnotationType for expiration: $expired
  lAnnotationType := TcAnnotationType.Create(Self, cMetaAnnotationType_Expired, akExpired);
  lAnnotationType.SetExpressionTemplate(Format('Claim <%s> has expired', [cMetaRoleName_Claim]));
  lAnnotationType.SetIdentity(cMetaRoleName_Claim);
  lAnnotationType.AfterInit;
  RelationTypes.Add(lAnnotationType.ID, lAnnotationType);
  ConceptualTypes.Add(lAnnotationType.Name, lAnnotationType);

  // Create AnnotationType for expiration: $deleted
  lAnnotationType := TcAnnotationType.Create(Self, cMetaAnnotationType_Deleted, akDeleted);
  lAnnotationType.SetExpressionTemplate(Format('Claim <%s> was deleted', [cMetaRoleName_Claim]));
  lAnnotationType.SetIdentity(cMetaRoleName_Claim);
  lAnnotationType.AfterInit;
  RelationTypes.Add(lAnnotationType.ID, lAnnotationType);
  ConceptualTypes.Add(lAnnotationType.Name, lAnnotationType);

  // Create ClaimType to relate Operations and Claims
  lClaimType := TcClaimType.Create(Self, cMetaClaimType_OperationClaim);
  lClaimType.SetExpressionTemplate(Format('<%s> was registered by <%s>', [cMetaRoleName_Claim, cMetaRoleName_Operation]));
  lClaimType.SetIdentity(cMetaRoleName_Claim);
  lClaimType.AfterInit;
  RelationTypes.Add(lClaimType.ID, lClaimType);
  ConceptualTypes.Add(lClaimType.Name, lClaimType);
  end;

procedure TcEngine.iMVCCClean;
  var
    lOldestRequiredID: integer;
    cs, c: Integer;
    lClaimSet: TcClaimSet;
    lClaim: TcClaim;
    lTransaction: TcTransaction;
    lCleaned: boolean;
  begin
  lOldestRequiredID := GetOldestRequiredTransactionID;
  for cs := ClaimSetsToClean.Count-1 downto 0 do
    begin
    lClaimSet := ClaimSetsToClean[cs];
    lCleaned := true;
    for c := lClaimSet.DerivedClaims.Count-1 downto 0 do
      begin
      lClaim := lClaimSet.DerivedClaims[c];
      if lClaim.MVCCMarkedForCleaningBy <> cNoID
         then begin
              lTransaction := Transactions.FindByID(lClaim.MVCCMarkedForCleaningBy);
              if not Assigned(lTransaction)
                 then EInt('TcEngine.Clean', 'Transaction not assigned');

              if lTransaction.EndID < lOldestRequiredID
                 then begin
                      lClaimSet.fDerivedClaims.Delete(c);
                      lClaim.Free;
                      end
                 else lCleaned := false;
              end;
      end;

    if lCleaned
       then ClaimSetsToClean.Delete(cs);
    end;
  end;

function TcEngine.GetSystemTime: integer;
  begin
  if fReplayMode
     then result := fReplayTime
     else result := fSystemTime;
  end;

procedure TcEngine.SetSystemTime(aTime: integer);
  begin
  if aTime<1 then EUsr('Invalid time: %d', [aTime]);

  fSystemTime := aTime;
  if aTime < fMinSystemTime then fMinSystemTime := aTime;
  if aTime > fMaxSystemTime then fMaxSystemTime := aTime;

  if Assigned(gRadioStation)
     then gRadioStation.BroadCast(cRadioEvent_SystemTime_Changed, Self);
  end;

function TcEngine.GetInTransaction: boolean;
  var
    i: integer;
  begin
  result := false;
  for i := fTransactions.Count-1 downto 0 do
    begin
    if fTransactions.Objects[i].State = tsActive
       then begin
            result := true;
            exit;
            end;
    end;
  end;

function TcEngine.GetStableReadSnapshotID: integer;
  var
    i: integer;
    lTransaction: TcTransaction;
  begin
  result := fMaxTransactionID;
  Debug(dtStableReadSnapshot, 'Stable ID - Init: %d', [result]);

  for i := 0 to Transactions.Count-1 do
    begin
    lTransaction := Transactions[i];
    if lTransaction.State <> tsActive then continue;
    if lTransaction.Readonly_ then continue; // Only consider write transactions

    if lTransaction.StartID < result
       then begin
            result := lTransaction.StartID;
            Debug(dtStableReadSnapshot, 'Stable ID - tr: %d', [result]);
            end;
    end;

  result := result - 1; // One ID before oldest transaction
  end;

function TcEngine.StableReadSnapshotIDToTime(aStableReadSnapshotID: integer): TcTime;
  var
    i: integer;
    lTransaction: TcTransaction;
    lStableReadSnapShotID: integer;
  begin
  result := SystemTime;
  Debug(dtStableReadSnapshot, 'ID 2 Time - Init: %d', [result]);

  // First non-stable ID was one later than stable snapshot ID
  lStableReadSnapShotID := aStableReadSnapshotID + 1;

  for i := Transactions.Count-1 downto 0 do
    begin
    lTransaction := Transactions[i];
    if lTransaction.StartID = lStableReadSnapshotID
       then begin
            // Transaction found
            result := lTransaction.StartTime - 1; // Time of transaction - 1
            Debug(dtStableReadSnapshot, 'ID 2 Time - tr: %d', [result]);
            exit;
            end;
    end;
  end;

function TcEngine.GetStableReadSnapshotTime: TcTime;
  var
    lStableReadSnapshotID: integer;
  begin
  lStableReadSnapshotID := GetStableReadSnapshotID;
  result := StableReadSnapshotIDToTime(lStableReadSnapshotID);
  Debug(dtStableReadSnapshot, 'Stable Time: %d', [result]);
  end;

function TcEngine.GetOldestRequiredTransactionID: integer;
  var
    i: integer;
    lTransaction: TcTransaction;
  begin
  result := MaxInt;

  for i := 0 to Transactions.Count-1 do
    begin
    lTransaction := Transactions[i];
    if lTransaction.State <> tsActive then continue;

    if lTransaction.StartID < result
       then result := lTransaction.StartID;
    end;
  end;

function TcEngine.GetNewElementID: integer;
  begin
  inc(fMaxElementID);
  result := fMaxElementID;
  end;

function TcEngine.FindElementByID(aID: integer): TcElement;
  begin
  result := Elements.Find(aID);
  end;

function TcEngine.FindElementByRefIDStr(aIDStr: string): TcElement;
  var
    lID: integer;
  begin
  if Length(aIDStr)=0 then EUsr('Missing reference');
  if aIDStr[1]<>cRefChar then EUsr('Invalid reference: %s', [aIDStr]);

  lID := StrToIntDef(Copy(aIDStr, 2, Length(aIDStr)), -1);
  if lID<0 then EUsr('Invalid reference: %s', [aIDStr]);

  result := FindElementByID(lID);
  end;

function TcEngine.FindClassByPluralName(aPluralName: string): TcClass;
  var
    i: integer;
    lClass: TcClass;
  begin
  result := nil;
  for i := 0 to Classes.Count-1 do
    begin
    lClass := Classes[i];
    if CompareText(lClass.GetOrDerivePluralName, aPluralName)=0
       then begin
            result := lClass;
            exit;
            end;
    end;
  end;

function TcEngine.FindClaim(aID: integer): TcClaim;
  var
    lElement: TcElement;
  begin
  result := nil;
  lElement := Elements.Find(aID);
  if Assigned(lElement) and (lElement is TcClaim)
     then result := lElement as TcClaim;
  end;

function TcEngine.FindClaimSet(aID: integer): TcClaimSet;
  var
    lElement: TcElement;
  begin
  result := nil;
  lElement := Elements.Find(aID);
  if Assigned(lElement)
     then begin
          if lElement is TcClaimSet
             then result := lElement as TcClaimSet;
          end;
  end;

procedure TcEngine.SetAutoIncrementSystemTime(aEnabled: boolean);
  begin
  fAutoIncrementSystemTime := aEnabled;
  if fSystemTime < 1
     then SetSystemTime(1);
  end;

procedure TcEngine.TriggerAutoIncrementSystemTime;
  begin
  if AutoIncrementSystemTime
     then IncrementSystemTime;
  end;

procedure TcEngine.IncrementSystemTime;
  begin
  SetSystemTime(fSystemTime + 1);
  end;

procedure TcEngine.RegisterValidPeriod(aValidFrom, aValidUntil: TcTime);
  begin
  if (aValidFrom <> cStartOfTime) and (aValidFrom <> cEndOfTime)
    then begin
         if aValidFrom < fMinValidTime then fMinValidTime := aValidFrom;
         if aValidFrom > fMaxValidTime then fMaxValidTime := aValidFrom;
         end;

  if (aValidUntil <> cStartOfTime) and (aValidUntil <> cEndOfTime)
    then begin
         if aValidUntil < fMinValidTime then fMinValidTime := aValidUntil;
         if aValidUntil > fMaxValidTime then fMaxValidTime := aValidUntil;
         end;
  end;

procedure TcEngine.StartReplay;
  begin
  fReplayMode := true;
  end;

procedure TcEngine.StopReplay;
  begin
  fReplayMode := false;
  end;

function TcEngine.GetNewTransactionID: integer;
  begin
  inc(fMaxTransactionID);
  result := fMaxTransactionID;
  end;


{ TcElements }

constructor TcElements.Create;
  begin
  inherited Create;
  fItems := TcStringObjectList<TcElement>.Create(false {Not owned}, true {Sorted});
  end;

destructor TcElements.Destroy;
  begin
  FreeAndNil(fItems);
  inherited;
  end;

function TcElements.GetItem(const aIndex: integer): TcElement;
  begin
  result := fItems.Objects[aIndex];
  end;

function TcElements.GetCount: integer;
  begin
  result := fItems.Count;
  end;

procedure TcElements.Add(aElement: TcElement);
  begin
  fItems.Add(IntToStr(aElement.ID), aElement);
  end;

procedure TcElements.Delete(aElement: TcElement);
  var
    lIndex: integer;
  begin
  lIndex := fItems.IndexOfObject(aElement);
  fItems.Delete(lIndex);
  end;

function TcElements.Find(aID: integer): TcElement;
  begin
  result := fItems.Find(IntToStr(aID));
  end;


{ TcConceptualTypes }

constructor TcConceptualTypes.Create(aEngine: TcEngine);
  begin
  inherited Create(aEngine, true {Owned}, true {Sorted});
  end;

function TcConceptualTypes.Find(aName: string): TcType;
  var
    i: integer;
  begin
  if fList.Find(aName, i)
    then result := Objects[i]
    else result := nil;
  end;

function TcConceptualTypes.FindLabelType(aName: string): TcLabelType;
  var
    lType: TcType;
  begin
  result := nil;
  lType := Find(aName);
  if not Assigned(lType) then exit;
  if lType is TcLabelType
    then result := lType as TcLabelType;
  end;

function TcConceptualTypes.FindClaimType(aName: string): TcClaimType;
  var
    lType: TcType;
  begin
  result := nil;
  lType := Find(aName);
  if not Assigned(lType) then exit;
  if lType is TcClaimType
    then result := lType as TcClaimType;
  end;

function TcConceptualTypes.FindAnnotationType(aName: string): TcAnnotationType;
  var
    lType: TcType;
  begin
  result := nil;
  lType := Find(aName);
  if not Assigned(lType) then exit;
  if lType is TcAnnotationType
    then result := lType as TcAnnotationType;
  end;

initialization
  gMetaRoleNames := TStringlist.Create;
  gMetaRoleNames.Add(cMetaRoleName_ID);
  gMetaRoleNames.Add(cMetaRoleName_Claim);
  gMetaRoleNames.Add(cMetaRoleName_Operation);

finalization
  FreeAndNil(gMetaRoleNames);
end.
