// -----------------------------------------------------------------------------
//
// ace.Core.Graph
//
// -----------------------------------------------------------------------------
//
// Graphs for Typed Operations and Predefined Queries
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.Core.Graph;

{$MODE Delphi}

interface

// _____________________________________________________________________________
//
//               ! ! !   WORK IN PROGRESS, NOT COMPLETE   ! ! !
// _____________________________________________________________________________
//

uses
  common.SimpleLists,
  ace.Core.Engine;

type
   TcGraphNodeList = class;

   TcGraphNode =
     class
       private
         fClaimType: TcClaimType;
         fRole: TcRole;
         fName: string;
         fInclude: boolean;
         fRequired: boolean;
         fSubNodes: TcGraphNodeList;
       protected
         function GetNodeName: string;
       public
         constructor Create(aClaimType: TcClaimType; aRole: TcRole;
           aInclude: boolean; aRequired: boolean);
         destructor Destroy; override;

         property ClaimType: TcClaimType read fClaimType;
         property Role: TcRole read fRole;
         property Name: string read fName write fName;
         property Include: boolean read fInclude write fInclude;
         property Required: boolean read fRequired write fRequired;
         property NodeName: string read GetNodeName;

         property SubNodes: TcGraphNodeList read fSubNodes;
       end;

   TcGraphNodeList =
     class
       private
         fList: TcStringObjectList<TcGraphNode>; // Role name, Graph Node - Owned, Not sorted
       protected
         function GetItem(const aIndex: integer): TcGraphNode;
         function GetCount: integer;
       public
         constructor Create;
         destructor Destroy; override;

         procedure Add(aGraphNode: TcGraphNode);
         procedure DestroyAll;

         property Items[const aIndex: integer]: TcGraphNode read GetItem; default;
         property Count: integer read GetCount;
       end;

   TcGraph =
     class (TcGraphNode)
       public

       end;

implementation

{ TcGraphNode }

constructor TcGraphNode.Create(aClaimType: TcClaimType; aRole: TcRole;
  aInclude: boolean; aRequired: boolean);
  begin
  inherited Create;
  fClaimType := aClaimType;
  fRole := aRole;
  fInclude := aInclude;
  fRequired := aRequired;
  fSubNodes := TcGraphNodeList.Create;
  end;

destructor TcGraphNode.Destroy;
  begin
  fSubNodes.Free;
  inherited;
  end;

function TcGraphNode.GetNodeName: string;
  begin
  result := Name;
  if Include
     then begin
          if Required
             then result := result + ' [required]';
          end
     else result := result + ' x';
  end;

{ TcGraphNodeList }

constructor TcGraphNodeList.Create;
  begin
  inherited Create;
  fList := TcStringObjectList<TcGraphNode>.Create(true {owned}, false {not sorted});
  end;

destructor TcGraphNodeList.Destroy;
  begin
  fList.Free;
  inherited;
  end;

function TcGraphNodeList.GetCount: integer;
  begin
  result := fList.Count;
  end;

function TcGraphNodeList.GetItem(const aIndex: integer): TcGraphNode;
  begin
  result := fList.Objects[aIndex];
  end;

procedure TcGraphNodeList.Add(aGraphNode: TcGraphNode);
  begin
  fList.Add(aGraphNode.Role.Name, aGraphNode);
  end;

procedure TcGraphNodeList.DestroyAll;
  var
    i: integer;
  begin
  for i := 0 to fList.Count-1 do
    fList.Objects[i].Free;
  fList.Clear;
  end;

end.
