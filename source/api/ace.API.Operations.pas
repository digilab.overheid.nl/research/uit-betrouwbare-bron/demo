// -----------------------------------------------------------------------------
//
// ace.API.Operations
//
// -----------------------------------------------------------------------------
//
// Implements a simple API to reroute incomming operations to pascal script
// methods that handle translation to claims, validation etc.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.API.Operations;

{$MODE Delphi}

interface

uses
  Classes,
  IdComponent, IdCustomHTTPServer,
  common.RadioStation,
  ace.Core.Engine, ace.Script, ace.API.Server;

const
  cOperations_Identifier = 'Operations';
  cOperations_Name = 'Operations';
  cOperations_Port = 4200;

type
  TcAPIOperations =
    class (TcAPIHandler)
      private
        fScript: TcScript;
        fRadioOnline: boolean;

      protected
        procedure InitScript;
        procedure HandleRequest; override;

        property Script: TcScript read fScript;

      public
        constructor Create(aIdentifier, aName: string; aPort: word);
        destructor Destroy; override;
        procedure Activate; override;
        procedure ListenToRadio (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);
      end;

implementation

uses
  SysUtils,
  common.Utils, common.Json, fpjson,
  ace.Debug;

constructor TcAPIOperations.Create(aIdentifier, aName: string; aPort: word);
  begin
  inherited Create(aIdentifier, aName, aPort);
  fScript := nil;
  fRadioOnline := false;
  end;

destructor TcAPIOperations.Destroy;
  begin
  if fRadioOnline
    then gRadioStation.RemoveRadio(Self);

  FreeAndNil(fScript);
  inherited;
  end;

procedure TcAPIOperations.Activate;
  begin
  inherited;
  if not fRadioOnline
     then begin
          gRadioStation.AddRadio(Self, ListenToRadio);
          fRadioOnline := true;
          end;
  end;

procedure TcAPIOperations.InitScript;
  begin
  FreeAndNil(fScript); // Exceptions might have led to not clearing the script

  Debug('asi', 'asi: TcAPIOperations.InitScript');
  Debug('asi', 'asi:   Engine assigned?: %s', [BooleanToYesNo(Assigned(asi.Engine))]);

  fScript := TcScript.Create;
  try
    fScript.Init(ASI);
    fScript.LoadFromFile;
    fScript.Compile;

    Debug('asi', 'asi:   Script compiled');
    Debug('asi', 'asi:   Script.asi.Engine assigned?: %s', [BooleanToYesNo(Assigned(fScript.asi.Engine))]);

  except
    on E: Exception do
      begin
      FreeAndNil(fScript);
      raise;
      end;
    end;
  end;

procedure TcAPIOperations.ListenToRadio (aSender: TObject; var aRadioEvent: TcRadioStation_Event; aData: Pointer);
  begin
  if assigned(aSender) then ; // ignore compiler warning

  if aRadioEvent = cRadioEvent_Offline
    then begin
         fRadioOnline := false;
         exit;
         end;

  if (aRadioEvent = cRadioEvent_Script_Changed)
     then begin
          // Clear cached script
          if Assigned(fScript)
             then FreeAndNil(fScript);
          end;
  end;

procedure TcAPIOperations.HandleRequest;
  var
    lOperationIdentifier,
    lOperationName,
    lMethodName: string;
    lJson: TJsonObject;
    lForValidation: boolean;
    lTime: integer;
  begin
  with asi do
    begin
    Debug('asi', 'asi: TcAPIOperations.HandleRequest');
    Debug('asi', 'asi:   Engine assigned?: %s', [BooleanToYesNo(Assigned(asi.Engine))]);

    // Fetch operation name
    lOperationName := Copy(Request.Resource, 2, Length(Request.Resource));
    lOperationIdentifier := ToIdentifier(lOperationName);

    // Create operation
    Session.StartOperation(lOperationName);
    Operation := Session.Operation; // We need the operation after commit, so store it directly
    Debug('httpServer', 'Operation started for ''%s''', [lOperationIdentifier]);
    DebugIncIndent('httpServer');
    try

      // Init script - will raise exception when problems arise
      InitScript;

      // Command should be POST
      if Request.CommandType <> hcPOST
         then begin
              Response.Content := Format('Command %s unsupported.', [Request.CommandTypeStr]);
              exit;
              end;

      // There should be no parameters present?
      if (Request.Params.Count>0)
         then begin
              Response.Content := 'No parameters allowed.';
              exit;
              end;

      // Valid json body?
      if (Request.Content='') or (Request.ContentParseError<>'') or not Assigned(Request.Json)
         then begin
              Response.Content := Format('Invalid json content (%s).', [Request.ContentParseError]);
              exit;
              end;
      lJson := Request.Json;

      // Try to find OnReceive handler in script
      lMethodName := Format('Operation_%s_OnReceive', [lOperationIdentifier]);
      if not Script.HasMethod(lMethodName)
         then begin
              Response.Content := Format('Unkown operation: %s.', [lOperationIdentifier]);
              exit;
              end;

      // Process operation parameters
      if lJson.TryGetValue('forValidation', lForValidation)
         then begin
              Session.Operation.ForValidation := true;
              Debug('httpServer', 'ForValidation');
              end;

      if lJson.TryGetValue('validFrom', lTime)
         then begin
              CheckTime('validFrom', lTime);
              Session.Operation.ValidFrom := lTime;
              Debug('httpServer', Format('ValidFrom = %d', [lTime]));
              end;

      if lJson.TryGetValue('validUntil', lTime)
         then begin
              CheckTime('validUntil', lTime);
              Session.Operation.ValidUntil := lTime;
              Debug('httpServer', Format('ValidUntil = %d', [lTime]));
              end;

      // Call method
      Debug('httpServer', 'Executing OnReceive');
      Script.CallMethod(lMethodName, []);

    finally
      DebugDecIndent('httpServer');
      if (Session.Operation.ForValidation)
         then begin
              // Rollback
              Session.RollBack;
              Debug('httpServer', 'No changes where made. Operation was for validation only.');
              end
      else if (Response.Code > cLastHttpOkCode)
         then begin
              // Rollback
              Session.RollBack;
              Debug('httpServer', 'Changes where discarded (rollback)');
              end
         else begin
              // Commit
              Session.Commit;
              Debug('httpServer', 'Changes where persisted (commit)');

              // Try to AfterCommit handler in script
              lMethodName := Format('Operation_%s_AfterCommit', [lOperationIdentifier]);
              if Script.HasMethod(lMethodName)
                 then begin
                      // Call method
                      Debug('httpServer', 'Executing AfterCommit');
                      DebugIncIndent('httpServer');
                      try
                        Script.CallMethod(lMethodName, []);
                      finally
                        DebugDecIndent('httpServer');
                        end;
                      end;
              end;
      end;
    end;
  end;

initialization
  gAPIHandlers.Add(TcAPIOperations.Create(cOperations_Identifier, cOperations_Name, cOperations_Port));
end.
