// -----------------------------------------------------------------------------
//
// ace.API.Server
//
// -----------------------------------------------------------------------------
//
// Simple Http Server implementation to respond to API requests.
// (not a real REST server, just basic responses).
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.API.Server;

{$MODE Delphi}

interface

uses
  Types, Graphics,
  Classes, Variants, common.Json, fpjson,
  Controls, Forms, Dialogs, StdCtrls, ExtCtrls, Buttons, Grids, Generics.Collections,
  IdBaseComponent, IdComponent, IdCustomTCPServer, IdCustomHTTPServer, IdHTTPServer, IdContext,
  common.ErrorHandling, common.RadioStation, common.SimpleLists, ace.Core.Engine;

const
  cIPAddress = '127.0.0.1'; // For all API's
  cBtnToggle_Start = 'Start';
  cBtnToggle_Stop = 'Stop';
  cLastHttpOkCode = 299;

  cRadioEvent_APICall_Handled = 'APIServer.Call.Handled'; // Object = nil

type
  EAPIError = class (EDeveloper);
  
  // Error codes
  TcAPIErrorCode = (
     ecRequestElementMissing, ecRequestInvalidValue,
     ecClaimTypeNotFound, ecAnnotationTypeNotFound, ecPropertyNotFound,
     ecProcessingFailed,
     ecRuleViolation,
     ecAssertionFailed,
     ecDataInconsistency
  );

  // Severity
  TcSeverity = (
     seNone, // Unknown / Not assigned
     seFatal, // Errors, not for rules.
     seBlocking, // Blocking rule
     seOverridable, // Can be overriden
     seWarning, // Warning
     seInfo
  );

type
  TfAPIServer = class(TForm)
    StringGrid: TStringGrid;
    pnlControls: TPanel;
    pnlControlsRight: TPanel;
    btnToggle: TButton;
    btnInspector: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure StringGridDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure StringGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure btnToggleClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnInspectorClick(Sender: TObject);
  private
  public
    procedure StartAPI(aIdentifier: string);
    procedure StopAPI(aIdentifier: string);
  end;

  // Forward declaration
  TcAPIHandler = class;

  // API Handlers
  //
  //   List of all API Handlers
  //
  TcAPIHandlers =
    class
      private
        fList: TObjectList<TcAPIHandler>;
      protected
        function GetValue(const aIndex: integer): TcAPIHandler;
        function GetCount: integer;
      public
        constructor Create;
        destructor Destroy; override;

        function Find(aIdentifier: string): TcAPIHandler;
        procedure Add(aAPIHandler: TcAPIHandler);
        property Values[const aIndex: integer]: TcAPIHandler read GetValue; default;
        property Count: integer read GetCount;
      end;

  // Request
  //
  //   Request as passed to and handled by an API handler
  //
  TcRequest =
    class
      private
        fCommandType: THttpCommandType;
        fCommandTypeStr: string;
        fResource: string;
        fParams: TStringlist;
        fContent: string;
        fContentType: string;
        fJson: TJsonObject;
        fContentParseError: string;

      protected
        function GetCommandTypeStr: string;
        function GetParamStr: string;

      public
        constructor Create(aRequestInfo: TIdHTTPRequestInfo);
        destructor Destroy; override;

        property CommandType: THttpCommandType read fCommandType;
        property CommandTypeStr: string read GetCommandTypeStr;
        property Resource: string read fResource;
        property Params: TStringlist read fParams;
        property ParamStr: string read GetParamStr;
        property Content: string read fContent;
        property ContentType: string read fContentType;
        property Json: TJsonObject read fJson;
        property ContentParseError: string read fContentParseError;
      end;

  // Response
  //
  //   Response as created by the API handler
  //
  TcResponse =
    class
      private
        fCode: integer;
        fContent: string;
        fContentType: string;

      protected
        function GetCodeStr: string;

      public
        constructor Create(aResponseInfo: TIdHTTPResponseInfo);

        // Properties
        property Code: integer read fCode write fCode;
        property CodeStr: string read GetCodeStr;
        property Content: string read fContent write fContent;
        property ContentType: string read fContentType write fContentType;
      end;


  // API Script Interface
  //
  //   Context and functions to be published to the script
  //
  TcAPIScriptInterface =
    class
      private
        fEngine: TcEngine;
        fSession: TcSession;
        fOperation: TcOperation;
        fRequest: TcRequest;
        fResponse: TcResponse;
        fRegisteredAt, fExpiredAt, fValidFrom, fValidUntil: TcTime;
        fRuleName: string;
        fClaimsInDoubt: TcStringObjectList<TcClaim>;

      protected
        // Property handlers
        function GetRuleActive: boolean;
        function GetRuleInDoubt: boolean;

        // Utility routines
        function TupleArgsToStr(aArgs: array of const): string;
        procedure iResetTemporal;

        // Internal error handling
        procedure iAPIError(aAPIErrorCode: TcAPIErrorCode; aPath, aMessage: string; aSeverity: TcSeverity = seFatal); overload;
        procedure iAPIError(aAPIErrorCode: TcAPIErrorCode; aPath: string); overload;

      public
        constructor Create;
        procedure Init(aRequestInfo: TIdHttpRequestInfo; aResponseInfo: TIdHttpResponseInfo);
        destructor Destroy; override;

        // Error handling routines
        procedure ProcessingFailed(aPath, aMessage: string);

        // Validation routines
        procedure CheckTime(aPath: string; aTime: integer);

        // Rules
        procedure StartRule(aName: string);
        procedure RuleViolation(aMessage: string);
        procedure EndRule;

        property RuleName: string read fRuleName;
        property RuleActive: boolean read GetRuleActive;
        property RuleInDoubt: boolean read GetRuleInDoubt;

        // Json routines
        function JsonTryGetStr(aJson: TJsonObject; aKey: string; out aValue: string): boolean;
        function JsonGetStr(aJson: TJsonObject; aKey: string): string;
        function JsonTryGetInt(aJson: TJsonObject; aKey: string; out aValue: integer): boolean;
        function JsonGetInt(aJson: TJsonObject; aKey: string): integer;
        function JsonTryGetBool(aJson: TJsonObject; aKey: string; out aValue: boolean): boolean;
        function JsonGetBool(aJson: TJsonObject; aKey: string): boolean;
        function JsonTryGetObj(aJson: TJsonObject; aKey: string; out aValue: TJsonObject): boolean;
        function JsonGetObj(aJson: TJsonObject; aKey: string): TJsonObject;
        function JsonTryGetArray(aJson: TJsonObject; aKey: string; out aValue: TJsonArray): boolean;
        function JsonGetArray(aJson: TJsonObject; aKey: string): TJsonArray;

        // Temporal Aspects
        procedure SetRegisteredAt(aTime: integer);
        procedure SetExpiredAt(aTime: integer);
        procedure SetValidFrom(aTime: integer);
        procedure SetValidUntil(aTime: integer);
        procedure ResetTemporal;
        function GetRegisteredAtForQuery: TcTime;
        function GetValidFromForQuery: TcTime;
        function GetValidFromForAdd: TcTime;
        function GetValidUntilForAdd: TcTime;

        // Claim routines
        function FindClaim(aClaimTypeName: string; aIdentityStr: string; var aClaim: TcClaim): boolean;
        function AddClaim(aClaimTypeName: string; aTuple: array of const): TcClaim;

        // Annotation routines
        function Annotate(aClaim: TcClaim; aAnnotationTypeName: string; aTuple: array of const): TcAnnotation;

        // Object routines
        function FindObject(aObjectTypeName: string; aIdentityStr: string; var aObject: TcClaim): boolean;
        function GetProperty(aObject: TcClaim; aPropertyName: string): TcClaim;
        function AddObject(aObjectTypeName: string; aTuple: array of const): TcClaim;
        function AddPropertyStr(aObject: TcClaim; aPropertyName: string; aValue: string): TcClaim;
        function AddPropertyInt(aObject: TcClaim; aPropertyName: string; aValue: integer): TcClaim;
        function AddPropertyBool(aObject: TcClaim; aPropertyName: string; aValue: boolean): TcClaim;
        function GetPropertyStr(aObject: TcClaim; aPropertyName: string): string;
        function GetPropertyInt(aObject: TcClaim; aPropertyName: string): integer;
        function GetPropertyBool(aObject: TcClaim; aPropertyName: string): boolean;
       
        // Notifications
        procedure BroadCastCloudEvent(aTypeStr: string; aObjectName, aObjectKey: string);

        //
        property Engine: TcEngine read fEngine;
        property Session: TcSession read fSession;
        property Operation: TcOperation read fOperation write fOperation;
        property Request: TcRequest read fRequest;
        property Response: TcResponse read fResponse;
      end;


  // API Handler
  //
  //   Ancestor for all API handlers
  //
  TcAPIHandler =
    class
      private
        fHTTPServer: TIdHTTPServer;
        fIdentifier: string; // API identifier
        fName: string; // API Name
        fIPAddress: string;
        fPort: word;

        fAsi: TcAPIScriptInterface;

      protected
        property HTTPServer: TIdHTTPServer read fHTTPServer;
        function GetIsActive: boolean;

        // 1. HandleHttpServerCommand or HandleHttpServerOtherCommand is
        //    invoked by the HTTPServer.
        //    (AddCorsHeadersToResponse is used by both).
        procedure HandleHttpServerCommand(
          aContext: TIdContext; aRequestInfo: TIdHTTPRequestInfo; aResponseInfo: TIdHTTPResponseInfo);
        procedure HandleHttpServerOtherCommand(
          aContext: TIdContext; aRequestInfo: TIdHTTPRequestInfo; aResponseInfo: TIdHTTPResponseInfo);
        procedure AddCorsHeadersToResponse(aResponseInfo: TIdHTTPResponseInfo);

        // 2. HandleHttpServerCommand calls iHandleRequest to perform basic
        //    request handling. (for example /favicon.ico).
        //    When the request needs to be handled HandleRequest is called.
        procedure iHandleRequest;

        // 3. Actual handling of the request.
        //    Override this method in subtypes.
        procedure HandleRequest; virtual; abstract;

      public
        constructor Create(aIdentifier, aName: string; aPort: word);
        destructor Destroy; override;

        procedure Activate; virtual;
        procedure Deactivate; virtual;
        property IsActive: boolean read GetIsActive;

        property Identifier: string read fIdentifier;
        property Name: string read fName;
        property IPAddress: string read fIPAddress;
        property Port: word read fPort;

        property asi: TcAPIScriptInterface read fAsi;
      end;

var
  fAPIServer: TfAPIServer;
  gAPIHandlers: TcAPIHandlers;

implementation

{$R *.lfm}

uses
  SysUtils,
  common.Utils, 
  ace.Core.Query,
  ace.Debug, ace.Settings,
  ace.Core.Notification, ace.Forms.Inspector,
  diagram.Settings;


// -- Misc functions

var
  // See initialization for content
  gResponseCodes: TStringlist;
  gTextualContentTypes: TStringlist;

function HttpCommandTypeToStr(aCommandType: THttpCommandType; aCommand: String): string;
  begin
  case aCommandType of
    hcHEAD: result := 'HEAD';
    hcGET: result := 'GET';
    hcPOST: result := 'POST';
    hcDELETE: result := 'DELETE';
    hcPUT: result := 'PUT';
    hcTRACE: result := 'TRACE';
    hcOPTION: result := 'OPTION';
    hcUnknown:
      begin
      if CompareText(aCommand, 'PATCH')=0
         then result := 'PATCH'
         else result := '';
      end
    else result := '';
    end;
  end;

function HttpResponseCodeToStr(aCode: integer): string;
  var
    lIndex: integer;
  begin
  lIndex := gResponseCodes.IndexOfName(IntToStr(aCode));
  if lIndex>=0
     then result := gResponseCodes.ValueFromIndex[lIndex]
     else result := '';
  end;

function IsTextualContentType(aContentType: string): boolean;
  var
    lIndex: integer;
  begin
  result := gTextualContentTypes.Find(aContentType, lIndex);
  end;

function IsJsonContentType(aContentType: string): boolean;
  begin
  result := Pos('JSON', Uppercase(aContentType))>0;
  end;

function SeverityToCode(aSeverity: string): TcSeverity;
  begin
  if CompareText(aSeverity, 'fatal')=0
     then result := seFatal
  else if CompareText(aSeverity, 'blocking')=0
     then result := seBlocking
  else if CompareText(aSeverity, 'overridable')=0
     then result := seOverridable
  else if CompareText(aSeverity, 'warning')=0
     then result := seWarning
  else if CompareText(aSeverity, 'info')=0
     then result := seInfo
  else result := seNone;
  end;

function SeverityToStr(aSeverity: TcSeverity): string;
  begin
  case aSeverity of
   seFatal: result := 'Fatal';
   seBlocking: result := 'Blocking';
   seOverridable: result := 'Overridable';
   seWarning: result := 'Warning';
   seInfo: result := 'Info';
   else result := 'none';
   end;
  end;


{ TfAPIServer }

procedure TfAPIServer.FormCreate(Sender: TObject);
  var
    i: integer;
    lHandler: TcAPIHandler;
  begin
  gSettings.LoadFormPosition(Self);

  // Dimensions
  StringGrid.ColCount := 2;
  StringGrid.RowCount := gAPIHandlers.Count+1;

  // Header
  StringGrid.Cells[0, 0] := 'API';
  StringGrid.Cells[1, 0] := 'Status';

  // API's
  for i := 0 to gAPIHandlers.Count-1 do
    begin
    lHandler := gAPIHandlers[i];
    StringGrid.Cells[0, i+1] := lHandler.Name;
    StringGrid.Cells[1, i+1] := 'Not active';
    end;

  // Hide inspector button
  pnlControlsRight.Width := 96;
  end;

procedure TfAPIServer.FormDestroy(Sender: TObject);
  var
    i: integer;
    lHandler: TcAPIHandler;
  begin
  gSettings.SaveFormPosition(Self);

  // Deactivate all API's
  for i := 0 to gAPIHandlers.Count-1 do
    begin
    lHandler := gAPIHandlers[i];
    lHandler.Deactivate;
    end;
  end;

procedure TfAPIServer.FormClose(Sender: TObject; var Action: TCloseAction);
  begin
  Action := caFree;
  fAPIServer := nil;
  end;

procedure TfAPIServer.StringGridDrawCell(Sender: TObject; aCol, aRow: Integer; Rect: TRect; State: TGridDrawState);
  var
    lHandler: TcAPIHandler;
    lStr: string;
  begin
  with TStringGrid(Sender) do
    begin
    if (aRow>0) and (aRow<=gAPIHandlers.Count)
       then begin
            lHandler := gAPIHandlers[aRow-1];
            if aCol=0
               then lStr := lHandler.Name
               else begin
                    if lHandler.IsActive
                       then lStr := Format('Active on http://%s:%d', [cIPAddress, lHandler.Port])
                       else lStr := 'Not active';
                    end;

            if lHandler.IsActive
               then Canvas.Brush.Color := clWebPaleGreen
               else Canvas.Brush.Color := clWindow;
            Canvas.FillRect(Rect);
            Canvas.Font.Color := clBlack;
            InflateRect(Rect, 2, 2);
            if gdSelected in State
               then Canvas.Font.Style := [fsBold]
               else Canvas.Font.Style := [];
            Canvas.TextOut(Rect.Left+4, Rect.Top+6, lStr);
            end;
    end;
  end;

procedure TfAPIServer.StringGridSelectCell(Sender: TObject; ACol, ARow: Integer; var CanSelect: Boolean);
  var
    lHandler: TcAPIHandler;
  begin
  btnToggle.Enabled := (aRow>0) and (aRow<=gAPIHandlers.Count);
  btnInspector.Enabled := false; // btnToggle.Enabled; -- Instable during update
  if btnToggle.Enabled
     then begin
          lHandler := gAPIHandlers[aRow-1];
          if lHandler.IsActive
             then btnToggle.Caption := cBtnToggle_Stop
             else btnToggle.Caption := cBtnToggle_Start;
          btnToggle.Tag := aRow-1; // Store handler ID in tag.
          end
     else btnToggle.Caption := cBtnToggle_Start;
  end;

procedure TfAPIServer.btnInspectorClick(Sender: TObject);
  var
    lHandler: TcAPIHandler;
    lfInspector: TfInspector;
  begin
  // Use tag from btnToggle
  if not ((btnToggle.Tag>=0) and (btnToggle.Tag<gAPIHandlers.Count))
     then exit;
  lHandler := gAPIHandlers[btnToggle.Tag];

  lfInspector := TfInspector.Create(Application);
  lfInspector.Init(lHandler.asi.Session, nil, nil, false {Not minimized}, false {No redraw during transaction});
  lfInspector.Show;
  end;

procedure TfAPIServer.btnToggleClick(Sender: TObject);
  var
    lHandler: TcAPIHandler;
  begin
  if not ((btnToggle.Tag>=0) and (btnToggle.Tag<gAPIHandlers.Count))
     then exit;
  lHandler := gAPIHandlers[btnToggle.Tag];
  if lHandler.IsActive
     then begin
          lHandler.Deactivate;
          btnToggle.Caption := cBtnToggle_Start;
          end
     else begin
          lHandler.Activate;
          btnToggle.Caption := cBtnToggle_Stop;
          end;
  StringGrid.Repaint;
  end;

procedure TfAPIServer.StartAPI(aIdentifier: string);
  var
    lHandler: TcAPIHandler;
  begin
  lHandler := gAPIHandlers.Find(aIdentifier);
  if not Assigned(lHandler)
     then EUsr('Unknown API: %s', [aIdentifier]);
  lHandler.Activate;
  end;

procedure TfAPIServer.StopAPI(aIdentifier: string);
  var
    lHandler: TcAPIHandler;
  begin
  lHandler := gAPIHandlers.Find(aIdentifier);
  if not Assigned(lHandler)
     then EUsr('Unknown API: %s', [aIdentifier]);
  lHandler.Deactivate;
  end;


{ TcAPIHandlers }

constructor TcAPIHandlers.Create;
  begin
  inherited Create;
  fList := TObjectList<TcAPIHandler>.Create;
  end;

destructor TcAPIHandlers.Destroy;
  begin
  FreeAndNil(fList);
  inherited;
  end;

function TcAPIHandlers.GetValue(const aIndex: integer): TcAPIHandler;
  begin
  result := fList[aIndex];
  end;

function TcAPIHandlers.GetCount: integer;
  begin
  result := fList.Count;
  end;

function TcAPIHandlers.Find(aIdentifier: string): TcAPIHandler;
  begin
  for result in fList do
    if result.Identifier = aIdentifier
       then exit;
  result := nil;
  end;

procedure TcAPIHandlers.Add(aAPIHandler: TcAPIHandler);
  begin
  fList.Add(aAPIHandler);
  end;


{ TcAPIInterface }

constructor TcAPIScriptInterface.Create;
  begin
  inherited Create;
  Debug('asi', 'asi: Create');
  fEngine := gSettings.Engine;
  Debug('asi', 'asi: Engine assigned');
  fSession := TcSession.Create(fEngine);
  fRequest := nil;
  fResponse := nil;
  fRegisteredAt := cNoTime;
  fExpiredAt := cNoTime;
  fValidFrom := cNoTime;
  fValidUntil := cNoTime;
  fRuleName := '';
  fClaimsInDoubt := TcStringObjectList<TcClaim>.Create(false {Not owned}, false {Not sorted});
  end;

procedure TcAPIScriptInterface.Init(aRequestInfo: TIdHttpRequestInfo; aResponseInfo: TIdHttpResponseInfo);
  begin
  FreeAndNil(fRequest);
  fRequest := TcRequest.Create(aRequestInfo);
  FreeAndNil(fResponse);
  fResponse := TcResponse.Create(aResponseInfo);
  iResetTemporal;
  fClaimsInDoubt.Clear;
  end;

destructor TcAPIScriptInterface.Destroy;
  begin
  Debug('asi', 'asi: Destroy');
  FreeAndNil(fSession);
  FreeAndNil(fRequest);
  FreeAndNil(fResponse);
  FreeAndNil(fClaimsInDoubt);
  inherited;
  end;

function TcAPIScriptInterface.GetRuleActive: boolean;
  begin
  result := fRuleName<>'';
  end;

function TcAPIScriptInterface.GetRuleInDoubt: boolean;
  begin
  result := fClaimsInDoubt.Count>0;
  end;

function TcAPIScriptInterface.TupleArgsToStr(aArgs: array of const): string;
  var
    i: integer;
    lArgStr: string;
    lArgs: TStringlist;
  begin
  result := '';
  (* Conversion with TVarRec gave issues.
     For debugging use a call with ('$operation', '#42').
    case aArgs[i].VType of
      vtInteger: lStr := IntToStr(aArgs[i].VInteger);
      vtBoolean: lStr := BooleanToTrueFalse(aArgs[i].VBoolean);
      vtChar: lStr := string(aArgs[i].VChar);
      //vtExtended: lStr := FloatToStr(aArgs[i].VExtended);
      vtString: lStr := string(aArgs[i].VString);
      vtAnsiString: lStr := string(aArgs[i].VAnsiString);
      //vtCurrency: lStr := FloatToStr(aArgs[i].VExtended);
      vtInt64: lStr := IntToStr(aArgs[i].VInteger);
      vtUnicodeString: lStr := string(aArgs[i].VUnicodeString);
      else lStr := '<value not supported>';
    end;
  *)

  // Workaround: Let Delphi handle conversion
  lArgStr := '';
  for i := Low(aArgs) to High(aArgs) do
    begin
    if lArgStr<>'' then lArgStr := lArgStr+',';
    lArgStr := lArgStr + '%s';
    end;
  lArgStr := Format(lArgStr, aArgs);

  // Pre process elements
  lArgs := StringToStringlist(lArgStr, ',');
  try
    for i := 0 to lArgs.Count-1 do
      if CompareText(lArgs[i], cMetaRoleName_Operation)=0
         then lArgs[i] := Operation.RefIDStr;

    result := StringListToString(lArgs,',');

  finally
    lArgs.Free;
    end;
  end;

procedure TcAPIScriptInterface.iResetTemporal;
  begin
  fRegisteredAt := cNoTime;
  fExpiredAt := cNoTime;
  fValidFrom := cNoTime;
  fValidUntil := cNoTime;
  end;

procedure TcAPIScriptInterface.iAPIError(aAPIErrorCode: TcAPIErrorCode; aPath, aMessage: string; aSeverity: TcSeverity = seFatal);
  var
    lHttpCode: integer;
    lErrorType, lPath, lMessage, lSeverity: string;
    lJson, ljClaim: TJsonObject;
    ljClaims: TJsonArray;
    lClaim: TcClaim;
    c: Integer;
  begin
  Debug('script', '- iAPIError(ErrorCode:%d, Path:''%s'')', [Ord(aAPIErrorCode), aPath]);

  lHttpCode := 400;
  lErrorType := 'General error';
  lPath := aPath;
  lMessage := '';
  lSeverity := SeverityToStr(aSeverity);

  case aAPIErrorCode of

    ecRequestElementMissing:
      begin
      lHttpCode := 400; // Bad request
      lErrorType := 'Invalid json';
      lMessage := Format('Element ''%s'' missing.', [aPath]);
      end;

    ecRequestInvalidValue:
      begin
      lHttpCode := 400; // Bad request
      lErrorType := 'Invalid value';
      lMessage := Format('Value of element ''%s'' invalid.', [aPath]);
      end;

    ecClaimTypeNotFound:
      begin
      lHttpCode := 412; // Precondition failed
      lErrorType := 'ClaimType not found';
      lMessage := Format('ClaimType ''%s'' not found.', [aPath]);
      end;

    ecAnnotationTypeNotFound:
      begin
      lHttpCode := 412; // Precondition failed
      lErrorType := 'AnnotationType not found';
      lMessage := Format('AnnotationType ''%s'' not found.', [aPath]);
      end;

    ecPropertyNotFound:
      begin
      lHttpCode := 412; // Precondition failed
      lErrorType := 'Property not found';
      lMessage := Format('Property ''%s'' not found.', [aPath]);
      end;

    ecProcessingFailed:
      begin
      lHttpCode := 412; // Precondition failed
      lErrorType := 'Processing error';
      lMessage := Format(lMessage, [aPath]);
      end;

    ecRuleViolation:
      begin
      lHttpCode := 412; // Precondition failed
      lErrorType := 'Rule violation';
      lPath := fRuleName;
      lMessage := aMessage;
      end;

    ecAssertionFailed:
      begin
      lHttpCode := 500; // Internal server error
      lErrorType := 'Assertion failed';
      lMessage := Format(aMessage, [aPath]);
      end;

    ecDataInconsistency:
      begin
      lHttpCode := 500; // Internal server error
      lErrorType := 'Data inconsistency found';
      lMessage := Format(aMessage, [aPath]);
      end;

    else EDev('Unknown error code: %d', [Ord(aAPIErrorCode)]);
    end;

  lJson := TJsonObject.Create;
  try
    // Create Response Json
    lJson.AddPair('error', lErrorType);
    lJson.AddPair('path', lPath);

    if (aAPIErrorCode = ecRuleViolation) and (RuleInDoubt)
       then begin
            lMessage := lMessage + ' ' +
                'The conclusion of this rule is based on claims that are doubted. ' +
                'Therefore the conclusion might be invalid. ' +
                'A manual validation must be performed. ' +
                'The rule is no longer blocking. An overrule can be issued';
            lJson.AddPair('message', lMessage);

            lJson.AddPair('originalSeverity', lSeverity);
            lSeverity := SeverityToStr(seOverridable);
            lJson.AddPair('severity', lSeverity);
            ljClaims := TJsonArray.Create;
            for c := 0 to fClaimsInDoubt.Count-1 do
              begin
              lClaim := fClaimsInDoubt.Objects[c];
              ljClaim := TJsonObject.Create;
              ljClaim.AddPair('claimType', lClaim.ClaimType.Name);
              ljClaim.AddPair('ID', lClaim.RefIDStr);
              ljClaim.AddPair('expression', lClaim.GetExpression);
              ljClaims.Add(ljClaim);
              end;
            lJson.AddPair('claimsInDoubt', ljClaims);
            end
       else begin
            lJson.AddPair('message', lMessage);
            lJson.AddPair('severity', lSeverity);
            end;

    // Assign to Response
    Response.Code := lHttpCode;
    Response.ContentType := 'application/json';
    Response.Content := lJson.Format(2);

    raise EAPIError.CreateFmt('%s: %s -> %s', [lErrorType, lPath, lMessage]);

  finally
    lJson.Free;
    end;
  end;

procedure TcAPIScriptInterface.iAPIError(aAPIErrorCode: TcAPIErrorCode; aPath: string);
  begin
  iAPIError(aAPIErrorCode, aPath, '');
  end;

procedure TcAPIScriptInterface.ProcessingFailed(aPath, aMessage: string);
  begin
  iAPIError(ecProcessingFailed, aPath, aMessage);
  end;

procedure TcAPIScriptInterface.CheckTime(aPath: string; aTime: integer);
  begin
  Debug('script', '- CheckTime(Path:''%s'', Time:%d)', [aPath, aTime]);
  DebugIncIndent('script');
  try
    if aTime<=0
       then iAPIError(ecRequestInvalidValue, aPath);
  finally
    DebugDecIndent('script');
    end;
  end;

procedure TcAPIScriptInterface.StartRule(aName: string);
  begin
  fRuleName := aName;
  fClaimsInDoubt.Clear;
  end;

procedure TcAPIScriptInterface.RuleViolation(aMessage: string);
  begin
  iAPIError(ecRuleViolation, '', aMessage);
  end;

procedure TcAPIScriptInterface.EndRule;
  begin
  fRuleName := '';
  fClaimsInDoubt.Clear;
  end;

function TcAPIScriptInterface.JsonTryGetStr(aJson: TJsonObject; aKey: string; out aValue: string): boolean;
  begin
  Debug('script', '- JsonTryGetStr(Obj:%s, Key:''%s'', Value:?)', [BooleanToYesNo(Assigned(aJson)), aKey]);
  DebugIncIndent('script');
  try
    aValue := '';
    result := aJson.TryGetValue(aKey, aValue);
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Value = ''%s''', [aValue]);
    end;
  end;

function TcAPIScriptInterface.JsonGetStr(aJson: TJsonObject; aKey: string): string;
  begin
  Debug('script', '- JsonGetStr(Obj:%s, Key:''%s'', Value:?)', [BooleanToYesNo(Assigned(aJson)), aKey]);
  DebugIncIndent('script');
  try
    if not JsonTryGetStr(aJson, aKey, result)
       then iAPIError(ecRequestElementMissing, aKey);
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Value = ''%s''', [result]);
    end;
  end;

function TcAPIScriptInterface.JsonTryGetInt(aJson: TJsonObject; aKey: string; out aValue: integer): boolean;
  begin
  Debug('script', '- JsonTryGetInt(Obj:%s, Key:''%s'', Value:?)', [BooleanToYesNo(Assigned(aJson)), aKey]);
  DebugIncIndent('script');
  try
    result := aJson.TryGetValue(aKey, aValue);
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Value = %d', [aValue]);
    end;
  end;

function TcAPIScriptInterface.JsonGetInt(aJson: TJsonObject; aKey: string): integer;
  begin
  Debug('script', '- JsonGetInt(Obj:%s, Key:''%s'', Value:?)', [BooleanToYesNo(Assigned(aJson)), aKey]);
  DebugIncIndent('script');
  try
    if not JsonTryGetInt(aJson, aKey, result)
       then iAPIError(ecRequestElementMissing, aKey);
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Value = %d', [result]);
    end;
  end;

function TcAPIScriptInterface.JsonTryGetBool(aJson: TJsonObject; aKey: string; out aValue: boolean): boolean;
  begin
  Debug('script', '- JsonTryGetBool(Obj:%s, Key:''%s'', Value:?)', [BooleanToYesNo(Assigned(aJson)), aKey]);
  DebugIncIndent('script');
  try
    result := aJson.TryGetValue(aKey, aValue);
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Value = %s', [BooleanToTrueFalse(aValue)]);
    end;
  end;

function TcAPIScriptInterface.JsonGetBool(aJson: TJsonObject; aKey: string): boolean;
  begin
  Debug('script', '- JsonGetBool(Obj:%s, Key:''%s'', Value:?)', [BooleanToYesNo(Assigned(aJson)), aKey]);
  DebugIncIndent('script');
  try
    if not JsonTryGetBool(aJson, aKey, result)
       then iAPIError(ecRequestElementMissing, aKey);
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Value = %s', [BooleanToTrueFalse(result)]);
    end;
  end;

function TcAPIScriptInterface.JsonTryGetObj(aJson: TJsonObject; aKey: string; out aValue: TJsonObject): boolean;
  begin
  Debug('script', '- JsonGetObj(Obj:%s, Key:''%s'', Value:?)', [BooleanToYesNo(Assigned(aJson)), aKey]);
  DebugIncIndent('script');
  try
    result := aJson.TryGetValue(aKey, aValue);
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Assigned = %s', [BooleanToYesNo(Assigned(aValue))]);
    end;
  end;

function TcAPIScriptInterface.JsonGetObj(aJson: TJsonObject; aKey: string): TJsonObject;
  begin
  Debug('script', '- JsonGetObj(Obj:%s, Key:''%s'', Value:?)', [BooleanToYesNo(Assigned(aJson)), aKey]);
  DebugIncIndent('script');
  try
    if not JsonTryGetObj(aJson, aKey, result)
       then iAPIError(ecRequestElementMissing, aKey);
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Assigned = %s', [BooleanToYesNo(Assigned(result))]);
    end;
  end;

function TcAPIScriptInterface.JsonTryGetArray(aJson: TJsonObject; aKey: string; out aValue: TJsonArray): boolean;
  begin
  Debug('script', '- JsonTryGetObj(Obj:%s, Key:''%s'', Value:?)', [BooleanToYesNo(Assigned(aJson)), aKey]);
  DebugIncIndent('script');
  try
    result := aJson.TryGetValue(aKey, aValue);
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Assigned = %s', [BooleanToYesNo(Assigned(aValue))]);
    end;
  end;

function TcAPIScriptInterface.JsonGetArray(aJson: TJsonObject; aKey: string): TJsonArray;
  begin
  Debug('script', '- JsonGetObj(Obj:%s, Key:''%s'', Value:?)', [BooleanToYesNo(Assigned(aJson)), aKey]);
  DebugIncIndent('script');
  try
    if not JsonTryGetArray(aJson, aKey, result)
       then iAPIError(ecRequestElementMissing, aKey);
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Assigned = %s', [BooleanToYesNo(Assigned(result))]);
    end;
  end;

procedure TcAPIScriptInterface.SetRegisteredAt(aTime: integer);
  begin
  Debug('script', '- SetRegisteredAt(%d)', [aTime]);
  fRegisteredAt := aTime;
  end;

procedure TcAPIScriptInterface.SetExpiredAt(aTime: integer);
  begin
  Debug('script', '- SetExpiredAt(%d)', [aTime]);
  fExpiredAt := aTime;
  end;

procedure TcAPIScriptInterface.SetValidFrom(aTime: integer);
  begin
  Debug('script', '- SetValidFrom(%d)', [aTime]);
  fValidFrom := aTime;
  end;

procedure TcAPIScriptInterface.SetValidUntil(aTime: integer);
  begin
  Debug('script', '- SetValidUntil(%d)', [aTime]);
  fValidUntil := aTime;
  end;

procedure TcAPIScriptInterface.ResetTemporal;
  begin
  Debug('script', '- ResetTemporal');
  iResetTemporal;
  end;

function TcAPIScriptInterface.GetRegisteredAtForQuery: TcTime;
  begin
  if fRegisteredAt <> cNoTime
     then result := fRegisteredAt
     else result := Engine.SystemTime;
  end;

function TcAPIScriptInterface.GetValidFromForQuery: TcTime;
  begin
  if fValidFrom <> cNoTime
     then result := fValidFrom
     else result := Engine.SystemTime;
  end;

function TcAPIScriptInterface.GetValidFromForAdd: TcTime;
  begin
  result := fValidFrom;
  end;

function TcAPIScriptInterface.GetValidUntilForAdd: TcTime;
  begin
  result := fValidUntil;
  end;

function TcAPIScriptInterface.FindClaim(aClaimTypeName: string; aIdentityStr: string; var aClaim: TcClaim): boolean;
  var
    c, NrFound: integer;
    lClaimType: TcClaimType;
    lClaimSet: TcClaimSet;
    lQuery: TcTemporalQuery;
    lClaim: TcClaim;
  begin
  Debug('asi', 'asi: TcAPIScriptInterface.FindClaim');
  Debug('asi', 'asi:   Engine assigned?: %s', [BooleanToYesNo(Assigned(Engine))]);

  Debug('script', '- FindClaim(ClaimType:%s, Identity:%s)', [aClaimTypeName, aIdentityStr]);
  DebugIncIndent('script');
  result := false;
  try
    lClaimType := Engine.ConceptualTypes.FindClaimType(aClaimTypeName);
    if not Assigned(lClaimType)
       then iAPIError(ecClaimTypeNotFound, aClaimTypeName);

    lClaimSet := lClaimType.ClaimSetList.Find(aIdentityStr);
    if not Assigned(lClaimSet)
       then exit;

    aClaim := nil;
    lQuery := TcTemporalQuery.Create(Session);
    try
      lQuery.AssignClaimSet(lClaimSet);
      lQuery.SetDefaultFilter(GetValidFromForQuery, GetRegisteredAtForQuery);
      lQuery.Execute;
      NrFound := 0;
      for c := 0 to lClaimSet.DerivedClaims.Count-1 do
        begin
        lClaim := lClaimSet.DerivedClaims[c];
        if lQuery.InResult(lClaim)
           then begin
                aClaim := lClaim;
                Inc(NrFound);
                end;
        if NrFound>1 then break;
        end;
      if NrFound>1
         then begin
              iAPIError(ecDataInconsistency, aClaimTypeName, '%s: Duplicate value during time travel');
              exit;
              end;
    finally
      lQuery.Free;
      end;

    result := Assigned(aClaim);

    if Assigned(aClaim) and (aClaim.InDoubt)
       then fClaimsInDoubt.Add(aClaim.IDStr, aClaim);

  finally
    DebugDecIndent('script');
    Debug('script', '  -> Found: %s', [BooleanToYesNo(result)]);
    end;
  end;

function TcAPIScriptInterface.AddClaim(aClaimTypeName: string; aTuple: array of const): TcClaim;
  var
    lTupleStr: string;
    lClaimType: TcClaimType;
  begin
  Debug('script', '- AddClaim(ClaimType:%s)', [aClaimTypeName]);
  DebugIncIndent('script');
  try
    lClaimType := Engine.ConceptualTypes.FindClaimType(aClaimTypeName);
    if not Assigned(lClaimType)
       then iAPIError(ecClaimTypeNotFound, aClaimTypeName);

    lTupleStr := TupleArgsToStr(aTuple);
    Debug('script', '  - Tuple: %s', [lTupleStr]);
    result := lClaimType.Add(Session, lTupleStr, GetValidFromForAdd, GetValidUntilForAdd);
  finally
    DebugDecIndent('script');
    end;
  end;

function TcAPIScriptInterface.Annotate(aClaim: TcClaim; aAnnotationTypeName: string; aTuple: array of const): TcAnnotation;
  var
    lTupleStr: string;
    lAnnotationType: TcAnnotationType;
  begin
  Debug('script', '- Annotate(Claim:%s, Type:%s)', [BooleanToYesNo(Assigned(aClaim)), aAnnotationTypeName]);
  DebugIncIndent('script');
  try
    lAnnotationType := Engine.ConceptualTypes.FindAnnotationType(aAnnotationTypeName);
    if not Assigned(lAnnotationType)
       then iAPIError(ecAnnotationTypeNotFound, aAnnotationTypeName);

    lTupleStr := TupleArgsToStr(aTuple);
    if lTupleStr<>''
       then lTupleStr := aClaim.SourceClaimRefIDStr+','+lTupleStr
       else lTupleStr := aClaim.SourceClaimRefIDStr;
    Debug('script', '  - Tuple: %s', [lTupleStr]);

    result := lAnnotationType.ClaimSetList.AddAnnotation(Session, lTupleStr, GetValidFromForAdd, GetValidUntilForAdd);

  finally
    DebugDecIndent('script');
    end;
  end;

function TcAPIScriptInterface.FindObject(aObjectTypeName: string; aIdentityStr: string; var aObject: TcClaim): boolean;
  begin
  Debug('script', '- FindObject -> FindClaim');
  DebugIncIndent('script');
  try
    result := FindClaim(aObjectTypeName, aIdentityStr, aObject);
  finally
    DebugDecIndent('script');
    end;
  end;

function TcAPIScriptInterface.AddObject(aObjectTypeName: string; aTuple: array of const): TcClaim;
  begin
  Debug('script', '- AddObject -> AddClaim');
  DebugIncIndent('script');
  try
    result := AddClaim(aObjectTypeName, aTuple);
  finally
    DebugDecIndent('script');
    end;
  end;

function TcAPIScriptInterface.AddPropertyStr(aObject: TcClaim; aPropertyName: string; aValue: string): TcClaim;
  var
    lClaimTypeName: string;
    lClaimType: TcClaimType;
  begin
  Debug('script', '- AddPropertyStr(Obj:%s, Property:''%s'', Value:''%s'')', [BooleanToYesNo(Assigned(aObject)), aPropertyName, aValue]);
  DebugIncIndent('script');
  try
    // Try to find ClaimType
    lClaimTypeName := aObject.ClaimType.Name + '\' + aPropertyName;
    lClaimType := Engine.ConceptualTypes.FindClaimType(lClaimTypeName);
    if not Assigned(lClaimType)
       then begin
            // Try '/'
            lClaimTypeName := aObject.ClaimType.Name + '/' + aPropertyName;
            lClaimType := Engine.ConceptualTypes.FindClaimType(lClaimTypeName);
            end;
    if not Assigned(lClaimType)
       then iAPIError(ecPropertyNotFound, Format('%s.%s', [aObject.ClaimType.Name, aPropertyName]));

    // Add Claim
    result := AddClaim(lClaimTypeName, [aObject.SourceClaimRefIDStr, aValue]);

  finally
    DebugDecIndent('script');
    end;
  end;

function TcAPIScriptInterface.AddPropertyInt(aObject: TcClaim; aPropertyName: string; aValue: integer): TcClaim;
  begin
  Debug('script', '- AddPropertyInt(Obj:%s, Property:''%s'', Value:%d)', [BooleanToYesNo(Assigned(aObject)), aPropertyName, aValue]);
  DebugIncIndent('script');
  try
    result := AddPropertyStr(aObject, aPropertyName, IntToStr(aValue));
  finally
    DebugDecIndent('script');
    end;
  end;

function TcAPIScriptInterface.AddPropertyBool(aObject: TcClaim; aPropertyName: string; aValue: boolean): TcClaim;
  begin
  Debug('script', '- AddPropertyBool(Obj:%s, Property:''%s'', Value:%s)', [BooleanToYesNo(Assigned(aObject)), aPropertyName, BooleanToTrueFalse(aValue)]);
  DebugIncIndent('script');
  try
    result := AddPropertyStr(aObject, aPropertyName, BooleanToTrueFalse(aValue));
  finally
    DebugDecIndent('script');
    end;
  end;

function TcAPIScriptInterface.GetProperty(aObject: TcClaim; aPropertyName: string): TcClaim;
  var
    lClaimTypeName: string;
    lClaimType: TcClaimType;
    lClaimSet: TcClaimSet;
    lQuery: TcTemporalQuery;
    c, NrFound: integer;
    lClaim: TcClaim;
  begin
  Debug('script', '- GetProperty(Obj:%s, Property:''%s'')', [BooleanToYesNo(Assigned(aObject)), aPropertyName]);
  DebugIncIndent('script');
  result := nil;
  try
    if not Assigned(aObject)
       then iAPIError(ecAssertionFailed, Format('GetProperty(%s)', [aPropertyName]), '%s: Object nil');

    // Try to find ClaimType
    lClaimTypeName := aObject.ClaimType.Name + '\' + aPropertyName;
    lClaimType := Engine.ConceptualTypes.FindClaimType(lClaimTypeName);
    if not Assigned(lClaimType)
       then begin
            // Try '/'
            lClaimTypeName := aObject.ClaimType.Name + '/' + aPropertyName;
            lClaimType := Engine.ConceptualTypes.FindClaimType(lClaimTypeName);
            end;
    if not Assigned(lClaimType)
       then iAPIError(ecPropertyNotFound, Format('%s.%s', [aObject.ClaimType.Name, aPropertyName]));

    // Find ClaimSet
    lClaimSet := lClaimType.ClaimSetList.Find(aObject.SourceClaimRefIDStr);
    if not Assigned(lClaimSet) then exit;

    // Find Claim
    result := nil;
    lQuery := TcTemporalQuery.Create(Session);
    try
      lQuery.AssignClaimSet(lClaimSet);
      lQuery.SetDefaultFilter(GetValidFromForQuery, GetRegisteredAtForQuery);
      lQuery.Execute;
      NrFound := 0;
      for c := 0 to lClaimSet.DerivedClaims.Count-1 do
        begin
        lClaim := lClaimSet.DerivedClaims[c];
        if lQuery.InResult(lClaim)
           then begin
                result := lClaim;
                Inc(NrFound);
                end;
        if NrFound>1 then break;
        end;
      if NrFound>1
         then begin
              // More than one 'current' claim = Integrity violation -> Internal error?
              iAPIError(ecDataInconsistency, lClaimTypeName, '%s: Duplicate value during time travel');
              exit;
              end;
    finally
      lQuery.Free;
      end;

    if Assigned(result) and (result.InDoubt)
       then fClaimsInDoubt.Add(result.IDStr, result);

  finally
    DebugDecIndent('script');
    Debug('script', '  -> Found: %s', [BooleanToYesNo(Assigned(result))]);
    end;
  end;

function TcAPIScriptInterface.GetPropertyStr(aObject: TcClaim; aPropertyName: string): string;
  var
    lClaim: TcClaim;
  begin
  Debug('script', '- GetPropertyStr(Obj:%s, Property:''%s'')', [BooleanToYesNo(Assigned(aObject)), aPropertyName]);
  DebugIncIndent('script');
  try
    lClaim := GetProperty(aObject, aPropertyName);

    // GetValueStrWithoutIdentity returns every value except the identity.
    // The identity is the role referencing the object, 'all other roles' is
    // exactly one role: the role with the property
    if Assigned(lClaim)
       then result := lClaim.GetValueStrWithoutIdentity
       else result := '';

  finally
    DebugDecIndent('script');
    Debug('script', '  -> Value = %s', [result]);
    end;
  end;

function TcAPIScriptInterface.GetPropertyInt(aObject: TcClaim; aPropertyName: string): integer;
  var
    lStr: string;
  begin
  Debug('script', '- GetPropertyInt(Obj:%s, Property:''%s'')', [BooleanToYesNo(Assigned(aObject)), aPropertyName]);
  DebugIncIndent('script');
  result := 0;
  try
    lStr := GetPropertyStr(aObject, aPropertyName);
    try
      result := StrToInt(lStr);
    except
      iAPIError(ecRequestInvalidValue, aPropertyName);
      end;
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Value = %d', [result]);
    end;
  end;

function TcAPIScriptInterface.GetPropertyBool(aObject: TcClaim; aPropertyName: string): boolean;
  var
    lStr: string;
  begin
  Debug('script', '- GetPropertyBool(Obj:%s, Property:''%s'')', [BooleanToYesNo(Assigned(aObject)), aPropertyName]);
  DebugIncIndent('script');
  result := false;
  try
    lStr := GetPropertyStr(aObject, aPropertyName);
    try
      result := TrueFalseToBoolean(lStr);
    except
      iAPIError(ecRequestInvalidValue, aPropertyName);
      end;
  finally
    DebugDecIndent('script');
    Debug('script', '  -> Value = %s', [BooleanToTrueFalse(result)]);
    end;
  end;

procedure TcAPIScriptInterface.BroadCastCloudEvent(aTypeStr: string; aObjectName, aObjectKey: string);
  begin
  Assert(Assigned(Operation));
  ace.Core.Notification.BroadCastCloudEvent('nl.ace.'+aTypeStr, Operation.RegisteredAt, aObjectName, aObjectKey);
  end;


{ TcRequest }

constructor TcRequest.Create(aRequestInfo: TIdHTTPRequestInfo);
  var
    lStringStream: TStringStream;
    lJsonValue: TJsonValue;
  begin
  inherited Create;

  // Command, Resource, Params
  fCommandType := aRequestInfo.CommandType; // No option for Patch!
  fCommandTypeStr := aRequestInfo.Command; // Does contain Patch
  fResource := aRequestInfo.Document;
  fParams := TStringlist.Create;
  fParams.AddStrings(aRequestInfo.Params);

  // Body
  fContentType := aRequestInfo.ContentType;
  if Assigned(aRequestInfo.PostStream) and
     IsTextualContentType(aRequestInfo.ContentType)
    then begin
         lStringStream := TStringStream.Create;
         lStringStream.LoadFromStream(aRequestInfo.PostStream);
         fContent := lStringStream.DataString;
         lStringStream.Free;

         // Json
         if IsJsonContentType(aRequestInfo.ContentType)
            then begin
                 try
                   lJsonValue := TJsonObject.ParseJSONValue(fContent);
                   if Assigned(lJsonValue) and (lJsonValue is TJsonObject)
                      then fJson := lJsonValue as TJsonObject;
                 except
                   on E:Exception do
                     // Catch exception and pass to request as error message.
                     fContentParseError := E.Message;
                   end;
                 end;
         end
    else begin
         // Other content
         fContent := '';
         fJson := nil;
         fContentParseError := '';
         end;
  end;

destructor TcRequest.Destroy;
  begin
  FreeAndNil(fParams);
  inherited;
  end;

function TcRequest.GetCommandTypeStr: string;
  begin
  result := HttpCommandTypeToStr(fCommandType, fCommandTypeStr);
  end;

function TcRequest.GetParamStr: string;
  begin
  result := StringListToString(fParams, '&');
  end;


{ TcResponse }

constructor TcResponse.Create(aResponseInfo: TIdHTTPResponseInfo);
  begin
  inherited Create;

  fCode := aResponseInfo.ResponseNo;
  fContent := aResponseInfo.ContentText;
  fContentType := '';
  end;

function TcResponse.GetCodeStr: string;
  begin
  result := HttpResponseCodeToStr(fCode);
  end;


{ TcAPIHandler }

constructor TcAPIHandler.Create(aIdentifier, aName: string; aPort: word);
  begin
  inherited Create;
  fIdentifier := aIdentifier;
  fName := aName;
  fIPAddress := cIPAddress;
  fPort := aPort;
  fHTTPServer := TIdHTTPServer.Create(nil);
  fHTTPServer.Active := false;
  fAsi := nil;
  end;

destructor TcAPIHandler.Destroy;
  begin
  Deactivate;
  FreeAndNil(fHTTPServer);
  inherited;
  end;

function TcAPIHandler.GetIsActive: boolean;
  begin
  result := HTTPServer.Active;
  end;

procedure TcAPIHandler.Activate;
  begin
  if isActive then exit;
  HttpServer.Active := false;
  HttpServer.Bindings.Clear;
  with HttpServer.Bindings.Add do
    begin
    ip := fIPAddress;
    port := fPort;
    end;

  fAsi := TcAPIScriptInterface.Create;

  HttpServer.OnCommandGet := HandleHttpServerCommand;
  HttpServer.OnCommandOther := HandleHttpServerOtherCommand;
  HttpServer.Active := True;

  Debug('httpserver', 'API ''%s'' started on http://%s:%d.', [Name, cIPAddress, Port]);
  Debug('httpserver', '');

  fAPIServer.Invalidate;
  end;

procedure TcAPIHandler.Deactivate;
  begin
  if not isActive then exit;
  HttpServer.Active := false;
  HttpServer.Bindings.Clear;
  HttpServer.OnCommandGet := nil;
  HttpServer.OnCommandOther := nil;
  FreeAndNil(fAsi);
  Debug('httpserver', 'API %s stopped.', [Name]);
  Debug('httpserver', '');
  end;

procedure TcAPIHandler.HandleHttpServerCommand(aContext: TIdContext; aRequestInfo: TIdHTTPRequestInfo; aResponseInfo: TIdHTTPResponseInfo);
  begin
  fAsi.Init(aRequestInfo, aResponseInfo);

  // Handle request (internal)
  try
  
    try
      iHandleRequest;
    except
      on E:Exception do
        begin
        if not (E is EAPIError) 
           then raise;
        end;
      end;
      
  finally
    // Pass response
    aResponseInfo.ResponseNo := asi.Response.Code;
    if asi.Response.ContentType <> ''
       then aResponseInfo.ContentType := asi.Response.ContentType;
    aResponseInfo.ContentText := asi.Response.Content;

    AddCORSHeadersToResponse(aResponseInfo);
    end;
  end;

procedure TcAPIHandler.HandleHttpServerOtherCommand(aContext: TIdContext; aRequestInfo: TIdHTTPRequestInfo; aResponseInfo: TIdHTTPResponseInfo);
  begin
  AddCORSHeadersToResponse(aResponseInfo);
  end;


procedure TcAPIHandler.AddCorsHeadersToResponse(aResponseInfo: TIdHTTPResponseInfo);
  begin
  aResponseInfo.CustomHeaders.AddValue('Access-Control-Allow-Origin', '*');
  aResponseInfo.CustomHeaders.AddValue('Access-Control-Allow-Headers', '*');
  end;

procedure TcAPIHandler.iHandleRequest;
  begin
  with asi do
    begin
    // Ignore standaard html requests
    if Request.Resource='/favicon.ico' then exit;

    // Log request
    Debug('httpServer', '%s %s %s', [
      Request.CommandTypeStr, Request.Resource, Request.ParamStr
    ]);

    // Actual handling of the request
    DebugIncIndent('httpServer');
    try
      if Assigned(Request.Json)
         then begin
              Debug('httpServer', 'body');
              DebugIncIndent('httpServer');
              Debug('httpServer', Request.Json.Format(2));
              DebugDecIndent('httpServer');
              end;

      Debug('asi', 'asi: TcAPIHandler.iHandleRequest');
      Debug('asi', 'asi:   Engine assigned?: %s', [BooleanToYesNo(Assigned(asi.Engine))]);
      HandleRequest;

    finally
      // Log response
      Debug('httpServer', 'response: %d (%s)', [Response.Code, Response.CodeStr]);
      if Response.Content <> ''
         then begin
              DebugIncIndent('httpServer');
              Debug('httpServer', Response.Content);
              DebugDecIndent('httpServer');
              end;
      Debug('httpServer', '');
      DebugDecIndent('httpServer');
      end;
    end;
  end;

initialization
  gAPIHandlers := TcAPIHandlers.Create;

  gResponseCodes := TStringlist.Create;
  gResponseCodes.Sorted := true;
  gResponseCodes.Duplicates := dupError;
  with gResponseCodes do
    begin
    AddPair('200', 'OK');
    AddPair('201', 'Created');
    AddPair('202', 'Accepted');
    AddPair('203', 'Non-authoritative Information');
    AddPair('204', 'No Content');
    AddPair('205', 'Reset Content -');
    AddPair('206', 'Partial Content');
    AddPair('301', 'Moved Permanently');
    AddPair('302', 'Moved Temporarily');
    AddPair('303', 'See Other');
    AddPair('304', 'Not Modified');
    AddPair('305', 'Use Proxy');
    AddPair('400', 'Bad Request');
    AddPair('401', 'Unauthorized');
    AddPair('403', 'Forbidden');
    AddPair('404', 'Not Found');
    AddPair('405', 'Method not allowed');
    AddPair('406', 'Not Acceptable');
    AddPair('407', 'Proxy Authentication Required');
    AddPair('408', 'Request Timeout');
    AddPair('409', 'Conflict');
    AddPair('410', 'Gone');
    AddPair('411', 'Length Required');
    AddPair('412', 'Precondition Failed');
    AddPair('413', 'Request Entity To Long');
    AddPair('414', 'Request-URI Too Long. 256 Chars max');
    AddPair('415', 'Unsupported Media Type');
    AddPair('500', 'Internal Server Error');
    AddPair('501', 'Not Implemented');
    AddPair('502', 'Bad Gateway');
    AddPair('503', 'Service Unavailable');
    AddPair('504', 'Gateway timeout');
    AddPair('505', 'HTTP version not supported');
    end;

  gTextualContentTypes := TStringlist.Create;
  gTextualContentTypes.Sorted := true;
  gTextualContentTypes.Duplicates := dupError;
  gTextualContentTypes.CaseSensitive := false;
  with gTextualContentTypes do
    begin
    Add('application/json');
    Add('application/hal+json');
    Add('text/csv');
    Add('text/html');
    Add('text/plain');
    end;

finalization
  FreeAndNil(gAPIHandlers);
  FreeAndNil(gResponseCodes);
  FreeAndNil(gTextualContentTypes);
end.
