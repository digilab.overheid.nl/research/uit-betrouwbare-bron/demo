// -----------------------------------------------------------------------------
//
// ace.API.SimpleQuery
//
// -----------------------------------------------------------------------------
//
// Implements a simple API to query a class.
//
// -----------------------------------------------------------------------------
//
// License: EUPL
// Copyright: VNG Realisatie
//
// -----------------------------------------------------------------------------

unit ace.API.SimpleQuery;

interface

uses
  Classes,
  IdComponent, IdCustomHTTPServer,
  ace.Core.Engine, ace.API.Server;

const
  cSimpleQuery_Identifier = 'SimpleQuery';
  cSimpleQuery_Name = 'Simple Query';
  cSimpleQuery_Port = 4201;

  cParamKnowledgeOf = 'KnowledgeOf';
  cParamValidOn = 'ValidOn';

type
  TcAPISimpleQuery =
    class (TcAPIHandler)
      protected
        procedure HandleRequest; override;

        function HandleGET_TechnicalID(aClass: TcClass; aID: integer): string;
        function HandleGET_Query(aClass: TcClass; aParams: TStringlist): string;
      end;

implementation

uses
  SysUtils, common.Utils, ace.Core.Query;

procedure TcAPISimpleQuery.HandleRequest;
  var
    lStr, lName, lIDStr: string;
    lClass: TcClass;
    lID: integer;
  begin
  with asi do
    begin
    // Try to find resource
    lStr := Copy(Request.Resource, 2, Length(Request.Resource));
    SplitString(lStr, '/', lName, lIDStr);
    lClass := Engine.FindClassByPluralName(lName);
    if not Assigned(lClass)
       then begin
            Response.Content := Format('Unknown resource: %s.', [lName]);
            exit;
            end;

    // Command should be GET
    if Request.CommandType <> hcGET
       then begin
            Response.Content := Format('Command %s unsupported.', [Request.CommandTypeStr]);
            exit;
            end;

    // Parameters present?
    if (lIDStr='') and (Request.Params.Count=0)
       then begin
            Response.Content := 'Parameters missing.';
            exit;
            end;

    if (lIDStr<>'') and (Request.Params.Count>0)
       then begin
            Response.Content := 'Invalid request.';
            exit;
            end;

    if lIDStr<>''
       then begin
            // Valid positive integer?
            lID := StrToIntDef(lIDStr, -1);
            if lID<0
               then begin
                    Response.Content := 'Technical ID must be a valid positive integer.';
                    exit;
                    end;

            Response.Content := HandleGET_TechnicalID(lClass, lID);
            end
       else Response.Content := HandleGet_Query(lClass, Request.Params);
    end;
  end;

const
  cNoClaimsFound = 'No claims found.';
  cMultipleClaimsFound = 'Multiple claims found.';

function TcAPISimpleQuery.HandleGET_TechnicalID(aClass: TcClass; aID: integer): string;
  var
    lResponse: TStringlist;
    cs, r, gct: integer;
    lClaimType: TcClaimType;
    lClaimSet: TcClaimSet;
    lClaim: TcClaim;
    lQuery: TcTemporalQuery;
    lMainClaimID: integer;
  begin
  with asi do
    begin
    // Generate response
    lResponse := TStringlist.Create;
    try
      lResponse.Add('{');

      // Main ClaimType
      lClaimType := aClass.MainClaimType;
      lClaimSet := nil;
      for cs := 0 to lClaimType.ClaimSetList.Count-1 do
        begin
        if lClaimType.ClaimSetList[cs].ID = aID
           then begin
                lClaimSet := lClaimType.ClaimSetList[cs];
                break;
                end;
        end;
      if not Assigned(lClaimSet)
         then begin
              result := cNoClaimsFound;
              exit;
              end;

      lQuery := TcTemporalQuery.Create(Session);
      lQuery.AssignClaimSet(lClaimSet);
      with lQuery do
        begin
        SetDefaultFilter;
        Execute;
        if ItemsInResult.Count=0
           then begin
                result := cNoClaimsFound;
                exit;
                end;
        if ItemsInResult.Count>1
           then begin
                result := cMultipleClaimsFound;
                exit;
                end;
        lClaim := ItemsInResult.Objects[0];
        for r := 0 to lClaimType.Roles.Count-1 do
          lResponse.Add(Format('  "%s":"%s"', [
            toIdentifier(lClaimType.Roles[r].Name),
            lClaim.RoleValues.Values[lClaimType.Roles[r].Name]
          ]));
        end;
      lQuery.Free;

      lMainClaimID := lClaim.ID;

      // Grouped ClaimTypes
      for gct := 0 to aClass.GroupedClaimTypes.Count-1 do
        begin
        lClaimType := aClass.GroupedClaimTypes.Objects[gct];
        lClaim := nil;
        lClaimSet := lClaimType.ClaimSetList.Find(cRefChar+IntToStr(lMainClaimID));
        if Assigned(lClaimSet)
           then begin
                lQuery := TcTemporalQuery.Create(Session);
                lQuery.AssignClaimSet(lClaimSet);
                with lQuery do
                  begin
                  SetDefaultFilter;
                  Execute;
                  if ItemsInResult.Count>1
                     then begin
                          result := cMultipleClaimsFound;
                          exit;
                          end;
                  if ItemsInResult.Count=1
                     then lClaim := ItemsInResult.Objects[0];
                  end;
                lQuery.Free;
                end;
        if Assigned(lClaimSet) and Assigned(lClaim)
           then begin
                for r := 0 to lClaimType.Roles.Count-1 do
                  begin
                  if lClaimType.Roles[r].Type_ = aClass.MainClaimType then continue;
                  lResponse.Add(Format('  "%s":"%s"', [
                    toIdentifier(lClaimType.Roles[r].Name),
                    lClaim.RoleValues.Values[lClaimType.Roles[r].Name]
                  ]));
                  end;
                end
           else begin
                // Empty value
                for r := 0 to lClaimType.Roles.Count-1 do
                  begin
                  if lClaimType.Roles[r].Type_ = aClass.MainClaimType then continue;
                  lResponse.Add(Format('  "%s":""', [toIdentifier(lClaimType.Roles[r].Name)]));
                  end;
                end;
        end;

      lResponse.Add('}');
      result := StringlistToString(lResponse, #13#10);

    finally
      lResponse.Free;
      end;
    end;
  end;

function TcAPISimpleQuery.HandleGET_Query(aClass: TcClass; aParams: TStringlist): string;
  var
    lResponse: TStringlist;
    lIDs: TStringlist;
    lIDStr: string;
    lIndex: integer;

    lKnowledgeOf, lValidOn: integer;

    r, gct: integer;
    lClaimType: TcClaimType;
    lClaimSet: TcClaimSet;
    lClaim: TcClaim;
    lRole: TcRole;
    lQuery: TcTemporalQuery;
    lMainClaimID: integer;
  begin
  with asi do
    begin
    // Generate response
    lResponse := TStringlist.Create;
    try
      lResponse.Add('{');

      // Main ClaimType
      lClaimType := aClass.MainClaimType;

      // Build Identity str
      lIDs := TStringlist.Create;
      try
        for r := 0 to lClaimType.Identity.Count-1 do
          begin
          lRole := lClaimType.Identity[r];
          lIndex := aParams.IndexOfName(lRole.Name);
          if lIndex < 0
             then begin
                  result := 'Invalid query parameters.';
                  exit;
                  end
             else lIDs.Add(aParams.ValueFromIndex[lIndex]);
          end;
        lIDStr := StringlistToString(lIDs, cIdentityValueSeparator);
      finally
        lIDs.Free;
        end;

      // Scan for time params
      lIndex := aParams.IndexOfName(cParamValidOn);
      if lIndex >= 0
         then begin
              lValidOn := StrToIntDef(aParams.ValueFromIndex[lIndex], -1);
              if lValidOn < 0
                 then begin
                      result := 'Parameter ValidOn contains an invalid time.';
                      exit;
                      end;
              end
         else lValidOn := Engine.SystemTime;

      lIndex := aParams.IndexOfName(cParamKnowledgeOf);
      if lIndex >= 0
         then begin
              lKnowledgeOf := StrToIntDef(aParams.ValueFromIndex[lIndex], -1);
              if lKnowledgeOf < 0
                 then begin
                      result := 'Parameter KnowledgeOf contains an invalid time.';
                      exit;
                      end;
              end
         else lKnowledgeOf := Engine.SystemTime;

      // Find ClaimSet
      lClaimSet := lClaimType.ClaimSetList.Find(lIDStr);
      if not Assigned(lClaimSet)
         then begin
              result := cNoClaimsFound;
              exit;
              end;

      lQuery := TcTemporalQuery.Create(Session);
      lQuery.AssignClaimSet(lClaimSet);
      with lQuery do
        begin
        SetDefaultFilter(lValidOn, lKnowledgeOf);
        Execute;
        if ItemsInResult.Count=0
           then begin
                result := cNoClaimsFound;
                exit;
                end;
        if ItemsInResult.Count>1
           then begin
                result := cMultipleClaimsFound;
                exit;
                end;
        lClaim := ItemsInResult.Objects[0];
        for r := 0 to lClaimType.Roles.Count-1 do
          lResponse.Add(Format('  "%s":"%s"', [
            toIdentifier(lClaimType.Roles[r].Name),
            lClaim.RoleValues.Values[lClaimType.Roles[r].Name]
          ]));
        end;
      lQuery.Free;

      lMainClaimID := lClaim.SourceClaim;

      // Grouped ClaimTypes
      for gct := 0 to aClass.GroupedClaimTypes.Count-1 do
        begin
        lClaimType := aClass.GroupedClaimTypes.Objects[gct];
        lClaim := nil;
        lClaimSet := lClaimType.ClaimSetList.Find(cRefChar+IntToStr(lMainClaimID));
        if Assigned(lClaimSet)
           then begin
                lQuery := TcTemporalQuery.Create(Session);
                lQuery.AssignClaimSet(lClaimSet);
                with lQuery do
                  begin
                  SetDefaultFilter(lValidOn, lKnowledgeOf);
                  Execute;
                  if ItemsInResult.Count>1
                     then begin
                          result := cMultipleClaimsFound;
                          exit;
                          end;
                  if ItemsInResult.Count=1
                     then lClaim := ItemsInResult.Objects[0];
                  end;
                lQuery.Free;
                end;
        if Assigned(lClaimSet) and Assigned(lClaim)
           then begin
                for r := 0 to lClaimType.Roles.Count-1 do
                  begin
                  if lClaimType.Roles[r].Type_ = aClass.MainClaimType then continue;
                  lResponse.Add(Format('  "%s":"%s"', [
                    toIdentifier(lClaimType.Roles[r].Name),
                    lClaim.RoleValues.Values[lClaimType.Roles[r].Name]
                  ]));
                  end;
                end
           else begin
                // Empty value
                for r := 0 to lClaimType.Roles.Count-1 do
                  begin
                  if lClaimType.Roles[r].Type_ = aClass.MainClaimType then continue;
                  lResponse.Add(Format('  "%s":""', [toIdentifier(lClaimType.Roles[r].Name)]));
                  end;
                end;
        end;

      lResponse.Add('}');
      result := StringlistToString(lResponse, #13#10);

    finally
      lResponse.Free;
      end;
    end;
  end;

initialization
  gAPIHandlers.Add(TcAPISimpleQuery.Create(cSimpleQuery_Identifier, cSimpleQuery_Name, cSimpleQuery_Port));
end.
