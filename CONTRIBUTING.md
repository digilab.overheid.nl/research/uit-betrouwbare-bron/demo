# Contributing

This is not an actively maintained repository, it is merely a demo.

People who want to contribute should check https://www.uitbetrouwbarebron.nl (Dutch) for more information about active developments.

## Building the code

- Go to the website of [Lazarus](https://www.lazarus-ide.org/) the open source IDE for Open Pascal.
- Follow the instructions to install Lazarus on your system.
- Clone this repository.
- Open the project and compile.

