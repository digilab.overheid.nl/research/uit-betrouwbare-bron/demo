set AutoIncrementSystemTime: no

set SystemTime: 2

define labelType: id
  scalar: string

define claimType: Persoon
  expressionTemplate: Er bestaat een persoon <id>
  nestedExpressionTemplate: persoon <id>

register Persoon: p1
register Persoon: p2
register Persoon: p3

define labelType: voornaam
  scalar: string

define claimType: Voornaam-NoTime-v1
  expressionTemplate: <Persoon> heet <voornaam>
  recordTransactionTime: false
  recordValidTime: false
  keepExpired: false
  keepEnded: false

define claimType: Voornaam-NoTime-v2
  expressionTemplate: <Persoon> heet <voornaam>
  recordTransactionTime: false
  keepExpired: false
  keepEnded: false

define claimType: Voornaam-NoTime-v3
  expressionTemplate: <Persoon> heet <voornaam>
  recordValidTime: false
  keepExpired: false
  keepEnded: false

define claimType: Voornaam-NoTime-v4
  expressionTemplate: <Persoon> heet <voornaam>
  keepExpired: false
  keepEnded: false

define claimType: Voornaam-TrTime-v1
  expressionTemplate: <Persoon> heet <voornaam>
  keepEnded: false

define claimType: Voornaam-TrTime-v2
  expressionTemplate: <Persoon> heet <voornaam>
  recordValidTime: false
  keepEnded: false

define claimType: Voornaam-VlTime-v1
  expressionTemplate: <Persoon> heet <voornaam>
  keepExpired: false

define claimType: Voornaam-VlTime-v2
  expressionTemplate: <Persoon> heet <voornaam>
  recordTransactionTime: false
  keepExpired: false

define claimType: Voornaam-2DH
  expressionTemplate: <Persoon> heet <voornaam>

define claimType: Voornaam-ES
  expressionTemplate: <Persoon> heet <voornaam>
  timelineInterpretation: false


register Voornaam-NoTime-v1: p1, Evet
  from: 1
register Voornaam-NoTime-v2: p1, Evet
  from: 1
register Voornaam-NoTime-v3: p1, Evet
  from: 1
register Voornaam-NoTime-v4: p1, Evet
  from: 1
register Voornaam-TrTime-v1: p1, Evet
  from: 1
register Voornaam-TrTime-v2: p1, Evet
  from: 1
register Voornaam-VlTime-v1: p1, Evet
  from: 1
register Voornaam-VlTime-v2: p1, Evet
  from: 1
register Voornaam-2DH: p1, Evet
  from: 1
register Voornaam-ES: p1, Evet
  from: 1

show Voornaam-NoTime-v1
  minimized
show Voornaam-NoTime-v2
  minimized
show Voornaam-NoTime-v3
  minimized
show Voornaam-NoTime-v4
  minimized
show Voornaam-TrTime-v1
  minimized
show Voornaam-TrTime-v2
  minimized
show Voornaam-VlTime-v1
  minimized
show Voornaam-VlTime-v2
  minimized
show Voornaam-2DH
  minimized
show Voornaam-ES
  minimized

wait

set SystemTime: 5

register Voornaam-NoTime-v1: p1, Evert
  from: 1
register Voornaam-NoTime-v2: p1, Evert
  from: 1
register Voornaam-NoTime-v3: p1, Evert
  from: 1
register Voornaam-NoTime-v4: p1, Evert
  from: 1
register Voornaam-TrTime-v1: p1, Evert
  from: 1
register Voornaam-TrTime-v2: p1, Evert
  from: 1
register Voornaam-VlTime-v1: p1, Evert
  from: 1
register Voornaam-VlTime-v2: p1, Evert
  from: 1
register Voornaam-2DH: p1, Evert
  from: 1
register Voornaam-ES: p1, Evert
  from: 1

wait

set SystemTime: 6

register Voornaam-NoTime-v1: p1, Eveline
  from: 5
register Voornaam-NoTime-v2: p1, Eveline
  from: 5
register Voornaam-NoTime-v3: p1, Eveline
  from: 5
register Voornaam-NoTime-v4: p1, Eveline
  from: 5
register Voornaam-TrTime-v1: p1, Eveline
  from: 5
register Voornaam-TrTime-v2: p1, Eveline
  from: 5
register Voornaam-VlTime-v1: p1, Eveline
  from: 5
register Voornaam-VlTime-v2: p1, Eveline
  from: 5
register Voornaam-2DH: p1, Eveline
  from: 5
register Voornaam-ES: p1, Eveline
  from: 5

wait

set SystemTime: 7

register Voornaam-NoTime-v1: p1, Karel
  from: 4
  until: 6
register Voornaam-NoTime-v2: p1, Karel
  from: 4
  until: 6
register Voornaam-NoTime-v3: p1, Karel
  from: 4
  until: 6
register Voornaam-NoTime-v4: p1, Karel
  from: 4
  until: 6
register Voornaam-TrTime-v1: p1, Karel
  from: 4
  until: 6
register Voornaam-TrTime-v2: p1, Karel
  from: 4
  until: 6
register Voornaam-VlTime-v1: p1, Karel
  from: 4
  until: 6
register Voornaam-VlTime-v2: p1, Karel
  from: 4
  until: 6
register Voornaam-2DH: p1, Karel
  from: 4
  until: 6
register Voornaam-ES: p1, Karel
  from: 4
  until: 6

wait

set SystemTime: 8

register Voornaam-NoTime-v1: p1, Eve
  from: 2
  until: 3
register Voornaam-NoTime-v2: p1, Eve
  from: 2
  until: 3
register Voornaam-NoTime-v3: p1, Eve
  from: 2
  until: 3
register Voornaam-NoTime-v4: p1, Eve
  from: 2
  until: 3
register Voornaam-TrTime-v1: p1, Eve
  from: 2
  until: 3
register Voornaam-TrTime-v2: p1, Eve
  from: 2
  until: 3
register Voornaam-VlTime-v1: p1, Eve
  from: 2
  until: 3
register Voornaam-VlTime-v2: p1, Eve
  from: 2
  until: 3
register Voornaam-2DH: p1, Eve
  from: 2
  until: 3
register Voornaam-ES: p1, Eve
  from: 2
  until: 3

