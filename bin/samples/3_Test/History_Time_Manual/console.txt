set AutoIncrementSystemTime: no

define labelType: id
  scalar: integer

define claimType: Persoon
  expressionTemplate: Er bestaat een persoon <id>
  nestedExpressionTemplate: persoon <id>

define labelType: voornaam
  scalar: string

define claimType: Persoon\Voornaam
  expressionTemplate: <Persoon> heet <voornaam>

set SystemTime: 1

wait

register Persoon\Voornaam: p1, Evet
  from: 1

show Persoon\Voornaam

wait

set SystemTime: 2

register Persoon\Voornaam: p1, Evert
  from: 1

wait

set SystemTime: 3

register Persoon\Voornaam: p1, Eveline
  from: 5

wait

set SystemTime: 4

register Persoon\Voornaam: p1, Karel
  from: 4
  until: 6

wait

set SystemTime: 5

register Persoon\Voornaam: p1, Eve
  from: 2
  until: 3

