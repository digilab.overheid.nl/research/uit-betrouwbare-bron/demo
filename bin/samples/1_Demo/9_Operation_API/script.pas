// -----------------------------------------------------------------------------
// -- Generic_OnReceive
// -- 
// --   Afhandeling van algemene 'meta' data over operatie
// -- 
procedure Generic_OnReceive(aJson: TJsonObject);
  var
    jDocument: TJsonObject;
    cDocument: TcClaim;
    DocumentID, Omschrijving, Toelichting, Rechtsgrond: string;
  begin 
  // Verwerk document, indien aanwezig
  if ace.JsonTryGetObj(aJson, 'document', jDocument)
     then begin
          // Haal eigenschappen op
          DocumentID := ace.JsonGetStr(jDocument, 'ID');
          Omschrijving := ace.JsonGetStr(jDocument, 'omschrijving');
          
          // Document mag nog niet bestaan
          if ace.FindClaim('Document', DocumentID, cDocument)
             then ace.ProcessingFailed('document', 'Duplicate document.');
             
          // Voeg claims toe
          cDocument := ace.AddClaim('Document', [DocumentID]);
          ace.AddClaim('Operation\Document', ['$operation', cDocument.RefIDStr]);
          ace.AddClaim('Document\Omschrijving', [cDocument.RefIDStr, Omschrijving]);
          end;
                 
  // Verwerk toelichting, indien aanwezig
  if ace.JsonTryGetStr(aJson, 'toelichting', Toelichting)
     then ace.AddClaim('Operation\Toelichting', ['$operation', Toelichting]);
     
  // Verwerk rechtsgrond, indien aanwezig
  if ace.JsonTryGetStr(aJson, 'rechtsgrond', Rechtsgrond)
     then ace.AddClaim('Operation\Rechtsgrond', ['$operation', Rechtsgrond]);
  end;

 
// -----------------------------------------------------------------------------
// -- Registreer Geboorte - Verwerkt request
// -- 
procedure Operation_RegistreerGeboorte_OnReceive;
  var
    jRoot, jPersoon: TJsonObject;
    cPersoon: TcClaim;
    PersoonID, Geslacht, Voornaam, Voorvoegsel, Achternaam: string;
    Geboortedatum: integer;
  begin
  jRoot := Request.Json;
  Generic_OnReceive(jRoot);
  
  // Haal persoon op
  jPersoon := ace.JsonGetObj(jRoot, 'persoon');
  
  // Haal eigenschappen op
  PersoonID := ace.JsonGetStr(jPersoon, 'ID');
  Geboortedatum := ace.JsonGetInt(jPersoon, 'geboortedatum');
  Geslacht := ace.JsonGetStr(jPersoon, 'geslacht');    
  Voornaam := ace.JsonGetStr(jPersoon, 'voornaam');  
  ace.JsonTryGetStr(jPersoon, 'voorvoegsel', Voorvoegsel);
  Achternaam := ace.JsonGetStr(jPersoon, 'achternaam');  
             
  // Person mag nog niet bestaan
  if ace.FindObject('Persoon', PersoonID, cPersoon)
     then ace.ProcessingFailed('persoon', 'Persoon bestaat al.');
             
  // Sla eigenschappen op
  cPersoon := ace.AddObject('Persoon', [PersoonID]);
  ace.AddPropertyInt(cPersoon, 'Geboortedatum', Geboortedatum);
  ace.AddPropertyStr(cPersoon, 'Geslacht', Geslacht);    
  ace.AddPropertyStr(cPersoon, 'Voornaam', Voornaam);
  if Voorvoegsel<>'' 
     then ace.AddPropertyStr(cPersoon, 'Voorvoegsel', Voorvoegsel);
  ace.AddPropertyStr(cPersoon, 'Achternaam', Achternaam);
  end;


// -----------------------------------------------------------------------------
// -- Registreer Geboorte - Verstuur notificatie
// -- 
procedure Operation_RegistreerGeboorte_AfterCommit;
  var
    cPersoon: TcClaim;
  begin
  cPersoon := Operation.FindClaim('Persoon', '');
  if not Assigned(cPersoon)
     then ace.ProcessingFailed('persoon', 'Operatie bevat geen persoon claim.');
     
  ace.BroadcastCloudEvent('persoon-geboren', 'persoon', cPersoon.IdentityStr);
  end;
  

// -----------------------------------------------------------------------------
// -- Corrigeer Voornaam - Verwerkt request
// -- 
procedure Operation_CorrigeerVoornaam_OnReceive;
  var
    jRoot, jPersoon: TJsonObject;
    cPersoon: TcClaim;    
    PersoonID, Voornaam: string;
    Geboortedatum: integer;    
  begin
  jRoot := Request.Json;
  Generic_OnReceive(jRoot);
  
  // Haal persoon op
  jPersoon := ace.JsonGetObj(jRoot, 'persoon');
  
  // Haal eigenschappen op
  PersoonID := ace.JsonGetStr(jPersoon, 'ID');
  Voornaam := ace.JsonGetStr(jPersoon, 'voornaam');
  
  // Zoek persoon
  if not ace.FindObject('Persoon', PersoonID, cPersoon)
     then ace.ProcessingFailed('persoon', 'Persoon niet gevonden.');
     
  // Registreer vanaf geboortedatum
  Geboortedatum := ace.GetPropertyInt(cPersoon, 'Geboortedatum');
  ace.SetValidFrom(Geboortedatum);
  ace.AddPropertyStr(cPersoon, 'Voornaam', Voornaam); 
  end;
  
  
// -----------------------------------------------------------------------------
// -- Registreer Twijfel
// --   
procedure Operation_RegistreerTwijfel_OnReceive;
  var
    jRoot: TJsonObject;
    cObject, cProperty: TcClaim;
    ObjectTypeName, ObjectID, PropertyName: string;
  begin
  jRoot := Request.Json;
  Generic_OnReceive(jRoot);

  // Haal info op
  ObjectTypeName := ace.JsonGetStr(jRoot, 'objecttype');
  ObjectID := ace.JsonGetStr(jRoot, 'objectID');
  ace.JsonTryGetStr(jRoot, 'property', PropertyName);
  
  // Zoek Object
  if not ace.FindClaim(ObjectTypeName, ObjectID, cObject)
     then ace.ProcessingFailed(ObjectTypeName, '%s niet gevonden');
  // Property?
  if PropertyName = ''
     then begin
          // Registreer twijfel op Object
          ace.Annotate(cObject, 'Twijfel', []);
          end
     else begin
          // Registreer twijfel op eigenschap
          cProperty := ace.GetProperty(cObject, PropertyName);
          ace.Annotate(cProperty, 'Twijfel', []);
          end;          
  end;


// -----------------------------------------------------------------------------
// -- Registreer Adoptie
// -- 
procedure Operation_RegistreerAdoptie_OnReceive;
  var
    jRoot, jPersoon: TJsonObject;
    cPersoon: TcClaim;
    PersoonID, Voornaam, Voorvoegsel, Achternaam: string;
  begin
  jRoot := Request.Json;
  Generic_OnReceive(jRoot);
  
  // Haal persoon op
  jPersoon := ace.JsonGetObj(jRoot, 'persoon');
  
  // Haal eigenschappen op
  PersoonID := ace.JsonGetStr(jPersoon, 'ID');
  Voornaam := ace.JsonGetStr(jPersoon, 'voornaam');  
  ace.JsonTryGetStr(jPersoon, 'voorvoegsel', Voorvoegsel);
  Achternaam := ace.JsonGetStr(jPersoon, 'achternaam');  
             
  // Person moet bestaan
  if not ace.FindObject('Persoon', PersoonID, cPersoon)
     then ace.ProcessingFailed('persoon', 'Persoon niet gevonden.');
             
  // Wijzig eigenschappen
  ace.AddPropertyStr(cPersoon, 'Voornaam', Voornaam);
  if Voorvoegsel<>'' 
     then ace.AddPropertyStr(cPersoon, 'Voorvoegsel', Voorvoegsel);
  ace.AddPropertyStr(cPersoon, 'Achternaam', Achternaam);
  end;
  
  
// -----------------------------------------------------------------------------
// -- Registreer Adoptie - Verstuur notificatie
// -- 
procedure Operation_RegistreerAdoptie_AfterCommit;
  var
    jRoot, jPersoon: TJsonObject;
    PersoonID: string;
  begin
  // Haal persoon en ID op
  jRoot := Request.Json;  
  jPersoon := ace.JsonGetObj(jRoot, 'persoon');
  PersoonID := ace.JsonGetStr(jPersoon, 'ID');
      
  ace.BroadcastCloudEvent('persoon-geadopteerd', 'persoon', PersoonID);
  ace.BroadcastCloudEvent('ouderschap-gewijzigd', 'persoon', PersoonID);  
  end;    


// -----------------------------------------------------------------------------
// -- Registreer Geslachtswijziging
// -- 
procedure Operation_RegistreerGeslachtswijziging_OnReceive;
  var
    jRoot, jPersoon: TJsonObject;
    cPersoon: TcClaim;
    PersoonID, Geslacht, Voornaam: string;
    Geboortedatum: integer;
  begin
  jRoot := Request.Json;
  Generic_OnReceive(jRoot);
  
  // Haal persoon op
  jPersoon := ace.JsonGetObj(jRoot, 'persoon');
  
  // Haal eigenschappen op
  PersoonID := ace.JsonGetStr(jPersoon, 'ID');
  Geslacht := ace.JsonGetStr(jPersoon, 'geslacht');    
  Voornaam := ace.JsonGetStr(jPersoon, 'voornaam');  
             
  // Person moet bestaan
  if not ace.FindObject('Persoon', PersoonID, cPersoon)
     then ace.ProcessingFailed('persoon', 'Persoon niet gevonden.');
           
  // Persoon moet minimaal 12 jaar zijn
  ace.StartRule('Leeftijdscontrole bij geslachtswijziging');
  Geboortedatum := ace.GetPropertyInt(cPersoon, 'Geboortedatum');
  if Operation.ValidFrom - GeboorteDatum < 12
     then ace.RuleViolation('Geslachtswijziging is toegestaan vanaf 12 jaar.');
  ace.EndRule;
             
  // Wijzig eigenschappen
  ace.AddPropertyStr(cPersoon, 'Geslacht', Geslacht);  
  ace.AddPropertyStr(cPersoon, 'Voornaam', Voornaam);
  end;  
  

// -----------------------------------------------------------------------------
// -- Vergeet Geslachtswijziging
// -- 
procedure Operation_VergeetGeslachtswijziging_OnReceive;
  var
    jRoot, jPersoon: TJsonObject;
    cPersoon: TcClaim;
    PersoonID, Geslacht, Voornaam: string;
    Geboortedatum: integer;
  begin
  jRoot := Request.Json;
  Generic_OnReceive(jRoot);
  
  // Haal persoon op
  jPersoon := ace.JsonGetObj(jRoot, 'persoon');
  
  // Haal eigenschappen op
  PersoonID := ace.JsonGetStr(jPersoon, 'ID');
             
  // Person moet bestaan
  if not ace.FindObject('Persoon', PersoonID, cPersoon)
     then ace.ProcessingFailed('persoon', 'Persoon niet gevonden.');
             
  // Haal eigenschappen op
  Geboortedatum := ace.GetPropertyInt(cPersoon, 'Geboortedatum');
  Geslacht := ace.GetPropertyStr(cPersoon, 'Geslacht');  
  Voornaam := ace.GetPropertyStr(cPersoon, 'Voornaam');
  
  // Registreer deze opnieuw vanaf de geboorte datum   
  ace.SetValidFrom(Geboortedatum); 
  ace.AddPropertyStr(cPersoon, 'Geslacht', Geslacht);  
  ace.AddPropertyStr(cPersoon, 'Voornaam', Voornaam);
  end;   

