# Examples

'''
Always use the file menu to open an example. It will ask for the folder of the example and load the necessary files.
'''

The 'load' function in the console does not reset the engine. It will just load a script that can be executed on the structures and data already present in the engine.

Some examples contain (experimental) features that bring the engine in a inconsistent state. Restarting the program can help if a new example fails after having examined another example.

## Folder: 1_Demo

Demos prepared to illustrate concepts.

### 1_Claims_Grouping_API

Goal:
Create a simple structure, visualise it, add data (with history), group the claimtypes into classes, generate a yaml for OAS, launch API server, test server to travel back in time

The script:
- Creates an information model with: Persons, Building, Ownership, Student, Apartment and Tenants.
- Shows the Claim Type diagam.
- Adds several tuples per claim where one person has multiple mutations on the name to build some history.
- Groups the Claim Types into a Class structure.
- Generates an OAS specification.
- Runs an simple HTTP Server that can server the generated OAS.

Instructions:
- Run the script and repeatedly press execute to walk through the different steps in the script.
- A yaml script with an Open API specification will be generated in the folder of the example.
- Load this script into [Swagger UI](https://editor-next.swagger.io/) or a similar tool.
- Enter the ip address and port of the server (http://127.0.0.1:4201)
- Go to the /Persons end point (NOT the /Persons/{id})
- Click 'try it out'
- Enter personID: p1
- Click 'Execute'
- Response should be 'Weisleigh Miller' (The current value)
- Enter 42 in the 'Knowledge of' field and re-execute
- Response should be 'Wesley Miller'.

### 2_History

Goal:
Explore how information changes over time and can be presented in atomic bi-temporal diagrams.

The script:
- Creates an information model of a person with a first name and last name. (Dutch example)
- Shows the Claim Type diagram.
- Adds the initial situation of the person.
- Adds several changes.
- Shows the changes in the Inspector.

Instructions:
- Run the script and repeatedly press execute to walk through the different steps in the script.

### 3_Operations_Doubt

Goal:
Illustrates the concepts Command (transaction) and annotation of Doubt.

The script:
- Creates an information model of a person with a first name and last name. (Dutch example)
- Creates an information model for commands and their context.
- Adds the initial situation of the person with a command.
- Adds several changes - with commands.
- Show the changes and the commands in the Inspector.
- One of the steps adds an annotation of Doubt.

Instructions:
- Run the script and repeatedly press execute to walk through the different steps in the script.
- When a claim ID is asked (to register doubt) enter the ID of the source claim 'Karel' (left side stack of claims).

### 4_Privacy

Goal:
Illustrates how a request to 'hide' or 'remove' data could be interpreted.

The script:
- Executes the script of demo 3 to the point that we have changed the gender and name of the person.
- In the first step it introduces a new claim that became valid from birth. This claim hides the claims below - for those who can only see the validity time line.
- In the second step the change is reverted. And the id of a claim is asked. The idea is that the ID of the 'blue' claim is entered. Thereby erasing the past from the validity time line.
- In the third step the change is again reverted. Next it asks for all the ID's in the schema on the right, deleting all information. Than it falsifies even the system time to pretend the name has always been 'Eve'.

### 5_Annotations

Goal:
Experiment: Should we end validity and/or expire using commands or annotations?

The script:
- Executes a script to create some structure and claims to work with.
- 1st step: Expire a claim with a command (pick any claim)
- 2nd step: End validity of a claim with a command
- 3rd step: Expire by adding an annotation.
- 4th step: End validity of a claim by adding an annotation.

### 6_Concurrency

NOTE: This example MUST be loaded using the File -> Open option !!!

Goal:
Illustrate transactions.

The script:
- Opens three consoles.
- It walks through a script where the names of person A, B and C are changed in different consoles.
- Conveniently the names of those persons start with the letter corresponding to A, B and C.
- A red border is shown around the input field that needs your attention.
- Walk through the scripts by clicking on 'execute' in the console that has a red border around it.
- After the first click the Transactions monitor will be shown to illustrate which data is present in the engine.
- By carefully walking through the steps and examining the data show in the consoles you can observe that the transactions are isolated.

This example shows that the engine successfully implements Multi Versioning Concurrency Control (MVCC).

### 7_Example_UI

Goal:
Illustration how events, context, doubt and history could be combined in a user interface in an attempt to hide some of the complexity.

The script:
- Executes a script to create some structure and claims to work with.
- It asks for an entity to display in the Sample UI. Select 'Persoon' and 'p1' (Basically in this demo the only useful values).
- Click on the stopwatch symbol to show detailed information.
- Select the different events to see how data changed through time. (Notice the colours of the edits. Yellow means the value was changed in the selected event. White means the value was already present)
- Hover over the red dot to see a verbalisation of the indication of doubt.
- Select the 'Registratie Geslachtswijzing' event to see that there are two historical values on the x-axis (that can be navigated below the left pane).

### 8_Operation_API

Goal:
- Illustrate a simple implementation of Commands issues through an specific API (called operations).
- Illustrate command handlers that can translate the incoming command to claims.
- Illustrate the concept of a conversational API. During one of the commands a rule is violated that checks the persons age. However the date birth is in doubt. Therefore the conclusion of the rule should be in doubt. By issuing a new command the rule is 'overruled'. This is accompanied by an explanation that is stored in the engine as context.
- Illustrate how a command leads to meaningful notifications.

Instructions:
- Start 'Poster.exe'. This application will send commands to Ace. 
- Click the button on the right of 'Select a file' and navigate to 'samples/1_Demo/9_Operation_Api/Request/requests.ini'.
- The poster application will now show the commands that will be send to Ace.
- Open Ace.exe and open the demo.
- Open NotificationHub.exe. This application will receive notifications that are broadcasted after handling the commands.
- Now return to Ace and execute the script to prepares the engine to receive commands about persons and start a command API server.
- Execute the commands in Poster from top to bottom.
- You can issue the command 'show persoon\voornaam' to follow the progress in the engine.

## Folder: 2_Concurrency

Test files concerning concurrency aspects that may be interesting to examine.

All the examples in this folder MUST be loaded using the File -> Open option !!!
Reset ACE after each example. It is known that these examples often leave the engine in an inconsistent state (sorry).

### Concurrency_Locking

Tests locking. Two consoles are opened trying to manipulate the same claim. One should fail.

### Concurrency_Stable_Snapshot

Demo 6 (Concurrency) illustrated that the engine successfully implements Multi Versioning Concurrency Control (MVCC).
MVCC is used to implement the 'snapshot stability' isolation level see [Wikipedia](https://en.wikipedia.org/wiki/Snapshot_isolation).

From the perspective of a session (here represented by a console) Snapshot isolation does show committed data from other transactions as soon as the transaction in the console is committed. 

The example followed the same pattern of Demo 6. It opens three consoles. Each console changing the name of a different person. Only this time console 1 and 3 that correspondingly manipulate person A and C are finished with their transaction before console 2 is. As soon as console 2 finishes changing the name of person B, the changes to A and C would be visible for console 2. Basically offering console 2 an inconsistent view.

Console B starts the following transaction:

start Transaction
  readonly
  stable

In this mode the engine will automatically search for the most recent moment in time where all transactions were committed. This will be the moment just before the longest running transaction. It will use this moment in queries thereby rendering a stable snapshot. Every data in this snapshot can be reproduced by traveling back in time to the same moment. (That's why starting the transaction responds by stating the moment it will use for the transaction).

(What makes this hard? It's not just time traveling in queries. Commits from our own session should not be ignored).

### Concurrency1

Explains the mechanism of a Stable Snapshot. See 'Concurrency_Stable_Snapshot'

### Concurrency2

This example is probably identical to Demo 6. Illustration Snapshot Isolation.

### Concurrency3

This example is shows three different ways to handle a request to 'forget' information.

Console A: Adheres to the Append only mechanism. It only forgets the information in the valid timeline.
Console B: Deletes older versions thereby creating a 'hole' in the memory of the system.
Console C: Does a complete overwrite.

While executing the example console tree will ask for an ID. In the transaction window search the claim with 'Caleb'. Then search the id behind 'SC'. This ID should be entered.

## Folder: 3_Test

Test files that may be interesting to examine.

### History_Variants

An example where we change the name of a person and multiple permutations of history settings are explored.

### History_Time_Auto

An example where we change the name of a person and time progresses automatically. (This has become the default of the demo).

### History_Time_Manual

An example where we change the time manually. This is how the engine once started. It's very easy to make mistakes in this mode that look like bugs in the engine. So this is no longer the default mode. It is some in some examples for extreme forms of falsification of history.

### Concrete_vs_Referencing_Tuples

Experiments with tuple references: By value or by claim ID.

### Grouping_Tuples

This example was created to test an algorithm that couldn't only group Claim Types but also all historical occurrences of Claims of those claim types.

### Klantinteractie

An attempt to generate Claim Types by reverse engineering an UML Model following the Dutch MIM standard, created in Enterprise Architect and exported to an xmi file.

The script asks for the xmi file to be converted. Select the 'Klantinteracties-SIM-model.xmi' file. Claim Types will be generated and a filename is asked to save a script with the generated Claim Types. Save the script, open it and paste the contents in the inspector. Execute to create the structure and use a modeller to inspect it.

## Folder: 4_Development

### Schema

Example with Claim Types that describe Claims, Claim Types and Annotations.

### Tutorial

Attempt to show images next to console to explain steps. Where the images could be created as a PowerPoint and exported.

