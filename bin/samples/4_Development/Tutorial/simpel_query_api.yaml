openapi: "3.1.0"
servers:
- description: "Test server"
  url: "http://127.0.0.1:4201"
info:
  title: "1_Claims_Grouping_API"
  description: "Test API to access the data of 1_Claims_Grouping_API in the Atomic Claim Engine."
  version: 0.0.1
  contact:
    url: "https://atomicclaimengine.org"
    email: "api@atomicclaimengine.org"
  license:
    name: "European Union Public License, version 1.2 (EUPL-1.2)"
    url: "https://eupl.eu/1.2/nl/"
paths:
  /Addresses:
    get:
      operationId: "getAddress"
      description: "Operation to retrieve information about a(n) Address."
      parameters:
      - name: "addressID"
        in: "query"
        required: true
        schema:
          $ref: "#/components/schemas/addressID"
      - name: "ValidOn"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      - name: "KnowledgeOf"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Address"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Addresses"
  /Addresses/{id}:
    get:
      operationId: "getAddress_tid"
      description: "Operation to retrieve information about a(n) Address."
      parameters:
      - in: "path"
        name: "id"
        description: "Unique technical key for a(n) Address."
        required: true
        schema:
          $ref: "#/components/schemas/TechnicalID"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Address"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Addresses"
  /Apartments:
    get:
      operationId: "getApartment"
      description: "Operation to retrieve information about a(n) Apartment."
      parameters:
      - name: "apartmentID"
        in: "query"
        required: true
        schema:
          $ref: "#/components/schemas/apartmentID"
      - name: "ValidOn"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      - name: "KnowledgeOf"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Apartment"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Apartments"
  /Apartments/{id}:
    get:
      operationId: "getApartment_tid"
      description: "Operation to retrieve information about a(n) Apartment."
      parameters:
      - in: "path"
        name: "id"
        description: "Unique technical key for a(n) Apartment."
        required: true
        schema:
          $ref: "#/components/schemas/TechnicalID"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Apartment"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Apartments"
  /Buildings:
    get:
      operationId: "getBuilding"
      description: "Operation to retrieve information about a(n) Building."
      parameters:
      - name: "buildingID"
        in: "query"
        required: true
        schema:
          $ref: "#/components/schemas/buildingID"
      - name: "ValidOn"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      - name: "KnowledgeOf"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Building"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Buildings"
  /Buildings/{id}:
    get:
      operationId: "getBuilding_tid"
      description: "Operation to retrieve information about a(n) Building."
      parameters:
      - in: "path"
        name: "id"
        description: "Unique technical key for a(n) Building."
        required: true
        schema:
          $ref: "#/components/schemas/TechnicalID"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Building"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Buildings"
  /Ownerships:
    get:
      operationId: "getBuildingOwnership"
      description: "Operation to retrieve information about a(n) BuildingOwnership."
      parameters:
      - name: "Building"
        in: "query"
        required: true
        schema:
          $ref: "#/components/schemas/TechnicalID"
      - name: "Person"
        in: "query"
        required: true
        schema:
          $ref: "#/components/schemas/TechnicalID"
      - name: "ValidOn"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      - name: "KnowledgeOf"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/BuildingOwnership"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Ownerships"
  /Ownerships/{id}:
    get:
      operationId: "getBuildingOwnership_tid"
      description: "Operation to retrieve information about a(n) BuildingOwnership."
      parameters:
      - in: "path"
        name: "id"
        description: "Unique technical key for a(n) BuildingOwnership."
        required: true
        schema:
          $ref: "#/components/schemas/TechnicalID"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/BuildingOwnership"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Ownerships"
  /Persons:
    get:
      operationId: "getPerson"
      description: "Operation to retrieve information about a(n) Person."
      parameters:
      - name: "personID"
        in: "query"
        required: true
        schema:
          $ref: "#/components/schemas/personID"
      - name: "ValidOn"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      - name: "KnowledgeOf"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Person"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Persons"
  /Persons/{id}:
    get:
      operationId: "getPerson_tid"
      description: "Operation to retrieve information about a(n) Person."
      parameters:
      - in: "path"
        name: "id"
        description: "Unique technical key for a(n) Person."
        required: true
        schema:
          $ref: "#/components/schemas/TechnicalID"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Person"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Persons"
  /Students:
    get:
      operationId: "getStudent"
      description: "Operation to retrieve information about a(n) Student."
      parameters:
      - name: "Person"
        in: "query"
        required: true
        schema:
          $ref: "#/components/schemas/TechnicalID"
      - name: "ValidOn"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      - name: "KnowledgeOf"
        in: "query"
        required: false
        schema:
          $ref: "#/components/schemas/SimpleTime"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Student"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Students"
  /Students/{id}:
    get:
      operationId: "getStudent_tid"
      description: "Operation to retrieve information about a(n) Student."
      parameters:
      - in: "path"
        name: "id"
        description: "Unique technical key for a(n) Student."
        required: true
        schema:
          $ref: "#/components/schemas/TechnicalID"
      responses:
        200:
          description: "Ok"
          headers:
            api-version:
              $ref: "#/components/headers/api_version"
            warning:
              $ref: "#/components/headers/warning"
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Student"
        400:
          $ref: "#/components/responses/400"
      tags:
      - "Students"
components:
  headers:
    api_version:
      schema:
        type: string
        description: Geeft een specifieke API-versie aan in de context van een specifieke aanroep.
    warning:
      schema:
        type: string
        description: Zie RFC 7234.
  responses:
    '400':
      description: Bad Request
      headers:
        api-version:
          $ref: "#/components/headers/api_version"
      content:
        application/problem+json:
          schema:
            $ref: "#/components/schemas/BadRequestFoutbericht"
  schemas:
    Foutbericht:
      type: object
      description: Terugmelding bij een fout. JSON representatie in lijn met [RFC7807](https://tools.ietf.org/html/rfc7807).
      properties:
        type:
          type: string
          format: uri
          description: Link naar meer informatie over deze fout
        title:
          type: string
          description: Beschrijving van de fout
        status:
          type: integer
          description: Http status code
        detail:
          type: string
          description: Details over de fout
        instance:
          type: string
          format: uri
          description: Uri van de aanroep die de fout heeft veroorzaakt
        code:
          type: string
          description: Systeemcode die het type fout aangeeft
          minLength: 1
    InvalidParams:
      type: object
      description: Details over fouten in opgegeven parameters
      properties:
        type:
          type: string
          format: uri
        name:
          type: string
          description: Naam van de parameter
        code:
          type: string
          description: Systeemcode die het type fout aangeeft
          minLength: 1
        reason:
          type: string
          description: Beschrijving van de fout op de parameterwaarde
    BadRequestFoutbericht:
      allOf:
      - $ref: "#/components/schemas/Foutbericht"
      - type: "object"
        properties:
          invalidParams:
            description: Foutmelding per fout in een parameter. Alle gevonden fouten worden ��n keer teruggemeld.
            type: array
            items:
              $ref: "#/components/schemas/InvalidParams"
    TechnicalID:
      type: "string"
      description: "Unieke technische identificerende code, primair bedoeld voor gebruik bij interacties tussen IT-systemen."
      readOnly: true
      maxLength: 40
      minLength: 1
    Address:
      type: "object"
      description: "Address"
      properties:
        addressID:
          $ref: "#/components/schemas/addressID"
        postalCode:
          $ref: "#/components/schemas/postalCode"
        streetName:
          $ref: "#/components/schemas/streetName"
    Apartment:
      type: "object"
      description: "Apartment"
      properties:
        apartmentID:
          $ref: "#/components/schemas/apartmentID"
        BuildingId:
          $ref: "#/components/schemas/TechnicalID"
        StudentId:
          $ref: "#/components/schemas/TechnicalID"
    Building:
      type: "object"
      description: "Building"
      properties:
        buildingID:
          $ref: "#/components/schemas/buildingID"
        AddressId:
          $ref: "#/components/schemas/TechnicalID"
    BuildingOwnership:
      type: "object"
      description: "BuildingOwnership"
      properties:
        BuildingId:
          $ref: "#/components/schemas/TechnicalID"
        PersonId:
          $ref: "#/components/schemas/TechnicalID"
        percentage:
          $ref: "#/components/schemas/percentage"
    Person:
      type: "object"
      description: "Person"
      properties:
        personID:
          $ref: "#/components/schemas/personID"
        firstName:
          $ref: "#/components/schemas/firstName"
        lastName:
          $ref: "#/components/schemas/lastName"
    Student:
      type: "object"
      description: "Student"
      properties:
        PersonId:
          $ref: "#/components/schemas/TechnicalID"
    addressID:
      type: "string"
    apartmentID:
      type: "string"
    buildingID:
      type: "string"
    firstName:
      type: "string"
    lastName:
      type: "string"
    percentage:
      type: "integer"
    personID:
      type: "string"
    postalCode:
      type: "string"
    streetName:
      type: "string"
    SimpleTime:
      type: "integer"
