## Console 1
// Main Console
//
// Responsible for creating initial dataset

show Transactions

define labelType: id
  scalar: integer

define claimType: Persoon
  expressionTemplate: Er bestaat een persoon <id>
  nestedExpressionTemplate: persoon <id>

define labelType: voornaam
  scalar: string

define claimType: Persoon\Voornaam
  expressionTemplate: <Persoon> heet <voornaam>

register Persoon: A
register Persoon\Voornaam: A, Adam
  from: 1
register Persoon: B
register Persoon\Voornaam: B, Brandon
  from: 1
register Persoon: C
register Persoon\Voornaam: C, Caleb
  from: 1

wait

// Console 1 starts a transaction to change A

start Operation: Register Name Change

register Persoon\Voornaam: A, Adeline
  from: 5

wait

console: 2

wait

// When snapshot isolation is working:
// A = Adam, changed to Adeline
// B = Brandon
// C = Caleb

list Persoon\Voornaam

commit

console: 2

## Console 2
// Console 2 starts a transaction to change B

start Operation: Register Name Change

register Persoon\Voornaam: B, Beatrice
  from: 1

wait

console: 3

wait

// When snapshot isolation is working:
// A = Adam
// B = Brandon, changed to Beatrice
// C = Caleb

list Persoon\Voornaam

commit

wait

start Transaction

// When snapshot isolation is working:
// A = Adam, changed to Adeline
// B = Brandon, changed to Beatrice
// C = Caleb, changed to Charlotte

list Persoon\Voornaam

commit

## Console 3
// Console 3 starts a transaction to change C.

start Operation: Register Name Change

delete $ask

register Persoon\Voornaam: C, Charlotte
  from: 1

wait

// When snapshot isolation is working:
// A = Adam
// B = Brandon
// C = Caleb changed to Charlotte

list Persoon\Voornaam

commit

console: 1



