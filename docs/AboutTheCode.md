## About the code

### Language & Platforms

Language: Free Pascal / Lazarus
Platform: Windows, Linux

### Getting started

| Folder      | Code file                        | Purpose |
| ----------- | -------------------------------- | ----------- | 
|             | ACE.lpr                          | Main program. |
| core        | ace.Core.Engine.pas              | The Atomic Claim Engine. |
| core        | ace.Core.Query.pas               | Queries on claimsets (incl. time travel queries). |
| interface   | ace.Forms.Main.pas               | Main form. |
| interface   | ace.Forms.Console.pas            | Console used to send commands to the engine. |
| commands    | ace.Commands.Dispatcher.pas      | Command interpreter and dispatcher. |
| commands    | ace.Commands.*.pas               | Command handlers (See individual descriptions below). |
| interface   | ace.Forms.Model.pas              | Form to draw models with ClaimTypes or Classses. |
| interface   | ace.Forms.Inspector.pas          | Form to inspect Operations, Claims and Annotations. |

### Short explanation per file

| Folder      | Code file                        | Purpose |
| ----------- | -------------------------------- | ----------- | 
|             | ACE.lpr                          | Main program. |
|             | ace.Settings.pas                 | Settings read from ini file. |
|             | ace.Debug.pas                    | Utility routines for debugging. |
| API         | ace.API.Server.pas               | Simple Http Server implementation to respond to API requests (not a real REST server, just basic responses). |
| API         | ace.API.SimpleQuery.pas          | Implements a simple API to query a class. |
| API         | ace.API.Operations.pas           | Implements a simple API to reroute incomming operations to pascal script methods that handle translation to claims, validation etc. |
| commands    | ace.Commands.Dispatcher.pas      | Command interpreter and dispatcher. |
| commands    | ace.Commands.Tokenizer.pas	     | Tokenizer used to tokenize commands. |
| commands    | ace.Commands.Structure.pas       | Commands to define LabelTypes and ClaimTypes. |
| commands    | ace.Commands.Claims.pas          | Commands to register and manipulate Claims. |
| commands    | ace.Commands.Transactions.pas    | Commands to work with transactions and operations. |
| commands    | ace.Commands.List.pas            | Commands to list (display in console) ClaimTypes and their Claims. |
| commands    | ace.Commands.Show.pas            | Commands to visualise ClaimSets (temporal diagram). |
| commands    | ace.Commands.Algorithms.pas      | Commands to group ClaimTypes into a grouped model and to generate a yaml specification. |
| commands    | ace.Commands.Portability.pas     | Commands to import VNG/MIM EA XMI file. |
| commands    | ace.Commands.Misc.pas            | Miscellaneous commands. |
| common      | common.ErrorHandling.pas         | Collection of general purpose error routines. |
| common      | common.FileUtils.pas             | Miscellaneous file utility routines for Delphi compatibility. |
| common      | common.InputQueryList.pas        | Dialog to ask a value from a predefined list. |
| common      | common.Json.pas                  | Miscellaneous Json utility routines for Delphi compatibility. |
| common      | common.RadioStation.pas          | Classes to broadcast and listen to events. |
| common      | common.SimpleLists.pas           | Simple list classes expressed as wrappers around Delphi classes (to make furture portability to another language easier). |
| common      | common.Utils.pas                 | Miscellaneous utility routines. |
| core        | ace.Core.Engine.pas              | The Atomic Claim Engine. |
| core        | ace.Core.Tokenizer.pas           | Tokenizer used to tokenize claimtype expressions. |
| core        | ace.Core.Query.pas               | Queries on claimsets (incl. time travel queries). |
| core        | ace.Core.Notification            | Routines to broadcast a CloudEvent. |
| core        | ace.Core.Graph.pas               | Graphs for Typed Operations and Predefined Queries. |
| diagram     | diagram.Core.pas                 | Visualisation library (core). |
| diagram     | diagram.Model.pas                | Visualisation of models with ClaimTypes or Classes. Used in ace.Forms.Model. |
| diagram     | diagram.Settings.pas             | Settings used to draw graphs with claims, operations etc. |
| diagram     | diagram.Claim.pas                | Visualisation of a Claim. Used in diagram.SourceClaims and diagram.Temporal |
| diagram     | diagram.ClaimSets.pas            | Visualisation of ClaimSets in engine. Used in ace.Forms.Transactions. |
| diagram     | diagram.InfoPanel.pas            | Visualisation of a panel with information about an operation, claim, etc. Used in ace.Forms.ClaimSet. |
| diagram     | diagram.Lines.pas                | Lines between objects in inspector. |
| diagram     | diagram.Operations.pas           | Visualisation of a stack of Operations. Used in ace.Forms.ClaimSet. |
| diagram     | diagram.Selection.pas            | Class to manage selected items in a diagram. Used in ace.Forms.ClaimSet. |
| diagram     | diagram.SourceClaims.pas         | Visualisation of a stack of Source Claims. Used in ace.Forms.ClaimSet. |
| diagram     | diagram.Temporal.pas             | Visualisation of Temporal ClaimSet diagram. Used in ace.Forms.ClaimSet. |
| diagram     | diagram.Transaction.pas          | Visualisation of Transactions. Used in ace.Forms.Transactions. |
| interface   | ace.Forms.Main.pas               | Main form. |
| interface   | ace.Forms.Console.pas            | Console used to send commands to the engine. |
| interface   | ace.Forms.Model.pas              | Form to draw models with ClaimTypes or Classses. |
| interface   | ace.Forms.Inspector.pas          | Form to inspect Operations, Claims and Annotations. |
| interface   | ace.Forms.Debug.pas              | Console to display debug messages. |
| interface   | ace.Forms.GraphEditor.pas        | Form to experiment with the syntax of operations derived by walking the ClaimType graph from a given start ClaimType. |
| interface   | ace.Forms.HistorySetttings.pas   | Form to change Temporal Settings of a ClaimType (to experiment with derivation of temporal claims from source claims). |
| interface   | ace.Forms.SampleUI.pas           | Form to demo a sample user interface. |
| interface   | ace.Forms.ScriptEditor.pas       | Simple editor to edit pascal scripts that can be executed at runtime. |
| interface   | ace.Forms.Transactions.pas       | Form to inspect transactions and all (beware!) claimsets in the engine. Used to debug concurrency. |
| interface   | ace.Forms.Tutorial               | Form to display tutorial pages. |
| portability | ace.Import.xmi.pas               | Import VNG/MIM XMI from EA. Produces a script that defines LabelTypes and ClaimTypes. |
| scripting   | ace.Script.pas                   | Integrates PascalScript (from RemObjects) in the prototype. Enables writing, compiling and running pascal scripts at runtime. |
| scripting   | ace.Script.*.pas                 | A unit to make types, functions, classes etc of category * available to the script. |

The folder NotificationHub contains a simple application that can receive CloudEvents (broadcasted from the demo) and display them.
